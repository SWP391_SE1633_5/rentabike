/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import Utils.Utils;
import com.google.gson.Gson;
import dao.BlogAccountSaveDao;
import dao.BlogAccountVoteDao;
import dao.BlogCommentDao;
import dao.BlogDao;
import dao.BlogTagDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.Account;
import model.Blog;
import model.BlogTag;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author win
 */
@WebServlet(name="AddBlogController", urlPatterns={"/manage-blog"})
public class ManageBlogController extends HttpServlet {
   
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        map.clear();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            response.sendRedirect("started.jsp");
            return;
        }
        String type = request.getParameter("type");
        //first load
        if(type==null || type.equals("")){
            request.getRequestDispatcher("AddBlog.html").forward(request, response);
            return;
        }
        if(type.equals("update")){
            request.getRequestDispatcher("UpBlog.html").forward(request, response);
            return;
        }
        PrintWriter out = response.getWriter();
        
        if(type.equals("getUpdate")){
            int accId = a.getAccountID();
            int blogId = Integer.parseInt(request.getParameter("blogId"));
            Blog b = BlogDao.getBlogbyBlogIdAndAccId(blogId, accId);
            List<BlogTag> tList = BlogTagDao.getAllBlogTagByBlogId(blogId);
            map.put("Blog", b);
            map.put("TagList", tList);
            map.put("SC", 1);
            out.print(gson.toJson(map));
            out.flush();
        }
        
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        map.clear();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        
        String type = request.getParameter("type");
        if(type.equals("search-tag")){
            String search = request.getParameter("searchString");
            List<BlogTag> tagList = BlogTagDao.findTopFivBlogTagByTagTitleContain(search);
            map.put("TagList", tagList);
            map.put("ME", "Search Success!");
            map.put("SC", 1);
            out.print(gson.toJson(map));
            out.flush();
        }
        if(type.equals("add")){
            int accountId=a.getAccountID();
            String title = request.getParameter("title");
            String content = request.getParameter("content");
            String description = request.getParameter("description");
            String image = request.getParameter("image");
            String timeCreate = Utils.getCurrentDateTime();
            String tagsArr= request.getParameter("tagsArr");
            JSONArray ja = new JSONArray(tagsArr);
            int blogId = BlogDao.addBlog(accountId, content, title, description, image, timeCreate, a.getRole());
            List<Integer> tagList = new ArrayList<>();
            for(int i=0; i<ja.length(); i++){
                JSONObject jsonObject = (JSONObject)ja.get(i);
                try {
                    tagList.add((int)jsonObject.get("id"));
                } catch (Exception e) {
                    int tagId = BlogTagDao.addNewTag((String)jsonObject.get("tagTitle"));
                    tagList.add(tagId);
                }
            }
            for(int i=0; i<tagList.size(); i++){
                BlogTagDao.addNewBlogBlogTag(blogId, tagList.get(i));
            }
            map.put("ME", "Add Success!");
            map.put("Role", a.getRole());
            map.put("SC", 1);
            out.print(gson.toJson(map));
            out.flush();
        }
        if(type.equals("update")){
            int accId = a.getAccountID();
            int blogId = Integer.parseInt(request.getParameter("blogId"));
            String title = request.getParameter("title");
            String content = request.getParameter("content");
            String description = request.getParameter("description");
            String image = request.getParameter("image");
            String updateTime = Utils.getCurrentDateTime();
            String tagsArr= request.getParameter("tagsArr");
            JSONArray ja = new JSONArray(tagsArr);
            
            Blog b = BlogDao.getBlogbyBlogIdAndAccId(blogId, accId);
            if(b==null){
                map.put("ME", "Access Denied");
                map.put("SC", 0);
                out.print(gson.toJson(map));
                out.flush();
                return;
            }
            BlogDao.updateBlog(blogId, content, title, description, image, updateTime, a.getRole());
            BlogTagDao.delAllRelatedBlogTagByBlogId(blogId);
            List<Integer> tagList = new ArrayList<>();
            for(int i=0; i<ja.length(); i++){
                JSONObject jsonObject = (JSONObject)ja.get(i);
                try {
                    BlogTag btd = BlogTagDao.findBlogTagByTagId((int)jsonObject.get("id"));
                    tagList.add(btd.getId());
                } catch (Exception e) {
                    int tagId = BlogTagDao.addNewTag((String)jsonObject.get("tagTitle"));
                    tagList.add(tagId);
                }
            }
            for(int i=0; i<tagList.size(); i++){
                BlogTagDao.addNewBlogBlogTag(blogId, tagList.get(i));
            }
            map.put("ME", "Update Success!");
            map.put("Role", a.getRole());
            map.put("SC", 1);
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        if(type.equals("delete")){
            int accId = a.getAccountID();
            int blogId = Integer.parseInt(request.getParameter("blogId"));
            Blog b = BlogDao.getBlogbyBlogIdAndAccId(blogId, accId);
            if(b==null){
                map.put("ME", "Access Denied");
                map.put("SC", 0);
                out.print(gson.toJson(map));
                out.flush();
                return;
            }
            BlogTagDao.delAllRelatedBlogTagByBlogId(blogId);
            BlogAccountSaveDao.DeleteBlogAccountSaveByBlogId(blogId);
            BlogCommentDao.deleteAllBlogCommentByBlogId(blogId);
            BlogAccountVoteDao.deleteBlogAccountVoteByBId(blogId);
            BlogDao.deleteBlogByBlogId(blogId);
            
            map.put("ME", "Delete Success!");
            map.put("SC", 1);
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
