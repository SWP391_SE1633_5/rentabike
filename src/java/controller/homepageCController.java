/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ProductDB;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.LinkedList;
import model.Product;
import model.ProductReview;
import model.Staff;
import model.User;

/**
 *
 * @author ADMIN
 */
public class homepageCController extends HttpServlet {

    private static final String sqlQuery = "SELECT P.*, [PI].IMAGE, [PS].DISPLACEMENT, [PS].TRANSMISSION, [PS].STARTER, [PS].FUEL_SYSTEM, [PS].FUEL_CAPACITY, [PS].DRY_WEIGHT, [PS].SEAT_HEIGHT\n"
            + "FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS_SPECIFICATIONS AS [PS]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.PRODUCT_ID = [PS].PRODUCT_ID";
    private static final String rentalQuery = "SELECT PRD.MODE FROM PRODUCTS_RENTAL_DETAILS AS PRD, PRODUCTS AS P WHERE P.MODE = 'Rental' AND ? = PRD.PRODUCT_ID";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getReviews(request.getSession());
        getProduct(request.getSession());
        response.sendRedirect("homepageC.jsp");
    }

    private void getProduct(HttpSession session) {
        LinkedList<Product> productRentalList = ProductDB.listProduct(sqlQuery + " AND P.MODE = 'Rental'");
        if (productRentalList != null) {
            for (Product products : productRentalList) {
                String priceMode = ProductDB.searchPersonProduct(products.getProductID(), rentalQuery);
                products.setModeRental(priceMode);
            }
        }
        LinkedList<Product> productSellList = ProductDB.listProduct(sqlQuery + " AND P.MODE = 'Sell'");
        session.setAttribute("productRentalList", productRentalList);
        session.setAttribute("productSellList", productSellList);
    }

    private void getReviews(HttpSession session) {
        LinkedList<ProductReview> listProductReview = ProductDB.productReviewsList();
        if (listProductReview != null) {
            for (ProductReview productReview : listProductReview) {
                String[] splitTime = productReview.getCommentDate().split("-");
                String day = splitTime[2];
                String month = splitTime[1];
                String year = splitTime[0];

                User user = UserDB.searchAccountID(productReview.getAccountID());

                Staff staff = StaffDB.searchAccountID(productReview.getAccountID());

                if (user != null) {
                    productReview.setAccountName(user.getFirstName() + " " + user.getLastName());
                    productReview.setAccountRole(user.getRole());
                    productReview.setImageAccount(user.getAvatar());
                } else {
                    productReview.setAccountName(staff.getFirstName() + " " + staff.getLastName());
                    productReview.setAccountRole(staff.getRole());
                    productReview.setImageAccount(staff.getAvatar());
                }
                productReview.setCommentDate(day + "/" + month + "/" + year);
            }
            session.setAttribute("listProductReview", listProductReview);
        }
    }
}
