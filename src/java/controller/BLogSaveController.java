/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.google.gson.Gson;
import dao.BlogAccountSaveDao;
import dao.BlogDao;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.Account;
import model.Blog;
import model.BlogAccountSave;

/**
 *
 * @author win
 */
@WebServlet(name="BLogSaveController", urlPatterns={"/blog-save"})
public class BLogSaveController extends HttpServlet {
   
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            response.sendRedirect("started.jsp");
            return;
        }
        String type = request.getParameter("type");
        //first load
        if(type==null || type.equals("")){
            request.getRequestDispatcher("BlogSave.html").forward(request, response);
            return;
        }
        PrintWriter out = response.getWriter();
        BlogDao bd = new BlogDao();
        List<BlogAccountSave> blogSaveList = BlogAccountSaveDao.getBlogSaveListByAccountId(a.getAccountID());
        List<Object> blogListRes  = new ArrayList<>();
        List<Blog> blogList  = new ArrayList<>();
        for(BlogAccountSave bas: blogSaveList){
            if(bd.getAuthenBlogbyBlogId(bas.getBlogId(), -1)!=null)
                blogList.add(bd.getAuthenBlogbyBlogId(bas.getBlogId(), -1));
        }
        for(Blog b: blogList){
            HashMap<Object, Object> mapTmp = new HashMap<>();
            mapTmp.put("blog", b);
            if(UserDB.searchAccountID(b.getAccountId())!=null){
                mapTmp.put("author", UserDB.searchAccountID(b.getAccountId()));
            } else if(StaffDB.searchAccountID(b.getAccountId())!=null){
                mapTmp.put("author", StaffDB.searchAccountID(b.getAccountId()));
            }
            blogListRes.add(mapTmp);
        }
        
        map.put("BlogList", blogListRes);
        map.put("SC", 1);
        out.print(gson.toJson(map));
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            map.put("SC", 0);
            map.put("ME", "Please login to do this action!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        String type = request.getParameter("type");
        int blogId = Integer.parseInt(request.getParameter("blogId"));
        if(type.equals("unsave")){
            boolean checkDel = BlogAccountSaveDao.DeleteBlogAccountSaveWithBlogIdAndAccId(blogId, a.getAccountID());
            if(checkDel){
                map.put("SC", 1);
                map.put("ME", "Unsave successfully!");
                out.print(gson.toJson(map));
                out.flush();
                return;
            }
            if(!checkDel){
                map.put("SC", -1);
                map.put("ME", "Unsave fail, try again!");
                out.print(gson.toJson(map));
                out.flush();
                return;
            }
        }
        if(type.equals("save")){
            boolean checkDel = BlogAccountSaveDao.insertBlogAccountSaveWithBlogIdAndAccId(blogId, a.getAccountID());
            if(checkDel){
                map.put("SC", 1);
                map.put("ME", "Save successfully!");
                out.print(gson.toJson(map));
                out.flush();
                return;
            }
            if(!checkDel){
                map.put("SC", -1);
                map.put("ME", "Save fail, try again!");
                out.print(gson.toJson(map));
                out.flush();
                return;
            }
        }
        
    }
    
    
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
