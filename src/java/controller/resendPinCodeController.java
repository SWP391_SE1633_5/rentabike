/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import static controller.loginController.emailHTML;
import static controller.loginController.generatePinCode;
import dao.Manager;
import dao.StaffDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;
import model.Staff;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "resendPinCodeController", urlPatterns = {"/resendPinCodeController"})
public class resendPinCodeController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        Account account = Manager.searchAccount(email);
        PrintWriter out = response.getWriter();
        if (email != null && email.length() != 0 && account != null) {
            Staff staff = StaffDB.searchAccountID(account.getAccountID());
            if (staff != null) {
                StaffDB.clearPinCode(staff.getAccountID());
                String pinCode = generatePinCode();
                String fullName = staff.getFirstName() + " " + staff.getLastName();
                StaffDB.createPinCode(staff.getAccountID(), pinCode);
                StaffDB.updatePinCode(pinCode, staff.getAccountID());

                String to = staff.getEmail();
                String subject = "Pin code verify";
                String message = emailHTML(fullName, pinCode, staff.getEmail(), staff.getAccountID());
                MailController.send(to, subject, message, "lifeofcoffie@gmail.com", "shgwxqwapkgwzqkk");
                out.print("SUCCESS");
            }else{
                out.print("FALSE");
            }
        } else {
            out.print("FAILED");
        }
    }
}
