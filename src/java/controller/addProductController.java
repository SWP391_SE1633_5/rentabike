/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ProductDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Product;

/**
 *
 * @author ASUS
 */
public class addProductController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected static void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            String name = request.getParameter("productName");
            String category = request.getParameter("productCategory");
            String brand = request.getParameter("productBrand");
            String modelyear = request.getParameter("modelYear");
            String Price = request.getParameter("price");
            String Displacement = request.getParameter("displacement");
            String Transmission = request.getParameter("productTransmission");
            String Starter = request.getParameter("productStarter");
            String FuelCapacity = request.getParameter("fuelCapacity");
            String FuelSystem = request.getParameter("FuelSystem");
            String DryWeight = request.getParameter("dryWeight");
            String SeatHeight = request.getParameter("seatHeight");
            String Mode = request.getParameter("productMode");
            String HireMode = request.getParameter("productHireMode");
            Product p = new Product(name, brand,category,Integer.valueOf(modelyear),Float.valueOf(Price), Mode, Integer.valueOf(Displacement), Transmission, Starter, FuelSystem, Float.valueOf(FuelCapacity), Float.valueOf(DryWeight), Float.valueOf(SeatHeight), HireMode);
            ProductDB dao = new ProductDB();
            p.setProductID(dao.getNumberOfProduct()+1);
            dao.saveProduct(p);
            dao.saveProductSpecifications(p);
            dao.insertProductImage(p);
            dao.setProductImage();
            response.sendRedirect(request.getContextPath() + "/productController");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
