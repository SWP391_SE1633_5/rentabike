/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.Manager;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import model.Account;
import model.Staff;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "userListController", urlPatterns = "/userListController")
public class userListController extends HttpServlet {

    private static final String queryTotal = "SELECT COUNT(A.ACCOUNT_ID) FROM ACCOUNT AS A";
    private static final String queryStaff = "SELECT COUNT(ACTIVE) FROM STAFFS WHERE ACTIVE = ?";
    private static final String queryUser = "SELECT COUNT([STATUS]) FROM CUSTOMERS WHERE [STATUS] = ?";
    private static final String queryRoleAccount = "SELECT ACCOUNT_ID, [ROLE] FROM ACCOUNT \n"
            + "ORDER BY (CASE ROLE \n"
            + "WHEN 'ADMIN' THEN 0  \n"
            + "WHEN 'STAFF' THEN 1\n"
            + "WHEN 'CUSTOMER' THEN 2\n"
            + "WHEN 'SALEPERSON' THEN 3\n"
            + "END)";
    private static final String searchQuery = "SELECT DISTINCT C.*, A.ROLE FROM CUSTOMERS AS C, ACCOUNT AS A WHERE C.[ACCOUNT_ID] = ? AND C.EMAIL = A.EMAIL";
    private static final String searchQueryAlt = "SELECT DISTINCT S.*, A.ROLE FROM STAFFS AS S, ACCOUNT AS A WHERE S.EMAIL = A.EMAIL AND S.[ACCOUNT_ID] = ?";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        String pagination = request.getParameter("page");
        PrintWriter out = response.getWriter();

        if (pagination == null) {
            pagination = "1";
        }

        if (account != null) {
            getTotalData(session);
            switch (pagination) {
                case "1":
                    listUser(session);
                    break;
                default:
                    paginationPage(request, out, session);
            }
            response.sendRedirect("app-user-list.jsp");
        } else {
            response.sendRedirect("page-misc-not-authorized.html");
        }
    }

    private void listUser(HttpSession session) {
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();
        LinkedList<Integer> numberPaginationList = new LinkedList<>();
        HashSet<String> listState = new HashSet<>();
        LinkedList<Integer> showEntryList = new LinkedList<>();
        showEntryList.add(10);
        showEntryList.add(25);
        showEntryList.add(50);
        showEntryList.add(100);
        String showEntry = (String) session.getAttribute("showEntry");
        int showEntryCurrent = 10;

        if (showEntry != null) {
            showEntryCurrent = Integer.parseInt(showEntry);
        }

        if (accountListRole != null) {
            for (Account account : accountListRole) {
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountID(account.getAccountID());
                    userAccountList.add(user);
                    listState.add(user.getState());
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountID(account.getAccountID());
                    staffAccountList.add(staff);
                    listState.add(staff.getState());
                }
            }
            int endStaff = staffAccountList.size();
            int endUser = showEntryCurrent - endStaff - 1;
            double showTotalPage = (double) (showEntryCurrent - 1) + 1.0;
            int numberPaginaton = (int) Math.ceil(accountListRole.size() / showTotalPage);
            for (int i = 0; i < numberPaginaton; i++) {
                numberPaginationList.add(i + 1);
            }
            if (showEntry != null) {
                session.setAttribute("endStaff", Integer.parseInt(showEntry) - 1);
            } else {
                session.setAttribute("endStaff", (endStaff - 2));
            }
            List<String> sortedList = new ArrayList<>(listState);
            Collections.sort(sortedList);
            session.setAttribute("listState", sortedList);
            session.setAttribute("showEntry", String.valueOf(showEntryCurrent));
            session.setAttribute("numberPaginationList", numberPaginationList);
            session.setAttribute("currentPage", 1);
            session.setAttribute("limitStart", 1);
            session.setAttribute("limitEnd", showEntryCurrent);
            session.setAttribute("beginStaff", 0);
            session.setAttribute("beginUser", 0);
            session.setAttribute("endUser", endUser);
            session.setAttribute("showEntryList", showEntryList);
            session.setAttribute("userAccountList", userAccountList);
            session.setAttribute("staffAccountList", staffAccountList);
        }
    }

    private void getTotalData(HttpSession session) {
        int totalUser = Manager.countUserData(queryTotal, -1);
        int activeUser = Manager.countUserData(queryStaff, 1);
        int activeStaff = Manager.countUserData(queryUser, 1);
        int inactiveUser = Manager.countUserData(queryUser, 2);
        int inactiveStaff = Manager.countUserData(queryStaff, 0);
        int bannedUser = Manager.countUserData(queryUser, 0);
        int bannedStaff = Manager.countUserData(queryStaff, 2);

        session.setAttribute("totalAccount", totalUser);
        session.setAttribute("activeAccountTotal", (activeUser + activeStaff));
        session.setAttribute("inactiveAccountTotal", (inactiveStaff + inactiveUser));
        session.setAttribute("bannedAccountTotal", (bannedUser + bannedStaff));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String mode = request.getParameter("mode");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        if (account != null) {
            switch (mode) {
                case "viewProfile":
                    viewProfile(request, out);
                    break;
                case "addNewStaff":
                    addNewStaff(request, out);
                    break;
                case "showEntry":
                    paginationPage(request, out, session);
                    break;
                case "searchName":
                    searchUserName(request, out);
                    break;
                case "selectRole":
                    selectRole(request, out);
                    break;
                case "selectStatus":
                    selectStatus(request, out);
                    break;
                case "selectState":
                    selectState(request, out);
                    break;
            }
        } else {
            response.sendRedirect("page-misc-not-authorized.html");
        }
    }

    private void paginationPage(HttpServletRequest request, PrintWriter out, HttpSession session) {
        String showEntry = request.getParameter("showEntry");
        String showEntryCurrent = (String) session.getAttribute("showEntry");
        int pagination = Integer.parseInt(request.getParameter("page"));
        int nextPage;
        if ((showEntry == null || "".equals(showEntry) || showEntry.length() == 0) && showEntryCurrent == null) {
            nextPage = 10;
        } else {
            if (showEntry != null) {
                nextPage = Integer.parseInt(showEntry);
            } else {
                nextPage = Integer.parseInt(showEntryCurrent);
            }
        }

        if (showEntryCurrent != null) {
            if (showEntry == null) {
                showEntry = showEntryCurrent;
            }
        }
        int totalPage = pagination * nextPage;
        int indexPage = totalPage - nextPage;
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();

        for (int i = indexPage; i < totalPage; i++) {
            if (i < accountListRole.size()) {
                Account account = accountListRole.get(i);
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountID(account.getAccountID());
                    userAccountList.add(user);
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountID(account.getAccountID());
                    staffAccountList.add(staff);
                }
            }
        }
        int endStaff = staffAccountList.size();
        int endUser = nextPage - endStaff - 1;

        session.setAttribute("showEntry", showEntry);
        session.setAttribute("currentPage", pagination);
        session.setAttribute("limitStart", indexPage + 1);
        session.setAttribute("limitEnd", totalPage);
        session.setAttribute("endStaff", endStaff);
        session.setAttribute("endUser", endUser);
        session.setAttribute("userAccountList", userAccountList);
        session.setAttribute("staffAccountList", staffAccountList);
    }

    public void viewProfile(HttpServletRequest request, PrintWriter out) {
        int accountID = Integer.parseInt(request.getParameter("accountID"));
        Account account = Manager.searchAccountID(accountID);

        if (account != null) {
            if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                User user = UserDB.searchAccountID(accountID);
                String splitEvent[] = user.getDateOfJoin().toString().split("-");
                String getTime = splitEvent[2] + "/" + splitEvent[1] + "/" + splitEvent[0];
                out.print(user.getAccountID() + "/;LGN" + user.getFirstName() + " " + user.getLastName()
                        + "/;LGN" + user.getAvatar() + "/;LGN" + user.getEmail() + "/;LGN" + user.getStatus()
                        + "/;LGN" + getTime + "/;LGN" + user.getPhoneNumber() + "/;LGN" + user.getCity()
                        + "/;LGN" + user.getStreet() + "/;LGN" + user.getState() + "/;LGN" + user.getRole());
            } else {
                Staff staff = StaffDB.searchAccountID(accountID);
                String splitEvent[] = staff.getDateOfJoin().toString().split("-");
                String getTime = splitEvent[2] + "/" + splitEvent[1] + "/" + splitEvent[0];
                out.print(staff.getAccountID() + "/;LGN" + staff.getFirstName() + " " + staff.getLastName()
                        + "/;LGN" + staff.getAvatar() + "/;LGN" + staff.getEmail() + "/;LGN" + staff.getActive()
                        + "/;LGN" + getTime + "/;LGN" + staff.getPhoneNumber() + "/;LGN" + staff.getCity()
                        + "/;LGN" + staff.getStreet() + "/;LGN" + staff.getState() + "/;LGN" + staff.getRole());
            }
        }
    }

    private void addNewStaff(HttpServletRequest request, PrintWriter out) {
        String userFullName = request.getParameter("userFullName");
        String userFirstName = request.getParameter("userFirstName");
        String userLastName = request.getParameter("userLastName");
        String userEmail = request.getParameter("userEmail");
        String userContact = request.getParameter("userContact");
        String userCity = request.getParameter("userCity");
        String userStreet = request.getParameter("userStreet");
        String userState = request.getParameter("userState");
        String userRole = request.getParameter("userRole");
        String userPassword = request.getParameter("userPassword");
        Account account = Manager.searchAccount(userEmail);

        if (account != null) {
            out.print("EMAIL");
        } else {
            int statusUpdateAccount = Manager.updateAccount(userEmail, userPassword, userRole);
            Account staffAccount = Manager.searchAccount(userEmail);
            Staff staff = new Staff();
            staff.setAccountID(staffAccount.getAccountID());
            staff.setUserName(userFullName);
            staff.setFirstName(userFirstName);
            staff.setLastName(userLastName);
            staff.setEmail(userEmail);
            staff.setPhoneNumber(userContact);
            staff.setCity(userCity);
            staff.setStreet(userStreet);
            staff.setState(userState);
            int statusUpdateStaff = StaffDB.saveStaff(staff);
            if (statusUpdateStaff > 0 && statusUpdateAccount > 0) {
                out.print("SUCCESS");
            } else {
                out.print("ERROR");
            }
        }

    }

    private void searchUserName(HttpServletRequest request, PrintWriter out) {
        String inputSearch = request.getParameter("inputSearch");
        if ("".equals(inputSearch) || inputSearch.length() == 0) {
            out.print("RESET");
            return;
        }
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();

        if (accountListRole != null) {
            for (Account account : accountListRole) {
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountByOption(account.getAccountID(), inputSearch,
                            searchQuery + " AND CONCAT(C.FIRST_NAME, ' ', C.LAST_NAME) LIKE ?", "Search");
                    if (user != null) {
                        userAccountList.add(user);
                    }
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountByOption(account.getAccountID(), inputSearch,
                            searchQueryAlt + " AND CONCAT(S.FIRST_NAME, ' ', S.LAST_NAME) LIKE ?", "Search");
                    if (staff != null) {
                        staffAccountList.add(staff);
                    }
                }
            }
            displayList(out, staffAccountList, userAccountList);
        } else {
            out.print("RESET");
        }
    }

    private void selectRole(HttpServletRequest request, PrintWriter out) {
        String selectOption = request.getParameter("selectOption");
        if ("".equals(selectOption) || selectOption.length() == 0) {
            out.print("RESET");
            return;
        }
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();

        if (accountListRole != null) {
            for (Account account : accountListRole) {
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountByOption(account.getAccountID(), selectOption,
                            searchQuery + " AND A.ROLE = ?", "Select");
                    if (user != null) {
                        userAccountList.add(user);
                    }
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountByOption(account.getAccountID(), selectOption,
                            searchQueryAlt + " AND A.ROLE = ?", "Select");
                    if (staff != null) {
                        staffAccountList.add(staff);
                    }
                }
            }
            displayList(out, staffAccountList, userAccountList);
        } else {
            out.print("RESET");
        }
    }

    private void selectStatus(HttpServletRequest request, PrintWriter out) {
        String selectOption = request.getParameter("selectOption");
        if ("".equals(selectOption) || selectOption.length() == 0) {
            out.print("RESET");
            return;
        }
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();
        int statusUser, statusStaff;
        if (selectOption.equalsIgnoreCase("Active")) {
            statusUser = 1;
            statusStaff = 1;
        } else if (selectOption.equalsIgnoreCase("Inactive")) {
            statusUser = 2;
            statusStaff = 0;
        } else {
            statusUser = 0;
            statusStaff = 2;
        }

        if (accountListRole != null) {
            for (Account account : accountListRole) {
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountByOption(account.getAccountID(), String.valueOf(statusUser),
                            searchQuery + " AND C.STATUS = ?", "Select");
                    if (user != null) {
                        userAccountList.add(user);
                    }
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountByOption(account.getAccountID(), String.valueOf(statusStaff),
                            searchQueryAlt + " AND S.ACTIVE = ?", "Select");
                    if (staff != null) {
                        staffAccountList.add(staff);
                    }
                }
            }
            displayList(out, staffAccountList, userAccountList);
        } else {
            out.print("RESET");
        }
    }

    private void displayList(PrintWriter out, LinkedList<Staff> staffAccountList, LinkedList<User> userAccountList) {
        for (Staff staff : staffAccountList) {
            String avatarElement;

            if (staff.getAvatar() != null) {
                avatarElement = "<div class=\"avatar-wrapper\">\n"
                        + "<div class=\"avatar  me-1\">\n"
                        + "<img src=\"" + staff.getAvatar() + "\" alt=\"Avatar\"\n"
                        + "height=\"32\" width=\"32\">\n"
                        + "</div>\n"
                        + "</div>\n";
            } else {
                avatarElement = "<div class=\"avatar-wrapper\">\n"
                        + "<div class=\"avatar bg-light-warning me-1\">\n"
                        + "<span class=\"avatar-content\">" + staff.getFirstName().substring(0, 1) + staff.getLastName().substring(0, 1) + "</span>\n"
                        + "</div>\n"
                        + "</div>\n";
            }

            String roleUser;

            if (staff.getRole().equalsIgnoreCase("STAFF")) {
                roleUser = "<span class=\"text-truncate align-middle\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-settings font-medium-3 text-warning me-50\">\n"
                        + "<circle cx=\"12\" cy=\"12\" r=\"3\"></circle>\n"
                        + "<path d=\"M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z\">\n"
                        + "</path>\n"
                        + "</svg>\n"
                        + "<span class=\"roleSort\">Staff</span>\n"
                        + "</span>";
            } else {
                roleUser = "<span class=\"text-truncate align-middle\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\n"
                        + "stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
                        + "class=\"feather feather-slack font-medium-3 text-danger me-50\">\n"
                        + "<path\n"
                        + "d=\"M14.5 10c-.83 0-1.5-.67-1.5-1.5v-5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5z\">\n"
                        + "</path>\n"
                        + "<path d=\"M20.5 10H19V8.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z\"></path>\n"
                        + "<path\n"
                        + "d=\"M9.5 14c.83 0 1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5S8 21.33 8 20.5v-5c0-.83.67-1.5 1.5-1.5z\">\n"
                        + "</path>\n"
                        + "<path d=\"M3.5 14H5v1.5c0 .83-.67 1.5-1.5 1.5S2 16.33 2 15.5 2.67 14 3.5 14z\"></path>\n"
                        + "<path\n"
                        + "d=\"M14 14.5c0-.83.67-1.5 1.5-1.5h5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-5c-.83 0-1.5-.67-1.5-1.5z\">\n"
                        + "</path>\n"
                        + "<path d=\"M15.5 19H14v1.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5z\"></path>\n"
                        + "<path\n"
                        + " d=\"M10 9.5C10 8.67 9.33 8 8.5 8h-5C2.67 8 2 8.67 2 9.5S2.67 11 3.5 11h5c.83 0 1.5-.67 1.5-1.5z\">\n"
                        + "</path>\n"
                        + "<path d=\"M8.5 5H10V3.5C10 2.67 9.33 2 8.5 2S7 2.67 7 3.5 7.67 5 8.5 5z\"></path>\n"
                        + "</svg><span class=\"roleSort\">Admin</span>\n"
                        + "</span>\n";
            }

            String status;
            if (staff.getActive() == 0) {
                status = "<span class=\"badge rounded-pill badge-light-primary\" text-capitalized=\"\">Inactive</span>\n";
            } else if (staff.getActive() == 1) {
                status = "<span class=\"badge rounded-pill badge-light-success\" text-capitalized=\"\">Active</span>\n";
            } else {
                status = "<span class=\"badge rounded-pill badge-light-secondary\" text-capitalized=\"\">Banned</span>\n";
            }
            out.print("<tr>\n"
                    + "<td class=\" control\" tabindex=\"0\" style=\"display: none;\"></td>\n"
                    + "<td class=\" dt-checkboxes-cell\">\n"
                    + "<div class=\"form-check\"> \n"
                    + "<input class=\"form-check-input dt-checkboxes\" type=\"checkbox\" value=\"\"\n"
                    + "id=\"checkbox" + staff.getAccountID() + "\"><label class=\"form-check-label\" for=\"checkbox" + staff.getAccountID() + "\"></label></div>\n"
                    + "</td>\n"
                    + "<td class=\"\">\n"
                    + "<div class=\"d-flex justify-content-left align-items-center\">\n"
                    + avatarElement
                    + "<div class=\"d-flex flex-column\">\n"
                    + "<a href=\"#/\" index=\"" + staff.getAccountID() + "\" onclick=\"sendParamter(this)\"  data-bs-toggle=\"modal\" data-bs-target=\"#defaultSize\"\n"
                    + "class=\"user_name text-body text-truncate\">\n"
                    + "<span class=\"fw-bolder\">" + staff.getFirstName() + " " + staff.getLastName() + "</span>\n"
                    + "</a>\n"
                    + "<small class=\"emp_post text-muted\">" + staff.getEmail() + "</small>\n"
                    + "</div>\n"
                    + "</div>\n"
                    + "</td>\n"
                    + "\n"
                    + "<td class=\"sorting_1\">\n"
                    + roleUser
                    + "</td>\n"
                    + "<td>" + staff.getPhoneNumber() + "</td>\n"
                    + "<td><span class=\"text-nowrap\">" + staff.getState() + "</span></td>\n"
                    + "<td>\n"
                    + status
                    + "</td>\n"
                    + "<td>\n"
                    + "<a class=\"btn btn-sm btn-icon\" data-bs-toggle=\"modal\" data-bs-target=\"#defaultSize\" index=\"" + staff.getAccountID() + "\" onclick=\"sendParamter(this)\">\n"
                    + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\n"
                    + "stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
                    + "class=\"feather feather-eye font-medium-3 text-body\">\n"
                    + "<path d=\"M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z\"></path>\n"
                    + "<circle cx=\"12\" cy=\"12\" r=\"3\"></circle>\n"
                    + "</svg>\n"
                    + "</a>\n"
                    + "</td>\n"
                    + "</tr>");
        }
        for (User user : userAccountList) {
            String avatarElement;

            if (user.getAvatar() != null) {
                avatarElement = "<div class=\"avatar-wrapper\">\n"
                        + "<div class=\"avatar  me-1\">\n"
                        + "<img src=\"" + user.getAvatar() + "\" alt=\"Avatar\"\n"
                        + "height=\"32\" width=\"32\">\n"
                        + "</div>\n"
                        + "</div>\n";
            } else {
                avatarElement = "<div class=\"avatar-wrapper\">\n"
                        + "<div class=\"avatar bg-light-warning me-1\">\n"
                        + "<span class=\"avatar-content\">" + user.getFirstName().substring(0, 1) + user.getLastName().substring(0, 1) + "</span>\n"
                        + "</div>\n"
                        + "</div>\n";
            }

            String roleUser;

            if (user.getRole().equalsIgnoreCase("CUSTOMER")) {
                roleUser = "<span class=\"text-truncate align-middle\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-user font-medium-3 text-primary me-50\">\n"
                        + "<path d=\"M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2\"></path>\n"
                        + "<circle cx=\"12\" cy=\"7\" r=\"4\"></circle>\n"
                        + "</svg><span class=\"roleSort\">Customer</span>\n"
                        + "</span>";
            } else {
                roleUser = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\"\n"
                        + "stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
                        + "class=\"feather feather-truck\"\n"
                        + "style=\"width: 16.8px;height: 16.8px;                                                                        margin-right: 7px;                                                                        \">\n"
                        + "<rect x=\"1\" y=\"3\" width=\"15\" height=\"13\"></rect>\n"
                        + "<polygon points=\"16 8 20 8 23 11 23 16 16 16 16 8\"></polygon>\n"
                        + "<circle cx=\"5.5\" cy=\"18.5\" r=\"2.5\"></circle>\n"
                        + "<circle cx=\"18.5\" cy=\"18.5\" r=\"2.5\"></circle>\n"
                        + "</svg><span class=\"roleSort\">Saleperson</span>";
            }

            String status;
            if (user.getStatus() == 2) {
                status = "<span class=\"badge rounded-pill badge-light-primary\" text-capitalized=\"\">Inactive</span>\n";
            } else if (user.getStatus() == 1) {
                status = "<span class=\"badge rounded-pill badge-light-success\" text-capitalized=\"\">Active</span>\n";
            } else {
                status = "<span class=\"badge rounded-pill badge-light-secondary\" text-capitalized=\"\">Banned</span>\n";
            }
            out.print("<tr>\n"
                    + "<td class=\" control\" tabindex=\"0\" style=\"display: none;\"></td>\n"
                    + "<td class=\" dt-checkboxes-cell\">\n"
                    + "<div class=\"form-check\"> \n"
                    + "<input class=\"form-check-input dt-checkboxes\" type=\"checkbox\" value=\"\"\n"
                    + "id=\"checkbox" + user.getAccountID() + "\"><label class=\"form-check-label\" for=\"checkbox" + user.getAccountID() + "\"></label></div>\n"
                    + "</td>\n"
                    + "<td class=\"\">\n"
                    + "<div class=\"d-flex justify-content-left align-items-center\">\n"
                    + avatarElement
                    + "<div class=\"d-flex flex-column\">\n"
                    + "<a href=\"#/\" index=\"" + user.getAccountID() + "\" onclick=\"sendParamter(this)\"  data-bs-toggle=\"modal\" data-bs-target=\"#defaultSize\"\n"
                    + "class=\"user_name text-body text-truncate\">\n"
                    + "<span class=\"fw-bolder\">" + user.getFirstName() + " " + user.getLastName() + "</span>\n"
                    + "</a>\n"
                    + "<small class=\"emp_post text-muted\">" + user.getEmail() + "</small>\n"
                    + "</div>\n"
                    + "</div>\n"
                    + "</td>\n"
                    + "\n"
                    + "<td class=\"sorting_1\">\n"
                    + roleUser
                    + "</td>\n"
                    + "<td>" + user.getPhoneNumber() + "</td>\n"
                    + "<td><span class=\"text-nowrap\">" + user.getState() + "</span></td>\n"
                    + "<td>\n"
                    + status
                    + "</td>\n"
                    + "<td>\n"
                    + "<a class=\"btn btn-sm btn-icon\" data-bs-toggle=\"modal\" data-bs-target=\"#defaultSize\" index=\"" + user.getAccountID() + "\" onclick=\"sendParamter(this)\">\n"
                    + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\n"
                    + "stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
                    + "class=\"feather feather-eye font-medium-3 text-body\">\n"
                    + "<path d=\"M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z\"></path>\n"
                    + "<circle cx=\"12\" cy=\"12\" r=\"3\"></circle>\n"
                    + "</svg>\n"
                    + "</a>\n"
                    + "</td>\n"
                    + "</tr>");
        }
    }

    private void selectState(HttpServletRequest request, PrintWriter out) {
        String selectOption = request.getParameter("selectOption");
        if ("".equals(selectOption) || selectOption.length() == 0) {
            out.print("RESET");
            return;
        }
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();

        if (accountListRole != null) {
            for (Account account : accountListRole) {
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountByOption(account.getAccountID(), selectOption,
                            searchQuery + " AND C.STATE = ?", "Select");
                    if (user != null) {
                        userAccountList.add(user);
                    }
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountByOption(account.getAccountID(), selectOption,
                            searchQueryAlt + " AND S.STATE = ?", "Select");
                    if (staff != null) {
                        staffAccountList.add(staff);
                    }
                }
            }
            displayList(out, staffAccountList, userAccountList);
        } else {
            out.print("RESET");
        }
    }
}
