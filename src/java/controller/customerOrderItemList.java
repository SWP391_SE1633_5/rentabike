/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.OrderDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Order;
import model.OrderItem;
import model.Product;

/**
 *
 * @author ASUS
 */
public class customerOrderItemList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            Account account = (Account) session.getAttribute("account");
            OrderDB dao = new OrderDB();
            double totalPrice = 0;
            double hasPaid = 0;
            double taxes = 10;
            double discount = 0;
            String Search = request.getParameter("Search");
            List<OrderItem> oItemList = dao.getAllOrderItemOfCustomer(account.getAccountID());
            if(oItemList.size()!=0){
            List<OrderItem> oProcessingItemList = dao.getAllProcessingOrderItemOfCustomer(account.getAccountID());
            List<OrderItem> oCompleteItemList = dao.getAllCompletedOrderItemOfCustomer(account.getAccountID());
            Order o = dao.getOrderById(""+oItemList.get(0).getOrderID());
            dao.getCustomerInfoForOrder(o);
            for (OrderItem orderItem : oItemList) {
                dao.getStoreForOrderItem(orderItem);
                orderItem.setP(dao.getProductById(orderItem.getProductID()));
                dao.setProductImage(orderItem.getP());
                Order cOrder = dao.getOrderById(""+orderItem.getOrderID());
                if(orderItem.getStatus()!=4)
                discount = discount + (cOrder.getDiscount()) * orderItem.getPrice() /100;
                if(orderItem.getStatus()!=4)
                taxes = taxes + (cOrder.getTaxes()) * (orderItem.getPrice() - (cOrder.getDiscount()) * orderItem.getPrice() /100) /100;;
                totalPrice = orderItem.getPrice() + totalPrice;
                if(orderItem.getStatus()==4)
                    hasPaid = hasPaid + orderItem.getPrice();
            }
            for (OrderItem orderItem : oProcessingItemList) {
                dao.getStoreForOrderItem(orderItem);
                orderItem.setP(dao.getProductById(orderItem.getProductID()));
                dao.setProductImage(orderItem.getP());
            }
            for (OrderItem orderItem : oCompleteItemList) {
                dao.getStoreForOrderItem(orderItem);
                orderItem.setP(dao.getProductById(orderItem.getProductID()));
                dao.setProductImage(orderItem.getP());
            }
            List<OrderItem> listDisplay = new ArrayList<OrderItem>();
            if(Search!=null ){
                for (OrderItem orderItem : oItemList) {
                    if(orderItem.getP().getName().contains(Search))
                        listDisplay.add(orderItem);
                }
                session.setAttribute("orderHistoryList", listDisplay);
            }
            else session.setAttribute("orderHistoryList", oItemList);
            double total = totalPrice + totalPrice*10/100 - hasPaid;
            session.setAttribute("total", String.format("%.2f", total));
            session.setAttribute("hasPaid", hasPaid);
            session.setAttribute("o", o);
            session.setAttribute("totalPrice", totalPrice);
            session.setAttribute("totalTax", totalPrice*10/100);
            session.setAttribute("numOfOrder", oItemList.size());
            session.setAttribute("discount", discount);
            session.setAttribute("taxes", taxes);
            session.setAttribute("oProcessingItemList", oProcessingItemList);
            session.setAttribute("oCompleteItemList", oCompleteItemList);
            }
            response.sendRedirect("orderHistory.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
