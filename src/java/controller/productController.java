/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ProductDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import model.Product;

/**
 *
 * @author ADMIN
 */
public class productController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String sqlQuery = "SELECT P.*, [PI].IMAGE, [PS].DISPLACEMENT, [PS].TRANSMISSION, [PS].STARTER, [PS].FUEL_SYSTEM, [PS].FUEL_CAPACITY, [PS].DRY_WEIGHT, [PS].SEAT_HEIGHT\n"
                + "FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS_SPECIFICATIONS AS [PS]\n"
                + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.PRODUCT_ID = [PS].PRODUCT_ID";
        LinkedList<Product> productList = ProductDB.listProduct(sqlQuery);
        session.setAttribute("productList", productList);

        int countTotalProduct = ProductDB.countProduct("SELECT COUNT(*) FROM PRODUCTS");
        int countRentalProduct = ProductDB.countProduct("SELECT COUNT(*) FROM PRODUCTS WHERE MODE = 'Rental'");
        int countSellProduct = ProductDB.countProduct("SELECT COUNT(*) FROM PRODUCTS WHERE MODE = 'Sell'");
        int countUnavailableProduct = ProductDB.countProduct("SELECT COUNT(*) FROM PRODUCTS WHERE STATUS = 3");
        String selectAmount = request.getParameter("amount");
        if(selectAmount == null) selectAmount = "10";
        ProductDB dao = new ProductDB();
        List<Product> listP = dao.getAllProduct();
        int page, numerpage = Integer.valueOf(selectAmount);
        int size = listP.size();
        int num = (size % numerpage == 0 ? (size / numerpage) : ((size / numerpage)) + 1);
        String xpage = request.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numerpage;
        end = Math.min(page * numerpage, size);

        List<Product> list = dao.getListByPage(listP, start, end);

        session.setAttribute("data", list);
        session.setAttribute("page", page);
        session.setAttribute("num", num);

        session.setAttribute("selectAmount", selectAmount);
        session.setAttribute("productList", list);
        session.setAttribute("totalProduct", countTotalProduct);
        session.setAttribute("rentalProduct", countRentalProduct);
        session.setAttribute("sellProduct", countSellProduct);
        session.setAttribute("unavalilableProduct", countUnavailableProduct);

        response.sendRedirect("app-ecommerce-list.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String filterMode = request.getParameter("mode");
        PrintWriter out = response.getWriter();
        String txtSearch = request.getParameter("txt");
        ProductDB dao = new ProductDB();
        List<Product> list = dao.getAllProduct();
        String Search = request.getParameter("Search");
        String selectBrand = request.getParameter("selectBrand");
        String selectCategory = request.getParameter("selectCategory");
        String selectType = request.getParameter("selectType");
        String selectAmount = request.getParameter("selectAmount");
        List<Product> listDisplay = new ArrayList<Product>();
        for (int i = 0; i < list.size(); i++) {
            listDisplay.add(list.get(i));
        }
        int size = -1;
        boolean check = false;
        while(check==false){
            size = listDisplay.size();
            check = true;
        for (int i = 0; i < size; i++) {
            if(!listDisplay.get(i).getCategory().contains(selectCategory)){
            listDisplay.remove(i);
            check = false;
            break;
            }
        }              
        }
        check = false;
        while(check==false){
            size = listDisplay.size();
            check = true;
        for (int i = 0; i < size; i++) {
            if(!listDisplay.get(i).getBrand().contains(selectBrand)){
            listDisplay.remove(i);
            check = false;
            break;
            }
        }              
        }
        
        check = false;
        while(check==false){
            size = listDisplay.size();
            check = true;
        for (int i = 0; i < size; i++) {
            if(!listDisplay.get(i).getMode().contains(selectType)){
            listDisplay.remove(i);
            check = false;
            break;
            }
        }              
        }
        
        check = false;
        while(check==false){
            size = listDisplay.size();
            check = true;
        for (int i = 0; i < size; i++) {
            if(!listDisplay.get(i).getName().contains(Search)){
            listDisplay.remove(i);
            check = false;
            break;
            }
        }              
        }

            
        
        

        for (Product product : listDisplay) {
            String status = "";
            String update = "";
            String U = "<a aria-controls=\"DataTables_Table_0\" type=\"button\" data-bs-toggle=\"modal\"\n" +
"                                                            data-bs-target=\"#updateProduct\" onclick=\"setID(${product.productID});\" class=\"dropdown-item delete-record\">\n" +
"                                                                        \n" +
"                                                                    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\"\n" +
"                                                                         viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\"\n" +
"                                                                         stroke-linecap=\"round\" stroke-linejoin=\"round\"\n" +
"                                                                         class=\"feather feather-file-text font-small-4 me-50\">\n" +
"                                                                    <path d=\"M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z\"></path>\n" +
"                                                                    <polyline points=\"14 2 14 8 20 8\"></polyline>\n" +
"                                                                    <line x1=\"16\" y1=\"13\" x2=\"8\" y2=\"13\"></line>\n" +
"                                                                    <line x1=\"16\" y1=\"17\" x2=\"8\" y2=\"17\"></line>\n" +
"                                                                    <polyline points=\"10 9 9 9 8 9\"></polyline>\n" +
"                                                                    </svg>Update\n" +
"                                                                </a>";
            if (product.getStatus() == 1) {
                status = "<td><span class=\"badge rounded-pill badge-light-success\" text-capitalized=\"\">Sell</span></td>\n";
                update = " <a href=\"changFromSellToSoldOut?mx="+product.getProductID()+"&status=sold\" href=\"javascript:;\" class=\"dropdown-item delete-record\"><svg\n" +
"                                                                        xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"\n" +
"                                                                        fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"\n" +
"                                                                        stroke-linejoin=\"round\" class=\"feather feather-trash-2 font-small-4 me-50\">\n" +
"                                                                    <polyline points=\"3 6 5 6 21 6\"></polyline>\n" +
"                                                                    <path\n" +
"                                                                        d=\"M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2\">\n" +
"                                                                    </path>\n" +
"                                                                    <line x1=\"10\" y1=\"11\" x2=\"10\" y2=\"17\"></line>\n" +
"                                                                    <line x1=\"14\" y1=\"11\" x2=\"14\" y2=\"17\"></line>\n" +
"                                                                    </svg>Sold\n" +
"                                                                </a>";
            } else if (product.getStatus() == 2) {
                status = "<td><span class=\"badge rounded-pill badge-light-warning\" text-capitalized=\"\">Sold out</span></td>\n";
            update = "<a href=\"changFromSellToSoldOut?mx="+product.getProductID()+"&status=disable\" class=\"dropdown-item delete-record\"><svg\n" +
"                                                                        xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"\n" +
"                                                                        fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"\n" +
"                                                                        stroke-linejoin=\"round\" class=\"feather feather-trash-2 font-small-4 me-50\">\n" +
"                                                                    <polyline points=\"3 6 5 6 21 6\"></polyline>\n" +
"                                                                    <path\n" +
"                                                                        d=\"M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2\">\n" +
"                                                                    </path>\n" +
"                                                                    <line x1=\"10\" y1=\"11\" x2=\"10\" y2=\"17\"></line>\n" +
"                                                                    <line x1=\"14\" y1=\"11\" x2=\"14\" y2=\"17\"></line>\n" +
"                                                                    </svg>Disable\n" +
"                                                                </a> ";
            } else if (product.getStatus() == 3) {
                status = "<td><span class=\"badge rounded-pill badge-light-secondary\" text-capitalized=\"\">Unavailable</span></td>\n";
            update = "<a href=\"changFromSellToSoldOut?mx="+product.getProductID()+"&status=sell\" class=\"dropdown-item delete-record\"><svg\n" +
"                                                                        xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"\n" +
"                                                                        fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"\n" +
"                                                                        stroke-linejoin=\"round\" class=\"feather feather-trash-2 font-small-4 me-50\">\n" +
"                                                                    <polyline points=\"3 6 5 6 21 6\"></polyline>\n" +
"                                                                    <path\n" +
"                                                                        d=\"M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2\">\n" +
"                                                                    </path>\n" +
"                                                                    <line x1=\"10\" y1=\"11\" x2=\"10\" y2=\"17\"></line>\n" +
"                                                                    <line x1=\"14\" y1=\"11\" x2=\"14\" y2=\"17\"></line>\n" +
"                                                                    </svg>Sell\n" +
"                                                                </a>";
            }
            if(product.getMode().equals("Sell")){
                U = "";
            }
            out.println("<tr>\n" +
"                                                    <td class=\"control\" tabindex=\"0\" style=\"display: none;\"></td>\n" +
"                                                    <td class=\"sorting_1\">\n" +
"                                                        <div class=\"d-flex justify-content-left align-items-center\">\n" +
"                                                            <!--<div class=\"avatar-wrapper\">\n" +
"                                                                <div class=\"avatar  me-1\">\n" +
"                                                                    <img src=\"${product.getImage()}\" alt=\"Avatar\"\n" +
"                                                                         height=\"32\" width=\"32\"></div>\n" +
"                                                            </div>-->\n" +
"                                                            <div class=\"d-flex flex-column\">\n" +
"                                                                <a href=\"productDetailController?mx="+product.getProductID()+"\"\n" +
"                                                                   class=\"user_name text-truncate text-body\"><span class=\"fw-bolder\">"+product.getName()+"</span></a>\n" +
"                                                                <small class=\"emp_post text-muted\">Model "+product.getModelYear()+"</small>\n" +
"                                                            </div>\n" +
"                                                        </div>\n" +
"                                                    </td>\n" +
"                                                    <td><span class=\"text-truncate align-middle\">"+product.getCategory()+"</span></td>\n" +
"                                                    <td><span class=\"text-truncate align-middle\">"+product.getPrice()+"</span></td>\n" +
"                                                    <td><span class=\"text-nowrap\">"+product.getBrand()+"</span></td>\n" +
status + 
                    "                                                    <td>\n" +
"                                                        <div class=\"btn-group\">\n" +
"                                                            <a class=\"btn btn-sm dropdown-toggle hide-arrow\"\n" +
"                                                               data-bs-toggle=\"dropdown\">\n" +
"                                                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\"\n" +
"                                                                     viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\"\n" +
"                                                                     stroke-linecap=\"round\" stroke-linejoin=\"round\"\n" +
"                                                                     class=\"feather feather-more-vertical font-small-4\">\n" +
"                                                                <circle cx=\"12\" cy=\"12\" r=\"1\"></circle>\n" +
"                                                                <circle cx=\"12\" cy=\"5\" r=\"1\"></circle>\n" +
"                                                                <circle cx=\"12\" cy=\"19\" r=\"1\"></circle>\n" +
"                                                                </svg></a>\n" +
"                                                            <div  href=\"javascript:;\" class=\"dropdown-menu dropdown-menu-end\">\n" +
"                                                                <a href=\"productDetailController?mx="+product.getProductID()+"\" class=\"dropdown-item delete-record\">\n" +
"                                                                    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\"\n" +
"                                                                         viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\"\n" +
"                                                                         stroke-linecap=\"round\" stroke-linejoin=\"round\"\n" +
"                                                                         class=\"feather feather-file-text font-small-4 me-50\">\n" +
"                                                                    <path d=\"M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z\"></path>\n" +
"                                                                    <polyline points=\"14 2 14 8 20 8\"></polyline>\n" +
"                                                                    <line x1=\"16\" y1=\"13\" x2=\"8\" y2=\"13\"></line>\n" +
"                                                                    <line x1=\"16\" y1=\"17\" x2=\"8\" y2=\"17\"></line>\n" +
"                                                                    <polyline points=\"10 9 9 9 8 9\"></polyline>\n" +
"                                                                    </svg>Details\n" +
"                                                                </a>\n" +
"                                                                \n" +
update +
        "                                                            \n" +
"                                                        \n" +
U + 
                "                                                                    \n" +
"                                                            </div>\n" +
"                                                        </div>\n" +
"                                                    </td>\n" +
"                                                </tr>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
