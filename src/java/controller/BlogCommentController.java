/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import Utils.Utils;
import com.google.gson.Gson;
import dao.BlogCommentDao;
import dao.BlogCommentReactDao;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.Account;
import model.BlogComment;
import model.BlogCommentReact;

/**
 *
 * @author win
 */
@WebServlet(name = "BlogCommentController", urlPatterns = {"/blog-comment"})
public class BlogCommentController extends HttpServlet {

    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();

    //load comments for blog detail
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        BlogCommentDao bcd = new BlogCommentDao();
        
        String type = request.getParameter("type");
        if(type.equals("loadCmt")){
            map.clear();
            int blogId = Integer.parseInt(request.getParameter("id"));
            int after = Integer.parseInt(request.getParameter("after"));

            List<Object> cmtListData = new ArrayList<>();
            List<BlogComment> cmtList = bcd.getNextBlogComment(blogId, after);
            for (BlogComment bc : cmtList) {
                HashMap<Object, Object> mapTmpCmt = new HashMap<>();
                if (UserDB.searchAccountID(bc.getAccountId()) != null) {
                    mapTmpCmt.put("accountCmt", UserDB.searchAccountID(bc.getAccountId()));
                } else if (StaffDB.searchAccountID(bc.getAccountId()) != null) {
                    mapTmpCmt.put("accountCmt", StaffDB.searchAccountID(bc.getAccountId()));
                }
                mapTmpCmt.put("BlogComment", bc);
                mapTmpCmt.put("blogReply", BlogCommentDao.getBlogCommentByCmtId(bc.getId()).size());
                mapTmpCmt.put("blogReact", BlogCommentReactDao.getCommentReactListDataByCommentId(bc.getId()));
                HttpSession session = request.getSession();
                Account a = (Account)session.getAttribute("account");
                if(a!=null){
                    BlogCommentReact bcr = BlogCommentReactDao.getCommentReactByCmtIdAndAccId(bc.getId(), a.getAccountID());
                    mapTmpCmt.put("blogOwnReact", bcr);
                }
                cmtListData.add(mapTmpCmt);
            }
            map.put("CmtData", cmtListData);

            map.put("SC", 1);
            out.print(gson.toJson(map));
            out.flush();
        }
        if(type.equals("loadReply")){
            int cmtId = Integer.parseInt(request.getParameter("cmtId"));
            
            List<Object> cmtListData = new ArrayList<>();
            List<BlogComment> listReply = BlogCommentDao.getBlogCommentByCmtId(cmtId);
            for (BlogComment bc : listReply) {
                HashMap<Object, Object> mapTmpCmt = new HashMap<>();
                if (UserDB.searchAccountID(bc.getAccountId()) != null) {
                    mapTmpCmt.put("accountCmt", UserDB.searchAccountID(bc.getAccountId()));
                } else if (StaffDB.searchAccountID(bc.getAccountId()) != null) {
                    mapTmpCmt.put("accountCmt", StaffDB.searchAccountID(bc.getAccountId()));
                }
                mapTmpCmt.put("BlogComment", bc);
                mapTmpCmt.put("blogReact", BlogCommentReactDao.getCommentReactListDataByCommentId(bc.getId()));
                HttpSession session = request.getSession();
                Account a = (Account)session.getAttribute("account");
                if(a!=null){
                    BlogCommentReact bcr = BlogCommentReactDao.getCommentReactByCmtIdAndAccId(bc.getId(), a.getAccountID());
                    mapTmpCmt.put("blogOwnReact", bcr);
                }
                cmtListData.add(mapTmpCmt);
            }
            map.put("CmtData", cmtListData);

            map.put("SC", 1);
            out.print(gson.toJson(map));
            out.flush();
        }
        if(type.equals("load-show-react")){
            int cmtId = Integer.parseInt(request.getParameter("cmtId"));
            List<Object> showReactList = new ArrayList<>();
            List<BlogCommentReact> list = BlogCommentReactDao.getCommentReactListByCommentId(cmtId);
            for(BlogCommentReact bcr: list){
                HashMap<Object, Object> mapShowBlogReact = new HashMap<>();
                if (UserDB.searchAccountID(bcr.getAccountId()) != null) {
                    mapShowBlogReact.put("accountReact", UserDB.searchAccountID(bcr.getAccountId()));
                } else if (StaffDB.searchAccountID(bcr.getAccountId()) != null) {
                    mapShowBlogReact.put("accountReact", StaffDB.searchAccountID(bcr.getAccountId()));
                }
                mapShowBlogReact.put("blogReactDetail", BlogCommentReactDao.getCommentReactTypeByReactId(bcr.getReactId()));
                showReactList.add(mapShowBlogReact);
            }
            
            map.put("showReact", showReactList);

            map.put("SC", 1);
            out.print(gson.toJson(map));
            out.flush();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        //check authen
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            map.put("SC", 0);
            map.put("ME", "Please login to continue!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        String type = request.getParameter("type");
        //insert
        if(type.equals("insert")){
            int blogId = Integer.parseInt(request.getParameter("blogId"));
            String content = request.getParameter("content");
            String currentTime = Utils.getCurrentDateTime();
            
            BlogComment b = new BlogComment(0, blogId, content, a.getAccountID(), currentTime, currentTime); 
            BlogCommentDao.insertNewCmt(b);
            BlogComment blogCmtInserted = BlogCommentDao.getBlogCommentByBlogInserted(b);
            
            map.put("SC", 1);
            map.put("ME", "Oke!");
            map.put("BlogComent", blogCmtInserted);
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        //update
        if(type.equals("update")){
            int cmtId = Integer.parseInt(request.getParameter("cmtId"));
            String content = request.getParameter("content");
            String updateTime = Utils.getCurrentDateTime();
            
            BlogCommentDao.updateCmt(content, updateTime, cmtId);
            BlogComment blogCmtUpdated = BlogCommentDao.getBlogCommentByBlogCmtId(cmtId);
            map.put("SC", 1);
            map.put("ME", "Oke!");
            map.put("BlogComent", blogCmtUpdated);
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        //reply
        if(type.equals("reply")){
            int blogId = Integer.parseInt(request.getParameter("blogId"));
            String content = request.getParameter("content");
            int replyId = Integer.parseInt(request.getParameter("replyId"));
            String currentTime = Utils.getCurrentDateTime();
            
            BlogComment b = new BlogComment(0, blogId, content, a.getAccountID(), currentTime, currentTime);
            b.setReplyId(replyId);
            BlogCommentDao.insertNewReplyCmt(b);
            BlogComment blogCmtInserted = BlogCommentDao.getBlogCommentByBlogInserted(b);
            map.put("SC", 1);
            map.put("ME", "Oke!");
            map.put("BlogComent", blogCmtInserted);
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        if(type.equals("add-react")){
            int cmtId = Integer.parseInt(request.getParameter("cmtId"));
            int reactId = Integer.parseInt(request.getParameter("reactId"));
            
            BlogCommentReactDao.handleAddNewCmtReact(cmtId, a.getAccountID(), reactId, Utils.getCurrentDateTime());
            BlogCommentReact bcr = BlogCommentReactDao.getCommentReactByCmtIdAndAccId(cmtId, a.getAccountID());
            
            map.put("SC", 1);
            map.put("ME", "Oke!");
            map.put("blogReact", BlogCommentReactDao.getCommentReactListDataByCommentId(cmtId));
            map.put("blogOwnReact", bcr);
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        //delete
        if(type.equals("delete")){
            int cmtId = Integer.parseInt(request.getParameter("cmtId"));
            BlogCommentDao.deleteBlogCommentByCmtId(cmtId);
            
            map.put("SC", 1);
            map.put("ME", "Delete Success!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
