/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.google.gson.Gson;
import dao.BlogAccountDao;
import dao.BlogDao;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author win
 */
@WebServlet(name="BlogAccountController", urlPatterns={"/blog-account"})
public class BlogAccountController extends HttpServlet {
   
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        int accountId = Integer.parseInt(request.getParameter("accountId"));
        String type = request.getParameter("type");
        if(type==null || type.equals("")){
            request.getRequestDispatcher("BlogAccount.html").forward(request, response);
            return;
        }
        
        //account
        if(UserDB.searchAccountID(accountId)!=null){
            map.put("Account", UserDB.searchAccountID(accountId));
        } else if(StaffDB.searchAccountID(accountId)!=null){
            map.put("Account", StaffDB.searchAccountID(accountId));
        }
        //cmt info list
        List<Object> cmtRecentInfoList = BlogAccountDao.getRecentCmtInfoListByAccountId(accountId);
        map.put("CmtRecentInfoList", cmtRecentInfoList);
        map.put("BlogHasPost", BlogDao.getBlogListByAcccountId(accountId));
        map.put("SC", 1);
        out.print(gson.toJson(map));
        out.flush();
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
