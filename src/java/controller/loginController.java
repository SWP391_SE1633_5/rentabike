/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import static controller.verifyAccountController.generateNewToken;
import dao.*;
import model.*;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Random;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "loginController", urlPatterns = "/loginController")
public class loginController extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String rememberMe = request.getParameter("rememberMe");
        String loginWith = request.getParameter("loginWith");
        String staffLogin = request.getParameter("staffLogin");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        Account account = Manager.searchAccount(email);
        StoreDB s = new StoreDB();
        int aStoreID = s.getStoreID(account.getAccountID());
        session.setAttribute("account", account);
        session.setAttribute("aStoreID", aStoreID);
        Clock clock = Clock.systemDefaultZone();
        LocalDateTime currentDate = LocalDateTime.now(clock).withNano(0);
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        String searchAccountBanned = Manager.searchBannedUser(account.getAccountID());

        if (!searchAccountBanned.equalsIgnoreCase("Not Found")) {
            String[] splitDuration = searchAccountBanned.split("LGN");
            String[] getDuration = splitDuration[1].split(" ");
            String[] splitExpired = splitDuration[0].split("\\.");
            LocalDateTime local = LocalDateTime.parse(splitExpired[0], dateFormat);
            LocalDateTime tempDateTime = LocalDateTime.from(local).withNano(0);
            long dayDistance = tempDateTime.until(currentDate, ChronoUnit.DAYS);
            
            if (dayDistance >= Integer.parseInt(getDuration[0])) {
                Manager.deleteNewBannedUser(account.getAccountID());
            } else {
                out.print("BANNED");
                return;
            }
        }

        if (staffLogin != null) {
            String staffPinCode = request.getParameter("pinCode");
            String staffS = StaffDB.searchPinCode(staffPinCode);
            if (session.getAttribute("staff") != null && staffS != null) {
                String split[] = staffS.split("DDA");
                Staff staff = StaffDB.searchAccountID(Integer.parseInt(split[0]));

                String[] splitExpired = split[1].split("\\.");
                LocalDateTime local = LocalDateTime.parse(splitExpired[0], dateFormat);
                LocalDateTime tempDateTime = LocalDateTime.from(local).withNano(0);
                long hourDistance = tempDateTime.until(currentDate, ChronoUnit.HOURS);
                if (hourDistance < 1) {
                    StaffDB.deletePinCode(staff.getAccountID(), staff.getPinCode());
                    session.setAttribute("staff", staff);
                    Manager.updateStatus(1, staff.getAccountID(), "UPDATE STAFFS SET ACTIVE = ? WHERE ACCOUNT_ID = ?");
                    Manager.activyAccountUpdate(staff.getAccountID(), "Status Login", "You login at");
                    out.print("SUCCESS");
                } else {
                    out.print("EXPIRED");
                }
            } else {
                out.print("FALSE");
            }
            return;
        }
        if (loginWith == null) {
            if (account != null && account.getPassword() != null) {
                if (account.getPassword().equals(password)) {
                    String roleUser = account.getRole();
                    if (roleUser.equals("CUSTOMER") || roleUser.equals("SALEPERSON")) {
                        User user = UserDB.searchAccountID(account.getAccountID());
                        session.setAttribute("user", user);
                        if (rememberMe.equalsIgnoreCase("true")) {
                            rememberMeCookie(account, response);
                        }
                        Manager.updateStatus(1, user.getAccountID(), "UPDATE STAFFS SET ACTIVE = ? WHERE ACCOUNT_ID = ?");
                        Manager.activyAccountUpdate(user.getAccountID(), "Status Login", "You login at");
                    } else {
                        Staff staff = StaffDB.searchAccountID(account.getAccountID());
                        StaffDB.clearPinCode(staff.getAccountID());
                        String pinCode = generatePinCode();
                        String fullName = staff.getFirstName() + " " + staff.getLastName();
                        StaffDB.createPinCode(staff.getAccountID(), pinCode);
                        StaffDB.updatePinCode(pinCode, staff.getAccountID());
                        session.setAttribute("staff", staff);

                        String to = staff.getEmail();
                        String subject = "Pin code verify";
                        String message = emailHTML(fullName, pinCode, staff.getEmail(), staff.getAccountID());
                        MailController.send(to, subject, message, "lifeofcoffie@gmail.com", "shgwxqwapkgwzqkk");
                    }
                    out.print(roleUser);
                }
            } else {
                out.print("FAILED");
            }
        } else {
            if (account == null) {
                String firstName = request.getParameter("firstName");
                String lastName = request.getParameter("lastName");
                byte[] avatar = User.convertUrlToByteArray(request.getParameter("avatar"));
                Manager.updateAccount(email, null, "CUSTOMER");
                account = Manager.searchAccount(email);
                UserDB.saveAccount(firstName, lastName, null, email, account.getAccountID(), avatar, null, null, null);
                User user = UserDB.searchAccountID(account.getAccountID());
                session.setAttribute("user", user);
                Manager.updateStatus(1, user.getAccountID(), "UPDATE STAFFS SET ACTIVE = ? WHERE ACCOUNT_ID = ?");
                Manager.activyAccountUpdate(user.getAccountID(), "Status Login", "You login at");
                out.print("FIRST");
            } else {
                User user = UserDB.searchAccountID(account.getAccountID());
                session.setAttribute("user", user);
                Manager.updateStatus(1, user.getAccountID(), "UPDATE STAFFS SET ACTIVE = ? WHERE ACCOUNT_ID = ?");
                Manager.activyAccountUpdate(user.getAccountID(), "Status Login", "You login at");
                out.print("FIRST");
            }
        }
    }

    private void rememberMeCookie(Account account, HttpServletResponse resp) {
        String token = generateNewToken();
        int s = Manager.createKey(token, account.getAccountID());
        Cookie cookieToken = new Cookie("remember_me", token);
        cookieToken.setPath("/");
        cookieToken.setMaxAge(60 * 60 * 24 * 30);
        resp.addCookie(cookieToken);
    }

    public static String generatePinCode() {
        Random random = new Random();
        String pinCode = "";

        do {
            for (int i = 0; i < 6; i++) {
                int numberIndex = random.nextInt(10);
                pinCode += numberIndex + "";
            }
            if (pinCode.length() == 6) {
                break;
            }
        } while (true);
        return pinCode;
    }

    public static String emailHTML(String fullName, String pinCode, String emailSend, int accountID) {
        String email = "    <div style=\"margin: 0; padding: 0; width: 100%; word-break: break-word; -webkit-font-smoothing: antialiased;  background-color: #eceff1; background-color: rgba(236, 239, 241, 1);\">\n"
                + "        <div style=\"display: none;\">A request to receive pin code was received from your Hilfmotor Account</div>\n"
                + "        <div role=\"article\" aria-roledescription=\"email\" aria-label=\"Pincode login\" lang=\"en\">\n"
                + "            <table style=\"font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; width: 100%;\" width=\"100%\"\n"
                + "                cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                <tr>\n"
                + "                    <td align=\"center\"\n"
                + "                        style=\" background-color: #eceff1; background-color: rgba(236, 239, 241, 1); font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif;\"\n"
                + "                        bgcolor=\"rgba(236, 239, 241, 1)\">\n"
                + "                        <table class=\"sm-w-full\" style=\"font-family: 'Montserrat',Arial,sans-serif; width: 600px;\"\n"
                + "                            width=\"600\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                            <tr>\n"
                + "                                <td class=\"sm-py-32 sm-px-24\"\n"
                + "                                    style=\"font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; padding: 48px; text-align: center;\"\n"
                + "                                    align=\"center\">\n"
                + "                                    <a>\n"
                + "                                        <img src=\"https://i.pinimg.com/736x/b3/a6/a1/b3a6a10a79908d5399673e4de0d89b80.jpg\"\n"
                + "                                            style=\"border-radius:50%;\" width=\"155\" alt=\"Hilfmotor Admin\"\n"
                + "                                            style=\"border: 0; max-width: 100%; line-height: 100%; vertical-align: middle;\">\n"
                + "                                    </a>\n"
                + "                                </td>\n"
                + "                            </tr>\n"
                + "                            <tr>\n"
                + "                                <td align=\"center\" class=\"sm-px-24\" style=\"font-family: 'Montserrat',Arial,sans-serif;\">\n"
                + "                                    <table style=\"font-family: 'Montserrat',Arial,sans-serif; width: 100%;\" width=\"100%\"\n"
                + "                                        cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                                        <tr>\n"
                + "                                            <td class=\"sm-px-24\"\n"
                + "                                                style=\" background-color: #ffffff; background-color: rgba(255, 255, 255, 1); border-radius: 4px; font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; font-size: 14px; line-height: 24px; padding: 48px; text-align: left;  color: #626262; color: rgba(98, 98, 98, 1);\"\n"
                + "                                                bgcolor=\"rgba(255, 255, 255, 1)\" align=\"left\">\n"
                + "                                                <p style=\"font-weight: 600; font-size: 18px; margin-bottom: 0;\">Hey</p>\n"
                + "                                                <p\n"
                + "                                                    style=\"font-weight: 700; font-size: 20px; margin-top: 0;  color: #ff5850; color: rgba(255, 88, 80, 1);\">\n"
                + "                                                     " + fullName + "</p>\n"
                + "                                                <p style=\"margin: 0 0 24px;\">\n"
                + "                                                    A request to receive your pin code from\n"
                + "                                                    <span style=\"font-weight: 600;\">Hilfsmotor</span> Account -\n"
                + "                                                    <a href=\"mailto:" + emailSend + "\" class=\"hover-underline\"\n"
                + "                                                        style=\" color: #7367f0; color: rgba(115, 103, 240, 1); text-decoration: none;\">" + emailSend + "</a>\n"
                + "                                                    (ID: " + accountID + ").\n"
                + "                                                </p>\n"
                + "                                                <p style=\"margin: 0 0 24px;\">Use this pin code to login your admin page.\n"
                + "                                                </p>\n"
                + "                                                <table style=\"font-family: 'Montserrat',Arial,sans-serif; margin:auto;\"\n"
                + "                                                    cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                                                    <tr>\n"
                + "                                                        <td style=\"mso-padding-alt: 16px 24px;  background-color: #7367f0; background-color: rgba(115, 103, 240, 1); border-radius: 4px; font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif;\"\n"
                + "                                                            bgcolor=\"rgba(115, 103, 240, 1)\">\n"
                + "                                                            <a href=\"#/\"\n"
                + "                                                                style=\"display: block; font-weight: 600; font-size: 14px; line-height: 100%; padding: 16px 24px;  color: #ffffff; color: rgba(255, 255, 255, 1); text-decoration: none;\">Your\n"
                + "                                                                Pin Code: " + pinCode + " </a>\n"
                + "                                                        </td>\n"
                + "                                                    </tr>\n"
                + "                                                </table>\n"
                + "                                                <p style=\"margin: 24px 0;\">\n"
                + "                                                    <span style=\"font-weight: 600;\">Note:</span> This pin code is valid\n"
                + "                                                    for 1 hour from the time it was\n"
                + "                                                    sent to you and can be used to login to admin page.\n"
                + "                                                </p>\n"
                + "                                                <p style=\"margin: 0;\">\n"
                + "                                                    If you did not intend to deactivate your account or need our help\n"
                + "                                                    keeping the account, please\n"
                + "                                                    contact us at\n"
                + "                                                    <a href=\"mailto:hilfsmotor@gmail.com\" class=\"hover-underline\"\n"
                + "                                                        style=\"color: #7367f0; color: rgba(115, 103, 240, 1); text-decoration: none;\">hilfsmotor@gmail.com</a>\n"
                + "                                                </p>\n"
                + "                                                <table style=\"font-family: 'Montserrat',Arial,sans-serif; width: 100%;\"\n"
                + "                                                    width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                                                    <tr>\n"
                + "                                                        <td\n"
                + "                                                            style=\"font-family: 'Montserrat',Arial,sans-serif; padding-top: 32px; padding-bottom: 32px;\">\n"
                + "                                                            <div\n"
                + "                                                                style=\"background-color: #eceff1; background-color: rgba(236, 239, 241, 1); height: 1px; line-height: 1px;\">\n"
                + "                                                                &zwnj;</div>\n"
                + "                                                        </td>\n"
                + "                                                    </tr>\n"
                + "                                                </table>\n"
                + "                                                <p style=\"margin: 0 0 16px;\">\n"
                + "                                                    Not sure why you received this email? Please\n"
                + "                                                    <a href=\"mailto:hilfsmotor@gmail.com\" class=\"hover-underline\"\n"
                + "                                                        style=\"color: #7367f0; color: rgba(115, 103, 240, 1); text-decoration: none;\">let\n"
                + "                                                        us know</a>.\n"
                + "                                                </p>\n"
                + "                                                <p style=\"margin: 0 0 16px;\">Thanks, <br>The Hilfsmotor Team</p>\n"
                + "                                            </td>\n"
                + "                                        </tr>\n"
                + "                                        <tr>\n"
                + "                                            <td style=\"font-family: 'Montserrat',Arial,sans-serif; height: 20px;\"\n"
                + "                                                height=\"20\"></td>\n"
                + "                                        </tr>\n"
                + "                                        <tr>\n"
                + "                                            <td style=\"font-family: 'Montserrat',Arial,sans-serif; height: 16px;\"\n"
                + "                                                height=\"16\"></td>\n"
                + "                                        </tr>\n"
                + "                                    </table>\n"
                + "                                </td>\n"
                + "                            </tr>\n"
                + "                        </table>\n"
                + "                    </td>\n"
                + "                </tr>\n"
                + "            </table>\n"
                + "        </div>\n"
                + "    </div>";
        return email;
    }
}
