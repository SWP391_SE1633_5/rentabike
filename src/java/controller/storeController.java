/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dao.StoreDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.LinkedList;
import model.Account;
import model.Product;
import model.Store;

/**
 *
 * @author baqua
 */
public class storeController extends HttpServlet {
   
   
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        int storeID = Integer.parseInt(request.getParameter("storeID"));
        HttpSession session = request.getSession();
        session.setAttribute("storeID", storeID);
        
        Account account = (Account) session.getAttribute("account");
        Store store = new Store();
        StoreDB sd = new StoreDB();
        store = StoreDB.getStoreByStoreID(storeID);
        Product productStore = new Product();
        
        request.setAttribute("store", store);
        
        LinkedList<Product> listall = sd.listProduct(storeID);
        request.setAttribute("listall", listall);
        int numProduct = sd.getNumberOfProductOfStore(storeID);
        session.setAttribute("numProduct", numProduct);
        request.getRequestDispatcher("user-store.jsp").forward(request, response);
    } 

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        response.sendRedirect("user-store.jsp");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    
}
