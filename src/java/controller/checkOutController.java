/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.Manager;
import dao.ProductDB;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import model.Account;
import model.Product;
import model.ProductCheckOut;
import model.Staff;
import model.User;

@WebServlet(name = "checkOutController", urlPatterns = "/checkOutController")

public class checkOutController extends HttpServlet {

    private static final String sqlQuery = "SELECT P.*, [PI].IMAGE, [PS].DISPLACEMENT, [PS].TRANSMISSION, [PS].STARTER, [PS].FUEL_SYSTEM, [PS].FUEL_CAPACITY, [PS].DRY_WEIGHT, [PS].SEAT_HEIGHT\n"
            + "FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS_SPECIFICATIONS AS [PS]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.PRODUCT_ID = [PS].PRODUCT_ID AND P.STATUS != 4";
    private static final String stockQuery = "SELECT QUANTITY FROM PRODUCTS_STOCK WHERE PRODUCT_ID = ?";
    private static final String staffQuery = "SELECT DISTINCT SF.[USER_NAME] FROM PRODUCTS AS P, STAFFS AS SF WHERE P.STAFF_ID = ?";
    private static final String storeQuery = "SELECT DISTINCT ST.[STORE_NAME] FROM PRODUCTS AS P, STORES AS ST WHERE P.STORE_ID = ?";
    private static final String rentalQuery = "SELECT PRD.MODE FROM PRODUCTS_RENTAL_DETAILS AS PRD, PRODUCTS AS P WHERE P.MODE = 'Rental' AND ? = PRD.PRODUCT_ID AND P.STATUS != 4";
    private static final String totalRating = "SELECT COUNT(*) AS TOTAL_RATING_COUNT FROM PRODUCTS_RATING WHERE PRODUCT_ID = ?";
    private static final String averageRating = "SELECT AVG(RATING) AS TOTAL_RATING FROM PRODUCTS_RATING WHERE PRODUCT_ID = ?";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        String pagination = request.getParameter("page");
        PrintWriter out = response.getWriter();

        if (pagination == null) {
            pagination = "1";
        }

        if (account != null) {
            switch (pagination) {
                case "1":
                    listCheckOut(session, account);
                    break;
                default:
                    paginationPage(request, out, session, account);
                    break;
            }
            response.sendRedirect("app-product-checkout.jsp");
        } else {
            response.sendRedirect("started.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String mode = request.getParameter("mode");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        switch (mode) {
            case "deleteCheckOut":
                deleteCheckOut(request, out, account);
                break;
            case "viewContract":
                viewContract(request, out, account);
                break;
        }
    }

    private void listCheckOut(HttpSession session, Account account) {
        LinkedList<ProductCheckOut> listCheckOut = ProductDB.listProductCheckOut(account.getAccountID());
        LinkedList<Integer> numberPaginationList = new LinkedList<>();
        LinkedList<Product> listProduct = ProductDB.listProduct(sqlQuery);
        LinkedList<Product> addProductCheckOut = new LinkedList<>();
        String showEntry = (String) session.getAttribute("showEntry");
        int showEntryCurrent = 5;
        int totalPrice = 0, totalEMT = 0;
        if (showEntry != null) {
            showEntryCurrent = Integer.parseInt(showEntry);
        }

        if (listCheckOut != null) {
            for (ProductCheckOut checkout : listCheckOut) {
                for (Product product : listProduct) {
                    if (product.getProductID() == checkout.getProductID()) {
                        totalEMT += (int) product.getPrice();
                        String staffName = ProductDB.searchPersonProduct(product.getStaffID(), staffQuery);
                        String storeName = ProductDB.searchPersonProduct(product.getStoreID(), storeQuery);
                        if (staffName != null) {
                            product.setStaffName(staffName);

                        } else {
                            product.setStoreName(storeName);
                        }
                        if (product.getMode().equalsIgnoreCase("Rental")) {
                            String priceMode = ProductDB.searchPersonProduct(product.getProductID(), rentalQuery);
                            product.setModeRental(priceMode);
                        }
                        product.setTotalRating(ProductDB.searchPersonProduct(product.getProductID(), totalRating));
                        product.setAverageRating(ProductDB.searchPersonProduct(product.getProductID(), averageRating));

                        int stockRemaining = ProductDB.countInformations(stockQuery, product.getProductID());
                        product.setProductStock(stockRemaining);
                        addProductCheckOut.add(product);
                        checkout.setTotalMRP((int) product.getPrice());
                        checkout.setTotalPrice((int) product.getPrice() - checkout.getBagDiscount());
                    }
                }
            }
            double showTotalPage = (double) (showEntryCurrent - 1) + 1.0;
            int numberPaginaton = (int) Math.ceil(listCheckOut.size() / showTotalPage);
            for (int i = 0; i < numberPaginaton; i++) {
                numberPaginationList.add(i + 1);
            }
            totalPrice = (totalEMT - 25) <= 0 ? 0 : (totalEMT - 25);
            session.setAttribute("totalPrice", totalPrice);
            session.setAttribute("totalEMT", totalEMT);
            session.setAttribute("showEntry", String.valueOf(showEntryCurrent));
            session.setAttribute("numberPaginationList", numberPaginationList);
            session.setAttribute("currentPage", 1);
            session.setAttribute("listCheckOut", listCheckOut);
            session.setAttribute("addProductCheckOut", addProductCheckOut);
            session.setAttribute("startData", 0);
            session.setAttribute("endData", (showEntryCurrent - 1));
        }
    }

    private void paginationPage(HttpServletRequest request, PrintWriter out, HttpSession session, Account account) {
        LinkedList<ProductCheckOut> listCheckOut = ProductDB.listProductCheckOut(account.getAccountID());
        LinkedList<Product> listProduct = ProductDB.listProduct(sqlQuery);
        LinkedList<Product> addProductCheckOut = new LinkedList<>();
        String showEntry = request.getParameter("showEntry");
        String showEntryCurrent = (String) session.getAttribute("showEntry");
        int pagination = Integer.parseInt(request.getParameter("page"));
        int nextPage;
        int totalPrice = 0, totalEMT = 0;
        if ((showEntry == null || "".equals(showEntry) || showEntry.length() == 0) && showEntryCurrent == null) {
            nextPage = 5;
        } else {
            if (showEntry != null) {
                nextPage = Integer.parseInt(showEntry);
            } else {
                nextPage = Integer.parseInt(showEntryCurrent);
            }
        }

        if (showEntryCurrent != null) {
            if (showEntry == null) {
                showEntry = showEntryCurrent;
            }
        }

        int totalPage = pagination * nextPage;
        int indexPage = totalPage - nextPage;

        if (listCheckOut != null) {
            for (ProductCheckOut checkout : listCheckOut) {
                for (Product product : listProduct) {
                    if (product.getProductID() == checkout.getProductID()) {
                        totalEMT += (int) product.getPrice();
                        String staffName = ProductDB.searchPersonProduct(product.getStaffID(), staffQuery);
                        String storeName = ProductDB.searchPersonProduct(product.getStoreID(), storeQuery);
                        if (staffName != null) {
                            product.setStaffName(staffName);

                        } else {
                            product.setStoreName(storeName);
                        }
                        if (product.getMode().equalsIgnoreCase("Rental")) {
                            String priceMode = ProductDB.searchPersonProduct(product.getProductID(), rentalQuery);
                            product.setModeRental(priceMode);
                        }
                        product.setTotalRating(ProductDB.searchPersonProduct(product.getProductID(), totalRating));
                        product.setAverageRating(ProductDB.searchPersonProduct(product.getProductID(), averageRating));

                        int stockRemaining = ProductDB.countInformations(stockQuery, product.getProductID());
                        product.setProductStock(stockRemaining);
                        addProductCheckOut.add(product);
                        checkout.setTotalMRP((int) product.getPrice());
                        checkout.setTotalPrice((int) product.getPrice() - checkout.getBagDiscount());
                    }
                }
            }
            totalPrice = (totalEMT - 25) <= 0 ? 0 : (totalEMT - 25);
            session.setAttribute("totalPrice", totalPrice);
            session.setAttribute("totalEMT", totalEMT);
            session.setAttribute("showEntry", showEntry);
            session.setAttribute("currentPage", pagination);
            session.setAttribute("listCheckOut", listCheckOut);
            session.setAttribute("addProductCheckOut", addProductCheckOut);
            session.setAttribute("startData", indexPage);
            session.setAttribute("endData", totalPage - 1);
        }
    }

    private void deleteCheckOut(HttpServletRequest request, PrintWriter out, Account account) {
        String productIDS = request.getParameter("idProduct");
        if (productIDS != null || !"".equals(productIDS)) {
            int productID = Integer.parseInt(productIDS);
            int status = ProductDB.removeCheckOut(account.getAccountID(), productID);

            if (status > 0) {
                out.print("SUCCESS");
            } else {
                out.print("FAILED");
            }
        } else {
            out.print("FAILED");
        }
    }

    private void viewContract(HttpServletRequest request, PrintWriter out, Account account) {
        String productIDS = request.getParameter("productID");
        String outPrint;
        Staff staff = null;
        User user = null;
        boolean checked = false;
        User currentUser = UserDB.searchAccountID(account.getAccountID());
        if (productIDS != null || !"".equals(productIDS)) {
            int productID = Integer.parseInt(productIDS);
            int storeID = Integer.parseInt(request.getParameter("storeID"));
            Product product = ProductDB.searchProduct(sqlQuery + " AND P.PRODUCT_ID = ?", productID);

            if (product != null) {
                String staffName = ProductDB.searchPersonProduct(storeID, staffQuery);
                if (staffName != null) {
                    staff = StaffDB.searchAccountID(storeID);
                    checked = true;
                } else {
                    user = UserDB.searchAccountID(storeID);
                }
                if (checked) {
                    outPrint = staff.getFirstName() + " " + staff.getLastName() + "LGN"
                            + staff.getCity() + "LGN" + staff.getStreet() + "LGN"
                            + staff.getState() + "LGN" + staff.getPhoneNumber();
                } else {
                    outPrint = user.getFirstName() + " " + user.getLastName() + "LGN"
                            + user.getCity() + "LGN" + user.getStreet() + "LGN"
                            + user.getState() + "LGN" + user.getPhoneNumber();
                }
                outPrint += "LGN" + currentUser.getFirstName() + " " + currentUser.getLastName() + "LGN"
                        + currentUser.getCity() + "LGN" + currentUser.getStreet() + "LGN"
                        + currentUser.getState() + "LGN" + currentUser.getPhoneNumber();
                outPrint += "LGN" + product.getCategory() + "LGN" + product.getDisplacement()
                        + "LGN" + product.getTransmission() + "LGN" + product.getStarter()
                        + "LGN" + product.getFuelSystem() + "LGN" + product.getFuelCapacity()
                        + "LGN" + product.getDryWeight() + "LGN" + product.getSeatHeight()
                        + "LGN" + product.getPrice() + "LGN" + product.getName();
                out.print(outPrint);
            }
        }
    }
}
