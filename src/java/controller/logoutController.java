/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.Manager;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Staff;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "logoutController", urlPatterns = {"/logoutController"})
public class logoutController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        rememberMeController tokenRemember = new rememberMeController();
        tokenRemember.deleteRememberMeCookie(request, response);
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Staff staff = (Staff) session.getAttribute("staff");
        if (session.getAttribute("user") != null) {
            session.removeAttribute("user");
            Manager.updateStatus(2, user.getAccountID(), "UPDATE CUSTOMERS SET [STATUS] = ? WHERE ACCOUNT_ID = ?");
            Manager.activyAccountUpdate(user.getAccountID(), "Status Logout", "You logout at");
            response.sendRedirect("homepageCController");
        }
        if (session.getAttribute("staff") != null) {
            session.removeAttribute("staff");
            Manager.updateStatus(0, staff.getAccountID(), "UPDATE STAFFS SET ACTIVE = ? WHERE ACCOUNT_ID = ?");
            Manager.activyAccountUpdate(staff.getAccountID(), "Status Logout", "You log out at");
            response.sendRedirect("started.jsp");
        }
    }
}
