/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.StoreDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Account;
import model.Store;

/**
 *
 * @author ASUS
 */
public class searchProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String txtSearch = request.getParameter("txtSearch");
            Account account = (Account) session.getAttribute("account");
        Store store = new Store();
        StoreDB sd = new StoreDB();
        store = StoreDB.searchStore();
        String status = "";
        String one = "";
        request.setAttribute("store", store);
        ArrayList<Store> listall = sd.getAllStore();
        request.setAttribute("listall", listall);
        ArrayList<Store> listNew = new ArrayList();
            for (Store store1 : listall) {
                if(store1.getFirstName().toLowerCase().contains(txtSearch.toLowerCase())||store1.getLastName().toLowerCase().contains(txtSearch.toLowerCase())||store1.getStoreName().toLowerCase().contains(txtSearch.toLowerCase()))
                    listNew.add(store1);
            }
            for (Store store1 : listNew) {
                if(store1.getStatus()==1){
                    status = "<td><span class=\"badge rounded-pill badge-light-success\" text-capitalized=\"\">Available</span></td>";
                    one ="<a href=\"changeStoreStatus?mx=${item.storeID}&status=disable\" class=\"dropdown-item delete-record\"><svg\n" +
"                                                                                xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"\n" +
"                                                                                fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"\n" +
"                                                                                stroke-linejoin=\"round\" class=\"feather feather-trash-2 font-small-4 me-50\">\n" +
"                                                                            <polyline points=\"3 6 5 6 21 6\"></polyline>\n" +
"                                                                            <path\n" +
"                                                                                d=\"M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2\">\n" +
"                                                                            </path>\n" +
"                                                                            <line x1=\"10\" y1=\"11\" x2=\"10\" y2=\"17\"></line>\n" +
"                                                                            <line x1=\"14\" y1=\"11\" x2=\"14\" y2=\"17\"></line>\n" +
"                                                                            </svg>Unavailable\n" +
"                                                                        </a> ";
                } else
                if(store1.getStatus()==2){
                    status = "<td><span class=\"badge rounded-pill badge-light-warning\" text-capitalized=\"\">Band</span>\n" +
"                                                            </td> ";
                    one ="<a href=\"changeStoreStatus?mx=${item.storeID}&status=available\" class=\"dropdown-item delete-record\"><svg\n" +
"                                                                                xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"\n" +
"                                                                                fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"\n" +
"                                                                                stroke-linejoin=\"round\" class=\"feather feather-trash-2 font-small-4 me-50\">\n" +
"                                                                            <polyline points=\"3 6 5 6 21 6\"></polyline>\n" +
"                                                                            <path\n" +
"                                                                                d=\"M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2\">\n" +
"                                                                            </path>\n" +
"                                                                            <line x1=\"10\" y1=\"11\" x2=\"10\" y2=\"17\"></line>\n" +
"                                                                            <line x1=\"14\" y1=\"11\" x2=\"14\" y2=\"17\"></line>\n" +
"                                                                            </svg>Available\n" +
"                                                                        </a>";
                } else
                {
                    status = "<td><span class=\"badge rounded-pill badge-light-secondary\" text-capitalized=\"\">Unavailable</span>\n" +
"                                                            </td>";
                    one ="<a href=\"changeStoreStatus?mx=${item.storeID}&status=band\" href=\"javascript:;\" class=\"dropdown-item delete-record\"><svg\n" +
"                                                                                xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"\n" +
"                                                                                fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"\n" +
"                                                                                stroke-linejoin=\"round\" class=\"feather feather-trash-2 font-small-4 me-50\">\n" +
"                                                                            <polyline points=\"3 6 5 6 21 6\"></polyline>\n" +
"                                                                            <path\n" +
"                                                                                d=\"M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2\">\n" +
"                                                                            </path>\n" +
"                                                                            <line x1=\"10\" y1=\"11\" x2=\"10\" y2=\"17\"></line>\n" +
"                                                                            <line x1=\"14\" y1=\"11\" x2=\"14\" y2=\"17\"></line>\n" +
"                                                                            </svg>Band\n" +
"                                                                        </a>";
                }
                String firstName = store1.getFirstName();
                String first = firstName.substring(0, 1);
                String lastName = store1.getLastName();
                String last = lastName.substring(0, 1);
                out.println("<tr class=\"odd\">\n" +
"                                                    <td class=\" control\" tabindex=\"0\" style=\"display: none;\"></td>\n" +
"\n" +
"                                                    <td class=\"\">\n" +
"                                                        <div class=\"d-flex justify-content-left align-items-center\">\n" +
"                                                            <div class=\"avatar-wrapper\">\n" +
"                                                                <div class=\"avatar bg-light-danger me-50\" style=\"margin-right:0 !important\">\n" +
"                                                                    <div class=\"avatar-content\">"+ first +""+ last +"</div>\n" +
"                                                                </div>\n" +
"                                                            </div>\n" +
"                                                            &nbsp <div class=\"d-flex flex-column\"><a href=\"user-store?storeID="+ store1.getStoreID() +"\"\n" +
"                                                                                                     class=\"user_name text-body text-truncate\"><span class=\"fw-bolder\">"+ store1.getStoreName()+"</span></a><small class=\"emp_post text-muted\">"+ store1.getEmail()+"</small></div>\n" +
"                                                        </div>\n" +
"                                                    </td>\n" +
"                                                    <td class=\"sorting_1\"><span class=\"text-truncate align-middle\"><svg\n" +
"                                                                xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\n" +
"                                                                stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n" +
"                                                                class=\"feather feather-user font-medium-3 text-primary me-50\">\n" +
"                                                            <path d=\"M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2\"></path>\n" +
"                                                            <circle cx=\"12\" cy=\"7\" r=\"4\"></circle>\n" +
"                                                            </svg>"+ store1.getLastName()+" "+ store1.getFirstName()+"</span></td>\n" +
"                                                    <td>"+ store1.getPhoneNumber()+"</td>\n" +
"                                                    <td><span class=\"text-nowrap\">"+ store1.getState()+"</span></td>\n" +
"\n" +
"                                                    <td>"+ store1.getCity()+"</td>\n" +
"                                                    <td>"+ store1.getStoreID()+"</td>\n" + status+
"                                                    <td>\n" +
"                                                        <div class=\"btn-group\">\n" +
"                                                            <a class=\"btn btn-sm dropdown-toggle hide-arrow\"\n" +
"                                                               data-bs-toggle=\"dropdown\">\n" +
"                                                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\"\n" +
"                                                                     viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\"\n" +
"                                                                     stroke-linecap=\"round\" stroke-linejoin=\"round\"\n" +
"                                                                     class=\"feather feather-more-vertical font-small-4\">\n" +
"                                                                <circle cx=\"12\" cy=\"12\" r=\"1\"></circle>\n" +
"                                                                <circle cx=\"12\" cy=\"5\" r=\"1\"></circle>\n" +
"                                                                <circle cx=\"12\" cy=\"19\" r=\"1\"></circle>\n" +
"                                                                </svg></a>\n" +
"                                                            <div  href=\"javascript:;\" class=\"dropdown-menu dropdown-menu-end\">\n" +
"                                                                \n" +
"\n" + one + one +
"                                                                \n" +
"\n" +
"                                                            </div>\n" +
"                                                        </div>\n" +
"                                                    </td>\n" +
"                                                </tr>");
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
