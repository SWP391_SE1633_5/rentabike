/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.Manager;
import dao.ProductDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import model.Account;
import model.User;
import model.UserActivy;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "userProfileController", urlPatterns = "/userProfileController")
public class userProfileController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        if (account != null) {
            LinkedList<UserActivy> listActivy = Manager.getActivyAccount(account.getAccountID());
            for (UserActivy activy : listActivy) {
                Clock clock = Clock.systemDefaultZone();
                LocalDateTime currentDate = LocalDateTime.now(clock).withNano(0);
                DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                String[] timeSplit = activy.getTimeActivy().split("\\.");
                LocalDateTime local = LocalDateTime.parse(timeSplit[0], dateFormat);
                String hour = String.valueOf(local.getHour());
                String minute = String.valueOf(local.getMinute());
                String second = String.valueOf(local.getSecond());
                if (Integer.parseInt(hour) < 10) {
                    hour = "0" + hour;
                }
                if (Integer.parseInt(minute) < 10) {
                    minute = "0" + minute;
                }
                if (Integer.parseInt(second) < 10) {
                    second = "0" + second;
                }
                activy.setTimeActivy(hour + ":" + minute + ":" + second);

                LocalDateTime tempDateTime = LocalDateTime.from(local).withNano(0);
                long secondDistance = tempDateTime.until(currentDate, ChronoUnit.SECONDS);
                long minuteDistance = tempDateTime.until(currentDate, ChronoUnit.MINUTES);
                long hourDistance = tempDateTime.until(currentDate, ChronoUnit.HOURS);
                long dayDistance = tempDateTime.until(currentDate, ChronoUnit.DAYS);
                long monthDistance = tempDateTime.until(currentDate, ChronoUnit.MONTHS);
                long yearDistance = tempDateTime.until(currentDate, ChronoUnit.YEARS);

                if (yearDistance > 0) {
                    activy.setTimeDistance(String.valueOf(yearDistance) + " year ago");
                }
                if (monthDistance <= 12 && monthDistance > 0) {
                    activy.setTimeDistance(String.valueOf(monthDistance) + " month ago");
                }
                if (dayDistance <= 30 && dayDistance > 0) {
                    activy.setTimeDistance(String.valueOf(dayDistance) + " day ago");
                }
                if (hourDistance <= 60 && hourDistance > 0) {
                    activy.setTimeDistance(String.valueOf(hourDistance) + " hour ago");
                }
                if (minuteDistance <= 60 && minuteDistance > 0) {
                    activy.setTimeDistance(String.valueOf(minuteDistance) + " minute ago");
                }
                if (secondDistance <= 60) {
                    activy.setTimeDistance(String.valueOf(second) + " second ago");
                }
            }
            session.setAttribute("listActivy", listActivy);
            response.sendRedirect("app-user-view-account.jsp");
        } else {
            response.sendRedirect("homepageC.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String mode = request.getParameter("mode");
        switch (mode) {
            case "editProfileuser":
                editProfileUser(request, out);
                break;
            default:
                throw new AssertionError();
        }
    }

    private void editProfileUser(HttpServletRequest request, PrintWriter out) {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String emailAddress = request.getParameter("emailAddress");
        String city = request.getParameter("city");
        String street = request.getParameter("street");
        String country = request.getParameter("country");
        String contact = request.getParameter("contact");
        String role = request.getParameter("role");
        String image = request.getParameter("image");

        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        int status = 0;

        if (account != null) {
            User user = new User();
            user.setAccountID(account.getAccountID());
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(emailAddress);
            user.setCity(city);
            user.setStreet(street);
            user.setState(country);
            user.setPhoneNumber(contact);
            user.setRole(role);
            if ("".equals(image) || image.length() == 0) {
                status = UserDB.updateUser(user, null);
            } else {
                status = UserDB.updateUser(user, getBytesFromString(image));
            }
            if (status > 0) {
                User userSet = UserDB.searchAccountID(account.getAccountID());
                out.print(userSet.getAvatar());
                session.setAttribute("user", userSet);
            } else {
                out.print("ERROR");
            }
        } else {
            out.print("FAILED");
        }
    }

    private byte[] getBytesFromString(String avatar) {
        byte[] byteArray = new byte[avatar.length()];
        String[] splitAvatar = avatar.split(",");

        for (int i = 0; i < splitAvatar.length; i++) {
            byteArray[i] = (byte) Integer.parseInt(splitAvatar[i]);
        }
        return byteArray;
    }

}
