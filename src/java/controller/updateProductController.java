/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ProductDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import model.Product;

/**
 *
 * @author baqua
 */
public class updateProductController extends HttpServlet {

    private static final String sqlQuery = "SELECT P.*, [PI].IMAGE, [PS].DISPLACEMENT, [PS].TRANSMISSION, [PS].STARTER, [PS].FUEL_SYSTEM, [PS].FUEL_CAPACITY, [PS].DRY_WEIGHT, [PS].SEAT_HEIGHT\n"
            + "FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS_SPECIFICATIONS AS [PS]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.PRODUCT_ID = [PS].PRODUCT_ID AND P.PRODUCT_ID = ?";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        Product product1 = (Product) session.getAttribute("product");
        int productID = product1.getProductID();
        String avatarImage = request.getParameter("avatarImage");
        String name = request.getParameter("productName");
        float price = Float.parseFloat(request.getParameter("price"));
        String brand = request.getParameter("brand");
        String category = request.getParameter("category");
        String des = request.getParameter("description");

        Product p = new Product();

        p.setName(name);
        p.setPrice(price);
        p.setBrand(brand);
        p.setCategory(category);
        p.setDescription(des);
        p.setProductID(productID);

        int status = 0;
        int status1 = 0;

        status = ProductDB.updateProduct(p);
        if ("".equals(avatarImage) || avatarImage.length() == 0) {
            status1 = ProductDB.updateProductImgae(p, null);
        } else {
            status1 = ProductDB.updateProductImgae(p, getBytesFromString(avatarImage));
        }

        Product product = ProductDB.searchProduct(sqlQuery, productID);
        request.setAttribute("product", product);
        request.getRequestDispatcher("app-product-details.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    private byte[] getBytesFromString(String avatar) {
        byte[] byteArray = new byte[avatar.length()];
        String[] splitAvatar = avatar.split(",");

        for (int i = 0; i < splitAvatar.length; i++) {
            byteArray[i] = (byte) Integer.parseInt(splitAvatar[i]);
        }
        return byteArray;
    }
}
