/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ProductDB;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import model.Account;
import model.Product;
import model.ProductReview;
import model.Staff;
import model.User;

public class productDetailController extends HttpServlet {

    private static final String sqlQuery = "SELECT P.*, [PI].IMAGE, [PS].DISPLACEMENT, [PS].TRANSMISSION, [PS].STARTER, [PS].FUEL_SYSTEM, [PS].FUEL_CAPACITY, [PS].DRY_WEIGHT, [PS].SEAT_HEIGHT\n"
            + "FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS_SPECIFICATIONS AS [PS]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.PRODUCT_ID = [PS].PRODUCT_ID AND P.PRODUCT_ID = ?";
    private static final String staffQuery = "SELECT DISTINCT SF.[USER_NAME] FROM PRODUCTS AS P, STAFFS AS SF WHERE P.STAFF_ID = ?";
    private static final String storeQuery = "SELECT DISTINCT ST.[STORE_NAME] FROM PRODUCTS AS P, STORES AS ST WHERE P.STORE_ID = ?";
    private static final String rentalQuery = "SELECT PRD.MODE FROM PRODUCTS_RENTAL_DETAILS AS PRD, PRODUCTS AS P WHERE P.MODE = 'Rental' AND ? = PRD.PRODUCT_ID";
    private static final String totalRating = "SELECT COUNT(*) AS TOTAL_RATING_COUNT FROM PRODUCTS_RATING WHERE PRODUCT_ID = ?";
    private static final String averageRating = "SELECT AVG(RATING) AS TOTAL_RATING FROM PRODUCTS_RATING WHERE PRODUCT_ID = ?";
    private static final String stockQuery = "SELECT QUANTITY FROM PRODUCTS_STOCK WHERE PRODUCT_ID = ?";
    private static final String ratingCount = "SELECT COUNT(RATING) FROM PRODUCTS_RATING";
    private static final String countProduct = "SELECT COUNT(PRODUCT_ID) FROM PRODUCTS";
    private static final String relatedProduct = "SELECT P.*, [PI].IMAGE FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.CATEGORY_NAME = ? AND P.BRAND_NAME = ? AND P.MODE = ?";
    private static final String rentalDetails = "SELECT * FROM PRODUCTS_RENTAL_DETAILS WHERE PRODUCT_ID = ?";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int productId = Integer.parseInt(request.getParameter("mx"));
        Product product = ProductDB.searchProduct(sqlQuery, productId);
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        if (product != null && account != null) {
            product = getProductData(product, account);
            if (product.getMode().equalsIgnoreCase("Rental")) {
                product = ProductDB.getRentalDetail(rentalDetails, product);
            }
            getInformations(session, product, account);
            getReviews(session, product, account);
            session.setAttribute("product", product);
            response.sendRedirect("app-product-details.jsp");
        } else {
            response.sendRedirect("homepageCController");
        }
    }

    private void getInformations(HttpSession session, Product product, Account account) {
        LinkedList<Product> relatedProductStoreList;
        int totalProductStore, reviewsStore, rentalTotal, sellTotal;
        int stockRemaining = ProductDB.countInformations(stockQuery, product.getProductID());
        if (product.getStaffName() != null) {
            reviewsStore = ProductDB.countInformations(ratingCount + " WHERE STAFF_ID = ?", product.getStaffID());
            totalProductStore = ProductDB.countInformations(countProduct + " WHERE STAFF_ID = ?", product.getStaffID());
            rentalTotal = ProductDB.countInformations(countProduct + " WHERE STAFF_ID = ? AND MODE = 'Rental'", product.getStaffID());
            sellTotal = ProductDB.countInformations(countProduct + " WHERE STAFF_ID = ? AND MODE = 'Sell'", product.getStaffID());
            relatedProductStoreList = ProductDB.relatedProduct(relatedProduct + " AND STAFF_ID ="
                    + product.getStaffID(), product.getCategory(), product.getBrand(), product.getMode());
            Staff staff = StaffDB.searchAccountID(product.getStaffID());
            session.setAttribute("staffStore", staff);
        } else {
            reviewsStore = ProductDB.countInformations(ratingCount + " WHERE STORE_ID = ?", product.getStoreID());
            totalProductStore = ProductDB.countInformations(countProduct + " WHERE STORE_ID = ?", product.getStoreID());
            rentalTotal = ProductDB.countInformations(countProduct + " WHERE STORE_ID = ? AND MODE = 'Rental'", product.getStoreID());
            sellTotal = ProductDB.countInformations(countProduct + " WHERE STORE_ID = ? AND MODE = 'Sell'", product.getStoreID());
            relatedProductStoreList = ProductDB.relatedProduct(relatedProduct + " AND STORE_ID ="
                    + product.getStoreID(), product.getCategory(), product.getBrand(), product.getMode());
            int accountID = UserDB.searchStoreID(product.getStoreID());
            User user = UserDB.searchAccountID(accountID);
            session.setAttribute("userStore", user);
        }
        LinkedList<Product> relatedProductList = ProductDB.relatedProduct(relatedProduct, product.getCategory(), product.getBrand(), product.getMode());
        for (int i = 0; i < relatedProductList.size(); i++) {
            relatedProductList.set(i, getProductData(relatedProductList.get(i), account));
        }
        for (int i = 0; i < relatedProductStoreList.size(); i++) {
            relatedProductStoreList.set(i, getProductData(relatedProductStoreList.get(i), account));
        }
        session.setAttribute("rentalTotal", rentalTotal);
        session.setAttribute("sellTotal", sellTotal);
        session.setAttribute("reviewsStore", reviewsStore);
        session.setAttribute("stockTotal", stockRemaining);
        session.setAttribute("totalProductStore", totalProductStore);
        session.setAttribute("relatedProductList", relatedProductList);
        session.setAttribute("relatedProductStoreList", relatedProductStoreList);
    }

    private void getReviews(HttpSession session, Product product, Account account) {
        LinkedList<ProductReview> listProductReview = ProductDB.productReviewsList(product.getProductID());
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (listProductReview != null) {
            for (ProductReview productReview : listProductReview) {
                String[] splitTime = productReview.getCommentDate().split("-");
                String day = splitTime[2];
                String month = splitTime[1];
                String year = splitTime[0];
                User user = UserDB.searchAccountID(productReview.getAccountID());
                Staff staff = StaffDB.searchAccountID(productReview.getAccountID());

                if (user != null) {
                    productReview.setAccountName(user.getFirstName() + " " + user.getLastName());
                    productReview.setAccountRole(user.getRole());
                    productReview.setImageAccount(user.getAvatar());
                } else {
                    productReview.setAccountName(staff.getFirstName() + " " + staff.getLastName());
                    productReview.setAccountRole(staff.getRole());
                    productReview.setImageAccount(staff.getAvatar());
                }
                productReview.setCommentDate(day + "/" + month + "/" + year);
            }
            session.setAttribute("listProductReview", listProductReview);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    private static Product getProductData(Product product, Account account) {
        String staffName = ProductDB.searchPersonProduct(product.getStaffID(), staffQuery);
        String storeName = ProductDB.searchPersonProduct(product.getStoreID(), storeQuery);
        if (staffName != null) {
            product.setStaffName(staffName);
        } else {
            product.setStoreName(storeName);
        }
        if (product.getMode().equalsIgnoreCase("Rental")) {
            String priceMode = ProductDB.searchPersonProduct(product.getProductID(), rentalQuery);
            product.setModeRental(priceMode);
        }
        product.setTotalRating(ProductDB.searchPersonProduct(product.getProductID(), totalRating));
        product.setAverageRating(ProductDB.searchPersonProduct(product.getProductID(), averageRating));
        product.setMarked(ProductDB.searchWishList(account.getAccountID(), product.getProductID()));
        return product;
    }
}
