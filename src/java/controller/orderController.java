/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.OrderDB;
import dao.ProductDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;
import model.Product;

/**
 *
 * @author ADMIN
 */
public class orderController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        int countTotalOrder = ProductDB.countProduct("SELECT COUNT(*) FROM ORDERS");
        int countProcessingOrder = ProductDB.countProduct("SELECT COUNT(*) FROM ORDERS where ORDERS.ORDER_STATUS = 1");
        int countTroubleOrder = ProductDB.countProduct("SELECT COUNT(*) FROM ORDERS where ORDERS.ORDER_STATUS = 2");
        int countCompletedOrder = ProductDB.countProduct("SELECT COUNT(*) FROM ORDERS where ORDERS.ORDER_STATUS = 3");
        String selectAmount = request.getParameter("amount");
        if (selectAmount == null) {
            selectAmount = "10";
        }
        OrderDB dao = new OrderDB();
        List<Order> listO = dao.getAllOrder();
        for (Order order : listO) {
            dao.getCustomerInfoForOrder(order);
        }

        int page, numerpage = Integer.valueOf(selectAmount);
        int size = listO.size();
        int num = (size % numerpage == 0 ? (size / numerpage) : ((size / numerpage)) + 1);
        String xpage = request.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numerpage;
        end = Math.min(page * numerpage, size);

        List<Order> list = dao.getListByPage(listO, start, end);

        session.setAttribute("data", list);
        session.setAttribute("page", page);
        session.setAttribute("num", num);

        session.setAttribute("selectAmount", selectAmount);
        session.setAttribute("orderList", list);
        session.setAttribute("totalOrder", countTotalOrder);
        session.setAttribute("processingOrder", countProcessingOrder);
        session.setAttribute("troubleOrder", countTroubleOrder);
        session.setAttribute("completedOrder", countCompletedOrder);

        response.sendRedirect("app-order-list.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        OrderDB dao = new OrderDB();
        List<Order> list = dao.getAllOrder();
        String Search = request.getParameter("Search");
        String selectOStatus = request.getParameter("selectOStatus");
        String selectPStatus = request.getParameter("selectPStatus");
        String selectOType = request.getParameter("selectOType");
        String selectAmount = request.getParameter("selectAmount");
        List<Order> listDisplay = new ArrayList<Order>();
        int oStatus = -1;
        int pStatus = -1;
        if (!selectOStatus.isEmpty()) {
            oStatus = Integer.parseInt(selectOStatus);
        }
        if (!selectPStatus.isEmpty()) {
            pStatus = Integer.parseInt(selectPStatus);
        }
        for (int i = 0; i < list.size(); i++) {
            dao.getCustomerInfoForOrder(list.get(i));
            listDisplay.add(list.get(i));
        }
        int size = -1;
        boolean check = false;
        while (check == false) {
            size = listDisplay.size();
            check = true;
            for (int i = 0; i < size; i++) {
                if (oStatus != -1) {
                    if (listDisplay.get(i).getStatus() != oStatus) {
                        listDisplay.remove(i);
                        check = false;
                        break;
                    }
                }
            }
        }
        check = false;
        while (check == false) {
            size = listDisplay.size();
            check = true;
            for (int i = 0; i < size; i++) {
                if (pStatus != -1) {
                    if (listDisplay.get(i).getPayStatus() != pStatus) {
                        listDisplay.remove(i);
                        check = false;
                        break;
                    }
                }
            }
        }

        check = false;
        while (check == false) {
            size = listDisplay.size();
            check = true;
            for (int i = 0; i < size; i++) {
                if (!listDisplay.get(i).getCustomerFirstName().contains(Search) && !listDisplay.get(i).getCustomerLastName().contains(Search)) {
                    listDisplay.remove(i);
                    check = false;
                    break;
                }
            }
        }
        int compare = 0;
        check = false;
        while (check == false) {
            size = listDisplay.size();
            check = true;
            for (int i = 0; i < size; i++) {
                if (selectOType.equals("Name")) {
                    Collections.sort(listDisplay, (Order o1, Order o2) -> o1.getCustomerLastName().compareTo(o2.getCustomerLastName()));
                }
                if (selectOType.equals("Price")) {
                    Collections.sort(listDisplay, (Order o1, Order o2) ->(int)Math.round(o1.getTotalPrice() - o2.getTotalPrice()));             
                }
                if (selectOType.equals("Date")) {
                    Collections.sort(listDisplay, (Order o1, Order o2) -> o1.getOrderDate().compareTo(o2.getOrderDate()));
                }

            }
        }

        for (Order order : listDisplay) {
            String status = "";
            String payStatus = "";
            String name = order.getCustomerFirstName().substring(0, 1) + order.getCustomerLastName().substring(0, 1);
            String U = "";
            if (order.getStatus() == 1) {
                status = "<td class=\"lidget-column-hidden-5-lite\">\n"
                        + "                                                                    <span data-bs-toggle=\"tooltip\" data-bs-html=\"true\" title=\"\" data-bs-original-title=\"<span>Processing<br> <span class=&quot;fw-bold&quot;>Value:</span> " + order.getTotalPrice() + "<br> <span class=&quot;fw-bold&quot;>Order Date:</span> " + order.getOrderDate() + "</span>\"\n"
                        + "                                                                          aria-label=\"<span>Processing<br> <span class=&quot;fw-bold&quot;>Value:</span> " + order.getTotalPrice() + "<br> <span class=&quot;fw-bold&quot;>Order Date:</span> " + order.getOrderDate() + "</span>\"><div class=\"avatar avatar-status bg-light-warning\"><span class=\"avatar-content\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-pie-chart avatar-icon\"><path d=\"M21.21 15.89A10 10 0 1 1 8 2.83\"></path><path d=\"M22 12A10 10 0 0 0 12 2v10z\"></path></svg></span></div></span>\n"
                        + "                                                                </td>";
            }
            if (order.getStatus() == 2) {
                status = "<td class=\"lidget-column-hidden-5-lite\">\n"
                        + "                                                                    <span data-bs-toggle=\"tooltip\" data-bs-html=\"true\" title=\"\" data-bs-original-title=\"<span>Have Trouble<br> <span class=&quot;fw-bold&quot;>Value:</span> " + order.getTotalPrice() + "<br> <span class=&quot;fw-bold&quot;>Order Date:</span> " + order.getOrderDate() + "</span>\"\n"
                        + "                                                                          aria-label=\"<span>Have Trouble<br> <span class=&quot;fw-bold&quot;>Value:</span> " + order.getTotalPrice() + "<br> <span class=&quot;fw-bold&quot;>Order Date:</span> " + order.getOrderDate() + "</span>\"><div class=\"avatar avatar-status bg-light-danger\"><span class=\"avatar-content\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-info avatar-icon\"><circle cx=\"12\" cy=\"12\" r=\"10\"></circle><line x1=\"12\" y1=\"16\" x2=\"12\" y2=\"12\"></line><line x1=\"12\" y1=\"8\" x2=\"12.01\" y2=\"8\"></line></svg></span></div></span></td>";
            }
            if (order.getStatus() == 3) {
                status = "<td class=\"lidget-column-hidden-5-lite\">\n"
                        + "                                                                    <span data-bs-toggle=\"tooltip\" data-bs-html=\"true\" title=\"\" data-bs-original-title=\"<span>Completed<br> <span class=&quot;fw-bold&quot;>Value:</span> " + order.getTotalPrice() + "<br> <span class=&quot;fw-bold&quot;>Order Date:</span> " + order.getOrderDate() + "</span>\"\n"
                        + "                                                                          aria-label=\"<span>Completed<br> <span class=&quot;fw-bold&quot;>Value:</span> " + order.getTotalPrice() + "<br> <span class=&quot;fw-bold&quot;>Order Date:</span> " + order.getOrderDate() + "</span>\"><div class=\"avatar avatar-status bg-light-success\"><span class=\"avatar-content\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-check-circle avatar-icon\"><path d=\"M22 11.08V12a10 10 0 1 1-5.93-9.14\"></path><polyline points=\"22 4 12 14.01 9 11.01\"></polyline></svg></span></div></span></td> ";
            }
            if (order.getPayStatus() == 1) {
                payStatus = "<td class=\"lidget-column-hidden-2\"><span class=\"badge rounded-pill badge-light-success\" text-capitalized=\"\"> Paid </span>\n"
                        + "                                                                </td>";
            }
            if (order.getPayStatus() == 2) {
                payStatus = "<td class=\"lidget-column-hidden-2\"><span class=\"badge rounded-pill badge-light-warning\" text-capitalized=\"\"> POST PAID </span>\n"
                        + "                                                                </td>";
            }

            out.println("<tr class=\"odd\">\n"
                    + "                                                        <td class=\" control lidget-column-show\" tabindex=\"0\" style=\"display: none;\"></td>\n"
                    + "                                                        <td class=\"\"><a class=\"fw-bold\" href=\"orderDetail?orderID=" + order.getOrderID() + "\"> #" + order.getOrderID() + "</a></td>\n"
                    + "\n" + status
                    + "                                                        <td class=\"sorting_1\">\n"
                    + "                                                            <div class=\"d-flex justify-content-left align-items-center\">\n"
                    + "                                                                <div class=\"avatar-wrapper\">\n"
                    + "                                                                    <div class=\"avatar bg-light-danger me-50\">\n"
                    + "                                                                        <div class=\"avatar-content\"><c:set var=\"firstName\" value=\"${order.customerFirstName}\"/>\n"
                    + "                                                                            <c:set var=\"lastName\" value=\"${order.customerLastName}\"/>\n"
                    + "                                                                            <div  class=\"avatar bg-light-danger me-50\" style=\"margin-right:0 !important\">\n"
                    + "                                                                                <div class=\"avatar-content\">" + name + "</div>\n"
                    + "                                                                            </div></div>\n"
                    + "                                                                    </div>\n"
                    + "                                                                </div>\n"
                    + "                                                                <div class=\"d-flex flex-column\">\n"
                    + "                                                                    <h6  class=\"user-name text-truncate mb-0\">" + order.getCustomerFirstName() + " " + order.getCustomerLastName() + "</h6><small\n"
                    + "                                                                        class=\"text-truncate text-muted\">" + order.getCustomerEmail() + "</small>\n"
                    + "                                                                </div>\n"
                    + "                                                            </div>\n"
                    + "                                                        </td>\n"
                    + "                                                        <td class=\"lidget-column-hidden-4\"><span class=\"d-none\">" + order.getTotalPrice() + "</span>$" + order.getTotalPrice() + "</td>\n"
                    + "                                                        <td class=\"lidget-column-hidden-3\"><span class=\"d-none\">" + order.getOrderDate() + "</span>" + order.getOrderDate() + "</td>\n"
                    + "\n"
                    + "\n" + payStatus
                    + "\n"
                    + "                                                        <td class=\"lidget-column-hidden\">\n"
                    + "                                                            <div class=\"d-flex align-items-center col-actions\">\n"
                    + "                                                                <a href=\"editOrderController?oID=" + order.getOrderID() + "\" class=\"me-1\" href=\"#\" data-bs-toggle=\"tooltip\"\n"
                    + "                                                                   data-bs-placement=\"top\" title=\"\" data-bs-original-title=\"Edit Orders\"\n"
                    + "                                                                   aria-label=\"Edit Orders\">\n"
                    + "                                                                    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-edit-2\"><path d=\"M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z\"></path></svg>\n"
                    + "                                                                </a>\n"
                    + "                                                                <a class=\"me-25\" href=\"orderDetail?orderID=" + order.getOrderID() + "\" data-bs-toggle=\"tooltip\"\n"
                    + "                                                                   data-bs-placement=\"top\" title=\"\" data-bs-original-title=\"View Order\"\n"
                    + "                                                                   aria-label=\"View Order\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\"\n"
                    + "                                                                                             viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\"\n"
                    + "                                                                                             stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
                    + "                                                                                             class=\"feather feather-eye font-medium-2 text-body\">\n"
                    + "                                                                    <path d=\"M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z\"></path>\n"
                    + "                                                                    <circle cx=\"12\" cy=\"12\" r=\"3\"></circle>\n"
                    + "                                                                    </svg>\n"
                    + "                                                                </a>\n"
                    + "                                                            </div>\n"
                    + "                                                        </td>\n"
                    + "                                                    </tr>");

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
