/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ReviewDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.Product;
import model.Review;
import model.User;

/**
 *
 * @author ASUS
 */
public class addReview extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            Account u = (Account) session.getAttribute("account");
            if (u == null) {
                response.sendRedirect("started.jsp");
            }
            String contentReview = request.getParameter("contentReview");
            Product product = (Product) session.getAttribute("product");
            int productID = product.getProductID();
            ReviewDB dao = new ReviewDB();
            int storeId = dao.getStoreId(productID);
            int count = dao.getNumberOfReview();
            Review r = new Review(1, productID, contentReview, u.getAccountID(), dao.getNameOfCustomer(u.getAccountID()), dao.getRoleCustomer(u.getAccountID()));
            r.setReviewId(count + 1);
            String star = request.getParameter("ratingStar");
            r.setRatingStar(Integer.valueOf(star));
            if (u != null) {
                dao.insertReview(r);
                dao.insertRatingStar(r);
            }

            List<Review> listR = dao.getAllReviewOfProduct(productID);
            session.setAttribute("reviewList", listR);
            for (Review review : listR) {

                out.println("" + u.getAccountID() + "");
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ReviewDB dao = new ReviewDB();
        HttpSession session = request.getSession();
        Product product = (Product) session.getAttribute("product");
            int productID = product.getProductID();

        PrintWriter out = response.getWriter();
        List<Review> listR = dao.getAllReviewOfProduct(productID);
        session.setAttribute("reviewList", listR);
        

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
