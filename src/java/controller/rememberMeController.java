/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.Manager;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import model.Account;
import model.Staff;
import model.User;
import model.UserService;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "rememberMeController", urlPatterns = "/rememberMeController")
public class rememberMeController extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String getCookie = getTokenizer(request);
        UserService userService = Manager.searchKey(getCookie);
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        boolean isTokenOut = false;

        if (userService != null) {
            Clock clock = Clock.systemDefaultZone();
            LocalDateTime currentDate = LocalDateTime.now(clock).withNano(0);
            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String sqlQuery = "SELECT [EXPIRED] FROM TOKEN_AUTH_KEY WHERE [KEY] = ?";
            String[] split = UserDB.checkTokenExpired(userService.getKey(), sqlQuery).split("\\.");
            LocalDateTime local = LocalDateTime.parse(split[0], dateFormat);
            LocalDateTime tempDateTime = LocalDateTime.from(local).withNano(0);
            long monthDistance = tempDateTime.until(currentDate, ChronoUnit.MONTHS);
            if (monthDistance < 1) {
                Account account = Manager.searchAccountID(userService.getUserID());
                if (account.getRole().equalsIgnoreCase("STAFF") || account.getRole().equalsIgnoreCase("ADMIN")) {
                    Staff staff = StaffDB.searchAccountID(account.getAccountID());
                    session.setAttribute("staff", staff);
                } else {
                    User user = UserDB.searchAccountID(account.getAccountID());
                    session.setAttribute("user", user);
                }
            } else {
                deleteRememberMeCookie(request, response);
                session.removeAttribute("user");
                session.removeAttribute("staff");
            }
        } else {
            isTokenOut = true;
        }
        response.sendRedirect("app-product-shop.jsp");
    }

    private String getTokenizer(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        String tokenizer = "";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("remember_me")) {
                    tokenizer = cookie.getValue();
                }
            }
        }
        return tokenizer;
    }

    public void deleteRememberMeCookie(HttpServletRequest request, HttpServletResponse resp) {
        Manager.deleteAuth(getTokenizer(request));
        Cookie cookie = new Cookie("remember_me", "");
        cookie.setPath("/");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
    }

}
