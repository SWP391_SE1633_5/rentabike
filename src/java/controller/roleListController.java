/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.Manager;
import dao.PermissionDetailDB;
import dao.ProductDB;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import model.Account;
import model.PermissionsDetail;
import model.Staff;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "roleListController", urlPatterns = "/roleListController")
public class roleListController extends HttpServlet {

    private static final String sqlQuery = "SELECT * FROM PERMISSION_DETAILS";
    private static final String queryData = "SELECT COUNT([ROLE]) FROM ACCOUNT WHERE [ROLE] = ?";
    private static final String queryTotal = "SELECT COUNT(A.ACCOUNT_ID) FROM ACCOUNT AS A";
    private static final String queryStaff = "SELECT COUNT(ACTIVE) FROM STAFFS WHERE ACTIVE = ?";
    private static final String queryUser = "SELECT COUNT([STATUS]) FROM CUSTOMERS WHERE [STATUS] = ?";
    private static final String queryRoleAccount = "SELECT ACCOUNT_ID, [ROLE] FROM ACCOUNT \n"
            + "ORDER BY (CASE ROLE \n"
            + "WHEN 'ADMIN' THEN 0  \n"
            + "WHEN 'STAFF' THEN 1\n"
            + "WHEN 'CUSTOMER' THEN 2\n"
            + "WHEN 'SALEPERSON' THEN 3\n"
            + "END)";
    private static final String searchQuery = "SELECT DISTINCT C.*, A.ROLE FROM CUSTOMERS AS C, ACCOUNT AS A WHERE C.[ACCOUNT_ID] = ? AND C.EMAIL = A.EMAIL";
    private static final String searchQueryAlt = "SELECT DISTINCT S.*, A.ROLE FROM STAFFS AS S, ACCOUNT AS A WHERE S.EMAIL = A.EMAIL AND S.[ACCOUNT_ID] = ?";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        String pagination = request.getParameter("page");
        PrintWriter out = response.getWriter();

        if (pagination == null) {
            pagination = "1";
        }

        if (account != null) {
            getTotalData(session);
            switch (pagination) {
                case "1":
                    listUser(session);
                    listPermission(session);
                    break;
                default:
                    paginationPage(request, out, session);
            }
            response.sendRedirect("app-access-roles.jsp");
        } else {
            response.sendRedirect("page-misc-not-authorized.html");
        }
    }

    private void selectRole(HttpServletRequest request, PrintWriter out) {
        String selectOption = request.getParameter("selectOption");
        if ("".equals(selectOption) || selectOption.length() == 0) {
            out.print("RESET");
            return;
        }
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();

        if (accountListRole != null) {
            for (Account account : accountListRole) {
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountByOption(account.getAccountID(), selectOption,
                            searchQuery + " AND A.ROLE = ?", "Select");
                    if (user != null) {
                        userAccountList.add(user);
                    }
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountByOption(account.getAccountID(), selectOption,
                            searchQueryAlt + " AND A.ROLE = ?", "Select");
                    if (staff != null) {
                        staffAccountList.add(staff);
                    }
                }
            }
            displayList(out, staffAccountList, userAccountList);
        } else {
            out.print("RESET");
        }
    }

    private void searchUserName(HttpServletRequest request, PrintWriter out) {
        String inputSearch = request.getParameter("inputSearch");
        if ("".equals(inputSearch) || inputSearch.length() == 0) {
            out.print("RESET");
            return;
        }
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();

        if (accountListRole != null) {
            for (Account account : accountListRole) {
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountByOption(account.getAccountID(), inputSearch,
                            searchQuery + " AND CONCAT(C.FIRST_NAME, ' ', C.LAST_NAME) LIKE ?", "Search");
                    if (user != null) {
                        userAccountList.add(user);
                    }
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountByOption(account.getAccountID(), inputSearch,
                            searchQueryAlt + " AND CONCAT(S.FIRST_NAME, ' ', S.LAST_NAME) LIKE ?", "Search");
                    if (staff != null) {
                        staffAccountList.add(staff);
                    }
                }
            }
            displayList(out, staffAccountList, userAccountList);
        } else {
            out.print("RESET");
        }
    }

    private void listUser(HttpSession session) {
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();
        LinkedList<Integer> numberPaginationList = new LinkedList<>();
        LinkedList<Integer> showEntryList = new LinkedList<>();
        showEntryList.add(10);
        showEntryList.add(5);
        showEntryList.add(50);
        showEntryList.add(100);
        String showEntry = (String) session.getAttribute("showEntry");
        int showEntryCurrent = 10;

        if (showEntry != null) {
            showEntryCurrent = Integer.parseInt(showEntry);
        }

        if (accountListRole != null) {
            for (Account account : accountListRole) {
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountID(account.getAccountID());
                    userAccountList.add(user);
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountID(account.getAccountID());
                    staffAccountList.add(staff);
                }
            }
            int endStaff = staffAccountList.size();
            int endUser = showEntryCurrent - endStaff - 1;
            double showTotalPage = (double) (showEntryCurrent - 1) + 1.0;
            int numberPaginaton = (int) Math.ceil(accountListRole.size() / showTotalPage);
            for (int i = 0; i < numberPaginaton; i++) {
                numberPaginationList.add(i + 1);
            }
            if (showEntry != null) {
                session.setAttribute("endStaff", Integer.parseInt(showEntry) - 1);
            } else {
                session.setAttribute("endStaff", (endStaff - 2));
            }
            session.setAttribute("showEntry", String.valueOf(showEntryCurrent));
            session.setAttribute("numberPaginationList", numberPaginationList);
            session.setAttribute("currentPage", 1);
            session.setAttribute("limitStart", 1);
            session.setAttribute("limitEnd", showEntryCurrent);
            session.setAttribute("beginStaff", 0);
            session.setAttribute("beginUser", 0);
            session.setAttribute("endUser", endUser);
            session.setAttribute("showEntryList", showEntryList);
            session.setAttribute("userAccountList", userAccountList);
            session.setAttribute("staffAccountList", staffAccountList);
        }
    }

    private void getTotalData(HttpSession session) {
        int totalUser = Manager.countUserData(queryTotal, -1);
        int totalAdmin = Manager.countRoleData(queryData, "ADMIN");
        int totalStaff = Manager.countRoleData(queryData, "STAFF");
        int totalCustomer = Manager.countRoleData(queryData, "CUSTOMER");
        int totalSupport = Manager.countRoleData(queryData, "SUPPORT");
        int totalSaleperson = Manager.countRoleData(queryData, "SALEPERSON");
        int bannedUser = Manager.countUserData(queryUser, 2);
        int bannedStaff = Manager.countUserData(queryStaff, 0);

        session.setAttribute("totalAccount", totalUser);
        session.setAttribute("totalAdmin", totalAdmin);
        session.setAttribute("totalStaff", totalStaff);
        session.setAttribute("totalCustomer", totalCustomer);
        session.setAttribute("totalSupport", totalSupport);
        session.setAttribute("totalSaleperson", totalSaleperson);
        session.setAttribute("totalBannedUser", (bannedUser + bannedStaff));
    }

    private void paginationPage(HttpServletRequest request, PrintWriter out, HttpSession session) {
        String showEntry = request.getParameter("showEntry");
        String showEntryCurrent = (String) session.getAttribute("showEntry");
        int pagination = Integer.parseInt(request.getParameter("page"));
        int nextPage;
        if ((showEntry == null || "".equals(showEntry) || showEntry.length() == 0) && showEntryCurrent == null) {
            nextPage = 10;
        } else {
            if (showEntry != null) {
                nextPage = Integer.parseInt(showEntry);
            } else {
                nextPage = Integer.parseInt(showEntryCurrent);
            }
        }

        if (showEntryCurrent != null) {
            if (showEntry == null) {
                showEntry = showEntryCurrent;
            }
        }
        int totalPage = pagination * nextPage;
        int indexPage = totalPage - nextPage;
        LinkedList<Account> accountListRole = Manager.accountListRole(queryRoleAccount);
        LinkedList<User> userAccountList = new LinkedList<>();
        LinkedList<Staff> staffAccountList = new LinkedList<>();

        for (int i = indexPage; i < totalPage; i++) {
            if (i < accountListRole.size()) {
                Account account = accountListRole.get(i);
                if (account.getRole().equalsIgnoreCase("CUSTOMER") || account.getRole().equalsIgnoreCase("SALEPERSON")) {
                    User user = UserDB.searchAccountID(account.getAccountID());
                    userAccountList.add(user);
                }
                if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
                    Staff staff = StaffDB.searchAccountID(account.getAccountID());
                    staffAccountList.add(staff);
                }
            }
        }
        int endStaff = staffAccountList.size();
        int endUser = nextPage - endStaff - 1;

        session.setAttribute("showEntry", showEntry);
        session.setAttribute("currentPage", pagination);
        session.setAttribute("limitStart", indexPage + 1);
        session.setAttribute("limitEnd", totalPage);
        session.setAttribute("endStaff", endStaff);
        session.setAttribute("endUser", endUser);
        session.setAttribute("userAccountList", userAccountList);
        session.setAttribute("staffAccountList", staffAccountList);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String mode = request.getParameter("mode");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
//        if (account != null) {
        switch (mode) {
            case "viewProfile":
                new userListController().viewProfile(request, out);
                break;
            case "showEntry":
                paginationPage(request, out, session);
                break;
            case "searchName":
                searchUserName(request, out);
                break;
            case "selectRole":
                selectRole(request, out);
                break;
            case "handleUserStatus":
                handleUserStatus(request, out);
                break;
            case "viewRole":
                viewRole(request, out);
                break;
            case "updateRole":
                updateRole(request, out);
                break;
        }
//        } else {
//            out.print("sessionFailed");
//        }
    }

    private void displayList(PrintWriter out, LinkedList<Staff> staffAccountList, LinkedList<User> userAccountList) {
        for (Staff staff : staffAccountList) {
            String avatarElement;

            if (staff.getAvatar() != null) {
                avatarElement = "<div class=\"avatar-wrapper\">\n"
                        + "<div class=\"avatar  me-1\">\n"
                        + "<img src=\"" + staff.getAvatar() + "\" alt=\"Avatar\"\n"
                        + "height=\"32\" width=\"32\">\n"
                        + "</div>\n"
                        + "</div>\n";
            } else {
                avatarElement = "<div class=\"avatar-wrapper\">\n"
                        + "<div class=\"avatar bg-light-warning me-1\">\n"
                        + "<span class=\"avatar-content\">" + staff.getFirstName().substring(0, 1) + staff.getLastName().substring(0, 1) + "</span>\n"
                        + "</div>\n"
                        + "</div>\n";
            }

            String roleUser;

            if (staff.getRole().equalsIgnoreCase("STAFF")) {
                roleUser = "<span class=\"text-truncate align-middle\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-settings font-medium-3 text-warning me-50\">\n"
                        + "<circle cx=\"12\" cy=\"12\" r=\"3\"></circle>\n"
                        + "<path d=\"M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z\">\n"
                        + "</path>\n"
                        + "</svg>\n"
                        + "<span class=\"roleSort\">Staff</span>\n"
                        + "</span>";
            } else {
                roleUser = "<span class=\"text-truncate align-middle\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\n"
                        + "stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
                        + "class=\"feather feather-slack font-medium-3 text-danger me-50\">\n"
                        + "<path\n"
                        + "d=\"M14.5 10c-.83 0-1.5-.67-1.5-1.5v-5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5z\">\n"
                        + "</path>\n"
                        + "<path d=\"M20.5 10H19V8.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z\"></path>\n"
                        + "<path\n"
                        + "d=\"M9.5 14c.83 0 1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5S8 21.33 8 20.5v-5c0-.83.67-1.5 1.5-1.5z\">\n"
                        + "</path>\n"
                        + "<path d=\"M3.5 14H5v1.5c0 .83-.67 1.5-1.5 1.5S2 16.33 2 15.5 2.67 14 3.5 14z\"></path>\n"
                        + "<path\n"
                        + "d=\"M14 14.5c0-.83.67-1.5 1.5-1.5h5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-5c-.83 0-1.5-.67-1.5-1.5z\">\n"
                        + "</path>\n"
                        + "<path d=\"M15.5 19H14v1.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5z\"></path>\n"
                        + "<path\n"
                        + " d=\"M10 9.5C10 8.67 9.33 8 8.5 8h-5C2.67 8 2 8.67 2 9.5S2.67 11 3.5 11h5c.83 0 1.5-.67 1.5-1.5z\">\n"
                        + "</path>\n"
                        + "<path d=\"M8.5 5H10V3.5C10 2.67 9.33 2 8.5 2S7 2.67 7 3.5 7.67 5 8.5 5z\"></path>\n"
                        + "</svg><span class=\"roleSort\">Admin</span>\n"
                        + "</span>\n";
            }

            String status;
            if (staff.getActive() == 0) {
                status = "<span class=\"badge rounded-pill badge-light-primary\" text-capitalized=\"\">Inactive</span>\n";
            } else if (staff.getActive() == 1) {
                status = "<span class=\"badge rounded-pill badge-light-success\" text-capitalized=\"\">Active</span>\n";
            } else {
                status = "<span class=\"badge rounded-pill badge-light-secondary\" text-capitalized=\"\">Banned</span>\n";
            }

            String buttonAction;
            if (staff.getActive() == 2) {
                buttonAction = "<button style=\"padding:0;\" class=\"btn btn-sm btn-icon waves-effect waves-float waves-light flex-default flex-center-align\" current-action=\"" + staff.getRole() + "\" data-title=\"unlock-account\" onclick=\"sendParamterHandler(this)\" index=\"" + staff.getAccountID() + "\" data-bs-toggle=\"modal\" data-bs-target=\"#unlockUser\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-edit font-medium-2 text-body\">\n"
                        + "<path d=\"M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7\"></path>\n"
                        + "<path d=\"M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z\"></path>\n"
                        + "</svg>\n"
                        + "<span style=\"margin-left:0.25rem;color:#B4B7BD;\">Unlocked</span>\n"
                        + "</button> ";
            } else {
                buttonAction = "<button style=\"padding:0;\" class=\"btn btn-sm btn-icon waves-effect waves-float waves-light flex-default flex-center-align\" current-action=\"" + staff.getRole() + "\" data-title=\"suspended-account\" onclick=\"sendParamterHandler(this)\" index=\"" + staff.getAccountID() + "\" data-bs-toggle=\"modal\" data-bs-target=\"#suspendedUser\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-edit font-medium-2 text-body\">\n"
                        + "<path d=\"M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7\"></path>\n"
                        + "<path d=\"M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z\"></path>\n"
                        + "</svg>\n"
                        + "<span style=\"margin-left:0.25rem;color:#B4B7BD;\">Suspended</span>\n"
                        + "</button> ";
            }

            out.print("<tr>\n"
                    + "<td class=\" control\" tabindex=\"0\" style=\"display: none;\"></td>\n"
                    + "<td class=\"\">\n"
                    + "<div class=\"d-flex justify-content-left align-items-center\">\n"
                    + avatarElement
                    + "<div class=\"d-flex flex-column\">\n"
                    + "<a href=\"#/\" index=\"" + staff.getAccountID() + "\" onclick=\"sendParamterView(this)\"  data-bs-toggle=\"modal\" data-bs-target=\"#defaultSize\"\n"
                    + "class=\"user_name text-body text-truncate\">\n"
                    + "<span class=\"fw-bolder\">" + staff.getFirstName() + " " + staff.getLastName() + "</span>\n"
                    + "</a>\n"
                    + "<small class=\"emp_post text-muted\">" + staff.getEmail() + "</small>\n"
                    + "</div>\n"
                    + "</div>\n"
                    + "</td>\n"
                    + "\n"
                    + "<td class=\"sorting_1\">\n"
                    + roleUser
                    + "</td>\n"
                    + "<td>" + staff.getDateOfJoin() + "</td>\n"
                    + "<td><span class=\"text-nowrap\">" + staff.getPermissionDetails() + "</span></td>\n"
                    + "<td>\n"
                    + status
                    + "</td>\n"
                    + "<td>\n"
                    + "<div class=\"d-flex align-items-center col-actions\">\n"
                    + "<div class=\"btn-group\">\n"
                    + "<a class=\"btn btn-sm dropdown-toggle hide-arrow waves-effect waves-float waves-light\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-more-vertical font-small-4\">\n"
                    + "<circle cx=\"12\" cy=\"12\" r=\"1\"></circle>\n"
                    + "<circle cx=\"12\" cy=\"5\" r=\"1\"></circle>\n"
                    + "<circle cx=\"12\" cy=\"19\" r=\"1\"></circle>\n"
                    + "</svg>\n"
                    + "</a>\n"
                    + "<div class=\"dropdown-menu dropdown-menu-end\">\n"
                    + "<a href=\"javascript:;\" data-title=\"edit-role-account\" current-action=\"" + staff.getRole() + "\" index=\"" + staff.getAccountID() + "\" onclick=\"sendParamterHandler(this)\" data-bs-target=\"#addRoleModal\"\n"
                    + "class=\"dropdown-item role-edit-modal\">\n"
                    + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-user-plus\"><path d=\"M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2\"></path><circle cx=\"8.5\" cy=\"7\" r=\"4\"></circle><line x1=\"20\" y1=\"8\" x2=\"20\" y2=\"14\"></line><line x1=\"23\" y1=\"11\" x2=\"17\" y2=\"11\"></line></svg><span style=\"margin-left:0.25rem\">Update Role</span>\n"
                    + "</a>\n"
                    + "<a class=\"dropdown-item\">\n"
                    + buttonAction
                    + "</a>\n"
                    + "</div>"
                    + "</div>\n"
                    + "</div>\n"
                    + "</td>"
                    + "</tr>");
        }
        for (User user : userAccountList) {
            String avatarElement;

            if (user.getAvatar() != null) {
                avatarElement = "<div class=\"avatar-wrapper\">\n"
                        + "<div class=\"avatar  me-1\">\n"
                        + "<img src=\"" + user.getAvatar() + "\" alt=\"Avatar\"\n"
                        + "height=\"32\" width=\"32\">\n"
                        + "</div>\n"
                        + "</div>\n";
            } else {
                avatarElement = "<div class=\"avatar-wrapper\">\n"
                        + "<div class=\"avatar bg-light-warning me-1\">\n"
                        + "<span class=\"avatar-content\">" + user.getFirstName().substring(0, 1) + user.getLastName().substring(0, 1) + "</span>\n"
                        + "</div>\n"
                        + "</div>\n";
            }

            String roleUser;

            if (user.getRole().equalsIgnoreCase("CUSTOMER")) {
                roleUser = "<span class=\"text-truncate align-middle\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-user font-medium-3 text-primary me-50\">\n"
                        + "<path d=\"M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2\"></path>\n"
                        + "<circle cx=\"12\" cy=\"7\" r=\"4\"></circle>\n"
                        + "</svg><span class=\"roleSort\">Customer</span>\n"
                        + "</span>";
            } else {
                roleUser = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\"\n"
                        + "stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
                        + "class=\"feather feather-truck\"\n"
                        + "style=\"width: 16.8px;height: 16.8px;margin-right: 7px;\">\n"
                        + "<rect x=\"1\" y=\"3\" width=\"15\" height=\"13\"></rect>\n"
                        + "<polygon points=\"16 8 20 8 23 11 23 16 16 16 16 8\"></polygon>\n"
                        + "<circle cx=\"5.5\" cy=\"18.5\" r=\"2.5\"></circle>\n"
                        + "<circle cx=\"18.5\" cy=\"18.5\" r=\"2.5\"></circle>\n"
                        + "</svg><span class=\"roleSort\">Saleperson</span>";
            }

            String status;
            if (user.getStatus() == 2) {
                status = "<span class=\"badge rounded-pill badge-light-primary\" text-capitalized=\"\">Inactive</span>\n";
            } else if (user.getStatus() == 1) {
                status = "<span class=\"badge rounded-pill badge-light-success\" text-capitalized=\"\">Active</span>\n";
            } else {
                status = "<span class=\"badge rounded-pill badge-light-secondary\" text-capitalized=\"\">Banned</span>\n";
            }

            String buttonAction;
            if (user.getStatus() == 0) {
                buttonAction = "<button style=\"padding:0;\" class=\"btn btn-sm btn-icon waves-effect waves-float waves-light flex-default flex-center-align\" current-action=\"" + user.getRole() + "\" data-title=\"unlock-account\" onclick=\"sendParamterHandler(this)\" index=\"" + user.getAccountID() + "\" data-bs-toggle=\"modal\" data-bs-target=\"#unlockUser\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-edit font-medium-2 text-body\">\n"
                        + "<path d=\"M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7\"></path>\n"
                        + "<path d=\"M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z\"></path>\n"
                        + "</svg>\n"
                        + "<span style=\"margin-left:0.25rem;color:#B4B7BD;\">Unlocked</span>\n"
                        + "</button> ";
            } else {
                buttonAction = "<button style=\"padding:0;\" class=\"btn btn-sm btn-icon waves-effect waves-float waves-light flex-default flex-center-align\" current-action=\"" + user.getRole() + "\" data-title=\"suspended-account\" onclick=\"sendParamterHandler(this)\" index=\"" + user.getAccountID() + "\" data-bs-toggle=\"modal\" data-bs-target=\"#suspendedUser\">\n"
                        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-edit font-medium-2 text-body\">\n"
                        + "<path d=\"M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7\"></path>\n"
                        + "<path d=\"M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z\"></path>\n"
                        + "</svg>\n"
                        + "<span style=\"margin-left:0.25rem;color:#B4B7BD;\">Suspended</span>\n"
                        + "</button> ";
            }
            out.print("<tr>\n"
                    + "<td class=\" control\" tabindex=\"0\" style=\"display: none;\"></td>\n"
                    + "<td class=\"\">\n"
                    + "<div class=\"d-flex justify-content-left align-items-center\">\n"
                    + avatarElement
                    + "<div class=\"d-flex flex-column\">\n"
                    + "<a href=\"#/\" index=\"" + user.getAccountID() + "\" onclick=\"sendParamterView(this)\"  data-bs-toggle=\"modal\" data-bs-target=\"#defaultSize\"\n"
                    + "class=\"user_name text-body text-truncate\">\n"
                    + "<span class=\"fw-bolder\">" + user.getFirstName() + " " + user.getLastName() + "</span>\n"
                    + "</a>\n"
                    + "<small class=\"emp_post text-muted\">" + user.getEmail() + "</small>\n"
                    + "</div>\n"
                    + "</div>\n"
                    + "</td>\n"
                    + "\n"
                    + "<td class=\"sorting_1\">\n"
                    + roleUser
                    + "</td>\n"
                    + "<td>" + user.getDateOfJoin() + "</td>\n"
                    + "<td><span class=\"text-nowrap\">" + user.getPermissionDetails() + "</span></td>\n"
                    + "<td>\n"
                    + status
                    + "</td>\n"
                    + "<td>\n"
                    + "<div class=\"d-flex align-items-center col-actions\">\n"
                    + "<div class=\"btn-group\">\n"
                    + "<a class=\"btn btn-sm dropdown-toggle hide-arrow waves-effect waves-float waves-light\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-more-vertical font-small-4\">\n"
                    + "<circle cx=\"12\" cy=\"12\" r=\"1\"></circle>\n"
                    + "<circle cx=\"12\" cy=\"5\" r=\"1\"></circle>\n"
                    + "<circle cx=\"12\" cy=\"19\" r=\"1\"></circle>\n"
                    + "</svg>\n"
                    + "</a>\n"
                    + "<div class=\"dropdown-menu dropdown-menu-end\">\n"
                    + "<a href=\"javascript:;\" data-title=\"edit-role-account\" index=\"" + user.getAccountID() + "\" current-action=\"" + user.getRole() + "\" onclick=\"sendParamterHandler(this)\" data-bs-target=\"#addRoleModal\"\n"
                    + "class=\"dropdown-item role-edit-modal\">\n"
                    + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-user-plus\"><path d=\"M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2\"></path><circle cx=\"8.5\" cy=\"7\" r=\"4\"></circle><line x1=\"20\" y1=\"8\" x2=\"20\" y2=\"14\"></line><line x1=\"23\" y1=\"11\" x2=\"17\" y2=\"11\"></line></svg><span style=\"margin-left:0.25rem\">Update Role</span>\n"
                    + "</a>\n"
                    + "<a class=\"dropdown-item\">\n"
                    + buttonAction
                    + "</a>\n"
                    + "</div>"
                    + "</div>\n"
                    + "</div>\n"
                    + "</td>"
                    + "</tr>");
        }
    }

    private void handleUserStatus(HttpServletRequest request, PrintWriter out) {
        int idUser = Integer.parseInt(request.getParameter("accountID"));
        String switchContent = request.getParameter("switch");
        String message = request.getParameter("message");
        String duration = request.getParameter("duration");

        if (message.length() == 0 || message.equalsIgnoreCase("") || message == null) {
            if (switchContent.equalsIgnoreCase("banned")) {
                message = "You have been found to have broken the law. We will temporarily lock your account for investigation.";
            } else {
                message = "Try not to break the law next time.";
            }
        }

        Account account = (Account) request.getSession().getAttribute("account");
        if (account != null) {
            Account accountSearch = Manager.searchAccountID(idUser);
            String to, fullName, email;
            int status = 0;
            int statusCreate = 0;
            int statusProduct = 0;
            if (accountSearch.getRole().equalsIgnoreCase("ADMIN") || accountSearch.getRole().equalsIgnoreCase("STAFF")) {
                Staff staff = StaffDB.searchAccountID(accountSearch.getAccountID());
                if (switchContent.equalsIgnoreCase("banned")) {
                    status = Manager.updateStatus(2, accountSearch.getAccountID(), "UPDATE STAFFS SET ACTIVE = ? WHERE [ACCOUNT_ID] = ?");
                    statusCreate = Manager.createNewBannedUser(message, accountSearch.getAccountID(), duration);
                    statusProduct = ProductDB.changeStatusProduct(accountSearch.getAccountID(), 4, true);
                } else {
                    status = Manager.updateStatus(0, accountSearch.getAccountID(), "UPDATE STAFFS SET ACTIVE = ? WHERE [ACCOUNT_ID] = ?");
                    statusCreate = Manager.deleteNewBannedUser(accountSearch.getAccountID());
                    statusProduct = ProductDB.changeStatusProduct(accountSearch.getAccountID(), 3, true);
                }
                to = staff.getEmail();
                email = staff.getEmail();
                fullName = staff.getFirstName() + " " + staff.getLastName();
            } else {
                User user = UserDB.searchAccountID(accountSearch.getAccountID());
                if (switchContent.equalsIgnoreCase("banned")) {
                    status = Manager.updateStatus(0, accountSearch.getAccountID(), "UPDATE CUSTOMERS SET STATUS = ? WHERE [ACCOUNT_ID] = ?");
                    statusCreate = Manager.createNewBannedUser(message, accountSearch.getAccountID(), duration);
                    statusProduct = ProductDB.changeStatusProduct(user.getAccountID(), 4, false);
                } else {
                    status = Manager.updateStatus(2, accountSearch.getAccountID(), "UPDATE CUSTOMERS SET STATUS = ? WHERE [ACCOUNT_ID] = ?");
                    statusCreate = Manager.deleteNewBannedUser(accountSearch.getAccountID());
                    statusProduct = ProductDB.changeStatusProduct(user.getAccountID(), 3, false);
                    if (message.length() == 0 || message.equalsIgnoreCase("") || message == null) {
                        message = "Try not to break the law next time.";
                    }
                }
                to = user.getEmail();
                email = user.getEmail();
                fullName = user.getFirstName() + " " + user.getLastName();
            }

            if (status > 0 && statusCreate > 0) {
                if (switchContent.equalsIgnoreCase("banned")) {
                    String subject = "Account Suspended";
                    String messageAlt = emailHTMLHandle(fullName, email, accountSearch.getAccountID(), message, duration);
                    MailController.send(to, subject, messageAlt, "lifeofcoffie@gmail.com", "shgwxqwapkgwzqkk");
                    out.print("SUCCESS");
                } else {
                    String subject = "Account Unlocked";
                    String messageAlt = emailHTMLHandle(fullName, email, accountSearch.getAccountID(), message, "Unlocked");
                    MailController.send(to, subject, messageAlt, "lifeofcoffie@gmail.com", "shgwxqwapkgwzqkk");
                }
            } else {
                out.print("FAILED");
            }
        }
    }

    private String emailHTMLHandle(String fullName, String emailSend, int accountID, String message, String duration) {
        String email = "    <div style=\"margin: 0; padding: 0; width: 100%; word-break: break-word; -webkit-font-smoothing: antialiased;  background-color: #eceff1; background-color: rgba(236, 239, 241, 1);\">\n"
                + "        <div style=\"display: none;\">A message about Your Hilfmotor Account</div>\n"
                + "        <div role=\"article\" aria-roledescription=\"email\" aria-label=\"Reset your Password\" lang=\"en\">\n"
                + "            <table style=\"font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; width: 100%;\" width=\"100%\"\n"
                + "                cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                <tr>\n"
                + "                    <td align=\"center\"\n"
                + "                        style=\" background-color: #eceff1; background-color: rgba(236, 239, 241, 1); font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif;\"\n"
                + "                        bgcolor=\"rgba(236, 239, 241, 1)\">\n"
                + "                        <table class=\"sm-w-full\" style=\"font-family: 'Montserrat',Arial,sans-serif; width: 600px;\"\n"
                + "                            width=\"600\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                            <tr>\n"
                + "                                <td class=\"sm-py-32 sm-px-24\"\n"
                + "                                    style=\"font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; padding: 48px; text-align: center;\"\n"
                + "                                    align=\"center\">\n"
                + "                                    <a>\n"
                + "                                        <img src=\"https://i.pinimg.com/736x/b3/a6/a1/b3a6a10a79908d5399673e4de0d89b80.jpg\"\n"
                + "                                            style=\"border-radius:50%;\" width=\"155\" alt=\"Hilfmotor Admin\"\n"
                + "                                            style=\"border: 0; max-width: 100%; line-height: 100%; vertical-align: middle;\">\n"
                + "                                    </a>\n"
                + "                                </td>\n"
                + "                            </tr>\n"
                + "                            <tr>\n"
                + "                                <td align=\"center\" class=\"sm-px-24\" style=\"font-family: 'Montserrat',Arial,sans-serif;\">\n"
                + "                                    <table style=\"font-family: 'Montserrat',Arial,sans-serif; width: 100%;\" width=\"100%\"\n"
                + "                                        cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                                        <tr>\n"
                + "                                            <td class=\"sm-px-24\"\n"
                + "                                                style=\" background-color: #ffffff; background-color: rgba(255, 255, 255, 1); border-radius: 4px; font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; font-size: 14px; line-height: 24px; padding: 48px; text-align: left;  color: #626262; color: rgba(98, 98, 98, 1);\"\n"
                + "                                                bgcolor=\"rgba(255, 255, 255, 1)\" align=\"left\">\n"
                + "                                                <p style=\"font-weight: 600; font-size: 18px; margin-bottom: 0;\">Hey</p>\n"
                + "                                                <p\n"
                + "                                                    style=\"font-weight: 700; font-size: 20px; margin-top: 0;  color: #ff5850; color: rgba(255, 88, 80, 1);\">\n"
                + "                                                     " + fullName + "</p>\n"
                + "                                                <p style=\"margin: 0 0 24px;\">\n"
                + "                                                    A request about your account status from\n"
                + "                                                    <span style=\"font-weight: 600;\">Hilfsmotor</span> Account -\n"
                + "                                                    <a href=\"#/\" class=\"hover-underline\"\n"
                + "                                                        style=\" color: #7367f0; color: rgba(115, 103, 240, 1); text-decoration: none;\">" + emailSend + "</a>\n"
                + "                                                    (ID: HF-" + accountID + ").\n"
                + "                                                </p>\n"
                + "                                                <p style=\"margin: 0 0 24px;\">Your account had been suspended.\n"
                + "                                                </p>\n"
                + "                                                <table style=\"font-family: 'Montserrat',Arial,sans-serif; margin:auto;\"\n"
                + "                                                    cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                                                    <tr>\n"
                + "                                                        <td style=\"mso-padding-alt: 16px 24px;  background-color: #7367f0; background-color: rgba(115, 103, 240, 1); border-radius: 4px; font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif;\"\n"
                + "                                                            bgcolor=\"rgba(115, 103, 240, 1)\">\n"
                + "                                                            <a href=\"#/\"\n"
                + "                                                                style=\"display: block; font-weight: 600; font-size: 14px; line-height: 100%; padding: 16px 24px;  color: #ffffff; color: rgba(255, 255, 255, 1); text-decoration: none;\">Your\n"
                + "                                                                Duration: " + duration + " </a>\n"
                + "                                                        </td>\n"
                + "                                                    </tr>\n"
                + "                                                </table>\n"
                + "                                                <p style=\"margin: 24px 0;\">\n"
                + "                                                    <span style=\"font-weight: 600;\">Message:</span> " + message + "\n"
                + "                                                </p>\n"
                + "                                                <p style=\"margin: 0;\">\n"
                + "                                                    If you did not think that's we did right or something mistake. You can contact to us.\n"
                + "                                                    <a href=\"mailto:hilfsmotor@gmail.com\" class=\"hover-underline\"\n"
                + "                                                        style=\"color: #7367f0; color: rgba(115, 103, 240, 1); text-decoration: none;\">hilfsmotor@gmail.com</a>\n"
                + "                                                </p>\n"
                + "                                                <table style=\"font-family: 'Montserrat',Arial,sans-serif; width: 100%;\"\n"
                + "                                                    width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                                                    <tr>\n"
                + "                                                        <td\n"
                + "                                                            style=\"font-family: 'Montserrat',Arial,sans-serif; padding-top: 32px; padding-bottom: 32px;\">\n"
                + "                                                            <div\n"
                + "                                                                style=\"background-color: #eceff1; background-color: rgba(236, 239, 241, 1); height: 1px; line-height: 1px;\">\n"
                + "                                                                &zwnj;</div>\n"
                + "                                                        </td>\n"
                + "                                                    </tr>\n"
                + "                                                </table>\n"
                + "                                                <p style=\"margin: 0 0 16px;\">\n"
                + "                                                    Not sure why you received this email? Please\n"
                + "                                                    <a href=\"mailto:hilfsmotor@gmail.com\" class=\"hover-underline\"\n"
                + "                                                        style=\"color: #7367f0; color: rgba(115, 103, 240, 1); text-decoration: none;\">let\n"
                + "                                                        us know</a>.\n"
                + "                                                </p>\n"
                + "                                                <p style=\"margin: 0 0 16px;\">Thanks, <br>The Hilfsmotor Team</p>\n"
                + "                                            </td>\n"
                + "                                        </tr>\n"
                + "                                        <tr>\n"
                + "                                            <td style=\"font-family: 'Montserrat',Arial,sans-serif; height: 20px;\"\n"
                + "                                                height=\"20\"></td>\n"
                + "                                        </tr>\n"
                + "                                        <tr>\n"
                + "                                            <td style=\"font-family: 'Montserrat',Arial,sans-serif; height: 16px;\"\n"
                + "                                                height=\"16\"></td>\n"
                + "                                        </tr>\n"
                + "                                    </table>\n"
                + "                                </td>\n"
                + "                            </tr>\n"
                + "                        </table>\n"
                + "                    </td>\n"
                + "                </tr>\n"
                + "            </table>\n"
                + "        </div>\n"
                + "    </div>";
        return email;
    }

    private String emailHTMLUpdate(String fullName, String emailSend, int accountID, String role) {
        String email = "<div style=\"margin: 0; padding: 0; width: 100%; word-break: break-word; -webkit-font-smoothing: antialiased;  background-color: #eceff1; background-color: rgba(236, 239, 241, 1);\">\n"
                + "        <div style=\"display: none;\">We bring some good new to you. Please open it.</div>\n"
                + "        <div role=\"article\" lang=\"en\">\n"
                + "            <table style=\"font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; width: 100%;\" width=\"100%\"\n"
                + "                cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                <tr>\n"
                + "                    <td align=\"center\"\n"
                + "                        style=\" background-color: #eceff1; background-color: rgba(236, 239, 241, 1); font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif;\"\n"
                + "                        bgcolor=\"rgba(236, 239, 241, 1)\">\n"
                + "                        <table class=\"sm-w-full\" style=\"font-family: 'Montserrat',Arial,sans-serif; width: 600px;\"\n"
                + "                            width=\"600\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                            <tr>\n"
                + "                                <td class=\"sm-py-32 sm-px-24\"\n"
                + "                                    style=\"font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; padding: 48px; text-align: center;\"\n"
                + "                                    align=\"center\">\n"
                + "                                    <a>\n"
                + "                                        <img src=\"https://i.pinimg.com/736x/b3/a6/a1/b3a6a10a79908d5399673e4de0d89b80.jpg\"\n"
                + "                                            style=\"border-radius:50%;\" width=\"155\" alt=\"Vuexy Admin\"\n"
                + "                                            style=\"border: 0; max-width: 100%; line-height: 100%; vertical-align: middle;\">\n"
                + "                                    </a>\n"
                + "                                </td>\n"
                + "                            </tr>\n"
                + "                            <tr>\n"
                + "                                <td align=\"center\" class=\"sm-px-24\" style=\"font-family: 'Montserrat',Arial,sans-serif;\">\n"
                + "                                    <table style=\"font-family: 'Montserrat',Arial,sans-serif; width: 100%;\" width=\"100%\"\n"
                + "                                        cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                                        <tr>\n"
                + "                                            <td class=\"sm-px-24\"\n"
                + "                                                style=\" background-color: #ffffff; background-color: rgba(255, 255, 255, 1); border-radius: 4px; font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; font-size: 14px; line-height: 24px; padding: 48px; text-align: left;  color: #626262; color: rgba(98, 98, 98, 1);\"\n"
                + "                                                bgcolor=\"rgba(255, 255, 255, 1)\" align=\"left\">\n"
                + "                                                <p style=\"font-weight: 600; font-size: 18px; margin-bottom: 0;\">Hey</p>\n"
                + "                                                <p\n"
                + "                                                    style=\"font-weight: 700; font-size: 20px; margin-top: 0;  color: #ff5850; color: rgba(255, 88, 80, 1);\">\n"
                + "                                                     " + fullName + "</p>\n"
                + "                                                <p style=\"margin: 0 0 24px;\">\n"
                + "                                                    A new request to\n"
                + "                                                    <span style=\"font-weight: 600;\">Hilfsmotor</span> Account -\n"
                + "                                                    <a href=\"mailto:john@example.com\" class=\"hover-underline\"\n"
                + "                                                        style=\" color: #7367f0; color: rgba(115, 103, 240, 1); text-decoration: none;\">" + emailSend + "</a>\n"
                + "                                                    (HF-" + accountID + ").\n"
                + "                                                </p>\n"
                + "                                                <p style=\"margin: 0 0 24px;\">Congratulation, you had been promoted to\n"
                + "                                                </p>\n"
                + "                                                <table style=\"font-family: 'Montserrat',Arial,sans-serif; margin:auto;\"\n"
                + "                                                    cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                                                    <tr>\n"
                + "                                                        <td style=\"mso-padding-alt: 16px 24px;  background-color: #7367f0; background-color: rgba(115, 103, 240, 1); border-radius: 4px; font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif;\"\n"
                + "                                                            bgcolor=\"rgba(115, 103, 240, 1)\">\n"
                + "                                                            <a href=\"#/\"\n"
                + "                                                                style=\"display: block; font-weight: 600; font-size: 14px; line-height: 100%; padding: 16px 24px;  color: #ffffff; color: rgba(255, 255, 255, 1); text-decoration: none;\">Role Permission: " + role + "</a>\n"
                + "                                                        </td>\n"
                + "                                                    </tr>\n"
                + "                                                </table>\n"
                + "                                                <p style=\"margin: 24px 0;\">\n"
                + "                                                    <span style=\"font-weight: 600;\">Message:</span> You can now access to more information with permission of role. We looking to working with you.\n"
                + "                                                </p>\n"
                + "                                                <p style=\"margin: 0;\">\n"
                + "                                                    If you did not think that's we did right or something mistake. You can contact to us.\n"
                + "                                                    <a href=\"mailto:support@example.com\" class=\"hover-underline\"\n"
                + "                                                        style=\"color: #7367f0; color: rgba(115, 103, 240, 1); text-decoration: none;\">hilfsmotor@example.com</a>\n"
                + "                                                </p>\n"
                + "                                                <table style=\"font-family: 'Montserrat',Arial,sans-serif; width: 100%;\"\n"
                + "                                                    width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n"
                + "                                                    <tr>\n"
                + "                                                        <td\n"
                + "                                                            style=\"font-family: 'Montserrat',Arial,sans-serif; padding-top: 32px; padding-bottom: 32px;\">\n"
                + "                                                            <div\n"
                + "                                                                style=\"background-color: #eceff1; background-color: rgba(236, 239, 241, 1); height: 1px; line-height: 1px;\">\n"
                + "                                                                &zwnj;</div>\n"
                + "                                                        </td>\n"
                + "                                                    </tr>\n"
                + "                                                </table>\n"
                + "                                                <p style=\"margin: 0 0 16px;\">\n"
                + "                                                    Not sure why you received this email? Please\n"
                + "                                                    <a href=\"mailto:support@example.com\" class=\"hover-underline\"\n"
                + "                                                        style=\"color: #7367f0; color: rgba(115, 103, 240, 1); text-decoration: none;\">let\n"
                + "                                                        us know</a>.\n"
                + "                                                </p>\n"
                + "                                                <p style=\"margin: 0 0 16px;\">Thanks, <br>The Hilfsmotor Team</p>\n"
                + "                                            </td>\n"
                + "                                        </tr>\n"
                + "                                        <tr>\n"
                + "                                            <td style=\"font-family: 'Montserrat',Arial,sans-serif; height: 20px;\"\n"
                + "                                                height=\"20\"></td>\n"
                + "                                        </tr>\n"
                + "                                        <tr>\n"
                + "                                            <td style=\"font-family: 'Montserrat',Arial,sans-serif; height: 16px;\"\n"
                + "                                                height=\"16\"></td>\n"
                + "                                        </tr>\n"
                + "                                    </table>\n"
                + "                                </td>\n"
                + "                            </tr>\n"
                + "                        </table>\n"
                + "                    </td>\n"
                + "                </tr>\n"
                + "            </table>\n"
                + "        </div>\n"
                + "    </div>";
        return email;
    }

    private void listPermission(HttpSession session) {
        LinkedList<PermissionsDetail> permissionList = PermissionDetailDB.listPermission(sqlQuery);
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        if (permissionList != null) {
            for (PermissionsDetail permissionsDetail : permissionList) {
                String[] splitEvent = permissionsDetail.getCreateDate().split("\\.");
                LocalDateTime local = LocalDateTime.parse(splitEvent[0], dateFormat);
                String timeSet = local.getHour() >= 12 ? "PM" : "AM";
                String localMonth = local.getMonth().toString().subSequence(0, 1)
                        + local.getMonth().toString().substring(1, 3).toLowerCase();
                String hour = local.getHour() < 10 ? "0" + local.getHour() : String.valueOf(local.getHour());
                String minute = local.getMinute() < 10 ? "0" + local.getMinute() : String.valueOf(local.getMinute());

                String localTimeModifier = local.getDayOfMonth() + " " + localMonth + " " + local.getYear() + ", " + hour + ":" + minute + " " + timeSet;
                permissionsDetail.setCreateDate(localTimeModifier);
            }
            session.setAttribute("permissionList", permissionList);
        }
    }

    private void viewRole(HttpServletRequest request, PrintWriter out) {
        int id = Integer.parseInt(request.getParameter("accountID"));
        Account account = Manager.searchAccountID(id);

        if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
            Staff staff = StaffDB.searchAccountID(id);
            out.print(staff.getPermissionDetails());
        } else {
            User user = UserDB.searchAccountID(id);
            out.print(user.getPermissionDetails());
        }
    }

    private void updateRole(HttpServletRequest request, PrintWriter out) {
        int id = Integer.parseInt(request.getParameter("accountID"));
        int status = 0;
        String roleSelected = request.getParameter("roleSeleceted");
        String allowedChanged = request.getParameter("allowedChanged");

        Account account = Manager.searchAccountID(id);
        String to = "", fullName = "", email = "", subject = "";

        if (account.getRole().equalsIgnoreCase("ADMIN") || account.getRole().equalsIgnoreCase("STAFF")) {
            Staff staff = StaffDB.searchAccountID(id);

            if (allowedChanged.equalsIgnoreCase("true")) {
                UserDB.saveAccount(staff.getFirstName(), staff.getLastName(), staff.getPhoneNumber(), staff.getEmail(), staff.getAccountID(), StaffDB.getImageBytes(staff.getAccountID()), staff.getCity(), staff.getStreet(), staff.getState());
                Manager.updateRole(roleSelected.toUpperCase(), staff.getAccountID(), "UPDATE ACCOUNT SET ROLE = ? WHERE ACCOUNT_ID = ?");
                User user = UserDB.searchAccountID(staff.getAccountID());
                String staffFirstName = staff.getFirstName().substring(0, 1) + staff.getFirstName().substring(1).toLowerCase();
                String staffLastName = staff.getLastName().substring(0, 1) + staff.getLastName().substring(1).toLowerCase();
                if (ProductDB.countInformations("SELECT COUNT(*) FROM PRODUCTS WHERE STAFF_ID = ?", staff.getStaffID()) > 0) {
                    fullName = staffFirstName + " " + staffLastName;
                    UserDB.insertStore(user.getCustomerID(), fullName, staff.getPhoneNumber(), staff.getEmail(), staff.getCity(), staff.getStreet(), staff.getState(), staff.getRole());
                    ProductDB.convertDataProduct(user.getCustomerID(), staff.getStaffID(), "UPDATE PRODUCTS SET STORE_ID = ?, STAFF_ID = NULL WHERE STAFF_ID = ?");
                    ProductDB.convertDataProduct(user.getCustomerID(), staff.getStaffID(), "UPDATE PRODUCTS_STOCK SET STORE_ID = ?, STAFF_ID = NULL WHERE STAFF_ID = ?");
                    ProductDB.convertDataProduct(user.getCustomerID(), staff.getStaffID(), "UPDATE PRODUCTS_RATING SET STORE_ID = ?, STAFF_ID = NULL WHERE STAFF_ID = ?");
                    ProductDB.convertDataProduct(user.getCustomerID(), staff.getStaffID(), "UPDATE PRODUCTS_REVIEW SET STORE_ID = ?, STAFF_ID = NULL WHERE STAFF_ID = ?");
                }
                StaffDB.deleteStaff(staff.getAccountID());
                status = UserDB.setPermissionDetails(user.getAccountID(), roleSelected);
                UserDB.setPermission(user.getAccountID(), user.getRole());
                subject = "Account Demote";
                to = user.getEmail();
                email = user.getEmail();
                fullName = user.getFirstName() + " " + user.getLastName();
            } else {
                status = StaffDB.setPermissionDetails(id, roleSelected);
                to = staff.getEmail();
                email = staff.getEmail();
                fullName = staff.getFirstName() + " " + staff.getLastName();
                subject = "Account Promted";
            }
        } else {
            User user = UserDB.searchAccountID(id);
            if (allowedChanged.equalsIgnoreCase("true")) {
                String roleModfier;
                if (roleSelected.equalsIgnoreCase("Administrator")) {
                    roleModfier = "ADMIN";
                } else {
                    roleModfier = "STAFF";
                }
                Staff newStaff = new Staff();
                String userFirstName = user.getFirstName().substring(0, 1) + user.getFirstName().substring(1).toLowerCase();
                String userLastName = user.getLastName().substring(0, 1) + user.getLastName().substring(1).toLowerCase();
                newStaff.setAccountID(user.getAccountID());
                newStaff.setEmail(user.getEmail());
                newStaff.setUserName(userFirstName + " " + userLastName);
                newStaff.setFirstName(user.getFirstName());
                newStaff.setLastName(user.getLastName());
                newStaff.setPhoneNumber(user.getPhoneNumber());
                newStaff.setCity(user.getCity());
                newStaff.setStreet(user.getStreet());
                newStaff.setState(user.getState());
                StaffDB.saveStaff(newStaff);
                StaffDB.uploadStaffImage(user.getAccountID(), UserDB.getImageBytes(user.getAccountID()));
                Manager.updateRole(roleModfier, user.getAccountID(), "UPDATE ACCOUNT SET ROLE = ? WHERE ACCOUNT_ID = ?");
                Staff staff = StaffDB.searchAccountID(user.getAccountID());
                if (ProductDB.countInformations("SELECT COUNT(*) FROM PRODUCTS WHERE STORE_ID = ?", user.getCustomerID()) > 0) {
                    ProductDB.convertDataProduct(staff.getStaffID(), user.getCustomerID(), "UPDATE PRODUCTS SET STORE_ID = NULL, STAFF_ID = ? WHERE STORE_ID = ?");
                    ProductDB.convertDataProduct(staff.getStaffID(), user.getCustomerID(), "UPDATE PRODUCTS_STOCK SET STORE_ID = NULL, STAFF_ID = ? WHERE STORE_ID = ?");
                    ProductDB.convertDataProduct(staff.getStaffID(), user.getCustomerID(), "UPDATE PRODUCTS_RATING SET STORE_ID = NULL, STAFF_ID = ? WHERE STORE_ID = ?");
                    ProductDB.convertDataProduct(staff.getStaffID(), user.getCustomerID(), "UPDATE PRODUCTS_REVIEW SET STORE_ID = NULL, STAFF_ID = ? WHERE STORE_ID = ?");
                    UserDB.deleteStore(user.getCustomerID());
                }
                UserDB.deleteCustomer(user.getCustomerID());
                status = StaffDB.setPermissionDetails(id, roleSelected);
                StaffDB.setPermission(staff.getAccountID(), roleModfier);
                subject = "Account Promted";
                to = staff.getEmail();
                email = staff.getEmail();
                fullName = staff.getFirstName() + " " + staff.getLastName();
            } else {
                status = UserDB.setPermissionDetails(id, roleSelected);
                to = user.getEmail();
                email = user.getEmail();
                fullName = user.getFirstName() + " " + user.getLastName();
                subject = "Account Promted";
            }
        }
        if (status > 0) {
            String messageAlt = emailHTMLUpdate(fullName, email, account.getAccountID(), roleSelected);
            MailController.send(to, subject, messageAlt, "lifeofcoffie@gmail.com", "shgwxqwapkgwzqkk");
            out.print("SUCCESS");
        } else {
            out.print("FAILED");
        }
    }
}
