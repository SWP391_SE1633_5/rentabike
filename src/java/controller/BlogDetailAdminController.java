/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.google.gson.Gson;
import dao.BlogDao;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import model.Account;
import model.Blog;

/**
 *
 * @author win
 */
@WebServlet(name="BlogDetailControl", urlPatterns={"/blog-detail-admin"})
public class BlogDetailAdminController extends HttpServlet {
   
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            response.sendRedirect("started.jsp");
            return;
        }
        if(!a.getRole().equals("ADMIN")){
            response.sendRedirect("app-product-shop.jsp");
            return;
        }
        String id_raw = request.getParameter("id");
        String type = request.getParameter("type");
        if(type==null || type.equals("")){
            request.getRequestDispatcher("BlogDetailControl.html").forward(request, response);
            return;
        }
        HashMap<Object, Object> mapTmpBlog = new HashMap<>();
        int id = Integer.parseInt(id_raw);
        Blog b = BlogDao.adminGetBlogByBlogId(id);
        if(b==null){
            //Xu li sau
            return;
        }
        
        //blog
        if(UserDB.searchAccountID(b.getAccountId())!=null){
            mapTmpBlog.put("author", UserDB.searchAccountID(b.getAccountId()));
        } else if(StaffDB.searchAccountID(b.getAccountId())!=null){
            mapTmpBlog.put("author", StaffDB.searchAccountID(b.getAccountId()));
        }
        mapTmpBlog.put("blog", b);
        map.put("BlogData", mapTmpBlog);

        map.put("SC", 1);
        out.print(gson.toJson(map));
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
