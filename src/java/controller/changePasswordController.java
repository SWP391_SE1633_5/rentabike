/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this templates
 */
package controller;
import com.google.gson.Gson;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import model.Account;

/**
 *
 * @author win
 */
@WebServlet(name = "ChangePassword", urlPatterns = {"/change-password"})
public class changePasswordController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HashMap<Object, Object> map = new HashMap<>();
        Gson gson = new Gson();
        String email = request.getParameter("getEmail");
        if(email==null){
            request.getRequestDispatcher("ChangePassword.html").forward(request, response);
        }
        if(!email.equals("")){
            HttpSession session = request.getSession();
            Account a = (Account)session.getAttribute("account");
            map.put("email", a.getEmail());
            out.print(gson.toJson(map));
            out.flush();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        HashMap<Object, Object> map = new HashMap<>();
        Gson gson = new Gson();
        UserDao ud = new UserDao();

        String email = request.getParameter("email");
        String oldPassword = request.getParameter("oldPassword");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");

        //check oladpassword
        if (!ud.findAccountByEmail(email).getPassword().equals(oldPassword)) {
            map.put("SC", -1);
            map.put("Mess", "Wrong old password!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        //check confirm
        if (!password.equals(confirmPassword)) {
            map.put("SC", -1);
            map.put("Mess", "Two password doesn't match another!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        session.removeAttribute("account");
        session.removeAttribute("user");
        ud.changePassword(password, email);
        map.put("SC", 0);
        map.put("Mess", "Reset password successfully!");
        out.print(gson.toJson(map));
        out.flush();
        return;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
