/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.ProductDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import model.Product;
import model.Store;

/**
 *
 * @author baqua
 */
public class requestAddProductController extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        int storeID = Integer.parseInt(session.getAttribute("storeID").toString());
        String avatarImage = request.getParameter("avatarImage");
        String name = request.getParameter("productName");
        String category = request.getParameter("productCategory");
        String brand = request.getParameter("productBrand");
        String modelyear = request.getParameter("modelYear");
        String Price = request.getParameter("price");
        String Displacement = request.getParameter("displacement");
        String Transmission = request.getParameter("productTransmission");
        String Starter = request.getParameter("productStarter");
        String FuelCapacity = request.getParameter("fuelCapacity");
        String FuelSystem = request.getParameter("FuelSystem");
        String DryWeight = request.getParameter("dryWeight");
        String SeatHeight = request.getParameter("seatHeight");
        String Mode = request.getParameter("productMode");
        String HireMode = request.getParameter("productHireMode");
        String Description = request.getParameter("description");
        Product p = new Product(name, brand, category, Integer.valueOf(modelyear), Float.valueOf(Price), Mode, Integer.valueOf(Displacement), Transmission, Starter, FuelSystem, Float.valueOf(FuelCapacity), Float.valueOf(DryWeight), Float.valueOf(SeatHeight), HireMode, Description, storeID);
        ProductDB dao = new ProductDB();
        p.setProductID(dao.getNumberOfProduct() + 1);
        dao.saveProduct1(p);
        dao.saveProductSpecifications(p);
        dao.insertProductImage(p);
        int status1 = 0;
        if ("".equals(avatarImage) || avatarImage.length() == 0) {
            status1 = ProductDB.updateProductImgae(p, null);
        } else {
            status1 = ProductDB.updateProductImgae(p, getBytesFromString(avatarImage));
        }
        response.sendRedirect("user-store?storeID=" + storeID);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private byte[] getBytesFromString(String avatar) {
        byte[] byteArray = new byte[avatar.length()];
        String[] splitAvatar = avatar.split(",");

        for (int i = 0; i < splitAvatar.length; i++) {
            byteArray[i] = (byte) Integer.parseInt(splitAvatar[i]);
        }
        return byteArray;
    }
}
