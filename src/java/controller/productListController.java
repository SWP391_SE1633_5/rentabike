/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.Manager;
import dao.ProductDB;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.LinkedList;
import model.Account;
import model.Product;
import model.Staff;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "productListController", urlPatterns = "/productListController")
public class productListController extends HttpServlet {

    private static final String sqlQuery = "SELECT P.*, [PI].IMAGE, [PS].DISPLACEMENT, [PS].TRANSMISSION, [PS].STARTER, [PS].FUEL_SYSTEM, [PS].FUEL_CAPACITY, [PS].DRY_WEIGHT, [PS].SEAT_HEIGHT\n"
            + "FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS_SPECIFICATIONS AS [PS]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.PRODUCT_ID = [PS].PRODUCT_ID AND P.STATUS != 4";
    private static final String searchQuery = "SELECT DISTINCT P.*, [PI].IMAGE FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI] WHERE P.PRODUCT_ID = [PI].PRODUCT_ID\n"
            + "AND(P.PRODUCT_NAME LIKE ? OR P.BRAND_NAME LIKE ? OR P.CATEGORY_NAME LIKE ? OR P.MODEL_YEAR LIKE ? OR P.MODE LIKE ? OR P.MODEL_YEAR LIKE ?) AND P.STATUS != 4";
    private static final String searchBrCtegory = "SELECT DISTINCT P.*, [PI].IMAGE FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI] WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.STATUS != 4";
    private static final String sortFeature = "SELECT P.*, [PI].IMAGE, [PS].DISPLACEMENT, [PS].TRANSMISSION, [PS].STARTER, [PS].FUEL_SYSTEM, [PS].FUEL_CAPACITY, [PS].DRY_WEIGHT, [PS].SEAT_HEIGHT\n"
            + "FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS_SPECIFICATIONS AS [PS]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.PRODUCT_ID = [PS].PRODUCT_ID AND P.STATUS != 4";
    private static final String multiRange = "SELECT P.*, [PI].IMAGE, [PS].DISPLACEMENT, [PS].TRANSMISSION, [PS].STARTER, [PS].FUEL_SYSTEM, [PS].FUEL_CAPACITY, [PS].DRY_WEIGHT, [PS].SEAT_HEIGHT\n"
            + "FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS_SPECIFICATIONS AS [PS]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.PRODUCT_ID = [PS].PRODUCT_ID AND P.STATUS != 4";
    private static final String staffQuery = "SELECT DISTINCT SF.[USER_NAME] FROM PRODUCTS AS P, STAFFS AS SF WHERE P.STAFF_ID = ?";
    private static final String storeQuery = "SELECT DISTINCT ST.[STORE_NAME] FROM PRODUCTS AS P, STORES AS ST WHERE P.STORE_ID = ?";
    private static final String rentalQuery = "SELECT PRD.MODE FROM PRODUCTS_RENTAL_DETAILS AS PRD, PRODUCTS AS P WHERE P.MODE = 'Rental' AND ? = PRD.PRODUCT_ID AND P.STATUS != 4";
    private static final String totalRating = "SELECT COUNT(*) AS TOTAL_RATING_COUNT FROM PRODUCTS_RATING WHERE PRODUCT_ID = ?";
    private static final String averageRating = "SELECT AVG(RATING) AS TOTAL_RATING FROM PRODUCTS_RATING WHERE PRODUCT_ID = ?";
    private static final String ratingQuery = "SELECT PR.RATING, COUNT(PR.RATING) AS TOTAL FROM PRODUCTS_RATING AS PR, PRODUCTS AS P WHERE P.STATUS != 4 AND PR.PRODUCT_ID = P.PRODUCT_ID GROUP BY PR.RATING ORDER BY PR.RATING DESC";
    private static final String modeQuery = "SELECT DISTINCT P.MODE, COUNT(P.MODE) AS COUNTNUMBER FROM PRODUCTS AS P WHERE STATUS != 4 GROUP BY P.MODE ORDER BY P.MODE ASC";
    private static final String brandQuery = "SELECT BRAND_NAME, COUNT(BRAND_NAME) AS NUMBER FROM PRODUCTS WHERE STATUS != 4 GROUP BY BRAND_NAME ORDER BY BRAND_NAME ASC";
    private static final String categoryQuery = "SELECT CATEGORY_NAME, COUNT(CATEGORY_NAME) AS NUMBER FROM PRODUCTS WHERE STATUS != 4 GROUP BY CATEGORY_NAME ORDER BY CATEGORY_NAME ASC";
    private static final String stockQuery = "SELECT QUANTITY FROM PRODUCTS_STOCK WHERE PRODUCT_ID = ?";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        LinkedList<Product> productList = ProductDB.listProduct(sqlQuery);
        Account account = (Account) session.getAttribute("account");
        if (account != null) {
            //Loop to get Category, Brand, All, Star
            HashMap<String, Integer> ratingList = ProductDB.countProductTypeIndividual(ratingQuery);
            session.setAttribute("ratingList", ratingList);

            HashMap<String, Integer> modeList = ProductDB.countProductTypeIndividual(modeQuery);
            session.setAttribute("modeList", modeList);

            int countTotalProduct = ProductDB.countProduct("SELECT COUNT(*) FROM PRODUCTS WHERE STATUS != 4");
            session.setAttribute("totalProduct", countTotalProduct);

            HashMap<String, Integer> brandList = ProductDB.countProductTypeIndividual(brandQuery);
            session.setAttribute("brandList", brandList);

            HashMap<String, Integer> categoryList = ProductDB.countProductTypeIndividual(categoryQuery);
            session.setAttribute("categoryList", categoryList);
            //Set up product show pagination        
            int begin = 0;
            int end = 8;
            //Get total Pagination
            int numberPaginaton = 0;
            LinkedList<Integer> numberPaginationList = new LinkedList<>();
            if (!productList.isEmpty()) {
                double showTotalPage = (double) end + 1.0;
                numberPaginaton = (int) Math.ceil(productList.size() / showTotalPage);
                for (int i = 0; i < numberPaginaton; i++) {
                    numberPaginationList.add(i + 1);
                }
            }
            session.setAttribute("numberPaginationList", numberPaginationList);
            response.sendRedirect("app-product-shop.jsp");
        } else {
            response.sendRedirect("started.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String mode = request.getParameter("mode");
        switch (mode) {
            case "pagePagination":
                paginationPage(request, out);
                break;
            case "searchProduct":
                searchProduct(request, out);
                break;
            case "sortOption":
                sortProduct(request, out);
                break;
            case "multiRange":
                multiRange(request, out);
                break;
            case "priceSlider":
                priceSlider(request, out);
                break;
            case "filterNavbar":
                filterNavbar(request, out);
                break;
            case "wishListHandle":
                handleWishList(request, out);
                break;
            case "addToCheckOut":
                handleCheckOut(request, out);
                break;
        }
    }

    private void paginationPage(HttpServletRequest request, PrintWriter out) {
        int pagination = Integer.parseInt(request.getParameter("page"));
        int nextPage = 9;
        int totalPage = pagination * nextPage;
        int indexPage = totalPage - 9;
        LinkedList<Product> products = ProductDB.listProduct(sqlQuery);
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        for (int i = indexPage; i < totalPage; i++) {
            if (i < products.size()) {
                String staffName = ProductDB.searchPersonProduct(products.get(i).getStaffID(), staffQuery);
                String storeName = ProductDB.searchPersonProduct(products.get(i).getStoreID(), storeQuery);
                if (staffName != null) {
                    products.get(i).setStaffName(staffName);

                } else {
                    products.get(i).setStoreName(storeName);
                }
                if (products.get(i).getMode().equalsIgnoreCase("Rental")) {
                    String priceMode = ProductDB.searchPersonProduct(products.get(i).getProductID(), rentalQuery);
                    products.get(i).setModeRental(priceMode);
                }
                products.get(i).setTotalRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), totalRating));
                products.get(i).setAverageRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), averageRating));
                products.get(i).setMarked(ProductDB.searchWishList(account.getAccountID(), products.get(i).getProductID()));
                products.get(i).setMarkedCheckOut(ProductDB.searchCheckOut(account.getAccountID(), products.get(i).getProductID()));
                out.print(htmlProductList(products.get(i)));
            }
        }
    }

    private void sortProduct(HttpServletRequest request, PrintWriter out) {
        String option = request.getParameter("option");
        LinkedList<Product> products;
        int limitedSize = 0;
        String copySqlQuery = sortFeature;

        if (option.equalsIgnoreCase("Highest Price")) {
            products = ProductDB.listProduct(copySqlQuery + " ORDER BY P.LIST_PRICE DESC");
        } else if (option.equalsIgnoreCase("Lowest Price")) {
            products = ProductDB.listProduct(copySqlQuery + " ORDER BY P.LIST_PRICE ASC");
        } else if (option.equalsIgnoreCase("Newest")) {
            products = ProductDB.listProduct(copySqlQuery + " ORDER BY P.CREATE_AT ASC");
        } else if (option.equalsIgnoreCase("Oldest")) {
            products = ProductDB.listProduct(copySqlQuery + " ORDER BY P.CREATE_AT DESC");
        } else if (option.equalsIgnoreCase("Popular")) {
            products = ProductDB.listProduct(sqlQuery);
        } else {
            products = ProductDB.listProduct(sqlQuery);
        }
        if (products == null || products.isEmpty()) {
            out.print("OUT");
            return;
        }
        displayProductsFilter(products, limitedSize, option, out, request);
    }

    private void multiRange(HttpServletRequest request, PrintWriter out) {
        String option = request.getParameter("choice");
        String copySqlQuery = multiRange;
        LinkedList<Product> products;
        int limitedSize = 0;
        if (option.equalsIgnoreCase("=<10")) {
            products = ProductDB.listProduct(copySqlQuery + " AND P.LIST_PRICE <= 10");
        } else if (option.equalsIgnoreCase("10-100")) {
            products = ProductDB.listProduct(copySqlQuery + " AND P.LIST_PRICE BETWEEN 10 AND 100");
        } else if (option.equalsIgnoreCase("100-500")) {
            products = ProductDB.listProduct(copySqlQuery + " AND P.LIST_PRICE BETWEEN 100 AND 500");
        } else if (option.equalsIgnoreCase(">=500")) {
            products = ProductDB.listProduct(copySqlQuery + " AND P.LIST_PRICE >= 500");
        } else {
            products = ProductDB.listProduct(sqlQuery);
        }
        if (products == null || products.isEmpty()) {
            out.print("OUT");
            return;
        }
        displayProductsFilter(products, limitedSize, option, out, request);
    }

    private void filterNavbar(HttpServletRequest request, PrintWriter out) {
        String option = request.getParameter("selectOption");
        String modeRequest = request.getParameter("request");
        String copySqlQuery = searchBrCtegory;
        LinkedList<Product> products = new LinkedList<>();
        int limitedSize = 0;
        if (option.equalsIgnoreCase("All")) {
            products = ProductDB.listProduct(sqlQuery);
        } else {
            if (modeRequest.equalsIgnoreCase("Category")) {
                products = ProductDB.listProductFilter(copySqlQuery + " AND P.CATEGORY_NAME = ?", option);
            } else if (modeRequest.equalsIgnoreCase("Brand")) {
                products = ProductDB.listProductFilter(copySqlQuery + " AND P.BRAND_NAME = ?", option);
            } else if (modeRequest.equalsIgnoreCase("Mode")) {
                products = ProductDB.listProductFilter(copySqlQuery + " AND P.MODE = ?", option);
            }
        }
        if (products == null || products.isEmpty()) {
            out.print("OUT");
            return;
        }
        displayProductsFilter(products, limitedSize, option, out, request);
    }

    private void priceSlider(HttpServletRequest request, PrintWriter out) {
        int minValue = Integer.parseInt(request.getParameter("minValue"));
        int maxValue = Integer.parseInt(request.getParameter("maxValue"));

        String copySqlQuery = multiRange;
        LinkedList<Product> products;
        int limitedSize = 0;
        products = ProductDB.listProductPriceSlider((copySqlQuery + " AND P.LIST_PRICE BETWEEN ? AND ?"), minValue, maxValue);
        if (products == null || products.isEmpty()) {
            out.print("OUT");
            return;
        }
        displayProductsFilter(products, limitedSize, "sliderPrice", out, request);
    }

    private void searchProduct(HttpServletRequest request, PrintWriter out) {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        String searchValue = request.getParameter("inputSearch");
        LinkedList<Product> products;
        if (searchValue.length() == 0 || "".equals(searchValue)) {
            products = ProductDB.listProduct(sqlQuery);
        } else {
            products = ProductDB.listProductSearch(searchQuery, searchValue);
        }
        if (!products.isEmpty()) {
            for (int i = 0; i < products.size(); i++) {
                String staffName = ProductDB.searchPersonProduct(products.get(i).getStaffID(), staffQuery);
                String storeName = ProductDB.searchPersonProduct(products.get(i).getStoreID(), storeQuery);
                if (staffName != null) {
                    products.get(i).setStaffName(staffName);
                } else {
                    products.get(i).setStoreName(storeName);
                }
                if (products.get(i).getMode().equalsIgnoreCase("Rental")) {
                    String priceMode = ProductDB.searchPersonProduct(products.get(i).getProductID(), rentalQuery);
                    products.get(i).setModeRental(priceMode);
                }
                products.get(i).setTotalRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), totalRating));
                products.get(i).setAverageRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), averageRating));
                products.get(i).setMarked(ProductDB.searchWishList(account.getAccountID(), products.get(i).getProductID()));
                products.get(i).setMarkedCheckOut(ProductDB.searchCheckOut(account.getAccountID(), products.get(i).getProductID()));
                out.print(htmlProductList(products.get(i)));
            }
        }
    }

    private void displayProductsFilter(LinkedList<Product> products, int limitedSize, String option, PrintWriter out, HttpServletRequest request) {
        if (option.equalsIgnoreCase("All")) {
            limitedSize = 9;
        } else {
            limitedSize = products.size();
        }
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        for (int i = 0; i < limitedSize; i++) {
            String staffName = ProductDB.searchPersonProduct(products.get(i).getStaffID(), staffQuery);
            String storeName = ProductDB.searchPersonProduct(products.get(i).getStoreID(), storeQuery);
            if (staffName != null) {
                products.get(i).setStaffName(staffName);
            } else {
                products.get(i).setStoreName(storeName);
            }
            if (products.get(i).getMode().equalsIgnoreCase("Rental")) {
                String priceMode = ProductDB.searchPersonProduct(products.get(i).getProductID(), rentalQuery);
                products.get(i).setModeRental(priceMode);
            }
            products.get(i).setTotalRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), totalRating));
            products.get(i).setAverageRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), averageRating));
            products.get(i).setMarked(ProductDB.searchWishList(account.getAccountID(), products.get(i).getProductID()));
            products.get(i).setMarkedCheckOut(ProductDB.searchCheckOut(account.getAccountID(), products.get(i).getProductID()));
            out.print(htmlProductList(products.get(i)));
        }
    }

    private void handleWishList(HttpServletRequest request, PrintWriter out) {
        int productID = Integer.parseInt(request.getParameter("productID"));
        String email = request.getParameter("email");
        String modeSwitch = request.getParameter("modeSwitch");
        Product productName = ProductDB.searchProduct("SELECT P.* FROM PRODUCTS AS P WHERE P.PRODUCT_ID = ?", productID);
        Account account = Manager.searchAccount(email);

        if (account != null) {
            if (modeSwitch.equalsIgnoreCase("addWishList")) {
                String sqlQuery = "INSERT INTO PRODUCTS__WISHLIST VALUES(?,?)";
                ProductDB.updateWishList(productID, account.getAccountID(), sqlQuery);
                Manager.activyAccountUpdate(account.getAccountID(), "Add Wishlist", "You add a product " + productName.getName() + " from wishlist");
            } else {
                String sqlQuery = "DELETE FROM PRODUCTS__WISHLIST WHERE PRODUCT_ID = ? AND ACCOUNT_ID = ?";
                ProductDB.updateWishList(productID, account.getAccountID(), sqlQuery);
                Manager.activyAccountUpdate(account.getAccountID(), "Remove Wishlist", "You remove a product " + productName.getName() + " from wishlist");
            }
            out.print("SUCCESS");
        } else {
            out.print("FAILED");
        }
    }

    private String htmlProductList(Product product) {
        String wishListStatus = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-heart\"><path d=\"M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z\"></path></svg>\n";
        if (product.getMarked() != null) {
            wishListStatus = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-heart text-danger\"><path d=\"M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z\"></path></svg>\n";
        }

        int stockRemaining = ProductDB.countInformations(stockQuery, product.getProductID());
        String disabledAdd = "";
        String addToCart = "Add to cart";
        if (stockRemaining == 0) {
            disabledAdd = "disabled";
            addToCart = "Sold out";
        }
        String markedHref = "#/";
        String checkOutDisabled = "onclick=\"sendToCheckout(this)\"";
        if (product.getMarkedCheckOut() != null) {
            markedHref = "checkOutController";
            addToCart = "View checkout";
            disabledAdd = "";
            checkOutDisabled = "";
        }
        String starProduct = "";
        if (product.getAverageRating() != null) {
            switch (product.getAverageRating().toUpperCase()) {
                case "5":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
                case "4":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
                case "3":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
                case "2":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
                case "1":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
            }
        } else {
            starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "</ul>"
                    + "<span class=\"card-text\">(" + product.getTotalRating() + " Review)</span>\n";
        }

        String modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "</h6>";
        if (product.getModeRental() != null) {
            switch (product.getModeRental().toUpperCase()) {
                case "DAY":
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "/Day</h6>";
                    break;
                case "WEEK":
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "/Week</h6>";
                    break;
                case "MONTH":
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "/Month</h6>";
                    break;
                case "YEAR":
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "/Year</h6>";
                    break;
                default:
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "</h6>";
                    break;
            }
        }
        String userPost;
        if (product.getStaffName() != null) {
            userPost = "<div class=\"card-text mt-1\" style=\"margin-top:0.25rem\">Post By <span style=\"color: #7367F0;\" class=\"company-name\">" + product.getStaffName() + "</span></div>";
        } else {
            userPost = "<div class=\"card-text mt-1\" style=\"margin-top:0.25rem\">Post By <span style=\"color: #7367F0;\" class=\"company-name\">" + product.getStoreName() + "</span></div>";
        }

        String rentalMode = "";
        if (product.getMode() != null) {
            if ("Rental".equalsIgnoreCase(product.getMode())) {
                rentalMode = "<div class=\"lidget-card-ecomerce\">\n"
                        + "Rental Only\n"
                        + "</div>";
            }
        }

        int idStore = 0;
        if (product.getStoreName() != null) {
            idStore = product.getStoreID();
        } else {
            idStore = product.getStaffID();
        }

        String html = "<div class=\"card ecommerce-card position-relative\" data=\"" + product.getProductID() + "\">\n"
                + "<div class=\"item-img text-center\" style=\"padding-top:0;min-height:unset;\">\n"
                + "<a href=\"productDetailController?mx=" + product.getProductID() + "\">\n"
                + "<img class=\"img-fluid card-img-top\" src=\"" + product.getImage() + "\"\n"
                + "alt=\"img-placeholder\" /></a>\n"
                + "</div>\n"
                + "<div class=\"card-body\">\n"
                + "<div class=\"item-wrapper\">\n"
                + "<div class=\"item-rating flex-default flex-direction-column\" style=\"row-gap:0.5rem\">\n"
                + starProduct
                + "</div>\n"
                + "<div>\n"
                + modeRental
                + "</div>\n"
                + "</div>\n"
                + "<h6 class=\"item-name\">\n"
                + "<div class=\"text-muted\">Product ID: MX-00" + product.getProductID() + "</div>\n"
                + "<a class=\"text-body mt-1\" href=\"productDetailController?mx=" + product.getProductID() + "\">" + product.getName() + " " + product.getModelYear() + "</a>\n"
                + "<div class=\"card-text\" style=\"margin-top:0.25rem\">" + product.getCategory() + " Version</div>\n"
                + "<div class=\"card-text\" style=\"margin-top:0.25rem\">By \n"
                + "<div class=\"company-name\" style=\"display:inline-block\">" + product.getBrand() + "</div>\n"
                + "</div>\n"
                + userPost
                + "</h6>\n"
                + "<p class=\"card-text item-description\">\n"
                + "" + product.getDescription() + "\n"
                + "</p>\n"
                + "</div>\n"
                + "<div class=\"item-options text-center\">\n"
                + "<div class=\"item-wrapper\">\n"
                + "<div class=\"item-cost\">\n"
                + "<h4 class=\"item-price\">$" + product.getPrice() + "</h4>\n"
                + "</div>\n"
                + "</div>\n"
                + "<a href=\"#/\" class=\"btn btn-light btn-wishlist waves-effect waves-float waves-light\" index-data=\"" + product.getProductID() + "\" onclick=\"sendToWishList(this)\">\n"
                + wishListStatus
                + "<span>Wishlist</span>\n"
                + "</a>"
                + "<a href=\"" + markedHref + "\" class=\"btn btn-primary btn-cart waves-effect waves-float waves-light " + disabledAdd + "\" index-data=\"" + product.getProductID() + "\" index=\"" + idStore + "\" "+checkOutDisabled+">\n"
                + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-shopping-cart\"><circle cx=\"9\" cy=\"21\" r=\"1\"></circle><circle cx=\"20\" cy=\"21\" r=\"1\"></circle><path d=\"M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6\"></path></svg>\n"
                + "<span class=\"add-to-cart\">" + addToCart + "</span>\n"
                + "</a>"
                + "</div>\n"
                + rentalMode
                + "</div>";
        return html;
    }

    private void handleCheckOut(HttpServletRequest request, PrintWriter out) {
        int storeID = Integer.parseInt(request.getParameter("storeID"));
        int productID = Integer.parseInt(request.getParameter("productID"));
        Account account = (Account) request.getSession().getAttribute("account");

        int status = ProductDB.addToCheckOut(account.getAccountID(), storeID, productID);
        if (status > 0) {
            out.print("SUCCESS");
        } else {
            out.print("FAILED");
        }
    }
}
