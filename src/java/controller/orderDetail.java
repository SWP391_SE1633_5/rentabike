/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.OrderDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;
import model.OrderItem;

/**
 *
 * @author ASUS
 */
public class orderDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat newFormatter = new SimpleDateFormat("dd/MM/yyyy");
            OrderDB dao = new OrderDB();
            String id = request.getParameter("orderID");
            Order o = dao.getOrderById(id);
            dao.getCustomerInfoForOrder(o);
            session.setAttribute("order", o);
            List<OrderItem> oItemList = dao.getAllOrderItem(o);
            for (OrderItem orderItem : oItemList) {
                orderItem.setP(dao.getProductById(orderItem.getProductID()));
                Date newDate = null;
                try {
                    if(orderItem.getShippedDate() == null)
                    newDate = formatter.parse(orderItem.getOrderDate());
                    else newDate = formatter.parse(orderItem.getShippedDate());
                    Calendar c = Calendar.getInstance();
                    c.setTime(newDate);
                    if(orderItem.getShippedDate() == null)
                    c.add(Calendar.DATE, 7);
                    newDate = c.getTime();  
                    String newStr = newFormatter.format(newDate);
                    orderItem.setShippedDate(newStr);
                    newStr = newStr.substring(3, 5) + "-" + newStr.substring(0, 2) + "-" + newStr.substring(6, 10);
                    dao.updateOrderItemShippedDate(""+orderItem.getId(), newStr);
                    
                } catch (ParseException ex) {
                    Logger.getLogger(orderDetail.class.getName()).log(Level.SEVERE, null, ex);
                }
                dao.getStoreForOrderItem(orderItem);

            }
            session.setAttribute("orderItemList", oItemList);
            response.sendRedirect("OrderDetail.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
