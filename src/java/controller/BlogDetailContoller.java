/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.google.gson.Gson;
import dao.BlogAccountSaveDao;
import dao.BlogAccountVoteDao;
import dao.BlogCommentDao;
import dao.BlogDao;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import model.Account;
import model.Blog;
import model.BlogAccountVote;


/**
 *
 * @author win
 */
@WebServlet(name="BlogDetailContoller", urlPatterns={"/blog-detail"})
public class BlogDetailContoller extends HttpServlet {
   
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        String id_raw = request.getParameter("id");
        String type = request.getParameter("type");
        //first load
        if(type==null || type.equals("")){
            request.getRequestDispatcher("BlogDetail.html").forward(request, response);
            return;
        }
        PrintWriter out = response.getWriter();
        HashMap<Object, Object> mapTmpBlog = new HashMap<>();
        BlogDao bd = new BlogDao();
        int id = Integer.parseInt(id_raw);
        int accId=-1;
        if(a!=null) accId=a.getAccountID();
        Blog b = bd.getAuthenBlogbyBlogId(id, accId);
        if(b==null){
            //Xu li sau
            return;
        }
        
        //blog
        if(UserDB.searchAccountID(b.getAccountId())!=null){
            mapTmpBlog.put("author", UserDB.searchAccountID(b.getAccountId()));
        } else if(StaffDB.searchAccountID(b.getAccountId())!=null){
            mapTmpBlog.put("author", StaffDB.searchAccountID(b.getAccountId()));
        }
        mapTmpBlog.put("blog", b);
        //vote
        mapTmpBlog.put("vote", BlogAccountVoteDao.getVoteByBlogId(b.getId()));
        map.put("BlogData", mapTmpBlog);
        
        //authen
        
        if(a!=null){
            //info acc
            if(UserDB.searchAccountID(a.getAccountID())!=null){
                map.put("AuthenAcc", UserDB.searchAccountID(a.getAccountID()));
            } else if(StaffDB.searchAccountID(a.getAccountID())!=null){
                map.put("AuthenAcc", StaffDB.searchAccountID(a.getAccountID()));
            }
            //check Vote
            map.put("typeVoted", BlogAccountVoteDao.getBlogAccountVoteByBIdAndAId(b.getId(), a.getAccountID()));
            map.put("blogSave", BlogAccountSaveDao.getBlogAccountSaveByBlogIdAndAccId(b.getId(), a.getAccountID()));
        }
        
        //comment
        map.put("CmtLenght", BlogCommentDao.getQuanCmtByBlogId(b.getId()));
        
        map.put("SC", 1);
        out.print(gson.toJson(map));
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            map.put("SC", 0);
            map.put("ME", "Please login to do this action!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        String type = request.getParameter("type");
        
        //vote
        if(type.equals("vote")){
            int blogId = Integer.parseInt(request.getParameter("blogId"));
            int accountId = a.getAccountID();
            int reactType = Integer.parseInt(request.getParameter("reactType"));
            BlogAccountVote bav = BlogAccountVoteDao.getBlogAccountVoteByBIdAndAId(blogId, accountId);
            if(bav==null){
                BlogAccountVoteDao.insertBlogAccountVoteWithBIdAndAIdAndType(blogId, accountId, reactType);
            }
            else {
                if(bav.getType() == reactType){
                    BlogAccountVoteDao.deleteBlogAccountVoteByBIdAndAId(blogId, accountId);
                }
                if(bav.getType() != reactType){
                    BlogAccountVoteDao.updateBlogAccountVoteByBIdAndAIdAndType(blogId, accountId, reactType);
                }
            }
            //!=null

            map.put("SC", 1);
            map.put("ME", "Oke");
            map.put("Num", BlogAccountVoteDao.getVoteByBlogId(blogId));
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        //save
        else if(type.equals("save")){
            int blogId = Integer.parseInt(request.getParameter("blogId"));
            BlogAccountSaveDao.insertBlogAccountSaveWithBlogIdAndAccId(blogId, a.getAccountID());
            map.put("SC", 1);
            map.put("ME", "Save successfully!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        //unsave
        else if(type.equals("unsave")){
            int blogId = Integer.parseInt(request.getParameter("blogId"));
            BlogAccountSaveDao.DeleteBlogAccountSaveWithBlogIdAndAccId(blogId, a.getAccountID());
            map.put("SC", 1);
            map.put("ME", "Unsave successfully!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
