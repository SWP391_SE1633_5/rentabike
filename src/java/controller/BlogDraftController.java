/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.google.gson.Gson;
import dao.BlogDao;
import dao.BlogTagDao;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import model.Account;
import model.Blog;
import model.BlogTag;

/**
 *
 * @author win
 */
@WebServlet(name="BlogDraftController", urlPatterns={"/blog-draft"})
public class BlogDraftController extends HttpServlet {
   
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        map.clear();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            response.sendRedirect("started.jsp");
            return;
        }
        String type = request.getParameter("type");
        //first load
        if(type==null || type.equals("")){
            request.getRequestDispatcher("BlogDraft.html").forward(request, response);
            return;
        }
        PrintWriter out = response.getWriter();
        if(type.equals("getAll")){
            int accId = a.getAccountID();
            List<Blog> blogList = BlogDao.getUnAuthenBlogListByAcccountId(accId);
            if(UserDB.searchAccountID(accId)!=null){
                map.put("Account", UserDB.searchAccountID(accId));
            } else if(StaffDB.searchAccountID(accId)!=null){
                map.put("Account", StaffDB.searchAccountID(accId));
            }
            map.put("BlogList", blogList);
            map.put("SC", 1);
            out.print(gson.toJson(map));
            out.flush();
        }

    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        map.clear();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        
        String type = request.getParameter("type");
        if(type.equals("update")){
            
        }
        if(type.equals("delete")){
            
        }
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
