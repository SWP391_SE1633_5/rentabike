/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import static controller.verifyAccountController.generateNewToken;
import dao.Manager;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import model.Account;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "forgotPasswordController", urlPatterns = "/forgotPasswordController")
public class forgotPasswordController extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String mode = request.getParameter("switchMode");
        PrintWriter out = response.getWriter();
        Account account = Manager.searchAccount(email);
        HttpSession session = request.getSession();
        
        if(mode == null){
            mode = "default";
        }
        switch (mode) {
            case "sendEmail":
                if (account != null) {
                    User user = UserDB.searchAccountID(account.getAccountID());
                    String to = account.getEmail();
                    String subject = "Change Password";
                    String tokenizer = generateNewToken();
                    String message = emailHtml(tokenizer, user.getLastName());

                    UserDB.createToken(tokenizer, account.getAccountID(), email);
                    MailController.send(to, subject, message, "lifeofcoffie@gmail.com", "shgwxqwapkgwzqkk");
                    out.print("SUCCESS");
                } else {
                    out.print("FAILED");
                }
                break;
            case "changedPassword":
                String tokenizer = (String)session.getAttribute("tokenizerTemp");
                String emailToken = UserDB.searchToken(tokenizer);
                String password = request.getParameter("password");
                int status = UserDB.changedPassword(emailToken, password);
                if (status > 0) {
                    session.setAttribute("changed", "process");
                    session.removeAttribute("tokenizerTemp");
                    UserDB.deleteToken(tokenizer);
                    out.print("changedSuccess");
                } else {
                    out.print("changedFailed");
                }
                break;
            default:
                String token = request.getParameter("tokenizer");
                String emailLocal = UserDB.searchToken(token);
                if (emailLocal != null) {
                    Clock clock = Clock.systemDefaultZone();
                    LocalDateTime currentDate = LocalDateTime.now(clock).withNano(0);
                    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    String sqlQuery = "SELECT [EXPIRED] FROM TOKEN_ACCOUNT WHERE [TOKEN] = ?";

                    String[] split = UserDB.checkTokenExpired(token, sqlQuery).split("\\.");
                    LocalDateTime local = LocalDateTime.parse(split[0], dateFormat);
                    LocalDateTime tempDateTime = LocalDateTime.from(local).withNano(0);
                    long dayDistance = tempDateTime.until(currentDate, ChronoUnit.DAYS);
                    if (dayDistance < 1) {
                        session.setAttribute("changed", "process");
                        session.setAttribute("tokenizerTemp", token);
                    } else {
                        session.setAttribute("changed", "done");
                    }
                } else {
                    session.setAttribute("changed", "done");
                }
                response.sendRedirect("changepassword.jsp");
                break;
        }
    }

    private String emailHtml(String tokenizer, String name) {
        String emailHtml = "    <div style=\"background:#f9f9f9;padding-bottom: 70px;\">\n"
                + "        <div style=\"margin:0px auto;max-width:640px;background-color:transparent\">\n"
                + "            <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-size:0px;width:100%;background:transparent;text-align:center;border:0\">\n"
                + "                <tbody>\n"
                + "                    <tr>\n"
                + "                        <td style=\"text-align:center;vertical-align:top;direction: 1tr;font-size:0px;padding:40px 0;\">\n"
                + "                            <div style=\"vertical-align: top;display:inline-block;direction:1tr;font-size:13px;text-align:left;width:100%\">\n"
                + "                                <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"\n"
                + "                                    style=\"border:0;\">\n"
                + "                                    <tbody>\n"
                + "                                        <tr>\n"
                + "                                            <td style=\"word-break: break-word;font-size:0px;padding:0px;\"\n"
                + "                                                align=\"center\">\n"
                + "                                                <table role=\"presentation\" cellpaddin=\"0\" cellspacing=\"0\"\n"
                + "                                                    style=\"border-collapse:collapse;border-spacing:0px;text-align:center;\">\n"
                + "                                                    <tbody>\n"
                + "                                                        <tr>\n"
                + "                                                            <td style=\"width: 140px;\">\n"
                + "                                                                <a href=\"index.jsp\" target=\"_blank\"\n"
                + "                                                                    data-saferedirecturl=\"\">\n"
                + "                                                                    <img src=\"https://scontent.xx.fbcdn.net/v/t1.15752-9/303700089_453522773400612_1790525134303783343_n.jpg?stp=dst-jpg_p403x403&_nc_cat=103&ccb=1-7&_nc_sid=aee45a&_nc_ohc=tUcPD2qlzVYAX-5Auz5&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVLLoHLv6NwL0CV7Omhz5bHWuC3E5pG24tnA-mah9_fdow&oe=6345A8F1\"\n"
                + "                                                                        width=\"140px\"\n"
                + "                                                                        style=\"border-radius:50%;height:auto;line-height:100%;outline:none;text-decoration:none;\"\n"
                + "                                                                        alt=\"\">\n"
                + "                                                                </a>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "                                                    </tbody>\n"
                + "                                                </table>\n"
                + "                                            </td>\n"
                + "                                        </tr>\n"
                + "                                    </tbody>\n"
                + "                                </table>\n"
                + "                            </div>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                </tbody>\n"
                + "            </table>\n"
                + "        </div>\n"
                + "        <div style=\"max-width:640px;margin:0 auto;border-radius:4px;overflow: hidden;\">\n"
                + "            <div style=\"margin:0 auto;max-width:640px;background-color:#ffffff\">\n"
                + "                <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\"\n"
                + "                    style=\"font-size:0px;width:100%;background-color:#ffffff\" align=\"center\" border=\"0\">\n"
                + "                    <tbody>\n"
                + "                        <tr>\n"
                + "                            <td style=\"text-align: center;vertical-align: center;direction:1tr;font-size:0px;padding:40px 70px;\">\n"
                + "                                <div style=\"vertical-align: top;display:inline-block;direction:1tr;font-size:13px;text-align:left;width:100%\">\n"
                + "                                    <table role=\"presentation\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n"
                + "                                        <tbody>\n"
                + "                                            <tr>\n"
                + "                                                <td style=\"word-break: break-word;font-size:0px;padding:0px;\" align=\"left\">\n"
                + "                                                    <div style=\"color:#737f8d;font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:16px;line-height:24px;text-align:left\">\n"
                + "\n"
                + "                                                        <h2 style=\"font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-weight:500;font-size:20px;color:#4f545c;letter-spacing:0.27px\">\n"
                + "                                                            What's up " + name + ",</h2>\n"
                + "                                                        <p>\n"
                + "                                                            We're channeled our psionic energy to change your <span\n"
                + "                                                            style=\"background:#fde293;color:#222;\">Hilfsmotor</span>\n"
                + "                                                            account password. Gonna go get a seltzer to calm down\n"
                + "                                                            <br><br>\n"
                + "                                                            If this wasn't done by you please immediately reset the password to your\n"
                + "                                                            <span style=\"background:#fde293;color:#222;\"> Hilfmotor</span> accout following the steps in\n"
                + "                                                            this link:\n"
                + "                                                        </p>\n"
                + "                                                    </div>\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "                                            <tr>\n"
                + "                                                <td style=\"word-break:break-word;font-size:0px;padding:10px 25px;padding-top:20px\"\n"
                + "                                                    align=\"center\">\n"
                + "                                                    <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\"\n"
                + "                                                        style=\"border-collapse:separate\" align=\"center\" border=\"0\">\n"
                + "                                                        <tbody>\n"
                + "                                                            <tr>\n"
                + "                                                                <td style=\"border:none;border-radius:3px;color:white;padding:15px 19px\"\n"
                + "                                                                    align=\"center\" valign=\"middle\" bgcolor=\"#7289DA\"><a\n"
                + "                                                                        href=\"http://localhost:8088/Rentabike/forgotPasswordController?tokenizer=" + tokenizer + "\" \n"
                + "                                                                        style=\"text-decoration:none;line-height:100%;background:#7289da;color:white;font-family:Ubuntu,Helvetica,Arial,sans-serif;font-size:15px;font-weight:normal;text-transform:none;margin:0px\"\n"
                + "                                                                        target=\"_blank\" data-saferedirecturl=\"\">\n"
                + "                                                                        Change your password\n"
                + "                                                                    </a></td>\n"
                + "                                                            </tr>\n"
                + "                                                        </tbody>\n"
                + "                                                    </table>\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "                                            <tr>\n"
                + "                                                <td style=\"word-break: break-word;font-size:0px;padding:0px;\" align=\"left\">\n"
                + "                                                    <div style=\"color:#737f8d;font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:16px;line-height:24px;text-align:left\">\n"
                + "                                                        <p>\n"
                + "                                                            If you have any other questions on how to reset your password, please let us know.\n"
                + "                                                            <br><br>\n"
                + "                                                            Best,<br>\n"
                + "                                                            Hilfmotor K.A\n"
                + "                                                        </p>\n"
                + "                                                    </div>\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "                                            <tr>\n"
                + "                                                <td style=\"word-break:break-word;font-size:0px;padding:30px 0px\">\n"
                + "                                                    <p style=\"font-size:1px;margin:0px auto;border-top:1px solid #dcddde;width:100%\">\n"
                + "                                                    </p>\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "                                            <tr>\n"
                + "                                                <td style=\"word-break:break-word;font-size:0px;padding:0px\"\n"
                + "                                                    align=\"left\">\n"
                + "                                                    <div style=\"color:#747f8d;font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:13px;line-height:16px;text-align:left\">\n"
                + "                                                        <p style=\"text-align:center\">Need help? <a href=\"#\"\n"
                + "                                                                style=\"font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;color:#7289da\"\n"
                + "                                                                target=\"_blank\" data-saferedirecturl=\"\">Contact our\n"
                + "                                                                support team\n"
                + "                                                                </a> or hit us up on Facebook <a\n"
                + "                                                                href=\"https://www.facebook.com/RogdriguesZ/\"\n"
                + "                                                                style=\"font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;color:#7289da\"\n"
                + "                                                                target=\"_blank\"\n"
                + "                                                                data-saferedirecturl=\"\">@hilfsmotor</a>.\n"
                + "                                                                <br>\n"
                + "                                                        </p>\n"
                + "                                                    </div>\n"
                + "                                                </td>\n"
                + "                                            </tr>\n"
                + "                                        </tbody>\n"
                + "                                    </table>\n"
                + "                                </div>\n"
                + "                            </td>\n"
                + "                        </tr>\n"
                + "                    </tbody>\n"
                + "                </table>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "        <div style=\"margin:0px auto;max-width:640px;background:transparent\">\n"
                + "            <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\"\n"
                + "                style=\"font-size:0px;width:100%;background:transparent\" align=\"center\" border=\"0\">\n"
                + "                <tbody>\n"
                + "                    <tr>\n"
                + "                        <td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px\">\n"
                + "                            <div aria-labelledby=\"mj-column-per-100\" class=\"m_6611936027347281407mj-column-per-100\"\n"
                + "                                style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\">\n"
                + "                                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\">\n"
                + "                                    <tbody>\n"
                + "                                        <tr>\n"
                + "                                            <td style=\"word-break:break-word;font-size:0px;padding:0px\" align=\"center\">\n"
                + "                                                <div style=\"color:#99aab5;font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:12px;line-height:24px;text-align:center\">\n"
                + "                                                    Sent by <span class=\"il\">Hilfsmotor</span> • <a href=\"\"\n"
                + "                                                        style=\"color:#1eb0f4;text-decoration:none\" target=\"_blank\"\n"
                + "                                                        data-saferedirecturl=\"\">check our blog</a> • <a\n"
                + "                                                        href=\"https://www.facebook.com/RogdriguesZ/\"\n"
                + "                                                        style=\"color:#1eb0f4;text-decoration:none\" target=\"_blank\"\n"
                + "                                                        data-saferedirecturl=\"\">@hilfsmotor</a>\n"
                + "                                                </div>\n"
                + "                                            </td>\n"
                + "                                        </tr>\n"
                + "                                        <tr>\n"
                + "                                            <td style=\"word-break:break-word;font-size:0px;padding:0px\" align=\"center\">\n"
                + "                                                <div\n"
                + "                                                    style=\"color:#99aab5;font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:12px;line-height:24px;text-align:center\">\n"
                + "                                                    444 De Haro Street, Suite 200, San Francisco, CA 94107\n"
                + "                                                </div>\n"
                + "                                            </td>\n"
                + "                                        </tr>\n"
                + "                                    </tbody>\n"
                + "                                </table>\n"
                + "                            </div>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                </tbody>\n"
                + "            </table>\n"
                + "        </div>\n"
                + "    </div>";
        return emailHtml;
    }
}
