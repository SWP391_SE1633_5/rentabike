/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.google.gson.Gson;
import dao.BlogAccountSaveDao;
import dao.BlogDao;
import dao.BlogTagDao;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.Account;
import model.Blog;
import model.BlogTag;

/**
 *
 * @author win
 */
@WebServlet(name="BlogListController", urlPatterns={"/blog-list"})
public class BlogListController extends HttpServlet {
   
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        map.clear();
        BlogDao bd = new BlogDao();
        BlogTagDao btd = new BlogTagDao();
        PrintWriter out = response.getWriter();
        String type = request.getParameter("type");
        if(type==null || type.equals("")){
            request.getRequestDispatcher("BlogList.html").forward(request, response);
            return;
        }
        
        List<Object> dataList = new ArrayList<>();
        List<BlogTag> blogTagList = btd.getTop5BlogTag();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(type.equals("All")){
            List<Blog> blogList = bd.getAllAuthenBlogs();
            
            for(Blog b: blogList){
                HashMap<Object, Object> mapTmp = new HashMap<>();
                if(UserDB.searchAccountID(b.getAccountId())!=null){
                    mapTmp.put("account", UserDB.searchAccountID(b.getAccountId()));
                } else if(StaffDB.searchAccountID(b.getAccountId())!=null){
                    mapTmp.put("account", StaffDB.searchAccountID(b.getAccountId()));
                }
                mapTmp.put("blog", b);
                mapTmp.put("tag", BlogTagDao.getAllBlogTagByBlogId(b.getId()));
                if(a!=null){
                    mapTmp.put("blogSave", BlogAccountSaveDao.getBlogAccountSaveByBlogIdAndAccId(b.getId(), a.getAccountID()));
                }
                dataList.add(mapTmp);
            }
        }
        if(!type.equals("All")){
            List<Blog> blogList = bd.getBlogsbyTagTitle(type);
            
            for(Blog b: blogList){
                HashMap<Object, Object> mapTmp = new HashMap<>();
                if(UserDB.searchAccountID(b.getAccountId())!=null){
                    mapTmp.put("account", UserDB.searchAccountID(b.getAccountId()));
                } else if(StaffDB.searchAccountID(b.getAccountId())!=null){
                    mapTmp.put("account", StaffDB.searchAccountID(b.getAccountId()));
                }
                mapTmp.put("blog", b);
                mapTmp.put("tag", BlogTagDao.getAllBlogTagByBlogId(b.getId()));
                if(a!=null){
                    mapTmp.put("blogSave", BlogAccountSaveDao.getBlogAccountSaveByBlogIdAndAccId(b.getId(), a.getAccountID()));
                }
                dataList.add(mapTmp);
            }
        }
        map.put("SC", 1);
        map.put("Data", dataList);
        map.put("TagList", blogTagList);
        out.print(gson.toJson(map));
        out.flush();
        
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
