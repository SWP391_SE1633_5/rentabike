/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import com.google.gson.Gson;
import dao.BlogAccountSaveDao;
import dao.BlogAccountVoteDao;
import dao.BlogCommentDao;
import dao.BlogDao;
import dao.BlogTagDao;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.Account;
import model.Blog;
import model.BlogTag;

/**
 *
 * @author win
 */
@WebServlet(name="BlogAdminControlController", urlPatterns={"/blog-admin-control"})
public class BlogAdminControlController extends HttpServlet {
   
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        map.clear();
        BlogDao bd = new BlogDao();
        PrintWriter out = response.getWriter();
        String type = request.getParameter("type");
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            response.sendRedirect("started.jsp");
            return;
        }
        if(!a.getRole().equals("ADMIN")){
            response.sendRedirect("app-product-shop.jsp");
            return;
        }
        if(type==null || type.equals("")){
            request.getRequestDispatcher("BlogAdminControl.html").forward(request, response);
            return;
        }
        
        List<Object> dataList = new ArrayList<>();
        if(type.equals("All")){
            List<Blog> blogList = bd.adminGetAllUnAuthenBlogs();
            
            for(Blog b: blogList){
                HashMap<Object, Object> mapTmp = new HashMap<>();
                if(UserDB.searchAccountID(b.getAccountId())!=null){
                    mapTmp.put("account", UserDB.searchAccountID(b.getAccountId()));
                } else if(StaffDB.searchAccountID(b.getAccountId())!=null){
                    mapTmp.put("account", StaffDB.searchAccountID(b.getAccountId()));
                }
                mapTmp.put("blog", b);
                mapTmp.put("tag", BlogTagDao.getAllBlogTagByBlogId(b.getId()));
                dataList.add(mapTmp);
            }
        }
        if(type.equals("Search")){
             String search = request.getParameter("search");
            List<Blog> blogList = BlogDao.adminSearchAllUnAuthenBlogs(search);
            
            
            for(Blog b: blogList){
                HashMap<Object, Object> mapTmp = new HashMap<>();
                if(UserDB.searchAccountID(b.getAccountId())!=null){
                    mapTmp.put("account", UserDB.searchAccountID(b.getAccountId()));
                } else if(StaffDB.searchAccountID(b.getAccountId())!=null){
                    mapTmp.put("account", StaffDB.searchAccountID(b.getAccountId()));
                }
                mapTmp.put("blog", b);
                mapTmp.put("tag", BlogTagDao.getAllBlogTagByBlogId(b.getId()));
                dataList.add(mapTmp);
            }
        }
        map.put("SC", 1);
        map.put("Data", dataList);
        out.print(gson.toJson(map));
        out.flush();
        
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        String type = request.getParameter("type");
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            response.sendRedirect("started.jsp");
            return;
        }
        if(!a.getRole().equals("ADMIN")){
            map.put("SC", -1);
            map.put("ME", "Permision denied!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        if(type.equals("handle-read")){
            String[] chooseCheck = request.getParameterValues("checkResultArr[]");
            int readType = Integer.parseInt(request.getParameter("readType"));
            for(String choose: chooseCheck){
                int bid = Integer.parseInt(choose);
                BlogDao.updateAdminReadState(bid, readType);
            }
        
            map.put("SC", 1);
            map.put("ME", "Change state success!");
            out.print(gson.toJson(map));
            out.flush();
        }
        if(type.equals("handle-delete")){
            String[] chooseCheck = request.getParameterValues("checkResultArr[]");
            for(String choose: chooseCheck){
                int bid = Integer.parseInt(choose);
                BlogTagDao.delAllRelatedBlogTagByBlogId(bid);
                BlogAccountSaveDao.DeleteBlogAccountSaveByBlogId(bid);
                BlogCommentDao.deleteAllBlogCommentByBlogId(bid);
                BlogAccountVoteDao.deleteBlogAccountVoteByBId(bid);
                BlogDao.deleteBlogByBlogId(bid);
            }
            map.put("SC", 1);
            map.put("ME", "Delete success!");
            out.print(gson.toJson(map));
            out.flush();
        }
        if(type.equals("handle-accept")){
            int bid = Integer.parseInt(request.getParameter("blogId"));
            BlogDao.adminAcceptBlog(bid);
            
            map.put("SC", 1);
            map.put("ME", "Accept success!");
            out.print(gson.toJson(map));
            out.flush();
        }
        if(type.equals("need-update")){
            int bid = Integer.parseInt(request.getParameter("blogId"));
            BlogDao.adminChangeAcceptToPending(bid);
            
            map.put("SC", 1);
            map.put("ME", "Change Status success!");
            out.print(gson.toJson(map));
            out.flush();
        }
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
