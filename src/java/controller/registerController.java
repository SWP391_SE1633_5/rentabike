/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.*;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import model.Staff;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "registerController", urlPatterns = "/registerController")
public class registerController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String modeSwitch = request.getParameter("mode");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        if (modeSwitch.equalsIgnoreCase("USER")) {
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String phoneNumber = request.getParameter("phoneNumber");
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String city = request.getParameter("city");
            String street = request.getParameter("street");
            String state = request.getParameter("state");

            Account account = Manager.searchAccount(email);

            if (account == null) {
                int statusUpdateRole = Manager.updateAccount(email, password, "CUSTOMER");
                account = Manager.searchAccount(email);
                int statusUpdate = UserDB.saveAccount(firstName, lastName, phoneNumber, email, account.getAccountID(), null, city, street, state);
                if (statusUpdate > 0 && statusUpdateRole > 0) {
                    out.print("SUCCESS");
                } else {
                    out.print("FAILED");
                }
            } else {
                out.print("EMAIL");
            }
        } else {
            String userName = request.getParameter("username");
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String mobileNumber = request.getParameter("mobileNumber");
            String city = request.getParameter("city");
            String street = request.getParameter("street");
            String state = request.getParameter("state");

            Account account = Manager.searchAccount(email);
            if (account == null) {
                int statusUpdateRole = Manager.updateAccount(email, password, "STAFF");
                account = Manager.searchAccount(email);
                Staff staff = new Staff();
                staff.setAccountID(account.getAccountID());
                staff.setEmail(account.getEmail());
                staff.setUserName(userName);
                staff.setFirstName(firstName);
                staff.setLastName(lastName);
                staff.setPhoneNumber(mobileNumber);
                staff.setCity(city);
                staff.setState(state);
                staff.setStreet(street);
                int statusUpdate = StaffDB.saveStaff(staff);
                if (statusUpdate > 0 && statusUpdateRole > 0) {
                    session.setAttribute("statusRegister", "true");
                    out.print("SUCCESS");
                } else {
                    out.print("FAILED");
                }
            } else {
                out.print("EMAIL");
            }
        }
    }
}
