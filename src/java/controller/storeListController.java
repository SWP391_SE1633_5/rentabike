/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.StoreDB;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Account;
import model.Store;

/**
 *
 * @author baqua
 */
public class storeListController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        Store store = new Store();
        StoreDB sd = new StoreDB();
        store = StoreDB.searchStore();
        request.setAttribute("store", store);
        
        if (account.getRole().equals("STAFF") || account.getRole().equals("ADMIN")){
        ArrayList<Store> listall = sd.getAllStore();
        request.setAttribute("listall", listall);
        String page = request.getParameter("page");
        int xpage;
        if (page == null) {
            xpage = 1;
        } else {
            xpage = Integer.parseInt(page);
        }
        String selectAmount = request.getParameter("amount");
        if(selectAmount == null) selectAmount = "5";
        int numerpage = Integer.valueOf(selectAmount);
        int size = listall.size();
        int numpage = (size % numerpage == 0) ? size / numerpage : size / numerpage + 1;
        int start = (xpage - 1) * numerpage;
        int end = Math.min(size, xpage * numerpage);
        request.setAttribute("page", page);
        ArrayList<Store> list = sd.getByPage(listall, start, end);
        request.setAttribute("numpage", numpage);
        request.setAttribute("list", list);
        int numstore = sd.getNumberOfStore();
        int numavailable = sd.getNumberOfAvailableStore();
        int numunavailable = sd.getNumberOfUnavailableStore();
        int numband = sd.getNumberOfBandStore();
        
        request.setAttribute("numstore", numstore);
        request.setAttribute("numavailable", numavailable);
        request.setAttribute("numunavailable", numunavailable);
        request.setAttribute("numband", numband);
        session.setAttribute("selectAmountStore", selectAmount);
        request.getRequestDispatcher("app-store-list.jsp").forward(request, response);
        }
        if (account.getRole().equals("CUSTOMER") || account.getRole().equals("SALEPERSON")){
        ArrayList<Store> listall = sd.getAllStoreAvailable();
        request.setAttribute("listall", listall);
        String page = request.getParameter("page");
        int xpage;
        if (page == null) {
            xpage = 1;
        } else {
            xpage = Integer.parseInt(page);
        }
        String selectAmount = request.getParameter("amount");
        if(selectAmount == null) selectAmount = "5";
        int numerpage = Integer.valueOf(selectAmount);
        int size = listall.size();
        int numpage = (size % numerpage == 0) ? size / numerpage : size / numerpage + 1;
        int start = (xpage - 1) * numerpage;
        int end = Math.min(size, xpage * numerpage);
        request.setAttribute("page", page);
        ArrayList<Store> list = sd.getByPage(listall, start, end);
        request.setAttribute("numpage", numpage);
        request.setAttribute("list", list);
        int numstore = sd.getNumberOfStore();
        int numavailable = sd.getNumberOfAvailableStore();
        int numunavailable = sd.getNumberOfUnavailableStore();
        int numband = sd.getNumberOfBandStore();
        
        request.setAttribute("numstore", numstore);
        request.setAttribute("numavailable", numavailable);
        request.setAttribute("numunavailable", numunavailable);
        request.setAttribute("numband", numband);
        session.setAttribute("selectAmountStore", selectAmount);
            request.getRequestDispatcher("store-list.jsp").forward(request, response);
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("store-list");
    }

}
