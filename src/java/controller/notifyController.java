/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.Manager;
import dao.NotifyDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import model.Account;
import model.Notify;
import model.Product;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "notifyController", urlPatterns = "/notifyController")
public class notifyController extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String mode = request.getParameter("mode");
        switch (mode) {
            case "notifyRecordAndShow":
                notifyRecordAndShow(request, out);
                break;
            case "showNotifyOnly":
                showNotify(request, out);
                break;
            case "NotifyTotalHandler":
                notifyTotalHandler(request, out);
                break;
        }
    }

    private void notifyRecordAndShow(HttpServletRequest request, PrintWriter out) {
        int productID = Integer.parseInt(request.getParameter("productID"));
        String email = request.getParameter("email");
        String setUpNotify = request.getParameter("setUpNotify");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        if (account != null) {
            Product product = NotifyDB.searchProduct("SELECT P.[PRODUCT_NAME], [PI].[IMAGE] FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI]  WHERE P.PRODUCT_ID = ? AND P.PRODUCT_ID = [PI].PRODUCT_ID", productID);
            Notify notify = new Notify();
            notify.setAccountID(account.getAccountID());
            if (setUpNotify.equalsIgnoreCase("Checkout")) {
                notify.setTitleNotify("A new checkout had been raised");
                notify.setDescription(product.getName() + " " + " had been add to your checkout.");
            } else if (setUpNotify.equalsIgnoreCase("addWishList")) {
                notify.setTitleNotify("Wishlist marked finished");
                notify.setDescription(product.getName() + " " + " had been add to your wishlist.");
            } else if (setUpNotify.equalsIgnoreCase("RemoveCheckout")) {
                notify.setTitleNotify("Checkout removed");
                notify.setDescription(product.getName() + " " + " had been removed from checkout.");
            } else if (setUpNotify.equalsIgnoreCase("removeWishList")) {
                notify.setTitleNotify("Wishlist removed");
                notify.setDescription(product.getName() + " " + " had been removed from wishlist.");
            }
            notify.setImageNotìycation(product.getImage());
            NotifyDB.addNotify(notify);
            int totalNew = NotifyDB.countTotalNew(account.getAccountID());
            session.setAttribute("totalNew", totalNew);
            showNotify(request, out);
        } else {
            out.print("FAILED");
        }
    }

    private void showNotify(HttpServletRequest request, PrintWriter out) {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        LinkedList<Notify> notifyList = NotifyDB.listNotifies(account.getAccountID());
        int sizeNotify = 5;
        if (notifyList != null) {
            if (notifyList.size() < sizeNotify) {
                sizeNotify = notifyList.size();
            }
            for (int i = 0; i < sizeNotify; i++) {
                Clock clock = Clock.systemDefaultZone();
                LocalDateTime currentDate = LocalDateTime.now(clock).withNano(0);
                DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                String[] timeSplit = notifyList.get(i).getTimeEvent().split("\\.");
                LocalDateTime local = LocalDateTime.parse(timeSplit[0], dateFormat);
                String hour = String.valueOf(local.getHour());
                String minute = String.valueOf(local.getMinute());
                String second = String.valueOf(local.getSecond());
                if (Integer.parseInt(hour) < 10) {
                    hour = "0" + hour;
                }
                if (Integer.parseInt(minute) < 10) {
                    minute = "0" + minute;
                }
                if (Integer.parseInt(second) < 10) {
                    second = "0" + second;
                }
                notifyList.get(i).setTimeEvent(hour + ":" + minute + ":" + second);

                LocalDateTime tempDateTime = LocalDateTime.from(local).withNano(0);
                long secondDistance = tempDateTime.until(currentDate, ChronoUnit.SECONDS);
                long minuteDistance = tempDateTime.until(currentDate, ChronoUnit.MINUTES);
                long hourDistance = tempDateTime.until(currentDate, ChronoUnit.HOURS);
                long dayDistance = tempDateTime.until(currentDate, ChronoUnit.DAYS);
                long monthDistance = tempDateTime.until(currentDate, ChronoUnit.MONTHS);
                long yearDistance = tempDateTime.until(currentDate, ChronoUnit.YEARS);

                if (yearDistance > 0) {
                    notifyList.get(i).setTimeEvent(String.valueOf(yearDistance) + " year ago");
                }
                if (monthDistance <= 12 && monthDistance > 0) {
                    notifyList.get(i).setTimeEvent(String.valueOf(monthDistance) + " month ago");
                }
                if (dayDistance <= 30 && dayDistance > 0) {
                    notifyList.get(i).setTimeEvent(String.valueOf(dayDistance) + " day ago");
                }
                if (hourDistance <= 60 && hourDistance > 0) {
                    notifyList.get(i).setTimeEvent(String.valueOf(hourDistance) + " hour ago");
                }
                if (minuteDistance <= 60 && minuteDistance > 0) {
                    notifyList.get(i).setTimeEvent(String.valueOf(minuteDistance) + " minute ago");
                }
                if (secondDistance <= 60) {
                    notifyList.get(i).setTimeEvent(String.valueOf(second) + " second ago");
                }
                out.print("<a class=\"d-flex\" href=\"#\">\n"
                        + "                                    <div class=\"list-item d-flex align-items-start\">\n"
                        + "                                        <div class=\"me-1\">\n"
                        + "                                            <div class=\"avatar\"><img src=\"" + notifyList.get(i).getImageNotìycation() + "\" alt=\"avatar\" width=\"32\" height=\"32\"></div>\n"
                        + "                                        </div>\n"
                        + "                                        <div class=\"list-item-body flex-grow-1\">\n"
                        + "                                            <p class=\"media-heading\"><span class=\"fw-bolder\">" + notifyList.get(i).getTitleNotify() + "</p><small class=\"notification-text\">" + notifyList.get(i).getDescription() + "</small></br><small class=\"notification-text\">" + notifyList.get(i).getTimeEvent() + "</small>\n"
                        + "                                        </div>\n"
                        + "                                    </div>\n"
                        + "                                </a>");
            }
        } else {
            out.print("FAILED");
        }
    }

    private void notifyTotalHandler(HttpServletRequest request, PrintWriter out) {
        String setUpNotify = request.getParameter("setUpNotify");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        int totalNew = NotifyDB.countTotalNew(account.getAccountID());
        if (setUpNotify.equalsIgnoreCase("openNotify")) {
            NotifyDB.updateMarked(account.getAccountID());
            session.setAttribute("totalNew", 0);
            out.print(0);
        } else if (setUpNotify.equalsIgnoreCase("calculateNotify")) {
            session.setAttribute("totalNew", totalNew);
            out.print(totalNew);
        }
    }
}
