/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.PermissionDetailDB;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import model.Account;
import model.PermissionsDetail;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "permissionListController", urlPatterns = "/permissionListController")
public class permissionListController extends HttpServlet {

    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final String sqlQuery = "SELECT * FROM PERMISSION_DETAILS";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        String pagination = request.getParameter("page");
        PrintWriter out = response.getWriter();

        if (pagination == null) {
            pagination = "1";
        }

        if (account != null) {
            switch (pagination) {
                case "1":
                    listPermission(session);
                    break;
                default:
                    paginationPage(request, out, session);
                    break;
            }
            response.sendRedirect("app-access-permission.jsp");
        } else {
            response.sendRedirect("page-misc-not-authorized.html");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String mode = request.getParameter("mode");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        if (account != null) {
            switch (mode) {
                case "addNewPermission":
                    addNewPermission(request, out);
                    break;
                case "showEntry":
                    paginationPage(request, out, session);
                    break;
                case "searchName":
                    searchUserName(request, out);
                    break;
                case "selectRole":
                    selectRole(request, out);
                    break;
                case "handleUserStatus":
                    handleUserStatus(request, out);
                    break;
                case "viewPermission":
                    viewPermission(request, out);
                    break;
            }
        } else {
            out.print("sessionFailed");
        }
    }

    private void listPermission(HttpSession session) {
        LinkedList<PermissionsDetail> permissionList = PermissionDetailDB.listPermission(sqlQuery);
        LinkedList<Integer> numberPaginationList = new LinkedList<>();
        LinkedList<Integer> showEntryList = new LinkedList<>();
        showEntryList.add(10);
        showEntryList.add(3);
        showEntryList.add(50);
        showEntryList.add(100);
        HashMap<Integer, String[]> mapAssignList = PermissionDetailDB.listPermissionAssign();
        String showEntry = (String) session.getAttribute("showEntry");
        int showEntryCurrent = 10;
        if (showEntry != null) {
            showEntryCurrent = Integer.parseInt(showEntry);
        }

        if (permissionList != null) {
            for (PermissionsDetail permissionsDetail : permissionList) {
                String[] splitEvent = permissionsDetail.getCreateDate().split("\\.");
                LocalDateTime local = LocalDateTime.parse(splitEvent[0], dateFormat);
                String timeSet = local.getHour() >= 12 ? "PM" : "AM";
                String localMonth = local.getMonth().toString().subSequence(0, 1)
                        + local.getMonth().toString().substring(1, 3).toLowerCase();
                String hour = local.getHour() < 10 ? "0" + local.getHour() : String.valueOf(local.getHour());
                String minute = local.getMinute() < 10 ? "0" + local.getMinute() : String.valueOf(local.getMinute());

                String localTimeModifier = local.getDayOfMonth() + " " + localMonth + " " + local.getYear() + ", " + hour + ":" + minute + " " + timeSet;
                permissionsDetail.setCreateDate(localTimeModifier);
            }
            double showTotalPage = (double) (showEntryCurrent - 1) + 1.0;
            int numberPaginaton = (int) Math.ceil(permissionList.size() / showTotalPage);
            for (int i = 0; i < numberPaginaton; i++) {
                numberPaginationList.add(i + 1);
            }
            session.setAttribute("showEntry", String.valueOf(showEntryCurrent));
            session.setAttribute("numberPaginationList", numberPaginationList);
            session.setAttribute("currentPage", 1);
            session.setAttribute("limitStart", 1);
            session.setAttribute("limitEnd", showEntryCurrent);
            session.setAttribute("showEntryList", showEntryList);
            session.setAttribute("permissionList", permissionList);
            session.setAttribute("mapAssignList", mapAssignList);
            session.setAttribute("startData", 0);
            session.setAttribute("endData", (showEntryCurrent - 1));
            session.setAttribute("totalPermission", permissionList.size() + 1);
        }
    }

    private void paginationPage(HttpServletRequest request, PrintWriter out, HttpSession session) {
        String showEntry = request.getParameter("showEntry");
        String showEntryCurrent = (String) session.getAttribute("showEntry");
        int pagination = Integer.parseInt(request.getParameter("page"));
        int nextPage;
        if ((showEntry == null || "".equals(showEntry) || showEntry.length() == 0) && showEntryCurrent == null) {
            nextPage = 10;
        } else {
            if (showEntry != null) {
                nextPage = Integer.parseInt(showEntry);
            } else {
                nextPage = Integer.parseInt(showEntryCurrent);
            }
        }

        if (showEntryCurrent != null) {
            if (showEntry == null) {
                showEntry = showEntryCurrent;
            }
        }

        int totalPage = pagination * nextPage;
        int indexPage = totalPage - nextPage;
        LinkedList<PermissionsDetail> permissionList = PermissionDetailDB.listPermission(sqlQuery);
        HashMap<Integer, String[]> mapAssignList = PermissionDetailDB.listPermissionAssign();

        for (int i = indexPage; i < totalPage; i++) {
            if (i < permissionList.size()) {
                String[] splitEvent = permissionList.get(i).getCreateDate().split("\\.");
                LocalDateTime local = LocalDateTime.parse(splitEvent[0], dateFormat);
                String timeSet = local.getHour() >= 12 ? "PM" : "AM";
                String localMonth = local.getMonth().toString().subSequence(0, 1)
                        + local.getMonth().toString().substring(1, 3).toLowerCase();
                String hour = local.getHour() < 10 ? "0" + local.getHour() : String.valueOf(local.getHour());
                String minute = local.getMinute() < 10 ? "0" + local.getMinute() : String.valueOf(local.getMinute());

                String localTimeModifier = local.getDayOfMonth() + " " + localMonth + " " + local.getYear() + ", " + hour + ":" + minute + " " + timeSet;
                permissionList.get(i).setCreateDate(localTimeModifier);
            }
        }
        session.setAttribute("mapAssignList", mapAssignList);
        session.setAttribute("showEntry", showEntry);
        session.setAttribute("startData", indexPage);
        session.setAttribute("endData", totalPage - 1);
        session.setAttribute("currentPage", pagination);
        session.setAttribute("limitStart", indexPage + 1);
        session.setAttribute("limitEnd", totalPage);
        session.setAttribute("permissionList", permissionList);
    }

    private void handleUserStatus(HttpServletRequest request, PrintWriter out) {
        String contextSwitch = request.getParameter("contextSwitch");
        String idPermission = request.getParameter("idPermission");
        String namePermission = request.getParameter("name");
        String assignTo = request.getParameter("assignTo");
        String oldPermission = request.getParameter("oldPermission");

        int status = 0;
        if (contextSwitch.equalsIgnoreCase("ADD")) {
            status = PermissionDetailDB.editPermission(Integer.parseInt(idPermission), namePermission, assignTo);
            if (status > 0) {
                UserDB.updatePermission(namePermission, oldPermission);
                StaffDB.updatePermission(namePermission, oldPermission);
            }
        } else {
            status = PermissionDetailDB.deletePermission(Integer.parseInt(idPermission));
        }
        if (status > 0) {
            out.print("SUCCESS");
        } else {
            out.print("FAILED");
        }
    }

    private void addNewPermission(HttpServletRequest request, PrintWriter out) {
        String name = request.getParameter("name");
        String assignTo = request.getParameter("assignTo");

        if (name != null && assignTo != null) {
            int status = PermissionDetailDB.createPermission(name, assignTo);
            if (status > 0) {
                out.print("SUCCESS");
            } else {
                out.print("ERROR");
            }
        } else {
            out.print("FAILED");
        }

    }

    private void selectRole(HttpServletRequest request, PrintWriter out) {
        String selectOption = request.getParameter("selectOption");
        if ("".equals(selectOption) || selectOption.length() == 0) {
            out.print("RESET");
            return;
        }
        LinkedList<PermissionsDetail> permissionList = PermissionDetailDB.listPermission(sqlQuery + " WHERE ASSIGN_TO LIKE '%" + selectOption + "%'");
        if (permissionList != null) {
            for (PermissionsDetail permissionsDetail : permissionList) {
                String[] splitEvent = permissionsDetail.getCreateDate().split("\\.");
                LocalDateTime local = LocalDateTime.parse(splitEvent[0], dateFormat);
                String hour = local.getHour() < 10 ? "0" + local.getHour() : String.valueOf(local.getHour());
                String minute = local.getMinute() < 10 ? "0" + local.getMinute() : String.valueOf(local.getMinute());
                String timeSet = local.getHour() >= 12 ? "PM" : "AM";
                String localMonth = local.getMonth().toString().subSequence(0, 1)
                        + local.getMonth().toString().substring(1, 3).toLowerCase();

                String localTimeModifier = local.getDayOfMonth() + " " + localMonth + " " + local.getYear() + ", " + hour + ":" + minute + " " + timeSet;
                permissionsDetail.setCreateDate(localTimeModifier);
            }
            displayPermissionList(out, permissionList);
        }
    }

    private void searchUserName(HttpServletRequest request, PrintWriter out) {
        String inputSearch = request.getParameter("inputSearch");
        if ("".equals(inputSearch) || inputSearch.length() == 0) {
            out.print("RESET");
            return;
        }
        LinkedList<PermissionsDetail> permissionList = PermissionDetailDB.listPermission(sqlQuery + " WHERE NAME LIKE '%" + inputSearch + "%'");
        if (permissionList != null) {
            for (PermissionsDetail permissionsDetail : permissionList) {
                String[] splitEvent = permissionsDetail.getCreateDate().split("\\.");
                LocalDateTime local = LocalDateTime.parse(splitEvent[0], dateFormat);
                String timeSet = local.getHour() >= 12 ? "PM" : "AM";
                String hour = local.getHour() < 10 ? "0" + local.getHour() : String.valueOf(local.getHour());
                String minute = local.getMinute() < 10 ? "0" + local.getMinute() : String.valueOf(local.getMinute());
                String localMonth = local.getMonth().toString().subSequence(0, 1)
                        + local.getMonth().toString().substring(1, 3).toLowerCase();

                String localTimeModifier = local.getDayOfMonth() + " " + localMonth + " " + local.getYear() + ", " + hour + ":" + minute + " " + timeSet;
                permissionsDetail.setCreateDate(localTimeModifier);
            }
            displayPermissionList(out, permissionList);
        }
    }

    private void displayPermissionList(PrintWriter out, LinkedList<PermissionsDetail> permissionList) {
        for (PermissionsDetail permissionsDetail : permissionList) {
            String listPermissionAssign = "";
            String splitItemAssign[] = permissionsDetail.getAssignTo().split(", ");
            for (String roleAssign : splitItemAssign) {
                if (roleAssign.equalsIgnoreCase("Admin")) {
                    listPermissionAssign += "<a href=\"#/\" class=\"me-50\">\n"
                            + "<span class=\"badge rounded-pill badge-light-primary\">Administrator</span>\n"
                            + "</a>";
                } else if (roleAssign.equalsIgnoreCase("Staff")) {
                    listPermissionAssign += "<a href=\"#/\" class=\"me-50\">\n"
                            + "<span class=\"badge rounded-pill badge-light-warning\">Staff</span>\n"
                            + "</a>";
                } else if (roleAssign.equalsIgnoreCase("Manager")) {
                    listPermissionAssign += "<a href=\"#/\" class=\"me-50\">\n"
                            + "<span class=\"badge rounded-pill badge-light-danger\">Manager</span>\n"
                            + "</a>";
                } else if (roleAssign.equalsIgnoreCase("User")) {
                    listPermissionAssign += "<a href=\"#/\" class=\"me-50\">\n"
                            + "<span class=\"badge rounded-pill badge-light-success\">User</span>\n"
                            + "</a>";
                } else if (roleAssign.equalsIgnoreCase("Blog")) {
                    listPermissionAssign += "<a href=\"#/\" class=\"me-50\">\n"
                            + "<span class=\"badge rounded-pill badge-light-info\">Blog</span>\n"
                            + "</a>";
                } else if (roleAssign.equalsIgnoreCase("Store")) {
                    listPermissionAssign += "<a href=\"#/\" class=\"me-50\">\n"
                            + "<span class=\"badge rounded-pill badge-light-success\">Store</span>\n"
                            + "</a>";
                } else if (roleAssign.equalsIgnoreCase("Support")) {
                    listPermissionAssign += "<a href=\"#/\" class=\"me-50\">\n"
                            + "<span class=\"badge rounded-pill badge-light-info\">Support</span>\n"
                            + "</a>";
                }
            }
            out.print("                                                <tr>\n"
                    + "                                                    <td class=\"control\" tabindex=\"0\" style=\"display: none;\"></td>\n"
                    + "                                                    <td>" + permissionsDetail.getName() + "</td>\n"
                    + "                                                    <td>\n"
                    + listPermissionAssign
                    + "                                                    </td>\n"
                    + "                                                    <td>" + permissionsDetail.getCreateDate() + "</td>\n"
                    + "                                                    <td>\n"
                    + "                                                        <button class=\"btn btn-sm btn-icon\" id=\"UpdatePermission\" onclick=\"sendParamterPermission(this)\" index=\"" + permissionsDetail.getIdPremission() + "\" data-bs-toggle=\"modal\"\n"
                    + "                                                                data-bs-target=\"#editPermissionModal\">\n"
                    + "                                                            <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\"\n"
                    + "                                                                 height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\"\n"
                    + "                                                                 stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
                    + "                                                                 class=\"feather feather-edit font-medium-2 text-body\">\n"
                    + "                                                            <path d=\"M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7\"></path>\n"
                    + "                                                            <path d=\"M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z\"></path>\n"
                    + "                                                            </svg>\n"
                    + "                                                        </button>\n"
                    + "                                                        <button class=\"btn btn-sm btn-icon delete-record\" id=\"DeletePermission\" onclick=\"sendParamterPermission(this)\" index=\"" + permissionsDetail.getIdPremission() + "\">\n"
                    + "                                                            <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\n"
                    + "                                                                 stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
                    + "                                                                 class=\"feather feather-trash font-medium-2 text-body\">\n"
                    + "                                                            <polyline points=\"3 6 5 6 21 6\"></polyline>\n"
                    + "                                                            <path d=\"M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2\">\n"
                    + "                                                            </path>\n"
                    + "                                                            </svg>\n"
                    + "                                                        </button>\n"
                    + "                                                    </td>\n"
                    + "                                                </tr>");
        }
    }

    private void viewPermission(HttpServletRequest request, PrintWriter out) {
        String idPermission = request.getParameter("idPermission");

        PermissionsDetail permission = PermissionDetailDB.searchPermission(Integer.parseInt(idPermission));

        if (permission != null) {
            String[] splitEvent = permission.getCreateDate().split("\\.");
            LocalDateTime local = LocalDateTime.parse(splitEvent[0], dateFormat);
            String timeSet = local.getHour() >= 12 ? "PM" : "AM";
            String hour = local.getHour() < 10 ? "0" + local.getHour() : String.valueOf(local.getHour());
            String minute = local.getMinute() < 10 ? "0" + local.getMinute() : String.valueOf(local.getMinute());
            String localMonth = local.getMonth().toString().subSequence(0, 1)
                    + local.getMonth().toString().substring(1, 3).toLowerCase();

            String localTimeModifier = local.getDayOfMonth() + " " + localMonth + " " + local.getYear() + ", " + hour + ":" + minute + " " + timeSet;
            permission.setCreateDate(localTimeModifier);

            out.print(permission);
        } else {
            out.print("FAILED");
        }
    }
}
