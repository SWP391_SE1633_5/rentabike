/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this templates
 */
package controller;

import Utils.Utils;
import com.google.gson.Gson;
import dao.AssistanceDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import model.Account;
import model.Assistance;

/**
 *
 * @author win
 */
@WebServlet(name = "HelpPageController", urlPatterns = {"/help-page"})
public class helpPageController extends HttpServlet {
    
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        String type = request.getParameter("type");
        if (type == null || type.equals("")) {
            request.getRequestDispatcher("HelpPage.html").include(request, response);
            return;
        }

        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        AssistanceDao ad = new AssistanceDao();
        Account a = (Account)session.getAttribute("account");
        
        List<Assistance> aList = ad.getAllListAssistance();
        map.put("SC", 1);
        map.put("Data", aList);
        if(a!=null){
            map.put("Role", a.getRole());
        }
        out.print(gson.toJson(map));
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        AssistanceDao ad = new AssistanceDao();
        
        //update useful or notuseful
        String type = request.getParameter("type");
        String id = request.getParameter("id");
        if(type!=null || id!=null){
            ad.updateQuantUsefullOrnot(Integer.parseInt(id), Integer.parseInt(type));
            Assistance a = ad.getAssistanceById(Integer.parseInt(id));
            map.put("SC", 1);
            map.put("Mess", "Update quant Succesfully!");
            map.put("Data", a);
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        //add-update assisstance
        String question = request.getParameter("question");
        String answer = request.getParameter("answer");
        String action = request.getParameter("action");
        String updId = request.getParameter("updId");
        String delId = request.getParameter("delId");
        
        if(a==null || !a.getRole().equals("ADMIN")){
            map.put("SC", 0);
            map.put("Mess", "Access denied!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        
        if(action.equals("add")){
            String curTime = Utils.getCurrentDateTime();
            ad.addNewAssistance(question, answer, curTime);
            map.put("SC", 1);
            map.put("Mess", "Add Succesfully!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        if(action.equals("update")){
            String curTime = Utils.getCurrentDateTime();
            ad.updAssitance(question, answer, curTime, Integer.parseInt(updId));
            map.put("SC", 1);
            map.put("Mess", "Updated Succesfully!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        if(action.equals("delete")){
            ad.delAssistance(Integer.parseInt(delId));
            map.put("SC", 1);
            map.put("Mess", "Delete Succesfully!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
