/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.Manager;
import dao.ProductDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.LinkedList;
import model.Account;
import model.Product;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "wishListController", urlPatterns = "/wishListController")

public class wishListController extends HttpServlet {

    private static final String sqlQuery = "SELECT P.*, [PI].IMAGE, [PS].DISPLACEMENT, [PS].TRANSMISSION, [PS].STARTER, [PS].FUEL_SYSTEM, [PS].FUEL_CAPACITY, [PS].DRY_WEIGHT, [PS].SEAT_HEIGHT\n"
            + "FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS_SPECIFICATIONS AS [PS]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND P.PRODUCT_ID = [PS].PRODUCT_ID";
    private static final String sqlQueryWishList = "SELECT P.*, [PI].IMAGE FROM PRODUCTS AS P, PRODUCT_IMAGE AS [PI], PRODUCTS__WISHLIST AS [PW]\n"
            + "WHERE P.PRODUCT_ID = [PI].PRODUCT_ID AND [PW].PRODUCT_ID = P.PRODUCT_ID AND ACCOUNT_ID = ?";
    private static final String countBookmarks = "SELECT COUNT(*) FROM PRODUCTS__WISHLIST WHERE ACCOUNT_ID = ?";
    private static final String staffQuery = "SELECT DISTINCT SF.[USER_NAME] FROM PRODUCTS AS P, STAFFS AS SF WHERE P.STAFF_ID = ?";
    private static final String storeQuery = "SELECT DISTINCT ST.[STORE_NAME] FROM PRODUCTS AS P, STORES AS ST WHERE P.STORE_ID = ?";
    private static final String rentalQuery = "SELECT PRD.MODE FROM PRODUCTS_RENTAL_DETAILS AS PRD, PRODUCTS AS P WHERE P.MODE = 'Rental' AND ? = PRD.PRODUCT_ID";
    private static final String totalRating = "SELECT COUNT(*) AS TOTAL_RATING_COUNT FROM PRODUCTS_RATING WHERE PRODUCT_ID = ?";
    private static final String averageRating = "SELECT AVG(RATING) AS TOTAL_RATING FROM PRODUCTS_RATING WHERE PRODUCT_ID = ?";
    private static final String stockQuery = "SELECT QUANTITY FROM PRODUCTS_STOCK WHERE PRODUCT_ID = ?";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        if (account != null) {
            int countWishList = ProductDB.countWishList(account.getAccountID(), countBookmarks);
            //Set up product show pagination
            int begin = 0;
            int end = 11;

            //Get total Pagination
            int numberPaginaton = 0;
            LinkedList<Integer> numberPaginationList = new LinkedList<>();
            if (countWishList != 0) {
                double showTotalPage = (double) end + 1.0;
                numberPaginaton = (int) Math.ceil(countWishList / showTotalPage);
                for (int i = 0; i < numberPaginaton; i++) {
                    numberPaginationList.add(i + 1);
                }
            }
            session.setAttribute("numberPaginationWishList", numberPaginationList);
            response.sendRedirect("app-product-wishlist.jsp");
        } else {
            response.sendRedirect("homepageC.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String mode = request.getParameter("mode");
        HttpSession session = request.getSession();
        Account acocunt = (Account) session.getAttribute("account");
        switch (mode) {
            case "pagePagination":
                paginationPage(request, out, acocunt);
                break;
            case "searchBookmark":
                searchBookmark(request, out, acocunt);
                break;
            case "sortOption":
                sortProduct(request, out, acocunt);
                break;
            case "modeProduct":
                modeProduct(request, out, acocunt);
                break;
            case "category":
                categoryProduct(request, out, acocunt);
                break;
            case "brand":
                brandProduct(request, out, acocunt);
                break;
            case "wishListHandle":
                handleWishList(request, out, acocunt);
                break;
            default:
                throw new AssertionError();
        }
    }

    private void paginationPage(HttpServletRequest request, PrintWriter out, Account account) {
        int pagination = Integer.parseInt(request.getParameter("page"));
        int nextPage = 13;
        int totalPage = pagination * nextPage;
        int indexPage = totalPage - 13;
        LinkedList<Product> products = ProductDB.listProduct(sqlQuery);

        for (int i = indexPage; i < totalPage; i++) {
            if (i < products.size()) {
                if (ProductDB.searchWishList(account.getAccountID(), products.get(i).getProductID()) != null) {
                    String staffName = ProductDB.searchPersonProduct(products.get(i).getStaffID(), staffQuery);
                    String storeName = ProductDB.searchPersonProduct(products.get(i).getStoreID(), storeQuery);
                    if (staffName != null) {
                        products.get(i).setStaffName(staffName);
                    } else {
                        products.get(i).setStoreName(storeName);
                    }
                    if (products.get(i).getMode().equalsIgnoreCase("Rental")) {
                        String priceMode = ProductDB.searchPersonProduct(products.get(i).getProductID(), rentalQuery);
                        products.get(i).setModeRental(priceMode);
                    }
                    products.get(i).setTotalRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), totalRating));
                    products.get(i).setAverageRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), averageRating));
                    out.print(htmlProductList(products.get(i)));
                }
            }
        }
    }

    private void searchBookmark(HttpServletRequest request, PrintWriter out, Account account) {
        HttpSession session = request.getSession();
        String searchValue = request.getParameter("inputSearch");
        String copySql = sqlQueryWishList + " AND(P.PRODUCT_NAME LIKE ? OR "
                + "P.BRAND_NAME LIKE ? OR P.CATEGORY_NAME LIKE ? "
                + "OR P.MODEL_YEAR LIKE ? OR P.MODE LIKE ? OR P.MODEL_YEAR LIKE ?)";
        LinkedList<Product> products;
        if (searchValue.length() == 0 || "".equals(searchValue)) {
            products = ProductDB.listProduct(sqlQuery);
        } else {
            products = ProductDB.listProductWishList(copySql, searchValue, account.getAccountID());
        }
        if (!products.isEmpty()) {
            for (int i = 0; i < products.size(); i++) {
                if (ProductDB.searchWishList(account.getAccountID(), products.get(i).getProductID()) != null) {
                    String staffName = ProductDB.searchPersonProduct(products.get(i).getStaffID(), staffQuery);
                    String storeName = ProductDB.searchPersonProduct(products.get(i).getStoreID(), storeQuery);
                    if (staffName != null) {
                        products.get(i).setStaffName(staffName);
                    } else {
                        products.get(i).setStoreName(storeName);
                    }
                    if (products.get(i).getMode().equalsIgnoreCase("Rental")) {
                        String priceMode = ProductDB.searchPersonProduct(products.get(i).getProductID(), rentalQuery);
                        products.get(i).setModeRental(priceMode);
                    }
                    products.get(i).setTotalRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), totalRating));
                    products.get(i).setAverageRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), averageRating));
                    out.print(htmlProductList(products.get(i)));
                }
            }
        }
    }

    private void sortProduct(HttpServletRequest request, PrintWriter out, Account account) {
        String option = request.getParameter("option");
        LinkedList<Product> products;
        int limitedSize = 0;
        String copySqlQuery = sqlQueryWishList;

        if (option.equalsIgnoreCase("Highest Price")) {
            products = ProductDB.listProductWishList(copySqlQuery + " ORDER BY P.LIST_PRICE DESC", account.getAccountID());
        } else if (option.equalsIgnoreCase("Lowest Price")) {
            products = ProductDB.listProductWishList(copySqlQuery + " ORDER BY P.LIST_PRICE ASC", account.getAccountID());
        } else if (option.equalsIgnoreCase("Newest")) {
            products = ProductDB.listProductWishList(copySqlQuery + " ORDER BY P.CREATE_AT ASC", account.getAccountID());
        } else if (option.equalsIgnoreCase("Oldest")) {
            products = ProductDB.listProductWishList(copySqlQuery + " ORDER BY P.CREATE_AT DESC", account.getAccountID());
        } else {
            option = "All";
            products = ProductDB.listProduct(sqlQuery);
        }
        if (products == null || products.isEmpty()) {
            out.print("OUT");
            return;
        }
        displayProductsFilter(products, limitedSize, option, out, request);
    }

    private void modeProduct(HttpServletRequest request, PrintWriter out, Account account) {
        String option = request.getParameter("option");
        LinkedList<Product> products;
        int limitedSize = 0;
        String copySqlQuery = sqlQueryWishList;

        if (option.equalsIgnoreCase("Rental")) {
            products = ProductDB.listProductWishList(copySqlQuery + " AND P.MODE = 'Rental'", account.getAccountID());
        } else if (option.equalsIgnoreCase("Sell")) {
            products = ProductDB.listProductWishList(copySqlQuery + " AND P.MODE = 'Sell'", account.getAccountID());
        } else {
            option = "All";
            products = ProductDB.listProduct(sqlQuery);
        }
        if (products == null || products.isEmpty()) {
            out.print("OUT");
            return;
        }
        displayProductsFilter(products, limitedSize, option, out, request);
    }

    private void categoryProduct(HttpServletRequest request, PrintWriter out, Account account) {
        String option = request.getParameter("option");
        LinkedList<Product> products;
        int limitedSize = 0;
        String copySqlQuery = sqlQueryWishList;

        if (option.equalsIgnoreCase("Ascending")) {
            products = ProductDB.listProductWishList(copySqlQuery + " ORDER BY P.CATEGORY_NAME ASC", account.getAccountID());
        } else if (option.equalsIgnoreCase("Descending")) {
            products = ProductDB.listProductWishList(copySqlQuery + " ORDER BY P.CATEGORY_NAME DESC", account.getAccountID());
        } else {
            option = "All";
            products = ProductDB.listProduct(sqlQuery);
        }
        if (products == null || products.isEmpty()) {
            out.print("OUT");
            return;
        }
        displayProductsFilter(products, limitedSize, option, out, request);
    }

    private void brandProduct(HttpServletRequest request, PrintWriter out, Account account) {
        String option = request.getParameter("option");
        LinkedList<Product> products;
        int limitedSize = 0;
        String copySqlQuery = sqlQueryWishList;

        if (option.equalsIgnoreCase("Ascending")) {
            products = ProductDB.listProductWishList(copySqlQuery + " ORDER BY P.BRAND_NAME ASC", account.getAccountID());
        } else if (option.equalsIgnoreCase("Descending")) {
            products = ProductDB.listProductWishList(copySqlQuery + " ORDER BY P.BRAND_NAME DESC", account.getAccountID());
        } else {
            option = "All";
            products = ProductDB.listProduct(sqlQuery);
        }
        if (products == null || products.isEmpty()) {
            out.print("OUT");
            return;
        }
        displayProductsFilter(products, limitedSize, option, out, request);
    }

    private void handleWishList(HttpServletRequest request, PrintWriter out, Account account) {
        String optionChoice = request.getParameter("choice");
        int productID = Integer.parseInt(request.getParameter("productID"));
        Product productName = ProductDB.searchProduct("SELECT P.* FROM PRODUCTS AS P WHERE P.PRODUCT_ID = ?", productID);
        
        if (optionChoice.equalsIgnoreCase("removeWishList")) {
            String sqlQuery = "DELETE FROM PRODUCTS__WISHLIST WHERE PRODUCT_ID = ? AND ACCOUNT_ID = ?";
            int status = ProductDB.updateWishList(productID, account.getAccountID(), sqlQuery);
            Manager.activyAccountUpdate(account.getAccountID(), "Remove Wishlist", "You remove a product " + productName.getName() + " from wishlist");
        }
        out.print("SUCCESS");
    }

    private void displayProductsFilter(LinkedList<Product> products, int limitedSize, String option, PrintWriter out, HttpServletRequest request) {
        if (option.equalsIgnoreCase("All")) {
            limitedSize = 9;
        } else {
            limitedSize = products.size();
        }
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        for (int i = 0; i < limitedSize; i++) {
            if (ProductDB.searchWishList(account.getAccountID(), products.get(i).getProductID()) != null) {
                String staffName = ProductDB.searchPersonProduct(products.get(i).getStaffID(), staffQuery);
                String storeName = ProductDB.searchPersonProduct(products.get(i).getStoreID(), storeQuery);
                if (staffName != null) {
                    products.get(i).setStaffName(staffName);
                } else {
                    products.get(i).setStoreName(storeName);
                }
                if (products.get(i).getMode().equalsIgnoreCase("Rental")) {
                    String priceMode = ProductDB.searchPersonProduct(products.get(i).getProductID(), rentalQuery);
                    products.get(i).setModeRental(priceMode);
                }
                products.get(i).setTotalRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), totalRating));
                products.get(i).setAverageRating(ProductDB.searchPersonProduct(products.get(i).getProductID(), averageRating));
                products.get(i).setMarked(ProductDB.searchWishList(account.getAccountID(), products.get(i).getProductID()));
                out.print(htmlProductList(products.get(i)));
            }
        }
    }

    private String htmlProductList(Product product) {
        int stockRemaining = ProductDB.countInformations(stockQuery, product.getProductID());
        String disabledAdd = "";
        String addToCart = "Add to cart";
        if (stockRemaining == 0) {
            disabledAdd = "disabled";
            addToCart = "Sold out";
        }
        String starProduct = "";
        if (product.getAverageRating() != null) {
            switch (product.getAverageRating().toUpperCase()) {
                case "5":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
                case "4":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
                case "3":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
                case "2":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
                case "1":
                    starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star filled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                            + "</ul>\n"
                            + "<span class=\"card-text\">(" + product.getTotalRating() + " Reviews)</span>\n";
                    break;
            }
        } else {
            starProduct = "<ul class=\"unstyled-list list-inline\">\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "<li class=\"ratings-list-item\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-star unfilled-star\"><polygon points=\"12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2\"></polygon></svg></li>\n"
                    + "</ul>"
                    + "<span class=\"card-text\">(" + product.getTotalRating() + " Review)</span>\n";
        }

        String modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "</h6>";
        if (product.getModeRental() != null) {
            switch (product.getModeRental().toUpperCase()) {
                case "DAY":
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "/Day</h6>";
                    break;
                case "WEEK":
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "/Week</h6>";
                    break;
                case "MONTH":
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "/Month</h6>";
                    break;
                case "YEAR":
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "/Year</h6>";
                    break;
                default:
                    modeRental = "<h6 class=\"item-price\">$" + product.getPrice() + "</h6>";
                    break;
            }
        }
        String userPost;
        if (product.getStaffName() != null) {
            userPost = "<div class=\"card-text mt-1\" style=\"margin-top:0.25rem\">Post By <span style=\"color: #7367F0;\" class=\"company-name\">" + product.getStaffName() + "</span></div>";
        } else {
            userPost = "<div class=\"card-text mt-1\" style=\"margin-top:0.25rem\">Post By <span style=\"color: #7367F0;\" class=\"company-name\">" + product.getStoreName() + "</span></div>";
        }

        String rentalMode = "";
        if (product.getMode() != null) {
            if ("Rental".equalsIgnoreCase(product.getMode())) {
                rentalMode = "<div class=\"lidget-card-ecomerce\">\n"
                        + "Rental Only\n"
                        + "</div>";
            }
        }
        String html = "<div class=\"card ecommerce-card position-relative\" data=\"" + product.getProductID() + "\">\n"
                + "<div class=\"item-img text-center\" style=\"padding-top:0;min-height:unset;\">\n"
                + "<a href=\"productDetailController?mx=" + product.getProductID() + "\">\n"
                + "<img class=\"img-fluid card-img-top\" src=\"" + product.getImage() + "\"\n"
                + "alt=\"img-placeholder\" /></a>\n"
                + "</div>\n"
                + "<div class=\"card-body\">\n"
                + "<div class=\"item-wrapper\">\n"
                + "<div class=\"item-rating flex-default flex-direction-column\" style=\"row-gap:0.5rem\">\n"
                + starProduct
                + "</div>\n"
                + "<div>\n"
                + modeRental
                + "</div>\n"
                + "</div>\n"
                + "<h6 class=\"item-name\">\n"
                + "<div class=\"text-muted\">Product ID: MX-00" + product.getProductID() + "</div>\n"
                + "<a class=\"text-body mt-1\" href=\"app-ecommerce-details.html\">" + product.getName() + " " + product.getModelYear() + " " + "</a>\n"
                + "<div class=\"card-text\" style=\"margin-top:0.25rem\">" + product.getCategory() + " Version</div>\n"
                + "<div class=\"card-text\" style=\"margin-top:0.25rem\">By \n"
                + "<div class=\"company-name\" style=\"display:inline-block\">" + product.getBrand() + "</div>\n"
                + "</div>\n"
                + userPost
                + "</h6>\n"
                + "<p class=\"card-text item-description\">\n"
                + "" + product.getDescription() + "\n"
                + "</p>\n"
                + "</div>\n"
                + "<div class=\"item-options text-center\">\n"
                + "<div class=\"item-wrapper\">\n"
                + "<div class=\"item-cost\">\n"
                + "<h4 class=\"item-price\">$" + product.getPrice() + "</h4>\n"
                + "</div>\n"
                + "</div>\n"
                + "<a href=\"#/\" class=\"btn btn-light btn-wishlist waves-effect waves-float waves-light remove-wishlist\" index-data=\"" + product.getProductID() + "\" onclick=\"sendToRemoveWishList(this)\">\n"
                + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x\"><line x1=\"18\" y1=\"6\" x2=\"6\" y2=\"18\"></line><line x1=\"6\" y1=\"6\" x2=\"18\" y2=\"18\"></line></svg>"
                + "<span>Remove</span>\n"
                + "</a>"
                + "<a href=\"#/\" class=\"btn btn-primary btn-cart waves-effect waves-float waves-light  " + disabledAdd + "\" index-data=\"" + product.getProductID() + "\" onclick=\"sendToMoveCart(this)\">\n"
                + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-shopping-cart\"><circle cx=\"9\" cy=\"21\" r=\"1\"></circle><circle cx=\"20\" cy=\"21\" r=\"1\"></circle><path d=\"M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6\"></path></svg>\n"
                + "<span class=\"add-to-cart\">" + addToCart + "</span>\n"
                + "</a>"
                + "</div>\n"
                + rentalMode
                + "</div>";
        return html;
    }
}
