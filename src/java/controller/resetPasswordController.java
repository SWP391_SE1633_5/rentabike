/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this templates
 */
package controller;

import Utils.Utils;
import com.google.gson.Gson;
import dao.TokenResetPasswordDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Scanner;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.Account;

/**
 *
 * @author win
 */
@WebServlet(name = "ResetPassword", urlPatterns = {"/reset-password"})
public class resetPasswordController extends HttpServlet {

    public Properties mailProperties() {
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.ssl.protocols", "TLSv1.2");
        return props;
    }

    public boolean sendAuthoEmail(HttpServletRequest request, HttpServletResponse response, String email, String token) throws ServletException, IOException {
        ArrayList<String> accountEmail = new ArrayList<>();
        try {
            File myObj = new File("D:\\code\\rentabike\\config\\configEmail.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                accountEmail.add(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        
        
        // Recipient's email ID needs to be mentioned.
        String to = email;
        // Sender's email ID needs to be mentioned
        String from = accountEmail.get(0);
        // Assuming you are sending email from localhost
        String host = "localhost";
        // Get system properties
        Properties props = mailProperties();
        // Get the default Session object.
        Session mailSession = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                       accountEmail.get(0), accountEmail.get(1));// Specify the Username and the PassWord
            }
        });
        try {
            String linkToken = "<a href=\"http://localhost:8088/Rentabike/reset-password?token=" + token + "\">Reset Password now!</a>";
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(mailSession);
            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));
            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            // Set Subject: header field
            message.setSubject("Reset your password!");
            // Now set the actual message
            message.setContent(linkToken, "text/html");
            // Send message
            Transport.send(message);
            return true;
        } catch (MessagingException mex) {
            return false;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String token = request.getParameter("token");
        if (token == null || token.equals("")) {
            request.getRequestDispatcher("SubmitEmailResetPassword.html").forward(request, response);
            return;
        }
        request.getRequestDispatcher("ResetPassword.html").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HashMap<Object, Object> map = new HashMap<>();
        Gson gson = new Gson();
        PrintWriter out = response.getWriter();
        boolean checkSend = false;
        UserDao ud = new UserDao();
        TokenResetPasswordDao tkd = new TokenResetPasswordDao();
        String pattern = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");
        String tokenReq = request.getParameter("token");

        //submitemailresetpassword
        if (email != null) {
            if (!email.matches(pattern)) {
                map.put("SC", -1);
                map.put("Mess", "Email wrong format!");
                out.print(gson.toJson(map));
                out.flush();
                return;
            }
            boolean emailExist = ud.checkEmailExist(email);
            if (!emailExist) {
                map.put("SC", -1);
                map.put("Mess", "Email not exist!");
                out.print(gson.toJson(map));
                out.flush();
                return;
            }
            if (emailExist) {
                Account a = ud.findAccountByEmail(email);
                tkd.delteTSRPasswordByAccountId(a.getAccountID());
                String token = "";
                String dateTime = Utils.getCurrentDateTime();
                do {
                    token = Utils.generateCode();
                    if (!tkd.checkTokenSubmitEmailExist(token)) {
                        break;
                    }
                } while (true);
                checkSend = sendAuthoEmail(request, response, email, token);
                tkd.saveTokenSubmitEmail(token, dateTime, ud.findAccountByEmail(email).getAccountID());
                if (checkSend) {
                    map.put("SC", 0);
                    map.put("Mess", "Email sent successfully!");
                    out.print(gson.toJson(map));
                    out.flush();
                } else {
                    map.put("SC", -1);
                    map.put("Mess", "Email send fail!");
                    out.print(gson.toJson(map));
                    out.flush();
                }
            }
            return;
        }
        //Reset password
        if (!tkd.checkTokenSubmitEmailExist(tokenReq)) {
            map.put("SC", -1);
            map.put("Mess", "Token is not exist!!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        String currentTime = Utils.getCurrentDateTime();
        String createdTime = tkd.findTSRPassworByToken(tokenReq).getCreatedTime();
        int dis = Utils.getDistanceTime(createdTime, currentTime);
        if (dis >= 1) {
            map.put("SC", -1);
            map.put("Mess", "Your token is expired!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        if (!password.equals(confirmPassword)) {
            map.put("SC", -1);
            map.put("Mess", "Two password doesn't match another!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        ud.resetPassword(tokenReq, password);
        tkd.deleteTSRPasswordByToken(tokenReq);
        map.put("SC", 0);
        map.put("Mess", "Reset password successfully!");
        out.print(gson.toJson(map));
        out.flush();
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
