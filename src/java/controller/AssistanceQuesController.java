/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import Utils.Utils;
import com.google.gson.Gson;
import dao.AssitanceQuesDao;
import dao.BlogDao;
import dao.StaffDB;
import dao.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.Account;
import model.AssistanceQues;
import model.Blog;

/**
 *
 * @author win
 */
@WebServlet(name="AssistanceQuesController", urlPatterns={"/assistance-quest"})
public class AssistanceQuesController extends HttpServlet {
   
    HashMap<Object, Object> map = new HashMap<>();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            response.sendRedirect("started.jsp");
            return;
        }
        if(!a.getRole().equals("ADMIN")){
            response.sendRedirect("app-product-shop.jsp");
            return;
        }
        String type = request.getParameter("type");
        if(type==null || type.equals("")){
            request.getRequestDispatcher("AssistanceAdminControl.html").forward(request, response);
            return;
        }
        List<Object> dataList = new ArrayList<>();
        if(type.equals("getAll")){
            List<AssistanceQues> aqList = AssitanceQuesDao.getAllAssistanceQues();
            for(AssistanceQues aq: aqList){
                HashMap<Object, Object> mapTmp = new HashMap<>();
                if(UserDB.searchAccountID(aq.getAccountId())!=null){
                    mapTmp.put("Account", UserDB.searchAccountID(aq.getAccountId()));
                } else if(StaffDB.searchAccountID(aq.getAccountId())!=null){
                    mapTmp.put("Account", StaffDB.searchAccountID(aq.getAccountId()));
                }
                mapTmp.put("AssistanceQues", aq);
                dataList.add(mapTmp);
            }
            
            map.put("SC", 1);
            map.put("Data", dataList);
            out.print(gson.toJson(map));
            out.flush();
        }
        if(type.equals("Search")){
            String search = request.getParameter("search");
            List<AssistanceQues> aqList = AssitanceQuesDao.searchAllAssistanceQuesBySearch(search);
            for(AssistanceQues aq: aqList){
                HashMap<Object, Object> mapTmp = new HashMap<>();
                if(UserDB.searchAccountID(aq.getAccountId())!=null){
                    mapTmp.put("Account", UserDB.searchAccountID(aq.getAccountId()));
                } else if(StaffDB.searchAccountID(aq.getAccountId())!=null){
                    mapTmp.put("Account", StaffDB.searchAccountID(aq.getAccountId()));
                }
                mapTmp.put("AssistanceQues", aq);
                dataList.add(mapTmp);
            }
            
            map.put("SC", 1);
            map.put("Data", dataList);
            out.print(gson.toJson(map));
            out.flush();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        map.clear();
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        if(a==null){
            response.sendRedirect("started.jsp");
            return;
        }
        String type = request.getParameter("type");
        if(type.equals("add-new")){  //15 min to continue to post assist-ques
            String content = request.getParameter("content");
            int accId = a.getAccountID();
            String createdAt = Utils.getCurrentDateTime();
            
            AssistanceQues aq = AssitanceQuesDao.findAssistanceQuesByAssQID(accId);
            if(aq!=null){
                int dis = (Utils.convertDateToInt(createdAt)-Utils.convertDateToInt(aq.getCreatedTime()))/1000/60;
                if(dis<15){
                    map.put("SC", -1);
                    map.put("ME", "You can post new in "+(15-dis)+" minutes!");
                    out.print(gson.toJson(map));
                    out.flush();
                    return;
                }
            }
            AssitanceQuesDao.addNewAssistanceQues(content, accId, createdAt);
            map.put("SC", 1);
            map.put("ME", "Add Success!");
            out.print(gson.toJson(map));
            out.flush();
            return;
        }
        if(!a.getRole().equals("ADMIN")){
            response.sendRedirect("app-product-shop.jsp");
            return;
        }
        
        if(type.equals("handle-delete")){
            String[] chooseCheck = request.getParameterValues("checkResultArr[]");
            AssitanceQuesDao.deleteAssistanceQuesByListAssQId(chooseCheck);
            map.put("SC", 1);
            map.put("ME", "Delete success!");
            out.print(gson.toJson(map));
            out.flush();
        }
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
