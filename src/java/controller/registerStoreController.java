/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;
import dao.*;
import model.*;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


/**
 *
 * @author baqua
 */
public class registerStoreController extends HttpServlet { 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        int accountID = account.getAccountID();
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String emailAddress = request.getParameter("email");
        String city = request.getParameter("city");
        String street = request.getParameter("street");
        String country = request.getParameter("country");
        String contact = request.getParameter("contact");
        
        String image = request.getParameter("image");
        String storename = request.getParameter("storename");
        String description = request.getParameter("description");
        int status = 3;
        Store s = new Store(accountID, storename, contact, emailAddress, city, street, description, country);
        StoreDB sd = new StoreDB();
        sd.registerNewStore(s);
        
        response.sendRedirect("app-user-view-account.jsp");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
