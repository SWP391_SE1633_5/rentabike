/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.Base64;
import javax.imageio.ImageIO;

/**
 *
 * @author ADMIN
 */
public class Staff {

    private int accountID;
    private int staffID;
    private String email;
    private String pinCode;
    private String userName;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String avatar;
    private Date dateOfJoin;
    private int active;
    private String city;
    private String street;
    private String state;
    private String permission;
    private String permissionDetails;
    private String role;

    public Staff() {
    }

    public Staff(int accountID, int staffID, String email, String pinCode, String userName, String firstName, String lastName, String phoneNumber, String avatar, Date dateOfJoin, int active, String city, String street, String state, String permission, String permissionDetails, String role) {
        this.accountID = accountID;
        this.staffID = staffID;
        this.email = email;
        this.pinCode = pinCode;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.avatar = avatar;
        this.dateOfJoin = dateOfJoin;
        this.active = active;
        this.city = city;
        this.street = street;
        this.state = state;
        this.permission = permission;
        this.permissionDetails = permissionDetails;
        this.role = role;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public int getStaffID() {
        return staffID;
    }

    public void setStaffID(int staffID) {
        this.staffID = staffID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getDateOfJoin() {
        return dateOfJoin;
    }

    public void setDateOfJoin(Date dateOfJoin) {
        this.dateOfJoin = dateOfJoin;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getPermissionDetails() {
        return permissionDetails;
    }

    public void setPermissionDetails(String permissionDetails) {
        this.permissionDetails = permissionDetails;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    public static String convertByteArrayToBase64(byte[] bytes) {
        return "data:image/;base64," + Base64.getEncoder().encodeToString(bytes);
    }
}
