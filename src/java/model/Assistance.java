/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this templates
 */
package model;

/**
 *
 * @author win
 */
public class Assistance {
    int id;
    String question;
    String answer;
    String createdAt;
    String updatedAd;
    int usefull, noUsefull;

    public Assistance() {
    }

    public Assistance(int id, String question, String answer, String createdAt, String updatedAd, int usefull, int noUsefull) {
        this.id = id;
        this.question = question;
        this.answer = answer;
        this.createdAt = createdAt;
        this.updatedAd = updatedAd;
        this.usefull = usefull;
        this.noUsefull = noUsefull;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAd() {
        return updatedAd;
    }

    public void setUpdatedAd(String updatedAd) {
        this.updatedAd = updatedAd;
    }

    public int getUsefull() {
        return usefull;
    }

    public void setUsefull(int usefull) {
        this.usefull = usefull;
    }

    public int getNoUsefull() {
        return noUsefull;
    }

    public void setNoUsefull(int noUsefull) {
        this.noUsefull = noUsefull;
    }
    
}
