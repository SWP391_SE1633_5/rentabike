/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
public class PermissionsDetail {

    private int idPremission;
    private String name;
    private String assignTo;
    private String createDate;

    public PermissionsDetail() {
    }

    public PermissionsDetail(int idPremission, String name, String assignTo, String createDate) {
        this.idPremission = idPremission;
        this.name = name;
        this.assignTo = assignTo;
        this.createDate = createDate;
    }

    public int getIdPremission() {
        return idPremission;
    }

    public void setIdPremission(int idPremission) {
        this.idPremission = idPremission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return this.name + "LGN" + this.assignTo + "LGN" + this.createDate + "LGN"; 
    }

}
