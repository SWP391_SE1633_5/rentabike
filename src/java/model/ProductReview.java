/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
public class ProductReview {

    private int storeID;
    private int staffID;
    private int productID; 
    private String commentReview; 
    private String commentDate; 
    private int accountID; 
    private int ratingStar; 
    private String imageAccount; 
    private String accountName; 
    private String accountRole; 

    public ProductReview() {
    }

    public ProductReview(int storeID, int staffID, int productID, String commentReview, String commentDate, int accountID, int ratingStar, String imageAccount, String accountName, String accountRole) {
        this.storeID = storeID;
        this.staffID = staffID;
        this.productID = productID;
        this.commentReview = commentReview;
        this.commentDate = commentDate;
        this.accountID = accountID;
        this.ratingStar = ratingStar;
        this.imageAccount = imageAccount;
        this.accountName = accountName;
        this.accountRole = accountRole;
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public int getStaffID() {
        return staffID;
    }

    public void setStaffID(int staffID) {
        this.staffID = staffID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getCommentReview() {
        return commentReview;
    }

    public void setCommentReview(String commentReview) {
        this.commentReview = commentReview;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public int getRatingStar() {
        return ratingStar;
    }

    public void setRatingStar(int ratingStar) {
        this.ratingStar = ratingStar;
    }

    public String getImageAccount() {
        return imageAccount;
    }

    public void setImageAccount(String imageAccount) {
        this.imageAccount = imageAccount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountRole() {
        String roleModifier = accountRole.substring(0, 1).concat(accountRole.substring(1).toLowerCase());
        return roleModifier;
    }

    public void setAccountRole(String accountRole) {
        this.accountRole = accountRole;
    }

}
