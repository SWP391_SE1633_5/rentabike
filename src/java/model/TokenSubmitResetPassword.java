/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this templates
 */
package model;

/**
 *
 * @author win
 */
public class TokenSubmitResetPassword {
    int id;
    String token;
    String createdTime;
    int accountId;

    public TokenSubmitResetPassword() {
    }

    public TokenSubmitResetPassword(int id, String token, String createdTime, int accountId) {
        this.id = id;
        this.token = token;
        this.createdTime = createdTime;
        this.accountId = accountId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
    
    
}
