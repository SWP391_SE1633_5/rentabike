/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.Date;
import java.util.Base64;
import javax.imageio.ImageIO;

/**
 *
 * @author ADMIN
 */
public class User {

    private int customerID;
    private int accountID;
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String avatar;
    private String city;
    private String street;
    private String state;
    private Date dateOfJoin;
    private String role;
    private String verify;
    private int status;
    private String permission;
    private String permissionDetails;
    private String zipCode;

    public User() {
    }

    public User(int customerID, int accountID, String email, String firstName, String lastName, String phoneNumber, String avatar, String city, String street, String state, Date dateOfJoin, String role, String verify, int status, String permission, String permissionDetails, String zipCode) {
        this.customerID = customerID;
        this.accountID = accountID;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.avatar = avatar;
        this.city = city;
        this.street = street;
        this.state = state;
        this.dateOfJoin = dateOfJoin;
        this.role = role;
        this.verify = verify;
        this.status = status;
        this.permission = permission;
        this.permissionDetails = permissionDetails;
        this.zipCode = zipCode;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getDateOfJoin() {
        return dateOfJoin;
    }

    public void setDateOfJoin(Date dateOfJoin) {
        this.dateOfJoin = dateOfJoin;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getPermissionDetails() {
        return permissionDetails;
    }

    public void setPermissionDetails(String permissionDetails) {
        this.permissionDetails = permissionDetails;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public static String convertByteArrayToBase64(byte[] bytes) {
        return "data:image/;base64," + Base64.getEncoder().encodeToString(bytes);
    }

    public static byte[] convertUrlToByteArray(String urlImage) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = null;
        try {
            URL url = new URL(urlImage);
            is = url.openStream();
            byte[] byteChunk = new byte[4096];
            int n;

            while ((n = is.read(byteChunk)) > 0) {
                baos.write(byteChunk, 0, n);
            }
        } catch (IOException e) {
        }
        return baos.toByteArray();
    }

    public static byte[] convertToByteArray(String base64) {
        byte[] decodedString = null;
        try {
            byte[] name = Base64.getEncoder().encode(base64.getBytes());
            decodedString = Base64.getDecoder().decode(new String(name).getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
        }
        return decodedString;
    }
}
