/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.OrderDB;
import dao.ProductDB;

/**
 *
 * @author ASUS
 */
public class OrderItem {
    int id;
    int orderID;
    int productID;
    int customerID;
    int storeID;
    int quantity;
    int status;
    String orderDate;
    String shippedDate;
    double price;
    String address;
    String phoneNum;
    String storeName;
    String email;
    int rentalDateNum;
    Product p ;

    public int getRentalDateNum() {
        return rentalDateNum;
    }

    public void setRentalDateNum(int rentalDateNum) {
        this.rentalDateNum = rentalDateNum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public Product getP() {
        return p;
    }

    public void setP(Product p) {
        this.p = p;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public OrderItem(int orderID, int productID, int customerID, int storeID, int quantity, String orderDate, double price) {
        this.orderID = orderID;
        this.productID = productID;
        this.customerID = customerID;
        this.storeID = storeID;
        this.quantity = quantity;
        this.orderDate = orderDate;
        this.price = price;
    }

    public OrderItem(int rentalDateNum,int id,int orderID, int productID, int customerID, int storeID, int quantity, int status, String orderDate, String shippedDate, double price) {
        this.rentalDateNum = rentalDateNum;
        this.id = id;
        this.orderID = orderID;
        this.productID = productID;
        this.customerID = customerID;
        this.storeID = storeID;
        this.quantity = quantity;
        this.status = status;
        this.orderDate = orderDate;
        this.shippedDate = shippedDate;
        this.price = price;
    }

    
    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(String shippedDate) {
        this.shippedDate = shippedDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
}
