/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ASUS
 */
public class Review {

    int storeId;
    int productId;
    String comment;
    String authorName;
    int author;
    String role;
    int ratingStar;
    int reviewId;

    public Review(int storeId, int productId, String comment, int author, String authorName, String role) {
        this.storeId = storeId;
        this.productId = productId;
        this.comment = comment;
        this.author = author;
        this.authorName = authorName;
        this.role = role;
        
    }
    
    public Review(int storeId, int productId, String comment, int author, String authorName, String role,int ratingStar) {
        this.storeId = storeId;
        this.productId = productId;
        this.comment = comment;
        this.author = author;
        this.authorName = authorName;
        this.role = role;
        this.ratingStar = ratingStar;
        
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public int getRatingStar() {
        return ratingStar;
    }

    public void setRatingStar(int ratingStar) {
        this.ratingStar = ratingStar;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Review() {
    }

    public int getStoreId() {
        return storeId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

}
