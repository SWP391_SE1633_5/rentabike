/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this templates
 */
package model;

/**
 *
 * @author win
 */
public class BlogCommentReact {
    int id, accountId, commentId, reactId;
    String timeReact;

    public BlogCommentReact() {
    }

    public BlogCommentReact(int id, int accountId, int commentId, int reactId, String timeReact) {
        this.id = id;
        this.accountId = accountId;
        this.commentId = commentId;
        this.reactId = reactId;
        this.timeReact = timeReact;
    }

    public String getTimeReact() {
        return timeReact;
    }

    public void setTimeReact(String timeReact) {
        this.timeReact = timeReact;
    }

    public int getReactId() {
        return reactId;
    }

    public void setReactId(int reactId) {
        this.reactId = reactId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }
    
}
