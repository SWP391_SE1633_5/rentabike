/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
public class UserService {
    private int AuthID;
    private int UserID;
    private String Key;
    private String expired;

    public UserService() {
    }

    public UserService(int AuthID, int UserID, String Key, String expired) {
        this.AuthID = AuthID;
        this.UserID = UserID;
        this.Key = Key;
        this.expired = expired;
    }

    public int getAuthID() {
        return AuthID;
    }

    public void setAuthID(int AuthID) {
        this.AuthID = AuthID;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String Key) {
        this.Key = Key;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

}