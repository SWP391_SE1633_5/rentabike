/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Base64;

/**
 *
 * @author ADMIN
 */
public class Product {

    private int productID;
    private String name;
    private String brand;
    private String category;
    private int modelYear;
    private float price;
    private String description;
    private int status;
    private String mode;
    private int displacement;
    private String transmission;
    private String starter;
    private String fuelSystem;
    private float fuelCapacity;
    private float dryWeight;
    private float seatHeight;
    private String image;
    private int staffID;
    private int storeID;
    private String modeRental;
    private String staffName;
    private String storeName;
    private String totalRating;
    private String averageRating;
    private String createAt;
    private String marked;
    private String markedCheckOut;
    private String deposit;
    private String requiredDocs;
    private String miliageLimit;
    private String minimumRental;
    private int touringMode;
    private int productStock;

    public Product() {
    }

    public Product(int productID, String name, String brand, String category, int modelYear, float price, String description, int status, String mode, int displacement, String transmission, String starter, String fuelSystem, float fuelCapacity, float dryWeight, float seatHeight, String image, int staffID, int storeID, String modeRental, String staffName, String storeName, String totalRating, String averageRating, String createAt, String marked, String deposit, String requiredDocs, String miliageLimit, String minimumRental, int touringMode, int productStock) {
        this.productID = productID;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.modelYear = modelYear;
        this.price = price;
        this.description = description;
        this.status = status;
        this.mode = mode;
        this.displacement = displacement;
        this.transmission = transmission;
        this.starter = starter;
        this.fuelSystem = fuelSystem;
        this.fuelCapacity = fuelCapacity;
        this.dryWeight = dryWeight;
        this.seatHeight = seatHeight;
        this.image = image;
        this.staffID = staffID;
        this.storeID = storeID;
        this.modeRental = modeRental;
        this.staffName = staffName;
        this.storeName = storeName;
        this.totalRating = totalRating;
        this.averageRating = averageRating;
        this.createAt = createAt;
        this.marked = marked;
        this.deposit = deposit;
        this.requiredDocs = requiredDocs;
        this.miliageLimit = miliageLimit;
        this.minimumRental = minimumRental;
        this.touringMode = touringMode;
        this.productStock = productStock;
    }
    
    public Product(int productID, String name, String brand, String category, int modelYear, float price, String description, int status, String mode, int displacement, String transmission, String starter, String fuelSystem, float fuelCapacity, float dryWeight, float seatHeight, String image, int staffID, int storeID) {
        this.productID = productID;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.modelYear = modelYear;
        this.price = price;
        this.description = description;
        this.status = status;
        this.mode = mode;
        this.displacement = displacement;
        this.transmission = transmission;
        this.starter = starter;
        this.fuelSystem = fuelSystem;
        this.fuelCapacity = fuelCapacity;
        this.dryWeight = dryWeight;
        this.seatHeight = seatHeight;
        this.image = image;
        this.staffID = staffID;
        this.storeID = storeID;
    }

    public Product( String name, String brand, String category, int modelYear, float price, String mode, int displacement, String transmission, String starter, String fuelSystem, float fuelCapacity, float dryWeight, float seatHeight, String modeRental) {
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.modelYear = modelYear;
        this.price = price;
        this.mode = mode;
        this.displacement = displacement;
        this.transmission = transmission;
        this.starter = starter;
        this.fuelSystem = fuelSystem;
        this.fuelCapacity = fuelCapacity;
        this.dryWeight = dryWeight;
        this.seatHeight = seatHeight;
        this.modeRental = modeRental;
    }

    public Product(int productID, String name, String brand, String category, int modelYear, float price, String description,int status, String mode,int staffID, int storeID) {
        this.productID = productID;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.modelYear = modelYear;
        this.price = price;
        this.description = description;
        this.mode = mode;
        this.status = status;
        this.staffID = staffID;
        this.storeID = storeID;
    }
    public Product(int productID, String name, String brand, String category, int modelYear, float price, String description, int status, String mode, int staffID, int storeID, String createAt) {
        this.productID = productID;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.modelYear = modelYear;
        this.price = price;
        this.description = description;
        this.status = status;
        this.mode = mode;
        this.staffID = staffID;
        this.storeID = storeID;
        this.createAt = createAt;
        
    }
    public Product( String name, String brand, String category, int modelYear, float price, String mode, int displacement, String transmission, String starter, String fuelSystem, float fuelCapacity, float dryWeight, float seatHeight, String modeRental, String description,int storeID) {
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.modelYear = modelYear;
        this.price = price;
        this.mode = mode;
        this.displacement = displacement;
        this.transmission = transmission;
        this.starter = starter;
        this.fuelSystem = fuelSystem;
        this.fuelCapacity = fuelCapacity;
        this.dryWeight = dryWeight;
        this.seatHeight = seatHeight;
        this.modeRental = modeRental;
        this.description = description;
        this.storeID = storeID;
    }

    public String getMarkedCheckOut() {
        return markedCheckOut;
    }

    public void setMarkedCheckOut(String markedCheckOut) {
        this.markedCheckOut = markedCheckOut;
    }

     public int getProductStock() {
        return productStock;
    }

    public void setProductStock(int productStock) {
        this.productStock = productStock;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getModelYear() {
        return modelYear;
    }

    public void setModelYear(int modelYear) {
        this.modelYear = modelYear;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public int getDisplacement() {
        return displacement;
    }

    public void setDisplacement(int displacement) {
        this.displacement = displacement;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getStarter() {
        return starter;
    }

    public void setStarter(String starter) {
        this.starter = starter;
    }

    public String getFuelSystem() {
        return fuelSystem;
    }

    public void setFuelSystem(String fuelSystem) {
        this.fuelSystem = fuelSystem;
    }

    public float getFuelCapacity() {
        return fuelCapacity;
    }

    public void setFuelCapacity(float fuelCapacity) {
        this.fuelCapacity = fuelCapacity;
    }

    public float getDryWeight() {
        return dryWeight;
    }

    public void setDryWeight(float dryWeight) {
        this.dryWeight = dryWeight;
    }

    public float getSeatHeight() {
        return seatHeight;
    }

    public void setSeatHeight(float seatHeight) {
        this.seatHeight = seatHeight;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStaffID() {
        return staffID;
    }

    public void setStaffID(int staffID) {
        this.staffID = staffID;
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public String getModeRental() {
        return modeRental;
    }

    public void setModeRental(String modeRental) {
        this.modeRental = modeRental;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(String totalRating) {
        this.totalRating = totalRating;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getMarked() {
        return marked;
    }

    public void setMarked(String marked) {
        this.marked = marked;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getRequiredDocs() {
        return requiredDocs;
    }

    public void setRequiredDocs(String requiredDocs) {
        this.requiredDocs = requiredDocs;
    }

    public String getMiliageLimit() {
        return miliageLimit;
    }

    public void setMiliageLimit(String miliageLimit) {
        this.miliageLimit = miliageLimit;
    }

    public String getMinimumRental() {
        return minimumRental;
    }

    public void setMinimumRental(String minimumRental) {
        this.minimumRental = minimumRental;
    }

    public int getTouringMode() {
        return touringMode;
    }

    public void setTouringMode(int touringMode) {
        this.touringMode = touringMode;
    }

    public static String convertByteArrayToBase64(byte[] bytes) {
        return "data:image/;base64," + Base64.getEncoder().encodeToString(bytes);
    }
}
