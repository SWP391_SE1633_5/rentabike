/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author baqua
 */
public class Store {
    private int storeID;
    
    private int accountID;
    private String storeName;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String city;
    private String street;
    private String description;
    private int status;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    private String state;
    private String role;

    public Store() {
        
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getStoreID() {
        return storeID;
    }

    public int getAccountID() {
        return accountID;
    }

    public String getStoreName() {
        return storeName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getState() {
        return state;
    }

    public String getRole() {
        return role;
    }

    public Store(int storeID, int accountID, String storeName, String phoneNumber, String email, String city, String street, String state, String role,String firstName, String lastName, int status) {
        this.storeID = storeID;
        this.accountID = accountID;
        this.storeName = storeName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.city = city;
        this.street = street;
        this.state = state;
        this.role = role;
        this.status = status;
    }

    public Store(int accountID, String storeName, String phoneNumber, String email, String city, String street, String description, String state) {
        this.accountID = accountID;
        this.storeName = storeName;
       this.phoneNumber = phoneNumber;
        this.email = email;
        this.city = city;
        this.street = street;
        this.description = description;
        
        this.state = state;
        
    }
    
    
    
}
