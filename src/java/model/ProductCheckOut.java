/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
public class ProductCheckOut {
    private int accountID;
    private int storeID;
    private int productID;
    private int totalMRP;
    private int bagDiscount;
    private float estimatedTax;
    private int totalPrice;
    private int offerPrice;
    private int contractID;
    private String deliveryStatus;
    private String createdDate;

    public ProductCheckOut() {
    }

    public ProductCheckOut(int accountID, int storeID, int productID, int totalMRP, int bagDiscount, float estimatedTax, int totalPrice, int offerPrice, int contractID, String deliveryStatus, String createdDate) {
        this.accountID = accountID;
        this.storeID = storeID;
        this.productID = productID;
        this.totalMRP = totalMRP;
        this.bagDiscount = bagDiscount;
        this.estimatedTax = estimatedTax;
        this.totalPrice = totalPrice;
        this.offerPrice = offerPrice;
        this.contractID = contractID;
        this.deliveryStatus = deliveryStatus;
        this.createdDate = createdDate;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getTotalMRP() {
        return totalMRP;
    }

    public void setTotalMRP(int totalMRP) {
        this.totalMRP = totalMRP;
    }

    public int getBagDiscount() {
        return bagDiscount;
    }

    public void setBagDiscount(int bagDiscount) {
        this.bagDiscount = bagDiscount;
    }

    public float getEstimatedTax() {
        return estimatedTax;
    }

    public void setEstimatedTax(float estimatedTax) {
        this.estimatedTax = estimatedTax;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(int offerPrice) {
        this.offerPrice = offerPrice;
    }

    public int getContractID() {
        return contractID;
    }

    public void setContractID(int contractID) {
        this.contractID = contractID;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    
    
}
