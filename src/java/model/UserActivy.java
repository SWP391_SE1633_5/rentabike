/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
public class UserActivy {
    private int accountID;
    private String titleNotify;
    private String description;
    private String timeActivy;
    private String timeDistance;

    public UserActivy() {
    }

    public UserActivy(int accountID, String titleNotify, String description, String timeActivy, String timeDistance) {
        this.accountID = accountID;
        this.titleNotify = titleNotify;
        this.description = description;
        this.timeActivy = timeActivy;
        this.timeDistance = timeDistance;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getTitleNotify() {
        return titleNotify;
    }

    public void setTitleNotify(String titleNotify) {
        this.titleNotify = titleNotify;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimeActivy() {
        return timeActivy;
    }

    public void setTimeActivy(String timeActivy) {
        this.timeActivy = timeActivy;
    }

    public String getTimeDistance() {
        return timeDistance;
    }

    public void setTimeDistance(String timeDistance) {
        this.timeDistance = timeDistance;
    }
    
   
    
}
