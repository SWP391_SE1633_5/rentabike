/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Base64;

/**
 *
 * @author ADMIN
 */
public class Notify {

    private int accountID;
    private String titleNotify;
    private String imageNotìycation;
    private String description;
    private String timeEvent;
    private int marked;

    public Notify() {
    }

    public Notify(int accountID, String titleNotify, String imageNotìycation, String description, String timeEvent, int marked) {
        this.accountID = accountID;
        this.titleNotify = titleNotify;
        this.imageNotìycation = imageNotìycation;
        this.description = description;
        this.timeEvent = timeEvent;
        this.marked = marked;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getTitleNotify() {
        return titleNotify;
    }

    public void setTitleNotify(String titleNotify) {
        this.titleNotify = titleNotify;
    }

    public String getImageNotìycation() {
        return imageNotìycation;
    }

    public void setImageNotìycation(String imageNotìycation) {
        this.imageNotìycation = imageNotìycation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimeEvent() {
        return timeEvent;
    }

    public void setTimeEvent(String timeEvent) {
        this.timeEvent = timeEvent;
    }

    public int getMarked() {
        return marked;
    }

    public void setMarked(int marked) {
        this.marked = marked;
    }
        
}
