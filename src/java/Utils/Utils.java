/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this templates
 */
package Utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author win
 */
public class Utils {

    public static String generateCode() {
        ArrayList<String> stringRand = new ArrayList<>();
        Random rand = new Random();
        String result = "";
        for (int i = 48; i <= 57; i++) {
            stringRand.add(Character.toString((char) i));
        }
        for (int i = 65; i <= 90; i++) {
            stringRand.add(Character.toString((char) i));
        }
        for (int i = 97; i <= 122; i++) {
            stringRand.add(Character.toString((char) i));
        }
        for (int i = 0; i <= 200; i++) {
            int n = rand.nextInt(stringRand.size());
            result += stringRand.get(n);
        }
        return result;
    }
    public static String getCurrentDateTime() {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
        Date dt = new Date(System.currentTimeMillis());
        return sf.format(dt);
    }
    public static int getDistanceTime(String start, String end) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
        int dis=0;
        try {
            Date d1 = sf.parse(start);
            Date d2 = sf.parse(end);
            dis = ((int)d2.getTime() - (int)d1.getTime()) / (1000 * 60 * 60 * 24);
        } catch (ParseException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dis;
    }
    //d with format yyyy-MM-dd HH:mm:ss.SS
    public static Date convertStringToDate(String d){
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
        Date date = new Date();
        try {
            date = sf.parse(d);
        } catch (ParseException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }
    public static int convertDateToInt(String date) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
        int dis=0;
        try {
            Date d = sf.parse(date);
            dis = (int)d.getTime();
        } catch (ParseException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dis;
    }
}
