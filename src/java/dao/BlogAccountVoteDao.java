/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.BlogAccountVote;

/**
 *
 * @author win
 */
public class BlogAccountVoteDao {
    public static int getVoteByBlogId(int id){
        int count=0;
        String sql = "select * from BLOG_ACCOUNT_VOTE where BLOG_ID = ?";
        List<BlogAccountVote> list = new ArrayList<>();
        BlogAccountVote a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogAccountVote(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));
                list.add(a);
            }
            
            for(BlogAccountVote b: list){
                if(b.getType()==0) count--;
                if(b.getType()==1) count++;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return count;
    }
    public static boolean checkVotedByAccountIdAndBlogId(int accountId, int blogId){
        String sql = "select * from BLOG_ACCOUNT_VOTE where ACCOUNT_ID = ? and BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            st.setInt(2, blogId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
    public static BlogAccountVote getBlogAccountVoteByBIdAndAId(int blogId, int accountId){
        String sql = "select * from BLOG_ACCOUNT_VOTE where ACCOUNT_ID = ? and BLOG_ID=?";
        BlogAccountVote a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            st.setInt(2, blogId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = new BlogAccountVote(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static void deleteBlogAccountVoteByBIdAndAId(int blogId, int accountId){
        String sql = "DELETE FROM BLOG_ACCOUNT_VOTE where ACCOUNT_ID = ? and BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            st.setInt(2, blogId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static void deleteBlogAccountVoteByBId(int blogId){
        String sql = "DELETE FROM BLOG_ACCOUNT_VOTE where BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, blogId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static void insertBlogAccountVoteWithBIdAndAIdAndType(int blogId, int accountId, int type){
        String sql = "insert into BLOG_ACCOUNT_VOTE values(?, ?, ?)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, blogId);
            st.setInt(2, accountId);
            st.setInt(3, type);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static void updateBlogAccountVoteByBIdAndAIdAndType(int blogId, int accountId, int type){
        String sql = "UPDATE BLOG_ACCOUNT_VOTE SET type = ? WHERE BLOG_ID = ? and ACCOUNT_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, type);
            st.setInt(2, blogId);
            st.setInt(3, accountId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
