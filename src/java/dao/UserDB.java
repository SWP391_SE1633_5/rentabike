/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.*;
import java.util.LinkedList;
import model.Staff;
import model.User;

/**
 *
 * @author ADMIN
 */
public class UserDB {

    public static int saveAccount(String firstName, String lastName, String phoneNumber, String email, int accountID, byte[] avatar, String city, String street, String state) {
        String sqlQuery = "INSERT INTO CUSTOMERS(FIRST_NAME, LAST_NAME, PHONE_NUMBER, EMAIL, ACCOUNT_ID, AVATAR, CITY, STREET, STATE) VALUES(?, ?, ?, ?, ?, ?, ?, ? ,?)";
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setNString(1, firstName);
            statement.setNString(2, lastName);
            statement.setString(3, phoneNumber);
            statement.setString(4, email);
            statement.setInt(5, accountID);
            statement.setBytes(6, avatar);
            statement.setNString(7, city);
            statement.setNString(8, street);
            statement.setNString(9, state);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static int setPermissionDetails(int accountID, String permission) {
        int status = 0;
        String sqlQuery = "UPDATE CUSTOMERS SET [PERMISSION_DETAILS] = ? WHERE ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, permission);
            statement.setInt(2, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int setPermission(int accountID, String permission) {
        int status = 0;
        String sqlQuery = "UPDATE CUSTOMERS SET [PERMISSION] = ? WHERE ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, permission);
            statement.setInt(2, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int changedPassword(String email, String password) {
        String sqlQuery = "UPDATE ACCOUNT SET PASSWORD = ? WHERE EMAIL = ?";
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, password);
            statement.setString(2, email);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static int createToken(String tokenizer, int accountID, String email) {
        String sqlQuery = "INSERT INTO [TOKEN_ACCOUNT](TOKEN, ACCOUNT_ID, EMAIL) VALUES(?,?,?)";
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, tokenizer);
            statement.setInt(2, accountID);
            statement.setString(3, email);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static String checkTokenExpired(String tokenizer, String sqlQuery) {
        String tokenDate = "";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, tokenizer);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                tokenDate = result.getString(1);
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return tokenDate;
    }

    public static String searchToken(String tokenizer) {
        String sqlQuery = "SELECT EMAIL FROM [TOKEN_ACCOUNT] WHERE [TOKEN] = ?";
        String tokenEmail = "";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, tokenizer);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                tokenEmail = result.getString(1);
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return tokenEmail;
    }

    public static int deleteToken(String tokenizer) {
        String sqlQuery = "DELETE FROM [TOKEN_ACCOUNT] WHERE [TOKEN] = ?";
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, tokenizer);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static int verifyAccount(String email) {
        String sqlQuery = "UPDATE CUSTOMERS SET VERIFY = ? WHERE EMAIL = ?";
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, "VERIFY");
            statement.setString(2, email);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static byte[] getImageBytes(int accountID) {
        Staff staff = new Staff();
        String sqlQuery = "SELECT DISTINCT C.AVATAR FROM CUSTOMERS AS C, ACCOUNT AS A WHERE C.[ACCOUNT_ID] = ? AND C.EMAIL = A.EMAIL";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                if (result.getString(1) != null) {
                    return result.getBytes(1);
                }
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return null;
    }

    public static User searchAccountByOption(int accountID, String option) {
        User user = new User();
        String sqlQuery = "SELECT DISTINCT C.*, A.ROLE FROM CUSTOMERS AS C, ACCOUNT AS A WHERE C.[ACCOUNT_ID] = ? AND C.EMAIL = A.EMAIL";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setNString(2, option);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                user.setCustomerID(result.getInt(1));
                user.setAccountID(result.getInt(2));
                user.setEmail(result.getString(3));
                user.setFirstName(result.getNString(4));
                user.setLastName(result.getNString(5));
                user.setPhoneNumber(result.getString(6));
                if (result.getString(7) != null) {
                    user.setAvatar(User.convertByteArrayToBase64(result.getBytes(7)));
                } else {
                    user.setAvatar(result.getString(7));
                }
                user.setCity(result.getNString(8));
                user.setStreet(result.getNString(9));
                user.setState(result.getNString(10));
                user.setDateOfJoin(result.getDate(11));
                user.setVerify(result.getString(12));
                user.setStatus(result.getInt(13));
                user.setPermission(result.getString(14));
                user.setPermissionDetails(result.getString(15));
                user.setZipCode(result.getString(16));
                user.setRole(result.getString(17));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return user;
    }

    public static User searchAccountByOption(int accountID, String optionValue, String sqlQuery, String acceptCommand) {
        User user = new User();
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            if (acceptCommand.equalsIgnoreCase("Search")) {
                statement.setNString(2, "%" + optionValue + "%");
            } else {
                statement.setString(2, optionValue);
            }
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                user.setCustomerID(result.getInt(1));
                user.setAccountID(result.getInt(2));
                user.setEmail(result.getString(3));
                user.setFirstName(result.getNString(4));
                user.setLastName(result.getNString(5));
                user.setPhoneNumber(result.getString(6));
                if (result.getString(7) != null) {
                    user.setAvatar(User.convertByteArrayToBase64(result.getBytes(7)));
                } else {
                    user.setAvatar(result.getString(7));
                }
                user.setCity(result.getNString(8));
                user.setStreet(result.getNString(9));
                user.setState(result.getNString(10));
                user.setDateOfJoin(result.getDate(11));
                user.setVerify(result.getString(12));
                user.setStatus(result.getInt(13));
                user.setPermission(result.getString(14));
                user.setPermissionDetails(result.getString(15));
                user.setZipCode(result.getString(16));
                user.setRole(result.getString(17));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return user;
    }

    public static int deleteCustomer(int customerID) {
        String sqlQuery = "DELETE FROM CUSTOMERS WHERE CUSTOMER_ID = ?";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, customerID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static User searchAccountID(int accountID) {
        User user = new User();
        String sqlQuery = "SELECT DISTINCT C.*, A.ROLE FROM CUSTOMERS AS C, ACCOUNT AS A WHERE C.[ACCOUNT_ID] = ? AND C.EMAIL = A.EMAIL";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                user.setCustomerID(result.getInt(1));
                user.setAccountID(result.getInt(2));
                user.setEmail(result.getString(3));
                user.setFirstName(result.getNString(4));
                user.setLastName(result.getNString(5));
                user.setPhoneNumber(result.getString(6));
                if (result.getString(7) != null) {
                    user.setAvatar(User.convertByteArrayToBase64(result.getBytes(7)));
                } else {
                    user.setAvatar(result.getString(7));
                }
                user.setCity(result.getNString(8));
                user.setStreet(result.getNString(9));
                user.setState(result.getNString(10));
                user.setDateOfJoin(result.getDate(11));
                user.setVerify(result.getString(12));
                user.setStatus(result.getInt(13));
                user.setPermission(result.getString(14));
                user.setPermissionDetails(result.getString(15));
                user.setZipCode(result.getString(16));
                user.setRole(result.getString(17));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return user;
    }

    public static int updateUser(User user, byte[] avatarImage) {
        int statusUpdate = 0;
        String sqlQuery = "";
        if (avatarImage != null) {
            sqlQuery = "UPDATE CUSTOMERS SET FIRST_NAME = ?, LAST_NAME = ?, PHONE_NUMBER = ?, AVATAR = ?,  CITY = ?, STREET = ?, "
                    + "[STATE] = ? WHERE ACCOUNT_ID = ?";
        } else {
            sqlQuery = "UPDATE CUSTOMERS SET FIRST_NAME = ?, LAST_NAME = ?, PHONE_NUMBER = ?, CITY = ?, STREET = ?, "
                    + "[STATE] = ? WHERE ACCOUNT_ID = ?";
        }
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getPhoneNumber());
            if (avatarImage != null) {
                statement.setBytes(4, avatarImage);
                statement.setNString(5, user.getCity());
                statement.setNString(6, user.getStreet());
                statement.setNString(7, user.getState());
                statement.setInt(8, user.getAccountID());
            } else {
                statement.setNString(4, user.getCity());
                statement.setNString(5, user.getStreet());
                statement.setNString(6, user.getState());
                statement.setInt(7, user.getAccountID());
            }
            statusUpdate = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return statusUpdate;
    }

    public static LinkedList displayCustomer() {
        LinkedList<User> listCustomer = new LinkedList();
        String sqlQuery = "SELECT DISTINCT C.*, A.ROLE FROM CUSTOMERS AS C, ACCOUNT AS A WHERE C.EMAIL = A.EMAIL";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                User user = new User();
                user.setCustomerID(result.getInt(1));
                user.setAccountID(result.getInt(2));
                user.setEmail(result.getString(3));
                user.setFirstName(result.getNString(4));
                user.setLastName(result.getNString(5));
                user.setPhoneNumber(result.getString(6));
                if (result.getString(7) != null) {
                    user.setAvatar(User.convertByteArrayToBase64(result.getBytes(7)));
                } else {
                    user.setAvatar(result.getString(7));
                }
                user.setCity(result.getNString(8));
                user.setStreet(result.getNString(9));
                user.setState(result.getNString(10));
                user.setDateOfJoin(result.getDate(11));
                user.setVerify(result.getString(12));
                user.setStatus(result.getInt(13));
                user.setPermission(result.getString(14));
                user.setPermissionDetails(result.getString(15));
                user.setZipCode(result.getString(16));
                user.setRole(result.getString(17));
                listCustomer.add(user);
            }
            if (listCustomer.size() == 0) {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return listCustomer;
    }

    public static int updatePermission(String permission, String oldPermission) {
        int status = 0;
        String sqlQuery = "UPDATE STAFFS SET [PERMISSION_DETAILS] = ? WHERE [PERMISSION_DETAILS] = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, permission);
            statement.setString(2, oldPermission);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int insertStore(int customerID, String name, String phoneNumber, String email, String city, String street, String state, String role) {
        String sqlQuery = "INSERT INTO STORES(STORE_ID, STORE_NAME, PHONE_NUMBER, EMAIL, CITY, STREET, [STATE], [ROLE]) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, customerID);
            statement.setNString(2, name);
            statement.setString(3, phoneNumber);
            statement.setString(4, email);
            statement.setNString(5, city);
            statement.setNString(6, street);
            statement.setNString(7, state);
            statement.setString(8, role);
            status = statement.executeUpdate();
        } catch (Exception errorSql) {
        }
        return status;
    }

    public static int deleteStore(int accountID) {
        String sqlQuery = "DELETE FROM STORES WHERE STORE_ID = ?";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            status = statement.executeUpdate();
        } catch (Exception errorSql) {
        }
        return status;
    }

    public static int searchStoreID(int customerID) {
        String sqlQuery = "SELECT CUSTOMER_ID, ACCOUNT_ID FROM CUSTOMERS WHERE CUSTOMER_ID = ?";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, customerID);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                status = result.getInt(2);
            }
        } catch (Exception errorSql) {
        }
        return status;
    }
}
