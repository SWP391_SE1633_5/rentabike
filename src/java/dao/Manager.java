/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import model.Account;
import model.UserActivy;
import model.UserService;

/**
 *
 * @author ADMIN
 */
public class Manager {

    public static int countAccount() {
        String sqlQuery = "INSERT INTO ACCOUNT(EMAIL, PASSWORD, ROLE) VALUES(?, ?, ?)";
        int count = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                count = result.getInt(1);
            }
        } catch (Exception sqlError) {
        }
        return count;
    }

    public static int updateAccount(String email, String password, String role) {
        String sqlQuery = "INSERT INTO ACCOUNT(EMAIL, PASSWORD, ROLE) VALUES(?, ?, ?)";
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, email);
            statement.setString(2, password);
            statement.setString(3, role);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static Account searchAccount(String email) {
        Account account = new Account();
        String sqlQuery = "SELECT * FROM ACCOUNT WHERE [EMAIL] = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                account.setAccountID(result.getInt(1));
                account.setEmail(result.getString(2));
                account.setPassword(result.getString(3));
                account.setRole(result.getString(4));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return account;
    }

    public static Account searchAccountID(int accountID) {
        Account account = new Account();
        String sqlQuery = "SELECT * FROM ACCOUNT WHERE [ACCOUNT_ID] = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                account.setAccountID(result.getInt(1));
                account.setEmail(result.getString(2));
                account.setPassword(result.getString(3));
                account.setRole(result.getString(4));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return account;
    }

    public static int createKey(String token, int accountID) {
        String sqlQuery = "INSERT INTO TOKEN_AUTH_KEY([ACCOUNT_ID], [KEY]) VALUES(?,?)";
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setString(2, token);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static UserService searchKey(String token) {
        UserService userService = new UserService();
        String sqlQuery = "SELECT * FROM TOKEN_AUTH_KEY WHERE [KEY] = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, token);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                userService.setAuthID(result.getInt(1));
                userService.setUserID(result.getInt(2));
                userService.setKey(result.getString(3));
                userService.setExpired(result.getString(4));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return userService;
    }

    public static int deleteAuth(String tokenizer) {
        String sqlQuery = "DELETE FROM TOKEN_AUTH_KEY WHERE [KEY] = ?";
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, tokenizer);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static int updateRole(String role, int accountID, String sqlQuery) {
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, role);
            statement.setInt(2, accountID);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static int updateStatus(int status, int accountID, String sqlQuery) {
        int updateStatus = 0;

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, status);
            statement.setInt(2, accountID);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static int createNewBannedUser(String message, int accountID, String duration) {
        int updateStatus = 0;
        String sqlQuery = "INSERT INTO ACCOUNT_BANNED([ACCOUNT_ID], [MESSAGE], [DURATION]) VALUES(?, ?, ?)";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setString(2, message);
            statement.setString(3, duration);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static int deleteNewBannedUser(int accountID) {
        int updateStatus = 0;
        String sqlQuery = "DELETE FROM ACCOUNT_BANNED WHERE ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            updateStatus = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return updateStatus;
    }

    public static String searchBannedUser(int accountID) {
        String sqlQuery = "SELECT [EXPIRED_DATE], [DURATION] FROM ACCOUNT_BANNED WHERE ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return result.getString(1) + "LGN" + result.getString(2);
            }
        } catch (Exception sqlError) {
        }
        return "Not Found";
    }

    public static int activyAccountUpdate(int accountID, String title, String description) {
        int status = 0;
        String sqlQuery = "INSERT INTO [ACTIVY_ACCOUNT]([ACCOUNT_ID],[TITLE_ACTIVY],[TITLE_DESCRIPTION]) VALUES(?, ?, ?)";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setString(2, title);
            statement.setString(3, description);
            status = statement.executeUpdate();
        } catch (Exception e) {

        }
        return status;
    }

    public static LinkedList<UserActivy> getActivyAccount(int accountID) {
        LinkedList<UserActivy> listActivy = new LinkedList<>();
        String sqlQuery = "SELECT * FROM [ACTIVY_ACCOUNT] WHERE ACCOUNT_ID = ? ORDER BY [TIME_ACTIVY] DESC";

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                UserActivy userActivy = new UserActivy();
                userActivy.setAccountID(result.getInt(1));
                userActivy.setTitleNotify(result.getString(2));
                userActivy.setDescription(result.getString(3));
                userActivy.setTimeActivy(result.getString(4));
                listActivy.add(userActivy);
            }
            if (listActivy.isEmpty()) {
                return null;
            }
        } catch (Exception e) {

        }
        return listActivy;
    }

    public static int countUserData(String sqlQuery, int status) {
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            if (status != -1) {
                statement.setInt(1, status);
            }
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return result.getInt(1);
            }
        } catch (Exception errorSql) {
        }
        return 0;
    }

    public static int countRoleData(String sqlQuery, String data) {
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, data);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return result.getInt(1);
            }
        } catch (Exception errorSql) {
        }
        return 0;
    }

    public static LinkedList<Account> accountListRole(String sqlQuery) {
        LinkedList<Account> listAccountRole = new LinkedList<>();
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Account account = new Account();
                account.setAccountID(result.getInt(1));
                account.setRole(result.getString(2));
                listAccountRole.add(account);
            }
            if (listAccountRole.isEmpty()) {
                return null;
            }
        } catch (Exception errorSql) {
        }
        return listAccountRole;
    }

    public static int searchAccountStatus(int accountID, boolean switchContent) {
        String sqlQuery;
        if (switchContent == true) {
            sqlQuery = "SELECT ACTIVE FROM STAFFS WHERE ACCOUNT_ID = ?";
        } else {
            sqlQuery = "SELECT STATUS FROM CUSTOMERS WHERE ACCOUNT_ID = ?";
        }
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return result.getInt(1);
            }
        } catch (Exception sqlError) {
        }
        return -1;
    }

}
