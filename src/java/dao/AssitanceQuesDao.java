/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Utils.Utils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import model.AssistanceQues;

/**
 *
 * @author win
 */
public class AssitanceQuesDao {
    public static List<AssistanceQues> getAllAssistanceQues(){
        List<AssistanceQues> list = new ArrayList<>();
        String sql = "select * from ASSISTANCE_QUESTION";
        AssistanceQues a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new AssistanceQues(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(list, (o1, o2) -> {
            Date dO1 = Utils.convertStringToDate(o1.getCreatedTime());
            Date dO2 = Utils.convertStringToDate(o2.getCreatedTime());
            return dO2.compareTo(dO1);
        });
        return list;
    }
    public static void addNewAssistanceQues(String content, int accId, String createdTime){
        String sql = "insert into ASSISTANCE_QUESTION values(?, ?,  ?)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, content);
            st.setInt(2, accId);
            st.setString(3, createdTime);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static AssistanceQues findAssistanceQuesByAssQID(int id){
        String sql = "select top 1 * from ASSISTANCE_QUESTION where ACCOUNT_ID=? order by TIME_CREATED desc";
        AssistanceQues a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new AssistanceQues(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static void deleteAssistanceQuesByListAssQId(String[] ids){
        for(String id: ids){
            String sql = "delete from ASSISTANCE_QUESTION where ASSISTANCE_QUESTION_ID=?";
            try {
                PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
                st.setString(1, id);
                st.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
    public static List<AssistanceQues> searchAllAssistanceQuesBySearch(String search){
        List<AssistanceQues> list = new ArrayList<>();
        String sql = "select * from ASSISTANCE_QUESTION where CONTENT like ?";
        AssistanceQues a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, "%"+search+"%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new AssistanceQues(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(list, (o1, o2) -> {
            Date dO1 = Utils.convertStringToDate(o1.getCreatedTime());
            Date dO2 = Utils.convertStringToDate(o2.getCreatedTime());
            return dO2.compareTo(dO1);
        });
        return list;
    }
}
