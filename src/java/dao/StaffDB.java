/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import model.*;

/**
 *
 * @author ADMIN
 */
public class StaffDB {

    public static int createPinCode(int accountID, String pinCode) {
        String sqlQuery = "INSERT INTO STAFF_PIN_CODE_TOKEN(ACCOUNT_ID,PIN_CODE) VALUES(?, ?)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setString(2, pinCode);
            status = statement.executeUpdate();
        } catch (Exception e) {

        }
        return status;
    }

    public static int updatePermission(String permission, String oldPermission) {
        int status = 0;
        String sqlQuery = "UPDATE STAFFS SET [PERMISSION_DETAILS] = ? WHERE [PERMISSION_DETAILS] = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, permission);
            statement.setString(2, oldPermission);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static String searchPinCode(String pinCode) {
        String sqlQuery = "SELECT [ACCOUNT_ID],[EXPIRED] FROM STAFF_PIN_CODE_TOKEN WHERE PIN_CODE = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, pinCode);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return result.getString(1) + "DDA" + result.getString(2);
            }
        } catch (Exception e) {

        }
        return null;
    }

    public static int deletePinCode(int accountID, String pinCode) {
        String sqlQuery = "DELETE FROM STAFF_PIN_CODE_TOKEN WHERE ACCOUNT_ID = ? AND PIN_CODE = ?";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setString(2, pinCode);
            status = statement.executeUpdate();
        } catch (Exception e) {

        }
        return status;
    }

    public static int clearPinCode(int accountID) {
        String sqlQuery = "DELETE FROM STAFF_PIN_CODE_TOKEN WHERE ACCOUNT_ID = ?";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {

        }
        return status;
    }

    public static int setActiveStaff(int accountID, int active) {
        int status = 0;
        String sqlQuery = "UPDATE STAFFS SET ACTIVE = ? WHERE ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setInt(2, active);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int setPermissionDetails(int accountID, String permission) {
        int status = 0;
        String sqlQuery = "UPDATE STAFFS SET [PERMISSION_DETAILS] = ? WHERE ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, permission);
            statement.setInt(2, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int setPermission(int accountID, String permission) {
        int status = 0;
        String sqlQuery = "UPDATE STAFFS SET [PERMISSION] = ? WHERE ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, permission);
            statement.setInt(2, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int saveStaff(Staff staff) {
        String sqlQuery = "INSERT INTO STAFFS(ACCOUNT_ID, EMAIL, USER_NAME, FIRST_NAME, LAST_NAME, PHONE_NUMBER, CITY, STREET, STATE) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, staff.getAccountID());
            statement.setString(2, staff.getEmail());
            statement.setNString(3, staff.getUserName());
            statement.setNString(4, staff.getFirstName());
            statement.setNString(5, staff.getLastName());
            statement.setString(6, staff.getPhoneNumber());
            statement.setNString(7, staff.getCity());
            statement.setNString(8, staff.getStreet());
            statement.setNString(9, staff.getState());
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int uploadStaffImage(int accountID, byte[] image) {
        String sqlQuery = "UPDATE STAFFS SET [AVATAR] = ? WHERE ACCOUNT_ID = ?";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setBytes(1, image);
            statement.setInt(2, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int deleteStaff(int accountID) {
        String sqlQuery = "DELETE FROM STAFFS WHERE ACCOUNT_ID = ?";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int updatePinCode(String pinCode, int accountID) {
        int status = 0;
        String sqlQuery = "UPDATE STAFFS SET PIN_CODE = ? WHERE ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, pinCode);
            statement.setInt(2, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static Staff searchAccountByOption(int accountID, String optionValue, String sqlQuery, String acceptCommand) {
        Staff staff = new Staff();
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            if (acceptCommand.equalsIgnoreCase("Search")) {
                statement.setNString(2, "%" + optionValue + "%");
            } else {
                statement.setString(2, optionValue);
            }
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                staff.setAccountID(result.getInt(1));
                staff.setStaffID(result.getInt(2));
                staff.setEmail(result.getString(3));
                staff.setPinCode(result.getString(4));
                staff.setUserName(result.getString(5));
                staff.setFirstName(result.getNString(6));
                staff.setLastName(result.getNString(7));
                staff.setPhoneNumber(result.getString(8));
                if (result.getString(9) != null) {
                    staff.setAvatar(Staff.convertByteArrayToBase64(result.getBytes(9)));
                } else {
                    staff.setAvatar(result.getString(9));
                }
                staff.setDateOfJoin(result.getDate(10));
                staff.setActive(result.getInt(11));
                staff.setCity(result.getNString(12));
                staff.setStreet(result.getNString(13));
                staff.setState(result.getNString(14));
                staff.setPermission(result.getString(15));
                staff.setPermissionDetails(result.getString(16));
                staff.setRole(result.getString(17));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return staff;
    }

    public static Staff searchAccountID(int accountID) {
        Staff staff = new Staff();
        String sqlQuery = "SELECT DISTINCT S.*, A.ROLE FROM STAFFS AS S, ACCOUNT AS A WHERE S.EMAIL = A.EMAIL AND S.[ACCOUNT_ID] = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                staff.setAccountID(result.getInt(1));
                staff.setStaffID(result.getInt(2));
                staff.setEmail(result.getString(3));
                staff.setPinCode(result.getString(4));
                staff.setUserName(result.getString(5));
                staff.setFirstName(result.getNString(6));
                staff.setLastName(result.getNString(7));
                staff.setPhoneNumber(result.getString(8));
                if (result.getString(9) != null) {
                    staff.setAvatar(Staff.convertByteArrayToBase64(result.getBytes(9)));
                } else {
                    staff.setAvatar(result.getString(9));
                }
                staff.setDateOfJoin(result.getDate(10));
                staff.setActive(result.getInt(11));
                staff.setCity(result.getNString(12));
                staff.setStreet(result.getNString(13));
                staff.setState(result.getNString(14));
                staff.setPermission(result.getString(15));
                staff.setPermissionDetails(result.getString(16));
                staff.setRole(result.getString(17));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return staff;
    }

    public static byte[] getImageBytes(int accountID) {
        Staff staff = new Staff();
        String sqlQuery = "SELECT DISTINCT S.AVATAR FROM STAFFS AS S, ACCOUNT AS A WHERE S.EMAIL = A.EMAIL AND S.[ACCOUNT_ID] = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                if (result.getString(1) != null) {
                    return result.getBytes(1);
                }
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return null;
    }

    public static LinkedList displayStaffs() {
        LinkedList<Staff> listStaff = new LinkedList();
        String sqlQuery = "SELECT DISTINCT S.*, A.ROLE FROM STAFFS AS S, ACCOUNT AS A WHERE S.EMAIL = A.EMAIL";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Staff staff = new Staff();
                staff.setAccountID(result.getInt(1));
                staff.setStaffID(result.getInt(2));
                staff.setEmail(result.getString(3));
                staff.setPinCode(result.getString(4));
                staff.setUserName(result.getNString(5));
                staff.setFirstName(result.getNString(6));
                staff.setLastName(result.getNString(7));
                staff.setPhoneNumber(result.getString(8));
                if (result.getString(9) != null) {
                    staff.setAvatar(User.convertByteArrayToBase64(result.getBytes(9)));
                } else {
                    staff.setAvatar(result.getString(9));
                }
                staff.setDateOfJoin(result.getDate(10));
                staff.setActive(result.getInt(11));
                staff.setCity(result.getNString(12));
                staff.setStreet(result.getNString(13));
                staff.setState(result.getNString(14));
                staff.setPermission(result.getString(15));
                staff.setPermissionDetails(result.getString(16));
                staff.setRole(result.getString(17));
                listStaff.add(staff);
            }
            if (listStaff.isEmpty()) {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return listStaff;
    }
}
