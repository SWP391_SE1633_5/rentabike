/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this templates
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Account;

/**
 *
 * @author win
 */
public class UserDao {
    
    public boolean checkEmailExist(String email) {
        String sql = "select * from account where email = ?";

        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
    public Account findAccountByEmail(String email){
        String sql = "select * from account where email = ?";
        Account a = new Account();
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a.setAccountID(rs.getInt(1));
                a.setEmail(rs.getString(2));
                a.setPassword(rs.getString(3));
                a.setRole(rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public void resetPassword(String token, String password){
        TokenResetPasswordDao tk = new TokenResetPasswordDao();
        int id = tk.findTSRPassworByToken(token).getAccountId();
        String sql = "update account set PASSWORD=? where ACCOUNT_ID=?";

        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, password);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void changePassword(String newPassword, String email){
        String sql = "update account set PASSWORD=? where email=?";

        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, newPassword);
            st.setString(2, email);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return;
    }
    
    public Account findAccountByAccountId(int id){
        String sql = "select * from account where ACCOUNT_ID = ?";
        Account a = new Account();
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a.setAccountID(rs.getInt(1));
                a.setEmail(rs.getString(2));
                a.setPassword(rs.getString(3));
                a.setRole(rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
}
