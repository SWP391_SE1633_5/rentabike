/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this templates
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.TokenSubmitResetPassword;

/**
 *
 * @author win
 */
public class TokenResetPasswordDao{
    public boolean checkTokenSubmitEmailExist(String token){
        String sql = "select * from TOKEN_SUBMIT_RESET_PASSWORD where token = ?";

        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, token);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }
    public TokenSubmitResetPassword findTSRPassworByToken(String token){
        String sql = "select * from TOKEN_SUBMIT_RESET_PASSWORD where token = ?";
        TokenSubmitResetPassword t = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, token);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                t = new TokenSubmitResetPassword(rs.getInt(1), token, rs.getString(3), rs.getInt(4));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return t;
    }
    public void saveTokenSubmitEmail(String token, String dateTime, int id){
        String sql = "insert into TOKEN_SUBMIT_RESET_PASSWORD values(?, ?, ?)";

        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, token);
            st.setString(2, dateTime);
            st.setInt(3, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return;
    }
    public void delteTSRPasswordByAccountId(int id){
        String sql = "delete from TOKEN_SUBMIT_RESET_PASSWORD where account_id=?";

        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return;
    }
    public void deleteTSRPasswordByToken(String token){
        String sql = "delete from TOKEN_SUBMIT_RESET_PASSWORD where token=?";

        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, token);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return;
    }
}
