/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import model.BlogComment;

/**
 *
 * @author win
 */
public class BlogCommentDao {

    public List<BlogComment> getNextBlogComment(int blogId, int after) {
        List<BlogComment> list = new ArrayList<>();
        String sql = "select top 3 *\n"
                + "from BLOG_COMMENT\n"
                + "where BLOG_ID=? and COMMENT_ID>? and REPLY_ID IS NULL";
        BlogComment a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, blogId);
            st.setInt(2, after );
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogComment(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6));
                if(rs.getString(7)==null || rs.getString(7).equals("")) a.setReplyId(0);
                else a.setReplyId(rs.getInt(7));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    public static List<BlogComment> getBlogListCommentNotReply(int blogId) {
        List<BlogComment> list = new ArrayList<>();
        String sql = "select * from BLOG_COMMENT where BLOG_ID=? and REPLY_ID is null";
        BlogComment a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, blogId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogComment(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6));
                if(rs.getString(7)==null || rs.getString(7).equals("")) a.setReplyId(0);
                else a.setReplyId(rs.getInt(7));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    public static List<BlogComment> getBlogCommentByReplyId(int replyId){
        List<BlogComment> list = new ArrayList<>();
        String sql = "select * from BLOG_COMMENT where REPLY_ID=?";
        BlogComment a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, replyId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogComment(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6));
                if(rs.getString(7)==null || rs.getString(7).equals("")) a.setReplyId(0);
                else a.setReplyId(rs.getInt(7));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    public static List<BlogComment> getBlogCommentByCmtId(int cmtId){
        List<BlogComment> list = new ArrayList<>();
        list.addAll(getBlogCommentByReplyId(cmtId));
        if(list.isEmpty()) return list;
        while(true){
            boolean check=true;
            for(int i=0; i<list.size(); i++){
                if(!getBlogCommentByReplyId(list.get(i).getId()).isEmpty()){
                    list.addAll(getBlogCommentByReplyId(list.get(i).getId()));
                    continue;
                }
                if(i==(list.size()-1)) check=false;
            }
            if(!check) break;
        }
        Collections.sort(list, (o1, o2) -> {
            return o2.getId()-o1.getId();
        });
        return list;
    }
    public static int getNumReplyByReplyId(int replyId){
        String sql = "select COUNT(*) from BLOG_COMMENT where REPLY_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, replyId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }
    public static void insertNewCmt(BlogComment b){
        String sql = "insert into BLOG_COMMENT values(?, ?, ?, ?, NULL, NULL)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, b.getBlogId());
            st.setString(2, b.getContent());
            st.setInt(3, b.getAccountId());
            st.setString(4, b.getTimeComment());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static BlogComment getBlogCommentByBlogInserted(BlogComment b){
        BlogComment newB = null;
        String sql = "select * from BLOG_COMMENT where BLOG_ID = ? and ACCOUNT_ID=? and TIME_COMMENT=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, b.getBlogId());
            st.setInt(2, b.getAccountId());
            st.setString(3, b.getTimeComment());
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                newB = new BlogComment(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return newB;
    }
    public static void updateCmt(String content, String updateTime, int cmtId){
        String sql = "update BLOG_COMMENT set COMMENT_CONTENT=?, TIME_COMMENT_UPDATE=? where COMMENT_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, content);
            st.setString(2, updateTime);
            st.setInt(3, cmtId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static BlogComment getBlogCommentByBlogCmtId(int cmtId){
        BlogComment newB = null;
        String sql = "select * from BLOG_COMMENT where COMMENT_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, cmtId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                newB = new BlogComment(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return newB;
    }
    public static void insertNewReplyCmt(BlogComment b){
        String sql = "insert into BLOG_COMMENT values(?, ?, ?, ?, NULL, ?)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, b.getBlogId());
            st.setString(2, b.getContent());
            st.setInt(3, b.getAccountId());
            st.setString(4, b.getTimeComment());
            st.setInt(5, b.getReplyId());
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static int getQuanCmtByBlogId(int id){
        String sql = "select COUNT(*) from BLOG_COMMENT where BLOG_ID=?";
        int count=0;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count=rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return count;
    }
    public static void deleteBlogCommentReplyByCmtId(int cmtId){
        BlogCommentReactDao.deleteCommmentReactByCmtId(cmtId);
        String sql = "delete from BLOG_COMMENT where COMMENT_ID = ?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, cmtId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static void deleteBlogCommentByCmtId(int cmtId){
        List<BlogComment> list = getBlogCommentByCmtId(cmtId);
        if(!list.isEmpty()){
            for(BlogComment bc: list){
                deleteBlogCommentReplyByCmtId(bc.getId());
            }
        }
        deleteBlogCommentReplyByCmtId(cmtId);
    }
    public static void deleteAllBlogCommentByBlogId(int blogId){
        List<BlogComment> bcList = getBlogListCommentNotReply(blogId);
        for(BlogComment bc: bcList){
            deleteBlogCommentByCmtId(bc.getId());
        }
    }
    public static List<BlogComment> getBlogCommentByAccountId(int accountId){
        List<BlogComment> list = new ArrayList<>();
        String sql = "select * from BLOG_COMMENT where ACCOUNT_ID=?";
        BlogComment a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogComment(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6));
                if(rs.getString(7)==null || rs.getString(7).equals("")) a.setReplyId(0);
                else a.setReplyId(rs.getInt(7));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
}
