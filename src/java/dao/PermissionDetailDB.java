/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import model.PermissionsDetail;

/**
 *
 * @author ADMIN
 */
public class PermissionDetailDB {

    public static LinkedList<PermissionsDetail> listPermission(String sqlQuery) {
        LinkedList<PermissionsDetail> permissionList = new LinkedList<>();
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statemnet = connection.prepareStatement(sqlQuery);
            ResultSet result = statemnet.executeQuery();

            while (result.next()) {
                PermissionsDetail permission = new PermissionsDetail();
                permission.setIdPremission(result.getInt(1));
                permission.setName(result.getNString(2));
                permission.setAssignTo(result.getString(3));
                permission.setCreateDate(result.getString(4));
                permissionList.add(permission);
            }
            if (permissionList.isEmpty()) {
                return null;
            }
        } catch (Exception errorSql) {
        }
        return permissionList;
    }

    public static PermissionsDetail searchPermission(int idPermission) {
        PermissionsDetail permission = new PermissionsDetail();
        String sqlQuery = "SELECT * FROM PERMISSION_DETAILS WHERE ID_PERMISSION = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, idPermission);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                permission.setIdPremission(result.getInt(1));
                permission.setName(result.getNString(2));
                permission.setAssignTo(result.getString(3));
                permission.setCreateDate(result.getString(4));
            } else {
                return null;
            }
        } catch (Exception errorSql) {
        }
        return permission;
    }

    public static int createPermission(String name, String assignTo) {
        String sqlQuery = "INSERT INTO PERMISSION_DETAILS([NAME], [ASSIGN_TO]) VALUES(?, ?)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statemnet = connection.prepareStatement(sqlQuery);
            statemnet.setNString(1, name);
            statemnet.setString(2, assignTo);
            status = statemnet.executeUpdate();
        } catch (Exception errorSql) {
        }
        return status;
    }

    public static int deletePermission(int idPermission) {
        String sqlQuery = "DELETE FROM PERMISSION_DETAILS WHERE [ID_PERMISSION] = ?";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statemnet = connection.prepareStatement(sqlQuery);
            statemnet.setInt(1, idPermission);
            status = statemnet.executeUpdate();
        } catch (Exception errorSql) {
        }
        return status;
    }

    public static int editPermission(int idPermission, String name, String assignTo) {
        String sqlQuery = "UPDATE PERMISSION_DETAILS SET [NAME] = ?, [ASSIGN_TO] = ? WHERE [ID_PERMISSION] = ?";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statemnet = connection.prepareStatement(sqlQuery);
            statemnet.setNString(1, name);
            statemnet.setString(2, assignTo);
            statemnet.setInt(3, idPermission);
            status = statemnet.executeUpdate();
        } catch (Exception errorSql) {
        }
        return status;
    }

    public static HashMap<Integer, String[]> listPermissionAssign() {
        HashMap<Integer, String[]> mapAssign = new HashMap<>();
        String sqlQuery = "SELECT PD.ID_PERMISSION, PD.ASSIGN_TO FROM PERMISSION_DETAILS AS PD";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statemnet = connection.prepareStatement(sqlQuery);
            ResultSet result = statemnet.executeQuery();

            while (result.next()) {
                mapAssign.put(result.getInt(1), result.getString(2).split(", "));
            }
            if (mapAssign.isEmpty()) {
                return null;
            }
        } catch (Exception errorSql) {
        }
        return mapAssign;
    }

}
