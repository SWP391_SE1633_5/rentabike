/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import model.Notify;
import model.Product;

/**
 *
 * @author ADMIN
 */
public class NotifyDB {

    public static int addNotify(Notify notify) {
        String sqlQuery = "INSERT INTO [NOTIFICATION](ACCOUNT_ID, TITLE_NOTIFY, IMAGE_NOTIFYCATION, DESCRIPTION) VALUES(?,?,?,?)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, notify.getAccountID());
            statement.setString(2, notify.getTitleNotify());
            statement.setString(3, notify.getImageNotìycation());
            statement.setString(4, notify.getDescription());
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static LinkedList<Notify> listNotifies(int accountID) {
        String sqlQuery = "SELECT * FROM [NOTIFICATION] WHERE ACCOUNT_ID = ? ORDER BY [TIME_EVENT] DESC";
        LinkedList<Notify> listNotify = new LinkedList<>();

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Notify notify = new Notify();
                notify.setAccountID(result.getInt(1));
                notify.setTitleNotify(result.getString(2));
                notify.setImageNotìycation(result.getString(3));
                notify.setDescription(result.getString(4));
                notify.setTimeEvent(result.getString(5));
                notify.setMarked(result.getInt(6));
                listNotify.add(notify);
            }
            if (listNotify.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return listNotify;
    }

    public static int countTotalNew(int accountID) {
        int totalNotify = 0;
        String sqlQuery = "SELECT COUNT(MARKED) FROM [NOTIFICATION] WHERE MARKED = 1 AND ACCOUNT_ID = ?";
        try {
            Connection connnection = new DBContext().getConnection();
            PreparedStatement statement = connnection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                totalNotify = result.getInt(1);
            }
        } catch (Exception e) {
        }
        return totalNotify;
    }

    public static int updateMarked(int accountID) {
        int status = 0;
        String sqlQuery = "UPDATE NOTIFICATION SET MARKED = 0 WHERE ACCOUNT_ID = ?";
        try {
            Connection connnection = new DBContext().getConnection();
            PreparedStatement statement = connnection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static Product searchProduct(String sqlQuery, int productID) {
        Product product = new Product();
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                product.setName(result.getNString(1));
                product.setImage(Product.convertByteArrayToBase64(result.getBytes(2)));
            } else {
                return null;
            }
        } catch (Exception e) {
        }
        return product;
    }
}
