/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import model.Product;
import model.ProductCheckOut;
import model.ProductReview;
import model.Staff;
import model.User;

/**
 *
 * @author ADMIN
 */
public class ProductDB {

    public static int saveProduct(Product product) {
        String sqlQuery = "INSERT INTO PRODUCTS(PRODUCT_NAME, BRAND_NAME, CATEGORY_NAME, MODEL_YEAR, LIST_PRICE, [DESCRIPTION], [MODE], STORE_ID, STAFF_ID] VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setNString(1, product.getName());
            statement.setNString(2, product.getBrand());
            statement.setString(3, product.getCategory());
            statement.setInt(4, product.getModelYear());
            statement.setFloat(5, product.getPrice());
            statement.setNString(6, product.getDescription());
            statement.setString(7, product.getMode());
            if (product.getStoreID() != 0) {
                statement.setInt(8, product.getStoreID());
            } else {
                statement.setNull(8, product.getStoreID());
            }
            if (product.getStaffID() != 0) {
                statement.setInt(9, product.getStaffID());
            } else {
                statement.setNull(9, Types.NULL);
            }
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int saveProductSpecifications(Product product) {
        String sqlQuery = "INSERT INTO PRODUCTS_SPECIFICATIONS VALUES(?, ?, ?, ?, ?, ? ,? ,?, ?)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, product.getProductID());
            statement.setNString(2, product.getName());
            statement.setInt(3, product.getDisplacement());
            statement.setString(4, product.getTransmission());
            statement.setString(5, product.getStarter());
            statement.setString(6, product.getFuelSystem());
            statement.setFloat(7, product.getFuelCapacity());
            statement.setFloat(8, product.getDryWeight());
            statement.setFloat(9, product.getSeatHeight());
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int saveProductImage(Product product, byte[] image) {
        String sqlQuery = "INSERT INTO PRODUCT_IMAGE VALUES(?, ?)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, product.getProductID());
            statement.setBytes(2, image);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static int convertDataProduct(int accountID, int idConvert, String sqlQuery) {
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setInt(2, idConvert);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static LinkedList<Product> listProduct(String sqlQuery) {
        LinkedList<Product> listProduct = new LinkedList<>();

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Product product = new Product();
                product.setProductID(result.getInt(1));
                product.setName(result.getNString(2));
                product.setBrand(result.getString(3));
                product.setCategory(result.getString(4));
                product.setModelYear(result.getInt(5));
                product.setPrice(result.getFloat(6));
                product.setDescription(result.getString(7));
                product.setStatus(result.getInt(8));
                product.setMode(result.getString(9));
                product.setStoreID(result.getInt(10));
                product.setStaffID(result.getInt(11));
                product.setCreateAt(result.getString(12));
                if (result.getString(13) != null) {
                    product.setImage(product.convertByteArrayToBase64(result.getBytes(13)));
                } else {
                    product.setImage(result.getString(13));
                }
                product.setDisplacement(result.getInt(14));
                product.setTransmission(result.getString(15));
                product.setStarter(result.getString(16));
                product.setFuelSystem(result.getString(17));
                product.setFuelCapacity(result.getFloat(18));
                product.setDryWeight(result.getFloat(19));
                product.setSeatHeight(result.getFloat(20));
                listProduct.add(product);
            }
            if (listProduct.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return listProduct;
    }

    public static LinkedList<Product> listProductFilter(String sqlQuery, String filterValue) {
        LinkedList<Product> listProduct = new LinkedList<>();

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, filterValue);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Product product = new Product();
                product.setProductID(result.getInt(1));
                product.setName(result.getNString(2));
                product.setBrand(result.getString(3));
                product.setCategory(result.getString(4));
                product.setModelYear(result.getInt(5));
                product.setPrice(result.getFloat(6));
                product.setDescription(result.getString(7));
                product.setStatus(result.getInt(8));
                product.setMode(result.getString(9));
                product.setStoreID(result.getInt(10));
                product.setStaffID(result.getInt(11));
                product.setCreateAt(result.getString(12));
                if (result.getString(13) != null) {
                    product.setImage(product.convertByteArrayToBase64(result.getBytes(13)));
                } else {
                    product.setImage(result.getString(13));
                }
                listProduct.add(product);
            }
            if (listProduct.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return listProduct;
    }

    public static LinkedList<Product> listProductPriceSlider(String sqlQuery, int minValue, int maxValue) {
        LinkedList<Product> listProduct = new LinkedList<>();

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, minValue);
            statement.setInt(2, maxValue);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Product product = new Product();
                product.setProductID(result.getInt(1));
                product.setName(result.getNString(2));
                product.setBrand(result.getString(3));
                product.setCategory(result.getString(4));
                product.setModelYear(result.getInt(5));
                product.setPrice(result.getFloat(6));
                product.setDescription(result.getString(7));
                product.setStatus(result.getInt(8));
                product.setMode(result.getString(9));
                product.setStoreID(result.getInt(10));
                product.setStaffID(result.getInt(11));
                product.setCreateAt(result.getString(12));
                if (result.getString(13) != null) {
                    product.setImage(product.convertByteArrayToBase64(result.getBytes(13)));
                } else {
                    product.setImage(result.getString(13));
                }
                listProduct.add(product);
            }
            if (listProduct.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return listProduct;
    }

    public static LinkedList<Product> listProductSearch(String sqlQuery, String inputSearch) {
        LinkedList<Product> listProduct = new LinkedList<>();

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, "%" + inputSearch + "%");
            statement.setString(2, "%" + inputSearch + "%");
            statement.setString(3, "%" + inputSearch + "%");
            statement.setString(4, "%" + inputSearch + "%");
            statement.setString(5, "%" + inputSearch + "%");
            statement.setString(6, "%" + inputSearch + "%");
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Product product = new Product();
                product.setProductID(result.getInt(1));
                product.setName(result.getNString(2));
                product.setBrand(result.getString(3));
                product.setCategory(result.getString(4));
                product.setModelYear(result.getInt(5));
                product.setPrice(result.getFloat(6));
                product.setDescription(result.getString(7));
                product.setStatus(result.getInt(8));
                product.setMode(result.getString(9));
                product.setStoreID(result.getInt(10));
                product.setStaffID(result.getInt(11));
                if (result.getString(13) != null) {
                    product.setImage(product.convertByteArrayToBase64(result.getBytes(13)));
                } else {
                    product.setImage(result.getString(13));
                }
                listProduct.add(product);
            }
            if (listProduct.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return listProduct;
    }

    public static Product searchProduct(String sqlQuery, int productID) {
        Product product = new Product();
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                product.setProductID(result.getInt(1));
                product.setName(result.getNString(2));
                product.setBrand(result.getString(3));
                product.setCategory(result.getString(4));
                product.setModelYear(result.getInt(5));
                product.setPrice(result.getFloat(6));
                product.setDescription(result.getString(7));
                product.setStatus(result.getInt(8));
                product.setMode(result.getString(9));
                product.setStoreID(result.getInt(10));
                product.setStaffID(result.getInt(11));
                product.setCreateAt(result.getString(12));
                if (result.getString(13) != null) {
                    product.setImage(product.convertByteArrayToBase64(result.getBytes(13)));
                } else {
                    product.setImage(result.getString(13));
                }
                product.setDisplacement(result.getInt(14));
                product.setTransmission(result.getString(15));
                product.setStarter(result.getString(16));
                product.setFuelSystem(result.getString(17));
                product.setFuelCapacity(result.getFloat(18));
                product.setDryWeight(result.getFloat(19));
                product.setSeatHeight(result.getFloat(20));
            } else {
                return null;
            }
        } catch (Exception e) {
        }
        return product;
    }

    public static int countProduct(String sqlQuery) {
        int count = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                count = result.getInt(1);
            }
        } catch (Exception e) {
        }
        return count;
    }

    public static int countInformations(String sqlQuery, int id) {
        int count = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                count = result.getInt(1);
            }
        } catch (Exception e) {
        }
        return count;
    }

    public static HashMap<String, Integer> countProductTypeIndividual(String sqlQuery) {
        HashMap<String, Integer> hashMapProductList = new HashMap<>();
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                hashMapProductList.put(result.getString(1), result.getInt(2));
            }
            if (hashMapProductList.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return hashMapProductList;
    }

    public static String searchPersonProduct(int storeID, String sqlQuery) {
        String namePerson = "";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, storeID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                namePerson = result.getString(1);
            } else {
                return null;
            }
        } catch (Exception e) {
        }
        return namePerson;
    }

    public static int updateWishList(int productID, int accountID, String sqlQuery) {
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productID);
            statement.setInt(2, accountID);
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }

    public static String searchWishList(int accountID, int productID) {
        String sqlQuery = "SELECT * FROM PRODUCTS__WISHLIST WHERE ACCOUNT_ID = ? AND PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setInt(2, productID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return result.getString(1);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static String searchCheckOut(int accountID, int productID) {
        String sqlQuery = "SELECT * FROM PRODUCT_CHECKOUT WHERE ACCOUNT_ID = ? AND PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setInt(2, productID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return result.getInt(1) + "";
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static LinkedList<Product> listProductWishList(String sqlQuery, String inputSearch, int accountID) {
        LinkedList<Product> listProduct = new LinkedList<>();

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            statement.setString(2, "%" + inputSearch + "%");
            statement.setString(3, "%" + inputSearch + "%");
            statement.setString(4, "%" + inputSearch + "%");
            statement.setString(5, "%" + inputSearch + "%");
            statement.setString(6, "%" + inputSearch + "%");
            statement.setString(7, "%" + inputSearch + "%");
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Product product = new Product();
                product.setProductID(result.getInt(1));
                product.setName(result.getNString(2));
                product.setBrand(result.getString(3));
                product.setCategory(result.getString(4));
                product.setModelYear(result.getInt(5));
                product.setPrice(result.getFloat(6));
                product.setDescription(result.getString(7));
                product.setStatus(result.getInt(8));
                product.setMode(result.getString(9));
                product.setStoreID(result.getInt(10));
                product.setStaffID(result.getInt(11));
                if (result.getString(13) != null) {
                    product.setImage(product.convertByteArrayToBase64(result.getBytes(13)));
                } else {
                    product.setImage(result.getString(13));
                }
                listProduct.add(product);
            }
            if (listProduct.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return listProduct;
    }

    public static LinkedList<Product> listProductWishList(String sqlQuery, int accountID) {
        LinkedList<Product> listProduct = new LinkedList<>();

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Product product = new Product();
                product.setProductID(result.getInt(1));
                product.setName(result.getNString(2));
                product.setBrand(result.getString(3));
                product.setCategory(result.getString(4));
                product.setModelYear(result.getInt(5));
                product.setPrice(result.getFloat(6));
                product.setDescription(result.getString(7));
                product.setStatus(result.getInt(8));
                product.setMode(result.getString(9));
                product.setStoreID(result.getInt(10));
                product.setStaffID(result.getInt(11));
                if (result.getString(13) != null) {
                    product.setImage(product.convertByteArrayToBase64(result.getBytes(13)));
                } else {
                    product.setImage(result.getString(13));
                }
                listProduct.add(product);
            }
            if (listProduct.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return listProduct;
    }

    public static LinkedList<Product> relatedProduct(String sqlQuery, String category, String brand, String mode) {
        LinkedList<Product> listProduct = new LinkedList<>();

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, category);
            statement.setString(2, brand);
            statement.setString(3, mode);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Product product = new Product();
                product.setProductID(result.getInt(1));
                product.setName(result.getNString(2));
                product.setBrand(result.getString(3));
                product.setCategory(result.getString(4));
                product.setModelYear(result.getInt(5));
                product.setPrice(result.getFloat(6));
                product.setDescription(result.getString(7));
                product.setStatus(result.getInt(8));
                product.setMode(result.getString(9));
                product.setStoreID(result.getInt(10));
                product.setStaffID(result.getInt(11));
                if (result.getString(13) != null) {
                    product.setImage(product.convertByteArrayToBase64(result.getBytes(13)));
                } else {
                    product.setImage(result.getString(13));
                }
                listProduct.add(product);
            }
            if (listProduct.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return listProduct;
    }

    public static int countWishList(int accountID, String sqlQuery) {
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return result.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public static Product getRentalDetail(String sqlQuery, Product product) {
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, product.getProductID());
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                product.setDeposit(result.getString(2));
                product.setRequiredDocs(result.getString(4));
                product.setMiliageLimit(result.getString(5));
                product.setMinimumRental(result.getString(6));
                product.setTouringMode(result.getInt(7));
            }
        } catch (Exception e) {

        }
        return product;
    }

    public static int addNewReviewsProduct(ProductReview productReview) {
        int status = 0;
        String sqlQuery = "INSERT INTO PRODUCTS_REVIEW(STORE_ID, STAFF_ID, PRODUCT_ID, COMMENT, ACCOUNT_ID) VALUES(?, ?, ?, ?, ?)";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productReview.getStoreID());
            statement.setInt(2, productReview.getStaffID());
            statement.setInt(3, productReview.getProductID());
            statement.setString(4, productReview.getCommentReview());
            statement.setInt(5, productReview.getAccountID());
            status = statement.executeUpdate();
        } catch (Exception errorSql) {
        }
        return status;
    }

    public static LinkedList<ProductReview> productReviewsList(int productID) {
        LinkedList<ProductReview> productListReviews = new LinkedList<>();
        String sqlQuery = "SELECT DISTINCT PR1.STORE_ID, PR2.STAFF_ID, PR1.PRODUCT_ID, PR1.COMMENT, PR1.COMMENT_DATE, PR1.ACCOUNT_ID, PR2.RATING\n"
                + "FROM PRODUCTS_REVIEW AS PR1, PRODUCTS_RATING AS PR2 \n"
                + "WHERE PR1.ACCOUNT_ID = PR2.ACCOUNT_ID AND (PR1.STORE_ID = PR2.STORE_ID OR PR1.STAFF_ID = PR2.STAFF_ID) AND PR1.PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productID);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                ProductReview productReview = new ProductReview();
                productReview.setStoreID(result.getInt(1));
                productReview.setStaffID(result.getInt(2));
                productReview.setProductID(result.getInt(3));
                productReview.setCommentReview(result.getString(4));
                productReview.setCommentDate(result.getString(5));
                productReview.setAccountID(result.getInt(6));
                productReview.setRatingStar(result.getInt(7));
                productListReviews.add(productReview);
            }
            if (productListReviews.isEmpty()) {
                return null;
            }
        } catch (Exception errorSql) {
        }
        return productListReviews;
    }

    public static LinkedList<ProductReview> productReviewsList() {
        LinkedList<ProductReview> productListReviews = new LinkedList<>();
        String sqlQuery = "SELECT DISTINCT PR1.STORE_ID, PR2.STAFF_ID, PR1.PRODUCT_ID, PR1.COMMENT, PR1.COMMENT_DATE, PR1.ACCOUNT_ID, PR2.RATING\n"
                + "                FROM PRODUCTS_REVIEW AS PR1, PRODUCTS_RATING AS PR2\n"
                + "                WHERE PR1.ACCOUNT_ID = PR2.ACCOUNT_ID AND (PR1.STORE_ID = PR2.STORE_ID OR PR1.STAFF_ID = PR2.STAFF_ID)";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                ProductReview productReview = new ProductReview();
                productReview.setStoreID(result.getInt(1));
                productReview.setStaffID(result.getInt(2));
                productReview.setProductID(result.getInt(3));
                productReview.setCommentReview(result.getString(4));
                productReview.setCommentDate(result.getString(5));
                productReview.setAccountID(result.getInt(6));
                productReview.setRatingStar(result.getInt(7));
                productListReviews.add(productReview);
            }
            if (productListReviews.isEmpty()) {
                return null;
            }
        } catch (Exception errorSql) {
        }
        return productListReviews;
    }

    public static int changeStatusProduct(int accountID, int statusProduct, boolean switchAccount) {
        String sqlQuery = "UPDATE PRODUCTS SET [STATUS] = 3 WHERE";
        int status = 0;
        if (switchAccount == true) {
            sqlQuery += " STAFF_ID = ?";
        } else {
            sqlQuery += " STORE_ID = ?";
        }
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statemnet = connection.prepareStatement(sqlQuery);
            statemnet.setInt(1, statusProduct);
            statemnet.setInt(2, accountID);
            status = statemnet.executeUpdate();
        } catch (Exception errorSql) {

        }
        return status;
    }

    public static LinkedList<ProductCheckOut> listProductCheckOut(int accountID) {
        LinkedList<ProductCheckOut> listCheckOut = new LinkedList<>();

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM PRODUCT_CHECKOUT WHERE ACCOUNT_ID = ?");
            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                ProductCheckOut checkOut = new ProductCheckOut();
                checkOut.setAccountID(result.getInt(1));
                checkOut.setStoreID(result.getInt(2));
                checkOut.setProductID(result.getInt(3));
                checkOut.setBagDiscount(result.getInt(4));
                checkOut.setEstimatedTax(result.getFloat(5));
                checkOut.setDeliveryStatus(result.getString(6));
                checkOut.setContractID(result.getInt(7));
                checkOut.setOfferPrice(result.getInt(8));
                checkOut.setCreatedDate(result.getString(9));
                listCheckOut.add(checkOut);
            }
            if (listCheckOut.isEmpty()) {
                return null;
            }
        } catch (Exception errorSql) {

        }
        return listCheckOut;
    }

    public static int addToCheckOut(int accountID, int storeID, int productID) {
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO PRODUCT_CHECKOUT(ACCOUNT_ID, STORE_ID, PRODUCT_ID) VALUES(?, ?, ?)");
            statement.setInt(1, accountID);
            statement.setInt(2, storeID);
            statement.setInt(3, productID);
            status = statement.executeUpdate();
        } catch (Exception errorSql) {

        }
        return status;
    }

    public static int removeCheckOut(int accountID, int productID) {
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement("DELETE FROM PRODUCT_CHECKOUT WHERE [PRODUCT_ID] = ? AND [ACCOUNT_ID] = ?");
            statement.setInt(1, productID);
            statement.setInt(2, accountID);
            status = statement.executeUpdate();
        } catch (Exception errorSql) {

        }
        return status;
    }

    public List<Product> getListByPage(List<Product> list, int start, int end) {
        ArrayList<Product> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "select * from PRODUCTS";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getFloat(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getInt(10), rs.getInt(11)));

            }
        } catch (Exception e) {

        }
        return list;
    }

    public void chageProductToSoldOut(int productid) {
        String query = "update Products set [STATUS] = 2 where PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, productid);
            ResultSet rs = statement.executeQuery();

        } catch (Exception e) {

        }
    }

    public void chageProductToSell(int productid) {
        String query = "update Products set [STATUS] = 1 where PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, productid);
            statement.executeQuery();

        } catch (Exception e) {

        }
    }

    public void chageProductToUnavailable(int productid) {
        String query = "update Products set [STATUS] = 3 where PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, productid);
            statement.executeQuery();

        } catch (Exception e) {

        }
    }

    public void setProductImage() {
        String query = "UPDATE PRODUCT_IMAGE SET IMAGE = (SELECT * FROM OPENROWSET(BULK N'C:\\Tai Lieu\\anh.jpg', SINGLE_BLOB) as [IMAGE])";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeQuery();

        } catch (Exception e) {

        }
    }

    public int getNumberOfProduct() {
        String sqlQuery = "SELECT MAX(PRODUCTS.PRODUCT_ID) FROM PRODUCTS";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                return result.getInt(1);
            }
        } catch (Exception e) {
        }
        return -1;
    }

    public void insertProductImage(Product product) {
        String sqlQuery = "INSERT INTO PRODUCT_IMAGE VALUES(?, NULL)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, product.getProductID());

            statement.executeUpdate();
        } catch (Exception e) {
        }

    }

    public static int updateProduct(Product product) {
        int statusUpdate = 0;
        String sqlQuery = "UPDATE PRODUCTS \n"
                + "SET PRODUCT_NAME = ?,LIST_PRICE= ?,BRAND_NAME=?, CATEGORY_NAME= ?,DESCRIPTION=? \n"
                + "WHERE PRODUCT_ID=?";

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, product.getName());
            statement.setFloat(2, product.getPrice());
            statement.setString(3, product.getBrand());

            statement.setNString(4, product.getCategory());
            statement.setNString(5, product.getDescription());
            statement.setInt(6, product.getProductID());

            statusUpdate = statement.executeUpdate();
        } catch (Exception sqlError) {
        }
        return statusUpdate;
    }

    public boolean findProduct(String productID) {
        String sqlQuery = "select * from dbo.PRODUCTS_RENTAL_DETAILS where PRODUCT_ID = ?";
        boolean status = false;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, productID);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                status = true;
            }

        } catch (Exception e) {
        }
        return status;
    }

    public static int updateProductImgae(Product product, byte[] avatarImage) {
        int statusUpdate = 0;
        if (avatarImage != null) {

            String sqlQuery = "UPDATE PRODUCT_IMAGE\n"
                    + "SET IMAGE = ?\n"
                    + "WHERE PRODUCT_ID = ?";

            try {
                Connection connection = new DBContext().getConnection();
                PreparedStatement statement = connection.prepareStatement(sqlQuery);

                statement.setBytes(1, avatarImage);
                statement.setInt(2, product.getProductID());

                statusUpdate = statement.executeUpdate();
            } catch (Exception sqlError) {
            }
        }
        return statusUpdate;
    }

    public void updateRentalProduct(String productId) {
        String sqlQuery = "DELETE FROM PRODUCTS_RENTAL_DETAILS WHERE PRODUCTS_RENTAL_DETAILS.PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);

            statement.setNString(1, productId);

            statement.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void saveDescription(String des, String productId) {
        String sqlQuery = "UPDATE PRODUCTS SET [DESCRIPTION] = ? where PRODUCT_ID = ?";

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setNString(1, des);
            statement.setNString(2, productId);

            statement.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void saveRentalProduct(String productId, String DEPOSIT, String REQUIRED_DOCS, String MILEAGE_LIMIT, String MINIMUMRENTAL, String TOURING) {
        String sqlQuery = "INSERT INTO PRODUCTS_RENTAL_DETAILS(PRODUCT_ID, DEPOSIT, MODE, REQUIRED_DOCS, MILEAGE_LIMIT, [MINIMUM RENTAL], TOURING) VALUES(?, ?, 'RENTAL', ?, ?,? ,?)";

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setNString(1, productId);
            statement.setNString(2, DEPOSIT);
            statement.setString(3, REQUIRED_DOCS);
            statement.setString(4, MILEAGE_LIMIT);
            statement.setString(5, MINIMUMRENTAL);
            statement.setString(6, TOURING);

            statement.executeUpdate();
        } catch (Exception e) {
        }
    }
    public int saveProduct1(Product product) {
        String sqlQuery = "INSERT INTO PRODUCTS(PRODUCT_NAME, BRAND_NAME, CATEGORY_NAME, MODEL_YEAR, LIST_PRICE, [DESCRIPTION], [MODE], STORE_ID, STAFF_ID) VALUES(?, ?, ?, ?, ?, ?,? , ?, ?)";
        int status = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setNString(1, product.getName());
            statement.setNString(2, product.getBrand());
            statement.setString(3, product.getCategory());
            statement.setInt(4, product.getModelYear());
            statement.setFloat(5, product.getPrice());
            statement.setNString(6, product.getDescription());
            statement.setString(7, product.getMode());
            statement.setInt(8, product.getStoreID());
            if (product.getStaffID() != 0) {
                statement.setInt(9, product.getStaffID());
            } else {
                statement.setNull(9, Types.NULL);
            }
            status = statement.executeUpdate();
        } catch (Exception e) {
        }
        return status;
    }
}
