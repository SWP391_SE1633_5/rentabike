/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import model.BlogTag;

/**
 *
 * @author win
 */
public class BlogTagDao {
    public List<BlogTag> getTop5BlogTag(){
        List<BlogTag> list = new ArrayList<>();
        String sql = "with r as(select BLOG_TAG.TAG_ID, count(BLOG_BLOGTAG.BLOG_ID) as Count from BLOG_TAG \n" +
            "left join BLOG_BLOGTAG on BLOG_BLOGTAG.TAG_ID = BLOG_TAG.TAG_ID group by BLOG_TAG.TAG_ID)\n" +
            "select top 5 BLOG_TAG.*, r.Count from BLOG_TAG join r on BLOG_TAG.TAG_ID = r.TAG_ID\n" +
            "order by Count desc";
        BlogTag a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogTag(rs.getInt(1) , rs.getString(2));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    public static List<Integer> getBlogTagIdListByBlogId(int blogId){
        List<Integer> list = new ArrayList<>();
        String sql = "select * from BLOG_BLOGTAG where BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, blogId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(rs.getInt(3));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    public static int countTagByTagId(int tagId){
        String sql = "select count(*) from BLOG_BLOGTAG where TAG_ID=?";
        int count=0;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, tagId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                count=rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return count;
    }
    public static BlogTag findBlogTagByTagTitle(String tagTitle){
        String sql = "select * from BLOG_TAG where TAG_TITLE=?";
        BlogTag a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, tagTitle);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogTag(rs.getInt(1) , rs.getString(2));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static BlogTag findBlogTagByTagId(int tid){
        String sql = "select * from BLOG_TAG where TAG_ID=?";
        BlogTag a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, tid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogTag(rs.getInt(1) , rs.getString(2));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static List<BlogTag> findTopFivBlogTagByTagTitleContain(String search){
        List<BlogTag> list = new ArrayList<>();
        String sql = "with r as(select BLOG_TAG.TAG_ID,  BLOG_TAG.TAG_TITLE, count(BLOG_BLOGTAG.BLOG_ID) as Count from BLOG_TAG \n" +
            "left join BLOG_BLOGTAG on BLOG_BLOGTAG.TAG_ID = BLOG_TAG.TAG_ID where  BLOG_TAG.TAG_TITLE LIKE ? group by BLOG_TAG.TAG_ID,  BLOG_TAG.TAG_TITLE)\n" +
            "select top 5 BLOG_TAG.*, r.Count from BLOG_TAG join r on BLOG_TAG.TAG_ID = r.TAG_ID\n" +
            "order by Count desc";
        BlogTag a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, "%"+search+"%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogTag(rs.getInt(1) , rs.getString(2));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    public static int addNewTag(String tagTitle){
        String sql = "insert into BLOG_TAG values(?)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, tagTitle);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        BlogTag b = findBlogTagByTagTitle(tagTitle);
        return b.getId();
    }
    public static void addNewBlogBlogTag(int blogId, int tagId){
        String sql = "insert into BLOG_BLOGTAG values(?, ?)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, blogId);
            st.setInt(2, tagId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static void deleteBlogTagByTagId(int tagId){
        String sql = "delete from BLOG_TAG where TAG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, tagId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static void delAllRelatedBlogTagByBlogId(int blogId){  
        List<Integer> iList = getBlogTagIdListByBlogId(blogId);
        String sql = "delete from BLOG_BLOGTAG where BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, blogId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        
        for(int i: iList){
            if(countTagByTagId(i)==0){
                deleteBlogTagByTagId(i);
            }
        }
    }
    public static List<BlogTag> getAllBlogTagByBlogId(int id){
        List<BlogTag> list = new ArrayList<>();
        String sql = "select BLOG_TAG.* from Blog join BLOG_BLOGTAG on Blog.BLOG_ID = BLOG_BLOGTAG.BLOG_ID join BLOG_TAG on BLOG_BLOGTAG.TAG_ID = BLOG_TAG.TAG_ID where BLOG.BLOG_ID = ?";
        BlogTag a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogTag(rs.getInt(1) , rs.getString(2));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(list, (o1, o2) -> {
            return o1.getId() - o2.getId();
        });
        return list;
    }
}
