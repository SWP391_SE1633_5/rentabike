/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this templates

 */
package dao;

import Utils.Utils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import model.Assistance;

/**
 *
 * @author win
 */
public class AssistanceDao {

    public List<Assistance> getAllListAssistance() {
        List<Assistance> aList = new ArrayList<>();
        String sql = "select * from Assistance";
        Assistance a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Assistance(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7));
                aList.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(aList, (o1, o2) -> {
            Date dO1 = Utils.convertStringToDate(o1.getCreatedAt());
            Date dO2 = Utils.convertStringToDate(o2.getCreatedAt());
            return dO2.compareTo(dO1);
        });
        return aList;
    }
    public Assistance getAssistanceById(int id){
        Assistance a = null;
        String sql = "select * from Assistance where ASSISTANCE_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = new Assistance(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7));
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return a;
    }
    public void updateQuantUsefullOrnot(int id, int type) {
        String sql = "";
        //useful
        if (type == 1) {
            sql = "UPDATE Assistance\n"
                    + "SET USEFULL = USEFULL + 1\n"
                    + "where ASSISTANCE_ID = ?";
        }else {
            sql = "UPDATE Assistance\n"
                    + "SET NOT_USEFULL = NOT_USEFULL + 1\n"
                    + "where ASSISTANCE_ID = ?";
        }
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return;
    }
    public void addNewAssistance(String ques, String answer, String createdTime){
        String sql = "insert into Assistance values(?, ?, ?, null, 0, 0)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, ques);
            st.setString(2, answer);
            st.setString(3, createdTime);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return;
    }
    public void updAssitance(String ques, String answer, String updatedTime, int id){
        String sql = "update Assistance set [QUESTION]=?, [ANSWER]=?, [UPDATE_AT]=? where [ASSISTANCE_ID]=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, ques);
            st.setString(2, answer);
            st.setString(3, updatedTime);
            st.setInt(4, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return;
    }
    public void delAssistance(int id){
        String sql = "delete from ASSISTANCE where ASSISTANCE_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return;
    }
//    public static void main(String[] args) {
//        AssistanceDao ad = new AssistanceDao();
//        Assistance a = ad.getAssistanceById(2);
//        System.out.println(a.getQuestion());
//    }
}
