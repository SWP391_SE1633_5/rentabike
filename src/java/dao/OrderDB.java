/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Order;
import model.OrderItem;
import model.Product;

/**
 *
 * @author ASUS
 */
public class OrderDB {

    public List<Order> getAllOrder() {
        List<Order> list = new ArrayList<>();
        String sql = "select *, FORMAT (ORDERS.ORDER_DATE, 'dd/MM/yyyy ') as date from ORDERS";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(new Order(rs.getInt(8),rs.getInt(7),rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(9), rs.getDouble(5), rs.getInt(6)));

            }
        } catch (Exception e) {

        }
        return list;
    }
    
    

    public List<Order> getListByPage(List<Order> list, int start, int end) {
        ArrayList<Order> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public void getCustomerInfoForOrder(Order o) {
        String sql = "select [First_Name],[LAST_NAME],[EMAIL],CITY,STREET,STATE,[PHONE_NUMBER] from CUSTOMERS where CUSTOMER_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, o.getCustomerID());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                o.setCustomerFirstName(rs.getString(1));
                o.setCustomerLastName(rs.getString(2));
                o.setCustomerEmail(rs.getString(3));
                o.setCustomerAddress(rs.getString(5) + "," + rs.getString(4) + "," + rs.getString(6));
                o.setCustomerPhoneNum(rs.getString(7));
            }
        } catch (Exception e) {

        }
    }

    public void getStoreForOrderItem(OrderItem o) {
        String sql = "select STORE_NAME,PHONE_NUMBER,EMAIL,CITY,STREET,STATE from STORES where STORES.STORE_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, o.getStoreID());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                o.setStoreName(rs.getString(1));
                o.setPhoneNum(rs.getString(2));
                o.setEmail(rs.getString(3));
                o.setAddress(rs.getString(4) + "," + rs.getString(5) + "," + rs.getString(6));
            }
        } catch (Exception e) {

        }
    }

    public Order getOrderById(String orderID) {
        String sqlQuery = "select *, FORMAT (ORDERS.ORDER_DATE, 'dd/MM/yyyy ') as date from dbo.ORDERS where ORDERS.ORDER_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, orderID);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new Order(rs.getInt(8),rs.getInt(7),rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(9), rs.getDouble(5), rs.getInt(6));
            }

        } catch (Exception e) {
        }
        return null;
    }

    public Product getProductById(int productID) {
        String sqlQuery = "select * from dbo.PRODUCTS where Products.PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productID);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getFloat(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getInt(10), rs.getInt(11));
            }

        } catch (Exception e) {
        }
        return null;
    }

    public double getProductPrice(int productId) {
        String sqlQuery = "select PRODUCTS.LIST_PRICE from dbo.PRODUCTS where PRODUCTS.PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productId);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getDouble(1);
            }

        } catch (Exception e) {
        }
        return -1;
    }
    
    public void setProductImage(Product p) {
        String sqlQuery = "select PRODUCT_IMAGE.[IMAGE] from dbo.PRODUCT_IMAGE where PRODUCT_IMAGE.PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, p.getProductID());

            ResultSet rs = statement.executeQuery();
            
            while (rs.next()) {
                if (rs.getString(1) != null) {
                    p.setImage(p.convertByteArrayToBase64(rs.getBytes(1)));
                } else {
                    p.setImage(rs.getString(13));
                }
            }

        } catch (Exception e) {
        }
    }

    public int getProductStoreId(int productId) {
        String sqlQuery = "select PRODUCTS.STORE_ID from dbo.PRODUCTS where PRODUCTS.PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productId);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }

        } catch (Exception e) {
        }
        return -1;
    }

    public void updateOrderItemShippedDate(String orderItemId, String date) {
        String sqlQuery = "UPDATE ORDER_ITEM SET ORDER_ITEM.SHIPPED_DATE = ? WHERE ORDER_ITEM.ORDER_ITEM_ID =  ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, date);
            statement.setString(2, orderItemId);
            statement.executeQuery();

        } catch (Exception e) {
        }
        

    }
    public int getAverageRating(int productID) {
        int ratingStar = 0;
        String sqlQuery = "SELECT AVG(RATING) AS TOTAL_RATING FROM PRODUCTS_RATING WHERE PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productID);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                ratingStar = result.getInt(1);
            } else {
                return ratingStar;
            }
        } catch (Exception e) {
        }
        return ratingStar;
    }
    public void updateOrderItemStatus(String orderItemId, String status) {
        String sqlQuery = "UPDATE ORDER_ITEM SET ORDER_ITEM.STATUS = ? WHERE ORDER_ITEM.ORDER_ITEM_ID =  ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, status);
            statement.setString(2, orderItemId);
            statement.executeQuery();

        } catch (Exception e) {
        }
        

    }
    public void updateOrderStatus(int orderId, String status) {
        String sqlQuery = "UPDATE ORDERS SET ORDERS.ORDER_STATUS = ? WHERE ORDERS.ORDER_ID =  ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, status);
            statement.setInt(2, orderId);
            statement.executeQuery();

        } catch (Exception e) {
        }
        

    }
    
    public void updateOrderDiscountAndTaxes(int orderId, String discount,String taxes) {
        String sqlQuery = "UPDATE ORDERS SET ORDERS.DISCOUNT = ?,  ORDERS.TAXES = ? WHERE ORDERS.ORDER_ID =  ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, discount);
            statement.setString(2, taxes);
            statement.setInt(3, orderId);
            statement.executeQuery();

        } catch (Exception e) {
        }
        

    }
    
    public void updateOrderPayStatus(int orderId, String status) {
        String sqlQuery = "UPDATE ORDERS SET ORDERS.PAY_STATUS = ? WHERE ORDERS.ORDER_ID =  ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, status);
            statement.setInt(2, orderId);
            statement.executeQuery();

        } catch (Exception e) {
        }
        

    }
    

    public List<OrderItem> getAllOrderItem(Order o) {
        List<OrderItem> list = new ArrayList<>();
        String sql = "select *, FORMAT (ORDER_ITEM.ORDER_DATE, 'dd/MM/yyyy ') as OrderDate, FORMAT (ORDER_ITEM.SHIPPED_DATE, 'dd/MM/yyyy ') as ShippedDate from ORDER_ITEM where ORDER_ITEM.ORDER_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, o.getOrderID());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(new OrderItem(rs.getInt(11),rs.getInt(10), rs.getInt(1), rs.getInt(2), rs.getInt(3), getProductStoreId(rs.getInt(2)), rs.getInt(5), rs.getInt(7), rs.getString(12), rs.getString(13),rs.getDouble(6)));

            }
        } catch (Exception e) {

        }
        return list;
    }
    
    public List<OrderItem> getAllOrderItemOfCustomer(int cusId) {
        List<OrderItem> list = new ArrayList<>();
        String sql = "select *, FORMAT (ORDER_ITEM.ORDER_DATE, 'dd/MM/yyyy ') as OrderDate, FORMAT (ORDER_ITEM.SHIPPED_DATE, 'dd/MM/yyyy ') as ShippedDate from ORDER_ITEM where ORDER_ITEM.CUSTOMER_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, cusId);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(new OrderItem(rs.getInt(11),rs.getInt(10), rs.getInt(1), rs.getInt(2), rs.getInt(3), getProductStoreId(rs.getInt(2)), rs.getInt(5), rs.getInt(7), rs.getString(12), rs.getString(13),rs.getDouble(6)));

            }
        } catch (Exception e) {

        }
        return list;
    }
    
    public List<OrderItem> getAllProcessingOrderItemOfCustomer(int cusId) {
        List<OrderItem> list = new ArrayList<>();
        String sql = "select *, FORMAT (ORDER_ITEM.ORDER_DATE, 'dd/MM/yyyy ') as OrderDate, FORMAT (ORDER_ITEM.SHIPPED_DATE, 'dd/MM/yyyy ') as ShippedDate from ORDER_ITEM where ORDER_ITEM.CUSTOMER_ID = ? and ORDER_ITEM.[STATUS] != 4";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, cusId);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(new OrderItem(rs.getInt(11),rs.getInt(10), rs.getInt(1), rs.getInt(2), rs.getInt(3), getProductStoreId(rs.getInt(2)), rs.getInt(5), rs.getInt(7), rs.getString(12), rs.getString(13),rs.getDouble(6)));

            }
        } catch (Exception e) {

        }
        return list;
    }
    public static int countOrder(String sqlQuery) {
        int count = 0;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                count = result.getInt(1);
            }
        } catch (Exception e) {
        }
        return count;
    }
    public List<OrderItem> getAllCompletedOrderItemOfCustomer(int cusId) {
        List<OrderItem> list = new ArrayList<>();
        String sql = "select *, FORMAT (ORDER_ITEM.ORDER_DATE, 'dd/MM/yyyy ') as OrderDate, FORMAT (ORDER_ITEM.SHIPPED_DATE, 'dd/MM/yyyy ') as ShippedDate from ORDER_ITEM where ORDER_ITEM.CUSTOMER_ID = ? and ORDER_ITEM.[STATUS] = 4";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, cusId);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(new OrderItem(rs.getInt(11),rs.getInt(10), rs.getInt(1), rs.getInt(2), rs.getInt(3), getProductStoreId(rs.getInt(2)), rs.getInt(5), rs.getInt(7), rs.getString(12), rs.getString(13),rs.getDouble(6)));

            }
        } catch (Exception e) {

        }
        return list;
    }
    

}
