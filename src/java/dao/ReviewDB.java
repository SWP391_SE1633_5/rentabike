/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import model.Product;
import model.Review;

/**
 *
 * @author ASUS
 */
public class ReviewDB {
    public void insertReview(Review review) {
        String sqlQuery = "INSERT INTO PRODUCTS_REVIEW(STORE_ID,PRODUCT_ID,COMMENT,ACCOUNT_ID) values(?, ?, ?,?)";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, review.getStoreId());
            statement.setInt(2, review.getProductId());
            statement.setString(3, review.getComment());
            statement.setInt(4, review.getAuthor());
            statement.executeUpdate();
        } catch (Exception e) {
        }
        
    }
    public int getNumberOfReview() {
        String sqlQuery = "SELECT MAX(PRODUCTS_REVIEW.REVIEW_ID) FROM PRODUCTS_REVIEW";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                return result.getInt(1);
            }
        } catch (Exception e) {
        }
        return -1;
    }
    public void insertRatingStar(Review review) {
        String sqlQuery = "INSERT INTO PRODUCTS_RATING(STORE_ID,STAFF_ID,PRODUCT_ID,RATING,ACCOUNT_ID,REVIEW_ID) values(?,?,?,?,?,?)";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, review.getStoreId());
            statement.setInt(2, 1);
            statement.setInt(3, review.getProductId());
            statement.setInt(4, review.getRatingStar());
            statement.setInt(5, review.getAuthor());
            statement.setInt(6, review.getReviewId());
            statement.executeUpdate();
        } catch (Exception e) {
        }
        
    }
    
    public int getStoreId(int productId) {
        int storeId = -1;
        String sqlQuery = "select STORE_ID from Products where PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, productId);          
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                storeId = result.getInt(1);
                return storeId;
            }
        } catch (Exception e) {
        }
        return storeId;
    }
    
    public List<Review> getAllReviewOfProduct(int productId) {
        List<Review> list = new ArrayList<>();
        String sql = "select * from PRODUCTS_REVIEW where PRODUCT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, productId);          
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(new Review(rs.getInt(1),productId,rs.getString(3),rs.getInt(5),getNameOfCustomer(rs.getInt(5)),getRoleCustomer(rs.getInt(5)),getRatingStarById(rs.getInt(6))));

            }
        } catch (Exception e) {

        }
        return list;
    }
    
    public String getNameOfCustomer(int accountID) {
        String name = "";
        String sql = "select [First_name], [Last_Name] from dbo.CUSTOMERS where CUSTOMERS.ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, accountID);          
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                name = rs.getString(1) +" "+ rs.getString(2);

            }
        } catch (Exception e) {

        }
        return name;
    }
    
    public String getRoleCustomer(int accountID) {
        String role = "";
        String sql = "select [ROLE] from dbo.ACCOUNT where ACCOUNT.ACCOUNT_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, accountID);          
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                role = rs.getString(1);

            }
        } catch (Exception e) {

        }
        return role;
    }
    
    public int getRatingStar(Review r) {
        int star = -1;
        String sql = "select RATING from dbo.PRODUCTS_RATING where PRODUCTS_RATING.REVIEW_ID = ? ";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, r.getReviewId());    
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                star = rs.getInt(1);

            }
        } catch (Exception e) {

        }
        return star;
    }
    
    public int getRatingStarById(int reviewID) {
        int star = -1;
        String sql = "select RATING from dbo.PRODUCTS_RATING where PRODUCTS_RATING.REVIEW_ID = ? ";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, reviewID);    
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                star = rs.getInt(1);

            }
        } catch (Exception e) {

        }
        return star;
    }
    public boolean findProduct(String productID) {
        String sqlQuery = "select * from dbo.PRODUCTS_RENTAL_DETAILS where PRODUCT_ID = ?";
        boolean status = false;
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, productID);
            
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                status = true;
            }

        } catch (Exception e) {
        }
        return status;
    }
}
