/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import model.Product;
import model.Store;

/**
 *
 * @author baqua
 */
public class StoreDB extends DBContext {

    Connection cnn;//kết nối đến db
    Statement stm;//thực thi các câu lệnh sql
    ResultSet rs;//lưu trữ và xử lý dữu liệu 
    PreparedStatement ptm;

    public static Store searchStore() {
        Store store = new Store();
        String sqlQuery = "SELECT S.STORE_ID, S.ACCOUNT_ID, S.STORE_NAME, S.PHONE_NUMBER, S.EMAIL, S.CITY, S.STREET, S.STATE,S.ROLE ,C.FIRST_NAME,C.LAST_NAME,S.STATUS\n"
                + "FROM STORES as S, CUSTOMERS as C WHERE S.ACCOUNT_ID=C.ACCOUNT_ID";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                store.setStoreID(result.getInt(1));
                store.setAccountID(result.getInt(2));
                store.setStoreName(result.getNString(3));
                store.setPhoneNumber(result.getString(4));
                store.setEmail(result.getString(5));
                store.setCity(result.getNString(6));
                store.setStreet(result.getNString(7));
                store.setState(result.getNString(8));
                store.setRole(result.getString(9));
                store.setFirstName(result.getNString(10));
                store.setLastName(result.getNString(11));
                store.setStatus(result.getInt(12));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return store;
    }

    public ArrayList<Store> getAllStore() {
        Store store = new Store();
        ArrayList<Store> list = new ArrayList<Store>();
        String sqlQuery = "SELECT S.STORE_ID, S.ACCOUNT_ID, S.STORE_NAME, S.PHONE_NUMBER, S.EMAIL, S.CITY, S.STREET, S.STATE,S.ROLE ,C.FIRST_NAME,C.LAST_NAME, S.STATUS\n"
                + "FROM STORES as S, CUSTOMERS as C WHERE S.ACCOUNT_ID=C.ACCOUNT_ID ";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                int storeID = result.getInt(1);
                int accountID = result.getInt(2);
                String storeName = result.getNString(3);
                String phoneNumber = result.getString(4);
                String email = result.getString(5);
                String city = result.getNString(6);
                String street = result.getNString(7);
                String state = result.getNString(8);
                String role = result.getString(9);
                String firstName = result.getNString(10);
                String lastName = result.getNString(11);
                int status = result.getInt(12);
                list.add(new Store(storeID, accountID, storeName, phoneNumber, email, city, street, state, role, firstName, lastName, status));

            }
        } catch (Exception sqlError) {
        }
        return list;
    }
    public ArrayList<Store> getAllStoreAvailable() {
        Store store = new Store();
        ArrayList<Store> list = new ArrayList<Store>();
        String sqlQuery = "SELECT S.STORE_ID, S.ACCOUNT_ID, S.STORE_NAME, S.PHONE_NUMBER, S.EMAIL, S.CITY, S.STREET, S.STATE,S.ROLE ,C.FIRST_NAME,C.LAST_NAME, S.STATUS\n"
                + "FROM STORES as S, CUSTOMERS as C WHERE S.ACCOUNT_ID=C.ACCOUNT_ID AND S.STATUS = 1";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                int storeID = result.getInt(1);
                int accountID = result.getInt(2);
                String storeName = result.getNString(3);
                String phoneNumber = result.getString(4);
                String email = result.getString(5);
                String city = result.getNString(6);
                String street = result.getNString(7);
                String state = result.getNString(8);
                String role = result.getString(9);
                String firstName = result.getNString(10);
                String lastName = result.getNString(11);
                int status = result.getInt(12);
                list.add(new Store(storeID, accountID, storeName, phoneNumber, email, city, street, state, role, firstName, lastName, status));

            }
        } catch (Exception sqlError) {
        }
        return list;
    }

    public ArrayList<Store> getByPage(ArrayList<Store> listall, int start, int end) {
        ArrayList<Store> list = new ArrayList<Store>();
        for (int i = start; i < end; i++) {
            list.add(listall.get(i));
        }
        return list;
    }

    public int getNumberOfStore() {
        int i = 0;
        String query = "SELECT MAX(STORES.STORE_ID) FROM STORES";
        try {
            ptm = getConnection().prepareStatement(query);
            rs = ptm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return i;
    }

    public int getNumberOfAvailableStore() {
        int i = 0;
        String query = "select count (STORES.STORE_ID)\n"
                + "From STORES\n"
                + "where STORES.[STATUS] = 1";
        try {
            ptm = getConnection().prepareStatement(query);
            rs = ptm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return i;
    }

    public int getNumberOfUnavailableStore() {
        int i = 0;
        String query = "select count (STORES.STORE_ID)\n"
                + "From STORES\n"
                + "where STORES.[STATUS] = 3";
        try {
            ptm = getConnection().prepareStatement(query);
            rs = ptm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return i;
    }

    public int getNumberOfBandStore() {
        int i = 0;
        String query = "select count (STORES.STORE_ID)\n"
                + "From STORES\n"
                + "where STORES.[STATUS] = 2";
        try {
            ptm = getConnection().prepareStatement(query);
            rs = ptm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return i;
    }

    public static Store getStoreByStoreID(int storeID) {
        Store store = new Store();
        String sqlQuery = "SELECT S.STORE_ID, S.ACCOUNT_ID, S.STORE_NAME, S.PHONE_NUMBER, S.EMAIL, S.CITY, S.STREET, S.STATE,S.ROLE ,C.FIRST_NAME,C.LAST_NAME\n"
                + "FROM STORES as S, CUSTOMERS as C WHERE S.ACCOUNT_ID=C.ACCOUNT_ID and S.STORE_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, storeID);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                store.setStoreID(result.getInt(1));
                store.setAccountID(result.getInt(2));
                store.setStoreName(result.getNString(3));
                store.setPhoneNumber(result.getString(4));
                store.setEmail(result.getString(5));
                store.setCity(result.getNString(6));
                store.setStreet(result.getNString(7));
                store.setState(result.getNString(8));
                store.setRole(result.getString(9));
                store.setFirstName(result.getNString(10));
                store.setLastName(result.getNString(11));
            } else {
                return null;
            }
        } catch (Exception sqlError) {
        }
        return store;
    }

    public ArrayList<Product> getProductFromStore(int storeID) {
        Product product = new Product();
        ArrayList<Product> list = new ArrayList<Product>();
        String sqlQuery = "select p.PRODUCT_ID, p.PRODUCT_NAME, p.BRAND_NAME, p.CATEGORY_NAME, p.MODEL_YEAR, p.LIST_PRICE, p.[DESCRIPTION], p.[STATUS],p.[MODE], p.STORE_ID, p.STAFF_ID,p.CREATE_AT from PRODUCTS as p where STORE_ID = ? ";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, storeID);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                
                int ProductID = result.getInt(1);
                String storeName = result.getNString(2);
                String brand = result.getString(3);
                String category = result.getString(4);
                int modelYear = result.getInt(5);
                float price = result.getFloat(6);
                String description = result.getString(7);
                int status = result.getInt(8);
                String mode = result.getString(9);

                int staffID = result.getInt(11);
                String createAt = result.getString(12);
                
                
                list.add(new Product(ProductID, storeName, brand, category, modelYear, price, description, status, mode, staffID, storeID, createAt));

            }
        } catch (Exception sqlError) {
        }
        return list;
    }

    public static LinkedList<Product> listProduct(int storeID) {
        LinkedList<Product> listProduct = new LinkedList<>();
        String sqlQuery = "select * from PRODUCTS where STORE_ID = ? and status =3";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, storeID);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Product product = new Product();
                product.setProductID(result.getInt(1));
                product.setName(result.getNString(2));
                product.setBrand(result.getString(3));
                product.setCategory(result.getString(4));
                product.setModelYear(result.getInt(5));
                product.setPrice(result.getFloat(6));
                product.setDescription(result.getString(7));
                product.setStatus(result.getInt(8));
                product.setMode(result.getString(9));
                product.setStoreID(result.getInt(10));
                product.setStaffID(result.getInt(11));
                product.setCreateAt(result.getString(12));
                
                listProduct.add(product);
            }
            if (listProduct.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return listProduct;
    }

    public int getNumberOfProductOfStore(int storeID) {

        String sqlQuery = "SELECT COUNT (PRODUCTS.PRODUCT_ID)\n"
                + "FROM PRODUCTS\n"
                + "WHERE STORE_ID = ? and [STATUS] = 3";

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);

            statement.setInt(1, storeID);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                return result.getInt(1);
            }
        } catch (Exception sqlError) {
        }
        return -1;
    }

    public int getStoreID(int accountID) {

        String sqlQuery = "select STORES.STORE_ID from STORES where ACCOUNT_ID = ?";

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);

            statement.setInt(1, accountID);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                return result.getInt(1);
            }
        } catch (Exception sqlError) {
        }
        return -1;
    }

    public void registerNewStore(Store store) {
        String sqlQuery = "INSERT INTO STORES "
                + "VALUES(?,?,? , ?,?, ?,? ,? ,?,?)";

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, store.getAccountID());
            statement.setNString(2, store.getStoreName());
            statement.setString(3, store.getPhoneNumber());
            statement.setString(4, store.getEmail());
            statement.setNString(5, store.getCity());
            statement.setNString(6, store.getStreet());
            statement.setNString(7, store.getState());
            statement.setString(8, "SALEPERSON");
            statement.setInt(9, 3);
            statement.setNString(10, store.getDescription());
            ResultSet result = statement.executeQuery();
        } catch (Exception e) {
        }

    }
    public void chageStoreToBand(int productid) {
        String query = "update Stores set [STATUS] = 2 where STORE_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, productid);
            ResultSet rs = statement.executeQuery();

        } catch (Exception e) {

        }
    }

    public void chageStoreToAvailable(int productid) {
        String query = "update Stores set [STATUS] = 1 where STORE_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, productid);
            statement.executeQuery();

        } catch (Exception e) {

        }
    }

    public void chageStoreToUnavailable(int productid) {
        String query = "update Stores set [STATUS] = 3 where STORE_ID = ?";
        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, productid);
            statement.executeQuery();

        } catch (Exception e) {

        }
    }

}
