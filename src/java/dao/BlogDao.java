/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Utils.Utils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import model.Blog;
/**
 *
 * @author win
 */
public class BlogDao{
    public Blog getAuthenBlogbyBlogId(int bId, int accId){
        String sql = "select * from BLOG where BLOG_ID=? and (STATUS = 1 or ACCOUNT_ID=?)";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, bId);
            st.setInt(2, accId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static Blog getBlogbyBlogIdAndAccId(int bid, int aid){
        String sql = "select * from BLOG where BLOG_ID=? and ACCOUNT_ID=?";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, bid);
            st.setInt(2, aid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public List<Blog> getAllAuthenBlogs(){
        List<Blog> list = new ArrayList<>();
        String sql = "select * from BLOG where STATUS = 1";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(list, (o1, o2) -> {
            Date dO1 = Utils.convertStringToDate(o1.getCreatedAt());
            Date dO2 = Utils.convertStringToDate(o2.getCreatedAt());
            return dO2.compareTo(dO1);
        });
        return list;
    }
    public List<Blog> getBlogsbyTagTitle(String tagTitle){
        List<Blog> list = new ArrayList<>();
        String sql = "select BLOG.* from Blog join BLOG_BLOGTAG on Blog.BLOG_ID = BLOG_BLOGTAG.BLOG_ID join BLOG_TAG on BLOG_BLOGTAG.TAG_ID = BLOG_TAG.TAG_ID where BLOG_TAG.TAG_TITLE = ? and STATUS = 1";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, tagTitle);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(list, (o1, o2) -> {
            Date dO1 = Utils.convertStringToDate(o1.getCreatedAt());
            Date dO2 = Utils.convertStringToDate(o2.getCreatedAt());
            return dO2.compareTo(dO1);
        });
        return list;
    }
    
    public static List<Blog> getBlogListByAcccountId(int accountId){
        List<Blog> list = new ArrayList<>();
        String sql = "select * from BLOG where ACCOUNT_ID=? and STATUS = 1";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(list, (o1, o2) -> {
            Date dO1 = Utils.convertStringToDate(o1.getCreatedAt());
            Date dO2 = Utils.convertStringToDate(o2.getCreatedAt());
            return dO2.compareTo(dO1);
        });
        return list;
    }
    public static List<Blog> getUnAuthenBlogListByAcccountId(int accountId){
        List<Blog> list = new ArrayList<>();
        String sql = "select * from BLOG where ACCOUNT_ID=? and STATUS = 0";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(list, (o1, o2) -> {
            Date dO1 = Utils.convertStringToDate(o1.getCreatedAt());
            Date dO2 = Utils.convertStringToDate(o2.getCreatedAt());
            return dO2.compareTo(dO1);
        });
        return list;
    }
    public static Blog getBlogByAcccountIdAndTimeCreated(int accountId, String createdAt){
        String sql = "select * from BLOG where ACCOUNT_ID=? and CREATE_AT=?";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            st.setString(2, createdAt);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static int addBlog(int accountId, String content, String title, String description, String image, String createTime, String role){
        String sql = "insert into BLOG values (?, ?, ?, ?, ?, ?, null, 0, 0)";
        if(role.equals("ADMIN"))
            sql = "insert into BLOG values (?, ?, ?, ?, ?, ?, null, 1, 1)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            st.setString(2, content);
            st.setString(3, title);
            st.setString(4, description);
            st.setString(5, image);
            st.setString(6, createTime);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        Blog b = getBlogByAcccountIdAndTimeCreated(accountId, createTime);
        return b.getId();
    }
    public static int updateBlog(int bid, String content, String title, String description, String image, String updateTime, String role){
        String sql = "update BLOG set CONTENT=?, BLOG_TITLE=?, BLOG_DESCRIPTION=?, IMAGE=?, UPDATE_AT=?, STATUS=0, ISREAD=0 where BLOG_ID=?";
        if(role.equals("ADMIN"))
            sql = "update BLOG set CONTENT=?, BLOG_TITLE=?, BLOG_DESCRIPTION=?, IMAGE=?, UPDATE_AT=?, STATUS=1, ISREAD=0 where BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, content);
            st.setString(2, title);
            st.setString(3, description);
            st.setString(4, image);
            st.setString(5, updateTime);
            st.setInt(6, bid);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return bid;
    }
    public static int deleteBlogByBlogId(int bid){
        String sql = "delete from BLOG where BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, bid);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return bid;
    }
    
    public List<Blog> adminGetAllUnAuthenBlogs(){
        List<Blog> list = new ArrayList<>();
        String sql = "select * from BLOG where STATUS = 0";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(list, (o1, o2) -> {
            Date dO1 = Utils.convertStringToDate(o1.getCreatedAt());
            Date dO2 = Utils.convertStringToDate(o2.getCreatedAt());
            return dO2.compareTo(dO1);
        });
        return list;
    }
    public static List<Blog> adminSearchAllUnAuthenBlogs(String search){
        List<Blog> list = new ArrayList<>();
        String sql = "select * from BLOG where (BLOG_TITLE like ? or BLOG_DESCRIPTION like ? ) and STATUS = 0";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setString(1, "%"+search+"%");
            st.setString(2, "%"+search+"%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        Collections.sort(list, (o1, o2) -> {
            Date dO1 = Utils.convertStringToDate(o1.getCreatedAt());
            Date dO2 = Utils.convertStringToDate(o2.getCreatedAt());
            return dO2.compareTo(dO1);
        });
        return list;
    }
    public static void adminAcceptBlog(int bid){
        String sql = "update BLOG set STATUS=1 where BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, bid);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static void updateAdminReadState(int bid, int type){
        String sql = "update BLOG set ISREAD=? where BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, type);
            st.setInt(2, bid);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static Blog adminGetBlogByBlogId(int bid){
        String sql = "select * from BLOG where BLOG_ID=? and STATUS=0";
        Blog a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, bid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = new Blog(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getBoolean(9), rs.getBoolean(10));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static void adminChangeAcceptToPending(int bid){
        String sql = "update BLOG set STATUS=0 where BLOG_ID=?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, bid);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
