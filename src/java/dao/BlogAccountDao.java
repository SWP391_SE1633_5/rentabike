/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Utils.Utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import model.BlogComment;
import model.BlogCommentReact;

/**
 *
 * @author win
 */
public class BlogAccountDao {
    //max 6
    public static List<Object> getRecentCmtInfoListByAccountId(int accountId){
        List<Object> cmtInfoList = new ArrayList<>();
        List<Object> objectList = new ArrayList<>();
        List<Object> tmpList = new ArrayList<>();
        List<BlogComment> ownCmtList = BlogCommentDao.getBlogCommentByAccountId(accountId);
        List<BlogCommentReact> ownReactList = BlogCommentReactDao.getCommentReactListByAccountId(accountId);
        tmpList.addAll(ownCmtList);
        tmpList.addAll(ownReactList);
        if(tmpList.size()<5)
            objectList.addAll(tmpList);
        else {
            Collections.sort(tmpList, (o1, o2) -> {
                String date1="", date2="";
                if(isBlogComment(o1)){
                    BlogComment b = (BlogComment)o1;
                    if(b.getUpdateTime()!=null) date1 = b.getUpdateTime();
                    else date1 = b.getTimeComment();
                }
                else if(isBlogCommtReact(o1)){
                    BlogCommentReact b = (BlogCommentReact)o1;
                    date1 = b.getTimeReact();
                }
                if(isBlogComment(o2)){
                    BlogComment b = (BlogComment)o2;
                    if(b.getUpdateTime()!=null) date2 = b.getUpdateTime();
                    else date2 = b.getTimeComment();
                }
                else if(isBlogCommtReact(o2)){
                    BlogCommentReact b = (BlogCommentReact)o2;
                    date2 = b.getTimeReact();
                }
                return Utils.convertStringToDate(date2).compareTo(Utils.convertStringToDate(date1));
            });            
            for(int i=0; i<5; i++){
                objectList.add(tmpList.get(i));
            }
        }
        
        for(Object o: objectList){
            HashMap<Object, Object> map = new HashMap<>();
            if(isBlogComment(o)){
                BlogComment b = (BlogComment)o;
                map.put("blogCmt", b);
                //get account which is replied if exist
                if(b.getReplyId()!=0){
                    BlogComment bTmp = BlogCommentDao.getBlogCommentByBlogCmtId(b.getReplyId());
                    if (UserDB.searchAccountID(bTmp.getAccountId()) != null) {
                        map.put("accountReplied", UserDB.searchAccountID(bTmp.getAccountId()));
                    } else if (StaffDB.searchAccountID(bTmp.getAccountId()) != null) {
                        map.put("accountReplied", StaffDB.searchAccountID(bTmp.getAccountId()));
                    }
                }
            }
            else if(isBlogCommtReact(o)){
                BlogCommentReact b = (BlogCommentReact)o;
                map.put("blogCmt", BlogCommentDao.getBlogCommentByBlogCmtId(b.getCommentId()));
                if (UserDB.searchAccountID(b.getAccountId()) != null) {
                    map.put("accountReacted", UserDB.searchAccountID(b.getAccountId()));
                } else if (StaffDB.searchAccountID(b.getAccountId()) != null) {
                    map.put("accountReacted", StaffDB.searchAccountID(b.getAccountId()));
                }
                map.put("reactType", BlogCommentReactDao.getCommentReactTypeByReactId(b.getReactId()));
            }
            cmtInfoList.add(map);
        }
        
        return cmtInfoList;
    }
    //check object is BlogComment
    public static boolean isBlogComment(Object o){
        try {
            BlogComment b = (BlogComment)o;
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    //check object is BlogCommentReact
    public static boolean isBlogCommtReact(Object o){
        try {
            BlogCommentReact b = (BlogCommentReact)o;
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
