/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.BlogAccountSave;
/**
 *
 * @author win
 */
public class BlogAccountSaveDao {
    public static List<BlogAccountSave> getBlogSaveListByAccountId(int id){
        List<BlogAccountSave> list = new ArrayList<>();
        String sql = "select  * from BLOG_ACCOUNT_SAVE where ACCOUNT_ID=?";
        BlogAccountSave a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogAccountSave(rs.getInt(1), rs.getInt(2), rs.getInt(3));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    public static BlogAccountSave getBlogAccountSaveByBlogIdAndAccId(int blogId, int accId){
        String sql = "select * from BLOG_ACCOUNT_SAVE where ACCOUNT_ID=? and BLOG_ID = ?";
        BlogAccountSave a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accId);
            st.setInt(2, blogId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = new BlogAccountSave(rs.getInt(1), rs.getInt(2), rs.getInt(3));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static boolean insertBlogAccountSaveWithBlogIdAndAccId(int blogId, int accountId){
        String sql = "insert into BLOG_ACCOUNT_SAVE values(?, ?)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, blogId);
            st.setInt(2, accountId);
            st.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }
    public static boolean DeleteBlogAccountSaveWithBlogIdAndAccId(int blogId, int accountId){
        String sql = "delete from BLOG_ACCOUNT_SAVE where ACCOUNT_ID=? and BLOG_ID = ?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            st.setInt(2, blogId);
            st.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }
    public static void DeleteBlogAccountSaveByBlogId(int blogId){
        String sql = "delete from BLOG_ACCOUNT_SAVE where BLOG_ID = ?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, blogId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
