/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.BlogCommentReact;
import model.BlogReactType;

/**
 *
 * @author win
 */
public class BlogCommentReactDao {
    public static List<BlogCommentReact> getCommentReactListByCommentId(int id){
        List<BlogCommentReact> list = new ArrayList<>();
        String sql = "select  * from BLOG_COMMENT_REACT where COMMENT_ID=?";
        BlogCommentReact a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogCommentReact(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getString(5));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    public static List<Object> getCommentReactListDataByCommentId(int id){
        List<Object> list = new ArrayList<>();
        
        String sql = "with r1 as(select * from BLOG_COMMENT_REACT where COMMENT_ID=?)\n" +
                        "select BLOG_REACT_TYPE.*, COUNT(r1.REACT_ID) as Count from BLOG_REACT_TYPE left join r1 on BLOG_REACT_TYPE.REACT_ID = r1.REACT_ID\n" +
                        "group by BLOG_REACT_TYPE.DESCRIPTION, BLOG_REACT_TYPE.REACT_ID, BLOG_REACT_TYPE.URL order by Count desc";
        BlogReactType a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                HashMap<Object, Object> map = new HashMap<>();
                a = new BlogReactType(rs.getInt(1), rs.getString(2), rs.getString(3));
                int count = rs.getInt(4);
                map.put("BlogReactType", a);
                map.put("count", count);
                list.add(map);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    public static void deleteCommmentReactByCmtIdAndAccId(int cmtId, int accId){
        String sql = "delete from BLOG_COMMENT_REACT where ACCOUNT_ID=? and COMMENT_ID = ?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accId);
            st.setInt(2, cmtId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static void insertCmtReact(int cmtId, int accId, int reactId, String currentTime){
        String sql = "insert into BLOG_COMMENT_REACT values(?, ?, ?, ?)";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accId);
            st.setInt(2, cmtId);
            st.setInt(3, reactId);
            st.setString(4, currentTime);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static BlogCommentReact getCommentReactByCmtIdAndAccId(int cmtId, int accId){
        String sql = "select * from BLOG_COMMENT_REACT where ACCOUNT_ID=? and COMMENT_ID = ?";
        BlogCommentReact a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accId);
            st.setInt(2, cmtId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = new BlogCommentReact(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getString(5));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static void handleAddNewCmtReact(int cmtId, int accId, int reactId, String currentTime){
        BlogCommentReact b = getCommentReactByCmtIdAndAccId(cmtId, accId);
        if(b!=null && b.getReactId()==reactId){
            deleteCommmentReactByCmtIdAndAccId(cmtId, accId);
        }else {
            deleteCommmentReactByCmtIdAndAccId(cmtId, accId);
            insertCmtReact(cmtId, accId, reactId, currentTime);
        } 
    } 
    public static void deleteCommmentReactByCmtId(int cmtId){
        String sql = "delete from BLOG_COMMENT_REACT where COMMENT_ID = ?";
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, cmtId);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static BlogReactType getCommentReactTypeByReactId(int reactId){
        String sql = "select * from BLOG_REACT_TYPE where REACT_ID=?";
        BlogReactType a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, reactId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                a = new BlogReactType(rs.getInt(1), rs.getString(2), rs.getString(3));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }
    public static List<BlogCommentReact> getCommentReactListByAccountId(int accountId){
        List<BlogCommentReact> list = new ArrayList<>();
        String sql = "select  * from BLOG_COMMENT_REACT where ACCOUNT_ID=?";
        BlogCommentReact a = null;
        try {
            PreparedStatement st = new DBContext().getConnection().prepareStatement(sql);
            st.setInt(1, accountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new BlogCommentReact(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getString(5));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
}
