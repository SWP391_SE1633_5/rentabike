<%-- 
    Document   : app-ecommerce-list
    Created on : Sep 21, 2022, 11:04:25 AM
    Author     : ADMIN
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->

        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-file-uploader.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-validation.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-file-uploader.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover"
          data-menu="horizontal-menu" data-col="">

        <!-- BEGIN: Header-->
        <nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center"
             data-nav="brand-center">
            <div class="navbar-header d-xl-block d-none">
                <ul class="nav navbar-nav">
                    <li class="nav-item">
                        <a class="navbar-brand" href="productListController">
                            <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                            <h2 class="brand-text mb-0 lidget-app__padding-left">Hilfsmotor</h2>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calendar.html" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Calendar"><i class="ficon" data-feather="calendar"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning"
                                                                                                    data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="#"
                                                                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"
                                                                                                        data-language="en"><i class="flag-icon flag-icon-us"></i> English</a>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon"
                                                                                                 data-feather="sun"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon"
                                                                                           data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Hilfsmotor..." tabindex="-1"
                                   data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#"
                                                                                 data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span
                                class="badge rounded-pill bg-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-15.jpg"
                                                                     alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small
                                                class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar"
                                                                     width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small
                                                class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small
                                                class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="list-item d-flex align-items-center">
                                    <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                    <div class="form-check form-check-primary form-switch">
                                        <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="form-check-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small
                                                class="notification-text"> USA Server is down due to hight CPU usage</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small
                                                class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small
                                                class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle dropdown-user-link"
                           id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none">
                                <span class="user-name fw-bolder">${staff.getUserName()}</span>
                                <span class="user-status">${staff.getRole()}</span></div>
                            <span class="avatar">
                                <c:choose>
                                    <c:when test="${staff.getAvatar() == null}">
                                        <c:set var="firstName" value="${staff.getFirstName()}"/>
                                        <c:set var="lastName" value="${staff.getLastName()}"/>
                                        <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                            <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                        </div>
                                    </c:when>
                                    <c:when test="${staff.getAvatar() != null}">
                                        <img class="round" src="${staff.getAvatar()}" alt="avatar" height="40"
                                             width="40">   
                                    </c:when>
                                </c:choose> 

                                <span class="avatar-status-online"></span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="page-profile.html"><i class="me-50" data-feather="user"></i> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="page-account-settings-account.html"><i
                                    class="me-50" data-feather="settings"></i> Settings
                            </a>
                            <a class="dropdown-item" href="page-faq.html"><i class="me-50" data-feather="help-circle"></i> FAQ
                            </a>
                            <a class="dropdown-item" href="logoutController">
                                <i class="me-50" data-feather="power"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd
                                Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small>
                </a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div>
                </a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a
                    class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No
                            results found.</span></div>
                </a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="horizontal-menu-wrapper">
            <div
                class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-dark navbar-shadow menu-border container-xxl"
                role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav">
                <div class="navbar-header" style="margin-bottom:2rem;">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item me-auto"><a class="navbar-brand"  href="dashboard-analytics.html">
                                <span>
                                    <img src="/app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                                </span>
                                <h2 class="brand-text mb-0" style="position: relative; right: 25px;">Hilfsmotor</h2>
                            </a></li>
                        <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                                    class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i></a></li>
                    </ul>
                </div>
                <div class="shadow-bottom"></div>
                <!-- Horizontal menu content-->
                <div class="navbar-container main-menu-content" data-menu="menu-container">
                    <!-- include includes/mixins-->
                    <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                        <li class="dropdown nav-item" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link d-flex align-items-center" href="dashboard-analytics.html" data-bs-toggle="dropdown"><i
                                    data-feather="home"></i><span data-i18n="Dashboards">Dashboards</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="dashboard-analytics.html"
                                                    data-bs-toggle="" data-i18n="Analytics"><i data-feather="activity"></i><span
                                            data-i18n="Analytics">Analytics</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown nav-item" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link d-flex align-items-center" href="#"
                                data-bs-toggle="dropdown"><i data-feather="package"></i><span data-i18n="Apps">Apps</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Invoice"><i data-feather="file-text"></i><span data-i18n="Invoice">Orders</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-invoice-list.html"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Roles &amp; Permission"><i data-feather="shield"></i><span
                                            data-i18n="Roles &amp; Permission">Roles &amp; Permission</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-access-roles.html"
                                                            data-bs-toggle="" data-i18n="Roles"><i data-feather="circle"></i><span
                                                    data-i18n="Roles">Roles</span></a>
                                        </li>
                                        <li class="active" data-menu=""><a class="dropdown-item d-flex align-items-center"
                                                                           href="app-access-permission.html" data-bs-toggle="" data-i18n="Permission"><i
                                                    data-feather="circle"></i><span data-i18n="Permission">Permission</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Store"><i data-feather='shopping-bag'></i><span
                                            data-i18n="Store">Store</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="storeListAdmin"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="eCommerce"><i data-feather='shopping-bag'></i><span
                                            data-i18n="eCommerce">Product</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-ecommerce-list.html"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="User"><i data-feather="user"></i><span data-i18n="User">User</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-user-list.html"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                                class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                                data-i18n="View"><i data-feather="circle"></i><span data-i18n="View">View</span></a>
                                            <ul class="dropdown-menu" data-bs-popper="none">
                                                <li data-menu=""><a class="dropdown-item d-flex align-items-center"
                                                                    href="app-user-view-account.html" data-bs-toggle="" data-i18n="Account"><i
                                                            data-feather="circle"></i><span data-i18n="Account">Account</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <!-- users list start -->
                    <section class="app-user-list">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body d-flex align-items-center justify-content-between">
                                        <div>
                                            <h3 class="fw-bolder mb-75">${numstore}</h3>
                                            <span>Total Store</span>
                                        </div>
                                        <div class="avatar bg-light-primary p-50">
                                            <span class="avatar-content">
                                                <i data-feather='shopping-bag' class="font-medium-4"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body d-flex align-items-center justify-content-between">
                                        <div>
                                            <h3 class="fw-bolder mb-75">${numavailable}</h3>
                                            <span>Total Available </span>
                                        </div>
                                        <div class="avatar bg-light-success p-50">
                                            <span class="avatar-content">
                                                <i data-feather='shopping-bag' class="font-medium-4"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body d-flex align-items-center justify-content-between">
                                        <div>
                                            <h3 class="fw-bolder mb-75">${numunavailable}</h3>
                                            <span>Total Unavailable</span>
                                        </div>
                                        <div class="avatar bg-light-warning p-50">
                                            <span class="avatar-content">
                                                <i data-feather='shopping-bag' class="font-medium-4"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body d-flex align-items-center justify-content-between">
                                        <div>
                                            <h3 class="fw-bolder mb-75">${numband}</h3>
                                            <span>Total Band</span>
                                        </div>
                                        <div class="avatar bg-light-danger p-50">
                                            <span class="avatar-content">
                                                <i data-feather='shopping-bag' class="font-medium-4"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- list and filter start -->
                        <div class="card">

                            <div class="card-datatable table-responsive pt-0">
                                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                    <div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-50 mb-1">
                                        <div class="col-sm-12 col-md-4 col-lg-6">
                                            <div class="col-sm-12 col-lg-4 d-flex justify-content-center justify-content-lg-start">
                                                <div class="dataTables_length lidget-list-select" id="DataTables_Table_0_length"><label>Show <select onchange="selectNumerPage(this)"
                                                                                                                                                     name="DataTables_Table_0_length" id="selectAmount" aria-controls="DataTables_Table_0" class="form-select">


                                                            <option ${sessionScope.selectAmountStore.equals("5")?"selected":""} href ="storeListController?amount=5" value="5">5</option>

                                                            <option ${sessionScope.selectAmountStore.equals("10")?"selected":""} href ="storeListController?amount=10" value="10">10</option>
                                                            <option ${sessionScope.selectAmountStore.equals("15")?"selected":""} href ="storeListController?amount=15" value="15">15</option>
                                                            <option ${sessionScope.selectAmountStore.equals("20")?"selected":""} href ="storeListController?amount=20" value="20">20</option>
                                                        </select> entries</label></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-8 col-lg-6 ps-xl-75 ps-0">
                                            <div
                                                class="dt-action-buttons d-flex align-items-center justify-content-md-end justify-content-center flex-sm-nowrap flex-wrap">
                                                <div class="me-1">
                                                    <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input onkeyup="searchByName(this);" id="searchtext" name="searchtext" type="search"
                                                                                                                                       class="form-control" placeholder="" aria-controls="DataTables_Table_0"></label></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <table class="user-list-table table dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid"
                                           aria-describedby="DataTables_Table_0_info" style="width: 1428px;">
                                        <thead class="table-light">
                                            <tr role="row">
                                                <th class="control sorting_disabled" rowspan="1" colspan="1" style="width: 0px; display: none;"
                                                    aria-label=""></th>

                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 320px;" aria-label="Name: activate to sort column ascending">Store Name</th>
                                                <th class="sorting sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                                    colspan="1" style="width: 158px;" aria-label="Role: activate to sort column ascending"
                                                    aria-sort="descending">Owner</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 116px;" aria-label="Mobile: activate to sort column ascending">Mobile
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 150px;" aria-label="State: activate to sort column ascending">State</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 102px;" aria-label="Status: activate to sort column ascending">City</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 102px;" aria-label="Status: activate to sort column ascending">Reported</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 102px;" aria-label="Status: activate to sort column ascending">Status</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 102px;" aria-label="Status: activate to sort column ascending">Actions</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="listnew">
                                            <c:forEach var="item" items="${list}">


                                                <tr class="odd">
                                                    <td class=" control" tabindex="0" style="display: none;"></td>

                                                    <td class="">
                                                        <div class="d-flex justify-content-left align-items-center">
                                                            <div class="avatar-wrapper">
                                                                <c:set var="firstName" value="${item.getFirstName()}"/>
                                                                <c:set var="lastName" value="${item.getLastName()}"/>
                                                                <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                                                    <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                                </div>
                                                            </div>
                                                            &nbsp <div class="d-flex flex-column"><a href="user-store?storeID=${item.getStoreID()}"
                                                                                                     class="user_name text-body text-truncate"><span class="fw-bolder">${item.getStoreName()}</span></a><small class="emp_post text-muted">${item.getEmail()}</small></div>
                                                        </div>
                                                    </td>
                                                    <td class="sorting_1"><span class="text-truncate align-middle"><svg
                                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                class="feather feather-user font-medium-3 text-primary me-50">
                                                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                                            <circle cx="12" cy="7" r="4"></circle>
                                                            </svg>${item.getLastName()} ${item.getFirstName()}</span></td>
                                                    <td>${item.getPhoneNumber()}</td>
                                                    <td><span class="text-nowrap">${item.getState()}</span></td>

                                                    <td>${item.getCity()}</td>
                                                    <td>${item.getStoreID()}</td>
                                                    <c:choose>
                                                        <c:when test="${item.getStatus() == 1}">
                                                            <td><span class="badge rounded-pill badge-light-success" text-capitalized="">Available</span></td>
                                                        </c:when>
                                                        <c:when test="${item.getStatus() == 2}">
                                                            <td><span class="badge rounded-pill badge-light-warning" text-capitalized="">Band</span>
                                                            </td>                                                        
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td><span class="badge rounded-pill badge-light-secondary" text-capitalized="">Unavailable</span>
                                                            </td>                                                        
                                                        </c:otherwise>
                                                    </c:choose> 
                                                    <td>
                                                        <div class="btn-group">
                                                            <a class="btn btn-sm dropdown-toggle hide-arrow"
                                                               data-bs-toggle="dropdown">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                     stroke-linecap="round" stroke-linejoin="round"
                                                                     class="feather feather-more-vertical font-small-4">
                                                                <circle cx="12" cy="12" r="1"></circle>
                                                                <circle cx="12" cy="5" r="1"></circle>
                                                                <circle cx="12" cy="19" r="1"></circle>
                                                                </svg></a>
                                                            <div  href="javascript:;" class="dropdown-menu dropdown-menu-end">
                                                                

                                                                <c:choose>
                                                                    <c:when test="${item.getStatus() == 1 }">
                                                                        <a href="changeStoreStatus?mx=${item.storeID}&status=band" href="javascript:;" class="dropdown-item delete-record"><svg
                                                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                stroke-linejoin="round" class="feather feather-trash-2 font-small-4 me-50">
                                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                                            <path
                                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                                            </path>
                                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                            </svg>Band
                                                                        </a>
                                                                    </c:when>
                                                                    <c:when test="${item.getStatus() == 2 }">
                                                                        <a href="changeStoreStatus?mx=${item.storeID}&status=disable" class="dropdown-item delete-record"><svg
                                                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                stroke-linejoin="round" class="feather feather-trash-2 font-small-4 me-50">
                                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                                            <path
                                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                                            </path>
                                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                            </svg>Unavailable
                                                                        </a>                                                        
                                                                    </c:when>
                                                                    <c:when test="${item.getStatus() == 3}">
                                                                        <a href="changeStoreStatus?mx=${item.storeID}&status=available" class="dropdown-item delete-record"><svg
                                                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                stroke-linejoin="round" class="feather feather-trash-2 font-small-4 me-50">
                                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                                            <path
                                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                                            </path>
                                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                            </svg>Available
                                                                        </a>                                                     
                                                                    </c:when>
                                                                </c:choose>
                                                                <c:choose>
                                                                    <c:when test="${item.getStatus() == 3 }">
                                                                        <a href="changeStoreStatus?mx=${item.storeID}&status=band" href="javascript:;" class="dropdown-item delete-record"><svg
                                                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                stroke-linejoin="round" class="feather feather-trash-2 font-small-4 me-50">
                                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                                            <path
                                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                                            </path>
                                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                            </svg>Band
                                                                        </a>
                                                                    </c:when>
                                                                    <c:when test="${item.getStatus() == 1 }">
                                                                        <a href="changeStoreStatus?mx=${item.storeID}&status=disable" class="dropdown-item delete-record"><svg
                                                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                stroke-linejoin="round" class="feather feather-trash-2 font-small-4 me-50">
                                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                                            <path
                                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                                            </path>
                                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                            </svg>Unavailable
                                                                        </a>                                                        
                                                                    </c:when>
                                                                    <c:when test="${item.getStatus() == 2}">
                                                                        <a href="changeStoreStatus?mx=${item.storeID}&status=available" class="dropdown-item delete-record"><svg
                                                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                stroke-linejoin="round" class="feather feather-trash-2 font-small-4 me-50">
                                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                                            <path
                                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                                            </path>
                                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                            </svg>Available
                                                                        </a>                                                     
                                                                    </c:when>
                                                                </c:choose>
                                                                

                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </c:forEach>



                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-between mx-2 row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1
                                                to ${sessionScope.selectAmountStore} of ${numstore} entries</div>
                                        </div>

                                        <c:set value ="$page" var = "age">    
                                        </c:set>


                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">


                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous " id="DataTables_Table_0_previous"><a
                                                            href="store-list?page=${page - 1 <= 0 ? 1 : page - 1}" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
                                                            class="page-link">&nbsp;</a></li>
                                                        <c:forEach begin="1" end="${numpage}" var = "item">    
                                                        <li class="paginate_button page-item active"><a href="store-list?page=${item}" aria-controls="DataTables_Table_0"
                                                                                                        data-dt-idx="1" tabindex="0" class="page-link">${item}</a></li>
                                                        </c:forEach>                                            

                                                    <li class="paginate_button page-item next" id="DataTables_Table_0_next"><a href="store-list?page=${page + 1 > numpage ? numpage : page + 1}"
                                                                                                                               aria-controls="DataTables_Table_0" data-dt-idx="6" tabindex="0" class="page-link">&nbsp;</a>
                                                    </li>
                                                </ul>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- list and filter end -->
                    </section>
                    <!-- product list ends -->
                </div>
            </div>
        </div>
        <div id="proID" name="proID" ></div>
        <!-- END: Content-->


        <!-- BEGIN: Customizer-->
        <div class="customizer d-none d-md-block"><a
                class="customizer-toggle d-flex align-items-center justify-content-center" href="#"><i class="spinner"
                                                                                                   data-feather="settings"></i></a>
            <div class="customizer-content">
                <!-- Customizer header -->
                <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                    <h4 class="mb-0">Theme Customizer</h4>
                    <p class="m-0">Customize & Preview in Real Time</p>

                    <a class="customizer-close" href="#"><i data-feather="x"></i></a>
                </div>

                <hr />

                <!-- Styling & Text Direction -->
                <div class="customizer-styling-direction px-2">
                    <p class="fw-bold">Skin</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="skinlight" name="skinradio" class="form-check-input layout-name" checked
                                   data-layout="" />
                            <label class="form-check-label" for="skinlight">Light</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skinbordered" name="skinradio" class="form-check-input layout-name"
                                   data-layout="bordered-layout" />
                            <label class="form-check-label" for="skinbordered">Bordered</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skindark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="dark-layout" />
                            <label class="form-check-label" for="skindark">Dark</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="skinsemidark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="semi-dark-layout" />
                            <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                        </div>
                    </div>
                </div>

                <hr />

                <!-- Menu -->
                <div class="customizer-menu px-2">
                    <div id="customizer-menu-collapsible" class="d-flex">
                        <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                        <div class="form-check form-check-primary form-switch">
                            <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                            <label class="form-check-label" for="collapse-sidebar-switch"></label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Layout Width -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Layout Width</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                            <label class="form-check-label" for="layout-width-full">Full Width</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Navbar -->
                <div class="customizer-navbar px-2">
                    <div id="customizer-navbar-colors">
                        <p class="fw-bold">Navbar Color</p>
                        <ul class="list-inline unstyled-list">
                            <li class="color-box bg-white border selected" data-navbar-default=""></li>
                            <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                            <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                            <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                            <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                            <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                            <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                            <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                        </ul>
                    </div>

                    <p class="navbar-type-text fw-bold">Navbar Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                            <label class="form-check-label" for="nav-type-floating">Floating</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-static">Static</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Footer -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Footer Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                            <label class="form-check-label" for="footer-type-static">Static</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End: Customizer-->


    </div>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>




    <!-- BEGIN: Vendor JS-->
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="app-assets/vendors/js/file-uploaders/dropzone.min.js"></script>

    <script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
    <script src="app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.min.js"></script>
    <script src="app-assets/js/core/app.min.js"></script>
    <script src="app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: MY JS-->
    <script src="assets/js/main.js"></script>
    <!-- END: MY JS-->
    <script id="auth-shop" data-value="${fn:length(numberPaginationList)}" src="assets/js/auth_shopping.js"></script>
    <!-- BEGIN: Page JS-->
    <script src="app-assets/js/scripts/forms/form-file-uploader.min.js"></script>
    <script src="app-assets/js/scripts/app-product-list.min.js"></script>
    <!-- END: Page JS-->

    <script>
                                                        function searchByName(param) {
                                                            var txtSearch = param.value;

                                                            $.ajax({
                                                                url: "/Rentabike/searchProduct",
                                                                type: "GET", //send it through get method
                                                                data: {
                                                                    txtSearch: txtSearch
                                                                },
                                                                success: function (data) {
                                                                    var row = document.getElementById("listnew");
                                                                    row.innerHTML = data;
                                                                },
                                                                error: function (xhr) {
                                                                    //Do Something to handle error
                                                                }
                                                            });
                                                        }
                                                        $(window).on('load', function () {
                                                            if (feather) {
                                                                feather.replace({width: 14, height: 14});
                                                            }
                                                        });

                                                        function selectNumerPage(param) {
                                                            var num = param.value;
                                                            window.location.href = "store-list?amount=" + num

                                                        }

    </script>

</body>
<!-- END: Body-->

</html>