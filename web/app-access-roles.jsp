<%-- 
    Document   : app-access-roles
    Created on : Oct 10, 2022, 1:27:50 PM
    Author     : ADMIN
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-validation.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover"
          data-menu="horizontal-menu" data-col="">

        <!-- BEGIN: Header-->
        <nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center"
             data-nav="brand-center">
            <div class="navbar-header d-xl-block d-none">
                <ul class="nav navbar-nav">
                    <li class="nav-item">
                        <a class="navbar-brand" href="dashboard-analytics.html">
                            <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                            <h2 class="brand-text mb-0 lidget-app__padding-left">Hilfsmotor</h2>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                     stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu ficon">
                                <line x1="3" y1="12" x2="21" y2="12"></line>
                                <line x1="3" y1="6" x2="21" y2="6"></line>
                                <line x1="3" y1="18" x2="21" y2="18"></line>
                                </svg></a></li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block">
                            <a class="nav-link" href="app-calendar.html" data-bs-toggle="tooltip"
                               data-bs-placement="bottom" title="" data-bs-original-title="Calendar" aria-label="Calendar"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none"
                                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-calendar ficon">
                                <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                <line x1="16" y1="2" x2="16" y2="6"></line>
                                <line x1="8" y1="2" x2="8" y2="6"></line>
                                <line x1="3" y1="10" x2="21" y2="10"></line>
                                </svg></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none"
                                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-star ficon text-warning">
                                <polygon
                                    points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2">
                                </polygon>
                                </svg></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-search">
                                    <circle cx="11" cy="11" r="8"></circle>
                                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                    </svg></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark ps">
                                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                    </div>
                                    <div class="ps__rail-y" style="top: 0px; right: 0px;">
                                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                    </div>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language">
                        <a class="nav-link dropdown-toggle" id="dropdown-flag" href="#"
                           data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="flag-icon flag-icon-us"></i>
                            <span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"
                                                                                                        data-language="en"><i class="flag-icon flag-icon-us"></i> English</a>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style">
                            <i class="ficon"
                               data-feather="sun"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search">
                            <i class="ficon"
                               data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Hilfsmotor..." tabindex="-1"
                                   data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>

                    <li class="nav-item dropdown dropdown-notification me-25">
                        <a class="nav-link" href="#"
                           data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span
                                class="badge rounded-pill bg-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-15.jpg"
                                                                     alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small
                                                class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar"
                                                                     width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small
                                                class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small
                                                class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="list-item d-flex align-items-center">
                                    <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                    <div class="form-check form-check-primary form-switch">
                                        <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="form-check-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small
                                                class="notification-text"> USA Server is down due to hight CPU usage</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small
                                                class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small
                                                class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle dropdown-user-link"
                           id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none">
                                <span class="user-name fw-bolder" data-attr="${staff.getAccountID()}">${staff.getUserName()}</span>
                                <span class="user-status" data="${staff.getPermissionDetails()}">${staff.getRole()}</span></div>
                            <span class="avatar">
                                <c:choose>
                                    <c:when test="${staff.getAvatar() == null}">
                                        <c:set var="firstName" value="${staff.getFirstName()}"/>
                                        <c:set var="lastName" value="${staff.getLastName()}"/>
                                        <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                            <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                        </div>
                                    </c:when>
                                    <c:when test="${staff.getAvatar() != null}">
                                        <img class="round" src="${staff.getAvatar()}" alt="avatar" height="40"
                                             width="40">   
                                    </c:when>
                                </c:choose> 
                                <span class="avatar-status-online"></span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="page-profile.jsp"><i class="me-50" data-feather="user"></i> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="page-account-settings-account.jsp"><i
                                    class="me-50" data-feather="settings"></i> Settings
                            </a>
                            <a class="dropdown-item" href="page-faq.jsp"><i class="me-50" data-feather="help-circle"></i> FAQ
                            </a>
                            <a class="dropdown-item" href="logoutController">
                                <i class="me-50" data-feather="power"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd
                                Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small>
                </a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div>
                </a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a
                    class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No
                            results found.</span></div>
                </a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="horizontal-menu-wrapper">
            <div
                class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-dark navbar-shadow menu-border container-xxl"
                role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav">
                <div class="navbar-header" style="margin-bottom:2rem;">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item me-auto"><a class="navbar-brand"
                                                        href="html/ltr/horizontal-menu-template-dark/dashboard-analytics.html">
                                <span>
                                    <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                                </span>
                                <h2 class="brand-text mb-0" style="position: relative; right: 25px;">Hilfsmotor</h2>
                            </a></li>
                        <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                                    class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i></a></li>
                    </ul>
                </div>
                <div class="shadow-bottom"></div>
                <!-- Horizontal menu content-->
                <div class="navbar-container main-menu-content" data-menu="menu-container">
                    <!-- include includes/mixins-->
                    <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                        <li class="dropdown nav-item" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link d-flex align-items-center" href="dashboard-analytics.html" data-bs-toggle="dropdown"><i
                                    data-feather="home"></i><span data-i18n="Dashboards">Dashboards</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="dashboard-analytics.html"
                                                    data-bs-toggle="" data-i18n="Analytics"><i data-feather="activity"></i><span
                                            data-i18n="Analytics">Analytics</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown nav-item" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link d-flex align-items-center" href="#"
                                data-bs-toggle="dropdown"><i data-feather="package"></i><span data-i18n="Apps">Apps</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Invoice"><i data-feather="file-text"></i><span data-i18n="Invoice">Orders</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-invoice-list.html"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Roles &amp; Permission"><i data-feather="shield"></i><span
                                            data-i18n="Roles &amp; Permission">Roles &amp; Permission</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li class="active" data-menu="">
                                            <a class="dropdown-item d-flex align-items-center"
                                               href="#/" data-bs-toggle="" data-i18n="Roles">
                                                <i data-feather="circle"></i>
                                                <span data-i18n="Roles">Roles</span></a>
                                        </li>
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="permissionListController"
                                                            data-bs-toggle="" data-i18n="Permission"><i data-feather="circle"></i><span
                                                    data-i18n="Permission">Permission</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="eCommerce"><i data-feather='shopping-bag'></i><span
                                            data-i18n="eCommerce">Product</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-ecommerce-list.html"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="User"><i data-feather="user"></i><span data-i18n="User">User</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="userListController"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                                class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                                data-i18n="View"><i data-feather="circle"></i><span data-i18n="View">View</span></a>
                                            <ul class="dropdown-menu" data-bs-popper="none">
                                                <li data-menu=""><a class="dropdown-item d-flex align-items-center"
                                                                    href="app-user-view-account.html" data-bs-toggle="" data-i18n="Account"><i
                                                            data-feather="circle"></i><span data-i18n="Account">Account</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <h3>Roles List</h3>
                    <p class="mb-2">
                        A role provided access to predefined menus and features. <br />
                        Note that onlyAdmin Pagecan move or change role.
                    </p>

                    <!-- Role cards -->
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                        <span>Total ${totalAdmin} users</span>
                                        <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                            <c:forEach items="${sessionScope.staffAccountList}" begin="0" end="9" var="admin">
                                                <c:if test="${admin.getRole() == 'ADMIN'}">
                                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                                        title="${admin.getFirstName()} ${admin.getLastName()}" class="avatar avatar-sm pull-up">
                                                        <c:choose>
                                                            <c:when test="${admin.getAvatar() != null}">
                                                                <img class="rounded-circle" src="${admin.getAvatar()}" alt="Avatar" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="firstName" value="${admin.getFirstName()}"/>
                                                                <c:set var="lastName" value="${admin.getLastName()}"/>
                                                                <div class="avatar bg-light-warning me-50" style="margin-right:0 !important">
                                                                    <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                                </div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </li> 
                                                </c:if>      
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-end mt-1 pt-25">
                                        <div class="role-heading">
                                            <h4 class="fw-bolder">Administrator</h4>
                                        </div>
                                        <a href="javascript:void(0);" class="text-body"><i data-feather="copy" class="font-medium-5"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                        <span>Total ${totalStaff} users</span>
                                        <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                            <c:forEach items="${sessionScope.staffAccountList}" begin="0" end="9" var="staff">
                                                <c:if test="${staff.getRole() == 'STAFF'}">
                                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                                        title="${staff.getFirstName()} ${staff.getLastName()}" class="avatar avatar-sm pull-up">
                                                        <c:choose>
                                                            <c:when test="${staff.getAvatar() != null}">
                                                                <img class="rounded-circle" src="${staff.getAvatar()}" alt="Avatar" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="firstName" value="${staff.getFirstName()}"/>
                                                                <c:set var="lastName" value="${staff.getLastName()}"/>
                                                                <div class="avatar bg-light-warning me-50" style="margin-right:0 !important">
                                                                    <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                                </div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </li> 
                                                </c:if>      
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-end mt-1 pt-25">
                                        <div class="role-heading">
                                            <h4 class="fw-bolder">Staffs</h4>
                                        </div>
                                        <a href="javascript:void(0);" class="text-body"><i data-feather="copy" class="font-medium-5"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                        <span>Total ${totalCustomer} users</span>
                                        <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                            <c:forEach items="${sessionScope.userAccountList}" begin="0" end="9" var="user">
                                                <c:if test="${user.getRole() == 'CUSTOMER'}">
                                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                                        title="${user.getFirstName()} ${user.getLastName()}" class="avatar avatar-sm pull-up">
                                                        <c:choose>
                                                            <c:when test="${user.getAvatar() != null}">
                                                                <img class="rounded-circle" src="${user.getAvatar()}" alt="Avatar" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="firstName" value="${user.getFirstName()}"/>
                                                                <c:set var="lastName" value="${user.getLastName()}"/>
                                                                <div class="avatar bg-light-warning me-50" style="margin-right:0 !important">
                                                                    <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                                </div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </li> 
                                                </c:if>      
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-end mt-1 pt-25">
                                        <div class="role-heading">
                                            <h4 class="fw-bolder">Users</h4>
                                        </div>
                                        <a href="javascript:void(0);" class="text-body"><i data-feather="copy" class="font-medium-5"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                        <span>Total ${totalSupport} users</span>
                                        <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                            <c:forEach items="${sessionScope.staffAccountList}" begin="0" end="9" var="support">
                                                <c:if test="${support.getRole() == 'SUPPORT'}">
                                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                                        title="${support.getFirstName()} ${support.getLastName()}" class="avatar avatar-sm pull-up">
                                                        <c:choose>
                                                            <c:when test="${support.getAvatar() != null}">
                                                                <img class="rounded-circle" src="${support.getAvatar()}" alt="Avatar" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="firstName" value="${support.getFirstName()}"/>
                                                                <c:set var="lastName" value="${support.getLastName()}"/>
                                                                <div class="avatar bg-light-warning me-50" style="margin-right:0 !important">
                                                                    <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                                </div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </li> 
                                                </c:if>      
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-end mt-1 pt-25">
                                        <div class="role-heading">
                                            <h4 class="fw-bolder">Support</h4>
                                        </div>
                                        <a href="javascript:void(0);" class="text-body"><i data-feather="copy" class="font-medium-5"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                        <span>Total ${totalSaleperson} users</span>
                                        <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                            <c:forEach items="${sessionScope.userAccountList}" begin="0" end="9" var="user">
                                                <c:if test="${user.getRole() == 'SALEPERSON'}">
                                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                                        title="${user.getFirstName()} ${user.getLastName()}" class="avatar avatar-sm pull-up">
                                                        <c:choose>
                                                            <c:when test="${user.getAvatar() != null}">
                                                                <img class="rounded-circle" src="${user.getAvatar()}" alt="Avatar" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="firstName" value="${user.getFirstName()}"/>
                                                                <c:set var="lastName" value="${user.getLastName()}"/>
                                                                <div class="avatar bg-light-warning me-50" style="margin-right:0 !important">
                                                                    <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                                </div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </li> 
                                                </c:if>      
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-end mt-1 pt-25">
                                        <div class="role-heading">
                                            <h4 class="fw-bolder">Saleperson</h4>
                                        </div>
                                        <a href="javascript:void(0);" class="text-body"><i data-feather="copy" class="font-medium-5"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                        <span>Total ${totalBannedUser} users</span>
                                        <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                            <c:forEach items="${sessionScope.staffAccountList}" begin="0" end="5" var="user">
                                                <c:if test="${user.getActive() == 2}">
                                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                                        title="${user.getFirstName()} ${user.getLastName()}" class="avatar avatar-sm pull-up">
                                                        <c:choose>
                                                            <c:when test="${user.getAvatar() != null}">
                                                                <img class="rounded-circle" src="${user.getAvatar()}" alt="Avatar" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="firstName" value="${user.getFirstName()}"/>
                                                                <c:set var="lastName" value="${user.getLastName()}"/>
                                                                <div class="avatar bg-light-warning me-50" style="margin-right:0 !important">
                                                                    <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                                </div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </li> 
                                                </c:if>      
                                            </c:forEach>
                                            <c:forEach items="${sessionScope.userAccountList}" begin="0" end="5" var="user">
                                                <c:if test="${user.getStatus() == 0}">
                                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                                        title="${user.getFirstName()} ${user.getLastName()}" class="avatar avatar-sm pull-up">
                                                        <c:choose>
                                                            <c:when test="${user.getAvatar() != null}">
                                                                <img class="rounded-circle" src="${user.getAvatar()}" alt="Avatar" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="firstName" value="${user.getFirstName()}"/>
                                                                <c:set var="lastName" value="${user.getLastName()}"/>
                                                                <div class="avatar bg-light-warning me-50" style="margin-right:0 !important">
                                                                    <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                                </div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </li> 
                                                </c:if>      
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-end mt-1 pt-25">
                                        <div class="role-heading">
                                            <h4 class="fw-bolder">Suspended User</h4>
                                        </div>
                                        <a href="javascript:void(0);" class="text-body"><i data-feather="copy" class="font-medium-5"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Role cards -->
                    <h3 class="mt-50">Total users with their roles</h3>
                    <p class="mb-2">Find all of your company’s administrator accounts and their associate roles.</p>
                    <!-- table -->
                    <div class="card">
                        <div class="table-responsive">
                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                <div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-50 mb-1">
                                    <div class="col-sm-12 col-md-4 col-lg-6">
                                        <div class="dataTables_length" id="DataTables_Table_0_length">
                                            <label>Show 
                                                <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" id="showEntry" class="form-select">
                                                    <c:forEach items="${sessionScope.showEntryList}" var="entry">
                                                        <c:choose>
                                                            <c:when test="${showEntry == entry}">
                                                                <option value="${entry}" selected="">${entry}</option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="${entry}">${entry}</option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </select> entries
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-8 col-lg-6 ps-xl-75 ps-0">
                                        <div
                                            class="dt-action-buttons d-flex align-items-center justify-content-md-end justify-content-center flex-sm-nowrap flex-wrap">
                                            <div class="me-1">
                                                <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:
                                                        <input type="search"
                                                               class="form-control" id="searchUser" placeholder="Search username..." aria-controls="DataTables_Table_0"></label></div>
                                            </div>
                                            <div class="user_role mt-50 width-200"><select id="UserRole" class="form-select text-capitalize">
                                                    <option value=""> Select Role </option>
                                                    <option value="ADMIN" class="text-capitalize">Admin</option>
                                                    <option value="STAFF" class="text-capitalize">Staff</option>
                                                    <option value="SALEPERSON" class="text-capitalize">Saleperson</option>
                                                    <option value="CUSTOMER" class="text-capitalize">Customer</option>
                                                    <option value="SUPPORT" class="text-capitalize">Support</option>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                                <table class="user-list-table table dataTable no-footer dtr-column" id="tableUserRole" id="DataTables_Table_0" role="grid"
                                       aria-describedby="DataTables_Table_0_info" style="width: 1428px;">
                                    <thead class="table-light">
                                        <tr role="row">
                                            <th class="control sorting_disabled" rowspan="1" colspan="1" style="width: 0px; display: none;"
                                                aria-label=""></th>
                                            <th class="sorting ds-s non-sorting" id="sorting-name" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                style="width: 361px;" aria-label="Name: activate to sort column ascending">Name</th>
                                            <th class="sorting ds-s non-sorting" id="sorting-role" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                                colspan="1" style="width: 158px;" aria-label="Role: activate to sort column ascending"
                                                aria-sort="descending">Role</th>
                                            <th class="sorting ds-s non-sorting" id="sorting-date-join" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                style="width: 116px;" aria-label="Date of Join: activate to sort column ascending">Date of Join
                                            </th>
                                            <th class="sorting ds-s non-sorting" id="sorting-permisson" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                style="width: 223px;" aria-label="Permission: activate to sort column ascending">Permission Details</th>
                                            <th class="sorting ds-s non-sorting" id="sorting-status" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                style="width: 102px;" aria-label="Status: activate to sort column ascending">Status</th>
                                            <th class="sorting_disabled" id="sorting-name" rowspan="1" colspan="1" style="width: 104px;" aria-label="Actions">
                                                Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="lidget__list-user">
                                        <c:forEach items="${sessionScope.staffAccountList}" begin="0" end="${endStaff}" var="staffList">
                                            <tr>
                                                <td class=" control" tabindex="0" style="display: none;"></td>
                                                <td class="">
                                                    <div class="d-flex justify-content-left align-items-center">
                                                        <c:choose>
                                                            <c:when test="${staffList.getAvatar() == null}">
                                                                <c:set var="firstName" value="${staffList.getFirstName()}"/>
                                                                <c:set var="lastName" value="${staffList.getLastName()}"/>
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar bg-light-warning me-1">
                                                                        <span class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</span>
                                                                    </div>
                                                                </div>
                                                            </c:when>
                                                            <c:when test="${staffList.getAvatar() != null}">
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar  me-1">
                                                                        <img src="${staffList.getAvatar()}" alt="Avatar"
                                                                             height="32" width="32">
                                                                    </div>
                                                                </div>
                                                            </c:when>
                                                        </c:choose> 
                                                        <div class="d-flex flex-column">
                                                            <a href="#/" index="${staffList.getAccountID()}" onclick="sendParamterView(this)"  data-bs-toggle="modal" data-bs-target="#defaultSize"
                                                               class="user_name text-body text-truncate">
                                                                <span class="fw-bolder">${staffList.getFirstName()} ${staffList.getLastName()}</span>
                                                            </a>
                                                            <small class="emp_post text-muted">${staffList.getEmail()}</small>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td class="sorting_1">
                                                    <c:choose>
                                                        <c:when test="${staffList.getRole() == 'ADMIN'}">
                                                            <span class="text-truncate align-middle">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                     class="feather feather-slack font-medium-3 text-danger me-50">
                                                                <path
                                                                    d="M14.5 10c-.83 0-1.5-.67-1.5-1.5v-5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5z">
                                                                </path>
                                                                <path d="M20.5 10H19V8.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z"></path>
                                                                <path
                                                                    d="M9.5 14c.83 0 1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5S8 21.33 8 20.5v-5c0-.83.67-1.5 1.5-1.5z">
                                                                </path>
                                                                <path d="M3.5 14H5v1.5c0 .83-.67 1.5-1.5 1.5S2 16.33 2 15.5 2.67 14 3.5 14z"></path>
                                                                <path
                                                                    d="M14 14.5c0-.83.67-1.5 1.5-1.5h5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-5c-.83 0-1.5-.67-1.5-1.5z">
                                                                </path>
                                                                <path d="M15.5 19H14v1.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5z"></path>
                                                                <path
                                                                    d="M10 9.5C10 8.67 9.33 8 8.5 8h-5C2.67 8 2 8.67 2 9.5S2.67 11 3.5 11h5c.83 0 1.5-.67 1.5-1.5z">
                                                                </path>
                                                                <path d="M8.5 5H10V3.5C10 2.67 9.33 2 8.5 2S7 2.67 7 3.5 7.67 5 8.5 5z"></path>
                                                                </svg>                                                                    
                                                                <span class="roleSort">Admin</span>
                                                            </span>
                                                        </c:when>
                                                        <c:when test="${staffList.getRole() == 'STAFF'}">
                                                            <span class="text-truncate align-middle">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                     class="feather feather-settings font-medium-3 text-warning me-50">
                                                                <circle cx="12" cy="12" r="3"></circle>
                                                                <path
                                                                    d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                                                </path>
                                                                </svg>
                                                                <span class="roleSort">Staff</span>
                                                            </span>
                                                        </c:when>
                                                    </c:choose>
                                                </td>
                                                <td>${staffList.getDateOfJoin()}</td>
                                                <td><span class="text-nowrap">${staffList.getPermissionDetails()}</span></td>
                                                <td>
                                                    <c:choose>
                                                        <c:when test="${staffList.getActive() == 0}">
                                                            <span class="badge rounded-pill badge-light-primary" text-capitalized="">Inactive</span>
                                                        </c:when>
                                                        <c:when test="${staffList.getActive() == 1}">
                                                            <span class="badge rounded-pill badge-light-success" text-capitalized="">Active</span>
                                                        </c:when>
                                                        <c:when test="${staffList.getActive() == 2}">
                                                            <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Banned</span>
                                                        </c:when>
                                                    </c:choose>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center col-actions">
                                                        <div class="btn-group">
                                                            <a class="btn btn-sm dropdown-toggle hide-arrow waves-effect waves-float waves-light" data-bs-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical font-small-4">
                                                                <circle cx="12" cy="12" r="1"></circle>
                                                                <circle cx="12" cy="5" r="1"></circle>
                                                                <circle cx="12" cy="19" r="1"></circle>
                                                                </svg>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-end">
                                                                <a href="javascript:;" data-title="edit-role-account" index="${staffList.getAccountID()}" current-action="${staffList.getRole()}" onclick="sendParamterHandler(this)" data-bs-target="#addRoleModal"
                                                                   class="dropdown-item role-edit-modal">
                                                                    <i data-feather='user-plus'></i><span style="margin-left:0.25rem">Update Role</span>
                                                                </a>
                                                                <a class="dropdown-item">
                                                                    <c:choose>
                                                                        <c:when test="${staffList.getActive() == 2}">
                                                                            <button style="padding:0;" class="btn btn-sm btn-icon waves-effect waves-float waves-light flex-default flex-center-align" data-title="unlock-account" onclick="sendParamterHandler(this)" current-action="${staffList.getRole()}" index="${staffList.getAccountID()}" data-bs-toggle="modal" data-bs-target="#unlockUser">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                                                                <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                                                <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                                                </svg>
                                                                                <span style="margin-left:0.25rem;color:#B4B7BD;">Unlocked</span>
                                                                            </button> 
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <button style="padding:0;" class="btn btn-sm btn-icon waves-effect waves-float waves-light flex-default flex-center-align" data-title="suspended-account" onclick="sendParamterHandler(this)" current-action="${staffList.getRole()}" index="${staffList.getAccountID()}" data-bs-target="#suspendedUser">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                                                                <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                                                <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                                                </svg>
                                                                                <span style="margin-left:0.25rem;color:#B4B7BD;">Suspended</span>
                                                                            </button> 
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        <c:if test="${endUser >= 0}">
                                            <c:forEach items="${sessionScope.userAccountList}" begin="0" end="${endUser}" var="userList">
                                                <tr>
                                                    <td class=" control" tabindex="0" style="display: none;"></td>
                                                    <td class="">
                                                        <div class="d-flex justify-content-left align-items-center">
                                                            <c:choose>
                                                                <c:when test="${userList.getAvatar() == null}">
                                                                    <c:set var="firstName" value="${userList.getFirstName()}"/>
                                                                    <c:set var="lastName" value="${userList.getLastName()}"/>
                                                                    <div class="avatar-wrapper">
                                                                        <div class="avatar bg-light-warning me-1">
                                                                            <span class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</span>
                                                                        </div>
                                                                    </div>
                                                                </c:when>
                                                                <c:when test="${userList.getAvatar() != null}">
                                                                    <div class="avatar-wrapper">
                                                                        <div class="avatar me-1">
                                                                            <img src="${userList.getAvatar()}" alt="Avatar"
                                                                                 height="32" width="32">
                                                                        </div>
                                                                    </div>
                                                                </c:when>
                                                            </c:choose> 
                                                            <div class="d-flex flex-column">
                                                                <a href="#/" data-bs-toggle="modal" data-bs-target="#defaultSize" index="${userList.getAccountID()}" onclick="sendParamterView(this)" 
                                                                   class="user_name text-body text-truncate">
                                                                    <span class="fw-bolder">${userList.getFirstName()} ${userList.getLastName()}</span>
                                                                </a>
                                                                <small class="emp_post text-muted">${userList.getEmail()}</small>
                                                            </div>
                                                        </div>
                                                    </td>

                                                    <td class="sorting_1">
                                                        <c:choose>
                                                            <c:when test="${userList.getRole() == 'CUSTOMER'}">
                                                                <span class="text-truncate align-middle">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                         class="feather feather-user font-medium-3 text-primary me-50">
                                                                    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                                                    <circle cx="12" cy="7" r="4"></circle>
                                                                    </svg>         
                                                                    <span class="roleSort">Customer</span>
                                                                </span>
                                                            </c:when>
                                                            <c:when test="${userList.getRole() == 'SALEPERSON'}">
                                                                <i data-feather='truck' style="
                                                                   width: 16.8px;
                                                                   height: 16.8px;
                                                                   margin-right: 7px;
                                                                   ">
                                                                </i>
                                                                <span class="roleSort">Saleperson</span>
                                                            </td>
                                                        </c:when>
                                                    </c:choose>
                                                    </td>
                                                    <td>${userList.getDateOfJoin()}</td>
                                                    <td><span class="text-nowrap">${userList.getPermissionDetails()}</span></td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${userList.getStatus() == 2}">
                                                                <span class="badge rounded-pill badge-light-primary" text-capitalized="">
                                                                    Inactive
                                                                </span>
                                                            </c:when>
                                                            <c:when test="${userList.getStatus() == 1}">
                                                                <span class="badge rounded-pill badge-light-success" text-capitalized="">Active</span>
                                                            </c:when>
                                                            <c:when test="${userList.getStatus() == 0}">
                                                                <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Banned</span>
                                                            </c:when>
                                                        </c:choose>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex align-items-center col-actions">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm dropdown-toggle hide-arrow waves-effect waves-float waves-light" data-bs-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical font-small-4">
                                                                    <circle cx="12" cy="12" r="1"></circle>
                                                                    <circle cx="12" cy="5" r="1"></circle>
                                                                    <circle cx="12" cy="19" r="1"></circle>
                                                                    </svg>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-end">
                                                                    <a href="javascript:;" data-bs-toggle="modal" data-title="edit-role-account" index="${userList.getAccountID()}" current-action="${userList.getRole()}" onclick="sendParamterHandler(this)" data-bs-target="#addRoleModal"
                                                                       class="dropdown-item role-edit-modal">
                                                                        <i data-feather='user-plus'></i><span style="margin-left:0.25rem">Update Role</span>
                                                                    </a>
                                                                    <a class="dropdown-item">
                                                                        <c:choose>
                                                                            <c:when test="${userList.getStatus() == 0}">
                                                                                <button style="padding:0;" class="btn btn-sm btn-icon waves-effect waves-float waves-light flex-default flex-center-align" current-action="${userList.getRole()}" data-title="unlock-account" onclick="sendParamterHandler(this)" index="${userList.getAccountID()}" data-bs-toggle="modal" data-bs-target="#unlockUser">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                                                                    <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                                                    <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                                                    </svg>
                                                                                    <span style="margin-left:0.25rem;color:#B4B7BD;">Unlocked</span>
                                                                                </button> 
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <button style="padding:0;" class="btn btn-sm btn-icon waves-effect waves-float waves-light flex-default flex-center-align" current-action="${userList.getRole()}" data-title="suspended-account" onclick="sendParamterHandler(this)" index="${userList.getAccountID()}" data-bs-toggle="modal" data-bs-target="#suspendedUser">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                                                                    <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                                                    <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                                                    </svg>
                                                                                    <span style="margin-left:0.25rem;color:#B4B7BD;">Suspended</span>
                                                                                </button> 
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </c:forEach>                                                
                                        </c:if>
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-between mx-2 row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="dataTables_info totalResult" id="totalInfo" style="transition: 0.6s" role="status" aria-live="polite">Showing ${limitStart}
                                            to ${limitEnd} of ${totalAccount} entries</div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                            <ul class="pagination" id="paginationNumber" currentIndex="${currentPage}" style="transition: 0.6s;">
                                                <c:choose>
                                                    <c:when test="${currentPage == 1}">
                                                        <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous">
                                                            <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
                                                               class="page-link">&nbsp;
                                                            </a>
                                                        </li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li class="paginate_button page-item previous" id="DataTables_Table_0_previous">
                                                            <a href="roleListController?page=${currentPage - 1}" data-dt-idx="0" tabindex="0"
                                                               class="page-link">&nbsp;
                                                            </a>
                                                        </li>
                                                    </c:otherwise>
                                                </c:choose>
                                                <c:forEach items="${sessionScope.numberPaginationList}" var="numberPage">
                                                    <c:set var="index" value="${numberPage}"/>
                                                    <c:choose>
                                                        <c:when test="${currentPage == index}">
                                                            <li class="paginate_button page-item active">
                                                                <a href="#/" aria-controls="DataTables_Table_0" data-dt-idx="${numberPage}" tabindex="0" class="page-link">${numberPage}</a>
                                                            </li>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <li class="paginate_button page-item">
                                                                <a href="roleListController?page=${numberPage}" aria-controls="DataTables_Table_0" data-dt-idx="${numberPage}" tabindex="0" class="page-link">${numberPage}</a>
                                                            </li>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                                <c:set var="totalPage" value="${fn:length(numberPaginationList)}"/>
                                                <c:choose>
                                                    <c:when test="${currentPage + 1 > totalPage}">
                                                        <li class="paginate_button page-item next disabled" id="DataTables_Table_0_next">
                                                            <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="6" 
                                                               tabindex="0" class="page-link">&nbsp;
                                                            </a>
                                                        </li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li class="paginate_button page-item next" id="DataTables_Table_0_next">
                                                            <a href="roleListController?page=${currentPage + 1}" aria-controls="DataTables_Table_0" data-dt-idx="6" 
                                                               tabindex="0" class="page-link">&nbsp;
                                                            </a>
                                                        </li>
                                                    </c:otherwise>
                                                </c:choose>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- table -->

                </div>
            </div>
        </div>
        <!-- END: Content-->

        <!-- BEGIN: EDIT ROLE -->
        <div class="modal fade show" id="addRoleModal" tabindex="-1" style="display: none;" aria-modal="true" role="dialog">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-add-new-role lidget-modal__role">
                <div class="modal-content">
                    <div class="modal-header bg-transparent">
                        <button type="button" class="btn-close" id="update-permission-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body px-5 pb-5">
                        <div class="text-center mb-4">
                            <h1 id="role-data" class="role-title">Edit Role</h1>
                            <p id="role-set">Set role permissions</p>
                        </div>
                        <!-- Add role form -->
                        <form id="addRoleForm" class="row" novalidate="novalidate">
                            <div class="col-12">
                                <h4 class="pt-50">Role Permissions</h4>
                                <!-- Permission table -->
                                <div class="table-responsive">
                                    <table class="table table-flush-spacing">
                                        <tbody>
                                            <c:forEach items="${sessionScope.permissionList}" var="role">
                                                <c:choose>
                                                    <c:when test="${role.getName() == 'Administrator'}">
                                                        <tr>
                                                            <td class="text-nowrap fw-bolder">
                                                                Administrator
                                                                <span data-bs-toggle="tooltip" data-bs-placement="top" title=""
                                                                      data-bs-original-title="Allows a full access to the system">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none"
                                                                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                         class="feather feather-info">
                                                                    <circle cx="12" cy="12" r="10"></circle>
                                                                    <line x1="12" y1="16" x2="12" y2="12"></line>
                                                                    <line x1="12" y1="8" x2="12.01" y2="8"></line>
                                                                    </svg>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <div class="form-check">
                                                                    <input class="form-check-input lidget__role-permission" name="optionRole" data-attr="${role.getName()}" type="radio" id="selectRole">
                                                                    <label class="form-check-label"  for="selectRole"> Select All </label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <tr>
                                                            <td class="text-nowrap fw-bolder">${role.getName()}</td>
                                                            <td>
                                                                <div class="d-flex">
                                                                    <div class="form-check me-3 me-lg-5">
                                                                        <input class="form-check-input lidget__role-permission" name="optionRole" data-attr="${role.getName()}" type="radio" id="${role.getName()}">
                                                                        <label class="form-check-label" for="${role.getName()}">Select</label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- Permission table -->
                            </div>
                            <div class="col-12 text-center mt-2">
                                <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary waves-effect" data-bs-dismiss="modal"
                                        aria-label="Close">
                                    Discard
                                </button>
                            </div>
                        </form>
                        <!--/ Add role form -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END: EDIT ROLE -->

        <!-- BEGIN: Customizer-->
        <div class="customizer d-none d-md-block">
            <a class="customizer-toggle d-flex align-items-center justify-content-center" href="#"><i class="spinner"
                                                                                                      data-feather="settings"></i>
            </a>
            <div class="customizer-content">
                <!-- Customizer header -->
                <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                    <h4 class="mb-0">Theme Customizer</h4>
                    <p class="m-0">Customize & Preview in Real Time</p>

                    <a class="customizer-close" href="#"><i data-feather="x"></i></a>
                </div>

                <hr />

                <!-- Styling & Text Direction -->
                <div class="customizer-styling-direction px-2">
                    <p class="fw-bold">Skin</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="skinlight" name="skinradio" class="form-check-input layout-name" checked
                                   data-layout="" />
                            <label class="form-check-label" for="skinlight">Light</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skinbordered" name="skinradio" class="form-check-input layout-name"
                                   data-layout="bordered-layout" />
                            <label class="form-check-label" for="skinbordered">Bordered</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skindark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="dark-layout" />
                            <label class="form-check-label" for="skindark">Dark</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="skinsemidark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="semi-dark-layout" />
                            <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                        </div>
                    </div>
                </div>

                <hr />

                <!-- Menu -->
                <div class="customizer-menu px-2">
                    <div id="customizer-menu-collapsible" class="d-flex">
                        <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                        <div class="form-check form-check-primary form-switch">
                            <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                            <label class="form-check-label" for="collapse-sidebar-switch"></label>
                        </div>
                    </div>
                </div>
                <hr />

                <div class="modal fade show" id="suspendedUser" tabindex="-1" aria-modal="true" role="dialog"
                     style="display: none; padding-left: 0px;">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header bg-transparent">
                                <button type="button" class="btn-close" id="suspended-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body p-3 pt-0">
                                <div class="text-center mb-2">
                                    <h1 class="mb-1">Suspended User</h1>
                                    <div class="d-flex flex-column">
                                        <a class="user_name text-body text-truncate">
                                            <span class="fw-bolder" id="name-suspended"></span>
                                        </a>
                                        <small class="emp_post text-muted" id="email-suspended"></small>
                                        <small class="emp_post text-muted" style="margin-top:0.5rem;" id="role-suspended"></small>
                                    </div>
                                </div>

                                <div class="alert alert-warning" role="alert">
                                    <h6 class="alert-heading">Warning!</h6>
                                    <div class="alert-body">
                                        By banned this user, all of process continue with this user will be hidden until unbanned. Please ensure
                                        you're absolutely certain before proceeding.
                                        After proceeding, a message will be send to user's email address.
                                    </div>
                                </div>

                                <form id="bannedUpdatePermission" class="row" onsubmit="return false">
                                    <div class="col-sm-9">
                                        <label class="form-label" for="bannedMessage">Message</label>
                                        <input type="text" id="bannedMessage" name="bannedPermission" class="form-control"
                                               placeholder="Enter a message banned" tabindex="-1" data-msg="Please enter message banned">
                                    </div>
                                    <div class="col-sm-3 ps-sm-0">
                                        <button type="submit" class="btn btn-danger mt-2 waves-effect waves-float waves-light">Suspended</button>
                                    </div>
                                    <div class="col-12 mt-75">
                                        <label class="form-label" for="FilterTransaction">Duration</label><select
                                            id="DurationSelect" class="form-select text-capitalize mb-md-0 mb-2xx">
                                            <option value="1 Day" class="text-capitalize">1 days</option>
                                            <option value="3 Days" class="text-capitalize">3 days</option>
                                            <option value="7 Days" class="text-capitalize">7 days</option>
                                            <option value="14 Days" class="text-capitalize">14 days</option>
                                            <option value="21 Days" class="text-capitalize">21 days</option>
                                            <option value="28 Days" class="text-capitalize">28 days</option>
                                            <option value="365 Days" class="text-capitalize">1 year</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade show" id="unlockUser" tabindex="-1" aria-modal="true" role="dialog"
                     style="display: none; padding-left: 0px;">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header bg-transparent">
                                <button type="button" class="btn-close" id="unlock-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body p-3 pt-0">
                                <div class="text-center mb-2">
                                    <h1 class="mb-1">Unlocked User</h1>
                                    <div class="d-flex flex-column">
                                        <a class="user_name text-body text-truncate">
                                            <span class="fw-bolder" id="name-unlock"></span>
                                        </a>
                                        <small class="emp_post text-muted" id="email-unlock"></small>
                                        <small class="emp_post text-muted" style="margin-top:0.5rem;" id="role-unlock"></small>
                                    </div>
                                </div>

                                <div class="alert alert-success" role="alert">
                                    <h6 class="alert-heading">Before you do this!</h6>
                                    <div class="alert-body">
                                        By unlocked this user, all of process continue with this user will be visible. Please ensure
                                        you're absolutely certain before proceeding.
                                        After proceeding, a message will be send to user's email address.
                                    </div>
                                </div>

                                <form id="unlockUpdatePermission" class="row" onsubmit="return false">
                                    <div class="col-sm-9">
                                        <label class="form-label" for="unlockMessage">Message</label>
                                        <input type="text" id="unlockMessage" name="unlockPermission" class="form-control"
                                               placeholder="Enter a message banned" tabindex="-1" data-msg="Please enter message...">
                                    </div>
                                    <div class="col-sm-3 ps-sm-0">
                                        <button type="submit" class="btn btn-success mt-2 waves-effect waves-float waves-light">Unlocked</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Layout Width -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Layout Width</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                            <label class="form-check-label" for="layout-width-full">Full Width</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Navbar -->
                <div class="customizer-navbar px-2">
                    <div id="customizer-navbar-colors">
                        <p class="fw-bold">Navbar Color</p>
                        <ul class="list-inline unstyled-list">
                            <li class="color-box bg-white border selected" data-navbar-default=""></li>
                            <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                            <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                            <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                            <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                            <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                            <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                            <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                        </ul>
                    </div>

                    <p class="navbar-type-text fw-bold">Navbar Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                            <label class="form-check-label" for="nav-type-floating">Floating</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-static">Static</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Footer -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Footer Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                            <label class="form-check-label" for="footer-type-static">Static</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal to show profile Begin-->
        <div class="modal fade text-start" id="defaultSize" tabindex="-1" aria-labelledby="myModalLabel18" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="titleDetails"></h4>
                        <button type="button" class="btn-close"
                                data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <tbody>
                                <tr data-dt-row="undefined" data-dt-column="1">
                                    <td>#:</td>
                                    <td><a class="fw-bold" id="userModalID" href="#/"> </a></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="3">
                                    <td>Info:</td>
                                    <td>
                                        <div class="d-flex justify-content-left align-items-center">
                                            <div class="avatar-wrapper">
                                                <div class="avatar me-50">
                                                    <img id="accoutAvatar" style="display: none;" src="app-assets/images/avatars/1-small.png" alt="Avatar"
                                                         width="32" height="32"></div>
                                            </div>
                                            <div class="avatar-wrapper">
                                                <div class="avatar bg-light-warning me-1">
                                                    <span class="avatar-content" id="accountAvatarAlt"></span>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column">
                                                <h6 class="user-name text-truncate mb-0" id="userModalFullName"></h6><small
                                                    class="text-truncate text-muted" id="userModalEmail"></small>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="4">
                                    <td>Status</td>
                                    <td id="userModalStatus">
                                    </td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="5">
                                    <td>Date of Join:</td>
                                    <td id="userModalJoin"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="6">
                                    <td>Contact:</td>
                                    <td id="userModalPhone"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="7">
                                    <td>City:</td>
                                    <td id="userModalCity"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="8">
                                    <td>Street:</td>
                                    <td id="userModalStreet"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="9">
                                    <td>State:</td>
                                    <td id="userModalState"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="10">
                                    <td>Role:</td>
                                    <td id="userModalRole">
                                        <span class="text-truncate align-middle">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-slack font-medium-3 text-danger me-50">
                                            <path
                                                d="M14.5 10c-.83 0-1.5-.67-1.5-1.5v-5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5z">
                                            </path>
                                            <path d="M20.5 10H19V8.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z"></path>
                                            <path
                                                d="M9.5 14c.83 0 1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5S8 21.33 8 20.5v-5c0-.83.67-1.5 1.5-1.5z">
                                            </path>
                                            <path d="M3.5 14H5v1.5c0 .83-.67 1.5-1.5 1.5S2 16.33 2 15.5 2.67 14 3.5 14z"></path>
                                            <path
                                                d="M14 14.5c0-.83.67-1.5 1.5-1.5h5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-5c-.83 0-1.5-.67-1.5-1.5z">
                                            </path>
                                            <path d="M15.5 19H14v1.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5z"></path>
                                            <path
                                                d="M10 9.5C10 8.67 9.33 8 8.5 8h-5C2.67 8 2 8.67 2 9.5S2.67 11 3.5 11h5c.83 0 1.5-.67 1.5-1.5z">
                                            </path>
                                            <path d="M8.5 5H10V3.5C10 2.67 9.33 2 8.5 2S7 2.67 7 3.5 7.67 5 8.5 5z"></path>
                                            </svg>Admin
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End: Customizer-->
        <div class="modal fade modal-danger text-start" id="danger_modal" tabindex="-1" aria-labelledby="myModalLabel120" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel135">Permission Limited</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body danger_modal-allowed-text">
                        Sorry, but only people with the admin role and staff with user management permission can do this. Try contacting the admins if you want to do this.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect waves-float waves-light lidget-accept-button" id="acceptChanged" data-bs-dismiss="modal">Accept</button>
                        <button type="button" class="btn btn-outline-secondary waves-effect" data-bs-dismiss="modal" id="resetChanged" style="display: none;"
                                aria-label="Close">
                            Discard
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Buynow Button-->

    </div>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Vendor JS-->
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.min.js"></script>
    <script src="app-assets/js/core/app.min.js"></script>
    <script src="app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="app-assets/js/scripts/pages/app-access-roles.min.js"></script>
    <script src="app-assets/js/scripts/modal-update-banned-message.js"></script>
    <!-- END: Page JS-->

    <!-- BEGIN: My JS-->
    <script src="assets/js/admin_role_access.js"></script>
    <!-- END: My JS-->
    <script>
                                    $(window).on('load', function () {
                                        if (feather) {
                                            feather.replace({width: 14, height: 14});
                                        }
                                    })
    </script>
</body>
<!-- END: Body-->

</html>