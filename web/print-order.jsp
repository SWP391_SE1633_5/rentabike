<%-- 
    Document   : print-order
    Created on : Oct 17, 2022, 1:32:38 AM
    Author     : ASUS
--%>

    <!-- BEGIN: Head-->
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/app-invoice.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="horizontal-layout horizontal-menu blank-page navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="blank-page">
    <!-- BEGIN: Content-->
    <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="invoice-preview-wrapper">
                        <div class="row invoice-preview">
                            <!-- Invoice -->
                            <div class="col-xl-9 col-md-8 col-12">
                                <div class="card invoice-preview-card">
                                    <c:set var = "itemNum" scope = "session" value = "${1}"/>
                                    <c:forEach items="${sessionScope.orderItemList}"  var="orderItem">
                                        <hr class="invoice-spacing" />
                                        <div class="card-body invoice-padding pb-0">
                                            <!-- Header starts -->
                                            <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                                                <div>
                                                    <div class="logo-wrapper">
                                                        <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo position-relative"
                                                             style="left:-15px">
                                                        <h3 class="text-primary invoice-logo position-relative" style="right:35px;">${orderItem.storeName}</h3>
                                                    </div>
                                                    <p class="card-text mb-25">Product Name: <a href="productDetailController?mx=${orderItem.p.productID}">${orderItem.p.name}</a></p>
                                                    <p class="card-text mb-25">Quantity: ${orderItem.quantity}</p>
                                                    <p class="card-text mb-25">Mode: ${orderItem.p.mode}</p>
                                                    <c:choose>
                                                        <c:when test="${orderItem.p.mode == 'Rental'}">
                                                            <p class="card-text mb-25">Price: ${orderItem.p.price} per Day</p>
                                                        </c:when>
                                                        <c:when test="${orderItem.p.mode == 'Sell'}">
                                                            <p class="card-text mb-25">Price: ${orderItem.p.price}</p>
                                                        </c:when>
                                                    </c:choose>
                                                    <p class="card-text mb-25">Store Name: <a href="user-store?storeID=${orderItem.storeID}">${orderItem.storeName}</a></p>
                                                    <p class="card-text mb-25">${orderItem.address}</p>
                                                    <p class="card-text mb-0">${orderItem.phoneNum}</p>
                                                </div>
                                                <div class="mt-md-0 mt-2">
                                                    <h4 class="invoice-title">
                                                        Order
                                                        <span class="invoice-number">#${order.orderID}</span>
                                                        <br><br>
                                                        Order Item
                                                        <span class="invoice-number">#${itemNum}</span>
                                                    </h4>

                                                    <div class="invoice-date-wrapper">
                                                        <p class="invoice-date-title">Order Date:</p>
                                                        <p class="invoice-date">${orderItem.orderDate}</p>
                                                    </div>
                                                    <div class="invoice-date-wrapper">
                                                        <p class="invoice-date-title">Shipped Date:</p>
                                                        <p class="invoice-date">${orderItem.shippedDate}</p>
                                                    </div>
                                                    <div class="invoice-date-wrapper">
                                                        <p class="invoice-date-title">Status: </p>
                                                        <c:choose>
                                                            <c:when test="${orderItem.status == 1}">
                                                                <span class="badge rounded-pill badge-light-primary me-1">PROCESSING</span>
                                                            </c:when>
                                                            <c:when test="${orderItem.status == 2}">
                                                                <span class="badge rounded-pill badge-light-info me-1">SHIPPING</span>
                                                            </c:when>
                                                            <c:when test="${orderItem.status == 3}">
                                                                <span class="badge rounded-pill badge-light-warning me-1">STOP</span>                                                            
                                                            </c:when>
                                                            <c:when test="${orderItem.status == 4}">
                                                                <span class="badge rounded-pill badge-light-success me-1">Completed</span>                                                            </c:when>
                                                        </c:choose>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Header ends -->
                                        </div>

                                        <c:set var = "itemNum" scope = "session" value = "${itemNum + 1}"/>
                                    </c:forEach>

                                    <hr class="invoice-spacing" />

                                    <!-- Address and Contact starts -->
                                    <div class="card-body invoice-padding pt-0">
                                        <div class="row invoice-spacing">
                                            <div class="col-xl-8 p-0">
                                                <h6 class="mb-2">Order to:</h6>
                                                <h6 class="mb-25">${order.customerFirstName} ${order.customerLastName}</h6>
                                                <p class="card-text mb-25">${order.customerAddress}</p>
                                                <p class="card-text mb-25">${order.customerPhoneNum}</p>
                                                <p class="card-text mb-0">${order.customerEmail}</p>
                                            </div>
                                            <div class="col-xl-4 p-0 mt-xl-0 mt-2">
                                                <h6 class="mb-2">Payment Details:</h6>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td class="pe-1">Total Due:</td>
                                                            <td><span class="fw-bold">$${order.totalPrice}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pe-1">Bank name:</td>
                                                            <!--<td>American Bank</td>-->
                                                        </tr>
                                                        <tr>
                                                            <td class="pe-1">State:</td>
                                                           <!-- <td>United States</td>-->
                                                        </tr>
                                                        <tr>
                                                            <td class="pe-1">Account:</td>
                                                            <!--<td>ETD95476213874685</td>-->
                                                        </tr>
                                                        <tr>
                                                            <td class="pe-1">ZIP code:</td>
                                                            <!--<td>BR91905</td>-->
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Address and Contact ends -->

                                    <!-- Invoice Description starts -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="py-1">Order</th>
                                                    <th class="py-1">Price</th>
                                                    <th class="py-1">Quantity</th>
                                                    <th class="py-1">Mode</th>
                                                    <th class="py-1">Time</th>
                                                    <th class="py-1">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${sessionScope.orderItemList}"  var="orderItem">
                                                    <tr>
                                                        <td class="py-1">
                                                            <p class="card-text fw-bold mb-25">${orderItem.p.name}</p>
                                                            <p class="card-text text-nowrap">
                                                                ${orderItem.p.modelYear}
                                                            </p>
                                                        </td>
                                                        <td class="py-1">
                                                            <span class="fw-bold">$${orderItem.p.price}</span>
                                                        </td>
                                                        <td class="py-1">
                                                            <span class="fw-bold">${orderItem.quantity}</span>
                                                        </td>
                                                        <td class="py-1">
                                                            <span class="fw-bold">${orderItem.p.mode}</span>
                                                        </td>
                                                        <c:choose>
                                                            <c:when test="${orderItem.p.mode == 'Rental'}">
                                                                <td class="py-1">
                                                                    <span class="fw-bold">${orderItem.rentalDateNum} Day</span>
                                                                </td>
                                                            </c:when>
                                                                <c:when test="${orderItem.p.mode == 'Sell'}">
                                                                <td class="py-1">
                                                                    <span class="fw-bold">UNLIMITED</span>
                                                                </td>
                                                            </c:when>
                                                        </c:choose>

                                                        <td class="py-1">
                                                            <span class="fw-bold">$${orderItem.price}</span>
                                                        </td>
                                                    </tr>

                                                </c:forEach>


                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="card-body invoice-padding pb-0">
                                        <div class="row invoice-sales-total-wrapper">
                                            <div class="col-md-6 order-md-1 order-2 mt-md-0 mt-3">
                                                <p class="card-text mb-0">
                                                    <span class="fw-bold">Salesperson:</span> <span class="ms-75">Hilfsmotor</span>
                                                </p>
                                            </div>
                                            <div class="col-md-6 d-flex justify-content-end order-md-2 order-1">
                                                <div class="invoice-total-wrapper">
                                                    <div class="invoice-total-item">
                                                        <p class="invoice-total-title">Subtotal:</p>
                                                        <p class="invoice-total-amount">$${order.totalPrice}</p>
                                                    </div>
                                                    <div class="invoice-total-item">
                                                        <p class="invoice-total-title">Discount:</p>
                                                        <p class="invoice-total-amount">${order.discount}%</p>
                                                    </div>
                                                    <div class="invoice-total-item">
                                                        <p class="invoice-total-title">Tax:</p>
                                                        <p class="invoice-total-amount">${order.taxes}%</p>
                                                    </div>
                                                    <hr class="my-50" />
                                                    <div class="invoice-total-item">
                                                        <p class="invoice-total-title">Total:</p>
                                                        <p class="invoice-total-amount">$${order.totalPrice*(100 - order.discount)/100*(100 + order.taxes)/100}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Invoice Description ends -->

                                    <hr class="invoice-spacing" />

                                    <!-- Invoice Note starts -->
                                    <div class="card-body invoice-padding pt-0">
                                        <div class="row">
                                            <div class="col-12">
                                                <span class="fw-bold">Note:</span>
                                                <span>It was a pleasure working with you and your team. We hope you will keep us in mind for future freelance projects. Thank You!</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Invoice Note ends -->
                                </div>
                            </div>
                            <!-- /Invoice -->

                            
                            <!-- /Invoice Actions -->
                        </div>
                    </section>

                    <!-- Send Invoice Sidebar -->
                    <div class="modal modal-slide-in fade" id="send-invoice-sidebar" aria-hidden="true">
                        <div class="modal-dialog sidebar-lg">
                            <div class="modal-content p-0">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                                <div class="modal-header mb-1">
                                    <h5 class="modal-title">
                                        <span class="align-middle">Send Invoice</span>
                                    </h5>
                                </div>
                                <div class="modal-body flex-grow-1">
                                    <form>
                                        <div class="mb-1">
                                            <label for="invoice-from" class="form-label">From</label>
                                            <input type="text" class="form-control" id="invoice-from" value="shelbyComapny@email.com"
                                                   placeholder="company@email.com" />
                                        </div>
                                        <div class="mb-1">
                                            <label for="invoice-to" class="form-label">To</label>
                                            <input type="text" class="form-control" id="invoice-to" value="qConsolidated@email.com"
                                                   placeholder="company@email.com" />
                                        </div>
                                        <div class="mb-1">
                                            <label for="invoice-subject" class="form-label">Subject</label>
                                            <input type="text" class="form-control" id="invoice-subject"
                                                   value="Invoice of purchased Admin Templates" placeholder="Invoice regarding goods" />
                                        </div>
                                        <div class="mb-1">
                                            <label for="invoice-message" class="form-label">Message</label>
                                            <textarea class="form-control" name="invoice-message" id="invoice-message" cols="3" rows="11"
                                                      placeholder="Message...">
                                                Dear Queen Consolidated,

                                                Thank you for your business, always a pleasure to work with you!

                                                We have generated a new invoice in the amount of $95.59

                                                We would appreciate payment of this invoice by 05/11/2019</textarea>
                                        </div>
                                        <div class="mb-1">
                                            <span class="badge badge-light-primary">
                                                <i data-feather="link" class="me-25"></i>
                                                <span class="align-middle">Invoice Attached</span>
                                            </span>
                                        </div>
                                        <div class="mb-1 d-flex flex-wrap mt-2">
                                            <button type="button" class="btn btn-primary me-1" data-bs-dismiss="modal">Send</button>
                                            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Send Invoice Sidebar -->

                    <!-- Add Payment Sidebar -->
                    <div class="modal modal-slide-in fade" id="add-payment-sidebar" aria-hidden="true">
                        <div class="modal-dialog sidebar-lg">
                            <div class="modal-content p-0">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                                <div class="modal-header mb-1">
                                    <h5 class="modal-title">
                                        <span class="align-middle">Add Payment</span>
                                    </h5>
                                </div>
                                <div class="modal-body flex-grow-1">
                                    <form>
                                        <div class="mb-1">
                                            <input id="balance" class="form-control" type="text" value="Invoice Balance: 5000.00" disabled />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="amount">Payment Amount</label>
                                            <input id="amount" class="form-control" type="number" placeholder="$1000" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="payment-date">Payment Date</label>
                                            <input id="payment-date" class="form-control date-picker" type="text" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="payment-method">Payment Method</label>
                                            <select class="form-select" id="payment-method">
                                                <option value="" selected disabled>Select payment method</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Bank Transfer">Bank Transfer</option>
                                                <option value="Debit">Debit</option>
                                                <option value="Credit">Credit</option>
                                                <option value="Paypal">Paypal</option>
                                            </select>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="payment-note">Internal Payment Note</label>
                                            <textarea class="form-control" id="payment-note" rows="5"
                                                      placeholder="Internal Payment Note"></textarea>
                                        </div>
                                        <div class="d-flex flex-wrap mb-0">
                                            <button type="button" class="btn btn-primary me-1" data-bs-dismiss="modal">Send</button>
                                            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Add Payment Sidebar -->

                </div>
            </div>
        </div>     
        
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->
<script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/forms/wizard/bs-stepper.min.js"></script>
    <script src="app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
    <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.min.js"></script>
    <script src="app-assets/js/core/app.min.js"></script>
    <script src="app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="app-assets/js/scripts/pages/app-ecommerce-checkout.min.js"></script>
    <script src="assets/js/notify_manager.js"></script>
    <!-- END: Page JS-->
  <script src="app-assets/js/scripts/pages/app-invoice-print.min.js"></script>
    <script>
                                            $(window).on('load', function () {
                                                if (feather) {
                                                    feather.replace({width: 14, height: 14});
                                                }
                                                $.ajax({
                                                    url: "/Rentabike/notifyController",
                                                    type: "post",
                                                    data: {
                                                        mode: "showNotifyOnly"
                                                    },
                                                    success: function (response) {
                                                        $("#notify").html(response);
                                                    }
                                                });
                                            })
    </script>
</body>
</html>
