<%-- 
    Document   : app-user-view-account
    Created on : Sep 29, 2022, 8:25:05 PM
    Author     : ADMIN
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/animate/animate.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-validation.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->
    <body class="vertical-layout vertical-menu-modern  navbar-floating footer-static   menu-collapsed" data-open="click" data-menu="vertical-menu-modern" data-col="">

        <!-- BEGIN: Header-->
        <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Contact"><i class="ficon" data-feather="mail"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Help"><i class="ficon" data-feather="help-circle"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning"
                                                                                                    data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="#"
                                                                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"
                                                                                                        data-language="en"><i class="flag-icon flag-icon-us"></i> English</a>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon"
                                                                                                 data-feather="moon"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon"
                                                                                           data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Hilfsmotor..." tabindex="-1"
                                   data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-cart me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i
                                class="ficon" data-feather="shopping-cart"></i><span
                                class="badge rounded-pill bg-primary badge-up cart-item-count">6</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">My Cart</h4>
                                    <div class="badge rounded-pill badge-light-primary">4 Items</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list">
                                <div class="list-item align-items-center"><img class="d-block rounded me-1"
                                                                               src="app-assets/images/pages/eCommerce/1.png" alt="donuts" width="62">
                                    <div class="list-item-body flex-grow-1">
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> Honda MX-219
                                                    2019</a>
                                            </h6>
                                            <div class="flex-default" style="column-gap:0.5rem">
                                                <div class="text-body badge rounded-pill badge-light-primary">Honda</div>
                                                <div class="text-body badge rounded-pill badge-light-primary">Chopper Version</div>
                                                <div class="text-body badge rounded-pill badge-light-primary">Rental Option</div>
                                            </div>
                                            <div class="flex-default" style="column-gap:0.5rem">
                                                <div class="text-body badge rounded-pill badge-light-success mt-1">Quantity: 1</div>
                                                <div class="text-body badge rounded-pill badge-light-danger mt-1">Tourist: None</div>
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$374.90</h5>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-menu-footer">
                                <div class="d-flex justify-content-between mb-1">
                                    <h6 class="fw-bolder mb-0">Total:</h6>
                                    <h6 class="text-primary fw-bolder mb-0">$10,999.00</h6>
                                </div><a class="btn btn-primary w-100" href="app-ecommerce-checkout.html">Checkout</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#"
                                                                                 data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span
                                class="badge rounded-pill bg-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-15.jpg"
                                                                     alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small
                                                class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar"
                                                                     width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small
                                                class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small
                                                class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="list-item d-flex align-items-center">
                                    <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                    <div class="form-check form-check-primary form-switch">
                                        <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="form-check-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small
                                                class="notification-text"> USA Server is down due to high CPU usage</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small
                                                class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small
                                                class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle dropdown-user-link"
                           id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none lidget-naz" current-login="${user.getEmail()}">
                                <span class="user-name fw-bolder">${user.getFirstName()} ${user.getLastName()}</span>
                                <span class="user-status" style="margin-top:0.25rem">${user.getRole()}</span></div>
                            <span class="avatar">
                                <c:choose>
                                    <c:when test="${user.getAvatar() == null}">
                                        <c:set var="firstName" value="${user.getFirstName()}"/>
                                        <c:set var="lastName" value="${user.getLastName()}"/>
                                        <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                            <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                        </div>
                                    </c:when>
                                    <c:when test="${user.getAvatar() != null}">
                                        <img class="round" src="${user.getAvatar()}" alt="avatar" height="40"
                                             width="40">   
                                    </c:when>
                                </c:choose> 
                                <span class="avatar-status-online"></span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="page-profile.jsp"><i class="me-50" data-feather="user"></i> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="page-account-settings-account.jsp"><i
                                    class="me-50" data-feather="settings"></i> Settings
                            </a>
                            <a class="dropdown-item" href="page-faq.jsp"><i class="me-50" data-feather="help-circle"></i> FAQ
                            </a>
                            <a class="dropdown-item" href="logoutController">
                                <i class="me-50" data-feather="power"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small></a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                        </div>
                    </div></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing Manager</small>
                        </div>
                    </div></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div></a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No results found.</span></div></a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item me-auto">
                        <a class="navbar-brand position-relative" style="top:-25px"
                           href="productListController">
                            <span>
                                <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo position-relative"
                                     style="left:-18px">
                            </span>
                            <h2 class="brand-text mb-0" style="position: relative; right: 35px;">Hilfsmotor</h2>
                        </a>
                    </li>
                    <li class="nav-item nav-toggle lidget-position-control">
                        <a class="nav-link modern-nav-toggle pe-0"
                           data-bs-toggle="collapse">
                            <i class="d-block d-xl-none text-primary toggle-icon font-medium-4"
                               data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary"
                               data-feather="disc" data-ticon="disc"></i></a></li>
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" navigation-header">
                        <span data-i18n="Apps &amp; Pages">Apps &amp; Pages
                        </span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="app-email.html"><i data-feather="mail"></i><span class="menu-title text-truncate" data-i18n="Email">Email</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="shopping-cart"></i><span
                                class="menu-title text-truncate" data-i18n="eCommerce">Product</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="productListController"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Shop">Shop</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="store-list"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="store-list">Store</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="wishListController">
                                    <i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Wish List">Wish
                                        List</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="app-ecommerce-checkout.html"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Checkout">Checkout</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="user"></i><span class="menu-title text-truncate" data-i18n="User">User</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="app-user-list.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">List</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="View">View</span></a>
                                <ul class="menu-content">
                                    <li class="active"><a class="d-flex align-items-center" href="userProfileController"><span class="menu-item text-truncate" data-i18n="Account">Account</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-security.html"><span class="menu-item text-truncate" data-i18n="Security">Security</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-billing.html"><span class="menu-item text-truncate" data-i18n="Billing &amp; Plans">Billing &amp; Plans</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-notifications.html"><span class="menu-item text-truncate" data-i18n="Notifications">Notifications</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-connections.html"><span class="menu-item text-truncate" data-i18n="Connections">Connections</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Pages">Pages</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="help-page"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="FAQ">FAQ</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Blog">Blog</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="blog-list"><span
                                                class="menu-item text-truncate" data-i18n="List">List</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="manage-blog"><span
                                                class="menu-item text-truncate" data-i18n="Detail">Add</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="blog-draft"><span
                                                class="menu-item text-truncate" data-i18n="Detail">Draft</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="blog-save"><span
                                                class="menu-item text-truncate" data-i18n="Edit">Saved</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" navigation-header"><span data-i18n="Misc">Misc</span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="menu"></i><span class="menu-title text-truncate" data-i18n="Menu Levels">Menu Levels</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.1</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.2</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate" data-i18n="Third Level">Third Level 3.1</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate" data-i18n="Third Level">Third Level 3.2</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="app-user-view-account">
                        <div class="row">
                            <!-- User Sidebar -->
                            <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
                                <!-- User Card -->
                                <div class="card">
                                    <div class="card-body">
                                        <div class="user-avatar-section">
                                            <div class="d-flex align-items-center flex-column">
                                                <c:choose>
                                                    <c:when test="${user.getAvatar() == null}">
                                                        <c:set var="firstName" value="${user.getFirstName()}"/>
                                                        <c:set var="lastName" value="${user.getLastName()}"/>
                                                        <div class="avatar bg-light-danger img-fluid rounded me-50 mb-2" id="noneBackgroundAvatar" style="margin-right:0 !important">
                                                            <div class="avatar-content" style="width:110px;height:110px;font-size:2rem;">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                        </div>
                                                        <img class="img-fluid rounded mt-3 mb-2" id="userAvatar" style="width:110px;height:110px;display:none;" src="${user.getAvatar()}" alt="avatar" height="40"
                                                             width="40">
                                                    </c:when>
                                                    <c:when test="${user.getAvatar() != null}">
                                                        <img class="img-fluid rounded mt-3 mb-2" id="userAvatar" style="width:110px;height:110px;" src="${user.getAvatar()}" alt="avatar" height="40"
                                                             width="40">   
                                                    </c:when>
                                                </c:choose> 
                                                <div class="user-info text-center">
                                                    <h4 id="fullnameE2">${user.getFirstName()} ${user.getLastName()}</h4>
                                                    <span class="badge bg-light-secondary">${user.getRole()}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-around my-2 pt-75">
                                            <div class="d-flex align-items-start me-2">
                                                <span class="badge bg-light-primary p-75 rounded">
                                                    <i data-feather="check" class="font-medium-2"></i>
                                                </span>
                                                <div class="ms-75">
                                                    <h4 class="mb-0">0</h4>
                                                    <small>Orders Completed</small>
                                                </div>
                                            </div>
                                            <div class="d-flex align-items-start">
                                                <span class="badge bg-light-primary p-75 rounded">
                                                    <i data-feather="briefcase" class="font-medium-2"></i>
                                                </span>
                                                <div class="ms-75">
                                                    <h4 class="mb-0">0</h4>
                                                    <small>Product Post</small>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="fw-bolder border-bottom pb-50 mb-1">Details</h4>
                                        <div class="info-container">
                                            <ul class="list-unstyled">
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Username:</span>
                                                    <span id="fullname">${user.getFirstName()} ${user.getLastName()}</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Email:</span>
                                                    <span>${user.getEmail()}</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Verify:</span>
                                                    <c:choose>
                                                        <c:when test="${user.getVerify() == 'VERIFY'}">
                                                            <span class="badge bg-light-success">Verify</span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span class="badge bg-light-danger">Not verify</span>

                                                        </c:otherwise>
                                                    </c:choose>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Contact:</span>
                                                    <span id="phoneNumber">${user.getPhoneNumber()}</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">City:</span>
                                                    <span id="city">${user.getCity()}</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Street:</span>
                                                    <span id="street">${user.getStreet()}</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Country:</span>
                                                    <span id="state">${user.getState()}</span>
                                                </li>
                                            </ul>
                                            <div class="d-flex justify-content-center pt-2">
                                                <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#editUser" id="editUserButton" data-bs-toggle="modal">
                                                    Edit
                                                </a>
                                                <c:if test="${user.getRole()== 'CUSTOMER'}">
                                                    <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#registerStore" id="registerStoreButton" data-bs-toggle="modal">
                                                        Register Store
                                                    </a>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /User Card -->
                            </div>
                            <!--/ User Sidebar -->

                            <!-- User Content -->
                            <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
                                <!-- User Pills -->
                                <ul class="nav nav-pills mb-2">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="userProfileController">
                                            <i data-feather="user" class="font-medium-3 me-50"></i>
                                            <span class="fw-bold">Account</span></a
                                        >
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="app-user-view-security.html">
                                            <i data-feather="lock" class="font-medium-3 me-50"></i>
                                            <span class="fw-bold">Security</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="app-user-view-billing.html">
                                            <i data-feather="bookmark" class="font-medium-3 me-50"></i>
                                            <span class="fw-bold">Billing & Plans</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="app-user-view-notifications.html">
                                            <i data-feather="bell" class="font-medium-3 me-50"></i><span class="fw-bold">Notifications</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="app-user-view-connections.html">
                                            <i data-feather="link" class="font-medium-3 me-50"></i><span class="fw-bold">Connections</span>
                                        </a>
                                    </li>
                                </ul>
                                <!--/ User Pills -->

                                <!-- Project table -->
                                <div class="card">
                                    <h4 class="card-header">User's product post list</h4>
                                    <div class="table-responsive">
                                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                            <table class="table datatable-project dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid"
                                                   style="width: 943px;">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting_disabled control" rowspan="1" colspan="1" style="width: 0px; display: none;"></th>
                                                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 376px;">Project</th>
                                                        <th class="text-nowrap sorting_disabled" rowspan="1" colspan="1" style="width: 152px;">Total Task</th>
                                                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Progress</th>
                                                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 108px;">Hours</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="odd">
                                                        <td class=" control" tabindex="0" style="display: none;"></td>
                                                        <td>
                                                            <div class="d-flex justify-content-left align-items-center">
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar me-1"><img src="app-assets/images/icons/drive.png"
                                                                                                  alt="Project Image" width="32" class="rounded-circle"></div>
                                                                </div>
                                                                <div class="d-flex flex-column"><span class="text-truncate fw-bolder">BGC eCommerce App</span><small
                                                                        class="text-muted">React Project</small></div>
                                                            </div>
                                                        </td>
                                                        <td style="">122/240</td>
                                                        <td>
                                                            <div class="d-flex flex-column"><small class="mb-1">60%</small>
                                                                <div class="progress w-100 me-3" style="height: 6px;">
                                                                    <div class="progress-bar bg-info" style="width: 60%" aria-valuenow="60%" aria-valuemin="0"
                                                                         aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="">210:30h</td>
                                                    </tr>
                                                    <tr class="even">
                                                        <td class=" control" tabindex="0" style="display: none;"></td>
                                                        <td>
                                                            <div class="d-flex justify-content-left align-items-center">
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar me-1"><img src="app-assets/images/icons/drive.png"
                                                                                                  alt="Project Image" width="32" class="rounded-circle"></div>
                                                                </div>
                                                                <div class="d-flex flex-column"><span class="text-truncate fw-bolder">Falcon Logo Design</span><small
                                                                        class="text-muted">UI/UX Project</small></div>
                                                            </div>
                                                        </td>
                                                        <td style="">9/50</td>
                                                        <td>
                                                            <div class="d-flex flex-column"><small class="mb-1">15%</small>
                                                                <div class="progress w-100 me-3" style="height: 6px;">
                                                                    <div class="progress-bar bg-danger" style="width: 15%" aria-valuenow="15%" aria-valuemin="0"
                                                                         aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="">89h</td>
                                                    </tr>
                                                    <tr class="odd">
                                                        <td class=" control" tabindex="0" style="display: none;"></td>
                                                        <td>
                                                            <div class="d-flex justify-content-left align-items-center">
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar me-1"><img src="app-assets/images/icons/drive.png"
                                                                                                  alt="Project Image" width="32" class="rounded-circle"></div>
                                                                </div>
                                                                <div class="d-flex flex-column"><span class="text-truncate fw-bolder">Dashboard Design</span><small
                                                                        class="text-muted">Vuejs Project</small></div>
                                                            </div>
                                                        </td>
                                                        <td style="">100/190</td>
                                                        <td>
                                                            <div class="d-flex flex-column"><small class="mb-1">90%</small>
                                                                <div class="progress w-100 me-3" style="height: 6px;">
                                                                    <div class="progress-bar bg-success" style="width: 90%" aria-valuenow="90%" aria-valuemin="0"
                                                                         aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="">129:45h</td>
                                                    </tr>
                                                    <tr class="even">
                                                        <td class=" control" tabindex="0" style="display: none;"></td>
                                                        <td>
                                                            <div class="d-flex justify-content-left align-items-center">
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar me-1"><img src="app-assets/images/icons/drive.png"
                                                                                                  alt="Project Image" width="32" class="rounded-circle"></div>
                                                                </div>
                                                                <div class="d-flex flex-column"><span class="text-truncate fw-bolder">Foodista mobile app</span><small
                                                                        class="text-muted">iPhone Project</small></div>
                                                            </div>
                                                        </td>
                                                        <td style="">12/86</td>
                                                        <td>
                                                            <div class="d-flex flex-column"><small class="mb-1">49%</small>
                                                                <div class="progress w-100 me-3" style="height: 6px;">
                                                                    <div class="progress-bar bg-warning" style="width: 49%" aria-valuenow="49%" aria-valuemin="0"
                                                                         aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="">45h</td>
                                                    </tr>
                                                    <tr class="odd">
                                                        <td class=" control" tabindex="0" style="display: none;"></td>
                                                        <td>
                                                            <div class="d-flex justify-content-left align-items-center">
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar me-1"><img src="app-assets/images/icons/drive.png"
                                                                                                  alt="Project Image" width="32" class="rounded-circle"></div>
                                                                </div>
                                                                <div class="d-flex flex-column"><span class="text-truncate fw-bolder">Dojo React Project</span><small
                                                                        class="text-muted">React Project</small></div>
                                                            </div>
                                                        </td>
                                                        <td style="">234/378</td>
                                                        <td>
                                                            <div class="d-flex flex-column"><small class="mb-1">73%</small>
                                                                <div class="progress w-100 me-3" style="height: 6px;">
                                                                    <div class="progress-bar bg-info" style="width: 73%" aria-valuenow="73%" aria-valuemin="0"
                                                                         aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="">67:10h</td>
                                                    </tr>
                                                    <tr class="even">
                                                        <td class=" control" tabindex="0" style="display: none;"></td>
                                                        <td>
                                                            <div class="d-flex justify-content-left align-items-center">
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar me-1"><img src="app-assets/images/icons/drive.png"
                                                                                                  alt="Project Image" width="32" class="rounded-circle"></div>
                                                                </div>
                                                                <div class="d-flex flex-column"><span class="text-truncate fw-bolder">Crypto Website</span><small
                                                                        class="text-muted">HTML Project</small></div>
                                                            </div>
                                                        </td>
                                                        <td style="">264/537</td>
                                                        <td>
                                                            <div class="d-flex flex-column"><small class="mb-1">81%</small>
                                                                <div class="progress w-100 me-3" style="height: 6px;">
                                                                    <div class="progress-bar bg-success" style="width: 81%" aria-valuenow="81%" aria-valuemin="0"
                                                                         aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="">108:39h</td>
                                                    </tr>
                                                    <tr class="odd">
                                                        <td class=" control" tabindex="0" style="display: none;"></td>
                                                        <td>
                                                            <div class="d-flex justify-content-left align-items-center">
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar me-1"><img src="app-assets/images/icons/drive.png"
                                                                                                  alt="Project Image" width="32" class="rounded-circle"></div>
                                                                </div>
                                                                <div class="d-flex flex-column"><span class="text-truncate fw-bolder">Vue Admin template</span><small
                                                                        class="text-muted">Vuejs Project</small></div>
                                                            </div>
                                                        </td>
                                                        <td style="">214/627</td>
                                                        <td>
                                                            <div class="d-flex flex-column"><small class="mb-1">78%</small>
                                                                <div class="progress w-100 me-3" style="height: 6px;">
                                                                    <div class="progress-bar bg-success" style="width: 78%" aria-valuenow="78%" aria-valuemin="0"
                                                                         aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="">88:19h</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Project table -->

                                <!-- Activity Timeline -->
                                <div class="card">
                                    <h4 class="card-header">User Activity Timeline</h4>
                                    <div class="card-body pt-1">
                                        <ul class="timeline ms-50">
                                            <c:forEach items="${sessionScope.listActivy}" begin="0" end="8" var="activy">
                                                <li class="timeline-item">
                                                    <c:choose>
                                                        <c:when test="${activy.getTitleNotify() == 'Status Login'}">
                                                            <span class="timeline-point timeline-point-indicator"></span>
                                                        </c:when>
                                                        <c:when test="${activy.getTitleNotify() == 'Status Logout'}">
                                                            <span class="timeline-point timeline-point-warning timeline-point-indicator"></span>
                                                        </c:when>
                                                        <c:when test="${activy.getTitleNotify() == 'Add Wishlist'}">
                                                            <span class="timeline-point timeline-point-success timeline-point-indicator"></span>
                                                        </c:when>
                                                        <c:when test="${activy.getTitleNotify() == 'Add to Cart'}">
                                                            <span class="timeline-point timeline-point-success timeline-point-indicator"></span>
                                                        </c:when>
                                                        <c:when test="${activy.getTitleNotify() == 'Remove Product from Checkout'}">
                                                            <span class="timeline-point timeline-point-dark timeline-point-indicator"></span>
                                                        </c:when>
                                                        <c:when test="${activy.getTitleNotify() == 'Remove Wishlist'}">
                                                            <span class="timeline-point timeline-point-warning timeline-point-indicator"></span>
                                                        </c:when>
                                                    </c:choose>
                                                    <div class="timeline-event">
                                                        <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                            <h6>${activy.getTitleNotify()}</h6>
                                                            <span class="timeline-event-time me-1">${activy.getTimeDistance()}</span>
                                                        </div>
                                                        <p>${activy.getDescription()} ${activy.getTimeActivy()}</p>
                                                    </div>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /Activity Timeline -->

                                <!-- Invoice table -->
                                <div class="card">
                                    <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                        <h4 class="card-header">User's Orders List</h4>
                                        <table class="invoice-table table text-nowrap dataTable no-footer dtr-column" id="DataTables_Table_1" role="grid">
                                            <thead>
                                                <tr role="row">
                                                    <th class="control sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                                        style="display: none;" aria-label=": activate to sort column ascending"></th>
                                                    <th class="sorting sorting_desc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                                        style="width: 46px;" aria-label="#ID: activate to sort column ascending" aria-sort="descending">#ID</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                                        style="width: 42px;" aria-label=": activate to sort column ascending"><svg
                                                            xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none"
                                                            stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                            class="feather feather-trending-up">
                                                        <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>
                                                        <polyline points="17 6 23 6 23 12"></polyline>
                                                        </svg></th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                                        style="width: 73px;" aria-label="TOTAL Paid: activate to sort column ascending">TOTAL Paid</th>
                                                    <th class="text-truncate sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                                        style="width: 130px;" aria-label="Issued Date: activate to sort column ascending">Issued Date</th>
                                                    <th class="cell-fit sorting_disabled" rowspan="1" colspan="1" style="width: 80px;" aria-label="Actions">
                                                        Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="odd">
                                                    <td class=" control" tabindex="0" style="display: none;"></td>
                                                    <td class="sorting_1"><a class="fw-bolder" href="app-invoice-preview.html"> #5089</a></td>
                                                    <td><span data-bs-toggle="tooltip" data-bs-html="true" title=""
                                                              data-bs-original-title="<span>Sent<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 05/09/2019</span>"
                                                              aria-label="<span>Sent<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 05/09/2019</span>">
                                                            <div class="avatar avatar-status bg-light-secondary"><span class="avatar-content"><svg
                                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                        class="feather feather-send avatar-icon">
                                                                    <line x1="22" y1="2" x2="11" y2="13"></line>
                                                                    <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                                                    </svg></span></div>
                                                        </span></td>
                                                    <td>$3077</td>
                                                    <td style="">02 May 2019</td>
                                                    <td style="">
                                                        <div class="d-flex align-items-center col-actions"><a class="me-1" href="#" data-bs-toggle="tooltip"
                                                                                                              data-bs-placement="top" title="" data-bs-original-title="Send Mail" aria-label="Send Mail"><svg
                                                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                    class="feather feather-send font-medium-2 text-body">
                                                                <line x1="22" y1="2" x2="11" y2="13"></line>
                                                                <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                                                </svg></a><a class="me-1" href="app-invoice-preview.html" data-bs-toggle="tooltip"
                                                                         data-bs-placement="top" title="" data-bs-original-title="Preview Invoice"
                                                                         aria-label="Preview Invoice"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                              viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                              stroke-linejoin="round" class="feather feather-eye font-medium-2 text-body">
                                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                <circle cx="12" cy="12" r="3"></circle>
                                                                </svg></a><a href="javascript:void(0)" data-bs-toggle="tooltip" data-bs-placement="top" title=""
                                                                         data-bs-original-title="Download" aria-label="Download"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                                         width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                                                                         stroke-linecap="round" stroke-linejoin="round"
                                                                                                                         class="feather feather-download font-medium-2 text-body">
                                                                <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                                <polyline points="7 10 12 15 17 10"></polyline>
                                                                <line x1="12" y1="15" x2="12" y2="3"></line>
                                                                </svg></a></div>
                                                    </td>
                                                </tr>
                                                <tr class="even">
                                                    <td class=" control" tabindex="0" style="display: none;"></td>
                                                    <td class="sorting_1"><a class="fw-bolder" href="app-invoice-preview.html"> #5041</a></td>
                                                    <td><span data-bs-toggle="tooltip" data-bs-html="true" title=""
                                                              data-bs-original-title="<span>Sent<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 11/19/2019</span>"
                                                              aria-label="<span>Sent<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 11/19/2019</span>">
                                                            <div class="avatar avatar-status bg-light-secondary"><span class="avatar-content"><svg
                                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                        class="feather feather-send avatar-icon">
                                                                    <line x1="22" y1="2" x2="11" y2="13"></line>
                                                                    <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                                                    </svg></span></div>
                                                        </span></td>
                                                    <td>$2230</td>
                                                    <td style="">01 Feb 2020</td>
                                                    <td style="">
                                                        <div class="d-flex align-items-center col-actions"><a class="me-1" href="#" data-bs-toggle="tooltip"
                                                                                                              data-bs-placement="top" title="" data-bs-original-title="Send Mail" aria-label="Send Mail"><svg
                                                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                    class="feather feather-send font-medium-2 text-body">
                                                                <line x1="22" y1="2" x2="11" y2="13"></line>
                                                                <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                                                </svg></a><a class="me-1" href="app-invoice-preview.html" data-bs-toggle="tooltip"
                                                                         data-bs-placement="top" title="" data-bs-original-title="Preview Invoice"
                                                                         aria-label="Preview Invoice"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                              viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                              stroke-linejoin="round" class="feather feather-eye font-medium-2 text-body">
                                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                <circle cx="12" cy="12" r="3"></circle>
                                                                </svg></a><a href="javascript:void(0)" data-bs-toggle="tooltip" data-bs-placement="top" title=""
                                                                         data-bs-original-title="Download" aria-label="Download"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                                         width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                                                                         stroke-linecap="round" stroke-linejoin="round"
                                                                                                                         class="feather feather-download font-medium-2 text-body">
                                                                <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                                <polyline points="7 10 12 15 17 10"></polyline>
                                                                <line x1="12" y1="15" x2="12" y2="3"></line>
                                                                </svg></a></div>
                                                    </td>
                                                </tr>
                                                <tr class="odd">
                                                    <td class=" control" tabindex="0" style="display: none;"></td>
                                                    <td class="sorting_1"><a class="fw-bolder" href="app-invoice-preview.html"> #5027</a></td>
                                                    <td><span data-bs-toggle="tooltip" data-bs-html="true" title=""
                                                              data-bs-original-title="<span>Partial Payment<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 09/25/2019</span>"
                                                              aria-label="<span>Partial Payment<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 09/25/2019</span>">
                                                            <div class="avatar avatar-status bg-light-warning"><span class="avatar-content"><svg
                                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                        class="feather feather-pie-chart avatar-icon">
                                                                    <path d="M21.21 15.89A10 10 0 1 1 8 2.83"></path>
                                                                    <path d="M22 12A10 10 0 0 0 12 2v10z"></path>
                                                                    </svg></span></div>
                                                        </span></td>
                                                    <td>$2787</td>
                                                    <td style="">28 Sep 2019</td>
                                                    <td style="">
                                                        <div class="d-flex align-items-center col-actions"><a class="me-1" href="#" data-bs-toggle="tooltip"
                                                                                                              data-bs-placement="top" title="" data-bs-original-title="Send Mail" aria-label="Send Mail"><svg
                                                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                    class="feather feather-send font-medium-2 text-body">
                                                                <line x1="22" y1="2" x2="11" y2="13"></line>
                                                                <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                                                </svg></a><a class="me-1" href="app-invoice-preview.html" data-bs-toggle="tooltip"
                                                                         data-bs-placement="top" title="" data-bs-original-title="Preview Invoice"
                                                                         aria-label="Preview Invoice"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                              viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                              stroke-linejoin="round" class="feather feather-eye font-medium-2 text-body">
                                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                <circle cx="12" cy="12" r="3"></circle>
                                                                </svg></a><a href="javascript:void(0)" data-bs-toggle="tooltip" data-bs-placement="top" title=""
                                                                         data-bs-original-title="Download" aria-label="Download"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                                         width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                                                                         stroke-linecap="round" stroke-linejoin="round"
                                                                                                                         class="feather feather-download font-medium-2 text-body">
                                                                <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                                <polyline points="7 10 12 15 17 10"></polyline>
                                                                <line x1="12" y1="15" x2="12" y2="3"></line>
                                                                </svg></a></div>
                                                    </td>
                                                </tr>
                                                <tr class="even">
                                                    <td class=" control" tabindex="0" style="display: none;"></td>
                                                    <td class="sorting_1"><a class="fw-bolder" href="app-invoice-preview.html"> #5024</a></td>
                                                    <td><span data-bs-toggle="tooltip" data-bs-html="true" title=""
                                                              data-bs-original-title="<span>Partial Payment<br> <span class=&quot;fw-bold&quot;>Balance:</span> -$202<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 08/02/2019</span>"
                                                              aria-label="<span>Partial Payment<br> <span class=&quot;fw-bold&quot;>Balance:</span> -$202<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 08/02/2019</span>">
                                                            <div class="avatar avatar-status bg-light-warning"><span class="avatar-content"><svg
                                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                        class="feather feather-pie-chart avatar-icon">
                                                                    <path d="M21.21 15.89A10 10 0 1 1 8 2.83"></path>
                                                                    <path d="M22 12A10 10 0 0 0 12 2v10z"></path>
                                                                    </svg></span></div>
                                                        </span></td>
                                                    <td>$5285</td>
                                                    <td style="">30 Jun 2019</td>
                                                    <td style="">
                                                        <div class="d-flex align-items-center col-actions"><a class="me-1" href="#" data-bs-toggle="tooltip"
                                                                                                              data-bs-placement="top" title="" data-bs-original-title="Send Mail" aria-label="Send Mail"><svg
                                                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                    class="feather feather-send font-medium-2 text-body">
                                                                <line x1="22" y1="2" x2="11" y2="13"></line>
                                                                <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                                                </svg></a><a class="me-1" href="app-invoice-preview.html" data-bs-toggle="tooltip"
                                                                         data-bs-placement="top" title="" data-bs-original-title="Preview Invoice"
                                                                         aria-label="Preview Invoice"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                              viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                              stroke-linejoin="round" class="feather feather-eye font-medium-2 text-body">
                                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                <circle cx="12" cy="12" r="3"></circle>
                                                                </svg></a><a href="javascript:void(0)" data-bs-toggle="tooltip" data-bs-placement="top" title=""
                                                                         data-bs-original-title="Download" aria-label="Download"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                                         width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                                                                         stroke-linecap="round" stroke-linejoin="round"
                                                                                                                         class="feather feather-download font-medium-2 text-body">
                                                                <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                                <polyline points="7 10 12 15 17 10"></polyline>
                                                                <line x1="12" y1="15" x2="12" y2="3"></line>
                                                                </svg></a></div>
                                                    </td>
                                                </tr>
                                                <tr class="odd">
                                                    <td class=" control" tabindex="0" style="display: none;"></td>
                                                    <td class="sorting_1"><a class="fw-bolder" href="app-invoice-preview.html"> #5020</a></td>
                                                    <td><span data-bs-toggle="tooltip" data-bs-html="true" title=""
                                                              data-bs-original-title="<span>Downloaded<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 12/15/2019</span>"
                                                              aria-label="<span>Downloaded<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 12/15/2019</span>">
                                                            <div class="avatar avatar-status bg-light-info"><span class="avatar-content"><svg
                                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                        class="feather feather-arrow-down-circle avatar-icon">
                                                                    <circle cx="12" cy="12" r="10"></circle>
                                                                    <polyline points="8 12 12 16 16 12"></polyline>
                                                                    <line x1="12" y1="8" x2="12" y2="16"></line>
                                                                    </svg></span></div>
                                                        </span></td>
                                                    <td>$5219</td>
                                                    <td style="">17 Jul 2019</td>
                                                    <td style="">
                                                        <div class="d-flex align-items-center col-actions"><a class="me-1" href="#" data-bs-toggle="tooltip"
                                                                                                              data-bs-placement="top" title="" data-bs-original-title="Send Mail" aria-label="Send Mail"><svg
                                                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                    class="feather feather-send font-medium-2 text-body">
                                                                <line x1="22" y1="2" x2="11" y2="13"></line>
                                                                <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                                                </svg></a><a class="me-1" href="app-invoice-preview.html" data-bs-toggle="tooltip"
                                                                         data-bs-placement="top" title="" data-bs-original-title="Preview Invoice"
                                                                         aria-label="Preview Invoice"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                              viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                              stroke-linejoin="round" class="feather feather-eye font-medium-2 text-body">
                                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                <circle cx="12" cy="12" r="3"></circle>
                                                                </svg></a><a href="javascript:void(0)" data-bs-toggle="tooltip" data-bs-placement="top" title=""
                                                                         data-bs-original-title="Download" aria-label="Download"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                                         width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                                                                         stroke-linecap="round" stroke-linejoin="round"
                                                                                                                         class="feather feather-download font-medium-2 text-body">
                                                                <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                                <polyline points="7 10 12 15 17 10"></polyline>
                                                                <line x1="12" y1="15" x2="12" y2="3"></line>
                                                                </svg></a></div>
                                                    </td>
                                                </tr>
                                                <tr class="even">
                                                    <td class=" control" tabindex="0" style="display: none;"></td>
                                                    <td class="sorting_1"><a class="fw-bolder" href="app-invoice-preview.html"> #4995</a></td>
                                                    <td><span data-bs-toggle="tooltip" data-bs-html="true" title=""
                                                              data-bs-original-title="<span>Partial Payment<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 06/09/2019</span>"
                                                              aria-label="<span>Partial Payment<br> <span class=&quot;fw-bold&quot;>Balance:</span> 0<br> <span class=&quot;fw-bold&quot;>Due Date:</span> 06/09/2019</span>">
                                                            <div class="avatar avatar-status bg-light-warning"><span class="avatar-content"><svg
                                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                        class="feather feather-pie-chart avatar-icon">
                                                                    <path d="M21.21 15.89A10 10 0 1 1 8 2.83"></path>
                                                                    <path d="M22 12A10 10 0 0 0 12 2v10z"></path>
                                                                    </svg></span></div>
                                                        </span></td>
                                                    <td>$3313</td>
                                                    <td style="">21 Aug 2019</td>
                                                    <td style="">
                                                        <div class="d-flex align-items-center col-actions"><a class="me-1" href="#" data-bs-toggle="tooltip"
                                                                                                              data-bs-placement="top" title="" data-bs-original-title="Send Mail" aria-label="Send Mail"><svg
                                                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                    class="feather feather-send font-medium-2 text-body">
                                                                <line x1="22" y1="2" x2="11" y2="13"></line>
                                                                <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                                                </svg></a><a class="me-1" href="app-invoice-preview.html" data-bs-toggle="tooltip"
                                                                         data-bs-placement="top" title="" data-bs-original-title="Preview Invoice"
                                                                         aria-label="Preview Invoice"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                              viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                                              stroke-linejoin="round" class="feather feather-eye font-medium-2 text-body">
                                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                <circle cx="12" cy="12" r="3"></circle>
                                                                </svg></a><a href="javascript:void(0)" data-bs-toggle="tooltip" data-bs-placement="top" title=""
                                                                         data-bs-original-title="Download" aria-label="Download"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                                                         width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                                                                         stroke-linecap="round" stroke-linejoin="round"
                                                                                                                         class="feather feather-download font-medium-2 text-body">
                                                                <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                                <polyline points="7 10 12 15 17 10"></polyline>
                                                                <line x1="12" y1="15" x2="12" y2="3"></line>
                                                                </svg></a></div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /Invoice table -->
                            </div>
                            <!--/ User Content -->
                        </div>
                    </section>
                    <!-- Edit User Modal -->
                    <div class="modal fade" id="editUser" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
                            <div class="modal-content">
                                <div class="modal-header bg-transparent">
                                    <button type="button" class="btn-close" id="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body pb-5 px-sm-5 pt-50" id="formEditProfile">
                                    <div class="text-center mb-2">
                                        <h1 class="mb-1">Edit User Information</h1>
                                        <p>Updating user details will receive a privacy audit.</p>
                                    </div>
                                    <form id="editUserForm" class="row gy-1 pt-75" onsubmit="return false">
                                        <div class="d-flex align-items-center justify-content-center flex-direction-column">
                                            <a href="#" class="me-25">
                                                <c:choose>
                                                    <c:when test="${user.getAvatar() == null}">
                                                        <c:set var="firstName" value="${user.getFirstName()}"/>
                                                        <c:set var="lastName" value="${user.getLastName()}"/>
                                                        <div id="none-background" class="avatar bg-light-danger img-fluid rounded me-50 mb-2" style="margin-right:0 !important">
                                                            <div class="avatar-content" style="width:200px;height:200px;font-size:2rem;">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                        </div>
                                                        <img src="app-assets/images/portrait/small/avatar-s-11.jpg" id="account-upload-img" class="uploadedAvatar rounded me-50" style="border: 1px solid var(--bs-body-color);display:none;" alt="profile image" height="200" width="200">
                                                    </c:when>
                                                    <c:when test="${user.getAvatar() != null}">
                                                        <img class="img-fluid rounded mt-3 mb-2" src="${user.getAvatar()}" id="account-upload-img" style="margin-top:0rem !important;margin-left:1rem; width:200px;height:200px;" alt="avatar" height="200"
                                                             width="200">   
                                                    </c:when>
                                                </c:choose>                                             
                                            </a>
                                            <!-- upload and reset button -->
                                            <div class="d-flex align-items-end mt-75 ms-1">
                                                <div>
                                                    <label for="account-upload" class="btn btn-sm btn-primary mb-75 me-75 waves-effect waves-float waves-light width-full">Upload</label>
                                                    <input type="file" id="account-upload" hidden="" accept="image/*">
                                                    <button type="button" id="account-reset" class="btn btn-sm btn-outline-secondary mb-75 waves-effect width-full">Reset</button>
                                                    <p class="mb-0">Allowed file types: png, jpg, jpeg.</p>
                                                </div>
                                            </div>
                                            <!--/ upload and reset button -->
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" for="modalEditUserFirstName">First Name</label>
                                            <input
                                                type="text"
                                                id="modalEditUserFirstName"
                                                name="modalEditUserFirstName"
                                                class="form-control"
                                                placeholder="John"
                                                value="${user.getFirstName()}"
                                                data-msg="Please enter your first name"
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" for="modalEditUserLastName">Last Name</label>
                                            <input
                                                type="text"
                                                id="modalEditUserLastName"
                                                name="modalEditUserLastName"
                                                class="form-control"
                                                placeholder="Doe"
                                                value="${user.getLastName()}"
                                                data-msg="Please enter your last name"
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" for="modalEditUserEmail">Email Address:</label>
                                            <input disabled
                                                   type="text"
                                                   id="modalEditUserEmail"
                                                   name="modalEditUserEmail"
                                                   class="form-control"
                                                   value="${user.getEmail()}"
                                                   placeholder="name@domain.com"
                                                   />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" for="modalEditCity">City</label>
                                            <input
                                                type="text"
                                                id="modalEditCity"
                                                name="modalEditCity"
                                                class="form-control modal-edit-tax-id"
                                                placeholder="Los Angeles"
                                                value="${user.getCity()}"
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" for="modalEditStreet">Street</label>
                                            <input
                                                type="text"
                                                id="modalEditStreet"
                                                name="modalEditStreet"
                                                class="form-control modal-edit-street"
                                                placeholder="Sanfoudry"
                                                value="${user.getStreet()}"
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" for="modalEditUserCountry">Country</label>
                                            <select id="modalEditUserCountry" name="modalEditUserCountry" class="select2 form-select">
                                                <option value="${user.getState()}">${user.getState()}</option>
                                                <option value="United State" class="text-capitalize">United State</option>
                                                <option value="Alaska" class="text-capitalize">Alaska</option>
                                                <option value="Hawaii" class="text-capitalize">Hawaii</option>
                                                <option value="California" class="text-capitalize">California</option>
                                                <option value="Nevada" class="text-capitalize">Nevada</option>
                                                <option value="Oregon" class="text-capitalize">Oregon</option>
                                                <option value="Washington" class="text-capitalize">Washington</option>
                                                <option value="Arizona" class="text-capitalize">Arizona</option>
                                                <option value="Colorado"class="text-capitalize">Colorado</option>
                                                <option value="Idaho" class="text-capitalize">Idaho</option>
                                                <option value="Montana"class="text-capitalize">Montana</option>
                                                <option value="Nebraska" class="text-capitalize">Nebraska</option>
                                                <option value="New Mexico" class="text-capitalize">New Mexico</option>
                                                <option value="North Dakota" class="text-capitalize">North Dakota</option>
                                                <option value="Utah" class="text-capitalize">Utah</option>
                                                <option value="Wyoming" class="text-capitalize">Wyoming</option>
                                                <option value="Alabama" class="text-capitalize">Alabama</option>
                                                <option value="Arkansas" class="text-capitalize">Arkansas</option>
                                                <option value="Illinois" class="text-capitalize">Illinois</option>
                                                <option value="Iowa" class="text-capitalize">Iowa</option>
                                                <option value="Kansas" class="text-capitalize">Kansas</option>
                                                <option value="Kentucky" class="text-capitalize">Kentucky</option>
                                                <option value="Louisiana" class="text-capitalize">Louisiana</option>
                                                <option value="Minnesota" class="text-capitalize">Minnesota</option>
                                                <option value="Mississippi" class="text-capitalize">Mississippi</option>
                                                <option value="Missouri" class="text-capitalize">Missouri</option>
                                                <option value="Oklahoma"class="text-capitalize">Oklahoma</option>
                                                <option value="South Dakota" class="text-capitalize">South Dakota</option>
                                                <option value="Texas" class="text-capitalize">Texas</option>
                                                <option value="Tennessee" class="text-capitalize">Tennessee</option>
                                                <option value="Wisconsin" class="text-capitalize">Wisconsin</option>
                                                <option value="Connecticut" class="text-capitalize">Connecticut</option>
                                                <option value="Delaware" class="text-capitalize">Delaware</option>
                                                <option value="Florida" class="text-capitalize">Florida</option>
                                                <option value="Georgia" class="text-capitalize">Georgia</option>
                                                <option value="Indiana" class="text-capitalize">Indiana</option>
                                                <option value="Maine" class="text-capitalize">Maine</option>
                                                <option value="Maryland" class="text-capitalize">Maryland</option>
                                                <option value="Massachusetts" class="text-capitalize">Massachusetts</option>
                                                <option value="Michigan" class="text-capitalize">Michigan</option>
                                                <option value="New Hampshire" class="text-capitalize">New Hampshire</option>
                                                <option value="New Jersey" class="text-capitalize">New Jersey</option>
                                                <option value="New York" class="text-capitalize">New York</option>
                                                <option value="North Carolina" class="text-capitalize">North Carolina</option>
                                                <option value="Ohio" class="text-capitalize">Ohio</option>
                                                <option value="Pennsylvania" class="text-capitalize">Pennsylvania</option>
                                                <option value="Rhode Island" class="text-capitalize">Rhode Island</option>
                                                <option value="South Carolina" class="text-capitalize">South Carolina</option>
                                                <option value="Vermont" class="text-capitalize">Vermont</option>
                                                <option value="Virginia" class="text-capitalize">Virginia</option>
                                                <option value="West Virginia" class="text-capitalize">West Virginia</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" for="modalEditUserPhone">Contact</label>
                                            <input
                                                type="text"
                                                id="modalEditUserPhone"
                                                name="modalEditUserPhone"
                                                class="form-control phone-number-mask"
                                                placeholder="+1 (609) 933-44-22"
                                                value="${user.getPhoneNumber()}"
                                                maxlength="13"
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" for="modalEditUserRole">Role</label>
                                            <input disabled
                                                   type="text"
                                                   id="modalEditUserRole"
                                                   name="modalEditUserRole"
                                                   class="form-control role-mask"
                                                   placeholder="Customer"
                                                   <c:set var="lengthRole" value="${fn:length(user.getRole())}"/>
                                                   <c:set var="halfString" value="${fn:substring(user.getRole(), 1, lengthRole)}"/>
                                                   value="${fn:substring(user.getRole(), 0, 1)}${fn:toLowerCase(halfString)}"
                                                   />
                                        </div>
                                        <div class="col-12 text-center mt-2 pt-50">
                                            <button type="submit" class="btn btn-primary me-1" id="editProfileUserSubmit">Submit</button>
                                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                                Discard
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Edit User Modal -->
                    
                    <!-- Request update user Modal -->

                    <div class="modal fade" id="registerStore" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
                            <div class="modal-content">
                                <div class="modal-header bg-transparent">
                                    <button type="button" class="btn-close" id="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body pb-5 px-sm-5 pt-50" id="registerStoreForm">
                                    <div class="text-center mb-2">
                                        <h1 class="mb-1">Register Store</h1>
                                        <p>User registers with admin to open a store</p>
                                    </div>
                                    <form action="registerStore" id="registerStoreForm" class="row gy-1 pt-75" method="post">
                                        <div class="d-flex align-items-center justify-content-center flex-direction-column">

                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" >First Name</label>
                                            <input
                                                type="text"
                                                id="firstname"
                                                name="firstname"
                                                class="form-control"
                                                placeholder="John"
                                                value="${user.getFirstName()}"
                                                data-msg="Please enter your first name"
                                                required pattern="^[a-zA-Z0-9]{4,20}$"
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" >Last Name</label>
                                            <input
                                                type="text"
                                                id="lastname"
                                                name="lastname"
                                                class="form-control"
                                                placeholder="Doe"
                                                value="${user.getLastName()}"
                                                data-msg="Please enter your last name"
                                                required pattern="^[a-zA-Z0-9]{4,20}$"
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" >Email Address:</label>
                                            <input 
                                                type="text"
                                                id="email"
                                                name="email"
                                                class="form-control"
                                                value="${user.getEmail()}"
                                                placeholder="name@domain.com"
                                                required pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" >Store Name</label>
                                            <input
                                                type="text"
                                                id="storename"
                                                name="storename"
                                                class="form-control"
                                                placeholder="Store Name"
                                                required pattern="^[a-zA-Z0-9]{4,20}$"
                                                data-msg="Please enter your first name"
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" >City</label>
                                            <input
                                                type="text"
                                                id="city"
                                                name="city"
                                                class="form-control modal-edit-tax-id"
                                                placeholder="Los Angeles"
                                                value="${user.getCity()}"
                                                required
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" >Street</label>
                                            <input
                                                type="text"
                                                id="street"
                                                name="street"
                                                class="form-control modal-edit-street"
                                                placeholder="Sanfoudry"
                                                value="${user.getStreet()}"
                                                required
                                                />
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-label" >Country</label>
                                            <input
                                                type="text"
                                                id="country"
                                                name="country"
                                                class="form-control modal-edit-street"
                                                placeholder="Sanfoudry"
                                                value="${user.getState()}"
                                                required
                                                />
                                        </div>

                                        <div class="col-12 col-md-6">
                                            <label class="form-label" >Contact</label>
                                            <input
                                                type="text"
                                                id="contact"
                                                name="contact"
                                                class="form-control phone-number-mask"
                                                placeholder="+1 (609) 933-44-22"
                                                value="${user.getPhoneNumber()}"
                                                maxlength="13"
                                                required pattern="^[0-9]{10}$"
                                                />
                                        </div>




                                        <div class="col-12 col-md-12">
                                            <label class="form-label" >Description</label>
                                            <input
                                                type="text"
                                                id="description"
                                                name="description"
                                                class="form-control modal-edit-street"
                                                placeholder="description"
                                                required
                                                />
                                        </div>
                                        <div class="col-12 col-md-12">
                                            <label class="form-label" >Other</label>
                                            <input
                                                type="text"
                                                id="other"
                                                name="other"
                                                class="form-control modal-edit-street"
                                                placeholder="Reason"
                                                required
                                                />
                                        </div>



                                        <div class="col-12 text-center mt-2 pt-50">
                                            <button type="submit" class="btn btn-primary me-1" id="registerStore">Submit</button>
                                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                                Discard
                                            </button>
                                        </div>
                                        <input type="hidden" name="avatarImage" class="hiddenI" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--/ Request update User Modal -->
                </div>
            </div>
        </div>
        <!-- END: Content-->


        <!-- BEGIN: Customizer-->
        <div class="customizer d-none d-md-block"><a class="customizer-toggle d-flex align-items-center justify-content-center" href="#"><i class="spinner" data-feather="settings"></i></a><div class="customizer-content">
                <!-- Customizer header -->
                <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                    <h4 class="mb-0">Theme Customizer</h4>
                    <p class="m-0">Customize & Preview in Real Time</p>

                    <a class="customizer-close" href="#"><i data-feather="x"></i></a>
                </div>

                <hr />

                <!-- Styling & Text Direction -->
                <div class="customizer-styling-direction px-2">
                    <p class="fw-bold">Skin</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input
                                type="radio"
                                id="skinlight"
                                name="skinradio"
                                class="form-check-input layout-name"
                                checked
                                data-layout=""
                                />
                            <label class="form-check-label" for="skinlight">Light</label>
                        </div>
                        <div class="form-check me-1">
                            <input
                                type="radio"
                                id="skinbordered"
                                name="skinradio"
                                class="form-check-input layout-name"
                                data-layout="bordered-layout"
                                />
                            <label class="form-check-label" for="skinbordered">Bordered</label>
                        </div>
                        <div class="form-check me-1">
                            <input
                                type="radio"
                                id="skindark"
                                name="skinradio"
                                class="form-check-input layout-name"
                                data-layout="dark-layout"
                                />
                            <label class="form-check-label" for="skindark">Dark</label>
                        </div>
                        <div class="form-check">
                            <input
                                type="radio"
                                id="skinsemidark"
                                name="skinradio"
                                class="form-check-input layout-name"
                                data-layout="semi-dark-layout"
                                />
                            <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                        </div>
                    </div>
                </div>

                <hr />

                <!-- Menu -->
                <div class="customizer-menu px-2">
                    <div id="customizer-menu-collapsible" class="d-flex">
                        <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                        <div class="form-check form-check-primary form-switch">
                            <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                            <label class="form-check-label" for="collapse-sidebar-switch"></label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Layout Width -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Layout Width</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                            <label class="form-check-label" for="layout-width-full">Full Width</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Navbar -->
                <div class="customizer-navbar px-2">
                    <div id="customizer-navbar-colors">
                        <p class="fw-bold">Navbar Color</p>
                        <ul class="list-inline unstyled-list">
                            <li class="color-box bg-white border selected" data-navbar-default=""></li>
                            <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                            <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                            <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                            <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                            <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                            <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                            <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                        </ul>
                    </div>

                    <p class="navbar-type-text fw-bold">Navbar Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                            <label class="form-check-label" for="nav-type-floating">Floating</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-static">Static</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Footer -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Footer Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                            <label class="form-check-label" for="footer-type-static">Static</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End: Customizer-->

        <!-- Buynow Button-->
        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix mb-0"><span class="float-md-start d-block d-md-inline-block mt-25">COPYRIGHT  &copy; 2022<a class="ms-25" href="https://1.envato.market/pixinvent_portfolio" target="_blank">Hilfsmotor</a></p>
        </footer>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
        <!-- END: Footer-->
        <input type="hidden" name="avatarImage" class="hiddenI" value="">


        <!-- BEGIN: Vendor JS-->
        <script src="app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
        <script src="app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
        <script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
        <script src="app-assets/vendors/js/extensions/moment.min.js"></script>
        <script src="app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
        <script src="app-assets/vendors/js/extensions/polyfill.min.js"></script>
        <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="app-assets/js/core/app-menu.min.js"></script>
        <script src="app-assets/js/core/app.min.js"></script>
        <script src="app-assets/js/scripts/customizer.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="app-assets/js/scripts/pages/modal-edit-user.min.js"></script>
        <script src="app-assets/js/scripts/pages/app-user-view-account.min.js"></script>
        <script src="app-assets/js/scripts/pages/app-user-view.min.js"></script>
        <script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
        <!-- END: Page JS-->

        <!-- BEGIN: Page JS-->
        <script src="app-assets/js/scripts/pages/page-account-settings-account.min.js"></script>
        <!-- END: Page JS-->
        <script src="assets/js/page-account-settings.js">
        </script>
        <script>
            $(window).on('load', function () {
                if (feather) {
                    feather.replace({width: 14, height: 14});
                }
            });
            $(document).ready(function () {
                var readURL = function (input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        var fileByteArray = [];
                        reader.readAsArrayBuffer(input.files[0]);
                        reader.onload = function (evt) {
                            if (evt.target.readyState === FileReader.DONE) {
                                var arrayBuffer = evt.target.result,
                                        array = new Uint8Array(arrayBuffer);
                                for (var i = 0; i < array.length; i++) {
                                    fileByteArray.push(array[i]);
                                }
                                console.log(fileByteArray);
                                document.querySelector(".hiddenI").value = fileByteArray;
                            }
                        };
                    }
                };
                $("#account-upload").on('change', function () {
                    readURL(this);
                });
            });
        </script>
    </body>
    <!-- END: Body-->
</html>