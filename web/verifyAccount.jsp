<%-- 
    Document   : verifyAccount
    Created on : Sep 12, 2022, 11:05:57 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Verify Account</title>
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <div class="main">
            <div class="verify-page">
                <div class="verify-page__content flex-default flex-direction-column flex-center row-gap-default" modifier="yes">
                    <img src="assets/img/RentabikeLogo.jpg" alt="" class="verify-page__image-logo">
                    <div class="verify-page__introduced">Hilfsmotor</div>
                    <div class="verify-page__introduced-slogan">Take your dream and ride to your future</div>
                </div>
                <%if(session.getAttribute("verifyStatus") == "success"){%>
                <div class="verify-page__content">
                    <img src="assets/img/handDrawn/success.png" alt="" class="verify-page__image">
                    <div class="verify-page__message">
                        Congratulation, you have verified your account.<br> 
                        Please comeback to login to enter your information. 
                    </div>
                    <div class="verify-page__action induct-page__btn home-page__introduce-option position-relative" type="wb">
                        <a href="started.jsp" class="induct-page__link home-page__introduce-link position-relative">BACK TO SIGN IN</a>
                        <div class="induct-page__seperate home-page__introduce-seperate"></div>
                    </div>
                </div>
                <%session.setAttribute("verifyStatus", "success");}else{%>
                <div class="verify-page__content">
                    <img src="assets/img/handDrawn/blacklines.jpg" alt="" class="verify-page__image">
                    <div class="verify-page__message">
                        Sorry, but your message verify is expired.<br> 
                        Please comeback to check your verify in profile user again. 
                    </div>
                    <div class="verify-page__action induct-page__btn home-page__introduce-option position-relative" type="wb">
                        <a href="started.jsp" class="induct-page__link home-page__introduce-link position-relative">BACK TO SIGN IN</a>
                        <div class="induct-page__seperate home-page__introduce-seperate"></div>
                    </div>
                </div>
                <%}%>
                <div class="footer-page__credit verify-page__credit">
                    <div class="footer-page__credit-text">(c) 2022 Hilfsmotor - Motorcycles Dealer Template. All rights
                        reserved.</div>
                </div>
            </div>
        </div>
    </body>
</html>