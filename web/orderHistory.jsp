<!DOCTYPE html>

<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/wizard/bs-stepper.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/app-ecommerce.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/pickers/form-pickadate.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-wizard.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-number-input.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="vertical-layout vertical-menu-modern  navbar-floating footer-static   menu-collapsed" data-open="click"
          data-menu="vertical-menu-modern" data-col="">

        <!-- BEGIN: Header-->
        <nav
            class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-shadow navbar-shadow container-xxl">
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Contact"><i class="ficon" data-feather="mail"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Help"><i class="ficon" data-feather="help-circle"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning"
                                                                                                    data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="#"
                                                                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"
                                                                                                        data-language="en"><i class="flag-icon flag-icon-us"></i> English</a>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon"
                                                                                                 data-feather="moon"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon"
                                                                                           data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Hilfsmotor..." tabindex="-1"
                                   data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-cart me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i
                                class="ficon" data-feather="shopping-cart"></i><span
                                class="badge rounded-pill bg-primary badge-up cart-item-count">6</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">My Cart</h4>
                                    <div class="badge rounded-pill badge-light-primary">4 Items</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list">
                                <div class="list-item align-items-center"><img class="d-block rounded me-1"
                                                                               src="app-assets/images/pages/eCommerce/1.png" alt="donuts" width="62">
                                    <div class="list-item-body flex-grow-1">
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="productDetailController?mx=${orderItem.p.productID}"> Honda MX-219 2019</a>
                                            </h6>
                                            <div class="flex-default" style="column-gap:0.5rem">
                                                <div class="text-body badge rounded-pill badge-light-primary">Honda</div>
                                                <div class="text-body badge rounded-pill badge-light-primary">Chopper Version</div>
                                                <div class="text-body badge rounded-pill badge-light-primary">Rental Option</div>
                                            </div>
                                            <div class="flex-default" style="column-gap:0.5rem">
                                                <div class="text-body badge rounded-pill badge-light-success mt-1">Quantity: 1</div>
                                                <div class="text-body badge rounded-pill badge-light-danger mt-1">Tourist: None</div>
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$374.90</h5>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-menu-footer">
                                <div class="d-flex justify-content-between mb-1">
                                    <h6 class="fw-bolder mb-0">Total:</h6>
                                    <h6 class="text-primary fw-bolder mb-0">$10,999.00</h6>
                                </div><a class="btn btn-primary w-100" href="app-ecommerce-checkout.html">Checkout</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-notification me-25" id="notifyOpen">
                        <a class="nav-link" href="#" data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span
                                class="badge rounded-pill bg-danger badge-up" id="notifyCalculation">${totalNew}</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">Total Slot New: 5</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list" id="notify">
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link"
                           id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none lidget-naz" current-login="${user.getEmail()}">
                                <span class="user-name fw-bolder">${user.getFirstName()} ${user.getLastName()}</span>
                                <span class="user-status" style="margin-top:0.25rem">${user.getRole()}</span></div>
                            <span class="avatar">
                                <c:choose>
                                    <c:when test="${user.getAvatar() == null}">
                                        <c:set var="firstName" value="${user.getFirstName()}"/>
                                        <c:set var="lastName" value="${user.getLastName()}"/>
                                        <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                            <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                        </div>
                                    </c:when>
                                    <c:when test="${user.getAvatar() != null}">
                                        <img class="round" src="${user.getAvatar()}" alt="avatar" height="40"
                                             width="40">   
                                    </c:when>
                                </c:choose> 
                                <span class="avatar-status-online"></span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="page-profile.html"><i class="me-50" data-feather="user"></i> Profile
                            </a>
                            <a class="dropdown-item" href="app-email.html"><i class="me-50" data-feather="mail"></i> Contact
                            </a>
                            <div class="dropdown-divider"></div><a class="dropdown-item" href="page-account-settings-account.html">
                                <i class="me-50" data-feather="settings"></i> Settings</a>
                            <a class="dropdown-item" href="page-faq.html"><i class="me-50" data-feather="help-circle"></i> FAQ</a>
                            <a class="dropdown-item" href="auth-login-cover.html"><i class="me-50" data-feather="power"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd
                                Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small>
                </a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div>
                </a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a
                    class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No
                            results found.</span></div>
                </a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item me-auto">
                        <a class="navbar-brand position-relative" style="top:-25px" href="productListController">
                            <span>
                                <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo position-relative"
                                     style="left:-18px">
                            </span>
                            <h2 class="brand-text mb-0" style="position: relative; right: 35px;">Hilfsmotor</h2>
                        </a>
                    </li>
                    <li class="nav-item nav-toggle lidget-position-control"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                                class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                                class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc"
                                data-ticon="disc"></i></a></li>
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content mt-1">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Apps &amp; Pages</span><i
                            data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="app-email.html"><i
                                data-feather="mail"></i><span class="menu-title text-truncate" data-i18n="Email">Email</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="shopping-cart"></i><span
                                class="menu-title text-truncate" data-i18n="eCommerce">Product</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="app-ecommerce-shop.html"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Shop">Shop</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="app-ecommerce-wishlist.html"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Wish List">Wish
                                        List</span></a>
                            </li>
                            <li ><a class="d-flex align-items-center" href="app-ecommerce-checkout.html"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Checkout">Checkout</span></a>
                            </li>
                            <li class="active"><a class="d-flex align-items-center" href="customerOrderItemList"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Checkout">My Order</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="user"></i><span
                                class="menu-title text-truncate" data-i18n="User">User</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="View">View</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="userProfileController"><span
                                                class="menu-item text-truncate" data-i18n="Account">Account</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-security.html"><span
                                                class="menu-item text-truncate" data-i18n="Security">Security</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-billing.html"><span
                                                class="menu-item text-truncate" data-i18n="Billing &amp; Plans">Billing &amp; Plans</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-notifications.html"><span
                                                class="menu-item text-truncate" data-i18n="Notifications">Notifications</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-connections.html"><span
                                                class="menu-item text-truncate" data-i18n="Connections">Connections</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span
                                class="menu-title text-truncate" data-i18n="Pages">Pages</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="page-faq.html"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="FAQ">FAQ</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Blog">Blog</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="page-blog-list.html"><span
                                                class="menu-item text-truncate" data-i18n="List">List</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="page-blog-detail.html"><span
                                                class="menu-item text-truncate" data-i18n="Detail">Detail</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="page-blog-edit.html"><span
                                                class="menu-item text-truncate" data-i18n="Edit">Edit</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" navigation-header"><span data-i18n="Misc">Misc</span><svg xmlns="http://www.w3.org/2000/svg"
                                                                                          width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                                          stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-horizontal">
                        <circle cx="12" cy="12" r="1"></circle>
                        <circle cx="19" cy="12" r="1"></circle>
                        <circle cx="5" cy="12" r="1"></circle>
                        </svg>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="menu"></i><span
                                class="menu-title text-truncate" data-i18n="Menu Levels">Menu Levels</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.1</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.2</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate"
                                                                                            data-i18n="Third Level">Third Level 3.1</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate"
                                                                                            data-i18n="Third Level">Third Level 3.2</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content ecommerce-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <div class="row breadcrumbs-top">
                            <div class="col-12">
                                <h2 class="content-header-title float-start mb-0">My Order</h2>
                                <div class="breadcrumb-wrapper">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="productListController">Home</a>
                                        </li>

                                        <li class="breadcrumb-item active">My Order
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                        <div class="mb-1 breadcrumb-right">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        data-feather="grid"></i></button>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <a class="dropdown-item" href="app-email.html">
                                        <i class="me-1" data-feather="mail"></i>
                                        <span class="align-middle">Contact</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <div class="bs-stepper checkout-tab-steps">
                        <!-- Wizard starts -->
                        <div class="bs-stepper-header">
                            <div class="step" data-target="#step-cart" role="tab" id="step-cart-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">
                                        <i data-feather="menu" class="font-medium-3"></i>
                                    </span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">All Orders</span>
                                        <span class="bs-stepper-subtitle">List all your order</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="minus" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#step-address" role="tab" id="step-address-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">
                                        <i data-feather="loader" class="font-medium-3"></i>
                                    </span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Processing Orders</span>
                                        <span class="bs-stepper-subtitle">List all processing order</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="minus" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#step-payment" role="tab" id="step-payment-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">
                                        <i data-feather="check" class="font-medium-3"></i>
                                    </span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Completed Orders</span>
                                        <span class="bs-stepper-subtitle">List all Completed order</span>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <!-- Wizard ends -->

                        <div class="bs-stepper-content">
                            <!-- Checkout Place order starts -->
                            <div id="step-cart" class="content" role="tabpanel" aria-labelledby="step-cart-trigger">
                                <div id="place-order" class="list-view product-checkout">
                                    <!-- Checkout Place Order Left starts -->
                                    <div class="checkout-items">
                                        <form action="customerOrderItemList">
                                            <div class="me-1">
                                                <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" on value="${txtS}" name="Search" id="Search"
                                                                                                                                   class="form-control" placeholder="" aria-controls="DataTables_Table_0"></label></div>
                                            <button type="submit"  class="btn btn-dark mt-1">
                                                <span>Search</span>
                                            </button>
                                            </div>
                                            
                                        </form>
                                        <br><br>
                                        <c:forEach items="${sessionScope.orderHistoryList}"  var="orderItem">
                                            <div class="card ecommerce-card position-relative">
                                                <div class="item-img">
                                                    <a href="productDetailController?mx=${orderItem.p.productID}">
                                                        <img src="${orderItem.p.image}" alt="img-placeholder" />

                                                    </a>
                                                </div>
                                                <div class="card-body">
                                                    <div class="item-name">
                                                        <h6 class="mb-0"><a href="productDetailController?mx=${orderItem.p.productID}" class="card-title" style="margin-bottom:0;">${orderItem.p.name}</a>
                                                        </h6>
                                                        <div class="text-body">${orderItem.p.brand}</div>
                                                        <div class="flex-default" style="column-gap:0.5rem">
                                                            <div class="card-text mb-1 badge rounded-pill badge-light-primary mt-1">${orderItem.p.modelYear} Version</div>
                                                            <div class="card-text mb-1 badge rounded-pill badge-light-danger mt-1">${orderItem.p.category}</div>
                                                            <div class="card-text mb-1 badge rounded-pill badge-light-warning mt-1">For ${orderItem.p.mode}</div>
                                                        </div>
                                                        <div class="item-company">Sell By <a style="color: #7367F0;" class="company-name">${orderItem.storeName}</a></div>

                                                    </div>
                                                    <span class="text-success mb-1">Mode: ${orderItem.p.mode}</span>
                                                    <span class="text-success mb-1">Quantity: ${orderItem.quantity}</span>


                                                    <c:choose>
                                                        <c:when test="${orderItem.p.mode == 'Rental'}">
                                                            <span class="text-success mb-1">Rent Time: ${orderItem.rentalDateNum} Days</span>
                                                        </c:when>
                                                        <c:when test="${orderItem.p.mode == 'Sell'}">

                                                        </c:when>
                                                    </c:choose>
                                                    <span class="text-success mb-1">Total price: ${orderItem.price}$</span>            
                                                    <span class="delivery-date text-muted">Order Date: ${orderItem.orderDate}</span>
                                                    <span class="delivery-date text-muted">Shipped Date: ${orderItem.shippedDate}</span>
                                                </div>
                                                <div class="item-options text-center">
                                                    <div class="item-wrapper">
                                                        <div class="item-cost">
                                                            <c:choose>
                                                                <c:when test="${orderItem.p.mode == 'Rental'}">
                                                                    <h4 class="item-price">${orderItem.p.price}$/Days</h4>
                                                                </c:when>
                                                                <c:when test="${orderItem.p.mode == 'Sell'}">
                                                                    <h4 class="item-price">${orderItem.p.price}$</h4>
                                                                </c:when>
                                                            </c:choose>


                                                            <c:choose>
                                                                <c:when test="${orderItem.status == 1}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-warning">PROCESSING</span>
                                                                    </p>
                                                                </c:when>
                                                                <c:when test="${orderItem.status == 2}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-primary">SHIPPING</span>
                                                                    </p>
                                                                </c:when>
                                                                <c:when test="${orderItem.status == 3}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-danger">STOP</span>
                                                                    </p>                                                           
                                                                </c:when>
                                                                <c:when test="${orderItem.status == 4}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-success">COMPLETE</span>
                                                                    </p>                                                           </c:when>
                                                            </c:choose>
                                                        </div>
                                                    </div>
                                                    <button type="button" onclick="location.href = 'productDetailController?mx=${orderItem.p.productID}';" class="btn btn-light mt-1">
                                                        <span>VIEW PRODUCT</span>
                                                    </button>
                                                    <button type="button" class="btn btn-primary btn-cart move-cart">
                                                        <i data-feather="heart" class="align-middle me-25"></i>
                                                        <span class="text-truncate">ORDER AGAIN</span>
                                                    </button>
                                                </div><c:choose>
                                                    <c:when test="${orderItem.p.mode == 'Rental'}">
                                                        <div class="lidget-card-ecomerce checkout">
                                                            Rental Only
                                                        </div>                                                                
                                                    </c:when>
                                                    <c:when test="${orderItem.p.mode == 'Sell'}">

                                                    </c:when>
                                                </c:choose>

                                            </div>
                                        </c:forEach>
                                    </div>
                                    <!-- Checkout Place Order Left ends -->

                                    <!-- Checkout Place Order Right starts -->
                                    <div class="checkout-options">
                                        <div class="card">
                                            <div class="card-body">

                                                <hr />
                                                <div class="price-details">
                                                    <h6 class="price-title">List Order Price</h6>
                                                    <ul class="list-unstyled">
                                                        <li class="price-detail">
                                                            <div class="detail-title">Total price</div>
                                                            <div class="detail-amt">$${sessionScope.totalPrice}</div>
                                                        </li>
                                                        <li class="price-detail">
                                                            <div class="detail-title">Total Paid</div>
                                                            <div class="detail-amt discount-amt text-success">-${sessionScope.hasPaid}$</div>
                                                        </li>
                                                        <li class="price-detail">
                                                            <div class="detail-title">Discount</div>
                                                            <div class="detail-amt">$${sessionScope.discount}</div>
                                                        </li>
                                                        <li class="price-detail">
                                                            <div class="detail-title">Tax</div>
                                                            <div class="detail-amt">$${sessionScope.taxes}</div>
                                                        </li>

                                                        <li class="price-detail">
                                                            <div class="detail-title">Delivery Charges</div>
                                                            <div class="detail-amt discount-amt text-success">Free</div>
                                                        </li>
                                                    </ul>
                                                    <hr />
                                                    <ul class="list-unstyled">
                                                        <li class="price-detail">
                                                            <div class="detail-title detail-total">Total Must Pay</div>
                                                            <div class="detail-amt fw-bolder">$${sessionScope.totalPrice - sessionScope.hasPaid - sessionScope.discount + sessionScope.taxes}</div>
                                                        </li>
                                                    </ul>
                                                    <button type="button" class="btn btn-primary w-100 ">Pay Now</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Checkout Place Order Right ends -->
                                    </div>
                                </div>
                                <!-- Checkout Place order Ends -->
                            </div>
                            <!-- Checkout Customer Address Starts -->
                            <div id="step-address" class="content" role="tabpanel" aria-labelledby="step-cart-trigger">
                                <div id="place-order" class="list-view product-checkout">
                                    <!-- Checkout Place Order Left starts -->
                                    <div class="checkout-items">
                                        <c:forEach items="${sessionScope.oProcessingItemList}"  var="orderItem">
                                            <div class="card ecommerce-card position-relative">
                                                <div class="item-img">
                                                   <div class="item-img">
                                                    <a href="productDetailController?mx=${orderItem.p.productID}">
                                                        <img src="${orderItem.p.image}" alt="img-placeholder" />

                                                    </a>
                                                </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="item-name">
                                                        <h6 class="mb-0"><a href="productDetailController?mx=${orderItem.p.productID}" class="card-title" style="margin-bottom:0;">${orderItem.p.name}</a>
                                                        </h6>
                                                        <div class="text-body">${orderItem.p.brand}</div>
                                                        <div class="flex-default" style="column-gap:0.5rem">
                                                            <div class="card-text mb-1 badge rounded-pill badge-light-primary mt-1">${orderItem.p.modelYear} Version</div>
                                                            <div class="card-text mb-1 badge rounded-pill badge-light-danger mt-1">${orderItem.p.category}</div>
                                                            <div class="card-text mb-1 badge rounded-pill badge-light-warning mt-1">For ${orderItem.p.mode}</div>
                                                        </div>
                                                        <div class="item-company">Sell By <a href='user-store?storeID=${orderItem.storeID}' style="color: #7367F0;" class="company-name">${orderItem.storeName}</a></div>

                                                    </div>
                                                    <span class="text-success mb-1">Mode: ${orderItem.p.mode}</span>
                                                    <span class="text-success mb-1">Quantity: ${orderItem.quantity}</span>


                                                    <c:choose>
                                                        <c:when test="${orderItem.p.mode == 'Rental'}">
                                                            <span class="text-success mb-1">Rent Time: ${orderItem.rentalDateNum} Days</span>
                                                        </c:when>
                                                        <c:when test="${orderItem.p.mode == 'Sell'}">

                                                        </c:when>
                                                    </c:choose>
                                                    <span class="text-success mb-1">Total price: ${orderItem.price}$</span>            
                                                    <span class="delivery-date text-muted">Order Date: ${orderItem.orderDate}</span>
                                                    <span class="delivery-date text-muted">Shipped Date: ${orderItem.shippedDate}</span>
                                                </div>
                                                <div class="item-options text-center">
                                                    <div class="item-wrapper">
                                                        <div class="item-cost">
                                                            <c:choose>
                                                                <c:when test="${orderItem.p.mode == 'Rental'}">
                                                                    <h4 class="item-price">${orderItem.p.price}$/Days</h4>
                                                                </c:when>
                                                                <c:when test="${orderItem.p.mode == 'Sell'}">
                                                                    <h4 class="item-price">${orderItem.p.price}$</h4>
                                                                </c:when>
                                                            </c:choose>


                                                            <c:choose>
                                                                <c:when test="${orderItem.status == 1}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-warning">PROCESSING</span>
                                                                    </p>
                                                                </c:when>
                                                                <c:when test="${orderItem.status == 2}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-primary">SHIPPING</span>
                                                                    </p>
                                                                </c:when>
                                                                <c:when test="${orderItem.status == 3}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-danger">STOP</span>
                                                                    </p>                                                           
                                                                </c:when>
                                                                <c:when test="${orderItem.status == 4}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-success">COMPLETE</span>
                                                                    </p>                                                           </c:when>
                                                            </c:choose>
                                                        </div>
                                                    </div>
                                                    <button type="button" onclick="location.href = 'productDetailController?mx=${orderItem.p.productID}';" class="btn btn-light mt-1">
                                                        <span>VIEW PRODUCT</span>
                                                    </button>
                                                    <button type="button" class="btn btn-primary btn-cart move-cart">
                                                        <i data-feather="heart" class="align-middle me-25"></i>
                                                        <span class="text-truncate">ORDER AGAIN</span>
                                                    </button>
                                                </div><c:choose>
                                                    <c:when test="${orderItem.p.mode == 'Rental'}">
                                                        <div class="lidget-card-ecomerce checkout">
                                                            Rental Only
                                                        </div>                                                                
                                                    </c:when>
                                                    <c:when test="${orderItem.p.mode == 'Sell'}">

                                                    </c:when>
                                                </c:choose>

                                            </div>
                                        </c:forEach>
                                    </div>
                                    <!-- Checkout Place Order Left ends -->

                                    <!-- Checkout Place Order Right starts -->
                                    <div class="checkout-options">
                                        <div class="card">
                                            <div class="card-body">
                                                
                                                <hr />
                                                <div class="price-details">
                                                    <h6 class="price-title">List Order Price</h6>
                                                    <ul class="list-unstyled">
                                                        <li class="price-detail">
                                                            <div class="detail-title">Total price</div>
                                                            <div class="detail-amt">$${sessionScope.totalPrice}</div>
                                                        </li>
                                                        <li class="price-detail">
                                                            <div class="detail-title">Total Paid</div>
                                                            <div class="detail-amt discount-amt text-success">-${sessionScope.hasPaid}$</div>
                                                        </li>
                                                        <li class="price-detail">
                                                            <div class="detail-title">Tax</div>
                                                            <div class="detail-amt">$${sessionScope.totalTax}</div>
                                                        </li>

                                                        <li class="price-detail">
                                                            <div class="detail-title">Delivery Charges</div>
                                                            <div class="detail-amt discount-amt text-success">Free</div>
                                                        </li>
                                                    </ul>
                                                    <hr />
                                                    <ul class="list-unstyled">
                                                        <li class="price-detail">
                                                            <div class="detail-title detail-total">Total Must Pay</div>
                                                            <div class="detail-amt fw-bolder">$${sessionScope.total}</div>
                                                        </li>
                                                    </ul>
                                                    <button type="button" class="btn btn-primary w-100 ">Pay Now</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Checkout Place Order Right ends -->
                                    </div>
                                </div>
                            </div>
                            <!-- Checkout Customer Address Left ends -->

                            <!-- Checkout Customer Address Right starts -->

                            <!-- Checkout Customer Address Right ends -->


                            <!-- Checkout Customer Address Ends -->
                            <!-- Checkout Payment Starts -->
                            <div id="step-payment" class="content" role="tabpanel" aria-labelledby="step-cart-trigger">
                                <div id="place-order" class="list-view product-checkout">
                                    <!-- Checkout Place Order Left starts -->
                                    <div class="checkout-items">
                                        <c:forEach items="${sessionScope.oCompleteItemList}"  var="orderItem">
                                            <div class="card ecommerce-card position-relative">
                                                <div class="item-img">
                                                    <a href="productDetailController?mx=${orderItem.p.productID}">
                                                        <img src="${orderItem.p.image}" alt="img-placeholder" />

                                                    </a>
                                                </div>
                                                <div class="card-body">
                                                    <div class="item-name">
                                                        <h6 class="mb-0"><a href="productDetailController?mx=${orderItem.p.productID}" class="card-title" style="margin-bottom:0;">${orderItem.p.name}</a>
                                                        </h6>
                                                        <div class="text-body">${orderItem.p.brand}</div>
                                                        <div class="flex-default" style="column-gap:0.5rem">
                                                            <div class="card-text mb-1 badge rounded-pill badge-light-primary mt-1">${orderItem.p.modelYear} Version</div>
                                                            <div class="card-text mb-1 badge rounded-pill badge-light-danger mt-1">${orderItem.p.category}</div>
                                                            <div class="card-text mb-1 badge rounded-pill badge-light-warning mt-1">For ${orderItem.p.mode}</div>
                                                        </div>
                                                        <div class="item-company">Sell By <a style="color: #7367F0;" class="company-name">${orderItem.storeName}</a></div>

                                                    </div>
                                                    <span class="text-success mb-1">Mode: ${orderItem.p.mode}</span>
                                                    <span class="text-success mb-1">Quantity: ${orderItem.quantity}</span>


                                                    <c:choose>
                                                        <c:when test="${orderItem.p.mode == 'Rental'}">
                                                            <span class="text-success mb-1">Rent Time: ${orderItem.rentalDateNum} Days</span>
                                                        </c:when>
                                                        <c:when test="${orderItem.p.mode == 'Sell'}">

                                                        </c:when>
                                                    </c:choose>
                                                    <span class="text-success mb-1">Total price: ${orderItem.price}$</span>            
                                                    <span class="delivery-date text-muted">Order Date: ${orderItem.orderDate}</span>
                                                    <span class="delivery-date text-muted">Shipped Date: ${orderItem.shippedDate}</span>
                                                </div>
                                                <div class="item-options text-center">
                                                    <div class="item-wrapper">
                                                        <div class="item-cost">
                                                            <c:choose>
                                                                <c:when test="${orderItem.p.mode == 'Rental'}">
                                                                    <h4 class="item-price">${orderItem.p.price}$/Days</h4>
                                                                </c:when>
                                                                <c:when test="${orderItem.p.mode == 'Sell'}">
                                                                    <h4 class="item-price">${orderItem.p.price}$</h4>
                                                                </c:when>
                                                            </c:choose>


                                                            <c:choose>
                                                                <c:when test="${orderItem.status == 1}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-warning">PROCESSING</span>
                                                                    </p>
                                                                </c:when>
                                                                <c:when test="${orderItem.status == 2}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-primary">SHIPPING</span>
                                                                    </p>
                                                                </c:when>
                                                                <c:when test="${orderItem.status == 3}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-danger">STOP</span>
                                                                    </p>                                                           
                                                                </c:when>
                                                                <c:when test="${orderItem.status == 4}">
                                                                    <p class="card-text shipping">
                                                                        <span class="badge rounded-pill badge-light-success">COMPLETE</span>
                                                                    </p>                                                           </c:when>
                                                            </c:choose>
                                                        </div>
                                                    </div>
                                                    <button type="button" onclick="location.href = 'productDetailController?mx=${orderItem.p.productID}';" class="btn btn-light mt-1">
                                                        <span>VIEW PRODUCT</span>
                                                    </button>
                                                    <button type="button" class="btn btn-primary btn-cart move-cart">
                                                        <i data-feather="heart" class="align-middle me-25"></i>
                                                        <span class="text-truncate">ORDER AGAIN</span>
                                                    </button>
                                                </div><c:choose>
                                                    <c:when test="${orderItem.p.mode == 'Rental'}">
                                                        <div class="lidget-card-ecomerce checkout">
                                                            Rental Only
                                                        </div>                                                                
                                                    </c:when>
                                                    <c:when test="${orderItem.p.mode == 'Sell'}">

                                                    </c:when>
                                                </c:choose>

                                            </div>
                                        </c:forEach>
                                    </div>
                                    <!-- Checkout Place Order Left ends -->

                                    <!-- Checkout Place Order Right starts -->
                                    <div class="checkout-options">
                                        <div class="card">
                                            <div class="card-body">
                                                
                                                <hr />
                                                <div class="price-details">
                                                    <h6 class="price-title">List Order Price</h6>
                                                    <ul class="list-unstyled">
                                                        <li class="price-detail">
                                                            <div class="detail-title">Total price</div>
                                                            <div class="detail-amt">$${sessionScope.totalPrice}</div>
                                                        </li>
                                                        <li class="price-detail">
                                                            <div class="detail-title">Total Paid</div>
                                                            <div class="detail-amt discount-amt text-success">-${sessionScope.hasPaid}$</div>
                                                        </li>
                                                        <li class="price-detail">
                                                            <div class="detail-title">Tax</div>
                                                            <div class="detail-amt">$${sessionScope.totalTax}</div>
                                                        </li>

                                                        <li class="price-detail">
                                                            <div class="detail-title">Delivery Charges</div>
                                                            <div class="detail-amt discount-amt text-success">Free</div>
                                                        </li>
                                                    </ul>
                                                    <hr />
                                                    <ul class="list-unstyled">
                                                        <li class="price-detail">
                                                            <div class="detail-title detail-total">Total Must Pay</div>
                                                            <div class="detail-amt fw-bolder">$${sessionScope.total}</div>
                                                        </li>
                                                    </ul>
                                                    <button type="button" class="btn btn-primary w-100 ">Pay Now</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Checkout Place Order Right ends -->
                                    </div>
                                </div>
                            </div>
                            <div class="customer-card">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">${sessionScope.o.customerFirstName} ${sessionScope.o.customerLastName}</h4>
                                    </div>
                                    <div class="card-body actions">
                                        <p class="card-text mb-0">${sessionScope.o.customerAddress}</p>
                                        <p class="card-text">${sessionScope.o.customerPhoneNum}</p>
                                        <p class="card-text">${sessionScope.o.customerEmail}</p>
                                        <button type="button" onclick="location.href = 'userProfileController';" class="btn btn-primary w-100 btn-next delivery-address mt-2">
                                            Change your information
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Checkout Payment Ends -->
                        <!-- </div> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Customizer-->
    <div class="customizer d-none d-md-block"><a
            class="customizer-toggle d-flex align-items-center justify-content-center" href="#"><i class="spinner"
                                                                                               data-feather="settings"></i></a>
        <div class="customizer-content">
            <!-- Customizer header -->
            <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                <h4 class="mb-0">Theme Customizer</h4>
                <p class="m-0">Customize & Preview in Real Time</p>

                <a class="customizer-close" href="#"><i data-feather="x"></i></a>
            </div>

            <hr />

            <!-- Styling & Text Direction -->
            <div class="customizer-styling-direction px-2">
                <p class="fw-bold">Skin</p>
                <div class="d-flex">
                    <div class="form-check me-1">
                        <input type="radio" id="skinlight" name="skinradio" class="form-check-input layout-name" checked
                               data-layout="" />
                        <label class="form-check-label" for="skinlight">Light</label>
                    </div>
                    <div class="form-check me-1">
                        <input type="radio" id="skinbordered" name="skinradio" class="form-check-input layout-name"
                               data-layout="bordered-layout" />
                        <label class="form-check-label" for="skinbordered">Bordered</label>
                    </div>
                    <div class="form-check me-1">
                        <input type="radio" id="skindark" name="skinradio" class="form-check-input layout-name"
                               data-layout="dark-layout" />
                        <label class="form-check-label" for="skindark">Dark</label>
                    </div>
                    <div class="form-check">
                        <input type="radio" id="skinsemidark" name="skinradio" class="form-check-input layout-name"
                               data-layout="semi-dark-layout" />
                        <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                    </div>
                </div>
            </div>

            <hr />

            <!-- Menu -->
            <div class="customizer-menu px-2">
                <div id="customizer-menu-collapsible" class="d-flex">
                    <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                    <div class="form-check form-check-primary form-switch">
                        <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                        <label class="form-check-label" for="collapse-sidebar-switch"></label>
                    </div>
                </div>
            </div>
            <hr />

            <!-- Layout Width -->
            <div class="customizer-footer px-2">
                <p class="fw-bold">Layout Width</p>
                <div class="d-flex">
                    <div class="form-check me-1">
                        <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                        <label class="form-check-label" for="layout-width-full">Full Width</label>
                    </div>
                    <div class="form-check me-1">
                        <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                        <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                    </div>
                </div>
            </div>
            <hr />

            <!-- Navbar -->
            <div class="customizer-navbar px-2">
                <div id="customizer-navbar-colors">
                    <p class="fw-bold">Navbar Color</p>
                    <ul class="list-inline unstyled-list">
                        <li class="color-box bg-white border selected" data-navbar-default=""></li>
                        <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                        <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                        <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                        <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                        <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                        <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                        <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                    </ul>
                </div>

                <p class="navbar-type-text fw-bold">Navbar Type</p>
                <div class="d-flex">
                    <div class="form-check me-1">
                        <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                        <label class="form-check-label" for="nav-type-floating">Floating</label>
                    </div>
                    <div class="form-check me-1">
                        <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                        <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                    </div>
                    <div class="form-check me-1">
                        <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                        <label class="form-check-label" for="nav-type-static">Static</label>
                    </div>
                    <div class="form-check">
                        <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                        <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                    </div>
                </div>
            </div>
            <hr />

            <!-- Footer -->
            <div class="customizer-footer px-2">
                <p class="fw-bold">Footer Type</p>
                <div class="d-flex">
                    <div class="form-check me-1">
                        <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                        <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                    </div>
                    <div class="form-check me-1">
                        <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                        <label class="form-check-label" for="footer-type-static">Static</label>
                    </div>
                    <div class="form-check me-1">
                        <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                        <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End: Customizer-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-md-start d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2021<a
                    class="ms-25" href="https://1.envato.market/pixinvent_portfolio" target="_blank">Hilfsmotor</a><span
                    class="d-none d-sm-inline-block">, All rights Reserved</span></span><span
                class="float-md-end d-none d-md-block">Hand-crafted & Made with<i data-feather="heart"></i></span></p>
    </footer>
    <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/forms/wizard/bs-stepper.min.js"></script>
    <script src="app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
    <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.min.js"></script>
    <script src="app-assets/js/core/app.min.js"></script>
    <script src="app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="app-assets/js/scripts/pages/app-ecommerce-checkout.min.js"></script>
    <script src="assets/js/notify_manager.js"></script>
    <!-- END: Page JS-->

    <script>
                                            $(window).on('load', function () {
                                                if (feather) {
                                                    feather.replace({width: 14, height: 14});
                                                }
                                                $.ajax({
                                                    url: "/Rentabike/notifyController",
                                                    type: "post",
                                                    data: {
                                                        mode: "showNotifyOnly"
                                                    },
                                                    success: function (response) {
                                                        $("#notify").html(response);
                                                    }
                                                });
                                            })
    </script>
</body>
<!-- END: Body-->

</html>