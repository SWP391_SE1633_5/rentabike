<%-- 
    Document   : app-product-details
    Created on : Sep 26, 2022, 8:36:43 PM
    Author     : ADMIN
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/swiper.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <!-- END: Vendor CSS-->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <!--using-Font-Awesome-------------------->
        <script src="https://kit.fontawesome.com/c8e4d183c2.js" crossorigin="anonymous"></script>
        <!-- BEGIN: Page CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/app-ecommerce-details.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-number-input.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <!-- END: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/stylesheet_tag.css">

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="vertical-layout vertical-menu-modern  navbar-floating footer-static   menu-collapsed" data-open="click"
          data-menu="vertical-menu-modern" data-col="">

        <!-- BEGIN: Header-->
        <nav
            class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Contact"><i class="ficon" data-feather="mail"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Help"><i class="ficon" data-feather="help-circle"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning"
                                                                                                    data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="#"
                                                                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"
                                                                                                        data-language="en"><i class="flag-icon flag-icon-us"></i> English</a>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon"
                                                                                                 data-feather="moon"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon"
                                                                                           data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Hilfsmotor..." tabindex="-1"
                                   data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-cart me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i
                                class="ficon" data-feather="shopping-cart"></i><span
                                class="badge rounded-pill bg-primary badge-up cart-item-count">6</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">My Cart</h4>
                                    <div class="badge rounded-pill badge-light-primary">4 Items</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list">
                                <div class="list-item align-items-center"><img class="d-block rounded me-1"
                                                                               src="app-assets/images/pages/eCommerce/1.png" alt="donuts" width="62">
                                    <div class="list-item-body flex-grow-1">
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> Honda MX-219
                                                    2019</a>
                                            </h6>
                                            <div class="flex-default" style="column-gap:0.5rem">
                                                <div class="text-body badge rounded-pill badge-light-primary">Honda</div>
                                                <div class="text-body badge rounded-pill badge-light-primary">Chopper Version</div>
                                                <div class="text-body badge rounded-pill badge-light-primary">Rental Option</div>
                                            </div>
                                            <div class="flex-default" style="column-gap:0.5rem">
                                                <div class="text-body badge rounded-pill badge-light-success mt-1">Quantity: 1</div>
                                                <div class="text-body badge rounded-pill badge-light-danger mt-1">Tourist: None</div>
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$374.90</h5>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-menu-footer">
                                <div class="d-flex justify-content-between mb-1">
                                    <h6 class="fw-bolder mb-0">Total:</h6>
                                    <h6 class="text-primary fw-bolder mb-0">$10,999.00</h6>
                                </div><a class="btn btn-primary w-100" href="app-ecommerce-checkout.html">Checkout</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#"
                                                                                 data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span
                                class="badge rounded-pill bg-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-15.jpg"
                                                                     alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small
                                                class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar"
                                                                     width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small
                                                class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small
                                                class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="list-item d-flex align-items-center">
                                    <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                    <div class="form-check form-check-primary form-switch">
                                        <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="form-check-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small
                                                class="notification-text"> USA Server is down due to high CPU usage</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small
                                                class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small
                                                class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle dropdown-user-link"
                           id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none lidget-naz" current-login="${user.getEmail()}">
                                <span class="user-name fw-bolder">${user.getFirstName()} ${user.getLastName()}</span>
                                <span class="user-status" style="margin-top:0.25rem">${user.getRole()}</span></div>
                            <span class="avatar">
                                <c:choose>
                                    <c:when test="${user.getAvatar() == null}">
                                        <c:set var="firstName" value="${user.getFirstName()}"/>
                                        <c:set var="lastName" value="${user.getLastName()}"/>
                                        <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                            <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                        </div>
                                    </c:when>
                                    <c:when test="${user.getAvatar() != null}">
                                        <img class="round" src="${user.getAvatar()}" alt="avatar" height="40"
                                             width="40">   
                                    </c:when>
                                </c:choose> 
                                <span class="avatar-status-online"></span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="page-profile.jsp"><i class="me-50" data-feather="user"></i> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="page-account-settings-account.jsp"><i
                                    class="me-50" data-feather="settings"></i> Settings
                            </a>
                            <a class="dropdown-item" href="page-faq.jsp"><i class="me-50" data-feather="help-circle"></i> FAQ
                            </a>
                            <a class="dropdown-item" href="logoutController">
                                <i class="me-50" data-feather="power"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd
                                Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small>
                </a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div>
                </a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a
                    class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No
                            results found.</span></div>
                </a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item me-auto">
                        <a class="navbar-brand position-relative" style="top:-25px"
                           href="productListController">
                            <span>
                                <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo position-relative"
                                     style="left:-18px">
                            </span>
                            <h2 class="brand-text mb-0" style="position: relative; right: 35px;">Hilfsmotor</h2>
                        </a>
                    </li>
                    <li class="nav-item nav-toggle lidget-position-control"><a class="nav-link modern-nav-toggle pe-0"
                                                                               data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4"
                                                     data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary"
                                                     data-feather="disc" data-ticon="disc"></i></a></li>
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content">
                <ul class="navigation navigation-main mt-1" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Apps &amp; Pages</span><i
                            data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="app-email.html"><i
                                data-feather="mail"></i><span class="menu-title text-truncate" data-i18n="Email">Email</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="shopping-cart"></i><span
                                class="menu-title text-truncate" data-i18n="eCommerce">Product</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="productListController"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Shop">Shop</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="wishListController">
                                    <i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Wish List">Wish
                                        List</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="app-ecommerce-checkout.html"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Checkout">Checkout</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="productController"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Product Management">Product Management</span></a>
                            </li>
                            <c:choose>
                                <c:when test="${account.role == 'STAFF'}">
                            <li><a class="d-flex align-items-center" href="productController"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Manage Product">Manage Product</span></a>
                            </li>
                            </c:when>
                            </c:choose>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="user"></i><span
                                class="menu-title text-truncate" data-i18n="User">User</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="View">View</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="userProfileController"><span
                                                class="menu-item text-truncate" data-i18n="Account">Account</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-security.html"><span
                                                class="menu-item text-truncate" data-i18n="Security">Security</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-billing.html"><span
                                                class="menu-item text-truncate" data-i18n="Billing &amp; Plans">Billing &amp; Plans</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-notifications.html"><span
                                                class="menu-item text-truncate" data-i18n="Notifications">Notifications</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-connections.html"><span
                                                class="menu-item text-truncate" data-i18n="Connections">Connections</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span
                                class="menu-title text-truncate" data-i18n="Pages">Pages</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="help-page"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="FAQ">FAQ</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Blog">Blog</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="blog-list"><span
                                                class="menu-item text-truncate" data-i18n="List">List</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="manage-blog"><span
                                                class="menu-item text-truncate" data-i18n="Detail">Add</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="blog-draft"><span
                                                class="menu-item text-truncate" data-i18n="Detail">Draft</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="blog-save"><span
                                                class="menu-item text-truncate" data-i18n="Edit">Saved</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" navigation-header"><span data-i18n="Misc">Misc</span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="menu"></i><span
                                class="menu-title text-truncate" data-i18n="Menu Levels">Menu Levels</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.1</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.2</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate"
                                                                                            data-i18n="Third Level">Third Level 3.1</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate"
                                                                                            data-i18n="Third Level">Third Level 3.2</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content ecommerce-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <div class="row breadcrumbs-top">
                            <div class="col-12">
                                <h2 class="content-header-title float-start mb-0">Product Details</h2>
                                <div class="breadcrumb-wrapper">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="productListController">Home</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#">Product</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="productListController">Shop</a>
                                        </li>
                                        <li class="breadcrumb-item active">Details
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                        <div class="mb-1 breadcrumb-right">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        data-feather="grid"></i></button>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <a class="dropdown-item" href="app-email.html">
                                        <i class="me-1" data-feather="mail"></i>
                                        <span class="align-middle">Contact</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <!-- app e-commerce details start -->
                    <section class="app-ecommerce-details">
                        <div class="card">
                            <!-- Product Details starts -->
                            <div class="card-body">
                                <div class="row my-2">
                                    <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                                        <div class="d-flex align-items-center justify-content-center">
                                            <img src="${product.getImage()}" class="img-fluid product-img" style="border-radius:8px;"
                                                 alt="product image" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <h4>${product.getName()} ${product.getModelYear()} (${product.getCategory()} Version)</h4>
                                        <div class="text-muted">Product ID: MX-00${product.getProductID()}</div>
                                        <span class="card-text item-company">By <a href="#" class="company-name">${product.getBrand()}</a></span>
                                        <div class="ecommerce-details-price d-flex flex-wrap mt-1">
                                            <c:choose>
                                                <c:when test="${product.getModeRental() == 'DAY'}">
                                                    <h4 class="item-price me-1">$${product.getPrice()}/Day</h4>
                                                </c:when>
                                                <c:when test="${product.getModeRental() == 'WEEK'}">
                                                    <h4 class="item-price me-1">$${product.getPrice()}/Week</h4>
                                                </c:when>
                                                <c:when test="${product.getModeRental() == 'MONTH'}">
                                                    <h4 class="item-price me-1">$${product.getPrice()}/Month</h4>
                                                </c:when>
                                                <c:when test="${product.getModeRental() == 'YEAR'}">
                                                    <h4 class="item-price me-1">$${product.getPrice()}/Year</h4>
                                                </c:when>
                                                <c:otherwise>
                                                    <h4 class="item-price me-1">$${product.getPrice()}</h4>
                                                </c:otherwise>
                                            </c:choose>
                                            <div class="item-rating flex-default flex-center-align" style="column-gap:0.5rem">
                                                <ul class="unstyled-list list-inline">
                                                    <c:choose>
                                                        <c:when test="${product.getAverageRating() == '5'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            </c:when>
                                                            <c:when test="${product.getAverageRating() == '4'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </c:when>
                                                            <c:when test="${product.getAverageRating() == '3'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </c:when>
                                                            <c:when test="${product.getAverageRating() == '2'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </c:when>
                                                            <c:when test="${product.getAverageRating() == '1'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>      
                                                            </c:otherwise>
                                                        </c:choose>
                                                </ul>
                                                <span class="card-text mb-1">(${product.getTotalRating()} Reviews)</span>
                                            </div>
                                        </div>
                                        <div class="flex-default mb-1" style="column-gap:0.5rem">
                                            <div class="text-body badge rounded-pill badge-light-primary">${stockTotal} Available</div>
                                        </div>
                                        <c:choose>
                                            <c:when test="${stockTotal == 0}">
                                                <p class="card-text">Unavailable - <span class="text-warning">Out stock</span></p>
                                            </c:when>
                                            <c:otherwise>
                                                <p class="card-text">Available - <span class="text-success">In stock</span></p>
                                            </c:otherwise>
                                        </c:choose>
                                        <p class="card-text">
                                            ${product.getDescription()}
                                        </p>
                                        <ul class="product-features list-unstyled">
                                            <li><i data-feather="shopping-cart"></i>
                                                <span>Shipping to:</span>
                                                <span class="text-body" style="font-weight:400;margin-left: 0.5rem;">${user.getStreet()}, ${user.getCity()}, ${user.getState()}</span>
                                            </li>
                                            <li>
                                                <i data-feather="dollar-sign"></i>
                                                <span>EMI options available</span>
                                            </li>
                                        </ul>
                                        <div class="item-quantity flex-default flex-center-align">
                                            <span class="quantity-title">Qty:</span>
                                            <div class="lidget-quantity-handle">
                                                <div class="cart-item-qty">
                                                    <div class="input-group">
                                                        <input class="touchspin-cart" id="touchspin-product" type="number" value="1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr/>
                                        <div class="d-flex flex-column flex-sm-row pt-1">
                                            <c:choose>
                                                <c:when test="${stockTotal == 0}">
                                                    <a href="#" class="btn btn-primary btn-cart me-0 me-sm-1 mb-1 mb-sm-0 disabled">
                                                        <i data-feather="shopping-cart" class="me-50"></i>
                                                        <span class="add-to-cart">Out of stock</span>
                                                    </a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="#" class="btn btn-primary btn-cart me-0 me-sm-1 mb-1 mb-sm-0">
                                                        <i data-feather="shopping-cart" class="me-50"></i>
                                                        <span class="add-to-cart">Add to cart</span>
                                                    </a>
                                                </c:otherwise>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${product.getMarked() != null}">
                                                    <a href="#" onclick="sendToWishList(this)" index-data="${product.getProductID()}"  class="btn btn-outline-secondary lidget-wishlist-btn btn-wishlist me-0 me-sm-1 mb-1 mb-sm-0">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart text-danger"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>                                                        <span>Wishlist</span>
                                                    </a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="#" onclick="sendToWishList(this)" index-data="${product.getProductID()}" class="btn btn-outline-secondary btn-wishlist me-0 me-sm-1 mb-1 mb-sm-0">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>                                                        <span>Wishlist</span>
                                                    </a>
                                                </c:otherwise>
                                            </c:choose>
                                            <button style="padding:0;"
                                                    class="btn btn-sm btn-icon waves-effect waves-float waves-light flex-default flex-center-align"
                                                    data-bs-toggle="modal" data-bs-target="#editPermissionModal">
                                                <a href="javascript:;" class="btn btn-outline-danger suspend-user waves-effect">Report this product</a>
                                            </button>
                                            <div class="btn-group dropdown-icon-wrapper btn-share" style="margin-left:0.5rem">
                                                <button type="button" class="btn btn-icon hide-arrow btn-outline-secondary dropdown-toggle"
                                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i data-feather="share-2"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-end">
                                                    <a href="#" class="dropdown-item">
                                                        <i data-feather="facebook"></i>
                                                    </a>
                                                    <a href="#" class="dropdown-item">
                                                        <i data-feather="twitter"></i>
                                                    </a>
                                                    <a href="#" class="dropdown-item">
                                                        <i data-feather="youtube"></i>
                                                    </a>
                                                    <a href="#" class="dropdown-item">
                                                        <i data-feather="instagram"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            &nbsp;
                                            &nbsp;
                                            <c:choose>
                                                <c:when test="${user.getRole()== 'CUSTOMER'}">

                                                </c:when>
                                                <c:when test="${user.getRole()== 'SALEPERSON'}">
                                                    <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#editUser" id="editUserButton" data-bs-toggle="modal">
                                                        Edit
                                                    </a>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Product Details ends -->

                            <!-- Item features starts -->
                            <div class="item-features">
                                <div class="col-xl-12 col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <ul class="nav nav-tabs justify-content-center" style="font-size:18px;margin-bottom:3rem;"
                                                role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="home-tab-center" data-bs-toggle="tab" href="#home-center"
                                                       aria-controls="home-center" role="tab" aria-selected="true">Storer</a>
                                                </li>
                                                <li class="nav-item">
                                                    <c:choose>
                                                        <c:when test="${product.getMode() == 'Rental'}">
                                                            <a class="nav-link" id="rental--tab-center" data-bs-toggle="tab"
                                                               href="#rental-center" aria-controls="rental--center" role="tab"
                                                               aria-selected="false">Rental</a>
                                                        </c:when>                        
                                                        <c:otherwise>
                                                            <a class="nav-link disabled">Rental</a>                                                            
                                                        </c:otherwise>
                                                    </c:choose>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="specifications--tab-center" data-bs-toggle="tab"
                                                       href="#specifications-center" aria-controls="specifications--center" role="tab"
                                                       aria-selected="false">Specifications</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="reviews--tab-center" data-bs-toggle="tab"
                                                       href="#reviews-center" aria-controls="reviews--center" role="tab"
                                                       aria-selected="false" class="nav-link ">Reviews</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home-center" aria-labelledby="home-tab-center" role="tabpanel">
                                                    <div class="row text-center">
                                                        <div class="col-12 col-md-12 mb-4 mb-md-0 flex-default flex-center-align">
                                                            <div class="w-75 mx-auto">
                                                                <img src="https://mdbcdn.b-cdn.net/img/new/avatars/1.webp" class="rounded-circle shadow-4"
                                                                     style="width: 80px" alt="Avatar" />
                                                                <div class="mt-1">
                                                                    <c:choose>
                                                                        <c:when test="${product.getStaffName() != null}">
                                                                            <h4>${product.getStaffName()}</h4>
                                                                            <span class="badge bg-light-secondary">Hilfsmotor</span>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <h4>${product.getStoreName()}</h4>
                                                                            <span class="badge bg-light-secondary">Store Owner</span>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </div>
                                                                <div class="flex-default mt-1" style="column-gap:0.5rem;justify-content: center;">
                                                                    <div class="text-body badge rounded-pill badge-light-primary">${reviewsStore} Reviews</div>
                                                                    <div class="text-body badge rounded-pill badge-light-primary">0% Response</div>
                                                                    <c:choose>
                                                                        <c:when test="${product.getStaffName() != null}">
                                                                            <div class="text-body badge rounded-pill badge-light-primary">Join at ${staffStore.getDateOfJoin()}</div>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div class="text-body badge rounded-pill badge-light-primary">Join at ${userStore.getDateOfJoin()}</div>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </div>
                                                                <div class="flex-default mt-1" style="column-gap:0.5rem;justify-content: center;">
                                                                    <div class="text-body badge rounded-pill badge-light-primary">${totalProductStore} Products</div>
                                                                    <div class="text-body badge rounded-pill badge-light-success">${sellTotal} Sell</div>
                                                                    <div class="text-body badge rounded-pill badge-light-warning">${rentalTotal} Rental</div>
                                                                </div>
                                                                <div class="mt-1">
                                                                    <span class="fw-bolder me-25">Status:</span>
                                                                    <c:choose>
                                                                        <c:when test="${product.getStaffName() != null}">
                                                                            <c:choose>
                                                                                <c:when test="${staffStore.getActive() == 1}">
                                                                                    <span class="badge bg-light-success">Active</span>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <span class="badge bg-light-danger">Inactive</span>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <c:choose>
                                                                                <c:when test="${userStore.getStatus() == 1}">
                                                                                    <span class="badge bg-light-success">Active</span>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <span class="badge bg-light-danger">Inactive</span>
                                                                                </c:otherwise>
                                                                            </c:choose>                                                                        
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </div>
                                                                <div class="flex-default mt-1" style="column-gap:0.5rem;justify-content: center;">
                                                                    <div class="mt-1">
                                                                        <a class="btn btn-primary me-1 waves-effect waves-float waves-light">
                                                                            View Shop
                                                                        </a>
                                                                    </div>
                                                                    <div class="mt-1">
                                                                        <a class="btn btn-outline-danger me-0 me-sm-1 mb-1 mb-sm-0 waves-effect"
                                                                           style="color: #EA5455;">
                                                                            <span>Contact me</span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="rental-center" aria-labelledby="specifications-center"
                                                     role="tabpanel">
                                                    <div class="col-12 col-md-12 mb-4 mb-1">
                                                        <div class="w-75 mx-auto">
                                                            <div class="flex-default flex-space-center">
                                                                <div class="d-flex column-gap-default">
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/deposit-rental.webp" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Deposit</h4>
                                                                        <p class="card-text">${product.getDeposit()}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex column-gap-default" style="text-align:right;">
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Required Docs</h4>
                                                                        <p class="card-text">${product.getRequiredDocs()}</p>
                                                                    </div>
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/document.png" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-12 mb-4 mb-1">
                                                        <div class="w-75 mx-auto">
                                                            <div class="flex-default flex-space-center">
                                                                <div class="d-flex column-gap-default">
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/info-2.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Mileage Limit</h4>
                                                                        <p class="card-text">${product.getMiliageLimit()}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex column-gap-default" style="text-align:right;">
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Minimum Rental</h4>
                                                                        <p class="card-text">${product.getMinimumRental()}</p>
                                                                    </div>
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/info-2.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-12 mb-4 mb-1">
                                                        <div class="w-75 mx-auto">
                                                            <div class="flex-default flex-space-center">
                                                                <div class="d-flex column-gap-default">
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/virtual-tour.png" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Touring</h4>
                                                                        <c:choose>
                                                                            <c:when test="${product.getTouringMode() == 0}">
                                                                                <p class="card-text">Allow</p>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <p class="card-text">Not allow</p>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="specifications-center" aria-labelledby="specifications-center"
                                                     role="tabpanel">
                                                    <div class="col-12 col-md-12 mb-4 mb-1">
                                                        <div class="w-75 mx-auto">
                                                            <div class="flex-default flex-space-center">
                                                                <div class="d-flex column-gap-default">
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/info-3.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Category</h4>
                                                                        <p class="card-text">${product.getCategory()}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex column-gap-default" style="text-align:right;">
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Transmission</h4>
                                                                        <p class="card-text">${product.getTransmission()}</p>
                                                                    </div>
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/specifications-7.png" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-12 mb-4 mb-1">
                                                        <div class="w-75 mx-auto">
                                                            <div class="flex-default flex-space-center">
                                                                <div class="d-flex column-gap-default">
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/about-1.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Starter</h4>
                                                                        <p class="card-text">${product.getStarter()}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex column-gap-default" style="text-align:right;">
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Fuel System</h4>
                                                                        <p class="card-text">${product.getFuelSystem()}</p>
                                                                    </div>
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/about-3.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-12 mb-4 mb-1">
                                                        <div class="w-75 mx-auto">
                                                            <div class="flex-default flex-space-center">
                                                                <div class="d-flex column-gap-default">
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/about-3.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Fuel Capacity</h4>
                                                                        <p class="card-text">${product.getFuelCapacity()} gallons</p>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex column-gap-default" style="text-align:right;">
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Dry Weight</h4>
                                                                        <p class="card-text">${product.getDryWeight()} pounds</p>
                                                                    </div>
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/info-2.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-12 mb-4 mb-1">
                                                        <div class="w-75 mx-auto">
                                                            <div class="flex-default flex-space-center">
                                                                <div class="d-flex column-gap-default">
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/about-4.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Seat Height</h4>
                                                                        <p class="card-text">${product.getSeatHeight()} inches</p>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex column-gap-default" style="text-align:right;">
                                                                    <div class="transaction-percentage">
                                                                        <h4 style="white-space: nowrap;">Displacement</h4>
                                                                        <p class="card-text">${product.getDisplacement()}cc</p>
                                                                    </div>
                                                                    <div class="avatar bg-light-primary rounded float-start avatar-lg">
                                                                        <div class="avatar-content">
                                                                            <img src="assets/img/icon/about-2.svg" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="reviews-center" aria-labelledby="reviews-center"
                                                     role="tabpanel">
                                                    <section id="testimonials">
                                                        <!--heading--->
                                                        <div class="testimonial-heading">
                                                            
                                                            <h1>List Review:</h1>
                                                        </div>


                                                        <!--testimonials-box-container------>
                                                        <div id="listReview" class="testimonial-box-container">

                                                            <c:forEach items="${sessionScope.reviewList}"  var="review">
                                                                <div class="testimonial-box">
                                                                    <!--top------------------------->
                                                                    <div class="box-top">
                                                                        <!--profile----->
                                                                        <div class="profile">
                                                                            <!--img---->
                                                                            <div class="profile-img">
                                                                                <img src="images/c-1.jpg" />
                                                                            </div>
                                                                            <!--name-and-username-->
                                                                            <div class="name-user">
                                                                                <strong>${review.authorName}</strong>
                                                                                <span>@${review.role}</span>
                                                                            </div>
                                                                        </div>
                                                                        <!--reviews------>
                                                                        <div class="reviews">
                                                                            <c:forEach begin="${1}" end="${review.ratingStar}" var = "i">
                                                                                <i class="fas fa-star"></i>
                                                                            </c:forEach>
                                                                            <c:forEach begin="${1}" end="${5 - review.ratingStar}" var = "i">
                                                                                <i class="far fa-star"></i>
                                                                            </c:forEach>

                                                                        </div>
                                                                    </div>
                                                                    <!--Comments---------------------------------------->
                                                                    <div class="client-comment">
                                                                        <p>${review.comment}</p>
                                                                    </div>
                                                                </div>
                                                            </c:forEach>

                                                        </div>
                                                    </section>

                                                    <div>
                                                        <!-- Your review -->
                                                        <div class="container">
                                                            <div class="rating-wrap">
                                                                <h2>Star Rating</h2>
                                                                <div class="center">
                                                                    <fieldset class="rating">
                                                                        <input type="radio" id="star5" name="rating" value="5"/><label for="star5" class="full" title="Awesome"></label>

                                                                        <input type="radio" id="star4" name="rating" value="4"/><label for="star4" class="full"></label>

                                                                        <input type="radio" id="star3" name="rating" value="3"/><label for="star3" class="full"></label>

                                                                        <input type="radio" id="star2" name="rating" value="2"/><label for="star2" class="full"></label>

                                                                        <input type="radio" id="star1" name="rating" value="1"/><label for="star1" class="full"></label>

                                                                    </fieldset>
                                                                </div>

                                                                <h4 id="rating-value" name="rating-value"></h4>
                                                            </div>
                                                        </div>

                                                        <script src="app-assets/js/scripts/star-rating.js"></script>
                                                        <div class="md-form md-outline">
                                                            <textarea id="reviewField" name="reviewField" class="md-textarea form-control pr-6" rows="4"></textarea>
                                                            <label for="reviewField">Your review</label>
                                                        </div>
                                                        <div class="text-right pb-2">
                                                            <a onclick="addReview(${product.productID})" href="productDetailController?mx=${product.productID}">
                                                            <button type="button" class="btn btn-primary" >Add a review</button>
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Item features ends -->

                            <!-- Related Products starts -->
                            <div class="card-body">
                                <div class="mt-4 mb-2 text-center">
                                    <h4>Related Products</h4>
                                    <p class="card-text">People also search for this items</p>
                                </div>
                                <div class="swiper-responsive-breakpoints swiper-container px-3 py-2">
                                    <div class="swiper-wrapper">
                                        <c:forEach items="${sessionScope.relatedProductList}" var="related">
                                            <div class="swiper-slide position-relative">
                                                <div class="item-img text-center">
                                                    <a href="productDetailController?mx=${related.getProductID()}">
                                                        <img class="img-fluid card-img-top" src="${related.getImage()}"
                                                             alt="img-placeholder"></a>
                                                </div>
                                                <div class="card-body">
                                                    <div class="item-wrapper">
                                                        <div class="item-rating">
                                                            <ul class="unstyled-list list-inline">
                                                                <c:choose>
                                                                    <c:when test="${related.getAverageRating() == '5'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        </c:when>
                                                                        <c:when test="${related.getAverageRating() == '4'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        </c:when>
                                                                        <c:when test="${related.getAverageRating() == '3'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        </c:when>
                                                                        <c:when test="${related.getAverageRating() == '2'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        </c:when>
                                                                        <c:when test="${related.getAverageRating() == '1'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>      
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                            </ul>
                                                            <div class="card-text mb-1" style="margin-top:-0.5rem">(${related.getTotalRating()} Reviews)</div>
                                                        </div>
                                                    </div>
                                                    <h6 class="item-name mt-1">
                                                        <div class="text-muted" style="margin-bottom:0.5rem;">Product ID: MX-00${related.getProductID()}</div>
                                                        <a class="text-body mt-1" href="productDetailController?mx=${related.getProductID()}">${related.getName()} ${related.getModelYear()}</a>
                                                        <div class="card-text" style="margin-top:0.25rem">By 
                                                            <div class="company-name" style="display:inline-block">${related.getBrand()}</div>
                                                        </div>
                                                    </h6>
                                                    <div>
                                                        <c:choose>
                                                            <c:when test="${related.getModeRental() == 'DAY'}">
                                                                <h6 class="item-price mt-1">$${related.getPrice()}/Day</h6>
                                                            </c:when>
                                                            <c:when test="${related.getModeRental() == 'WEEK'}">
                                                                <h6 class="item-price mt-1">$${related.getPrice()}/Week</h6>
                                                            </c:when>
                                                            <c:when test="${related.getModeRental() == 'MONTH'}">
                                                                <h6 class="item-price mt-1">$${related.getPrice()}/Month</h6>
                                                            </c:when>
                                                            <c:when test="${related.getModeRental() == 'YEAR'}">
                                                                <h6 class="item-price mt-1">$${related.getPrice()}/Year</h6>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <h6 class="item-price mt-1">$${related.getPrice()}</h6>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                    <c:choose>
                                                        <c:when test="${related.getStaffName() != null}">
                                                            <div class="card-text" style="margin-top:0.25rem">Post By <span style="color: #7367F0;" class="company-name">${related.getStaffName()}</span></div>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <div class="card-text" style="margin-top:0.25rem">Post By <span style="color: #7367F0;" class="company-name">${related.getStoreName()}</span></div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                </div>
                                                <c:if test="${related.getMode() == 'Rental'}">
                                                    <div class="lidget-card-ecomerce">
                                                        Rental Only
                                                    </div>
                                                </c:if>
                                            </div>
                                        </c:forEach>
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                            </div>
                            <!-- Related Products ends -->
                            <!-- app e-commerce store related begin -->
                            <div class="item-features" style="padding-top:1rem;"></div>
                            <div class="card-body">
                                <div class="mt-4 mb-2 text-center">
                                    <h4>Related Shop Also</h4>
                                    <p class="card-text">Shop also post their new related product</p>
                                </div>
                                <div class="swiper-responsive-breakpoints swiper-container px-3 py-2">
                                    <div class="swiper-wrapper">
                                        <c:forEach items="${sessionScope.relatedProductStoreList}" var="relatedShop">
                                            <div class="swiper-slide position-relative">
                                                <div class="item-img text-center">
                                                    <a href="productDetailController?mx=${relatedShop.getProductID()}">
                                                        <img class="img-fluid card-img-top" src="${relatedShop.getImage()}"
                                                             alt="img-placeholder"></a>
                                                </div>
                                                <div class="card-body">
                                                    <div class="item-wrapper">
                                                        <div class="item-rating">
                                                            <ul class="unstyled-list list-inline">
                                                                <c:choose>
                                                                    <c:when test="${relatedShop.getAverageRating() == '5'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        </c:when>
                                                                        <c:when test="${relatedShop.getAverageRating() == '4'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        </c:when>
                                                                        <c:when test="${relatedShop.getAverageRating() == '3'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        </c:when>
                                                                        <c:when test="${relatedShop.getAverageRating() == '2'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        </c:when>
                                                                        <c:when test="${relatedShop.getAverageRating() == '1'}">
                                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>      
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                            </ul>
                                                            <div class="card-text mb-1" style="margin-top:-0.5rem">(${relatedShop.getTotalRating()} Reviews)</div>
                                                        </div>
                                                    </div>
                                                    <h6 class="item-name mt-1">
                                                        <div class="text-muted" style="margin-bottom:0.5rem;">Product ID: MX-00${relatedShop.getProductID()}</div>
                                                        <a class="text-body mt-1" href="productDetailController?mx=${relatedShop.getProductID()}">${relatedShop.getName()} ${related.getModelYear()}</a>
                                                        <div class="card-text" style="margin-top:0.25rem">By 
                                                            <div class="company-name" style="display:inline-block">${relatedShop.getBrand()}</div>
                                                        </div>
                                                    </h6>
                                                    <div>
                                                        <c:choose>
                                                            <c:when test="${relatedShop.getModeRental() == 'DAY'}">
                                                                <h6 class="item-price mt-1">$${relatedShop.getPrice()}/Day</h6>
                                                            </c:when>
                                                            <c:when test="${relatedShop.getModeRental() == 'WEEK'}">
                                                                <h6 class="item-price mt-1">$${relatedShop.getPrice()}/Week</h6>
                                                            </c:when>
                                                            <c:when test="${relatedShop.getModeRental() == 'MONTH'}">
                                                                <h6 class="item-price mt-1">$${relatedShop.getPrice()}/Month</h6>
                                                            </c:when>
                                                            <c:when test="${relatedShop.getModeRental() == 'YEAR'}">
                                                                <h6 class="item-price mt-1">$${relatedShop.getPrice()}/Year</h6>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <h6 class="item-price mt-1">$${relatedShop.getPrice()}</h6>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                    <c:choose>
                                                        <c:when test="${relatedShop.getStaffName() != null}">
                                                            <div class="card-text" style="margin-top:0.25rem">Post By <span style="color: #7367F0;" class="company-name">${relatedShop.getStaffName()}</span></div>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <div class="card-text" style="margin-top:0.25rem">Post By <span style="color: #7367F0;" class="company-name">${relatedShop.getStoreName()}</span></div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                </div>
                                                <c:if test="${relatedShop.getMode() == 'Rental'}">
                                                    <div class="lidget-card-ecomerce">
                                                        Rental Only
                                                    </div>
                                                </c:if>
                                            </div>
                                        </c:forEach>
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                            </div>
                            <!-- app e-commerce store related end -->
                        </div>
                    </section>
                    <!-- app e-commerce details end -->

                </div>
            </div>
        </div>
        <!-- END: Content-->
        <!-- Edit User Modal -->
        <div class="modal fade" id="editUser" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
                <div class="modal-content">
                    <div class="modal-header bg-transparent">
                        <button type="button" class="btn-close" id="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body pb-5 px-sm-5 pt-50" id="formEditProfile">
                        <div class="text-center mb-2">
                            <h1 class="mb-1">Edit Product Information</h1>
                            <p>Updating product details will receive a privacy audit.</p>
                        </div>
                        <form action="updateProduct?productID=${product.getProductID()}" id="editUserForm" class="row gy-1 pt-75" method="post">
                            <div class="d-flex align-items-center justify-content-center flex-direction-column">
                                <a href="#" class="me-25">
                                    <img class="img-fluid rounded mt-3 mb-2" src="${product.getImage()}" id="account-upload-img" style="margin-top:0rem !important;margin-left:1rem; width:300px;height:300px;" alt="avatar" height="300"
                                         width="300">   
                                </a>
                                <!-- upload and reset button -->
                                <div class="d-flex align-items-end mt-75 ms-1">
                                    <div>
                                        <label for="account-upload" class="btn btn-sm btn-primary mb-75 me-75 waves-effect waves-float waves-light width-full">Upload</label>
                                        <input type="file" id="account-upload" name="image" hidden="" accept="image/*">
                                        <button type="button" id="account-reset" class="btn btn-sm btn-outline-secondary mb-75 waves-effect width-full">Reset</button>
                                        <p class="mb-0">Allowed file types: png, jpg, jpeg.</p>
                                    </div>
                                </div>
                                <!--/ upload and reset button -->
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserFirstName">Product Name</label>
                                <input
                                    type="text"
                                    id="productName"
                                    name="productName"
                                    class="form-control"
                                    placeholder="John"
                                    value="${product.getName()}"
                                    data-msg="Please enter your first name"
                                    />
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserLastName">Price</label>
                                <input
                                    type="text"
                                    id="price"
                                    name="price"
                                    class="form-control"
                                    placeholder="Doe"
                                    value="${product.getPrice()}"
                                    data-msg="Please enter your last name"
                                    />
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserLastName">Brand</label>
                                <input
                                    type="text"
                                    id="brand"
                                    name="brand"
                                    class="form-control"
                                    placeholder="Doe"
                                    value="${product.getBrand()}"
                                    data-msg="Please enter your last name"
                                    />
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditCity">Category</label>
                                <input
                                    type="text"
                                    id="category"
                                    name="category"
                                    class="form-control modal-edit-tax-id"
                                    placeholder="Los Angeles"
                                    value="${product.getCategory()}"
                                    />
                            </div>
                            <div class="col-12 col-md-12">
                                <label class="form-label" for="modalEditStreet">Description</label>
                                <input
                                    type="text"
                                    id="description"
                                    name="description"
                                    class="form-control modal-edit-street"
                                    placeholder="Sanfoudry"
                                    value="${product.getDescription()}"
                                    />
                            </div>



                            <div class="col-12 text-center mt-2 pt-50">
                                <button type="submit" class="btn btn-primary me-1" id="editProfileUserSubmit">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                    Discard
                                </button>
                            </div>
                            <input type="hidden" name="avatarImage" class="hiddenI" value="">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Edit User Modal -->

        <!-- BEGIN: Customizer-->
        <div class="customizer d-none d-md-block"><a
                class="customizer-toggle d-flex align-items-center justify-content-center" href="#">
                <i class="spinner" data-feather="settings"></i></a>
            <div class="customizer-content">
                <!-- Customizer header -->
                <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                    <h4 class="mb-0">Theme Customizer</h4>
                    <p class="m-0">Customize & Preview in Real Time</p>

                    <a class="customizer-close" href="#"><i data-feather="x"></i></a>
                </div>

                <hr />

                <!-- Styling & Text Direction -->
                <div class="customizer-styling-direction px-2">
                    <p class="fw-bold">Skin</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="skinlight" name="skinradio" class="form-check-input layout-name" checked
                                   data-layout="" />
                            <label class="form-check-label" for="skinlight">Light</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skinbordered" name="skinradio" class="form-check-input layout-name"
                                   data-layout="bordered-layout" />
                            <label class="form-check-label" for="skinbordered">Bordered</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skindark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="dark-layout" />
                            <label class="form-check-label" for="skindark">Dark</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="skinsemidark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="semi-dark-layout" />
                            <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                        </div>
                    </div>
                </div>

                <hr />

                <!-- Menu -->
                <div class="customizer-menu px-2">
                    <div id="customizer-menu-collapsible" class="d-flex">
                        <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                        <div class="form-check form-check-primary form-switch">
                            <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                            <label class="form-check-label" for="collapse-sidebar-switch"></label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Layout Width -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Layout Width</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                            <label class="form-check-label" for="layout-width-full">Full Width</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Navbar -->
                <div class="customizer-navbar px-2">
                    <div id="customizer-navbar-colors">
                        <p class="fw-bold">Navbar Color</p>
                        <ul class="list-inline unstyled-list">
                            <li class="color-box bg-white border selected" data-navbar-default=""></li>
                            <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                            <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                            <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                            <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                            <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                            <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                            <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                        </ul>
                    </div>

                    <p class="navbar-type-text fw-bold">Navbar Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                            <label class="form-check-label" for="nav-type-floating">Floating</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-static">Static</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Footer -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Footer Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                            <label class="form-check-label" for="footer-type-static">Static</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End: Customizer-->

        <!-- Buynow Button-->
        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix mb-0">
                <span class="float-md-start d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2021<a
                        class="ms-25" href="https://1.envato.market/pixinvent_portfolio" target="_blank">Hilfsmotor</a><span
                        class="d-none d-sm-inline-block">, All rights Reserved</span></span>
            </p>
        </footer>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
        <!-- END: Footer-->
        <!<!-- Modal Report Product Begin -->

        <div class="modal fade show" id="editPermissionModal" tabindex="-1" aria-modal="true" role="dialog"
             style="display: none; padding-left: 0px;">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-transparent">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-3 pt-0">
                        <div class="text-center mb-2">
                            <h1 class="mb-1">Report Product</h1>
                            <div class="d-flex flex-column">
                                <a class="user_name text-body text-truncate">
                                    <span class="fw-bolder">${product.getName()}
                                    </span>
                                </a>
                                <small class="emp_post text-muted">MX-00${product.getProductID()}</small>
                            </div>
                        </div>

                        <div class="alert alert-warning" role="alert">
                            <h6 class="alert-heading">Warning!</h6>
                            <div class="alert-body">
                                By report this product, you have to wait in 24 hours so we can check if this product infringe then we will send a mail to you.
                                Be careful when you do this thing.
                            </div>
                        </div>

                        <form id="bannedUpdatePermission" class="row" onsubmit="return false">
                            <div class="col-sm-9">
                                <label class="form-label" for="bannedPermission">Message</label>
                                <input type="text" id="bannedPermission" name="bannedPermission" class="form-control"
                                       placeholder="Enter a message banned" tabindex="-1" data-msg="Please enter message banned">
                            </div>
                            <div class="col-sm-3 ps-sm-0">
                                <button type="submit" class="btn btn-danger mt-2 waves-effect waves-float waves-light">Report</button>
                            </div>
                            <div class="col-12 mt-75">
                                <label class="form-label" for="FilterTransaction">Reason</label><select
                                    id="FilterTransaction" class="form-select text-capitalize mb-md-0 mb-2xx">
                                    <option value=""> Select Your Reason </option>
                                    <option value="3" class="text-capitalize">The product violates community standards</option>
                                    <option value="7" class="text-capitalize">Product does not match the picture shown</option>
                                    <option value="14" class="text-capitalize">Scam</option>
                                    <option value="21" class="text-capitalize">Other reasons</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!<!-- Modal Report Product End -->
        <input type="hidden" name="avatarImage" class="hiddenI" value="">
        <!-- BEGIN: Vendor JS-->
        <script src="app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
        <script src="app-assets/vendors/js/extensions/swiper.min.js"></script>
        <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="app-assets/js/core/app-menu.min.js"></script>
        <script src="app-assets/js/core/app.min.js"></script>
        <script src="app-assets/js/scripts/customizer.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="app-assets/js/scripts/pages/app-ecommerce-details.min.js"></script>
        <script src="app-assets/js/scripts/forms/form-number-input.min.js"></script>
        <!-- END: Page JS-->
        <!-- BEGIN: Page JS-->
        <script src="app-assets/js/scripts/pages/page-account-settings-account.min.js"></script>
        <script src="assets/js/page-account-settings.js"></script>
        <!-- END: Page JS-->

        <!-- BEGIN: My JS-->
        <script id="auth-details" data-value="${stockTotal}" src="assets/js/auth_product_details.js"></script>
        <!-- END: My JS-->
        <script>
                            function addReview(pID) {
                                var cntReview = document.getElementById("reviewField").value;
                                var ratingStar = document.getElementById("rating-value").value;

                                $.ajax({
                                    url: "/Rentabike/addReview",
                                    type: "GET", //send it through get method
                                    data: {
                                        productID: pID,
                                        ratingStar: ratingStar,
                                        contentReview: cntReview
                                    },
                                    success: function (data) {
                                        var row = document.getElementById("testimonials");
                                        row.innerHTML = data;
                                    },
                                    error: function (xhr) {
                                        //Do Something to handle error
                                    }
                                });
                            }



                            $(window).on('load', function () {
                                if (feather) {
                                    feather.replace({width: 14, height: 14});
                                }
                            });
                            $(document).ready(function () {
                                var readURL = function (input) {
                                    if (input.files && input.files[0]) {
                                        var reader = new FileReader();
                                        var fileByteArray = [];
                                        reader.readAsArrayBuffer(input.files[0]);
                                        reader.onload = function (evt) {
                                            if (evt.target.readyState === FileReader.DONE) {
                                                var arrayBuffer = evt.target.result,
                                                        array = new Uint8Array(arrayBuffer);
                                                for (var i = 0; i < array.length; i++) {
                                                    fileByteArray.push(array[i]);
                                                }
                                                console.log(fileByteArray);
                                                document.querySelector(".hiddenI").value = fileByteArray;
                                            }
                                        };
                                    }
                                };
                                $("#account-upload").on('change', function () {
                                    readURL(this);
                                });
                            });
        </script>
    </body>
    <!-- END: Body-->

</html>