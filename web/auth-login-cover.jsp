<%-- 
    Document   : auth-login-cover
    Created on : Sep 20, 2022, 7:23:11 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">


        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-validation.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/authentication.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="horizontal-layout horizontal-menu blank-page navbar-floating footer-static" data-open="hover"
          data-menu="horizontal-menu" data-col="blank-page">
        <!-- BEGIN: Content-->
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <div class="auth-wrapper auth-cover">
                        <div class="auth-inner row m-0">
                            <!-- Brand logo-->
                            <a class="brand-logo flex-center-align" style="cursor: default;">
                                <img src="app-assets/images/ico/hilf.png" alt="" class="position-relative lidget-login__logo">
                                <h2 class="lidget-login__logo-text brand-text text-primary ms-1">Hilfsmotor</h2>
                            </a>
                            <!-- /Brand logo-->
                            <!-- Left Text-->
                            <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
                                <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                                    <img class="img-fluid" src="app-assets/images/pages/login-v2-dark.svg" alt="Login V2" /></div>
                            </div>
                            <!-- /Left Text-->
                            <!-- Login-->
                            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                                    <h2 class="card-title fw-bold mb-1">Welcome to Hilfsmotor Administrator! 👋</h2>
                                    <p class="card-text mb-2">Please enter your login details to continue our adventures</p>
                                    <form class="auth-login-form mt-2" id="lidget-login__form">
                                        <div class="mb-1">
                                            <label class="form-label" for="login-email">Email</label>
                                            <input class="form-control lidget-login__input" id="login-email" type="text" name="login-email"
                                                   placeholder="nicolasjensen@gmail.com" value="${staff.getEmail()}" aria-describedby="login-email" autofocus="" tabindex="1" />
                                        </div>
                                        <div class="mb-1">
                                            <div class="d-flex justify-content-between">
                                                <label class="form-label" for="login-password">Password</label><a
                                                    href="auth-forgot-password-cover.html"><small>Forgot Password?</small></a>
                                            </div>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input class="form-control form-control-merge lidget-login__input" id="login-password" type="password"
                                                       name="login-password" placeholder="············" aria-describedby="login-password"
                                                       tabindex="2"/><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                            </div>
                                        </div>
                                        <div class="mb-1">
                                            <div class="alert alert-warning lidget-login__error" role="alert">
                                                <h6 class="alert-heading">Warning!</h6>
                                                <div class="alert-body">
                                                    Your login details are incorrect.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-1 lidget-login__transition">
                                            <div class="form-check">
                                                <input class="form-check-input" id="remember-me" type="checkbox" tabindex="3" />
                                                <label class="form-check-label" for="remember-me"> Remember Me</label>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary w-100" tabindex="4">Sign in</button>
                                    </form>
                                    <p class="text-center mt-2"><span>Are you a new member of our team?</span><a
                                            href="auth-register-multisteps.html"><span>&nbsp;Create an account</span></a></p>
                                </div>
                            </div>
                            <!-- /Login-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content-->

        <div class="status-login" data="${statusRegister}"></div>
        <%session.removeAttribute("statusRegister");%>
        <!-- BEGIN: Vendor JS-->
        <script src="app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
        <script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="app-assets/js/core/app-menu.min.js"></script>
        <script src="app-assets/js/core/app.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="app-assets/js/scripts/pages/auth-login.js"></script>
        <!-- END: Page JS-->

        <!-- BEGIN: My JS-->
        <script src="assets/js/Admin.js"></script>
        <script src="assets/js/auth-admin-login.js"></script>
        <!-- END: My JS-->

        <script>
            $(window).on('load', function () {
                if (feather) {
                    feather.replace({width: 14, height: 14});
                }
            })
        </script>
    </body>
    <!-- END: Body-->

</html>