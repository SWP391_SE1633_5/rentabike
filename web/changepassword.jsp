<%-- 
    Document   : changepassword
    Created on : Sep 16, 2022, 10:53:47 AM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Change account password</title>
        <link rel="stylesheet" href="assets/css/style.css">
    </head>

    <body>
        <div class="main">
            <div class="changed-page">
                <div class="changed-page__content flex-default flex-direction-column flex-center row-gap-default"
                     modifier="yes">
                    <img src="assets/img/RentabikeLogo.jpg" alt="" class="changed-page__image-logo">
                    <div class="changed-page__introduced">Hilfsmotor</div>
                    <div class="changed-page__introduced-slogan">Take your dream and ride to your future</div>
                </div>
                <%if(session.getAttribute("changed") == "process"){%>
                <div class="changed-page__content" type="first-child">
                    <img src="assets/img/RentabikeLogo.jpg" alt="" class="changed-page__logo-image">
                    <div class="changed-page__title">
                        Change account password<br>
                    </div>
                    <div class="changed-page__input-field">
                        <label for="changed-page__input-password">
                            <div class="changed-page__title-input cursor-select-pointer" title="password">
                                Password</div>
                        </label>
                        <input type="password" name="password" role="password" id="changed-page__input-password"
                               class="changed-page__input" placeholder="New password">
                    </div>
                    <div class="changed-page__message" mode="success">
                        Congratulation, you have changed your account.<br> 
                        Please comeback to login to enter your information. 
                    </div>
                    <div class="changed-page__action induct-page__btn home-page__introduce-option position-relative"
                         type="wb">
                        <a class="changed-page__action-link induct-page__link home-page__introduce-link position-relative"
                           type="disabled">
                            <span>CHANGE ACCOUNT PASSWORD</span>
                            <div class="changed-page__icon-loading">
                                <div class="loading-screen__icon">
                                    <div class="snippet" data-title=".dot-windmill">
                                        <div class="stage">
                                            <div class="dot-windmill"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="changed-page__action-color induct-page__seperate home-page__introduce-seperate"
                             type="disabled"></div>
                    </div>
                </div>
                <%}else{%>
                <div class="changed-page__content">
                    <img src="assets/img/handDrawn/blacklines.jpg" alt="" class="changed-page__image">
                    <div class="changed-page__message">
                        Sorry, but your message is expired.<br> 
                        Please comeback to check your verify in profile user again. 
                    </div>
                    <div class="changed-page__action induct-page__btn home-page__introduce-option position-relative" type="wb">
                        <a href="started.jsp" class="induct-page__link home-page__introduce-link position-relative">BACK TO SIGN IN</a>
                        <div class="induct-page__seperate home-page__introduce-seperate"></div>
                    </div>
                </div> 
                <div class="footer-page__credit changed-page__credit">
                    <div class="footer-page__credit-text">(c) 2022 Hilfsmotor - Motorcycles Dealer Template. All rights
                        reserved.</div>
                </div>
                <%}%>
            </div>
        </div>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/changed.js"></script>
    </body>

</html>