<%-- 
    Document   : homepageC
    Created on : Sep 11, 2022, 9:10:27 PM
    Author     : ADMIN
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/nouislider.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/css/plugins/extensions/ext-component-sliders.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/app-ecommerce.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <!-- END: Custom CSS-->

    </head>

    <body>
        <div class="main ecommerce-application">
            <section class="home-page position-relative">
                <header class="home-page__header position-relative">
                    <div class="home-page__header-navbar flex-default flex-space-center position-relative">
                        <div class="home-page__navbar-content flex-default">
                            <a href="HelpPageController">
                                <div class="home-page__navbar-item cursor-select-pointer position-relative">
                                    <div class="home-page__navbar-name" type="roleStarted">Need Help?</div>
                                    <div class="home-page__navbar-seperate"></div>
                                </div>
                            </a>
                            <div class="home-page__navbar-item cursor-select-pointer position-relative">
                                <div class="home-page__navbar-name" type="roleStarted">Shop</div>
                                <div class="home-page__navbar-seperate"></div>
                            </div>
                            <div class="home-page__navbar-item cursor-select-pointer position-relative">
                                <div class="home-page__navbar-name" type="roleStarted">Blog</div>
                                <div class="home-page__navbar-seperate"></div>
                            </div>
                        </div>
                        <div class="home-page__navbar-content flex-default">
                            <div class="home-page__navbar-item cursor-select-pointer position-relative">
                                <div class="home-page__navbar-name" type="roleStarted">Rentals</div>
                                <div class="home-page__navbar-seperate"></div>
                            </div>
                            <div class="home-page__navbar-item cursor-select-pointer position-relative">
                                <div class="home-page__navbar-name" type="roleStarted">Contacts</div>
                                <div class="home-page__navbar-seperate"></div>
                            </div> 
                            <a href="started.jsp">
                                <div class="home-page__navbar-item cursor-select-pointer position-relative" role="getStarted">
                                    <div class="home-page__navbar-name">Get Started</div>
                                    <div class="home-page__navbar-seperate"></div>
                                </div>           
                            </a>

                        </div>
                        <div class="home-page__navbar-main-title cursor-select-pointer">
                            <a href="homepageCController">
                                <img src="app-assets/images/ico/hilf.png" alt="" class="home-page__introduce-img">
                            </a>
                        </div>
                    </div>
                </header>
                <div class="home-page__introduce">
                    <div class="home-page__introduce-main-title">
                        <h1 class="home-page__introduce-header">
                            Hilfsmotor
                        </h1>
                    </div>
                    <div class="home-page__introduce-sub-title">
                        Welcome to Hilfsmotor, feel free to find your best motorbike rental with easy way for your real
                        adventure.
                    </div>
                    <div class="home-page__introduce-action flex-default flex-center">
                        <div class="home-page__introduce-option position-relative">
                            <a href="#" class="home-page__introduce-link position-relative">SEE OUR SHOP</a>
                            <div class="home-page__introduce-seperate"></div>
                        </div>
                        <div class="home-page__introduce-option position-relative" type="wb">
                            <a href="#" class="home-page__introduce-link position-relative">HIRE A BIKE</a>
                            <div class="home-page__introduce-seperate"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="induct-page" mode="slider-content">
                <slider class="induct-page__main grid-default grid-two-column-center">
                    <div class="induct-page__slider-illustration">
                        <img src="assets/img/slider/slider05.jpg" alt="" class="induct-page__slider-image">
                    </div>
                    <div class="induct-page__content">
                        <div class="induct-page__title">What is a Hilfsmotor?</div>
                        <div class="induct-page__descripton">Donec rutrum in justo eget. In porta nisi fringilla arcu, ac
                            vulputate dui tempor et.</div>
                        <div class="induct-page__summary">Quis lectus nulla at volutpat diam ut. Mollis aliquam ut porttitor
                            leo a diam, suspendisse, potenti nullam ac tortor vitae purus faucibus ornare. Curabitur vitae
                            nunc sed velit dignissim sodales ut. Quam nulla porttitor massa id neque aliquam. Id leo in
                            vitae turpis massa sed diam vel quam elementum cursus in.
                            Volutpat maecenas volutpat. Iaculis at erat pellentesque adipiscing. Praesent tristique magna
                            sit amet purus gravida quis blandit turpis egestas pretium.</div>
                        <div class="induct-page__btn home-page__introduce-option position-relative" type="wb">
                            <a href="#" class="induct-page__link home-page__introduce-link position-relative">READ MORE</a>
                            <div class="induct-page__seperate home-page__introduce-seperate"></div>
                        </div>
                    </div>
                </slider>
            </section>
            <section class="induct-page" mode="slider-content">
                <slider class="induct-page__main grid-default grid-two-column-center">
                    <div class="induct-page__content">
                        <div class="induct-page__title">A greate performance that matters in future</div>
                        <div class="induct-page__descripton">Donec rutrum in justo eget. In porta nisi fringilla arcu, ac
                            vulputate dui tempor et.</div>
                        <div class="induct-page__summary">Quis lectus nulla at volutpat diam ut. Mollis aliquam ut porttitor
                            leo a diam, suspendisse, potenti nullam ac tortor vitae purus faucibus ornare. Curabitur vitae
                            nunc sed velit dignissim sodales ut. Quam nulla porttitor massa id neque aliquam. Id leo in
                            vitae turpis massa sed diam vel quam elementum cursus in.
                            Volutpat maecenas volutpat. Iaculis at erat pellentesque adipiscing. Praesent tristique magna
                            sit amet purus gravida quis blandit turpis egestas pretium.
                        </div>
                        <div class="induct-page__profit grid-default column-gap-default grid-two-column-center">
                            <div class="induct-page__profit-item flex-default column-gap-default flex-center-align">
                                <img src="assets/img/icon/about-1.svg" alt="" type="perfome-profit-icon">
                                <div class="induct-page__profit-item-box">
                                    <div class="induct-page__profit-item-title">Cutting Edge Tech</div>
                                    <div class="induct-page__profit-item-description">Exercitation ullamco laboris nis exa
                                        duis aute irure dolor.</div>
                                </div>
                            </div>
                            <div class="induct-page__profit-item flex-default column-gap-default flex-center-align">
                                <img src="assets/img/icon/about-2.svg" alt="" type="perfome-profit-icon">
                                <div class="induct-page__profit-item-box">
                                    <div class="induct-page__profit-item-title">Perfect Styling</div>
                                    <div class="induct-page__profit-item-description">Exercitation ullamco laboris nis exa
                                        duis aute irure dolor.</div>
                                </div>
                            </div>
                            <div class="induct-page__profit-item flex-default column-gap-default flex-center-align">
                                <img src="assets/img/icon/about-3.svg" alt="" type="perfome-profit-icon">
                                <div class="induct-page__profit-item-box">
                                    <div class="induct-page__profit-item-title">Distinctive Beauty</div>
                                    <div class="induct-page__profit-item-description">Exercitation ullamco laboris nis exa
                                        duis aute irure dolor.</div>
                                </div>
                            </div>
                            <div class="induct-page__profit-item flex-default column-gap-default flex-center-align">
                                <img src="assets/img/icon/about-4.svg" alt="" type="perfome-profit-icon">
                                <div class="induct-page__profit-item-box">
                                    <div class="induct-page__profit-item-title">Radiance Polishing</div>
                                    <div class="induct-page__profit-item-description">Exercitation ullamco laboris nis exa
                                        duis aute irure dolor.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="induct-page__slider-illustration">
                        <img src="assets/img/slider/slider14.jpg" alt="" class="induct-page__slider-image">
                    </div>
                </slider>
            </section>
            <section class="perfome-page" mode="slider-content">
                <div class="perfome-page__content grid-default grid-two-column-center">
                    <div class="perfome-page__illstrutaion">
                        <img src="assets/img/slider/slider13.png" alt="" class="perfome-page__illstrutation-img">
                    </div>
                    <div class="perfome-page__details">
                        <div class="perfome-page__details-title">A step above with rider-friendly nature</div>
                        <div class="perfome-page__details-description">A step above with rider-friendly nature</div>
                        <div class="perfome-page__details-summary">Dolore magna aliqua quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex consequat. Duis aute irure dolor in reprehenderit in voluptate velit
                            esse cilum dol sed ipsum nulla pariatur nostrul done elit magna.</div>
                        <div class="perfome-page__profit grid-default grid-three-column-center">
                            <div class="perfome-page__profit-item column-gap-default flex-center-align flex-default">
                                <img src="assets/img/icon/info-1.svg" alt="" type="perfome-profit-icon">
                                <div class="perfome-page__profit-text">EASY TO BOOK FOR RENTALS</div>
                            </div>
                            <div class="perfome-page__profit-item column-gap-default flex-center-align flex-default">
                                <img src="assets/img/icon/info-2.svg" alt="" type="perfome-profit-icon">
                                <div class="perfome-page__profit-text">LONGER RIDES FOR ALL DAY</div>
                            </div>
                            <div class="perfome-page__profit-item column-gap-default flex-center-align flex-default">
                                <img src="assets/img/icon/info-3.svg" alt="" type="perfome-profit-icon">
                                <div class="perfome-page__profit-text">GET MILEAGE UNLIMITED</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="rent-page" mode="slider-content">
                <div class="rent-page__main-title-box">
                    <div class="rent-page-title">MOTORCYCLES ON RENT</div>
                    <div class="rent-page__details-summary">Dolore magna aliqua quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex consequat. Duis aute irure dolor in reprehenderit in voluptate velit
                        esse cilum dol sed ipsum nulla pariatur nostrul done elit magna.</div>\
                </div>
                <div class="rent-page__wrapper">
                    <div class="rent-page__list column-gap-default" style="--slide-to-index:0; --per-page:1; --show-page:0">
                        <c:forEach items="${sessionScope.productRentalList}" var="rental" begin="0" end="9">
                            <div class="rent-page__item">
                                <div class="rent-page__content grid-default grid-two-column-center">
                                    <div class="rent-page__details">
                                        <div class="rent-page__details-title">${rental.getModelYear()} ${rental.getName()}</div>
                                        <div class="rent-page__details-description">${rental.getBrand()}</div>
                                        <div class="rent-page__details-tiny flex-default flex-center-align">RENT FOR AS LOW AS <span
                                                class="rent-page__details-highlight-price">$${rental.getPrice()}</span>
                                            <div class="flex-default flex-direction-column">
                                                <span>PER</span>
                                                <c:choose>
                                                    <c:when test="${rental.getModeRental()} == 'DAY'">
                                                        <span>DAY</span>
                                                    </c:when>
                                                    <c:when test="${rental.getModeRental()} == 'WEEK'">
                                                        <span>WEEK</span>
                                                    </c:when>
                                                    <c:when test="${rental.getModeRental()} == 'MONTH'">
                                                        <span>MONTH</span>
                                                    </c:when>
                                                    <c:when test="${rental.getModeRental()} == 'YEAR'">
                                                        <span>YEAR</span>
                                                    </c:when>
                                                </c:choose>
                                            </div>
                                        </div>
                                        <div class="rent-page__details-summary">${rental.getDescription()}</div>
                                        <div class="rent-page__performance-list grid-default grid-two-column-center">
                                            <div class="rent-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-6.svg" alt="" class="rent-page__performance-image">
                                                <div class="rent-page__performance-box">
                                                    <div class="rent-page__performance-title">CATEGORY</div>
                                                    <div class="rent-page__performance-description">${rental.getCategory()}</div>
                                                </div>
                                            </div>
                                            <div class="rent-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-4.svg" alt="" class="rent-page__performance-image">
                                                <div class="rent-page__performance-box">
                                                    <div class="rent-page__performance-title">TRANSMISSION</div>
                                                    <div class="rent-page__performance-description">${rental.getTransmission()}</div>
                                                </div>
                                            </div>
                                            <div class="rent-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-1.svg" alt="" class="rent-page__performance-image">
                                                <div class="rent-page__performance-box">
                                                    <div class="rent-page__performance-title">STARTER</div>
                                                    <div class="rent-page__performance-description">${rental.getStarter()}</div>
                                                </div>
                                            </div>
                                            <div class="rent-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-3.svg" alt="" class="rent-page__performance-image">
                                                <div class="rent-page__performance-box">
                                                    <div class="rent-page__performance-title">DISPLACEMENT</div>
                                                    <div class="rent-page__performance-description">${rental.getDisplacement()}cc</div>
                                                </div>
                                            </div>
                                            <div class="rent-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-2.svg" alt="" class="rent-page__performance-image">
                                                <div class="rent-page__performance-box">
                                                    <div class="rent-page__performance-title">FUEL CAPACITY</div>
                                                    <div class="rent-page__performance-description">${rental.getFuelCapacity()} gallons</div>
                                                </div>
                                            </div>
                                            <div class="rent-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-2.svg" alt="" class="rent-page__performance-image">
                                                <div class="rent-page__performance-box">
                                                    <div class="rent-page__performance-title">FUEL SYSTEM</div>
                                                    <div class="rent-page__performance-description">${rental.getFuelSystem()}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rent-page__illstrutation">
                                        <img src="${rental.getImage()}" alt="" class="rent-page__illstrutation-img">
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <div class="rent-page__controller flex-default flex-center column-gap-default">
                    <div class="rent-page__controller-arrow" mode="leftArrow">
                        <img src="assets/img/action/arrow.svg" alt="" class="rent-page__controller-arrow-img" type="leftArrow">
                    </div>
                    <div class="rent-page__controller-number"><span class="rent-page__index-slider-page"></span> / <span class="rent-page__total-slider-page"></span></div>
                    <div class="rent-page__controller-arrow" mode="rightArrow">
                        <img src="assets/img/action/arrow.svg" alt="" class="rent-page__controller-arrow-img" type="rightArrow">
                    </div>
                </div>
            </section>
            <section class="sell-page" mode="slider-content">
                <div class="sell-page__main-title-box">
                    <div class="sell-page-title">MOTORCYCLES ON SELL</div>
                    <div class="sell-page__details-summary">Dolore magna aliqua quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex consequat. Duis aute irure dolor in reprehenderit in voluptate velit
                        esse cilum dol sed ipsum nulla pariatur nostrul done elit magna.</div>\
                </div>
                <div class="sell-page__wrapper">
                    <div class="sell-page__list column-gap-default" style="--slide-to-index:0; --per-page:1; --show-page:0">
                        <c:forEach items="${sessionScope.productSellList}" var="sell" begin="0" end="9">
                            <div class="sell-page__item">
                                <div class="sell-page__content grid-default grid-two-column-center">
                                    <div class="sell-page__details">
                                        <div class="sell-page__details-title">${sell.getModelYear()} ${sell.getName()}</div>
                                        <div class="sell-page__details-description">${sell.getBrand()}</div>
                                        <div class="sell-page__details-tiny flex-default flex-center-align">ONLY <span
                                                class="sell-page__details-highlight-price">$${sell.getPrice()}</span>
                                            <div class="flex-default flex-direction-column">
                                                <span>TO GET</span>
                                                <span>THIS ONE</span>
                                            </div>
                                        </div>
                                        <div class="sell-page__details-summary">${sell.getDescription()}</div>
                                        <div class="sell-page__performance-list grid-default grid-two-column-center">
                                            <div class="sell-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-6.svg" alt="" class="sell-page__performance-image">
                                                <div class="sell-page__performance-box">
                                                    <div class="sell-page__performance-title">CATEGORY</div>
                                                    <div class="sell-page__performance-description">${sell.getCategory()}</div>
                                                </div>
                                            </div>
                                            <div class="sell-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-4.svg" alt="" class="sell-page__performance-image">
                                                <div class="sell-page__performance-box">
                                                    <div class="sell-page__performance-title">TRANSMISSION</div>
                                                    <div class="sell-page__performance-description">${sell.getTransmission()}</div>
                                                </div>
                                            </div>
                                            <div class="sell-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-1.svg" alt="" class="sell-page__performance-image">
                                                <div class="sell-page__performance-box">
                                                    <div class="sell-page__performance-title">STARTER</div>
                                                    <div class="sell-page__performance-description">${sell.getStarter()}</div>
                                                </div>
                                            </div>
                                            <div class="sell-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-3.svg" alt="" class="sell-page__performance-image">
                                                <div class="sell-page__performance-box">
                                                    <div class="sell-page__performance-title">DISPLACEMENT</div>
                                                    <div class="sell-page__performance-description">${sell.getDisplacement()}cc</div>
                                                </div>
                                            </div>
                                            <div class="sell-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-2.svg" alt="" class="sell-page__performance-image">
                                                <div class="sell-page__performance-box">
                                                    <div class="sell-page__performance-title">FUEL CAPACITY</div>
                                                    <div class="sell-page__performance-description">${sell.getFuelCapacity()} gallons</div>
                                                </div>
                                            </div>
                                            <div class="sell-page__performance-item flex-default column-gap-default">
                                                <img src="assets/img/icon/specifications-2.svg" alt="" class="sell-page__performance-image">
                                                <div class="sell-page__performance-box">
                                                    <div class="sell-page__performance-title">FUEL SYSTEM</div>
                                                    <div class="sell-page__performance-description">${sell.getFuelSystem()}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sell-page__illstrutation">
                                        <img src="${sell.getImage()}" alt="" class="sell-page__illstrutation-img">
                                    </div>
                                </div>
                            </div>                            
                        </c:forEach>
                    </div>
                </div>
                <div class="sell-page__controller flex-default flex-center column-gap-default">
                    <div class="sell-page__controller-arrow" mode="leftArrow">
                        <img src="assets/img/action/arrow.svg" alt="" class="sell-page__controller-arrow-img" type="leftArrow">
                    </div>
                    <div class="sell-page__controller-number"><span class="sell-page__index-slider-page"></span> / <span class="sell-page__total-slider-page"></span></div>
                    <div class="sell-page__controller-arrow" mode="rightArrow">
                        <img src="assets/img/action/arrow.svg" alt="" class="sell-page__controller-arrow-img" type="rightArrow">
                    </div>
                </div>
            </section>
            <section class="content-page" mode="slider-content">
                <div class="content-page__box-title grid-default grid-two-column-center">
                    <div class="content-page__title">We have our best destination for you.</div>
                    <div class="content-page__sub-title">With many people wanting to enjoy a long trip, choosing or renting
                        a car to experience for themselves is a better choice. That's why we guarantee to give you the best
                        service possible.</div>
                </div>
                <div class="content-page__list grid-default grid-four-column-center">
                    <div class="content-page__item">
                        <img src="assets/img/slider/slider07.webp" alt="" class="content-page__list-image">
                        <div class="content-page__details">
                            <div class="content-page__details-title">Motorcycle Warranties</div>
                            <div class="content-page__details-sub-title">Not only you get enthusiastic advice about the most
                                famous and safest motorbike brands. You also get warranty support packages if your product
                                has problems.</div>
                        </div>
                        <div class="contact-page__btn-action induct-page__btn home-page__introduce-option position-relative"
                             type="wb">
                            <a href="#" class="induct-page__link home-page__introduce-link position-relative">See our new
                                bikes</a>
                            <div class="induct-page__seperate home-page__introduce-seperate"></div>
                        </div>
                    </div>
                    <div class="content-page__item">
                        <div class="content-page__details">
                            <div class="content-page__details-title">Rental service</div>
                            <div class="content-page__details-sub-title">Too worried about motorbike prices? We are here to
                                help you. True to the name, we and the business people will rent you motorbikes with the
                                corresponding price for the day with guaranteed quality.</div>
                        </div>
                        <div class="contact-page__btn-action induct-page__btn home-page__introduce-option position-relative"
                             type="wb">
                            <a href="#" class="induct-page__link home-page__introduce-link position-relative">Hire your new
                                bike</a>
                            <div class="induct-page__seperate home-page__introduce-seperate"></div>
                        </div>
                        <img src="assets/img/slider/slider06.jpg" alt="" class="content-page__list-image">
                    </div>
                    <div class="content-page__item">
                        <img src="assets/img/slider/slider10.gif" alt="" class="content-page__list-image">
                        <div class="content-page__details">
                            <div class="content-page__details-title">Blog</div>
                            <div class="content-page__details-sub-title">Everyone can post news right here. Categories are
                                diverse from buying, selling, renting cars to problems encountered when using our products.
                            </div>
                        </div>
                        <div class="contact-page__btn-action induct-page__btn home-page__introduce-option position-relative"
                             type="wb">
                            <a href="#" class="induct-page__link home-page__introduce-link position-relative">Upload your
                                new post</a>
                            <div class="induct-page__seperate home-page__introduce-seperate"></div>
                        </div>
                    </div>
                    <div class="content-page__item">
                        <div class="content-page__details">
                            <div class="content-page__details-title">Businessman</div>
                            <div class="content-page__details-sub-title">Welcome to the business world. You will be licensed
                                to become a dealer, motorbike rental here. Besides ensuring the interests of a business
                                person, we are ready to assist you anytime you need us.</div>
                        </div>
                        <div class="contact-page__btn-action induct-page__btn home-page__introduce-option position-relative"
                             type="wb">
                            <a href="#" class="induct-page__link home-page__introduce-link position-relative">Shake a
                                hand</a>
                            <div class="induct-page__seperate home-page__introduce-seperate"></div>
                        </div>
                        <img src="assets/img/slider/slider11.jpg" alt="" class="content-page__list-image">
                    </div>
                </div>
            </section>
            <section class="Feedback-page" mode="slider-content">
                <section class="app-ecommerce-details">
                    <div class="Feedback-page__box-content flex-default">
                        <div class="Feedback-page__box-title grid-default grid-two-column-center">
                            <div class="Feedback-page__title">Let's our customers talk about us</div>
                        </div>
                        <div class="Feedback-page__action">
                            <div class="Feedback-page__option-list column-gap-default flex-default">
                                <div class="Feedback-page__control cursor-select-pointer" type="leftControl">
                                    <img src="assets/img/action/arrow-alt.svg" alt="" class="control-img" type="leftControl">
                                </div>
                                <div class="Feedback-page__control cursor-select-pointer" type="rightControl">
                                    <img src="assets/img/action/arrow-alt.svg" alt="" class="control-img" type="rightControl">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Feedback-page__wrapper">
                        <div class="Feedback-page__list column-gap-default flex-default grid-three-column-center" style="--slide-to-index:0; --per-page:3; --show-page:0;">
                            <c:forEach items="${sessionScope.listProductReview}" var="reviews">
                                <div class="Feedback-page__item">
                                    <div class="Feedback-page__comment">
                                        <q>
                                            ${reviews.getCommentReview()}
                                        </q>
                                    </div>
                                    <div class="Feedback-page__user flex-default">
                                        <c:choose>
                                            <c:when test="${reviews.getImageAccount() != null}"> 
                                                <img src="${reviews.getImageAccount()}" alt="" class="Feedback-page__user-img">
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="name" value="${reviews.getAccountName()}"/>
                                                <span class="avatar">
                                                    <div class="avatar bg-light-danger me-50" style="margin-right:0 !important;width:60px;height:60px;border: 1px solid #949b83;">
                                                        <div class="avatar-content avatar-fixed-up">${fn:substring(name, 0, 1)}</div>
                                                    </div>
                                                </span>
                                            </c:otherwise>     
                                        </c:choose>
                                        <div class="Feedback-page-rating-star">
                                            <div class="Feedback-page__user-details">
                                                <div class="Feedback-page__user-display-name">${reviews.getAccountName()}</div>
                                                <div class="Feedback-page__user-display-role">${reviews.getAccountRole()}</div>
                                            </div>
                                            <div class="item-rating">
                                                <div style="text-align: right;color:black">${reviews.getCommentDate()}</div>
                                                <ul class="unstyled-list list-inline" style="margin-bottom:0;">
                                                    <c:choose>
                                                        <c:when test="${reviews.getRatingStar() == '5'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            </c:when>
                                                            <c:when test="${reviews.getRatingStar() == '4'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </c:when>
                                                            <c:when test="${reviews.getRatingStar() == '3'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </c:when>
                                                            <c:when test="${reviews.getRatingStar() == '2'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </c:when>
                                                            <c:when test="${reviews.getRatingStar() == '1'}">
                                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>      
                                                            </c:otherwise>
                                                        </c:choose>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>                    
                </section>
            </section>
            <section class="join-page" mode="slider-content">
                <div class="join-page__content flex-default flex-center">
                    <div class="join-page__box-title">
                        <div class="join-page__title">We are waiting for you</div>
                        <div class="join-page__description">The opportunity to be welcomed into the Hilfsmotor community
                            lies
                            with you. Join now to receive many incentives from us as well as the community.</div>
                        <div class="join-page__btn-action contact-page__btn-action induct-page__btn home-page__introduce-option position-relative"
                             type="wb">
                            <a href="#" class="induct-page__link home-page__introduce-link position-relative">Get
                                started</a>
                            <div class="induct-page__seperate home-page__introduce-seperate"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="brands-page" mode="slider-content">
                <div class="brands-page__content flex-default flex-center column-gap-default">
                    <div class="brands-page__item">
                        <img src="assets/img/icon/brand-1.png" alt="" class="brands-page__img">
                    </div>
                    <div class="brands-page__item">
                        <img src="assets/img/icon/brand-2.png" alt="" class="brands-page__img" type="resize">
                    </div>
                    <div class="brands-page__item">
                        <img src="assets/img/icon/brand-3.png" alt="" class="brands-page__img" type="resize">
                    </div>
                    <div class="brands-page__item">
                        <img src="assets/img/icon/brand-4.png" alt="" class="brands-page__img" type="resize">
                    </div>
                    <div class="brands-page__item">
                        <img src="assets/img/icon/brand-5.png" alt="" class="brands-page__img">
                    </div>
                </div>
            </section>
            <footer>
                <section class="footer-page" mode="footer-content">
                    <div class="footer-page__content">
                        <div class="footer-page__box-logo">
                            <div class="footer-page__logo column-gap-default flex-center-align flex-default">
                                <img src="app-assets/images/ico/hilf.png" alt="" class="footer-page__logo-image">
                                <div class="footer-page__logo-text">Hilfsmotor</div>
                                <div class="footer-page__seperate"></div>
                            </div>
                        </div>
                        <div class="footer-page__superivors grid-default grid-four-column-center">
                            <div class="footer-page__contacts">
                                <div class="footer-page__dealer-title">Contact to us</div>
                                <div class="footer-page__contacts-item column-gap-default flex-center-align flex-default">
                                    <img src="assets/img/action/phoneIcon.svg" alt="" class="footer-page__contacts-icon">
                                    <div class="footer-page__contacts-text">0246.329.1101</div>
                                </div>
                                <div class="footer-page__contacts-item column-gap-default flex-center-align flex-default">
                                    <img src="assets/img/action/mainIcon.svg" alt="" class="footer-page__contacts-icon">
                                    <div class="footer-page__contacts-text">hilfsmotor@gmail.com</div>
                                </div>
                                <div class="footer-page__contacts-item column-gap-default flex-center-align flex-default">
                                    <img src="assets/img/action/addressIcon.svg" alt="" class="footer-page__contacts-icon">
                                    <div class="footer-page__contacts-text">House D9, Lot A10, South Trung Yen, Trung Hoa,
                                        Cau Giay, Hanoi</div>
                                </div>
                                <div class="footer-page__contacts-support flex-default">
                                    <div class="footer-page__contacts-icon">
                                        <svg style="width:36px;height:36px;" class="svg-inline--fa fa-headset fa-w-16"
                                             aria-hidden="true" focusable="false" data-prefix="fas" data-icon="headset"
                                             role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                             data-fa-i2svg="">
                                        <path fill="currentColor"
                                              d="M192 208c0-17.67-14.33-32-32-32h-16c-35.35 0-64 28.65-64 64v48c0 35.35 28.65 64 64 64h16c17.67 0 32-14.33 32-32V208zm176 144c35.35 0 64-28.65 64-64v-48c0-35.35-28.65-64-64-64h-16c-17.67 0-32 14.33-32 32v112c0 17.67 14.33 32 32 32h16zM256 0C113.18 0 4.58 118.83 0 256v16c0 8.84 7.16 16 16 16h16c8.84 0 16-7.16 16-16v-16c0-114.69 93.31-208 208-208s208 93.31 208 208h-.12c.08 2.43.12 165.72.12 165.72 0 23.35-18.93 42.28-42.28 42.28H320c0-26.51-21.49-48-48-48h-32c-26.51 0-48 21.49-48 48s21.49 48 48 48h181.72c49.86 0 90.28-40.42 90.28-90.28V256C507.42 118.83 398.82 0 256 0z">
                                        </path>
                                        </svg>
                                    </div>
                                    <div class="footer-page__contacts-desc">
                                        <div class="footer-page__contacts-desc-title">Talk with our support</div>
                                        <div class="footer-page__contacts-phone">+1 (300) 490 5008</div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer-page__dealer">
                                <div class="footer-page__dealer-title">About Hilfsmotor</div>
                                <div
                                    class="footer-page__contacts-item column-gap-default flex-default cursor-select-pointer">
                                    <img src="assets/img/action/arrow.svg" alt="" class="footer-page__contacts-icon"
                                         type="arrow">
                                    <div class="footer-page__contacts-text">Shop</div>
                                </div>
                                <div
                                    class="footer-page__contacts-item column-gap-default  flex-default cursor-select-pointer">
                                    <img src="assets/img/action/arrow.svg" alt="" class="footer-page__contacts-icon"
                                         type="arrow">
                                    <div class="footer-page__contacts-text">Rentals</div>
                                </div>
                                <div
                                    class="footer-page__contacts-item column-gap-default  flex-default cursor-select-pointer">
                                    <img src="assets/img/action/arrow.svg" alt="" class="footer-page__contacts-icon"
                                         type="arrow">
                                    <div class="footer-page__contacts-text">Blog</div>
                                </div>
                                <div
                                    class="footer-page__contacts-item column-gap-default  flex-default cursor-select-pointer">
                                    <img src="assets/img/action/arrow.svg" alt="" class="footer-page__contacts-icon"
                                         type="arrow">
                                    <div class="footer-page__contacts-text">Contact</div>
                                </div>
                            </div>
                            <div class="footer-page__dealer">
                                <div class="footer-page__dealer-title">Dealer Information</div>
                                <div class="footer-page__dealer-box">
                                    <div class="footer-page__details">
                                        <h3 class="footer-page__details-title">SALES HOURS</h3>
                                        <div>
                                            <div class="footer-page__details-item">Monday - Friday:</div>
                                            <div class="footer-page__details-item">09:00 am to 06:00 pm</div>
                                        </div>
                                    </div>
                                    <div class="footer-page__details">
                                        <h3 class="footer-page__details-title">SALES HOURS</h3>
                                        <div>
                                            <div class="footer-page__details-item">Monday - Friday:</div>
                                            <div class="footer-page__details-item">09:00 am to 06:00 pm</div>
                                        </div>
                                    </div>
                                    <div class="footer-page__details">
                                        <h3 class="footer-page__details-title">SALES HOURS</h3>
                                        <div>
                                            <div class="footer-page__details-item">Monday - Friday:</div>
                                            <div class="footer-page__details-item">09:00 am to 06:00 pm</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer-page__about">
                                <div class="footer-page__about-company-name">Hilfsmotor - Motorcycles Joint Stock Company
                                </div>
                                <div class="footer-page__details">
                                    <div class="footer-page__details-item">Tax code: 0109922950</div>
                                    <div class="footer-page__details-item">Foundation: 04/09/2022</div>
                                    <div class="footer-page__details-item">Field: Motorbike</div>
                                    <div class="footer-page__details-item">Hilfsmotor builds and develops products that
                                        bring value to the community.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-page__credit">
                        <div class="footer-page__credit-text">(c) 2022 Hilfsmotor - Motorcycles Dealer Template. All rights
                            reserved.</div>
                    </div>
                </section>
            </footer>
        </div>
        <!-- BEGIN: Vendor JS-->
        <script src="app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="app-assets/js/core/app-menu.min.js"></script>
        <script src="app-assets/js/core/app.min.js"></script>
        <script src="app-assets/js/scripts/customizer.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="app-assets/js/scripts/pages/app-ecommerce.min.js"></script>
        <!-- END: Page JS-->
        <script src="assets/js/homeC.js"></script>
        <script>
            $(window).on('load', function () {
                if (feather) {
                    feather.replace({width: 14, height: 14});
                }
            });
        </script>
    </body>

</html>
