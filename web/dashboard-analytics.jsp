<%-- 
    Document   : dashboard-analytics
    Created on : Sep 20, 2022, 10:34:14 PM
    Author     : ADMIN
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/apexcharts.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/charts/chart-apex.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/app-invoice-list.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->
    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover"
          data-menu="horizontal-menu" data-col="">

        <!-- BEGIN: Header-->
        <nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center"
             data-nav="brand-center">
            <div class="navbar-header d-xl-block d-none">
                <ul class="nav navbar-nav">
                    <li class="nav-item">
                        <a class="navbar-brand" href="dashboard-analytics.jsp">
                            <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                            <h2 class="brand-text mb-0 lidget-app__padding-left">Hilfsmotor</h2>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calendar.jsp" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Calendar"><i class="ficon" data-feather="calendar"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning"
                                                                                                    data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="#"
                                                                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"
                                                                                                        data-language="en"><i class="flag-icon flag-icon-us"></i> English</a>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon"
                                                                                                 data-feather="sun"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon"
                                                                                           data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Hilfsmotor..." tabindex="-1"
                                   data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#"
                                                                                 data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span
                                class="badge rounded-pill bg-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-15.jpg"
                                                                     alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small
                                                class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar"
                                                                     width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small
                                                class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small
                                                class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="list-item d-flex align-items-center">
                                    <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                    <div class="form-check form-check-primary form-switch">
                                        <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="form-check-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small
                                                class="notification-text"> USA Server is down due to hight CPU usage</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small
                                                class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small
                                                class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle dropdown-user-link"
                           id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none">
                                <span class="user-name fw-bolder">${staff.getUserName()}</span>
                                <span class="user-status">${staff.getRole()}</span></div>
                            <span class="avatar">
                                <c:choose>
                                    <c:when test="${staff.getAvatar() == null}">
                                        <c:set var="firstName" value="${staff.getFirstName()}"/>
                                        <c:set var="lastName" value="${staff.getLastName()}"/>
                                        <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                            <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                        </div>
                                    </c:when>
                                    <c:when test="${staff.getAvatar() != null}">
                                        <img class="round" src="${staff.getAvatar()}" alt="avatar" height="40"
                                             width="40">   
                                    </c:when>
                                </c:choose> 
                                <span class="avatar-status-online"></span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="page-profile.jsp"><i class="me-50" data-feather="user"></i> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="page-account-settings-account.jsp"><i
                                    class="me-50" data-feather="settings"></i> Settings
                            </a>
                            <a class="dropdown-item" href="page-faq.jsp"><i class="me-50" data-feather="help-circle"></i> FAQ
                            </a>
                            <a class="dropdown-item" href="logoutController">
                                <i class="me-50" data-feather="power"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6>
                </a></li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between w-100"
                   href="app-file-manager.jsp">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small>
                </a>
            </li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between w-100"
                   href="app-file-manager.jsp">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd
                                Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small>
                </a></li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between w-100"
                   href="app-file-manager.jsp">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small>
                </a></li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between w-100"
                   href="app-file-manager.jsp">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small>
                </a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6>
                </a></li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between py-50 w-100"
                   href="app-user-view-account.jsp">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Rogdrigues</p><small class="text-muted">UI designer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between py-50 w-100"
                   href="app-user-view-account.jsp">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between py-50 w-100"
                   href="app-user-view-account.jsp">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between py-50 w-100"
                   href="app-user-view-account.jsp">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div>
                </a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a
                    class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No
                            results found.</span></div>
                </a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="horizontal-menu-wrapper">
            <div class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-dark navbar-shadow menu-border container-xxl"
                 role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav">
                <div class="navbar-header" style="margin-bottom:2rem;">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item me-auto"><a class="navbar-brand"
                                                        href="html/ltr/horizontal-menu-template-dark/dashboard-analytics.jsp">
                                <span>
                                    <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                                </span>
                                <h2 class="brand-text mb-0" style="position: relative; right: 25px;">Hilfsmotor</h2>
                            </a></li>
                        <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                                    class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i></a></li>
                    </ul>
                </div>
                <div class="shadow-bottom"></div>
                <!-- Horizontal menu content-->
                <div class="navbar-container main-menu-content" data-menu="menu-container">
                    <!-- include includes/mixins-->
                    <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                        <li class="dropdown nav-item sidebar-group-active active open" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link d-flex align-items-center" data-bs-toggle="dropdown"><i
                                    data-feather="home"></i><span data-i18n="Dashboards">Dashboards</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li data-menu=""><a class="dropdown-item d-flex align-items-center" href=""
                                                    data-bs-toggle="" data-i18n="Analytics"><i data-feather="activity"></i><span
                                            data-i18n="Analytics">Analytics</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown nav-item" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link lidget-dashboard-analytics d-flex align-items-center" href="#"
                                data-bs-toggle="dropdown"><i data-feather="package"></i><span data-i18n="Apps">Apps</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Invoice"><i data-feather="file-text"></i><span data-i18n="Invoice">Orders</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="orderController"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Roles &amp; Permission"><i data-feather="shield"></i><span
                                            data-i18n="Roles &amp; Permission">Roles &amp; Permission</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="roleListController"
                                                            data-bs-toggle="" data-i18n="Roles"><i data-feather="circle"></i><span
                                                    data-i18n="Roles">Roles</span></a>
                                        </li>
                                        <li class="active" data-menu=""><a class="dropdown-item d-flex align-items-center"
                                                                           href="permissionListController" data-bs-toggle="" data-i18n="Permission"><i
                                                    data-feather="circle"></i><span data-i18n="Permission">Permission</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Store"><i data-feather='shopping-bag'></i><span
                                            data-i18n="Store">Store</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="store-list"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="eCommerce"><i data-feather='shopping-bag'></i><span
                                            data-i18n="eCommerce">Product</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="productController"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="User"><i data-feather="user"></i><span data-i18n="User">User</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="userListController"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                                class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                                data-i18n="View"><i data-feather="circle"></i><span data-i18n="View">View</span></a>
                                            <ul class="dropdown-menu" data-bs-popper="none">
                                                <li data-menu=""><a class="dropdown-item d-flex align-items-center"
                                                                    href="app-user-view-account.jsp" data-bs-toggle="" data-i18n="Account"><i
                                                            data-feather="circle"></i><span data-i18n="Account">Account</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a
                                        class="dropdown-item d-flex align-items-center" href="/Rentabike/blog-admin-control"
                                        data-i18n="eCommerce"><span data-i18n="eCommerce">Manage Blogs</span></a>
                                </li>
                                <li class="dropdown"><a
                                    class="dropdown-item d-flex align-items-center" href="/Rentabike/assistance-quest"
                                    data-i18n="eCommerce"><span data-i18n="eCommerce">Manage Help Page</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Main Menu-->
        <!-- BEGIN: Content-->
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <!-- Dashboard Analytics Start -->
                    <section id="dashboard-analytics">
                        <div class="row match-height">
                            <!-- Greetings Card starts -->
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="card card-congratulations">
                                    <div class="card-body text-center">
                                        <img src="app-assets/images/elements/decore-left.png" class="congratulations-img-left"
                                             alt="card-img-left" />
                                        <img src="app-assets/images/elements/decore-right.png" class="congratulations-img-right"
                                             alt="card-img-right" />
                                        <div class="avatar avatar-xl bg-primary shadow">
                                            <div class="avatar-content">
                                                <i data-feather="award" class="font-large-1"></i>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <h1 class="mb-1 text-white">Welcome ${staff.getUserName()},</h1>
                                            <p class="card-text m-auto w-75">
                                                You have been working <strong>23 days</strong> since you joined us. Keep your fire together with
                                                us.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Greetings Card ends -->

                            <!-- Subscribers Chart Card starts -->
                            <div class="col-lg-3 col-sm-6 col-12">
                                <div class="card">
                                    <div class="card-header flex-column align-items-start pb-0">
                                        <div class="avatar bg-light-primary p-50 m-0">
                                            <div class="avatar-content">
                                                <i data-feather="users" class="font-medium-5"></i>
                                            </div>
                                        </div>
                                        <h2 class="fw-bolder mt-1">92.6k</h2>
                                        <p class="card-text">User Register to our system</p>
                                    </div>
                                    <div id="gained-chart"></div>
                                </div>
                            </div>
                            <!-- Subscribers Chart Card ends -->

                            <!-- Orders Chart Card starts -->
                            <div class="col-lg-3 col-sm-6 col-12">
                                <div class="card">
                                    <div class="card-header flex-column align-items-start pb-0">
                                        <div class="avatar bg-light-warning p-50 m-0">
                                            <div class="avatar-content">
                                                <i data-feather="package" class="font-medium-5"></i>
                                            </div>
                                        </div>
                                        <h2 class="fw-bolder mt-1">38.4K</h2>
                                        <p class="card-text">Orders Shipped Completed</p>
                                    </div>
                                    <div id="order-chart"></div>
                                </div>
                            </div>
                            <!-- Orders Chart Card ends -->
                        </div>

                        <div class="row match-height">
                            <!-- Avg Sessions Chart Card starts -->
                            <div class="col-lg-6 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row pb-50">
                                            <div
                                                class="col-sm-6 col-12 d-flex justify-content-between flex-column order-sm-1 order-2 mt-1 mt-sm-0">
                                                <div class="mb-1 mb-sm-0">
                                                    <h2 class="fw-bolder mb-25">2.7K</h2>
                                                    <p class="card-text fw-bold mb-2">Our goals</p>
                                                    <div class="font-medium-2">
                                                        <span class="text-success me-25">+5.2%</span>
                                                        <span>vs last 7 days</span>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-primary">View Details</button>
                                            </div>
                                            <div class="col-sm-6 col-12 d-flex justify-content-between flex-column text-end order-sm-2 order-1">
                                                <div class="dropdown chart-dropdown">
                                                    <button class="btn btn-sm border-0 dropdown-toggle p-50" type="button" id="dropdownItem5"
                                                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Last 7 Days
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownItem5">
                                                        <a class="dropdown-item" href="#">Last 28 Days</a>
                                                        <a class="dropdown-item" href="#">Last Month</a>
                                                        <a class="dropdown-item" href="#">Last Year</a>
                                                    </div>
                                                </div>
                                                <div id="avg-sessions-chart"></div>
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="row avg-sessions pt-50">
                                            <div class="col-6 mb-2">
                                                <p class="mb-50">Goal: $100000</p>
                                                <div class="progress progress-bar-primary" style="height: 6px">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="50"
                                                         aria-valuemax="100" style="width: 50%"></div>
                                                </div>
                                            </div>
                                            <div class="col-6 mb-2">
                                                <p class="mb-50">Users: 100K</p>
                                                <div class="progress progress-bar-warning" style="height: 6px">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="60"
                                                         aria-valuemax="100" style="width: 60%"></div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <p class="mb-50">Retention: 90%</p>
                                                <div class="progress progress-bar-danger" style="height: 6px">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="70"
                                                         aria-valuemax="100" style="width: 70%"></div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <p class="mb-50">Duration: 365 days</p>
                                                <div class="progress progress-bar-success" style="height: 6px">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="90"
                                                         aria-valuemax="100" style="width: 90%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Avg Sessions Chart Card ends -->

                            <!-- Support Tracker Chart Card starts -->
                            <div class="col-lg-6 col-12">
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between pb-0">
                                        <h4 class="card-title">Reports</h4>
                                        <div class="dropdown chart-dropdown">
                                            <button class="btn btn-sm border-0 dropdown-toggle p-50" type="button" id="dropdownItem4"
                                                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Last 7 Days
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownItem4">
                                                <a class="dropdown-item" href="#">Last 28 Days</a>
                                                <a class="dropdown-item" href="#">Last Month</a>
                                                <a class="dropdown-item" href="#">Last Year</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-2 col-12 d-flex flex-column flex-wrap text-center">
                                                <h1 class="font-large-2 fw-bolder mt-2 mb-0">163</h1>
                                                <p class="card-text">Tickets</p>
                                            </div>
                                            <div class="col-sm-10 col-12 d-flex justify-content-center">
                                                <div id="support-trackers-chart"></div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-1">
                                            <div class="text-center">
                                                <p class="card-text mb-50">New Tickets</p>
                                                <span class="font-large-1 fw-bold">29</span>
                                            </div>
                                            <div class="text-center">
                                                <p class="card-text mb-50">Open Tickets</p>
                                                <span class="font-large-1 fw-bold">63</span>
                                            </div>
                                            <div class="text-center">
                                                <p class="card-text mb-50">Response Tickets</p>
                                                <span class="font-large-1 fw-bold">100</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Support Tracker Chart Card ends -->
                        </div>

                        <div class="row match-height">
                            <!-- Timeline Card -->
                            <div class="col-lg-4 col-12">
                                <div class="card card-user-timeline">
                                    <div class="card-header">
                                        <div class="d-flex align-items-center">
                                            <i data-feather="list" class="user-timeline-title-icon"></i>
                                            <h4 class="card-title">User Customer Timeline</h4>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <ul class="timeline ms-50">
                                            <li class="timeline-item">
                                                <span class="timeline-point timeline-point-indicator"></span>
                                                <div class="timeline-event">
                                                    <h6>12 Invoices have been paid</h6>
                                                    <p>Invoices are paid to the company</p>
                                                    <div class="d-flex align-items-center">
                                                        <img class="me-1" src="app-assets/images/icons/json.png" alt="data.json"
                                                             height="23" />
                                                        <h6 class="more-info mb-0">Check details in user list</h6>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <span class="timeline-point timeline-point-warning timeline-point-indicator"></span>
                                                <div class="timeline-event">
                                                    <h6>Team Meeting</h6>
                                                    <p>Project meeting with Jensen</p>
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar me-50">
                                                            <img src="app-assets/images/portrait/small/avatar-s-9.jpg" alt="Avatar" width="38"
                                                                 height="38" />
                                                        </div>
                                                        <div class="more-info">
                                                            <h6 class="mb-0">Nicolas Jensen (Client)</h6>
                                                            <p class="mb-0">CEO of Hilfsmotor</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <span class="timeline-point timeline-point-info timeline-point-indicator"></span>
                                                <div class="timeline-event">
                                                    <h6>A new product had been added</h6>
                                                    <p>Honda D53 (Sell)</p>
                                                    <div class="avatar-group">
                                                        <div data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="bottom"
                                                             title="Billy Hopkins" class="avatar pull-up">
                                                            <img src="app-assets/images/portrait/small/avatar-s-9.jpg" alt="Avatar" width="33"
                                                                 height="33" />
                                                        </div>
                                                        <div data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="bottom"
                                                             title="Amy Carson" class="avatar pull-up">
                                                            <img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="Avatar" width="33"
                                                                 height="33" />
                                                        </div>
                                                        <div data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="bottom"
                                                             title="Brandon Miles" class="avatar pull-up">
                                                            <img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="Avatar" width="33"
                                                                 height="33" />
                                                        </div>
                                                        <div data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="bottom"
                                                             title="Daisy Weber" class="avatar pull-up">
                                                            <img src="app-assets/images/portrait/small/avatar-s-7.jpg" alt="Avatar" width="33"
                                                                 height="33" />
                                                        </div>
                                                        <div data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="bottom"
                                                             title="Jenny Looper" class="avatar pull-up">
                                                            <img src="app-assets/images/portrait/small/avatar-s-20.jpg" alt="Avatar" width="33"
                                                                 height="33" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <span class="timeline-point timeline-point-danger timeline-point-indicator"></span>
                                                <div class="timeline-event">
                                                    <h6>Receive a new report from Rogdrigues</h6>
                                                    <p>Cannot buy my motorbike</p>
                                                </div>
                                                <div class="avatar-group">
                                                    <div data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="bottom"
                                                         title="Billy Hopkins" class="avatar pull-up">
                                                        <img src="app-assets/images/portrait/small/avatar-s-9.jpg" alt="Avatar" width="33"
                                                             height="33" />
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--/ Timeline Card -->

                            <!-- Sales Stats Chart Card starts -->
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between align-items-start pb-1">
                                        <div>
                                            <h4 class="card-title mb-25">Sales</h4>
                                            <p class="card-text">Last 6 months</p>
                                        </div>
                                        <div class="dropdown chart-dropdown">
                                            <i data-feather="more-vertical" class="font-medium-3 cursor-pointer" data-bs-toggle="dropdown"></i>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a class="dropdown-item" href="#">Last 28 Days</a>
                                                <a class="dropdown-item" href="#">Last Month</a>
                                                <a class="dropdown-item" href="#">Last Year</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="d-inline-block me-1">
                                            <div class="d-flex align-items-center">
                                                <i data-feather="circle" class="font-small-3 text-primary me-50"></i>
                                                <h6 class="mb-0">Sales</h6>
                                            </div>
                                        </div>
                                        <div class="d-inline-block">
                                            <div class="d-flex align-items-center">
                                                <i data-feather="circle" class="font-small-3 text-info me-50"></i>
                                                <h6 class="mb-0">Visits</h6>
                                            </div>
                                        </div>
                                        <div id="sales-visit-chart" class="mt-50"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Sales Stats Chart Card ends -->

                            <!-- App Design Card -->
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="card card-app-design">
                                    <div class="card-body">
                                        <span class="badge badge-light-primary">03 Sep, 2022</span>
                                        <h4 class="card-title mt-1 mb-75 pt-25">Product Management</h4>
                                        <p class="card-text font-small-2 mb-2">
                                            You are required to add your new product and post to product list
                                        </p>
                                        <div class="design-group mb-2 pt-50">
                                            <h6 class="section-label">Team</h6>
                                            <span class="badge badge-light-warning me-1">Seller</span>
                                            <span class="badge badge-light-primary">Management</span>
                                        </div>
                                        <div class="design-group pt-25">
                                            <h6 class="section-label">Members</h6>
                                            <div class="avatar">
                                                <img src="app-assets/images/portrait/small/avatar-s-9.jpg" width="34" height="34"
                                                     alt="Avatar" />
                                            </div>
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">PI</div>
                                            </div>
                                            <div class="avatar">
                                                <img src="app-assets/images/portrait/small/avatar-s-14.jpg" width="34" height="34"
                                                     alt="Avatar" />
                                            </div>
                                            <div class="avatar">
                                                <img src="app-assets/images/portrait/small/avatar-s-7.jpg" width="34" height="34"
                                                     alt="Avatar" />
                                            </div>
                                            <div class="avatar bg-light-secondary">
                                                <div class="avatar-content">AL</div>
                                            </div>
                                        </div>
                                        <div class="design-planning-wrapper mb-2 py-75">
                                            <div class="design-planning">
                                                <p class="card-text mb-25">Join date</p>
                                                <h6 class="mb-0">12 Apr, 23</h6>
                                            </div>
                                            <div class="design-planning">
                                                <p class="card-text mb-25">Leader</p>
                                                <h6 class="mb-0">Nicolas Jensen</h6>
                                            </div>
                                            <div class="design-planning">
                                                <p class="card-text mb-25">Members</p>
                                                <h6 class="mb-0">5</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ App Design Card -->
                        </div>

                        <!-- List DataTable -->
                        <div class="row">
                            <div class="col-12">
                                <div class="card invoice-list-wrapper">
                                    <div class="card-datatable table-responsive">
                                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                            <div class="row d-flex justify-content-between align-items-center m-1">
                                                <div class="col-lg-6 d-flex align-items-center">
                                                    <div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select
                                                                name="DataTables_Table_0_length" class="form-select">
                                                                <option value="10">10</option>
                                                                <option value="25">25</option>
                                                                <option value="50">50</option>
                                                                <option value="100">100</option>
                                                            </select></label></div>
                                                </div>
                                                <div
                                                    class="col-lg-6 d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap pe-lg-1 p-0">
                                                    <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search
                                                            <input type="search" class="form-control" placeholder="Search Invoice"></label></div>
                                                    <div class="invoice_status ms-sm-2">
                                                        <select id="UserRole"class="form-select ms-50 text-capitalize" style="margin-right: 1rem;margin-top: 8px;">
                                                            <option value=""> Select Status </option>
                                                            <option value="Edit" class="text-capitalize">Edit</option>
                                                            <option value="View" class="text-capitalize">View</option>
                                                        </select></div>
                                                </div>
                                            </div>
                                            <table class="invoice-list-table table dataTable no-footer dtr-column" id="DataTables_Table_0"
                                                   role="grid" aria-describedby="DataTables_Table_0_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="control sorting lidget-column-show" tabindex="0" rowspan="1" colspan="1"
                                                            aria-label=": activate to sort column ascending" style="display: none;"></th>
                                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 46px;"
                                                            aria-label="#: activate to sort column ascending">#</th>
                                                        <th class="sorting lidget-column-hidden-5" tabindex="0" rowspan="1" colspan="1" style="width: 42px;"
                                                            aria-label=": activate to sort column ascending">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                                 stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                 class="feather feather-trending-up">
                                                            <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>
                                                            <polyline points="17 6 23 6 23 12"></polyline>
                                                            </svg></th>
                                                        <th class="sorting sorting_desc" tabindex="0" rowspan="1" colspan="1" style="width: 270px;"
                                                            aria-label="Name: activate to sort column ascending" aria-sort="descending">Name</th>
                                                        <th class="sorting lidget-column-hidden-4" tabindex="0" rowspan="1" colspan="1" style="width: 73px;"
                                                            aria-label="Total: activate to sort column ascending">Total</th>
                                                        <th class="text-truncate sorting lidget-column-hidden-3" tabindex="0" rowspan="1" colspan="1" style="width: 130px;"
                                                            aria-label="Issued Date: activate to sort column ascending">Shipped Date</th>
                                                        <th class="sorting lidget-column-hidden-2" tabindex="0" rowspan="1" colspan="1" style="width: 98px;"
                                                            aria-label="Status: activate to sort column ascending">Status</th>
                                                        <th class="cell-fit sorting_disabled lidget-column-hidden" rowspan="1" colspan="1" style="width: 80px;"
                                                            aria-label="Actions">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="odd">
                                                        <td class=" control lidget-column-show" tabindex="0" style="display: none;"></td>
                                                        <td class=""><a class="fw-bold" href="app-invoice-preview.jsp"> #4881</a></td>
                                                        <td class="lidget-column-hidden-5-lite"><span data-bs-toggle="tooltip" data-bs-html="true">
                                                                <div class="avatar avatar-status bg-light-success lidget-column-hidden-5"><span class="avatar-content"><svg
                                                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                                            stroke-linejoin="round" class="feather feather-check-circle avatar-icon">
                                                                        <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                        <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                        </svg></span></div>
                                                            </span></td>
                                                        <td class="sorting_1">
                                                            <div class="d-flex justify-content-left align-items-center">
                                                                <div class="avatar-wrapper">
                                                                    <div class="avatar bg-light-danger me-50">
                                                                        <div class="avatar-content">ZK</div>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex flex-column">
                                                                    <h6 class="user-name text-truncate mb-0">Zeb Kenningham</h6><small
                                                                        class="text-truncate text-muted">zebkenningham@email.com</small>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="lidget-column-hidden-4"><span class="d-none">2771</span>$2771</td>
                                                        <td class="lidget-column-hidden-3"><span class="d-none">20190624</span>24 Jun 2019</td>
                                                        <td class="lidget-column-hidden-2"><span class="badge rounded-pill badge-light-success" text-capitalized=""> Paid </span>
                                                        </td>
                                                        <td class="lidget-column-hidden">
                                                            <div class="d-flex align-items-center col-actions">
                                                                <a href="app-invoice-edit.jsp" class="me-1" href="#" data-bs-toggle="tooltip"
                                                                   data-bs-placement="top" title="" data-bs-original-title="Edit Orders"
                                                                   aria-label="Edit Orders">
                                                                    <i data-feather='edit-2'></i>
                                                                </a>
                                                                <a class="me-25" href="app-invoice-preview.jsp" data-bs-toggle="tooltip"
                                                                   data-bs-placement="top" title="" data-bs-original-title="View Order"
                                                                   aria-label="View Order"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                                             stroke-linecap="round" stroke-linejoin="round"
                                                                                             class="feather feather-eye font-medium-2 text-body">
                                                                    <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                    <circle cx="12" cy="12" r="3"></circle>
                                                                    </svg>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-between mx-2 row">
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">
                                                        Showing 1 to 10 of 50 entries</div>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                                        <ul class="pagination">
                                                            <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous"><a
                                                                    href="#" data-dt-idx="0" tabindex="0" class="page-link">&nbsp;</a></li>
                                                            <li class="paginate_button page-item active"><a href="#" data-dt-idx="1" tabindex="0"
                                                                                                            class="page-link">1</a></li>
                                                            <li class="paginate_button page-item "><a href="#" data-dt-idx="2" tabindex="0"
                                                                                                      class="page-link">2</a></li>
                                                            <li class="paginate_button page-item "><a href="#" data-dt-idx="3" tabindex="0"
                                                                                                      class="page-link">3</a></li>
                                                            <li class="paginate_button page-item "><a href="#" data-dt-idx="4" tabindex="0"
                                                                                                      class="page-link">4</a></li>
                                                            <li class="paginate_button page-item "><a href="#" data-dt-idx="5" tabindex="0"
                                                                                                      class="page-link">5</a></li>
                                                            <li class="paginate_button page-item next" id="DataTables_Table_0_next">
                                                                <a href="#" data-dt-idx="6" tabindex="0" class="page-link">&nbsp;</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--/ List DataTable -->
                    </section>
                    <!-- Dashboard Analytics end -->

                </div>
            </div>
        </div>
        <!-- END: Content-->


        <!-- BEGIN: Customizer-->
        <div class="customizer d-none d-md-block"><a
                class="customizer-toggle d-flex align-items-center justify-content-center" href="#"><i class="spinner"
                                                                                                   data-feather="settings"></i></a>
            <div class="customizer-content">
                <!-- Customizer header -->
                <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                    <h4 class="mb-0">Theme Customizer</h4>
                    <p class="m-0">Customize & Preview in Real Time</p>

                    <a class="customizer-close" href="#"><i data-feather="x"></i></a>
                </div>

                <hr />

                <!-- Styling & Text Direction -->
                <div class="customizer-styling-direction px-2">
                    <p class="fw-bold">Skin</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="skinlight" name="skinradio" class="form-check-input layout-name" checked
                                   data-layout="" />
                            <label class="form-check-label" for="skinlight">Light</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skinbordered" name="skinradio" class="form-check-input layout-name"
                                   data-layout="bordered-layout" />
                            <label class="form-check-label" for="skinbordered">Bordered</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skindark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="dark-layout" />
                            <label class="form-check-label" for="skindark">Dark</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="skinsemidark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="semi-dark-layout" />
                            <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                        </div>
                    </div>
                </div>

                <hr />

                <!-- Menu -->
                <div class="customizer-menu px-2">
                    <div id="customizer-menu-collapsible" class="d-flex">
                        <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                        <div class="form-check form-check-primary form-switch">
                            <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                            <label class="form-check-label" for="collapse-sidebar-switch"></label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Layout Width -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Layout Width</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                            <label class="form-check-label" for="layout-width-full">Full Width</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Navbar -->
                <div class="customizer-navbar px-2">
                    <div id="customizer-navbar-colors">
                        <p class="fw-bold">Navbar Color</p>
                        <ul class="list-inline unstyled-list">
                            <li class="color-box bg-white border selected" data-navbar-default=""></li>
                            <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                            <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                            <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                            <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                            <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                            <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                            <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                        </ul>
                    </div>

                    <p class="navbar-type-text fw-bold">Navbar Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                            <label class="form-check-label" for="nav-type-floating">Floating</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-static">Static</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Footer -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Footer Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                            <label class="form-check-label" for="footer-type-static">Static</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End: Customizer-->
    </div>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <!-- BEGIN: MODAL -->
    <div class="modal fade dtr-bs-modal" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Details of Shamus Tuttle</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <tbody>
                            <tr data-dt-row="undefined" data-dt-column="1">
                                <td>#:</td>
                                <td><a class="fw-bold" href="app-invoice-preview.jsp"> #5041</a></td>
                            </tr>
                            <tr data-dt-row="undefined" data-dt-column="3">
                                <td>Name:</td>
                                <td>
                                    <div class="d-flex justify-content-left align-items-center">
                                        <div class="avatar-wrapper">
                                            <div class="avatar me-50"><img src="app-assets/images/avatars/1-small.png" alt="Avatar"
                                                                           width="32" height="32"></div>
                                        </div>
                                        <div class="d-flex flex-column">
                                            <h6 class="user-name text-truncate mb-0">Shamus Tuttle</h6><small
                                                class="text-truncate text-muted">shamustuttle@email.com</small>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr data-dt-row="undefined" data-dt-column="4">
                                <td>Total:</td>
                                <td><span class="d-none">2230</span>$2230</td>
                            </tr>
                            <tr data-dt-row="undefined" data-dt-column="5">
                                <td>Shipped Date:</td>
                                <td><span class="d-none">20191119</span>19 Nov 2019</td>
                            </tr>
                            <tr data-dt-row="undefined" data-dt-column="6">
                                <td>Status:</td>
                                <td><span class="badge rounded-pill badge-light-success" text-capitalized=""> Paid </span></td>
                            </tr>
                            <tr data-dt-row="undefined" data-dt-column="8">
                                <td>Actions:</td>
                                <td>
                                    <div class="d-flex align-items-center col-actions">
                                        <a href="app-invoice-edit.jsp" class="me-1" href="#" data-bs-toggle="tooltip"
                                           data-bs-placement="top" title="" data-bs-original-title="Edit Orders"
                                           aria-label="Edit Orders">
                                            <i data-feather='edit-2'></i>
                                        </a>
                                        <a class="me-25" href="app-invoice-preview.jsp" data-bs-toggle="tooltip"
                                           data-bs-placement="top" title="" data-bs-original-title="View Order"
                                           aria-label="View Order"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                     stroke-linecap="round" stroke-linejoin="round"
                                                                     class="feather feather-eye font-medium-2 text-body">
                                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                            <circle cx="12" cy="12" r="3"></circle>
                                            </svg>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END: MODAL -->




    <!-- BEGIN: Vendor JS-->
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/charts/apexcharts.min.js"></script>
    <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
    <script src="app-assets/vendors/js/extensions/moment.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.min.js"></script>
    <script src="app-assets/js/core/app.min.js"></script>
    <script src="app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="app-assets/js/scripts/pages/dashboard-analytics.min.js"></script>
    <!-- END: Page JS-->

    <!-- BEGIN: My JS-->
    <script src="assets/js/authorized.js"></script>
    <!-- END: My JS-->
    <script>
        $(window).on('load', function () {
            if (feather) {
                feather.replace({width: 14, height: 14});
            }
        });
    </script>
</body>
<!-- END: Body-->

</html>