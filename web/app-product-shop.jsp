<%-- 
    Document   : app-ecommerce-shop
    Created on : Sep 23, 2022, 8:37:36 AM
    Author     : ADMIN
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/nouislider.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/css/plugins/extensions/ext-component-sliders.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/app-ecommerce.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body
        class="vertical-layout vertical-menu-modern content-detached-left-sidebar navbar-floating footer-static   menu-collapsed"
        data-open="click" data-menu="vertical-menu-modern" data-col="content-detached-left-sidebar">

        <!-- BEGIN: Header-->
        <nav
            class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block">
                            <a class="nav-link" href="app-email.html" data-bs-toggle="tooltip"
                               data-bs-placement="bottom" title="Contact"><i class="ficon" data-feather="mail"></i></a></li>
                        <li class="nav-item d-none d-lg-block">
                            <a class="nav-link" href="app-email.html" data-bs-toggle="tooltip"
                               data-bs-placement="bottom" title="Help"><i class="ficon" data-feather="help-circle"></i></a></li>

                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning"
                                                                                                    data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language">
                        <a class="nav-link dropdown-toggle" id="dropdown-flag" href="#"
                           data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag">
                            <a class="dropdown-item" href="#"
                               data-language="en"><i class="flag-icon flag-icon-us"></i> English</a>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style">
                            <i class="ficon" data-feather="moon"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon" data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Vuexy..." tabindex="-1"
                                   data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-cart me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i
                                class="ficon" data-feather="shopping-cart"></i><span
                                class="badge rounded-pill bg-primary badge-up cart-item-count">6</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">My Cart</h4>
                                    <div class="badge rounded-pill badge-light-primary">4 Items</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list">
                                <div class="list-item align-items-center">
                                    <img class="d-block rounded me-1" src="app-assets/images/pages/eCommerce/1.png" alt="donuts" width="62">
                                    <div class="list-item-body flex-grow-1">
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-product-details.jsp"> Honda MX-219 2019</a>
                                            </h6>
                                            <div class="flex-default" style="column-gap:0.5rem">
                                                <div class="text-body badge rounded-pill badge-light-primary">Honda</div>
                                                <div class="text-body badge rounded-pill badge-light-primary">Chopper Version</div>
                                                <div class="text-body badge rounded-pill badge-light-primary">Rental Option</div>
                                            </div>
                                            <div class="flex-default" style="column-gap:0.5rem">
                                                <div class="text-body badge rounded-pill badge-light-success mt-1">Quantity: 1</div>
                                                <div class="text-body badge rounded-pill badge-light-danger mt-1">Tourist: None</div>
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$374.90</h5>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-menu-footer">
                                <div class="d-flex justify-content-between mb-1">
                                    <h6 class="fw-bolder mb-0">Total:</h6>
                                    <h6 class="text-primary fw-bolder mb-0">$10,999.00</h6>
                                </div><a class="btn btn-primary w-100" href="checkOutController">Checkout</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#"
                                                                                 data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span
                                class="badge rounded-pill bg-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-15.jpg"
                                                                     alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small
                                                class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar"
                                                                     width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small
                                                class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small
                                                class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="list-item d-flex align-items-center">
                                    <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                    <div class="form-check form-check-primary form-switch">
                                        <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="form-check-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small
                                                class="notification-text"> USA Server is down due to high CPU usage</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small
                                                class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small
                                                class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle dropdown-user-link"
                           id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none lidget-naz" current-login="${user.getEmail()}">
                                <span class="user-name fw-bolder">${user.getFirstName()} ${user.getLastName()}</span>
                                <span class="user-status" style="margin-top:0.25rem">${user.getRole()}</span></div>
                            <span class="avatar">
                                <c:choose>
                                    <c:when test="${user.getAvatar() == null}">
                                        <c:set var="firstName" value="${user.getFirstName()}"/>
                                        <c:set var="lastName" value="${user.getLastName()}"/>
                                        <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                            <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                        </div>
                                    </c:when>
                                    <c:when test="${user.getAvatar() != null}">
                                        <img class="round" src="${user.getAvatar()}" alt="avatar" height="40"
                                             width="40">   
                                    </c:when>
                                </c:choose> 
                                <span class="avatar-status-online"></span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="userProfileController"><i class="me-50" data-feather="user"></i> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="page-account-settings-account.jsp"><i
                                    class="me-50" data-feather="settings"></i> Settings
                            </a>
                            <a class="dropdown-item" href="page-faq.jsp"><i class="me-50" data-feather="help-circle"></i> FAQ
                            </a>
                            <a class="dropdown-item" href="logoutController">
                                <i class="me-50" data-feather="power"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd
                                Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small>
                </a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="userProfileController">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div>
                </a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a
                    class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No
                            results found.</span></div>
                </a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item me-auto">
                        <a class="navbar-brand position-relative" style="top:-25px" href="#">
                            <span>
                                <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo position-relative"
                                     style="left:-18px">
                            </span>
                            <h2 class="brand-text mb-0" style="position: relative; right: 35px;">Hilfsmotor</h2>
                        </a>
                    </li>
                    <li class="nav-item nav-toggle lidget-position-control"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                                class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                                class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc"
                                data-ticon="disc"></i></a></li>
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content">
                <ul class="navigation navigation-main mt-1" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" navigation-header"><span data-i18n="Misc">Apps & Page</span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="shopping-cart"></i><span
                                class="menu-title text-truncate" data-i18n="eCommerce">Product</span></a>
                        <ul class="menu-content">
                            <li class="active"><a class="d-flex align-items-center" href="#"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Shop">Shop</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="store-list"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="store-list">Store</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="wishListController"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Wish List">Wish
                                        List</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="checkOutController"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Checkout">Checkout</span></a>
                            </li>
                            <li ><a class="d-flex align-items-center" href="customerOrderItemList"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Checkout">My Order</span></a>
                            </li>
                            <c:choose>
                                <c:when test="${account.role == 'STAFF'}">
                            <li><a class="d-flex align-items-center" href="productController"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Manage Product">Manage Product</span></a>
                            </li>
                            </c:when>
                            </c:choose>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="user"></i><span
                                class="menu-title text-truncate" data-i18n="User">User</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="View">View</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="userProfileController"><span
                                                class="menu-item text-truncate" data-i18n="Account">Account</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-security.html"><span
                                                class="menu-item text-truncate" data-i18n="Security">Security</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-billing.html"><span
                                                class="menu-item text-truncate" data-i18n="Billing &amp; Plans">Billing &amp; Plans</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-notifications.html"><span
                                                class="menu-item text-truncate" data-i18n="Notifications">Notifications</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="app-user-view-connections.html"><span
                                                class="menu-item text-truncate" data-i18n="Connections">Connections</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span
                                class="menu-title text-truncate" data-i18n="Pages">Pages</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="help-page"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="FAQ">FAQ</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Blog">Blog</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="blog-list"><span
                                                class="menu-item text-truncate" data-i18n="List">List</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="manage-blog"><span
                                                class="menu-item text-truncate" data-i18n="Detail">Add</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="blog-draft"><span
                                                class="menu-item text-truncate" data-i18n="Detail">Draft</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="blog-save"><span
                                                class="menu-item text-truncate" data-i18n="Edit">Saved</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" navigation-header"><span data-i18n="Misc">Misc</span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="menu"></i><span
                                class="menu-title text-truncate" data-i18n="Menu Levels">Menu Levels</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.1</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.2</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate"
                                                                                            data-i18n="Third Level">Third Level 3.1</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate"
                                                                                            data-i18n="Third Level">Third Level 3.2</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content ecommerce-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <div class="row breadcrumbs-top">
                            <div class="col-12">
                                <h2 class="content-header-title float-start mb-0">Shop</h2>
                                <div class="breadcrumb-wrapper">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Home</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#">Product</a>
                                        </li>
                                        <li class="breadcrumb-item active">Shop
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                        <div class="mb-1 breadcrumb-right">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <a class="dropdown-item" href="app-email.html">
                                        <i class="me-1" data-feather="mail"></i>
                                        <span class="align-middle">Contact</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-detached content-right">
                    <div class="content-body">
                        <!-- E-commerce Content Section Starts -->
                        <section id="ecommerce-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="ecommerce-header-items">
                                        <div class="result-toggler">
                                            <button class="navbar-toggler shop-sidebar-toggler" type="button" data-bs-toggle="collapse">
                                                <span class="navbar-toggler-icon d-block d-lg-none"><i data-feather="menu"></i></span>
                                            </button>
                                        </div>
                                        <div class="view-options d-flex">
                                            <div class="btn-group dropdown-sort">
                                                <button type="button" id="selectOption" class="btn btn-outline-primary dropdown-toggle me-1"
                                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="active-sorting" id="sortingChoice">Sort</span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="#">All</a>
                                                    <a class="dropdown-item" href="#">Popular</a>
                                                    <a class="dropdown-item" href="#">Lowest Price</a>
                                                    <a class="dropdown-item" href="#">Highest Price</a>
                                                </div>
                                            </div>
                                            <div class="btn-group dropdown-update">
                                                <button type="button" id="selectOption-1" class="btn btn-outline-primary dropdown-toggle update me-1"
                                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="active-update" id="sortingChoice-1">Post</span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="#">All</a>
                                                    <a class="dropdown-item" href="#">Newest</a>
                                                    <a class="dropdown-item" href="#">Oldest</a
                                                </div>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <input type="radio" class="btn-check" name="radio_options" id="radio_option1" autocomplete="off"
                                                       checked />
                                                <label class="btn btn-icon btn-outline-primary view-btn grid-view-btn" for="radio_option1"><i
                                                        data-feather="grid" class="font-medium-3"></i></label>
                                                <input type="radio" class="btn-check" name="radio_options" id="radio_option2"
                                                       autocomplete="off" />
                                                <label class="btn btn-icon btn-outline-primary view-btn list-view-btn" for="radio_option2"><i
                                                        data-feather="list" class="font-medium-3"></i></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- E-commerce Content Section Starts -->

                        <!-- background Overlay when sidebar is shown  starts-->
                        <div class="body-content-overlay"></div>
                        <!-- background Overlay when sidebar is shown  ends-->

                        <!-- E-commerce Search Bar Starts -->
                        <section id="ecommerce-searchbar" class="ecommerce-searchbar">
                            <div class="row mt-1">
                                <div class="col-sm-12">
                                    <div class="input-group input-group-merge">
                                        <input type="text" class="form-control search-product" id="shop-search" placeholder="Search Product"
                                               aria-label="Search..." aria-describedby="shop-search" />
                                        <span class="input-group-text"><i data-feather="search" class="text-muted"></i></span>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- E-commerce Search Bar Ends -->

                        <!-- E-commerce Products Starts -->
                        <section id="ecommerce-products" style="transition: 0.6s" class="grid-view">
       
                        </section>
                        <!-- E-commerce Products Ends -->

                        <!-- E-commerce Pagination Starts -->

                        <section id="ecommerce-pagination" style="transition: 0.6s;opacity:0;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <nav aria-label="Page navigation">
                                        <ul class="pagination justify-content-center mt-2" id="pagination-slider">
                                            <li class="page-item first"><a href="#" class="page-link">First</a></li>
                                            <li class="page-item prev"><a href="#" class="page-link">Prev</a></li>
                                            <li class="page-item next"><a href="#" class="page-link">Next</a></li>
                                            <li class="page-item last"><a href="#" class="page-link">Last</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </section>
                        <!-- E-commerce Pagination Ends -->

                    </div>
                </div>
                <div class="sidebar-detached sidebar-left">
                    <div class="sidebar">
                        <!-- Ecommerce Sidebar Starts -->
                        <div class="sidebar-shop">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h6 class="filter-heading d-none d-lg-block">Filters</h6>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <!-- Price Filter starts -->
                                    <div class="multi-range-price">
                                        <h6 class="filter-title mt-0">Multi Range</h6>
                                        <ul class="list-unstyled price-range" id="price-range">
                                            <li>
                                                <div class="form-check">
                                                    <input type="radio" id="priceAll" name="price-range" value="All" class="form-check-input lidget-form-checkbox" checked />
                                                    <label class="form-check-label" for="priceAll">All</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input type="radio" id="priceRange1" name="price-range" value="=<10" class="form-check-input lidget-form-checkbox" />
                                                    <label class="form-check-label" for="priceRange1">&lt;=$10</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input type="radio" id="priceRange2" name="price-range" value="10-100" class="form-check-input lidget-form-checkbox" />
                                                    <label class="form-check-label" for="priceRange2">$10 - $100</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input type="radio" id="priceARange3" name="price-range" value="100-500" class="form-check-input lidget-form-checkbox" />
                                                    <label class="form-check-label" for="priceARange3">$100 - $500</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input type="radio" id="priceRange4" name="price-range" value=">=500" class="form-check-input lidget-form-checkbox" />
                                                    <label class="form-check-label" for="priceRange4">&gt;= $500</label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Price Filter ends -->

                                    <!-- Price Slider starts -->
                                    <div class="price-slider">
                                        <h6 class="filter-title">Price Range</h6>
                                        <div class="price-slider">
                                            <div class="range-slider mt-2" id="slider-price"></div>
                                        </div>
                                    </div>
                                    <!-- Price Range ends -->

                                    <!-- Categories Starts -->
                                    <div id="product-categories">
                                        <h6 class="filter-title">Categories</h6>
                                        <ul class="list-unstyled categories-list">
                                            <li>
                                                <div class="form-check">
                                                    <input type="radio" id="category1" name="category-filter" value="All" class="form-check-input categoryList" checked />
                                                    <label class="form-check-label" for="category1">All</label>
                                                </div>
                                            </li>
                                            <c:forEach items="${sessionScope.categoryList}" var="category">
                                                <li>
                                                    <div class="form-check">
                                                        <input type="radio" id="category1" name="category-filter" value="${category.key}" class="form-check-input categoryList"/>
                                                        <label class="form-check-label" for="category1">${category.key}</label>
                                                    </div>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <!-- Categories Ends -->

                                    <!-- Brands starts -->
                                    <div class="brands">
                                        <h6 class="filter-title">Brands</h6>
                                        <ul class="list-unstyled brand-list">
                                            <li>
                                                <div class="form-check">
                                                    <input type="radio" name="brand-filter" value="All" class="form-check-input brandList" id="productBrand1" checked/>
                                                    <label class="form-check-label" for="productBrand1">All</label>
                                                </div>
                                                <span>${totalProduct}</span>
                                            </li>
                                            <c:forEach items="${sessionScope.brandList}" var="brand">
                                                <li>
                                                    <div class="form-check">
                                                        <input type="radio" name="brand-filter" value="${brand.key}" class="form-check-input brandList" id="productBrand1" />
                                                        <label class="form-check-label" for="productBrand1">${brand.key}</label>
                                                    </div>
                                                    <span>${brand.value}</span>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <!-- Brand ends -->
                                    <div class="mode">
                                        <h6 class="filter-title">Modes</h6>
                                        <ul class="list-unstyled brand-list">
                                            <li>
                                                <div class="form-check">
                                                    <input type="radio" name="mode-filter" value="All" class="form-check-input modeProduct" id="mode1" checked/>
                                                    <label class="form-check-label" for="mode1">All</label>
                                                </div>
                                                <span>${totalProduct}</span>
                                            </li>
                                            <c:forEach items="${sessionScope.modeList}" var="mode">
                                                <li>
                                                    <div class="form-check">
                                                        <input type="radio" name="mode-filter" value="${mode.key}" class="form-check-input modeProduct" id="mode2" />
                                                        <label class="form-check-label" for="mode2">${mode.key}</label>
                                                    </div>
                                                    <span>${mode.value}</span>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <!-- Rating starts -->
                                    <div id="ratings">
                                        <h6 class="filter-title">Ratings</h6>
                                        <c:forEach items="${sessionScope.ratingList}" var="rating">
                                            <c:choose>
                                                <c:when test="${rating.key == '1'}">
                                                    <div class="ratings-list">
                                                        <a href="#">
                                                            <ul class="unstyled-list list-inline">
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </ul>
                                                        </a>
                                                        <div class="stars-received">${rating.value}</div>
                                                    </div>
                                                </c:when>
                                                <c:when test="${rating.key == '2'}">
                                                    <div class="ratings-list">
                                                        <a href="#">
                                                            <ul class="unstyled-list list-inline">
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </ul>
                                                        </a>
                                                        <div class="stars-received">${rating.value}</div>
                                                    </div>
                                                </c:when>
                                                <c:when test="${rating.key == '3'}">
                                                    <div class="ratings-list">
                                                        <a href="#">
                                                            <ul class="unstyled-list list-inline">
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </ul>
                                                        </a>
                                                        <div class="stars-received">${rating.value}</div>
                                                    </div>
                                                </c:when>
                                                <c:when test="${rating.key == '4'}">
                                                    <div class="ratings-list">
                                                        <a href="#">
                                                            <ul class="unstyled-list list-inline">
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                            </ul>
                                                        </a>
                                                        <div class="stars-received">${rating.value}</div>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="ratings-list">
                                                        <a href="#">
                                                            <ul class="unstyled-list list-inline">
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                            </ul>
                                                        </a>
                                                        <div class="stars-received">${rating.value}</div>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </div>
                                    <!-- Rating ends -->

                                    <!-- Clear Filters Starts -->
                                    <div id="clear-filters">
                                        <button type="button" class="btn w-100 btn-primary lidget-clear-filter">Clear All Filters</button>
                                    </div>
                                    <!-- Clear Filters Ends -->
                                </div>
                            </div>
                        </div>
                        <!-- Ecommerce Sidebar Ends -->

                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content-->


        <!-- BEGIN: Customizer-->
        <div class="customizer d-none d-md-block"><a
                class="customizer-toggle d-flex align-items-center justify-content-center" href="#">
                <i class="spinner" data-feather="settings"></i></a>
            <div class="customizer-content">
                <!-- Customizer header -->
                <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                    <h4 class="mb-0">Theme Customizer</h4>
                    <p class="m-0">Customize & Preview in Real Time</p>

                    <a class="customizer-close" href="#"><i data-feather="x"></i></a>
                </div>

                <hr />

                <!-- Styling & Text Direction -->
                <div class="customizer-styling-direction px-2">
                    <p class="fw-bold">Skin</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="skinlight" name="skinradio" class="form-check-input layout-name" checked
                                   data-layout="" />
                            <label class="form-check-label" for="skinlight">Light</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skinbordered" name="skinradio" class="form-check-input layout-name"
                                   data-layout="bordered-layout" />
                            <label class="form-check-label" for="skinbordered">Bordered</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skindark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="dark-layout" />
                            <label class="form-check-label" for="skindark">Dark</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="skinsemidark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="semi-dark-layout" />
                            <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                        </div>
                    </div>
                </div>

                <hr />

                <!-- Menu -->
                <div class="customizer-menu px-2">
                    <div id="customizer-menu-collapsible" class="d-flex">
                        <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                        <div class="form-check form-check-primary form-switch">
                            <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                            <label class="form-check-label" for="collapse-sidebar-switch"></label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Layout Width -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Layout Width</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                            <label class="form-check-label" for="layout-width-full">Full Width</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Navbar -->
                <div class="customizer-navbar px-2">
                    <div id="customizer-navbar-colors">
                        <p class="fw-bold">Navbar Color</p>
                        <ul class="list-inline unstyled-list">
                            <li class="color-box bg-white border selected" data-navbar-default=""></li>
                            <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                            <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                            <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                            <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                            <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                            <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                            <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                        </ul>
                    </div>

                    <p class="navbar-type-text fw-bold">Navbar Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                            <label class="form-check-label" for="nav-type-floating">Floating</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-static">Static</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Footer -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Footer Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                            <label class="form-check-label" for="footer-type-static">Static</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End: Customizer-->

        <!-- Buynow Button-->
        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix mb-0">
            <div class="float-md-start d-block d-md-inline-block mt-25" style="width:100%">COPYRIGHT &copy; 2022
                <a class="ms-25" href="#" target="_blank">Hilfsmotor
                </a>
                <span class="d-none d-sm-inline-block">, All rights
                    Reserved
                </span>
            </div>
        </p>
    </footer>
    <button class="lidget-shop-button btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/pagination/jquery.bootpag.min.js"></script>
    <script src="app-assets/vendors/js/pagination/jquery.twbsPagination.min.js"></script>

    <script src="app-assets/vendors/js/extensions/wNumb.min.js"></script>
    <script src="app-assets/vendors/js/extensions/nouislider.min.js"></script>
    <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.min.js"></script>
    <script src="app-assets/js/core/app.min.js"></script>
    <script src="app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="app-assets/js/scripts/pages/app-ecommerce.min.js"></script>
    <script src="app-assets/js/scripts/pagination/components-pagination.js"></script>

    <!-- END: Page JS-->

    <!-- BEGIN: My JS-->
    <script id="auth-shop" data-value="${fn:length(numberPaginationList)}" src="assets/js/auth_shopping.js"></script>
    <!-- END: My JS-->
    <script>
        $(window).on('load', function () {
            if (feather) {
                feather.replace({width: 14, height: 14});
            }
        });
    </script>
</body>
<!-- END: Body-->

</html>