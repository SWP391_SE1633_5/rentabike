<%-- 
    Document   : app-user-list
    Created on : Oct 8, 2022, 1:55:40 PM
    Author     : ADMIN
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible"q content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css"
              href="app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-validation.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover"
          data-menu="horizontal-menu" data-col="">

        <!-- BEGIN: Header-->
        <nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center"
             data-nav="brand-center">
            <div class="navbar-header d-xl-block d-none">
                <ul class="nav navbar-nav">
                    <li class="nav-item">
                        <a class="navbar-brand" href="dashboard-analytics.jsp">
                            <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                            <h2 class="brand-text mb-0 lidget-app__padding-left">Hilfsmotor</h2>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calendar.html" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Calendar"><i class="ficon" data-feather="calendar"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning"
                                                                                                    data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="#"
                                                                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"
                                                                                                        data-language="en"><i class="flag-icon flag-icon-us"></i> English</a>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon"
                                                                                                 data-feather="sun"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon"
                                                                                           data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Hilfmotor..." tabindex="-1"
                                   data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#"
                                                                                 data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span
                                class="badge rounded-pill bg-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar">
                                                <img src="app-assets/images/portrait/small/avatar-s-15.jpg"
                                                     alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small
                                                class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar"
                                                                     width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small
                                                class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small
                                                class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="list-item d-flex align-items-center">
                                    <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                    <div class="form-check form-check-primary form-switch">
                                        <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="form-check-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small
                                                class="notification-text"> USA Server is down due to hight CPU usage</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small
                                                class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small
                                                class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle dropdown-user-link"
                           id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none">
                                <span class="user-name fw-bolder">${staff.getUserName()}</span>
                                <span class="user-status" data="${staff.getPermissionDetails()}">${staff.getRole()}</span></div>
                            <span class="avatar">
                                <c:choose>
                                    <c:when test="${staff.getAvatar() == null}">
                                        <c:set var="firstName" value="${staff.getFirstName()}"/>
                                        <c:set var="lastName" value="${staff.getLastName()}"/>
                                        <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                            <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                        </div>
                                    </c:when>
                                    <c:when test="${staff.getAvatar() != null}">
                                        <img class="round" src="${staff.getAvatar()}" alt="avatar" height="40"
                                             width="40">   
                                    </c:when>
                                </c:choose> 
                                <span class="avatar-status-online"></span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="page-profile.jsp"><i class="me-50" data-feather="user"></i> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="page-account-settings-account.jsp"><i
                                    class="me-50" data-feather="settings"></i> Settings
                            </a>
                            <a class="dropdown-item" href="page-faq.jsp"><i class="me-50" data-feather="help-circle"></i> FAQ
                            </a>
                            <a class="dropdown-item" href="logoutController">
                                <i class="me-50" data-feather="power"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd
                                Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small>
                </a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6>
                </a></li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between py-50 w-100"
                   href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75">
                            <img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png"
                                 height="32">
                        </div>
                        <!--<div class="search-data">-->
                        <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                    </div>
                </a>
            </li>
            <li class="auto-suggestion">
                <a class="d-flex align-items-center justify-content-between py-50 w-100"
                   href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div>
                </a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a
                    class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No
                            results found.</span></div>
                </a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="horizontal-menu-wrapper">
            <div
                class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-dark navbar-shadow menu-border container-xxl"
                role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav">
                <div class="navbar-header" style="margin-bottom:2rem;">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item me-auto"><a class="navbar-brand"
                                                        href="html/ltr/horizontal-menu-template-dark/dashboard-analytics.jsp">
                                <span>
                                    <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                                </span>
                                <h2 class="brand-text mb-0" style="position: relative; right: 25px;">Hilfsmotor</h2>
                            </a></li>
                        <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                                    class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i></a></li>
                    </ul>
                </div>
                <div class="shadow-bottom"></div>
                <!-- Horizontal menu content-->
                <div class="navbar-container main-menu-content" data-menu="menu-container">
                    <!-- include includes/mixins-->
                    <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                        <li class="dropdown nav-item" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link d-flex align-items-center" href="dashboard-analytics.jsp" data-bs-toggle="dropdown"><i
                                    data-feather="home"></i><span data-i18n="Dashboards">Dashboards</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="dashboard-analytics.jsp"
                                                    data-bs-toggle="" data-i18n="Analytics"><i data-feather="activity"></i><span
                                            data-i18n="Analytics">Analytics</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown nav-item" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link d-flex align-items-center" href="#"
                                data-bs-toggle="dropdown"><i data-feather="package"></i><span data-i18n="Apps">Apps</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Invoice"><i data-feather="file-text"></i><span data-i18n="Invoice">Orders</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-invoice-list.html"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Roles &amp; Permission"><i data-feather="shield"></i><span
                                            data-i18n="Roles &amp; Permission">Roles &amp; Permission</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="roleListController"
                                                            data-bs-toggle="" data-i18n="Roles"><i data-feather="circle"></i><span
                                                    data-i18n="Roles">Roles</span></a>
                                        </li>
                                        <li class="active" data-menu=""><a class="dropdown-item d-flex align-items-center"
                                                                           href="permissionListController" data-bs-toggle="" data-i18n="Permission"><i
                                                    data-feather="circle"></i><span data-i18n="Permission">Permission</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="eCommerce"><i data-feather='shopping-bag'></i><span
                                            data-i18n="eCommerce">Product</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menAu="">
                                            <a class="dropdown-item d-flex align-items-center" href="app-ecommerce-list.html"
                                               data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="User"><i data-feather="user"></i><span data-i18n="User">User</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="#/"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                                class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                                data-i18n="View"><i data-feather="circle"></i><span data-i18n="View">View</span></a>
                                            <ul class="dropdown-menu" data-bs-popper="none">
                                                <li data-menu=""><a class="dropdown-item d-flex align-items-center"
                                                                    href="app-user-view-account.html" data-bs-toggle="" data-i18n="Account"><i
                                                            data-feather="circle"></i><span data-i18n="Account">Account</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <!-- users list start -->
                    <section class="app-user-list">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body d-flex align-items-center justify-content-between">
                                        <div>
                                            <h3 class="fw-bolder mb-75">${totalAccount}</h3>
                                            <span>Total Users</span>
                                        </div>
                                        <div class="avatar bg-light-primary p-50">
                                            <span class="avatar-content">
                                                <i data-feather="user" class="font-medium-4"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body d-flex align-items-center justify-content-between">
                                        <div>
                                            <h3 class="fw-bolder mb-75">${activeAccountTotal}</h3>
                                            <span>Active Users</span>
                                        </div>
                                        <div class="avatar bg-light-danger p-50">
                                            <span class="avatar-content">
                                                <i data-feather="user-plus" class="font-medium-4"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body d-flex align-items-center justify-content-between">
                                        <div>
                                            <h3 class="fw-bolder mb-75">${inactiveAccountTotal}</h3>
                                            <span>Inactive Users</span>
                                        </div>
                                        <div class="avatar bg-light-success p-50">
                                            <span class="avatar-content">
                                                <i data-feather="user-check" class="font-medium-4"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body d-flex align-items-center justify-content-between">
                                        <div>
                                            <h3 class="fw-bolder mb-75">${bannedAccountTotal}</h3>
                                            <span>Banned Users</span>
                                        </div>
                                        <div class="avatar bg-light-warning p-50">
                                            <span class="avatar-content">
                                                <i data-feather="user-x" class="font-medium-4"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- list and filter start -->
                        <div class="card">
                            <div class="card-body border-bottom">
                                <h4 class="card-title">Search &amp; Filter</h4>
                                <div class="row">
                                    <div class="col-md-4 user_role">
                                        <label class="form-label" for="UserRole">Role</label>
                                        <select id="UserRole" class="form-select text-capitalize mb-md-0 mb-2">
                                            <option value=""> Select Role </option>
                                            <option value="ADMIN" class="text-capitalize">Admin</option>
                                            <option value="STAFF" class="text-capitalize">Staff</option>
                                            <option value="CUSTOMER" class="text-capitalize">Customer</option>
                                            <option value="SALEPERSON" class="text-capitalize">Saleperson</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 user_plan">
                                        <label class="form-label" for="UserState">State</label>
                                        <select id="UserState" class="form-select text-capitalize mb-md-0 mb-2">
                                            <option value=""> Select State</option>
                                            <c:forEach items="${sessionScope.listState}" var="state">
                                                <option value="${state}" class="text-capitalize">${state}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="col-md-4 user_status"><label class="form-label" for="UserStatus">Status</label>
                                        <select id="UserStatus" class="form-select text-capitalize mb-md-0 mb-2xx">
                                            <option value=""> Select Status </option>
                                            <option value="Banned" class="text-capitalize">Banned</option>
                                            <option value="Active" class="text-capitalize">Active</option>
                                            <option value="Inactive" class="text-capitalize">Inactive</option>
                                        </select></div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                    <div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-50 mb-1">
                                        <div class="col-sm-12 col-md-4 col-lg-6">
                                            <div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select
                                                        name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" id="showEntry" class="form-select">
                                                        <c:forEach items="${sessionScope.showEntryList}" var="entry">
                                                            <c:choose>
                                                                <c:when test="${showEntry == entry}">
                                                                    <option value="${entry}" selected="">${entry}</option>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option value="${entry}">${entry}</option>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </select> entries</label></div>
                                        </div>
                                        <div class="col-sm-12 col-md-8 col-lg-6 ps-xl-75 ps-0">
                                            <div
                                                class="dt-action-buttons d-flex align-items-center justify-content-md-end justify-content-center flex-sm-nowrap flex-wrap">
                                                <div class="me-1">
                                                    <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:
                                                            <input type="search" id="searchUser"
                                                                   class="form-control" placeholder="Search name user..." aria-controls="DataTables_Table_0"></label></div>
                                                </div>
                                                <div class="dt-buttons d-inline-flex mt-50">
                                                    <button class="dt-button add-new btn btn-primary" tabindex="0" onclick="sendASignCheck()"
                                                            aria-controls="DataTables_Table_0" type="button"
                                                            data-bs-target="#modals-slide-in"><span>Add a new Staff</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="user-list-table table dataTable no-footer dtr-column" id="tableUserList" role="grid"
                                           aria-describedby="DataTables_Table_0_info" style="width: 1428px;">
                                        <thead class="table-light">
                                            <tr role="row">
                                                <th class="control sorting_disabled" rowspan="1" colspan="1" style="width: 0px; display: none;"
                                                    aria-label=""></th>
                                                <th class="sorting_disabled dt-checkboxes-cell dt-checkboxes-select-all" rowspan="1" colspan="1"
                                                    style="width: 25px;" data-col="1" aria-label="">
                                                    <div class="form-check"> <input class="form-check-input" type="checkbox" value=""
                                                                                    id="checkboxSelectAll"><label class="form-check-label" for="checkboxSelectAll"></label></div>
                                                </th>
                                                <th class="sorting ds-s non-sorting" id="sorting-name" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 361px;" aria-label="Name: activate to sort column ascending">Name</th>
                                                <th class="sorting ds-s non-sorting" id="sorting-role" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                                    colspan="1" style="width: 158px;" aria-label="Role: activate to sort column ascending"
                                                    aria-sort="descending">Role</th>
                                                <th class="sorting ds-s non-sorting" id="sorting-mobile" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 116px;" aria-label="Mobile: activate to sort column ascending">Mobile
                                                </th>
                                                <th class="sorting ds-s non-sorting" id="sorting-state" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 223px;" aria-label="State: activate to sort column ascending">State</th>
                                                <th class="sorting ds-s non-sorting" id="sorting-status" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                    style="width: 102px;" aria-label="Status: activate to sort column ascending">Status</th>
                                                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 104px;" aria-label="Actions">
                                                    Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lidget__list-user">
                                            <c:forEach items="${sessionScope.staffAccountList}" begin="${beginStaff}" end="${endStaff}" var="staffList">
                                                <tr>
                                                    <td class=" control" tabindex="0" style="display: none;"></td>
                                                    <td class=" dt-checkboxes-cell">
                                                        <div class="form-check"> 
                                                            <input class="form-check-input dt-checkboxes" type="checkbox" value=""
                                                                   id="checkbox${staffList.getAccountID()}"><label class="form-check-label" for="checkbox${staffList.getAccountID()}"></label></div>
                                                    </td>
                                                    <td class="">
                                                        <div class="d-flex justify-content-left align-items-center">
                                                            <c:choose>
                                                                <c:when test="${staffList.getAvatar() == null}">
                                                                    <c:set var="firstName" value="${staffList.getFirstName()}"/>
                                                                    <c:set var="lastName" value="${staffList.getLastName()}"/>
                                                                    <div class="avatar-wrapper">
                                                                        <div class="avatar bg-light-warning me-1">
                                                                            <span class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</span>
                                                                        </div>
                                                                    </div>
                                                                </c:when>
                                                                <c:when test="${staffList.getAvatar() != null}">
                                                                    <div class="avatar-wrapper">
                                                                        <div class="avatar  me-1">
                                                                            <img src="${staffList.getAvatar()}" alt="Avatar"
                                                                                 height="32" width="32">
                                                                        </div>
                                                                    </div>
                                                                </c:when>
                                                            </c:choose> 
                                                            <div class="d-flex flex-column">
                                                                <a href="#/" index="${staffList.getAccountID()}" onclick="sendParamter(this)"  data-bs-toggle="modal" data-bs-target="#defaultSize"
                                                                   class="user_name text-body text-truncate">
                                                                    <span class="fw-bolder">${staffList.getFirstName()} ${staffList.getLastName()}</span>
                                                                </a>
                                                                <small class="emp_post text-muted">${staffList.getEmail()}</small>
                                                            </div>
                                                        </div>
                                                    </td>

                                                    <td class="sorting_1">
                                                        <c:choose>
                                                            <c:when test="${staffList.getRole() == 'ADMIN'}">
                                                                <span class="text-truncate align-middle">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                         class="feather feather-slack font-medium-3 text-danger me-50">
                                                                    <path
                                                                        d="M14.5 10c-.83 0-1.5-.67-1.5-1.5v-5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5z">
                                                                    </path>
                                                                    <path d="M20.5 10H19V8.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z"></path>
                                                                    <path
                                                                        d="M9.5 14c.83 0 1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5S8 21.33 8 20.5v-5c0-.83.67-1.5 1.5-1.5z">
                                                                    </path>
                                                                    <path d="M3.5 14H5v1.5c0 .83-.67 1.5-1.5 1.5S2 16.33 2 15.5 2.67 14 3.5 14z"></path>
                                                                    <path
                                                                        d="M14 14.5c0-.83.67-1.5 1.5-1.5h5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-5c-.83 0-1.5-.67-1.5-1.5z">
                                                                    </path>
                                                                    <path d="M15.5 19H14v1.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5z"></path>
                                                                    <path
                                                                        d="M10 9.5C10 8.67 9.33 8 8.5 8h-5C2.67 8 2 8.67 2 9.5S2.67 11 3.5 11h5c.83 0 1.5-.67 1.5-1.5z">
                                                                    </path>
                                                                    <path d="M8.5 5H10V3.5C10 2.67 9.33 2 8.5 2S7 2.67 7 3.5 7.67 5 8.5 5z"></path>
                                                                    </svg>                                                                    
                                                                    <span class="roleSort">Admin</span>
                                                                </span>
                                                            </c:when>
                                                            <c:when test="${staffList.getRole() == 'STAFF'}">
                                                                <span class="text-truncate align-middle">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                         class="feather feather-settings font-medium-3 text-warning me-50">
                                                                    <circle cx="12" cy="12" r="3"></circle>
                                                                    <path
                                                                        d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                                                    </path>
                                                                    </svg>
                                                                    <span class="roleSort">Staff</span>
                                                                </span>
                                                            </c:when>
                                                        </c:choose>
                                                    </td>
                                                    <td>${staffList.getPhoneNumber()}</td>
                                                    <td><span class="text-nowrap">${staffList.getState()}</span></td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${staffList.getActive() == 0}">
                                                                <span class="badge rounded-pill badge-light-primary" text-capitalized="">Inactive</span>
                                                            </c:when>
                                                            <c:when test="${staffList.getActive() == 1}">
                                                                <span class="badge rounded-pill badge-light-success" text-capitalized="">Active</span>
                                                            </c:when>
                                                            <c:when test="${staffList.getActive() == 2}">
                                                                <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Banned</span>
                                                            </c:when>
                                                        </c:choose>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-sm btn-icon" data-bs-toggle="modal" data-bs-target="#defaultSize" index="${staffList.getAccountID()}" onclick="sendParamter(this)">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                 class="feather feather-eye font-medium-3 text-body">
                                                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                            <circle cx="12" cy="12" r="3"></circle>
                                                            </svg>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            <c:if test="${endUser >= 0}">
                                                <c:forEach items="${sessionScope.userAccountList}" begin="${beginUser}" end="${endUser}" var="userList">
                                                    <tr>
                                                        <td class=" control" tabindex="0" style="display: none;"></td>
                                                        <td class=" dt-checkboxes-cell">
                                                            <div class="form-check"> 
                                                                <input class="form-check-input dt-checkboxes" type="checkbox" value=""
                                                                       id="checkbox${userList.getAccountID()}"><label class="form-check-label" for="checkbox${userList.getAccountID()}"></label></div>
                                                        </td>
                                                        <td class="">
                                                            <div class="d-flex justify-content-left align-items-center">
                                                                <c:choose>
                                                                    <c:when test="${userList.getAvatar() == null}">
                                                                        <c:set var="firstName" value="${userList.getFirstName()}"/>
                                                                        <c:set var="lastName" value="${userList.getLastName()}"/>
                                                                        <div class="avatar-wrapper">
                                                                            <div class="avatar bg-light-warning me-1">
                                                                                <span class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</span>
                                                                            </div>
                                                                        </div>
                                                                    </c:when>
                                                                    <c:when test="${userList.getAvatar() != null}">
                                                                        <div class="avatar-wrapper">
                                                                            <div class="avatar me-1">
                                                                                <img src="${userList.getAvatar()}" alt="Avatar"
                                                                                     height="32" width="32">
                                                                            </div>
                                                                        </div>
                                                                    </c:when>
                                                                </c:choose> 
                                                                <div class="d-flex flex-column">
                                                                    <a href="#/" data-bs-toggle="modal" data-bs-target="#defaultSize" index="${userList.getAccountID()}" onclick="sendParamter(this)" 
                                                                       class="user_name text-body text-truncate">
                                                                        <span class="fw-bolder">${userList.getFirstName()} ${userList.getLastName()}</span>
                                                                    </a>
                                                                    <small class="emp_post text-muted">${userList.getEmail()}</small>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td class="sorting_1">
                                                            <c:choose>
                                                                <c:when test="${userList.getRole() == 'CUSTOMER'}">
                                                                    <span class="text-truncate align-middle">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                             class="feather feather-user font-medium-3 text-primary me-50">
                                                                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                                                        <circle cx="12" cy="7" r="4"></circle>
                                                                        </svg>         
                                                                        <span class="roleSort">Customer</span>
                                                                    </span>
                                                                </c:when>
                                                                <c:when test="${userList.getRole() == 'SALEPERSON'}">
                                                                    <i data-feather='truck' style="
                                                                       width: 16.8px;
                                                                       height: 16.8px;
                                                                       margin-right: 7px;
                                                                       ">
                                                                    </i>
                                                                    <span class="roleSort">Saleperson</span>
                                                                </td>
                                                            </c:when>
                                                        </c:choose>
                                                        </td>
                                                        <td>${userList.getPhoneNumber()}</td>
                                                        <td><span class="text-nowrap">${userList.getState()}</span></td>
                                                        <td>
                                                            <c:choose>
                                                                <c:when test="${userList.getStatus() == 2}">
                                                                    <span class="badge rounded-pill badge-light-primary" text-capitalized="">
                                                                        Inactive
                                                                    </span>
                                                                </c:when>
                                                                <c:when test="${userList.getStatus() == 1}">
                                                                    <span class="badge rounded-pill badge-light-success" text-capitalized="">Active</span>
                                                                </c:when>
                                                                <c:when test="${userList.getStatus() == 0}">
                                                                    <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Banned</span>
                                                                </c:when>
                                                            </c:choose>
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-sm btn-icon" data-bs-toggle="modal" data-bs-target="#defaultSize" index="${userList.getAccountID()}" onclick="sendParamter(this)" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                                     class="feather feather-eye font-medium-3 text-body">
                                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                <circle cx="12" cy="12" r="3"></circle>
                                                                </svg>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </c:forEach>                                                
                                            </c:if>
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-between mx-2 row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_info totalResult" id="totalInfo" style="transition: 0.6s" role="status" aria-live="polite">Showing ${limitStart}
                                                to ${limitEnd} of ${totalAccount} entries</div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                                <ul class="pagination" id="paginationNumber" style="transition: 0.6s;">
                                                    <c:choose>
                                                        <c:when test="${currentPage == 1}">
                                                            <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous">
                                                                <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
                                                                   class="page-link">&nbsp;
                                                                </a>
                                                            </li>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <li class="paginate_button page-item previous" id="DataTables_Table_0_previous">
                                                                <a href="userListController?page=${currentPage - 1}" data-dt-idx="0" tabindex="0"
                                                                   class="page-link">&nbsp;
                                                                </a>
                                                            </li>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <c:forEach items="${sessionScope.numberPaginationList}" var="numberPage">
                                                        <c:set var="index" value="${numberPage}"/>
                                                        <c:choose>
                                                            <c:when test="${currentPage == index}">
                                                                <li class="paginate_button page-item active">
                                                                    <a href="#/" aria-controls="DataTables_Table_0" data-dt-idx="${numberPage}" tabindex="0" class="page-link">${numberPage}</a>
                                                                </li>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <li class="paginate_button page-item">
                                                                    <a href="userListController?page=${numberPage}" aria-controls="DataTables_Table_0" data-dt-idx="${numberPage}" tabindex="0" class="page-link">${numberPage}</a>
                                                                </li>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                    <c:set var="totalPage" value="${fn:length(numberPaginationList)}"/>
                                                    <c:choose>
                                                        <c:when test="${currentPage + 1 > totalPage}">
                                                            <li class="paginate_button page-item next disabled" id="DataTables_Table_0_next">
                                                                <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="6" 
                                                                   tabindex="0" class="page-link">&nbsp;
                                                                </a>
                                                            </li>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <li class="paginate_button page-item next" id="DataTables_Table_0_next">
                                                                <a href="userListController?page=${currentPage + 1}" aria-controls="DataTables_Table_0" data-dt-idx="6" 
                                                                   tabindex="0" class="page-link">&nbsp;
                                                                </a>
                                                            </li>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- list and filter end -->
                    </section>
                    <!-- users list ends -->

                </div>
            </div>
        </div>
        <!-- END: Content-->


        <!-- BEGIN: Customizer-->
        <div class="customizer d-none d-md-block"><a
                class="customizer-toggle d-flex align-items-center justify-content-center" href="#"
                ><i class="spinner"
                data-feather="settings"></i></a>
            <div class="customizer-content">
                <!-- Customizer header -->
                <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                    <h4 class="mb-0">Theme Customizer</h4>
                    <p class="m-0">Customize & Preview in Real Time</p>

                    <a class="customizer-close" href="#"><i data-feather="x"></i></a>
                </div>

                <hr />

                <!-- Styling & Text Direction -->
                <div class="customizer-styling-direction px-2">
                    <p class="fw-bold">Skin</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="skinlight" name="skinradio" class="form-check-input layout-name" checked
                                   data-layout="" />
                            <label class="form-check-label" for="skinlight">Light</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skinbordered" name="skinradio" class="form-check-input layout-name"
                                   data-layout="bordered-layout" />
                            <label class="form-check-label" for="skinbordered">Bordered</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skindark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="dark-layout" />
                            <label class="form-check-label" for="skindark">Dark</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="skinsemidark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="semi-dark-layout" />
                            <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                        </div>
                    </div>
                </div>

                <hr />

                <!-- Menu -->
                <div class="customizer-menu px-2">
                    <div id="customizer-menu-collapsible" class="d-flex">
                        <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                        <div class="form-check form-check-primary form-switch">
                            <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                            <label class="form-check-label" for="collapse-sidebar-switch"></label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Layout Width -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Layout Width</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                            <label class="form-check-label" for="layout-width-full">Full Width</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Navbar -->
                <div class="customizer-navbar px-2">
                    <div id="customizer-navbar-colors">
                        <p class="fw-bold">Navbar Color</p>
                        <ul class="list-inline unstyled-list">
                            <li class="color-box bg-white border selected" data-navbar-default=""></li>
                            <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                            <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                            <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                            <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                            <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                            <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                            <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                        </ul>
                    </div>

                    <p class="navbar-type-text fw-bold">Navbar Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                            <label class="form-check-label" for="nav-type-floating">Floating</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-static">Static</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Footer -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Footer Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                            <label class="form-check-label" for="footer-type-static">Static</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End: Customizer-->
        <!-- Modal to add new user starts-->
        <div class="modal modal-slide-in new-user-modal fade" id="modals-slide-in" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
                <form class="add-new-user modal-content pt-0" novalidate="novalidate" onsubmit="return false">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                    <div class="modal-header mb-1">
                        <h5 class="modal-title" id="exampleModalLabel">Add Staff User</h5>
                    </div>
                    <div class="modal-body flex-grow-1">
                        <div class="mb-1">
                            <label class="form-label" for="userFullname">Username</label>
                            <input type="text" class="form-control form-control-staff dt-full-name" id="userFullname" placeholder="John Doe" name="userFullname">
                        </div>
                        <div class="row">
                            <div class="mb-1 col-6 col-sm-6">
                                <label class="form-label" for="userFirstName">First Name</label>
                                <input type="text" id="userFirstName" class="form-control form-control-staff dt-uname-first" placeholder="Nicolas" name="userFirstName">
                            </div>
                            <div class="mb-1 col-6 col-sm-6">
                                <label class="form-label" for="userLastName">Last Name</label>
                                <input type="text" id="userLastName" class="form-control form-control-staff dt-uname-last" placeholder="Jensen" name="userLastName">
                            </div>
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="userEmail">Email</label>
                            <input type="text" id="userEmail" class="form-control form-control-staff dt-email" placeholder="nicolasJensen@gmail.com" name="userEmail">
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="userContact">Contact</label>
                            <input type="number" id="userContact" class="form-control form-control-staff dt-contact" placeholder="+1 (609) 933-44-22" name="userContact">
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="userCity">City</label>
                            <input type="text" id="userCity" class="form-control form-control-staff dt-city" placeholder="New York" name="userCity">
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="userStreet">Street</label>
                            <input type="text" id="userStreet" class="form-control form-control-staff dt-street" placeholder="143 Sanfoudry" name="userStreet">
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="country">State</label>
                            <div class="position-relative">
                                <select id="country" class="modal-select-hidden select2 form-select select2-hidden-accessible" 
                                        data-select2-id="country" tabindex="-1" aria-hidden="true">
                                    <option value="United State"data-select2-id="2">United State</option>
                                    <option value="Alaska" class="text-capitalize">Alaska</option>
                                    <option value="Hawaii" class="text-capitalize">Hawaii</option>
                                    <option value="California" class="text-capitalize">California</option>
                                    <option value="Nevada" class="text-capitalize">Nevada</option>
                                    <option value="Oregon" class="text-capitalize">Oregon</option>
                                    <option value="Washington" class="text-capitalize">Washington</option>
                                    <option value="Arizona" class="text-capitalize">Arizona</option>
                                    <option value="Colorado"class="text-capitalize">Colorado</option>
                                    <option value="Idaho" class="text-capitalize">Idaho</option>
                                    <option value="Montana"class="text-capitalize">Montana</option>
                                    <option value="Nebraska" class="text-capitalize">Nebraska</option>
                                    <option value="New Mexico" class="text-capitalize">New Mexico</option>
                                    <option value="North Dakota" class="text-capitalize">North Dakota</option>
                                    <option value="Utah" class="text-capitalize">Utah</option>
                                    <option value="Wyoming" class="text-capitalize">Wyoming</option>
                                    <option value="Alabama" class="text-capitalize">Alabama</option>
                                    <option value="Arkansas" class="text-capitalize">Arkansas</option>
                                    <option value="Illinois" class="text-capitalize">Illinois</option>
                                    <option value="Iowa" class="text-capitalize">Iowa</option>
                                    <option value="Kansas" class="text-capitalize">Kansas</option>
                                    <option value="Kentucky" class="text-capitalize">Kentucky</option>
                                    <option value="Louisiana" class="text-capitalize">Louisiana</option>
                                    <option value="Minnesota" class="text-capitalize">Minnesota</option>
                                    <option value="Mississippi" class="text-capitalize">Mississippi</option>
                                    <option value="Missouri" class="text-capitalize">Missouri</option>
                                    <option value="Oklahoma"class="text-capitalize">Oklahoma</option>
                                    <option value="South Dakota" class="text-capitalize">South Dakota</option>
                                    <option value="Texas" class="text-capitalize">Texas</option>
                                    <option value="Tennessee" class="text-capitalize">Tennessee</option>
                                    <option value="Wisconsin" class="text-capitalize">Wisconsin</option>
                                    <option value="Connecticut" class="text-capitalize">Connecticut</option>
                                    <option value="Delaware" class="text-capitalize">Delaware</option>
                                    <option value="Florida" class="text-capitalize">Florida</option>
                                    <option value="Georgia" class="text-capitalize">Georgia</option>
                                    <option value="Indiana" class="text-capitalize">Indiana</option>
                                    <option value="Maine" class="text-capitalize">Maine</option>
                                    <option value="Maryland" class="text-capitalize">Maryland</option>
                                    <option value="Massachusetts" class="text-capitalize">Massachusetts</option>
                                    <option value="Michigan" class="text-capitalize">Michigan</option>
                                    <option value="New Hampshire" class="text-capitalize">New Hampshire</option>
                                    <option value="New Jersey" class="text-capitalize">New Jersey</option>
                                    <option value="New York" class="text-capitalize">New York</option>
                                    <option value="North Carolina" class="text-capitalize">North Carolina</option>
                                    <option value="Ohio" class="text-capitalize">Ohio</option>
                                    <option value="Pennsylvania" class="text-capitalize">Pennsylvania</option>
                                    <option value="Rhode Island" class="text-capitalize">Rhode Island</option>
                                    <option value="South Carolina" class="text-capitalize">South Carolina</option>
                                    <option value="Vermont" class="text-capitalize">Vermont</option>
                                    <option value="Virginia" class="text-capitalize">Virginia</option>
                                    <option value="West Virginia" class="text-capitalize">West Virginia</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="user-role">User Role</label>
                            <div class="position-relative"><select id="user-role" class="select2 form-select select2-hidden-accessible" data-select2-id="user-role" tabindex="-1" aria-hidden="true">
                                    <option value="STAFF" data-select2-id="4">Staff</option>
                                    <option value="ADMIN" disabled>Admin</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="userPassword">Password</label>
                            <input type="password" id="userPassword" class="form-control form-control-staff dt-password" placeholder="**************" name="userPassword">
                        </div>
                        <div class="row">
                            <div class="mb-1 mt-1 col-6 col-sm-6">
                                <button type="submit"
                                        class="full-width btn btn-primary me-1 data-submit waves-effect waves-float waves-light">Submit</button>
                            </div>
                            <div class="mb-1 mt-1 col-6 col-sm-6">
                                <button type="reset" class="full-width btn btn-outline-secondary waves-effect"
                                        data-bs-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Modal to add new user Ends-->
        <!-- Modal to show profile Begin-->
        <div class="modal fade text-start" id="defaultSize" tabindex="-1" aria-labelledby="myModalLabel18" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="titleDetails"></h4>
                        <button type="button" class="btn-close"
                                data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <tbody>
                                <tr data-dt-row="undefined" data-dt-column="1">
                                    <td>#:</td>
                                    <td><a class="fw-bold" id="userModalID" href="#/"> </a></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="3">
                                    <td>Info:</td>
                                    <td>
                                        <div class="d-flex justify-content-left align-items-center">
                                            <div class="avatar-wrapper">
                                                <div class="avatar me-50">
                                                    <img id="accoutAvatar" style="display: none;" src="app-assets/images/avatars/1-small.png" alt="Avatar"
                                                         width="32" height="32"></div>
                                            </div>
                                            <div class="avatar-wrapper">
                                                <div class="avatar bg-light-warning me-1">
                                                    <span class="avatar-content" id="accountAvatarAlt"></span>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column">
                                                <h6 class="user-name text-truncate mb-0" id="userModalFullName"></h6><small
                                                    class="text-truncate text-muted" id="userModalEmail"></small>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="4">
                                    <td>Status</td>
                                    <td id="userModalStatus">
                                    </td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="5">
                                    <td>Date of Join:</td>
                                    <td id="userModalJoin"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="6">
                                    <td>Contact:</td>
                                    <td id="userModalPhone"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="7">
                                    <td>City:</td>
                                    <td id="userModalCity"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="8">
                                    <td>Street:</td>
                                    <td id="userModalStreet"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="9">
                                    <td>State:</td>
                                    <td id="userModalState"></td>
                                </tr>
                                <tr data-dt-row="undefined" data-dt-column="10">
                                    <td>Role:</td>
                                    <td id="userModalRole">
                                        <span class="text-truncate align-middle">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-slack font-medium-3 text-danger me-50">
                                            <path
                                                d="M14.5 10c-.83 0-1.5-.67-1.5-1.5v-5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5z">
                                            </path>
                                            <path d="M20.5 10H19V8.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z"></path>
                                            <path
                                                d="M9.5 14c.83 0 1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5S8 21.33 8 20.5v-5c0-.83.67-1.5 1.5-1.5z">
                                            </path>
                                            <path d="M3.5 14H5v1.5c0 .83-.67 1.5-1.5 1.5S2 16.33 2 15.5 2.67 14 3.5 14z"></path>
                                            <path
                                                d="M14 14.5c0-.83.67-1.5 1.5-1.5h5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-5c-.83 0-1.5-.67-1.5-1.5z">
                                            </path>
                                            <path d="M15.5 19H14v1.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5z"></path>
                                            <path
                                                d="M10 9.5C10 8.67 9.33 8 8.5 8h-5C2.67 8 2 8.67 2 9.5S2.67 11 3.5 11h5c.83 0 1.5-.67 1.5-1.5z">
                                            </path>
                                            <path d="M8.5 5H10V3.5C10 2.67 9.33 2 8.5 2S7 2.67 7 3.5 7.67 5 8.5 5z"></path>
                                            </svg>Admin
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade modal-danger text-start" id="danger_modal" tabindex="-1" aria-labelledby="myModalLabel120" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel120">Permission Limited</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Sorry, but only people with the admin role and staff with user management permission can do this. Try contacting the admins if you want to do this.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect waves-float waves-light" data-bs-dismiss="modal">Accept</button>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal to show profile Begin-->
        <div class="modal-size-default d-inline-block">
            <button type="button" data-bs-toggle="modal" data-bs-target="#defaultSize">
            </button>
        </div>
        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Vendor JS-->
        <script src="app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>

        <script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
        <script src="app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
        <script src="app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
        <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="app-assets/js/core/app-menu.min.js"></script>
        <script src="app-assets/js/core/app.min.js"></script>
        <script src="app-assets/js/scripts/customizer.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: My JS-->
        <script src="assets/js/user_staff_validate.js"></script>
        <script src="assets/js/admin_user_list.js"></script>
        <!-- END: My JS-->
        <script>
                    $(window).on('load', function () {
                        if (feather) {
                            feather.replace({width: 14, height: 14});
                        }
                        $('.modal-select-hidden').select2({
                            dropdownParent: $('#modals-slide-in')
                        });
                    });
        </script>
    </body>
    <!-- END: Body-->

</html>