$(function () {
    ('use strict');
    var bannedPermissionForm = $('#bannedUpdatePermission');
  
    // jQuery Validation
    // --------------------------------------------------------------------
    if (bannedPermissionForm.length) {
        bannedPermissionForm.validate({
        rules: {
            bannedPermissionForm: {
            required: true
          }
        }
      });
    }
  });
  