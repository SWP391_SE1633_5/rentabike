<%-- 
    Document   : login
    Created on : Sep 11, 2022, 9:10:49 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="google-signin-scope" content="profile email">
        <meta name="google-signin-client_id" content="737845884816-h4dhj7gn4hgpbjgtrmucktql5rm8tvg4.apps.googleusercontent.com">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <title>Hilfsmotor</title>
        <script src="https://accounts.google.com/gsi/client" async defer></script>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/wizard/bs-stepper.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <!-- END: Vendor CSS-->

        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-wizard.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-validation.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/authentication.css">
        <!-- END: Page CSS-->

        <link rel="stylesheet" href="assets/css/style.css">
    </head>

    <body>
        <div class="main">
            <section class="widget-login overlay-fixed-settings">
                <header class="widget-login__header flex-default position-relative">
                    <div class="widget-login__content-left">
                        <div class="widget-login__content-container flex-center flex-default flex-direction-column height-full">
                            <div class="widget-login__content-header flex-default flex-direction-column">
                                <div class="widget-login__left-title">Welcome to Hilfsmotor! 👋</div>
                                <div class="widget-login__left-sub-title">Ready to join your adventure?</div>
                            </div>
                            <div class="widget-login__message-notify">
                                <div class="widget-login__message-description">THE LOGIN DETAILS YOU ENTERED ARE INCORRECT.<br> TRY AGAIN...</div>
                            </div>
                            <div class="widget-login__form width-full">
                                <form action="loginController" method="post" id="loginController">
                                    <div class="widget-login__input-field" style="margin-top:2rem;">
                                        <label for="widget-login__input-email">
                                            <div class="widget-login__title-input cursor-select-pointer">Email</div>
                                        </label>                                    
                                        <input type="text" name="useremail" role="email" autocomplete="on" id="widget-login__input-email" class="widget-login__input"
                                               placeholder="E-mail Address">
                                    </div>
                                    <div class="widget-login__input-field">
                                        <label for="widget-login__input-password">
                                            <div class="widget-login__title-input cursor-select-pointer">Password</div>
                                        </label>
                                        <div class="widget-login__input-field-box position-relative">
                                            <input type="password" name="password" role="password" autocomplete="on" id="widget-login__input-password" class="widget-login__input"
                                                   placeholder="Password">
                                            <button type="button" class="widget-login__input-show">
                                                <svg fill="none" viewBox="0 0 20 10" class="_340FWs" style="width:20px;height:20px">
                                                <path stroke="none" fill="#000" fill-opacity=".54"
                                                      d="M19.834 1.15a.768.768 0 00-.142-1c-.322-.25-.75-.178-1 .143-.035.036-3.997 4.712-8.709 4.712-4.569 0-8.71-4.712-8.745-4.748a.724.724 0 00-1-.071.724.724 0 00-.07 1c.07.106.927 1.07 2.283 2.141L.631 5.219a.69.69 0 00.036 1c.071.142.25.213.428.213a.705.705 0 00.5-.214l1.963-2.034A13.91 13.91 0 006.806 5.86l-.75 2.535a.714.714 0 00.5.892h.214a.688.688 0 00.679-.535l.75-2.535a9.758 9.758 0 001.784.179c.607 0 1.213-.072 1.785-.179l.75 2.499c.07.321.392.535.677.535.072 0 .143 0 .179-.035a.714.714 0 00.5-.893l-.75-2.498a13.914 13.914 0 003.248-1.678L18.3 6.147a.705.705 0 00.5.214.705.705 0 00.499-.214.723.723 0 00.036-1l-1.82-1.891c1.463-1.071 2.32-2.106 2.32-2.106z">
                                                </path>
                                                </svg>                                            
                                            </button>                                             
                                        </div>                                       
                                    </div>
                                    <div class="widget-login__options flex-default">
                                        <div class="widget-login__option form-check">
                                            <input type="checkbox" name="re" id="remember-me" class="form-check-input widget-login__checkbox cursor-select-pointer">
                                            <label for="remember-me" class="widget-login__option-text">Remember me</label>
                                        </div>
                                        <div class="widget-login__option position-relative" data="Forgot Password">
                                            <a href="#" class="widget-login__option-text">Forgot password?</a>
                                            <div class="widget-login__separate"></div>
                                        </div>
                                    </div>
                                    <div class="widget-login__input-field position-relative">
                                        <button type="submit" role="LoginSubmit" class="widget-login__input-submit width-full cursor-select-not-allowed" disabled>Sign in</button>
                                        <div class="widget-login__process-loading">
                                            <div class="loading-screen__icon">
                                                <div class="snippet" data-title=".dot-windmill">
                                                    <div class="stage">
                                                        <div class="dot-windmill"></div>
                                                    </div>
                                                </div>                                    
                                            </div>
                                        </div>                                
                                    </div>       
                                    <div class="widget-login__with-seperate">OR</div>
                                    <div class="widget-login__with-pattern flex-default flex-center">
                                        <div id="g_id_onload" data-client_id="737845884816-h4dhj7gn4hgpbjgtrmucktql5rm8tvg4.apps.googleusercontent.com"
                                             data-login_uri="http://localhost:8088" data-auto_prompt="false" data-callback="handleCredentialResponse">
                                        </div>
                                        <div class="g_id_signin" data-type="standard" data-size="large" data-theme="outline"
                                             data-text="sign_in_with" data-shape="rectangular" data-width="400" data-logo_alignment="left">
                                        </div>                                     
                                    </div>                                                                 
                                    <div class="widget-login__switch flex-default flex-center">
                                        <div class="widget-login__switch-text cursor-select-none">Don't have an account?</div>
                                        <div class="widget-login__switch-box position-relative">
                                            <a class="widget-login__switch-text">Sign up for free
                                            </a>
                                            <div class="widget-login__separate"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="widget-signin__content-right position-relative flex-center flex-default">
                        <div class="widget-signin__content-container flex-center flex-default flex-direction-column width-full height-full">
                            <div class="bs-stepper register-multi-steps-wizard shadow-none">
                                <div class="bs-stepper-header flex-center px-0 flex-center" role="tablist" style="justify-content: center;">
                                    <div class="step" data-target="#account-details" role="tab" id="account-details-trigger">
                                        <button type="button" class="step-trigger">
                                            <span class="bs-stepper-box active">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home font-medium-3"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                            </span>
                                            <span class="bs-stepper-label" index="1">
                                                <span class="bs-stepper-title">Account</span>
                                                <span class="bs-stepper-subtitle">Enter username</span>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="line">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right font-medium-2"><polyline points="9 18 15 12 9 6"></polyline></svg>                                    </div>
                                    <div class="step" data-target="#personal-info" role="tab" id="personal-info-trigger">
                                        <button type="button" class="step-trigger">
                                            <span class="bs-stepper-box" index="2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user font-medium-3"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                            </span>
                                            <span class="bs-stepper-label">
                                                <span class="bs-stepper-title">Personal</span>
                                                <span class="bs-stepper-subtitle">Enter Information</span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-signin__content-header flex-default flex-direction-column">
                                <div class="widget-signin__right-title">This is your first time?</div>
                                <div class="widget-signin__right-sub-title">Become one of us in Hilfsmotor! 👋</div>
                            </div>
                            <div class="widget-signin__form width-full">
                                <form action="registerController" method="post" id="registerController">
                                    <div class="widget-signin__first-form" style="display:block;"> 
                                        <div class="widget-sign__box-input-content grid-default grid-two-column-center column-gap-default">
                                            <div class="widget-signin__input-field">
                                                <label for="widget-signin__input-fname">
                                                    <div class="widget-signin__title-input cursor-select-pointer" title="firstName">First Name</div>
                                                </label>                                    
                                                <input type="text" name="firstName" role="firstName" id="widget-signin__input-fname" class="widget-signin__input"
                                                       placeholder="Nicolas">
                                            </div>
                                            <div class="widget-signin__input-field">
                                                <label for="widget-signin__input-lname">
                                                    <div class="widget-signin__title-input cursor-select-pointer" title="lastName">Last Name</div>
                                                </label>
                                                <input type="text" name="lastName" role="lastName" id="widget-signin__input-lname" class="widget-signin__input"
                                                       placeholder="Jensen">
                                            </div>
                                        </div>
                                        <div class="widget-sign__box-input-content grid-default grid-two-column-center column-gap-default">
                                            <div class="widget-signin__input-field">
                                                <label for="widget-signin__input-phoneNumber">
                                                    <div class="widget-signin__title-input cursor-select-pointer" title="phoneNumber">Phone Number</div>
                                                </label>                                    
                                                <input type="number" name="phoneNumber" role="phoneNumber" id="widget-signin__input-phoneNumber" class="widget-signin__input"
                                                       placeholder="0-357-4815" maxlength="11">
                                            </div>
                                            <div class="widget-signin__input-field">
                                                <label for="widget-signin__input-email">
                                                    <div class="widget-signin__title-input cursor-select-pointer" title="email">Email</div>
                                                </label>
                                                <input type="text" name="email" role="email" id="widget-signin__input-email" class="widget-signin__input"
                                                       placeholder="nicolasjensen@gmail.com">
                                            </div>
                                        </div>
                                        <div class="widget-sign__box-input-content grid-default grid-two-column-center column-gap-default">
                                            <div class="widget-signin__input-field">
                                                <label for="widget-signin__input-password">
                                                    <div class="widget-signin__title-input cursor-select-pointer">Password</div>
                                                </label>
                                                <div class="widget-signin__input-box">
                                                    <input type="password" name="password" role="password" id="widget-signin__input-password" class="widget-signin__input"
                                                           placeholder="**********" minlength="8" maxlength="20">
                                                </div>
                                                <div class="widget-signin__title-input" style="margin-top:0.25rem" title="password"></div>
                                            </div>
                                            <div class="widget-signin__input-field">
                                                <label for="widget-signin__input-password">
                                                    <div class="widget-signin__title-input cursor-select-pointer">Confirm Password</div>
                                                </label>
                                                <div class="widget-signin__input-box">
                                                    <input type="password" role="confirm-password" id="widget-signin__input-confirm-password" class="widget-signin__input"
                                                           placeholder="**********">
                                                </div>
                                                <div class="widget-signin__title-input" style="margin-top:0.25rem" title="confirm-password"></div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <a>
                                                <button type="button" class="btn btn-outline-secondary btn-prev waves-effect widget-login__switch-box">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left align-middle me-sm-25 me-0"><polyline points="15 18 9 12 15 6"></polyline></svg>
                                                    <span class="align-middle d-sm-inline-block d-none widget-login__switch-text">Log in</span>
                                                </button>
                                            </a>
                                            <button type="button" disabled class="widget-get-sign-in-next btn btn-primary btn-next waves-effect waves-float waves-light">
                                                <span class="align-middle d-sm-inline-block d-none">Next</span>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right align-middle ms-sm-25 ms-0"><polyline points="9 18 15 12 9 6"></polyline></svg>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="widget-signin__second-form" style="display:none;"> 
                                        <div class="row">
                                            <div class="col-md-6 mb-1" style="text-align:left;">
                                                <label class="form-label form-control-title-city" for="city">City</label>
                                                <input type="text" name="city" id="city" role="city" class="form-control" placeholder="City">
                                            </div>
                                            <div class="mb-1 col-md-6" style="text-align:left;">
                                                <label class="form-label form-control-title-street" for="street">Street</label>
                                                <input type="text" name="street" id="street" role="street" class="form-control" placeholder="Street">
                                            </div>
                                            
                                            <div class="mb-1 col-md-12" style="text-align:left;">
                                                <label class="form-label" for="state">State</label>
                                                <select id="FilterTransaction" name="state" class="form-select form-select-option text-capitalize mb-md-0 mb-2xx">
                                                    <option value="United State" class="text-capitalize">United State</option>
                                                    <option value="Alaska" class="text-capitalize">Alaska</option>
                                                    <option value="Hawaii" class="text-capitalize">Hawaii</option>
                                                    <option value="California" class="text-capitalize">California</option>
                                                    <option value="Nevada" class="text-capitalize">Nevada</option>
                                                    <option value="Oregon" class="text-capitalize">Oregon</option>
                                                    <option value="Washington" class="text-capitalize">Washington</option>
                                                    <option value="Arizona" class="text-capitalize">Arizona</option>
                                                    <option value="Colorado"class="text-capitalize">Colorado</option>
                                                    <option value="Idaho" class="text-capitalize">Idaho</option>
                                                    <option value="Montana"class="text-capitalize">Montana</option>
                                                    <option value="Nebraska" class="text-capitalize">Nebraska</option>
                                                    <option value="New Mexico" class="text-capitalize">New Mexico</option>
                                                    <option value="North Dakota" class="text-capitalize">North Dakota</option>
                                                    <option value="Utah" class="text-capitalize">Utah</option>
                                                    <option value="Wyoming" class="text-capitalize">Wyoming</option>
                                                    <option value="Alabama" class="text-capitalize">Alabama</option>
                                                    <option value="Arkansas" class="text-capitalize">Arkansas</option>
                                                    <option value="Illinois" class="text-capitalize">Illinois</option>
                                                    <option value="Iowa" class="text-capitalize">Iowa</option>
                                                    <option value="Kansas" class="text-capitalize">Kansas</option>
                                                    <option value="Kentucky" class="text-capitalize">Kentucky</option>
                                                    <option value="Louisiana" class="text-capitalize">Louisiana</option>
                                                    <option value="Minnesota" class="text-capitalize">Minnesota</option>
                                                    <option value="Mississippi" class="text-capitalize">Mississippi</option>
                                                    <option value="Missouri" class="text-capitalize">Missouri</option>
                                                    <option value="Oklahoma"class="text-capitalize">Oklahoma</option>
                                                    <option value="South Dakota" class="text-capitalize">South Dakota</option>
                                                    <option value="Texas" class="text-capitalize">Texas</option>
                                                    <option value="Tennessee" class="text-capitalize">Tennessee</option>
                                                    <option value="Wisconsin" class="text-capitalize">Wisconsin</option>
                                                    <option value="Connecticut" class="text-capitalize">Connecticut</option>
                                                    <option value="Delaware" class="text-capitalize">Delaware</option>
                                                    <option value="Florida" class="text-capitalize">Florida</option>
                                                    <option value="Georgia" class="text-capitalize">Georgia</option>
                                                    <option value="Indiana" class="text-capitalize">Indiana</option>
                                                    <option value="Maine" class="text-capitalize">Maine</option>
                                                    <option value="Maryland" class="text-capitalize">Maryland</option>
                                                    <option value="Massachusetts" class="text-capitalize">Massachusetts</option>
                                                    <option value="Michigan" class="text-capitalize">Michigan</option>
                                                    <option value="New Hampshire" class="text-capitalize">New Hampshire</option>
                                                    <option value="New Jersey" class="text-capitalize">New Jersey</option>
                                                    <option value="New York" class="text-capitalize">New York</option>
                                                    <option value="North Carolina" class="text-capitalize">North Carolina</option>
                                                    <option value="Ohio" class="text-capitalize">Ohio</option>
                                                    <option value="Pennsylvania" class="text-capitalize">Pennsylvania</option>
                                                    <option value="Rhode Island" class="text-capitalize">Rhode Island</option>
                                                    <option value="South Carolina" class="text-capitalize">South Carolina</option>
                                                    <option value="Vermont" class="text-capitalize">Vermont</option>
                                                    <option value="Virginia" class="text-capitalize">Virginia</option>
                                                    <option value="West Virginia" class="text-capitalize">West Virginia</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <a>
                                            <button type="button" class="widget-get-sign-in-previous btn btn-primary btn-next waves-effect waves-float waves-light">
                                                <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right align-middle ms-sm-25 ms-0"><polyline points="9 18 15 12 9 6"></polyline></svg>
                                            </button>
                                            </a>
                                            <button type="submit" class="btn btn-success btn-submit waves-effect waves-float waves-light widget-login__input-submit" disabled role="SignIn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check align-middle me-sm-25 me-0"><polyline points="20 6 9 17 4 12"></polyline></svg>
                                                <span class="align-middle d-sm-inline-block d-none">Submit</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="widget-signin__status">
                            <div class="widget-signin__status-message">
                                <div class="widget-signin__status-reg"></div>
                                <div class="widget-signin__status-description"></div>
                                <div class="widget-button__action home-page__introduce-option position-relative">
                                    <div class="widget-button__action-link home-page__introduce-link position-relative cursor-select-pointer" mode="default"></div>
                                    <div class="home-page__introduce-seperate"></div>
                                </div>
                            </div>
                            <div class="widget-signin__status-loading flex-default flex-center flex-direction-column row-gap-default">
                                <img src="assets/img/gif/Biker.gif" alt="" class="widget-signin__status-loading-image">
                                <div class="loading-screen position-relative">
                                    <div class="loading-screen__text">NOW LOADING</div>
                                    <div class="loading-screen__icon" type="register">
                                        <div class="snippet" data-title=".dot-windmill">
                                            <div class="stage">
                                                <div class="dot-windmill"></div>
                                            </div>
                                        </div>
                                    </div>                           
                                </div>
                                <div class="widget-button__action home-page__introduce-option position-relative" style="visibility: hidden;" type="verifySend">
                                    <div class="widget-button__action-link home-page__introduce-link position-relative cursor-select-pointer" mode="back">CANCEL</div>
                                    <div class="home-page__introduce-seperate"></div>
                                </div>     
                            </div>
                        </div>
                    </div>
                    <div class="widget-option__take width-full position-relative" style="display: none;">
                        <div class="widget-login__content-container flex-center flex-default flex-direction-column height-full">
                            <div class="widget-option__content-header flex-default flex-direction-column">
                                <div style="line-height: 2rem;">
                                    <img src="assets/img/RentabikeLogo.jpg" class="widget-login__logo-image">
                                    <h1 class="widget-login__title">Hilfsmotor</h1>
                                </div>
                                <div class="widget-login__left-title">Forgot password</div>
                                <div class="widget-login__left-sub-title">First, we need to have your email address to working!</div>
                            </div>
                            <div class="widget-option__reset-form width-full">
                                <form action="" method="post">
                                    <div class="widget-login__input-field">
                                        <label for="widget-option__input-email">
                                            <div class="widget-reset__title-input cursor-select-pointer">Email</div>
                                        </label>                                    
                                        <input type="text" name="useremail" role="email" id="widget-option__input-email" class="widget-option__reset-input"
                                               placeholder="E-mail Address">
                                    </div>
                                    <div class="widget-login__input-field">
                                        <button type="button" role="ResetPassword" class="widget-login__input-submit width-full" disabled>Reset password</button>
                                    </div>
                                    <div class="widget-login__switch flex-default flex-center">
                                        <div class="widget-login__switch-text cursor-select-none">Change your mind?</div>
                                        <div class="widget-login__switch-box position-relative widget-option__switch-box">
                                            <a class="widget-option__switch-back">Let's go back!
                                            </a>
                                            <div class="widget-login__separate"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="widget-option__message-notify" style="display:none;">
                                <div class="widget-option__reset-header flex-default flex-direction-column">
                                    <div style="line-height: 2rem;">
                                        <img src="assets/img/icon/mail__sent.gif" class="widget-login__logo-gif">
                                        <h1 class="widget-login__title">Hilfsmotor</h1>
                                    </div>
                                    <div class="widget-login__left-title">Process Successfully! Check your email</div>
                                    <div class="widget-login__left-title-description">We're sent an email to [Name]. Please check your spam folder if you don't see the email in your inbox.</div>
                                </div>
                                <div>
                                    <button type="button" class="widget-option__reset-switch">Back to log in</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-login__background-img flex-default">
                        <div class="widget-login__overlay overlay-fixed-settings"></div>
                        <div class="widget-login__box-title flex-center width-full flex-direction-column flex-default position-relative">
                            <div style="line-height: 2rem;">
                                <img src="assets/img/RentabikeLogo.jpg" class="widget-login__logo-image">
                                <h1 class="widget-login__title">Hilfsmotor</h1>
                            </div>
                            <div class="widget-login__paragraph">Take your bike and riding into the future</div>
                            <div class="widget-login__description">Explore and discover our best motorcycle, become a
                                sellermotor, connect with others over mutual hobbies, or buy and sell your motorcycle –
                                Everything you need is right here.</div>
                        </div>
                        <div class="widget-login__credit">
                            @ 2022 Hilfsmotor
                        </div>
                        <a href="homepageC.jsp">
                            <div class="widget-login__homepage">
                                <?xml version="1.0" ?>
                                <svg height="48" viewBox="0 0 48 48" width="48" xmlns="http://www.w3.org/2000/svg">
                                <path fill="white" d="M38 12.83l-2.83-2.83-11.17 11.17-11.17-11.17-2.83 2.83 11.17 11.17-11.17 11.17 2.83 2.83 11.17-11.17 11.17 11.17 2.83-2.83-11.17-11.17z"/>
                                <path fill="none" d="M0 0h48v48h-48z"/>
                                </svg>
                                <div class="widget-login__fill-bg"></div>                            
                            </div>
                        </a>
                    </div>
                </header>
            </section>
        </div>
        <!-- BEGIN: Vendor JS-->
        <script src="app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="assets/js/jquery.min.js"></script>
        <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
        <script src="app-assets/vendors/js/forms/wizard/bs-stepper.min.js"></script>
        <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
        <script src="app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
        <script src="app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
        <!-- END: Page Vendor JS-->f

        <!-- BEGIN: Theme JS-->
        <script src="app-assets/js/core/app-menu.min.js"></script>
        <script src="app-assets/js/core/app.min.js"></script>
        <!-- END: Theme JS-->
        <script src="assets/js/jwt-decode.js"></script>
        <script src="assets/js/start.js"></script>
    </body>

</html>