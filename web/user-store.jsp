    <!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="PIXINVENT">
        <title>User View - Account - Vuexy - Bootstrap HTML admin template</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/animate/animate.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/sweetalert2.min.css">
        <!--    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
            <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
            <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
            <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">-->
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/form-validation.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->
    <body class="vertical-layout vertical-menu-modern  navbar-floating footer-static   menu-collapsed" data-open="click" data-menu="vertical-menu-modern" data-col="">

        <!-- BEGIN: Header-->
        <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Email"><i class="ficon" data-feather="mail"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-chat.html" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Chat"><i class="ficon" data-feather="message-square"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calendar.html" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Calendar"><i class="ficon" data-feather="calendar"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-todo.html" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Todo"><i class="ficon" data-feather="check-square"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning" data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#" data-language="en"><i class="flag-icon flag-icon-us"></i> English</a><a class="dropdown-item" href="#" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a><a class="dropdown-item" href="#" data-language="de"><i class="flag-icon flag-icon-de"></i> German</a><a class="dropdown-item" href="#" data-language="pt"><i class="flag-icon flag-icon-pt"></i> Portuguese</a></div>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon" data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Vuexy..." tabindex="-1" data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-cart me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i class="ficon" data-feather="shopping-cart"></i><span class="badge rounded-pill bg-primary badge-up cart-item-count">6</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">My Cart</h4>
                                    <div class="badge rounded-pill badge-light-primary">4 Items</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list">
                                <div class="list-item align-items-center"><img class="d-block rounded me-1" src="app-assets/images/pages/eCommerce/1.png" alt="donuts" width="62">
                                    <div class="list-item-body flex-grow-1"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> Apple watch 5</a></h6><small class="cart-item-by">By Apple</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="1">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$374.90</h5>
                                    </div>
                                </div>
                                <div class="list-item align-items-center"><img class="d-block rounded me-1" src="app-assets/images/pages/eCommerce/7.png" alt="donuts" width="62">
                                    <div class="list-item-body flex-grow-1"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> Google Home Mini</a></h6><small class="cart-item-by">By Google</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="3">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$129.40</h5>
                                    </div>
                                </div>
                                <div class="list-item align-items-center"><img class="d-block rounded me-1" src="app-assets/images/pages/eCommerce/2.png" alt="donuts" width="62">
                                    <div class="list-item-body flex-grow-1"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> iPhone 11 Pro</a></h6><small class="cart-item-by">By Apple</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="2">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$699.00</h5>
                                    </div>
                                </div>
                                <div class="list-item align-items-center"><img class="d-block rounded me-1" src="app-assets/images/pages/eCommerce/3.png" alt="donuts" width="62">
                                    <div class="list-item-body flex-grow-1"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> iMac Pro</a></h6><small class="cart-item-by">By Apple</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="1">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$4,999.00</h5>
                                    </div>
                                </div>
                                <div class="list-item align-items-center"><img class="d-block rounded me-1" src="app-assets/images/pages/eCommerce/5.png" alt="donuts" width="62">
                                    <div class="list-item-body flex-grow-1"><i class="ficon cart-item-remove" data-feather="x"></i>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html"> MacBook Pro</a></h6><small class="cart-item-by">By Apple</small>
                                        </div>
                                        <div class="cart-item-qty">
                                            <div class="input-group">
                                                <input class="touchspin-cart" type="number" value="1">
                                            </div>
                                        </div>
                                        <h5 class="cart-item-price">$2,999.00</h5>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-menu-footer">
                                <div class="d-flex justify-content-between mb-1">
                                    <h6 class="fw-bolder mb-0">Total:</h6>
                                    <h6 class="text-primary fw-bolder mb-0">$10,999.00</h6>
                                </div><a class="btn btn-primary w-100" href="app-ecommerce-checkout.html">Checkout</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge rounded-pill bg-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-15.jpg" alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div></a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div></a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div></a>
                                <div class="list-item d-flex align-items-center">
                                    <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                    <div class="form-check form-check-primary form-switch">
                                        <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="form-check-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small class="notification-text"> USA Server is down due to high CPU usage</small>
                                        </div>
                                    </div></a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div></a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div></a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle dropdown-user-link"
                           id="dropdown-user" href="" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none lidget-naz" current-login="${user.getEmail()}">
                                <span class="user-name fw-bolder">${user.getFirstName()} ${user.getLastName()}</span>
                                <span class="user-status" style="margin-top:0.25rem">${user.getRole()}</span></div>
                            <span class="avatar">
                                <c:choose>
                                    <c:when test="${user.getAvatar() == null}">
                                        <c:set var="firstName" value="${user.getFirstName()}"/>
                                        <c:set var="lastName" value="${user.getLastName()}"/>
                                        <div class="avatar bg-light-danger me-50" style="margin-right:0 !important">
                                            <div class="avatar-content">${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                        </div>
                                    </c:when>
                                    <c:when test="${user.getAvatar() != null}">
                                        <img class="round" src="${user.getAvatar()}" alt="avatar" height="40"
                                             width="40">   
                                    </c:when>
                                </c:choose> 
                                <span class="avatar-status-online"></span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="userProfileController"><i class="me-50" data-feather="user"></i> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="page-account-settings-account.jsp"><i
                                    class="me-50" data-feather="settings"></i> Settings
                            </a>
                            <a class="dropdown-item" href="page-faq.jsp"><i class="me-50" data-feather="help-circle"></i> FAQ
                            </a>
                            <a class="dropdown-item" href="logoutController">
                                <i class="me-50" data-feather="power"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small></a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                        </div>
                    </div></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing Manager</small>
                        </div>
                    </div></a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div></a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No results found.</span></div></a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item me-auto">
                        <a class="navbar-brand position-relative" style="top:-25px" href="#">
                            <span>
                                <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo position-relative"
                                     style="left:-18px">
                            </span>
                            <h2 class="brand-text mb-0" style="position: relative; right: 35px;">Hilfsmotor</h2>
                        </a>
                    </li>
                    <li class="nav-item nav-toggle lidget-position-control"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                                class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                                class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc"
                                data-ticon="disc"></i></a></li>
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content">
                <ul class="navigation navigation-main mt-1" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" navigation-header"><span data-i18n="Misc">Apps & Page</span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="shopping-cart"></i><span
                                class="menu-title text-truncate" data-i18n="eCommerce">Product</span></a>
                        <ul class="menu-content">
                            <li class="d-flex align-items-center"><a class="d-flex align-items-center" href="productListController"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Shop">Shop</span></a>
                            </li>
                            <li class="active"><a class="d-flex align-items-center" href="store-list"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="store-list">Store</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="wishListController"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Wish List">Wish
                                        List</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="app-product-checkout.jsp"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Checkout">Checkout</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="user"></i><span
                                class="menu-title text-truncate" data-i18n="User">User</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="View">View</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="userProfileController"><span
                                                class="menu-item text-truncate" data-i18n="Account">Account</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="user-store.jsp"><span
                                                class="menu-item text-truncate" data-i18n="Security">Security</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="user-store.jsp"><span
                                                class="menu-item text-truncate" data-i18n="Billing &amp; Plans">Billing &amp; Plans</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="user-store.jsp"><span
                                                class="menu-item text-truncate" data-i18n="Notifications">Notifications</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="user-store.jsp"><span
                                                class="menu-item text-truncate" data-i18n="Connections">Connections</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span
                                class="menu-title text-truncate" data-i18n="Pages">Pages</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="help-page"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="FAQ">FAQ</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Blog">Blog</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="blog-list"><span
                                                class="menu-item text-truncate" data-i18n="List">List</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="manage-blog"><span
                                                class="menu-item text-truncate" data-i18n="Detail">Add</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="blog-draft"><span
                                                class="menu-item text-truncate" data-i18n="Detail">Draft</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="blog-save"><span
                                                class="menu-item text-truncate" data-i18n="Edit">Saved</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class=" navigation-header"><span data-i18n="Misc">Misc</span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="menu"></i><span
                                class="menu-title text-truncate" data-i18n="Menu Levels">Menu Levels</span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.1</span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Second Level">Second Level 2.2</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate"
                                                                                            data-i18n="Third Level">Third Level 3.1</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="#"><span class="menu-item text-truncate"
                                                                                            data-i18n="Third Level">Third Level 3.2</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="app-user-view-account">
                        <div class="row">
                            <!-- User Sidebar -->
                            <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
                                <!-- User Card -->
                                <div class="card">
                                    <div class="card-body">
                                        <div class="user-avatar-section">
                                            <div class="d-flex align-items-center flex-column">
                                                <c:set var="firstName" value="${store.getFirstName()}"/>
                                                <c:set var="lastName" value="${store.getLastName()}"/>
                                                <div class="avatar bg-light-danger me-50" style="margin-right:0 !important; height: 110px;width: 110px">
                                                    <div class="avatar-contentS" style="width:130px;height:150px;font-size:2rem;padding-top: 35px" >${fn:substring(firstName, 0, 1)}${fn:substring(lastName, 0, 1)}</div>
                                                </div>

                                                <div class="user-info text-center">
                                                    <h4>${store.getStoreName()} </h4>
                                                    <span class="badge bg-light-secondary">Store</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-around my-2 pt-75">
                                            <div class="d-flex align-items-start me-2">
                                                <span class="badge bg-light-primary p-75 rounded">
                                                    <i data-feather="check" class="font-medium-2"></i>
                                                </span>
                                                <div class="ms-75">
                                                    <h4 class="mb-0">${numProduct}</h4>
                                                    <small>Products</small>
                                                </div>
                                            </div>
                                            <div class="d-flex align-items-start">
                                                <span class="badge bg-light-primary p-75 rounded">
                                                    <i data-feather="briefcase" class="font-medium-2"></i>
                                                </span>
                                                <div class="ms-75">
                                                    <h4 class="mb-0">0</h4>
                                                    <small>News</small>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="fw-bolder border-bottom pb-50 mb-1">Details</h4>
                                        <div class="info-container">
                                            <ul class="list-unstyled">
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Owner:</span>
                                                    <span>${store.getFirstName()} ${store.getLastName()}</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Billing Email:</span>
                                                    <span>${store.getEmail()}</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Status:</span>
                                                    <span class="badge bg-light-success">Active</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Role:</span>
                                                    <span>SALEPERSON</span>
                                                </li>

                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Contact:</span>
                                                    <span>${store.getPhoneNumber()}</span>
                                                </li>

                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">State:</span>
                                                    <span>${store.getState()}</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">City:</span>
                                                    <span>${store.getCity()}</span>
                                                </li>
                                                <li class="mb-75">
                                                    <span class="fw-bolder me-25">Street:</span>
                                                    <span>${store.getStreet()}</span>
                                                </li>

                                            </ul>
                                            <div class="d-flex justify-content-center pt-2">
                                                <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#" data-bs-toggle="modal">
                                                    Follow
                                                </a>

                                                
                                                <a href="javascript:;" class="btn btn-outline-danger" data-bs-target="#report" id="reportButton" data-bs-toggle="modal">
                                                        Report
                                                </a>


                                            </div>
                                            <br>
                                            <div class="d-flex justify-content-center">

                                                <c:if test="${user.getRole()== 'SALEPERSON' && account.getAccountID()== store.getAccountID()}">
                                                    <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#addProduct" id="addProductButton" data-bs-toggle="modal">
                                                        Add Product
                                                    </a>
                                                </c:if>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /User Card -->

                            </div>
                            <!--/ User Sidebar -->

                            <!-- User Content -->
                            <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
                                <!-- User Pills -->

                                <!--/ User Pills -->

                                <!-- Project table -->
                                <div class="card">
                                    <h4 class="card-header">Product Upload</h4>
                                </div>    
                                <!-- /Project table -->
                                <div class="row">
                                    <c:forEach var="items" items="${listall}">
                                        <div class="col-lg-4 col-xl-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row m-b-10">
                                                        <div class="col-md-5 col-xxl-12">
                                                            <div class="new-arrival-product mb-4 mb-xxl-4 mb-md-0">
                                                                <div class="new-arrivals-img-contnent">
                                                                    <img class="img-fluid" src="assets//img//anh.jpg" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-7 col-xxl-12">
                                                            <div class="new-arrival-content position-relative">
                                                                <h4><a href="productDetailController?mx=${items.getProductID()}">${items.getName()}</a></h4>
                                                                <div class="comment-review star-rating">
                                                                    <p class="productid">Product ID: MX-00${items.getProductID()}</p>
                                                                    <span class="review-text">(2 reviews) </span>
                                                                    <p class="price">Price: ${items.getPrice()}</p>
                                                                    <span class="review-text">Brand: ${items.getBrand()} </span>
                                                                    <span class="review-text">Category: ${items.getCategory()} </span>

                                                                    <p class="text-content"></p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>



                                </div>
                                <div class="card">
                                    <h4 class="card-header">News</h4>
                                </div>
                                <div class="card">
                                    <h4 class="card-header"><br><br><br><br><br><br><br><br><br><br><br></h4>
                                </div>  
                                <!--/ User Content -->sus
                            </div>

                    </section>


                    <!-- add product Modal -->
                    <div class="modal fade" id="addProduct" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
                            <div class="modal-content">
                                <div class="modal-header bg-transparefsusnt">
                                    <button type="button" class="btn-close" id="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body pb-5 px-sm-5 pt-50" id="forAddproduct">
                                    <div class="text-center mb-2">
                                        <h1 class="mb-1">Add New Product</h1>
                                        <p>Request admin to add new product</p>
                                    </div>
                                    <form action="requestAddProduct" id="editUserForm" class="row gy-1 pt-75" method="post">

                                        <div class="d-flex align-items-center justify-content-center flex-direction-column">
                                            <a href="#" class="me-25">
                                                <img class="img-fluid rounded mt-3 mb-2" src="${product.getImage()}" id="account-upload-img" style="margin-top:0rem !important;margin-left:1rem; width:300px;height:300px;" alt="Picture" height="300"
                                                     width="300">   
                                            </a>
                                            <!-- upload and reset button -->
                                            <div class="d-flex align-items-end mt-75 ms-1">
                                                <div>
                                                    <label for="account-upload" class="btn btn-sm btn-primary mb-75 me-75 waves-effect waves-float waves-light width-full">Upload</label>
                                                    <input type="file" id="account-upload" name="image" hidden="" accept="image/*" required>

                                                    <p class="mb-0">Allowed file types: png, jpg, jpeg.</p>
                                                </div>
                                            </div>
                                            <!--/ upload and reset button -->
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-icon-default-fullname">Product Name</label>
                                            <input type="text" class="form-control dt-product-name" id="basic-icon-default-product-name"
                                                   placeholder="Honda MX-219" name="productName" required pattern="^[a-zA-Z0-9]{1,20}$" >
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="category">Category</label>
                                                <div class="position-relative">
                                                    <select name="productCategory" id="category" class="select2 form-select select2-hidden-accessible"
                                                            data-select1-id="category" tabindex="-1" aria-hidden="true">
                                                        <option value="Chopper" data-select1-id="1">Chopper</option>
                                                        <option value="Cross">Cross</option>
                                                        <option value="Enduro">Enduro</option>
                                                        <option value="Cruiser">Cruiser</option>
                                                        <option value="ATV">ATV</option>
                                                        <option value="Touring">Touring</option>
                                                        <option value="Naked">Naked</option>
                                                        <option value="Street">Street</option>
                                                        <option value="Sport Tourist">Sport Tourist</option>
                                                        <option value="Sport">Sport</option>
                                                        <option value="Scooter">Scooter</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="brand">Brand</label>
                                                <div class="position-relative">
                                                    <select name="productBrand" id="brand" class="select2 form-select select2-hidden-accessible"
                                                            data-select2-id="brand" tabindex="-1" aria-hidden="true">
                                                        <option value="Arch Motorcycle" data-select2-id="1">Arch Motorcycle</option>
                                                        <option value="Harley Davidson">Harley Davidson</option>
                                                        <option value="Indian Motorcycle">Indian Motorcycle</option>
                                                        <option value="Janus Motorcycles">Janus Motorcycles</option>
                                                        <option value="Lightning Motorcycle">Lightning Motorcycle</option>
                                                        <option value="Zero Motorcycles">Zero Motorcycles</option>
                                                        <option value="Ace Motor Corporation">Ace Motor Corporation</option>
                                                        <option value="Alligator Motorcycle Company">Alligator Motorcycle Company</option>
                                                        <option value="Allstate">Allstate</option>
                                                        <option value="Arch Motorcycle">Arch Motorcycle</option>
                                                        <option value="Boss Hoss Cycles">Boss Hoss Cycles</option>
                                                        <option value="Brammo Inc.">Brammo Inc.</option>
                                                        <option value="Buell Motorcycle Company">Buell Motorcycle Company</option>
                                                        <option value="Confederate Motors">Confederate Motors</option>
                                                        <option value="Cooper">Cooper</option>
                                                        <option value="Crocker Motorcycle Company">Crocker Motorcycle Company</option>
                                                        <option value="Erik Buell Racing">Erik Buell Racing</option>
                                                        <option value="Excelsior-Henderson Motorcycle">Excelsior-Henderson Motorcycle</option>
                                                        <option value="Fischer Motor Company">Fischer Motor Company</option>
                                                        <option value="Flying Merkel">Flying Merkel</option>
                                                        <option value="Penton">Penton</option>
                                                        <option value="Thor">Thor</option>
                                                        <option value="Titan Motorcycle Company">Titan Motorcycle Company</option>
                                                        <option value="Victory Motorcycles">Victory Motorcycles</option>
                                                        <option value="Z Electric Vehicle">Z Electric Vehicle</option>
                                                        <option value="Zero Motorcycles">Zero Motorcycles</option>
                                                        <option value="Triumph">Triumph</option>
                                                        <option value="AJS">AJS</option>
                                                        <option value="AJW Motorcycles">AJW Motorcycles</option>
                                                        <option value="Ambassador Motorcycles">Ambassador Motorcycles</option>
                                                        <option value="Ariel Motor Company">Ariel Motor Company</option>
                                                        <option value="Associated Motor Cycles">Associated Motor Cycles</option>
                                                        <option value="Armstrong">Armstrong</option>
                                                        <option value="Beardmore Precision">Beardmore Precision</option>
                                                        <option value="Blackburne">Blackburne</option>
                                                        <option value="Brough Motorcycles">Brough Motorcycles</option>
                                                        <option value="Brough Superior">Brough Superior</option>
                                                        <option value="BSA Motorcycles">BSA Motorcycles</option>
                                                        <option value="Calthorpe Motor Company">Calthorpe Motor Company</option>
                                                        <option value="Cheney Racing">Cheney Racing</option>
                                                        <option value="Clews Competition Motorcycles">Clews Competition Motorcycles</option>
                                                        <option value="Clyno">Clyno</option>
                                                        <option value="Cotton Motor Company">Cotton Motor Company</option>
                                                        <option value="Coventry-Eagle">Coventry-Eagle</option>
                                                        <option value="Douglas">Douglas</option>
                                                        <option value="EMC Motorcycles">EMC Motorcycles</option>
                                                        <option value="Excelsior Motor Company">Excelsior Motor Company</option>
                                                        <option value="Francis-Barnett">Francis-Barnett</option>
                                                        <option value="Greeves">Greeves</option>
                                                        <option value="Hesketh Motorcycles">Hesketh Motorcycles</option>
                                                        <option value="Haden">Haden</option>
                                                        <option value="HRD Motorcycles">HRD Motorcycles</option>
                                                        <option value="Ivy">Ivy</option>
                                                        <option value="James Cycle Co">James Cycle Co</option>
                                                        <option value="Levis">Levis</option>
                                                        <option value="Megelli Motorcycles">Megelli Motorcycles</option>
                                                        <option value="Métisse Motorcycles">Métisse Motorcycles</option>
                                                        <option value="Martinsyde">Martinsyde</option>
                                                        <option value="Matchless">Matchless</option>
                                                        <option value="New Hudson Motorcycles">New Hudson Motorcycles</option>
                                                        <option value="New Imperial Motors">New Imperial Motors</option>
                                                        <option value="Norman Cycles">Norman Cycles</option>
                                                        <option value="Norton Motorcycle Company">Norton Motorcycle Company</option>
                                                        <option value="Osborn Engineering Company">Osborn Engineering Company</option>
                                                        <option value="OK-Supreme">OK-Supreme</option>
                                                        <option value="Premier Motorcycles">Premier Motorcycles</option>
                                                        <option value="Quadrant">Quadrant</option>
                                                        <option value="Raleigh Bicycle Company">Raleigh Bicycle Company</option>
                                                        <option value="Rickman Motorcycles">Rickman Motorcycles</option>
                                                        <option value="Royal Enfield">Royal Enfield</option>
                                                        <option value="Rudge-Whitworth">Rudge-Whitworth</option>
                                                        <option value="Scott Motorcycle Company">Scott Motorcycle Company</option>
                                                        <option value="Silk Engineering">Silk Engineering</option>
                                                        <option value="Singer Motors">Singer Motors</option>
                                                        <option value="Sprite">Sprite</option>
                                                        <option value="Sun">Sun</option>
                                                        <option value="Sunbeam Cycles">Sunbeam Cycles</option>
                                                        <option value="Triumph Engineering">Triumph Engineering</option>
                                                        <option value="Velocette">Velocette</option>
                                                        <option value="Villiers Engineering">Villiers Engineering</option>
                                                        <option value="Vincent Motorcycles">Vincent Motorcycles</option>
                                                        <option value="Wasp Motorcycles">Wasp Motorcycles</option>
                                                        <option value="Wooler">Wooler</option>
                                                        <option value="Zenith Motorcycles">Zenith Motorcycles</option>
                                                        <option value="Ditrimit">Ditrimit</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="basic-icon-default-model-year">Model Year</label>
                                                <input  type="number" id="basic-icon-default-model-year" class="form-control dt-model-year"
                                                        placeholder="2022" name="modelYear" required pattern="/^(19[5-9]\d|20[0-4]\d|2050)$/">
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="basic-icon-default-price">Price</label>
                                                <input  type="number" id="basic-icon-default-price" class="form-control dt-price"
                                                        placeholder="19.99" name="price" required pattern="^[0-9]{10}$">
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="basic-icon-default-displacement">Displacement</label>
                                                <input  type="number" id="basic-icon-default-displacement" class="form-control dt-displacement"
                                                        placeholder="400cc" name="displacement" required pattern="^[a-zA-Z0-9]{1,20}$">
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="brand">Transmission</label>
                                                <div class="position-relative">
                                                    <select name="productTransmission" id="transmission" class="select2 form-select select2-hidden-accessible"
                                                            data-select3-id="transmission" tabindex="-1" aria-hidden="true">
                                                        <option value="Manual gearing" data-select3-id="1">Manual gearing</option>
                                                        <option value="Automatic">Automatic</option>
                                                        <option value="Semi-automatic">Semi-automatic</option>
                                                        <option value="Reverse gear">Reverse gear</option>
                                                        <option value="Shift control">Shift control</option>
                                                        <option value="Scooters">Scooters</option>
                                                        <option value="underbones">underbones</option>
                                                        <option value="Miniatures">Miniatures</option>
                                                        <option value="Clutch">Clutch</option>
                                                        <option value="Construction">Construction</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="starter">Starter</label>
                                            <div class="position-relative">
                                                <select name="productStater" id="starter" class="select2 form-select select2-hidden-accessible"
                                                        data-select4-id="starter" tabindex="-1" aria-hidden="true">
                                                    <option value="Kickstart" data-select4-id="1">Kickstart</option>
                                                    <option value="Electric">Electric</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="fuelSystem">Fuel System</label>
                                            <div class="position-relative">
                                                <select name="productFuelSystem" id="fuelSystem" class="select2 form-select select2-hidden-accessible"
                                                        data-select4-id="fuelSystem" tabindex="-1" aria-hidden="true">
                                                    <option value="Ethanol Fuel" data-select4-id="1">Ethanol Fuel</option>
                                                    <option value="Diesel Fuels">Diesel Fuels</option>
                                                    <option value="Liquid Fuels">Liquid Fuels</option>
                                                    <option value="Gasoline Fuels">Gasoline Fuels</option>
                                                    <option value="Biodiesel Fuels">Biodiesel Fuels</option>
                                                    <option value="Artificial Fuels">Artificial Fuels</option>
                                                    <option value="Natural Fuels">Natural Fuels</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="basic-icon-default-fuelcapacity">Fuel Capacity</label>
                                            <input type="number" id="basic-icon-default-fuelcapacity" class="form-control dt-fuelcapacity"
                                                   placeholder="2.1 - 5.75 gallons" name="fuelCapacity" required pattern="^[a-zA-Z0-9]{4,10}$">
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="basic-icon-default-dryweight">Dry Weight</label>
                                            <input type="number" id="basic-icon-default-dryweight" class="form-control dt-dryweight"
                                                   placeholder="13.5 pounds" name="dryWeight" required pattern="^[a-zA-Z0-9]{4,10}$">
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="basic-icon-default-seatHeight">Seat Height</label>
                                            <input type="number" id="basic-icon-default-seatHeight" class="form-control dt-seatHeight"
                                                   placeholder="31.6 inches" name="seatHeight" required pattern="^[a-zA-Z0-9]{4,10}$">
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="Mode">Mode</label>
                                            <div class="position-relative">
                                                <select onchange="changeType();"  name="productMode" id="productMode" class="select2 form-select select2-hidden-accessible"
                                                        data-select4-id="productMode" tabindex="-1" aria-hidden="true">
                                                    <option value="Rental" data-select4-id="1">Rental</option>
                                                    <option value="Sell">Sell</option>


                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-1 col-12 col-sm-12"  >
                                            <label class="form-label" for="basic-icon-default-fullname">Description</label>
                                            <input type="text" class="form-control dt-product-name" id="basic-icon-default-product-name"
                                                   placeholder="Description" name="description" required pattern="^[a-zA-Z0-9]{1,20}$" >
                                        </div>
                                        <div class="mb-1 col-12 col-sm-12" id="chooseHireMode" >
                                            <label class="form-label" for="hireMode">Hire Mode</label>
                                            <div class="position-relative">
                                                <select name="productHireMode" id="hireMode" class="select2 form-select select2-hidden-accessible"
                                                        data-select4-id="hireMode" tabindex="-1" aria-hidden="true">
                                                    <option value="For Day" data-select4-id="1">For Day</option>
                                                    <option value="For Week">For Week</option>
                                                    <option value="For Month">For Month</option>
                                                    <option value="For Year">For Year</option>

                                                </select>
                                            </div>
                                        </div>






                                        <div class="col-12 text-center mt-2 pt-50">
                                            <button type="submit" class="btn btn-primary me-1" id="addProduct">Submit</button>
                                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                                Discard
                                            </button>
                                        </div>
                                        <input type="hidden" name="avatarImage" class="hiddenI" value="">
                                    </form>
                                </div>
                            </div>
                        </div>                      
                    </div>

                    <!--/ Add product -->
                    <!--/ Report Store -->
                    <div class="modal fade" id="report" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
                            <div class="modal-content">
                                <div class="modal-header bg-transparefsusnt">
                                    <button type="button" class="btn-close" id="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body pb-5 px-sm-5 pt-50" id="forreport">
                                    <div class="text-center mb-2">
                                        <h1 class="mb-1">Report</h1>
                                        <p>Request admin to add new product</p>
                                    </div>
                                    <form action="requestAddProduct" id="editUserForm" class="row gy-1 pt-75" method="post">

                                        <div class="d-flex align-items-center justify-content-center flex-direction-column">
                                            <a href="#" class="me-25">
                                                <img class="img-fluid rounded mt-3 mb-2" src="${product.getImage()}" id="account-upload-img" style="margin-top:0rem !important;margin-left:1rem; width:300px;height:300px;" alt="Picture" height="300"
                                                     width="300">   
                                            </a>
                                            <!-- upload and reset button -->
                                            <div class="d-flex align-items-end mt-75 ms-1">
                                                <div>
                                                    <label for="account-upload" class="btn btn-sm btn-primary mb-75 me-75 waves-effect waves-float waves-light width-full">Upload</label>
                                                    <input type="file" id="account-upload" name="image" hidden="" accept="image/*" required>

                                                    <p class="mb-0">Allowed file types: png, jpg, jpeg.</p>
                                                </div>
                                            </div>
                                            <!--/ upload and reset button -->
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-icon-default-fullname">Product Name</label>
                                            <input type="text" class="form-control dt-product-name" id="basic-icon-default-product-name"
                                                   placeholder="Honda MX-219" name="productName" required pattern="^[a-zA-Z0-9]{1,20}$" >
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="category">Category</label>
                                                <div class="position-relative">
                                                    <select name="productCategory" id="category" class="select2 form-select select2-hidden-accessible"
                                                            data-select1-id="category" tabindex="-1" aria-hidden="true">
                                                        <option value="Chopper" data-select1-id="1">Chopper</option>
                                                        <option value="Cross">Cross</option>
                                                        <option value="Enduro">Enduro</option>
                                                        <option value="Cruiser">Cruiser</option>
                                                        <option value="ATV">ATV</option>
                                                        <option value="Touring">Touring</option>
                                                        <option value="Naked">Naked</option>
                                                        <option value="Street">Street</option>
                                                        <option value="Sport Tourist">Sport Tourist</option>
                                                        <option value="Sport">Sport</option>
                                                        <option value="Scooter">Scooter</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="brand">Brand</label>
                                                <div class="position-relative">
                                                    <select name="productBrand" id="brand" class="select2 form-select select2-hidden-accessible"
                                                            data-select2-id="brand" tabindex="-1" aria-hidden="true">
                                                        <option value="Arch Motorcycle" data-select2-id="1">Arch Motorcycle</option>
                                                        <option value="Harley Davidson">Harley Davidson</option>
                                                        <option value="Indian Motorcycle">Indian Motorcycle</option>
                                                        <option value="Janus Motorcycles">Janus Motorcycles</option>
                                                        <option value="Lightning Motorcycle">Lightning Motorcycle</option>
                                                        <option value="Zero Motorcycles">Zero Motorcycles</option>
                                                        <option value="Ace Motor Corporation">Ace Motor Corporation</option>
                                                        <option value="Alligator Motorcycle Company">Alligator Motorcycle Company</option>
                                                        <option value="Allstate">Allstate</option>
                                                        <option value="Arch Motorcycle">Arch Motorcycle</option>
                                                        <option value="Boss Hoss Cycles">Boss Hoss Cycles</option>
                                                        <option value="Brammo Inc.">Brammo Inc.</option>
                                                        <option value="Buell Motorcycle Company">Buell Motorcycle Company</option>
                                                        <option value="Confederate Motors">Confederate Motors</option>
                                                        <option value="Cooper">Cooper</option>
                                                        <option value="Crocker Motorcycle Company">Crocker Motorcycle Company</option>
                                                        <option value="Erik Buell Racing">Erik Buell Racing</option>
                                                        <option value="Excelsior-Henderson Motorcycle">Excelsior-Henderson Motorcycle</option>
                                                        <option value="Fischer Motor Company">Fischer Motor Company</option>
                                                        <option value="Flying Merkel">Flying Merkel</option>
                                                        <option value="Penton">Penton</option>
                                                        <option value="Thor">Thor</option>
                                                        <option value="Titan Motorcycle Company">Titan Motorcycle Company</option>
                                                        <option value="Victory Motorcycles">Victory Motorcycles</option>
                                                        <option value="Z Electric Vehicle">Z Electric Vehicle</option>
                                                        <option value="Zero Motorcycles">Zero Motorcycles</option>
                                                        <option value="Triumph">Triumph</option>
                                                        <option value="AJS">AJS</option>
                                                        <option value="AJW Motorcycles">AJW Motorcycles</option>
                                                        <option value="Ambassador Motorcycles">Ambassador Motorcycles</option>
                                                        <option value="Ariel Motor Company">Ariel Motor Company</option>
                                                        <option value="Associated Motor Cycles">Associated Motor Cycles</option>
                                                        <option value="Armstrong">Armstrong</option>
                                                        <option value="Beardmore Precision">Beardmore Precision</option>
                                                        <option value="Blackburne">Blackburne</option>
                                                        <option value="Brough Motorcycles">Brough Motorcycles</option>
                                                        <option value="Brough Superior">Brough Superior</option>
                                                        <option value="BSA Motorcycles">BSA Motorcycles</option>
                                                        <option value="Calthorpe Motor Company">Calthorpe Motor Company</option>
                                                        <option value="Cheney Racing">Cheney Racing</option>
                                                        <option value="Clews Competition Motorcycles">Clews Competition Motorcycles</option>
                                                        <option value="Clyno">Clyno</option>
                                                        <option value="Cotton Motor Company">Cotton Motor Company</option>
                                                        <option value="Coventry-Eagle">Coventry-Eagle</option>
                                                        <option value="Douglas">Douglas</option>
                                                        <option value="EMC Motorcycles">EMC Motorcycles</option>
                                                        <option value="Excelsior Motor Company">Excelsior Motor Company</option>
                                                        <option value="Francis-Barnett">Francis-Barnett</option>
                                                        <option value="Greeves">Greeves</option>
                                                        <option value="Hesketh Motorcycles">Hesketh Motorcycles</option>
                                                        <option value="Haden">Haden</option>
                                                        <option value="HRD Motorcycles">HRD Motorcycles</option>
                                                        <option value="Ivy">Ivy</option>
                                                        <option value="James Cycle Co">James Cycle Co</option>
                                                        <option value="Levis">Levis</option>
                                                        <option value="Megelli Motorcycles">Megelli Motorcycles</option>
                                                        <option value="Métisse Motorcycles">Métisse Motorcycles</option>
                                                        <option value="Martinsyde">Martinsyde</option>
                                                        <option value="Matchless">Matchless</option>
                                                        <option value="New Hudson Motorcycles">New Hudson Motorcycles</option>
                                                        <option value="New Imperial Motors">New Imperial Motors</option>
                                                        <option value="Norman Cycles">Norman Cycles</option>
                                                        <option value="Norton Motorcycle Company">Norton Motorcycle Company</option>
                                                        <option value="Osborn Engineering Company">Osborn Engineering Company</option>
                                                        <option value="OK-Supreme">OK-Supreme</option>
                                                        <option value="Premier Motorcycles">Premier Motorcycles</option>
                                                        <option value="Quadrant">Quadrant</option>
                                                        <option value="Raleigh Bicycle Company">Raleigh Bicycle Company</option>
                                                        <option value="Rickman Motorcycles">Rickman Motorcycles</option>
                                                        <option value="Royal Enfield">Royal Enfield</option>
                                                        <option value="Rudge-Whitworth">Rudge-Whitworth</option>
                                                        <option value="Scott Motorcycle Company">Scott Motorcycle Company</option>
                                                        <option value="Silk Engineering">Silk Engineering</option>
                                                        <option value="Singer Motors">Singer Motors</option>
                                                        <option value="Sprite">Sprite</option>
                                                        <option value="Sun">Sun</option>
                                                        <option value="Sunbeam Cycles">Sunbeam Cycles</option>
                                                        <option value="Triumph Engineering">Triumph Engineering</option>
                                                        <option value="Velocette">Velocette</option>
                                                        <option value="Villiers Engineering">Villiers Engineering</option>
                                                        <option value="Vincent Motorcycles">Vincent Motorcycles</option>
                                                        <option value="Wasp Motorcycles">Wasp Motorcycles</option>
                                                        <option value="Wooler">Wooler</option>
                                                        <option value="Zenith Motorcycles">Zenith Motorcycles</option>
                                                        <option value="Ditrimit">Ditrimit</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="basic-icon-default-model-year">Model Year</label>
                                                <input  type="number" id="basic-icon-default-model-year" class="form-control dt-model-year"
                                                        placeholder="2022" name="modelYear" required pattern="/^(19[5-9]\d|20[0-4]\d|2050)$/">
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="basic-icon-default-price">Price</label>
                                                <input  type="number" id="basic-icon-default-price" class="form-control dt-price"
                                                        placeholder="19.99" name="price" required pattern="^[0-9]{10}$">
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="basic-icon-default-displacement">Displacement</label>
                                                <input  type="number" id="basic-icon-default-displacement" class="form-control dt-displacement"
                                                        placeholder="400cc" name="displacement" required pattern="^[a-zA-Z0-9]{1,20}$">
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="brand">Transmission</label>
                                                <div class="position-relative">
                                                    <select name="productTransmission" id="transmission" class="select2 form-select select2-hidden-accessible"
                                                            data-select3-id="transmission" tabindex="-1" aria-hidden="true">
                                                        <option value="Manual gearing" data-select3-id="1">Manual gearing</option>
                                                        <option value="Automatic">Automatic</option>
                                                        <option value="Semi-automatic">Semi-automatic</option>
                                                        <option value="Reverse gear">Reverse gear</option>
                                                        <option value="Shift control">Shift control</option>
                                                        <option value="Scooters">Scooters</option>
                                                        <option value="underbones">underbones</option>
                                                        <option value="Miniatures">Miniatures</option>
                                                        <option value="Clutch">Clutch</option>
                                                        <option value="Construction">Construction</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="starter">Starter</label>
                                            <div class="position-relative">
                                                <select name="productStater" id="starter" class="select2 form-select select2-hidden-accessible"
                                                        data-select4-id="starter" tabindex="-1" aria-hidden="true">
                                                    <option value="Kickstart" data-select4-id="1">Kickstart</option>
                                                    <option value="Electric">Electric</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="fuelSystem">Fuel System</label>
                                            <div class="position-relative">
                                                <select name="productFuelSystem" id="fuelSystem" class="select2 form-select select2-hidden-accessible"
                                                        data-select4-id="fuelSystem" tabindex="-1" aria-hidden="true">
                                                    <option value="Ethanol Fuel" data-select4-id="1">Ethanol Fuel</option>
                                                    <option value="Diesel Fuels">Diesel Fuels</option>
                                                    <option value="Liquid Fuels">Liquid Fuels</option>
                                                    <option value="Gasoline Fuels">Gasoline Fuels</option>
                                                    <option value="Biodiesel Fuels">Biodiesel Fuels</option>
                                                    <option value="Artificial Fuels">Artificial Fuels</option>
                                                    <option value="Natural Fuels">Natural Fuels</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="basic-icon-default-fuelcapacity">Fuel Capacity</label>
                                            <input type="number" id="basic-icon-default-fuelcapacity" class="form-control dt-fuelcapacity"
                                                   placeholder="2.1 - 5.75 gallons" name="fuelCapacity" required pattern="^[a-zA-Z0-9]{4,10}$">
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="basic-icon-default-dryweight">Dry Weight</label>
                                            <input type="number" id="basic-icon-default-dryweight" class="form-control dt-dryweight"
                                                   placeholder="13.5 pounds" name="dryWeight" required pattern="^[a-zA-Z0-9]{4,10}$">
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="basic-icon-default-seatHeight">Seat Height</label>
                                            <input type="number" id="basic-icon-default-seatHeight" class="form-control dt-seatHeight"
                                                   placeholder="31.6 inches" name="seatHeight" required pattern="^[a-zA-Z0-9]{4,10}$">
                                        </div>
                                        <div class="mb-1 col-6 col-sm-6">
                                            <label class="form-label" for="Mode">Mode</label>
                                            <div class="position-relative">
                                                <select onchange="changeType();"  name="productMode" id="productMode" class="select2 form-select select2-hidden-accessible"
                                                        data-select4-id="productMode" tabindex="-1" aria-hidden="true">
                                                    <option value="Rental" data-select4-id="1">Rental</option>
                                                    <option value="Sell">Sell</option>


                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-1 col-12 col-sm-12"  >
                                            <label class="form-label" for="basic-icon-default-fullname">Description</label>
                                            <input type="text" class="form-control dt-product-name" id="basic-icon-default-product-name"
                                                   placeholder="Description" name="description" required pattern="^[a-zA-Z0-9]{1,20}$" >
                                        </div>
                                        <div class="mb-1 col-12 col-sm-12" id="chooseHireMode" >
                                            <label class="form-label" for="hireMode">Hire Mode</label>
                                            <div class="position-relative">
                                                <select name="productHireMode" id="hireMode" class="select2 form-select select2-hidden-accessible"
                                                        data-select4-id="hireMode" tabindex="-1" aria-hidden="true">
                                                    <option value="For Day" data-select4-id="1">For Day</option>
                                                    <option value="For Week">For Week</option>
                                                    <option value="For Month">For Month</option>
                                                    <option value="For Year">For Year</option>

                                                </select>
                                            </div>
                                        </div>






                                        <div class="col-12 text-center mt-2 pt-50">
                                            <button type="submit" class="btn btn-primary me-1" id="addProduct">Submit</button>
                                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                                Discard
                                            </button>
                                        </div>
                                        <input type="hidden" name="avatarImage" class="hiddenI" value="">
                                    </form>
                                </div>
                            </div>
                        </div>                      
                    </div>
                    
                    <!--/ Report Store -->

                </div>
            </div>
        </div>
        <!-- END: Content-->


        <!-- BEGIN: Customizer-->
        <div class="customizer d-none d-md-block"><a class="customizer-toggle d-flex align-items-center justify-content-center" href="#"><i class="spinner" data-feather="settings"></i></a><div class="customizer-content">
                <!-- Customizer header -->
                <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                    <h4 class="mb-0">Theme Customizer</h4>
                    <p class="m-0">Customize & Preview in Real Time</p>

                    <a class="customizer-close" href="#"><i data-feather="x"></i></a>
                </div>

                <hr />

                <!-- Styling & Text Direction -->
                <div class="customizer-styling-direction px-2">
                    <p class="fw-bold">Skin</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input
                                type="radio"
                                id="skinlight"
                                name="skinradio"
                                class="form-check-input layout-name"
                                checked
                                data-layout=""
                                />
                            <label class="form-check-label" for="skinlight">Light</label>
                        </div>
                        <div class="form-check me-1">
                            <input
                                type="radio"
                                id="skinbordered"
                                name="skinradio"
                                class="form-check-input layout-name"
                                data-layout="bordered-layout"
                                />
                            <label class="form-check-label" for="skinbordered">Bordered</label>
                        </div>
                        <div class="form-check me-1">
                            <input
                                type="radio"
                                id="skindark"
                                name="skinradio"
                                class="form-check-input layout-name"
                                data-layout="dark-layout"
                                />
                            <label class="form-check-label" for="skindark">Dark</label>
                        </div>
                        <div class="form-check">
                            <input
                                type="radio"
                                id="skinsemidark"
                                name="skinradio"
                                class="form-check-input layout-name"
                                data-layout="semi-dark-layout"
                                />
                            <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                        </div>
                    </div>
                </div>

                <hr />

                <!-- Menu -->
                <div class="customizer-menu px-2">
                    <div id="customizer-menu-collapsible" class="d-flex">
                        <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                        <div class="form-check form-check-primary form-switch">
                            <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                            <label class="form-check-label" for="collapse-sidebar-switch"></label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Layout Width -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Layout Width</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                            <label class="form-check-label" for="layout-width-full">Full Width</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Navbar -->
                <div class="customizer-navbar px-2">
                    <div id="customizer-navbar-colors">
                        <p class="fw-bold">Navbar Color</p>
                        <ul class="list-inline unstyled-list">
                            <li class="color-box bg-white border selected" data-navbar-default=""></li>
                            <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                            <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                            <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                            <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                            <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                            <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                            <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                        </ul>
                    </div>

                    <p class="navbar-type-text fw-bold">Navbar Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                            <label class="form-check-label" for="nav-type-floating">Floating</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-static">Static</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Footer -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Footer Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                            <label class="form-check-label" for="footer-type-static">Static</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End: Customizer-->



        <!-- Buynow Button-->
        <div class="buy-now"><a href="https://1.envato.market/vuexy_admin" target="_blank" class="btn btn-danger">Buy Now</a>

        </div>
        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix mb-0"><span class="float-md-start d-block d-md-inline-block mt-25">COPYRIGHT  &copy; 2021<a class="ms-25" href="https://1.envato.market/pixinvent_portfolio" target="_blank">Pixinvent</a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span><span class="float-md-end d-none d-md-block">Hand-crafted & Made with<i data-feather="heart"></i></span></p>
        </footer>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
        <!-- END: Footer-->


        <!-- BEGIN: Vendor JS-->
        <script src="app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
        <script src="app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
        <script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
        <script src="app-assets/vendors/js/extensions/moment.min.js"></script>
        <!--    <script src="app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/responsive.bootstrap5.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
            <script src="app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>-->
        <script src="app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
        <script src="app-assets/vendors/js/extensions/polyfill.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="app-assets/js/core/app-menu.min.js"></script>
        <script src="app-assets/js/core/app.min.js"></script>
        <script src="app-assets/js/scripts/customizer.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="app-assets/js/scripts/pages/modal-edit-user.min.js"></script>
        <script src="app-assets/js/scripts/pages/app-user-view-account.min.js"></script>
        <script src="app-assets/js/scripts/pages/app-user-view.min.js"></script>
        <script src="app-assets/js/scripts/pages/page-account-settings-account.min.js"></script>
        <script src="assets/js/page-account-settings.js"></script>
        <!-- END: Page JS-->

        <script>
                                                    function changeType() {

                                                        var i = document.getElementById("productMode");
                                                        var e = i.options[i.selectedIndex].value;
                                                        var select = document.getElementById('chooseHireMode');

                                                        if (e === "Rental") {

                                                            select.style.visibility = 'visible';
                                                        }
                                                        if (e === "Sell") {

                                                            select.style.visibility = 'hidden';

                                                        }


                                                    }
                                                    $(window).on('load', function () {
                                                        if (feather) {
                                                            feather.replace({width: 14, height: 14});
                                                        }
                                                    })
                                                    $(document).ready(function () {
                                                        var readURL = function (input) {
                                                            if (input.files && input.files[0]) {
                                                                var reader = new FileReader();
                                                                var fileByteArray = [];
                                                                reader.readAsArrayBuffer(input.files[0]);
                                                                reader.onload = function (evt) {
                                                                    if (evt.target.readyState === FileReader.DONE) {
                                                                        var arrayBuffer = evt.target.result,
                                                                                array = new Uint8Array(arrayBuffer);
                                                                        for (var i = 0; i < array.length; i++) {
                                                                            fileByteArray.push(array[i]);
                                                                        }
                                                                        console.log(fileByteArray);
                                                                        document.querySelector(".hiddenI").value = fileByteArray;
                                                                    }
                                                                };
                                                            }
                                                        };
                                                        $("#account-upload").on('change', function () {
                                                            readURL(this);
                                                        });
                                                    });
        </script>
    </body>
    <!-- END: Body-->
</html>