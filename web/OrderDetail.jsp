<!DOCTYPE html>

<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
    <%@page contentType="text/html" pageEncoding="UTF-8"%>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/app-invoice.min.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover"
          data-menu="horizontal-menu" data-col="">

        <!-- BEGIN: Header-->
        <nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center"
             data-nav="brand-center">
            <div class="navbar-header d-xl-block d-none">
                <ul class="nav navbar-nav">
                    <li class="nav-item">
                        <a class="navbar-brand" href="productListController">
                            <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                            <h2 class="brand-text mb-0 lidget-app__padding-left">Hilfsmotor</h2>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="navbar-container d-flex content">
                <div class="bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calendar.html" data-bs-toggle="tooltip"
                                                                  data-bs-placement="bottom" title="Calendar"><i class="ficon" data-feather="calendar"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning"
                                                                                                    data-feather="star"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                                <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav align-items-center ms-auto">
                    <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag" href="#"
                                                                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="flag-icon flag-icon-us"></i><span class="selected-language">English</span></a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"
                                                                                                        data-language="en"><i class="flag-icon flag-icon-us"></i> English</a>
                    </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon"
                                                                                                 data-feather="sun"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon"
                                                                                           data-feather="search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Explore Hilfsmotor..." tabindex="-1"
                                   data-search="search">
                            <div class="search-input-close"><i data-feather="x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>

                    <li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#"
                                                                                 data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span
                                class="badge rounded-pill bg-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header d-flex">
                                    <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                    <div class="badge rounded-pill badge-light-primary">6 New</div>
                                </div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-15.jpg"
                                                                     alt="avatar" width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Congratulation Sam ?</span>winner!</p><small
                                                class="notification-text"> Won the monthly best seller badge.</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar"><img src="app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar"
                                                                     width="32" height="32"></div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small
                                                class="notification-text"> You have 10 unread messages</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content">MD</div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Revised Order ?</span>&nbsp;checkout</p><small
                                                class="notification-text"> MD Inc. order updated</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="list-item d-flex align-items-center">
                                    <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                    <div class="form-check form-check-primary form-switch">
                                        <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                        <label class="form-check-label" for="systemNotification"></label>
                                    </div>
                                </div><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-danger">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small
                                                class="notification-text"> USA Server is down due to hight CPU usage</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-success">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small
                                                class="notification-text"> Last month sales report generated</small>
                                        </div>
                                    </div>
                                </a><a class="d-flex" href="#">
                                    <div class="list-item d-flex align-items-start">
                                        <div class="me-1">
                                            <div class="avatar bg-light-warning">
                                                <div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
                                            </div>
                                        </div>
                                        <div class="list-item-body flex-grow-1">
                                            <p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small
                                                class="notification-text"> BLR Server using high memory</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link"
                                                                   id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none"><span class="user-name fw-bolder">John Doe</span><span
                                    class="user-status">Admin</span></div><span class="avatar"><img class="round"
                                                                                            src="app-assets//images/portrait/small/avatar-s-11.jpg" alt="avatar" height="40"
                                                                                            width="40"><span class="avatar-status-online"></span></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user"><a class="dropdown-item"
                                                                                                        href="page-profile.html"><i class="me-50" data-feather="user"></i> Profile</a>
                            <div class="dropdown-divider"></div><a class="dropdown-item" href="page-account-settings-account.html"><i
                                    class="me-50" data-feather="settings"></i> Settings</a><a class="dropdown-item" href="page-faq.html"><i
                                    class="me-50" data-feather="help-circle"></i> FAQ</a><a class="dropdown-item"
                                                                                    href="auth-login-cover.html"><i class="me-50" data-feather="power"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <ul class="main-search-list-defaultlist d-none">
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Files</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/xls.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;17kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd
                                Developer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;11kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;150kb</small>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                                           href="app-file-manager.html">
                    <div class="d-flex">
                        <div class="me-75"><img src="app-assets/images/icons/doc.png" alt="png" height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div><small class="search-data-size me-50 text-muted">&apos;256kb</small>
                </a></li>
            <li class="d-flex align-items-center"><a href="#">
                    <h6 class="section-label mt-75 mb-0">Members</h6>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-8.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-1.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-14.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing
                                Manager</small>
                        </div>
                    </div>
                </a></li>
            <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                                           href="app-user-view-account.html">
                    <div class="d-flex align-items-center">
                        <div class="avatar me-75"><img src="app-assets/images/portrait/small/avatar-s-6.jpg" alt="png"
                                                       height="32"></div>
                        <div class="search-data">
                            <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                        </div>
                    </div>
                </a></li>
        </ul>
        <ul class="main-search-list-defaultlist-other-list d-none">
            <li class="auto-suggestion justify-content-between"><a
                    class="d-flex align-items-center justify-content-between w-100 py-50">
                    <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No
                            results found.</span></div>
                </a></li>
        </ul>
        <!-- END: Header-->


        <!-- BEGIN: Main Menu-->
        <div class="horizontal-menu-wrapper">
            <div
                class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-dark navbar-shadow menu-border container-xxl"
                role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav">
                <div class="navbar-header" style="margin-bottom:2rem;">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item me-auto"><a class="navbar-brand"
                                                        href="html/ltr/horizontal-menu-template-dark/productListController">
                                <span>
                                    <img src="/app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                                </span>
                                <h2 class="brand-text mb-0" style="position: relative; right: 25px;">Hilfsmotor</h2>
                            </a></li>
                        <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                                    class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i></a></li>
                    </ul>
                </div>
                <div class="shadow-bottom"></div>
                <!-- Horizontal menu content-->
                <div class="navbar-container main-menu-content" data-menu="menu-container">
                    <!-- include includes/mixins-->
                    <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                        <li class="dropdown nav-item sidebar-group-active active open" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link d-flex align-items-center" href="productListController" data-bs-toggle="dropdown"><i
                                    data-feather="home"></i><span data-i18n="Dashboards">Dashboards</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="dashboard-analytics"
                                                    data-bs-toggle="" data-i18n="Analytics"><i data-feather="activity"></i><span
                                            data-i18n="Analytics">Analytics</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown nav-item" data-menu="dropdown"><a
                                class="dropdown-toggle nav-link lidget-dashboard-analytics d-flex align-items-center" href="#"
                                data-bs-toggle="dropdown"><i data-feather="package"></i><span data-i18n="Apps">Apps</span></a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Invoice"><i data-feather="file-text"></i><span data-i18n="Invoice">Orders</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-invoice-list.html"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="Roles &amp; Permission"><i data-feather="shield"></i><span
                                            data-i18n="Roles &amp; Permission">Roles &amp; Permission</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-access-roles.html"
                                                            data-bs-toggle="" data-i18n="Roles"><i data-feather="circle"></i><span
                                                    data-i18n="Roles">Roles</span></a>
                                        </li>
                                        <li class="active" data-menu=""><a class="dropdown-item d-flex align-items-center"
                                                                           href="app-access-permission.html" data-bs-toggle="" data-i18n="Permission"><i
                                                    data-feather="circle"></i><span data-i18n="Permission">Permission</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="eCommerce"><i data-feather='shopping-bag'></i><span
                                            data-i18n="eCommerce">Product</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-ecommerce-list.html"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                        class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        data-i18n="User"><i data-feather="user"></i><span data-i18n="User">User</span></a>
                                    <ul class="dropdown-menu" data-bs-popper="none">
                                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="app-user-list.html"
                                                            data-bs-toggle="" data-i18n="List"><i data-feather="circle"></i><span
                                                    data-i18n="List">List</span></a>
                                        </li>
                                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a
                                                class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                                data-i18n="View"><i data-feather="circle"></i><span data-i18n="View">View</span></a>
                                            <ul class="dropdown-menu" data-bs-popper="none">
                                                <li data-menu=""><a class="dropdown-item d-flex align-items-center"
                                                                    href="app-user-view-account.html" data-bs-toggle="" data-i18n="Account"><i
                                                            data-feather="circle"></i><span data-i18n="Account">Account</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="invoice-preview-wrapper">
                        <div class="row invoice-preview">
                            <!-- Invoice -->
                            <div class="col-xl-9 col-md-8 col-12">
                                <div class="card invoice-preview-card">
                                    <c:set var = "itemNum" scope = "session" value = "${1}"/>
                                    <c:forEach items="${sessionScope.orderItemList}"  var="orderItem">
                                        <hr class="invoice-spacing" />
                                        <div class="card-body invoice-padding pb-0">
                                            <!-- Header starts -->
                                            <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                                                <div>
                                                    <div class="logo-wrapper">
                                                        <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo position-relative"
                                                             style="left:-15px">
                                                        <h3 class="text-primary invoice-logo position-relative" style="right:35px;">${orderItem.storeName}</h3>
                                                    </div>
                                                    <p class="card-text mb-25">Product Name: <a href="productDetailController?mx=${orderItem.p.productID}">${orderItem.p.name}</a></p>
                                                    <p class="card-text mb-25">Quantity: ${orderItem.quantity}</p>
                                                    <p class="card-text mb-25">Mode: ${orderItem.p.mode}</p>
                                                    <c:choose>
                                                        <c:when test="${orderItem.p.mode == 'Rental'}">
                                                            <p class="card-text mb-25">Price: ${orderItem.p.price} per Day</p>
                                                        </c:when>
                                                        <c:when test="${orderItem.p.mode == 'Sell'}">
                                                            <p class="card-text mb-25">Price: ${orderItem.p.price}</p>
                                                        </c:when>
                                                    </c:choose>
                                                    <p class="card-text mb-25">Store Name: <a href="user-store?storeID=${orderItem.storeID}">${orderItem.storeName}</a></p>
                                                    <p class="card-text mb-25">${orderItem.address}</p>
                                                    <p class="card-text mb-0">${orderItem.phoneNum}</p>
                                                </div>
                                                <div class="mt-md-0 mt-2">
                                                    <h4 class="invoice-title">
                                                        Order
                                                        <span class="invoice-number">#${order.orderID}</span>
                                                        <br><br>
                                                        Order Item
                                                        <span class="invoice-number">#${itemNum}</span>
                                                    </h4>

                                                    <div class="invoice-date-wrapper">
                                                        <p class="invoice-date-title">Order Date:</p>
                                                        <p class="invoice-date">${orderItem.orderDate}</p>
                                                    </div>
                                                    <div class="invoice-date-wrapper">
                                                        <p class="invoice-date-title">Shipped Date:</p>
                                                        <p class="invoice-date">${orderItem.shippedDate}</p>
                                                    </div>
                                                    <div class="invoice-date-wrapper">
                                                        <p class="invoice-date-title">Status: </p>
                                                        <c:choose>
                                                            <c:when test="${orderItem.status == 1}">
                                                                <span class="badge rounded-pill badge-light-primary me-1">PROCESSING</span>
                                                            </c:when>
                                                            <c:when test="${orderItem.status == 2}">
                                                                <span class="badge rounded-pill badge-light-info me-1">SHIPPING</span>
                                                            </c:when>
                                                            <c:when test="${orderItem.status == 3}">
                                                                <span class="badge rounded-pill badge-light-warning me-1">STOP</span>                                                            
                                                            </c:when>
                                                            <c:when test="${orderItem.status == 4}">
                                                                <span class="badge rounded-pill badge-light-success me-1">Completed</span>                                                            </c:when>
                                                        </c:choose>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Header ends -->
                                        </div>

                                        <c:set var = "itemNum" scope = "session" value = "${itemNum + 1}"/>
                                    </c:forEach>

                                    <hr class="invoice-spacing" />

                                    <!-- Address and Contact starts -->
                                    <div class="card-body invoice-padding pt-0">
                                        <div class="row invoice-spacing">
                                            <div class="col-xl-8 p-0">
                                                <h6 class="mb-2">Order to:</h6>
                                                <h6 class="mb-25">${order.customerFirstName} ${order.customerLastName}</h6>
                                                <p class="card-text mb-25">${order.customerAddress}</p>
                                                <p class="card-text mb-25">${order.customerPhoneNum}</p>
                                                <p class="card-text mb-0">${order.customerEmail}</p>
                                            </div>
                                            <div class="col-xl-4 p-0 mt-xl-0 mt-2">
                                                <h6 class="mb-2">Payment Details:</h6>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td class="pe-1">Total Due:</td>
                                                            <td><span class="fw-bold">$${order.totalPrice}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pe-1">Bank name:</td>
                                                            <!--<td>American Bank</td>-->
                                                        </tr>
                                                        <tr>
                                                            <td class="pe-1">State:</td>
                                                           <!-- <td>United States</td>-->
                                                        </tr>
                                                        <tr>
                                                            <td class="pe-1">Account:</td>
                                                            <!--<td>ETD95476213874685</td>-->
                                                        </tr>
                                                        <tr>
                                                            <td class="pe-1">ZIP code:</td>
                                                            <!--<td>BR91905</td>-->
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Address and Contact ends -->

                                    <!-- Invoice Description starts -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="py-1">Order</th>
                                                    <th class="py-1">Price</th>
                                                    <th class="py-1">Quantity</th>
                                                    <th class="py-1">Mode</th>
                                                    <th class="py-1">Time</th>
                                                    <th class="py-1">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${sessionScope.orderItemList}"  var="orderItem">
                                                    <tr>
                                                        <td class="py-1">
                                                            <p class="card-text fw-bold mb-25">${orderItem.p.name}</p>
                                                            <p class="card-text text-nowrap">
                                                                ${orderItem.p.modelYear}
                                                            </p>
                                                        </td>
                                                        <td class="py-1">
                                                            <span class="fw-bold">$${orderItem.p.price}</span>
                                                        </td>
                                                        <td class="py-1">
                                                            <span class="fw-bold">${orderItem.quantity}</span>
                                                        </td>
                                                        <td class="py-1">
                                                            <span class="fw-bold">${orderItem.p.mode}</span>
                                                        </td>
                                                        <c:choose>
                                                            <c:when test="${orderItem.p.mode == 'Rental'}">
                                                                <td class="py-1">
                                                                    <span class="fw-bold">${orderItem.rentalDateNum} Day</span>
                                                                </td>
                                                            </c:when>
                                                                <c:when test="${orderItem.p.mode == 'Sell'}">
                                                                <td class="py-1">
                                                                    <span class="fw-bold">UNLIMITED</span>
                                                                </td>
                                                            </c:when>
                                                        </c:choose>

                                                        <td class="py-1">
                                                            <span class="fw-bold">$${orderItem.price}</span>
                                                        </td>
                                                    </tr>

                                                </c:forEach>


                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="card-body invoice-padding pb-0">
                                        <div class="row invoice-sales-total-wrapper">
                                            <div class="col-md-6 order-md-1 order-2 mt-md-0 mt-3">
                                                <p class="card-text mb-0">
                                                    <span class="fw-bold">Salesperson:</span> <span class="ms-75">Hilfsmotor</span>
                                                </p>
                                            </div>
                                            <div class="col-md-6 d-flex justify-content-end order-md-2 order-1">
                                                <div class="invoice-total-wrapper">
                                                    <div class="invoice-total-item">
                                                        <p class="invoice-total-title">Subtotal:</p>
                                                        <p class="invoice-total-amount">$${order.totalPrice}</p>
                                                    </div>
                                                    <div class="invoice-total-item">
                                                        <p class="invoice-total-title">Discount:</p>
                                                        <p class="invoice-total-amount">${order.discount}%</p>
                                                    </div>
                                                    <div class="invoice-total-item">
                                                        <p class="invoice-total-title">Tax:</p>
                                                        <p class="invoice-total-amount">${order.taxes}%</p>
                                                    </div>
                                                    <hr class="my-50" />
                                                    <div class="invoice-total-item">
                                                        <p class="invoice-total-title">Total:</p>
                                                        <p class="invoice-total-amount">$${order.totalPrice*(100 - order.discount)/100*(100 + order.taxes)/100}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Invoice Description ends -->

                                    <hr class="invoice-spacing" />

                                    <!-- Invoice Note starts -->
                                    <div class="card-body invoice-padding pt-0">
                                        <div class="row">
                                            <div class="col-12">
                                                <span class="fw-bold">Note:</span>
                                                <span>It was a pleasure working with you and your team. We hope you will keep us in mind for future freelance projects. Thank You!</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Invoice Note ends -->
                                </div>
                            </div>
                            <!-- /Invoice -->

                            <!-- Invoice Actions -->
                            <div class="col-xl-3 col-md-4 col-12 invoice-actions mt-md-0 mt-2">
                                <div class="card">
                                    <div class="card-body">
                                        <a class="btn btn-outline-secondary w-100 mb-75" href="./editOrderController?oID=${order.orderID}"> Edit </a>
                                        <a class="btn btn-outline-secondary w-100 mb-75" href="print-order.jsp"> Print </a>
                                        <a href="orderController">
                                            <button onclick="location.href='orderController';" class="btn btn-primary w-100 mb-75" data-bs-toggle="modal"
                                                    data-bs-target="#send-invoice-sidebar">
                                                List Orders
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- /Invoice Actions -->
                        </div>
                    </section>

                    <!-- Send Invoice Sidebar -->
                    <div class="modal modal-slide-in fade" id="send-invoice-sidebar" aria-hidden="true">
                        <div class="modal-dialog sidebar-lg">
                            <div class="modal-content p-0">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                                <div class="modal-header mb-1">
                                    <h5 class="modal-title">
                                        <span class="align-middle">Send Invoice</span>
                                    </h5>
                                </div>
                                <div class="modal-body flex-grow-1">
                                    <form>
                                        <div class="mb-1">
                                            <label for="invoice-from" class="form-label">From</label>
                                            <input type="text" class="form-control" id="invoice-from" value="shelbyComapny@email.com"
                                                   placeholder="company@email.com" />
                                        </div>
                                        <div class="mb-1">
                                            <label for="invoice-to" class="form-label">To</label>
                                            <input type="text" class="form-control" id="invoice-to" value="qConsolidated@email.com"
                                                   placeholder="company@email.com" />
                                        </div>
                                        <div class="mb-1">
                                            <label for="invoice-subject" class="form-label">Subject</label>
                                            <input type="text" class="form-control" id="invoice-subject"
                                                   value="Invoice of purchased Admin Templates" placeholder="Invoice regarding goods" />
                                        </div>
                                        <div class="mb-1">
                                            <label for="invoice-message" class="form-label">Message</label>
                                            <textarea class="form-control" name="invoice-message" id="invoice-message" cols="3" rows="11"
                                                      placeholder="Message...">
                                                Dear Queen Consolidated,

                                                Thank you for your business, always a pleasure to work with you!

                                                We have generated a new invoice in the amount of $95.59

                                                We would appreciate payment of this invoice by 05/11/2019</textarea>
                                        </div>
                                        <div class="mb-1">
                                            <span class="badge badge-light-primary">
                                                <i data-feather="link" class="me-25"></i>
                                                <span class="align-middle">Invoice Attached</span>
                                            </span>
                                        </div>
                                        <div class="mb-1 d-flex flex-wrap mt-2">
                                            <button type="button" class="btn btn-primary me-1" data-bs-dismiss="modal">Send</button>
                                            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Send Invoice Sidebar -->

                    <!-- Add Payment Sidebar -->
                    <div class="modal modal-slide-in fade" id="add-payment-sidebar" aria-hidden="true">
                        <div class="modal-dialog sidebar-lg">
                            <div class="modal-content p-0">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                                <div class="modal-header mb-1">
                                    <h5 class="modal-title">
                                        <span class="align-middle">Add Payment</span>
                                    </h5>
                                </div>
                                <div class="modal-body flex-grow-1">
                                    <form>
                                        <div class="mb-1">
                                            <input id="balance" class="form-control" type="text" value="Invoice Balance: 5000.00" disabled />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="amount">Payment Amount</label>
                                            <input id="amount" class="form-control" type="number" placeholder="$1000" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="payment-date">Payment Date</label>
                                            <input id="payment-date" class="form-control date-picker" type="text" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="payment-method">Payment Method</label>
                                            <select class="form-select" id="payment-method">
                                                <option value="" selected disabled>Select payment method</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Bank Transfer">Bank Transfer</option>
                                                <option value="Debit">Debit</option>
                                                <option value="Credit">Credit</option>
                                                <option value="Paypal">Paypal</option>
                                            </select>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="payment-note">Internal Payment Note</label>
                                            <textarea class="form-control" id="payment-note" rows="5"
                                                      placeholder="Internal Payment Note"></textarea>
                                        </div>
                                        <div class="d-flex flex-wrap mb-0">
                                            <button type="button" class="btn btn-primary me-1" data-bs-dismiss="modal">Send</button>
                                            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Add Payment Sidebar -->

                </div>
            </div>
        </div>
        <!-- END: Content-->


        <!-- BEGIN: Customizer-->
        <div class="customizer d-none d-md-block"><a
                class="customizer-toggle d-flex align-items-center justify-content-center" href="#"><i class="spinner"
                                                                                                   data-feather="settings"></i></a>
            <div class="customizer-content">
                <!-- Customizer header -->
                <div class="customizer-header px-2 pt-1 pb-0 position-relative">
                    <h4 class="mb-0">Theme Customizer</h4>
                    <p class="m-0">Customize & Preview in Real Time</p>

                    <a class="customizer-close" href="#"><i data-feather="x"></i></a>
                </div>

                <hr />

                <!-- Styling & Text Direction -->
                <div class="customizer-styling-direction px-2">
                    <p class="fw-bold">Skin</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="skinlight" name="skinradio" class="form-check-input layout-name" checked
                                   data-layout="" />
                            <label class="form-check-label" for="skinlight">Light</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skinbordered" name="skinradio" class="form-check-input layout-name"
                                   data-layout="bordered-layout" />
                            <label class="form-check-label" for="skinbordered">Bordered</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="skindark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="dark-layout" />
                            <label class="form-check-label" for="skindark">Dark</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="skinsemidark" name="skinradio" class="form-check-input layout-name"
                                   data-layout="semi-dark-layout" />
                            <label class="form-check-label" for="skinsemidark">Semi Dark</label>
                        </div>
                    </div>
                </div>

                <hr />

                <!-- Menu -->
                <div class="customizer-menu px-2">
                    <div id="customizer-menu-collapsible" class="d-flex">
                        <p class="fw-bold me-auto m-0">Menu Collapsed</p>
                        <div class="form-check form-check-primary form-switch">
                            <input type="checkbox" class="form-check-input" id="collapse-sidebar-switch" />
                            <label class="form-check-label" for="collapse-sidebar-switch"></label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Layout Width -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Layout Width</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-full" name="layoutWidth" class="form-check-input" checked />
                            <label class="form-check-label" for="layout-width-full">Full Width</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="layout-width-boxed" name="layoutWidth" class="form-check-input" />
                            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Navbar -->
                <div class="customizer-navbar px-2">
                    <div id="customizer-navbar-colors">
                        <p class="fw-bold">Navbar Color</p>
                        <ul class="list-inline unstyled-list">
                            <li class="color-box bg-white border selected" data-navbar-default=""></li>
                            <li class="color-box bg-primary" data-navbar-color="bg-primary"></li>
                            <li class="color-box bg-secondary" data-navbar-color="bg-secondary"></li>
                            <li class="color-box bg-success" data-navbar-color="bg-success"></li>
                            <li class="color-box bg-danger" data-navbar-color="bg-danger"></li>
                            <li class="color-box bg-info" data-navbar-color="bg-info"></li>
                            <li class="color-box bg-warning" data-navbar-color="bg-warning"></li>
                            <li class="color-box bg-dark" data-navbar-color="bg-dark"></li>
                        </ul>
                    </div>

                    <p class="navbar-type-text fw-bold">Navbar Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-floating" name="navType" class="form-check-input" checked />
                            <label class="form-check-label" for="nav-type-floating">Floating</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-sticky" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="nav-type-static" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-static">Static</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" id="nav-type-hidden" name="navType" class="form-check-input" />
                            <label class="form-check-label" for="nav-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
                <hr />

                <!-- Footer -->
                <div class="customizer-footer px-2">
                    <p class="fw-bold">Footer Type</p>
                    <div class="d-flex">
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-sticky" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-sticky">Sticky</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-static" name="footerType" class="form-check-input" checked />
                            <label class="form-check-label" for="footer-type-static">Static</label>
                        </div>
                        <div class="form-check me-1">
                            <input type="radio" id="footer-type-hidden" name="footerType" class="form-check-input" />
                            <label class="form-check-label" for="footer-type-hidden">Hidden</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End: Customizer-->


    </div>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>




    <!-- BEGIN: Vendor JS-->
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
    <script src="app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.min.js"></script>
    <script src="app-assets/js/core/app.min.js"></script>
    <script src="app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="app-assets/js/scripts/pages/app-invoice.min.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function () {
            if (feather) {
                feather.replace({width: 14, height: 14});
            }
        })
    </script>
</body>
<!-- END: Body-->

</html>