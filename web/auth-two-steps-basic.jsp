<%-- 
    Document   : auth-two-steps-basic
    Created on : Sep 21, 2022, 2:50:14 PM
    Author     : ADMIN
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title>Hilfsmotor</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/hilf.png">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/authentication.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/tempcss.css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->
    <body class="horizontal-layout horizontal-menu blank-page navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="blank-page">
        <!-- BEGIN: Content-->
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body"><div class="auth-wrapper auth-basic px-2">
                        <div class="auth-inner my-2">
                            <!-- two steps verification basic-->
                            <div class="card mb-0">
                                <div class="card-body">
                                    <a class="navbar-brand" href="auth-login-cover.jsp" style="display: flex;align-items: center;margin-bottom: 2rem;justify-content: center;">
                                        <img src="app-assets/images/ico/hilf.png" alt="" class="lidget-login__logo">
                                        <h2 class="brand-text" style="margin-bottom: 0;position: relative;right: 0.75rem;">Hilfsmotor</h2>
                                    </a>
                                    <h2 class="card-title fw-bolder mb-1">Two Step Verification 💬</h2>
                                    <p class="card-text mb-75">
                                        We sent a verification code to your mail. Enter the code you received from email.
                                    </p>
                                    <p class="card-text fw-bolder mb-2 data-email" id="emailPinCode">${staff.getEmail()}</p>

                                    <form class="mt-2" id="two-step-form">
                                        <h6>Type your 6 digit security pin code</h6>
                                        <div class="auth-input-wrapper d-flex align-items-center justify-content-between">
                                            <input
                                                type="text"
                                                class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1"
                                                maxlength="1"
                                                autofocus=""
                                                />

                                            <input
                                                type="text"
                                                class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1"
                                                maxlength="1"
                                                />

                                            <input
                                                type="text"
                                                class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1"
                                                maxlength="1"
                                                />

                                            <input
                                                type="text"
                                                class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1"
                                                maxlength="1"
                                                />

                                            <input
                                                type="text"
                                                class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1"
                                                maxlength="1"
                                                />

                                            <input
                                                type="text"
                                                class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1"
                                                maxlength="1"
                                                />
                                        </div>
                                        <button type="submit" class="btn btn-primary w-100" tabindex="4">Sign in</button>
                                    </form>
                                    <div class="auth-login-switch mt-1">
                                        <a href="auth-login-cover.jsp">
                                            <button class="btn btn-outline-secondary btn-prev waves-effect">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left align-middle me-sm-25 me-0"><polyline points="15 18 9 12 15 6"></polyline></svg>
                                                <span class="align-middle d-sm-inline-block d-none">Log in</span>
                                            </button>
                                        </a>
                                    </div>
                                    <p class="text-center mt-2">
                                        <span>Didn’t get the code?</span><a><span class="lidget-resend-code" style="color: #7367F0;">&nbsp;Resend</span></a>
                                        <span>or</span>
                                        <a href="Javascript:void(0)"><span>&nbsp;Call Us</span></a>
                                    </p>
                                </div>
                            </div>
                            <!-- /two steps verification basic -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content-->


        <!-- BEGIN: Vendor JS-->
        <script src="app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
        <script src="app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="app-assets/js/core/app-menu.min.js"></script>
        <script src="app-assets/js/core/app.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="app-assets/js/scripts/pages/auth-two-steps.min.js"></script>
        <!-- END: Page JS-->

        <!-- BEGIN: My JS-->
        <script src="assets/js/admin.js"></script>
        <!-- END: My JS-->
        <script>
            $(window).on('load', function () {
                if (feather) {
                    feather.replace({width: 14, height: 14});
                }
            });
        </script>
    </body>
    <!-- END: Body-->
</html>