const originalHtml = $("#lidget__list-permission").html();
/*=====================WIDGET-USER=====================*/
const widgetUser = {
    showEntry: function () {
        $("#showEntry").on("change", function () {
            let showEntry = $('#showEntry option:selected').val();
            $.ajax({
                url: "/Rentabike/permissionListController",
                type: "post",
                data: {
                    mode: "showEntry",
                    showEntry: showEntry,
                    page: "1"
                },
                success: function (response) {
                    window.location.replace("http://localhost:8088/Rentabike/permissionListController?page=1");
                }
            });
        });
    },
    searchPermission: function () {
        $("#searchPermission").on("keyup", function () {
            if (this.value.length === 0 || this.value === "") {
                $("#paginationNumber").css("opacity", "1");
                $("#paginationNumber").css("visibility", "visible");
                $("#totalInfo").css("opacity", "1");
                $("#totalInfo").css("visibility", "visible");
            } else {
                $("#paginationNumber").css("opacity", "0");
                $("#paginationNumber").css("visibility", "hidden");
                $("#totalInfo").css("opacity", "0");
                $("#totalInfo").css("visibility", "hidden");
            }
            $.ajax({
                url: "/Rentabike/permissionListController",
                type: "post",
                data: {
                    mode: "searchName",
                    inputSearch: this.value
                },
                success: function (response) {
                    if (response !== "RESET") {
                        $("#lidget__list-permission").html(response);
                    } else {
                        $("#lidget__list-permission").html("");
                        $("#lidget__list-permission").html(originalHtml);
                    }
                }
            });
        });
    },
    selectRole: function () {
        $("#UserRole").on("change", function () {
            let showValue = $('#UserRole option:selected').val();
            if (showValue.length === 0 || showValue === "") {
                $("#paginationNumber").css("opacity", "1");
                $("#paginationNumber").css("visibility", "visible");
                $("#totalInfo").css("opacity", "1");
                $("#totalInfo").css("visibility", "visible");
            } else {
                $("#paginationNumber").css("opacity", "0");
                $("#paginationNumber").css("visibility", "hidden");
                $("#totalInfo").css("opacity", "0");
                $("#totalInfo").css("visibility", "hidden");
            }
            $.ajax({
                url: "/Rentabike/permissionListController",
                type: "post",
                data: {
                    mode: "selectRole",
                    selectOption: showValue
                },
                success: function (response) {
                    if (response !== "RESET") {
                        $("#lidget__list-permission").html(response);
                    } else {
                        $("#lidget__list-permission").html("");
                        $("#lidget__list-permission").html(originalHtml);
                    }
                }
            });
        });
    },
    handleSorting: function () {
        let ds = document.querySelectorAll(".ds-s");
        for (var item of ds) {
            item.onclick = function () {
                let sortContent = this.getAttribute("id");
                switching = true;
                if (this.classList.contains("non-sorting")) {
                    this.classList.remove("sorting_desc", "non-sorting");
                    this.classList.add("sorting_asc");
                    switchTableAsc(sortContent);
                } else if (this.classList.contains("sorting_asc")) {
                    this.classList.remove("sorting_asc", "non-sorting");
                    this.classList.add("sorting_desc");
                    switchTableDesc(sortContent);
                } else {
                    this.classList.remove("sorting_asc", "sorting_desc");
                    this.classList.add("non-sorting");
                    $("#lidget__list-permission").html(originalHtml);
                }
            }
        }
    },
    handlePermissionRoleSelected: function () {
        let drsr = document.querySelectorAll(".dr-sr");
        for (let userRole of Array.from(drsr)) {
            userRole.onchange = function () {
                let urs = document.querySelectorAll(".urs-s");
                for (let i = 0; i < drsr.length; i++) {
                    for (let item of Array.from(urs)) {
                        if (this.value === item.value) {
                            $('#UserRole' + (i + 1) + ' option[value="' + this.value + '"]').attr("disabled", "disabled");
                        } else {
                            $('#UserRole' + (i + 1) + ' option[value="' + item.value + '"]').attr("disabled", false);
                        }
                    }
                }
                for (let i = 0; i < drsr.length; i++) {
                    let value = $('#UserRole' + (i + 1)).find(":selected").val();
                    if (value !== "") {
                        for (let i = 0; i < drsr.length; i++) {
                            $('#UserRole' + (i + 1) + ' option[value="' + value + '"]').attr("disabled", "disabled");
                        }
                    }
                }
            }
        }
    },
    handlePermissionRoleEdit: function () {
        let drsr = document.querySelectorAll(".ds-sr");
        for (let userRole of Array.from(drsr)) {
            userRole.onchange = function () {
                let urs = document.querySelectorAll(".urs-sk");
                for (let i = 0; i < drsr.length; i++) {
                    for (let item of Array.from(urs)) {
                        if (this.value === item.value) {
                            $('#UserRoleS' + (i + 1) + ' option[value="' + this.value + '"]').attr("disabled", "disabled");
                        } else {
                            $('#UserRoleS' + (i + 1) + ' option[value="' + item.value + '"]').attr("disabled", false);
                        }
                    }
                }
                for (let i = 0; i < drsr.length; i++) {
                    let value = $('#UserRoleS' + (i + 1)).find(":selected").val();
                    if (value !== "") {
                        for (let i = 0; i < drsr.length; i++) {
                            $('#UserRoleS' + (i + 1) + ' option[value="' + value + '"]').attr("disabled", "disabled");
                        }
                    }
                }
            }
        }
    },
    viewPermissionUpdate: function (index, param) {
        $.ajax({
            url: "/Rentabike/permissionListController",
            type: "post",
            data: {
                mode: "viewPermission",
                idPermission: index
            },
            success: function (response) {
                if (response !== "FAILED") {
                    let namePermission, userRole;
                    if (param === "Update") {
                        namePermission = "#editPermissionName";
                        userRole = "UserRoleS";
                    } else {
                        namePermission = "#deletePermissionName";
                        userRole = "UserRoleSK";
                    }
                    let splitItem = response.split("LGN");
                    let itemAssignTo = splitItem[1].split(", ");
                    let indexCurrent = 1;
                    $(namePermission).val(splitItem[0]);
                    $(namePermission).attr("currentData", splitItem[0]);

                    for (let itemAssign of itemAssignTo) {
                        if (itemAssign !== "undefined") {
                            selectElement(userRole + (indexCurrent), itemAssign);
                            indexCurrent++;
                        }
                    }
                }
            }
        });
    },
    handlePermissionSettings: function () {
        $("#editPermissionForm").on("submit", function (event) {
            event.preventDefault();
            let validateStatus = false;
            let drsr = document.querySelectorAll(".ds-sr");
            let selectRoleCombine = "";
            let modalPermissionName = $("#editPermissionName").val();
            for (let i = 0; i < drsr.length; i++) {
                let value = $('#UserRoleS' + (i + 1)).find(":selected").val();
                if (value != "") {
                    selectRoleCombine += value + ", ";
                }
            }
            selectRoleCombine = selectRoleCombine.substring(0, selectRoleCombine.length - 2);
            if (modalPermissionName !== "") {
                if (selectRoleCombine === "") {
                    toastr.error("You need a set atleast one role for this permission.", "Role assign for permission.", {
                        closeButton: !0,
                        tapToDismiss: !1,
                        rtl: !1
                    });
                } else {
                    validateStatus = true;
                }
            }
            if (validateStatus) {
                $.ajax({
                    url: "/Rentabike/permissionListController",
                    type: "post",
                    data: {
                        mode: "handleUserStatus",
                        contextSwitch: "ADD",
                        name: modalPermissionName,
                        assignTo: selectRoleCombine,
                        idPermission: $("#editPermissionName").attr("data-attr"),
                        oldPermission: $("#editPermissionName").attr("currentData")
                    },
                    success: function (response) {
                        if (response === "SUCCESS") {
                            toastr.success("The system will be reset automatically in 5 seconds to load new data", "Success update permission", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                            $("#updatePermissionClose").click();
                            setTimeout(() => {
                                window.location.replace("http://localhost:8088/Rentabike/permissionListController?page=1");
                            }, 5000);
                        } else if (response === "FAILED") {
                            toastr.error("There are some error expected. Refresh your page and try again.", "Failed to update permission.", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                        } else {
                            toastr.error("There are some error expected. Refresh your page and try again.", "Error to update permission.", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                        }
                    }
                });
            }
        })
    },
    deletePermission: function () {
        $("#deletePermissionForm").on("submit", function (event) {
            event.preventDefault();

            $.ajax({
                url: "/Rentabike/permissionListController",
                type: "post",
                data: {
                    mode: "handleUserStatus",
                    contextSwitch: "DELETE",
                    idPermission: $("#deletePermissionName").attr("data-attr")
                },
                success: function (response) {
                    if (response === "SUCCESS") {
                        toastr.success("The system will be reset automatically in 5 seconds to load new data", "Success delete permission", {
                            closeButton: !0,
                            tapToDismiss: !1,
                            rtl: !1
                        });
                        $("#deletePermissionClose").click();
                        setTimeout(() => {
                            window.location.replace("http://localhost:8088/Rentabike/permissionListController?page=1");
                        }, 5000);
                    } else if (response === "FAILED") {
                        toastr.error("There are some error expected. Refresh your page and try again.", "Failed to delete permission.", {
                            closeButton: !0,
                            tapToDismiss: !1,
                            rtl: !1
                        });
                    }
                }
            });
        })
    },
    addNewPermission: function () {
        $("#addPermissionForm").on("submit", function (event) {
            let validateStatus = false;
            let drsr = document.querySelectorAll(".dr-sr");
            let selectRoleCombine = "";
            let modalPermissionName = $("#modalPermissionName").val();
            for (let i = 0; i < drsr.length; i++) {
                let value = $('#UserRole' + (i + 1)).find(":selected").val();
                if (value != "") {
                    selectRoleCombine += value + ", ";
                }
            }
            selectRoleCombine = selectRoleCombine.substring(0, selectRoleCombine.length - 2);
            if (modalPermissionName !== "") {
                if (selectRoleCombine === "") {
                    toastr.error("You need a set atleast one role for this permission.", "Role assign for permission.", {
                        closeButton: !0,
                        tapToDismiss: !1,
                        rtl: !1
                    });
                } else {
                    validateStatus = true;
                }
            }

            if (validateStatus) {
                $.ajax({
                    url: "/Rentabike/permissionListController",
                    type: "post",
                    data: {
                        mode: "addNewPermission",
                        name: modalPermissionName,
                        assignTo: selectRoleCombine
                    },
                    success: function (response) {
                        if (response === "SUCCESS") {
                            toastr.success("The system will be reset automatically in 5 seconds to load new data", "Success add new permission", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                            $("#addNewPermissionClose").click();
                            setTimeout(() => {
                                window.location.replace("http://localhost:8088/Rentabike/permissionListController?page=1");
                            }, 5000);
                        } else if (response === "FAILED") {
                            toastr.error("There are some error expected. Refresh your page and try again.", "Failed to add new permission.", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                        } else {
                            toastr.error("There are some error expected. Refresh your page and try again.", "Error to add new permission.", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                        }
                    }
                });
            }
        })
    },
    start: function () {
        this.handleSorting();
        this.showEntry();
        this.selectRole();
        this.searchPermission();
        this.handlePermissionRoleSelected();
        this.addNewPermission();
        this.handlePermissionRoleEdit();
        this.handlePermissionSettings();
        this.deletePermission();
    }
};
widgetUser.start();

function sendParamterHandler(param) {
    let indexUser = param.getAttribute("index");
    let title = param.getAttribute("data-title");
    if (title === "suspended-account") {
        widgetUser.showDetailsModal(indexUser, title);
        $('#suspendedUser').modal("show");
    } else if (title === "edit-role-account") {
        console.log(indexUser);
    } else if (title === "unlock-account") {
        widgetUser.showDetailsModal(indexUser, title);
        $('#unlockUser').modal("show");
    }
}

function sendParamterView(param) {
    let indexUser = param.getAttribute("index");
    widgetUser.handleViewProfile(indexUser);
}

function sendParamterPermission(param) {
    let indexPermission = param.getAttribute("index");
    let userStatus = document.querySelector(".user-status").innerHTML;
    console.log(userStatus);
    if (param.getAttribute("id") === "UpdatePermission") {
        if (userStatus !== "ADMIN") {
            $("#danger_modal").modal("show");
        } else {
            $("#editPermissionName").attr("data-attr", indexPermission);
            widgetUser.viewPermissionUpdate(indexPermission, "Update");
            $("#editPermissionModal").modal("show");
        }
    } else {
        if (userStatus !== "ADMIN") {
            $("#danger_modal").modal("show");
        } else {
            $("#deletePermissionName").attr("data-attr", indexPermission);
            widgetUser.viewPermissionUpdate(indexPermission, "Delete");
            $("#deletePermissionModal").modal("show");
        }
    }
}

function sendParamterNew() {
    let userStatus = document.querySelector(".user-status").innerHTML;
    if (userStatus !== "ADMIN") {
        $("#danger_modal").modal("show");
    } else {
        $("#addPermissionModal").modal("show");
    }
}

function switchTableAsc(sortContent) {
    let table, rows, switching, i, x, y, shouldSwitch;
    table = document.querySelector("#tablePermissionList");
    switching = true;
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            if (sortContent === "sorting-name") {
                x = rows[i].getElementsByTagName("TD")[1];
                y = rows[i + 1].getElementsByTagName("TD")[1];
            } else if (sortContent === "sorting-date") {
                x = rows[i].getElementsByTagName("TD")[3];
                y = rows[i + 1].getElementsByTagName("TD")[3];
            }
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

function switchTableDesc(sortContent) {
    let table, rows, switching, i, x, y, shouldSwitch;
    table = document.querySelector("#tablePermissionList");
    switching = true;
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            if (sortContent === "sorting-name") {
                x = rows[i].getElementsByTagName("TD")[1];
                y = rows[i + 1].getElementsByTagName("TD")[1];
            } else if (sortContent === "sorting-date") {
                x = rows[i].getElementsByTagName("TD")[3];
                y = rows[i + 1].getElementsByTagName("TD")[3];
            }
            if (y.innerHTML.toLowerCase() > x.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

function selectElement(id, valueToSelect) {
    let element = document.getElementById(id);
    element.value = valueToSelect;
}