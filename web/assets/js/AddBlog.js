let _$ = (selector) => document.querySelector(selector);
let toastElement = _$('.toast');
let toast = new bootstrap.Toast(toastElement)
let toastBody = _$('.toast-body');
let errorMess='';
//handle event toast
toastElement.addEventListener('show.bs.toast', () => {
    toastBody.innerHTML = errorMess
})
let tagsArr = [];
let image=null;
const handleImage = ()=>{
    let changeImage = _$('button.change-image');
    let imageDetailWrapper = _$('.image-detail__wrappper');
    let addImage = _$('#add-image');
    let delImage = _$('.delete-image__btn');

    changeImage.addEventListener('click', (e)=> {
        imageDetailWrapper.classList.toggle('d-none')
    })
    window.addEventListener('click', (e)=> {
        if(e.target.closest('.image-detail__wrappper')==null && !e.target.classList.contains('change-image')){
            imageDetailWrapper.classList.add('d-none')
        }
    })
    addImage.addEventListener('change', (e)=>{
        const [file] = addImage.files //dua bien file nay ra de submit
        let imageCheck = new Image();
        imageCheck.onload = function() {
            if (file) {
                if(_$('.image-detail__view span')) _$('.image-detail__view span').remove();
                _$('.image-detail__view').style.backgroundImage = `url(${URL.createObjectURL(file)})`
            }
            let reader = new FileReader();
            reader.onloadend = function() {
                image = reader.result;
            }
            reader.readAsDataURL(file);
        };
        imageCheck.onerror = function() {
            errorMess = 'Invalid image!'
            toast.show();
        };
        imageCheck.src = URL.createObjectURL(file);
    })
    delImage.addEventListener('click', (e) => {
        //xoa bien file
        image = null;

        if(_$('.image-detail__view span')==null){
            _$('.image-detail__view').innerHTML = `<span>Your image here</span>`;
        }
        _$('.image-detail__view').style.backgroundImage = `none`;
    })
}
const handleDeleteTag = () => {
    let tagAttachWrapper = _$('.tag-attach__wrapper');
    let tagDelAttachs = tagAttachWrapper.querySelectorAll('.tag-attach i')
    if(tagDelAttachs==null) return;
    for(let tagDelAttach of tagDelAttachs){
        tagDelAttach.addEventListener('click', ()=>{
            tagsArr = tagsArr.filter(e => e.tagTitle!==tagDelAttach.closest('.tag-attach').querySelector('span').innerHTML)
            tagDelAttach.closest('.tag-attach').remove();
        })
    }
}
const handleTags = ()=> {
    let tagAttachWrapper = _$('.tag-attach__wrapper');
    let tagInput = _$('.news-tag input');
    let loadingInput = _$('.search-result__loading');
    let tagResults = _$('.search-result__list');
    let checkShowResult = true;
    let timeoutId;
    let checkContinueInput = false

    tagInput.addEventListener('input', (e) => {
        if(e.target.value === '') {
            if(timeoutId)
                clearTimeout(timeoutId);
            loadingInput.classList.add('d-none');
            tagResults.classList.add('d-none');
            checkShowResult = false;
            return;
        }
        checkShowResult = true;
        loadingInput.classList.add('d-none');
        tagResults.classList.add('d-none');
        checkContinueInput = true;
        if(timeoutId)
            clearTimeout(timeoutId);

        //delay when user keyup
        timeoutId = setTimeout( async() => {
            loadingInput.classList.remove('d-none');
            checkContinueInput = false;

            let tagDataResult = await postSearchString(e.target.value);
            // console.log("check tag data result: ", tagDataResult);
            if(checkContinueInput || !checkShowResult){
                return;
            }
            let tagList = JSON.parse(tagDataResult).TagList
            let tagResult
            if(tagList.length == 0){
                tagResult = `<p class="no-tag-found">No tag found!</p>`
            }
            else {
                tagResult = tagList.map((cur, index) => {
                    return `<div class="search-result__item" data-id="${cur.id}"
                    data-value="${cur.tagTitle}" 
                >
                                <p>${cur.tagTitle}</p></p>
                            </div>`
                }).join('')
            }
            tagResults.innerHTML = tagResult;

            loadingInput.classList.add('d-none');
            tagResults.classList.remove('d-none');

            //handle user click on tag results
            let tagResultItems = tagResults.querySelectorAll('.search-result__item')
            for(let tagResultItem of tagResultItems) {
                tagResultItem.addEventListener('click', () => {
                    if(!tagsArr.some(cur => cur.id && cur.id === +tagResultItem.dataset.id) && tagsArr.length<5){
                        tagsArr = [...tagsArr, {id: +tagResultItem.dataset.id, tagTitle: tagResultItem.dataset.value}]
                        
                        tagAttachWrapper.innerHTML += `<div class="tag-attach">
                        <span>${tagResultItem.dataset.value}</span>
                        <i data-id="${tagResultItem.dataset.id}" class="fa-solid fa-xmark"></i>
                        </div>`
                    }

                    tagInput.value = ''
                    loadingInput.classList.add('d-none');
                    tagResults.classList.add('d-none');
                    
                    //handle when user want to delete tag
                    handleDeleteTag()
                });
            }
        },500);
    })
    tagInput.addEventListener('keydown', (e) => {
        if(e.key=='Enter'){
            if(!tagsArr.some(cur => cur.tagTitle && cur.tagTitle === tagInput.value) && tagsArr.length<5){
                tagsArr = [...tagsArr, {tagTitle: tagInput.value}]
                
                tagAttachWrapper.innerHTML += `<div class="tag-attach">
                <span>${tagInput.value}</span>
                <i class="fa-solid fa-xmark"></i>
                </div>`
            }

            if(timeoutId)
                clearTimeout(timeoutId);
            tagInput.value = ''
            loadingInput.classList.add('d-none');
            tagResults.classList.add('d-none');
            checkShowResult = false;
            
            //handle when user want to delete tag
            handleDeleteTag()
        }
    })
}
function AddNews(){
    let editorData;
    let editorPlacholder = _$('.editor-placeholder');
    let previewEditor = _$('.preview-editor');
    let postBtn = _$('.add-news__btn');
    let title = _$('.news-title input');
    
    ClassicEditor
        .create( _$( '#editor' ))
        .then( editor => {
            editor.model.document.on('change:data', (evt, data) => {
                editorData = editor.getData();
                if(editorData!==''){
                    editorPlacholder.classList.add('d-none')
                }else {
                    editorPlacholder.classList.remove('d-none')
                }

                previewEditor.innerHTML = editorData;
            });
        } )
        .catch( error => {
            console.error( error );
        } );
        
    window.addEventListener('keydown', (e) => {
        if(e.ctrlKey && e.key == 'p'){
            e.preventDefault();
            previewEditor.classList.toggle('d-none');
        }
    })

    //tag
    handleTags()

    //image
    handleImage()

    //handle submitAnswer
    postBtn.addEventListener('click', (e) => {
        if(title.value.trim() === ''){
            errorMess = 'Please input a title!'
            toast.show();
            return;
        }
        if(tagsArr.length <1){
            errorMess = 'At least one tag is required!'
            toast.show();
            return;
        }
        if(image==null){
            errorMess = 'Image is required!'
            toast.show();
            return;
        }
        if(editorData === undefined || editorData === ''){
            errorMess = 'News content is required!'
            toast.show();
            return;
        }
        let descriptionArr = _$('.preview-editor').textContent.split(' ')
        let description = '';
        if (descriptionArr.length < 20) description = descriptionArr.join(' ');
        else {
            for(let i=0; i<20; i++) description += ' ' +descriptionArr[i];
        }
        let dataSend = {
            type: 'add',
            title: title.value,
            tagsArr: JSON.stringify(tagsArr),
            content: editorData,
            image,
            description
        };
        $.ajax({
            url: `http://localhost:8088/Rentabike/manage-blog`,
            type: 'post', 
            data: dataSend,
            success: function (data) {
                let dataRs = JSON.parse(data)
                errorMess = dataRs.ME
                toast.show()
                setTimeout(()=>{
                    if(dataRs.Role==='ADMIN'){
                        document.location.href = '/Rentabike/blog-list';
                        return;
                    }
                    document.location.href = 'http://localhost:8088/Rentabike/blog-draft';
                }, 1000)
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
        console.log(dataSend);
    })
}

AddNews()

//fake return result
const postSearchString = (searchString)=>{
    let dataSend = {
        type: 'search-tag',
        searchString
    }
    return $.ajax({
        url: `http://localhost:8088/Rentabike/manage-blog`,
        type: 'post', 
        data: dataSend,
    })
}
function getFakeListTags(){
    return new Promise(resolve => setTimeout(()=>{resolve(
        [
            {id: 1, tagTitle:'sieu nhan0'},
            {id: 2, tagTitle:'sieu nhan1'},
            {id: 3, tagTitle:'sieu nhan2'},
            {id: 4, tagTitle:'sieu nhan3'},
            {id: 5, tagTitle:'sieu nhan4'},
        ]
    )}, 500));
}