let _$ = (selector) => document.querySelector(selector);
let toastElement = document.querySelector('.toast');
let toast = new bootstrap.Toast(toastElement)
let toastBody = document.querySelector('.toast-body');
let errorMess='';
//handle event toast
toastElement.addEventListener('show.bs.toast', () => {
    toastBody.innerHTML = errorMess
})
let processDistanceDate = (update, start)=>{
    let result;
    let d1 = Date.parse(update||start)
    let d2 = new Date()
    let diffTime = Math.abs(d1 - d2);
    let diffMinutes = Math.floor(diffTime / (1000 * 60)); 
    let diffHours = Math.floor(diffTime / (1000 * 60 * 60)); 
    let diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24)); 
    let diffMoths = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 30)); 
    let diffYears = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 365)); 
    if(diffMinutes < 1){
        result = 'Just now!'
    } else if (diffMinutes>=1 && diffHours<1){
        result = `${diffMinutes} minutes ago`
    } else if (diffHours>=1 && diffDays<1){
        result = `${diffHours} hours ago`
    } else if (diffDays>=1 && diffMoths<1){
        result = `${diffDays} days ago`
    } else if (diffMoths>=1 && diffYears<1){
        result = `${diffMoths} moths ago`
    }else if (diffYears>=1)
        result = `${diffYears} years ago`
    return `${result}${update==null ? '' : ' (has been updated)'}`
}
const handleClickDelete = () => {
    let btnDeletes = document.querySelectorAll('.auth-side .btn-danger');
    for(let btnDelete of btnDeletes) {
        btnDelete.addEventListener('click', () => {
            let dataSend = {
                type: 'delete',
                blogId: btnDelete.closest('.blog-item').dataset.blogid
            }
            $.ajax({
                url: `http://localhost:8088/Rentabike/manage-blog`,
                type: 'post', //send it through get method
                data: dataSend,
                success: function (data) {
                    let dataRs = JSON.parse(data)
                    //data
                    errorMess = dataRs.ME
                    toast.show()
                    if(dataRs.SC!=1) return;
                    btnDelete.closest('.blog-item').remove();
                    _$('.heading-content span').innerHTML = Number(_$('.heading-content span').innerHTML)-1
                    if(_$('.heading-content span').innerHTML==0){
                        _$('.blog-list').innerHTML = `<div class="no-thing">
                                <i class="fa-solid fa-magnifying-glass"></i>
                                <h3>No blog in blog draft!</h3>
                            </div>`
                    }
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        });
    }
}

const BlogSave = ()=>{
    let dataRPOnLoad
    $(window).on('load', () => {
        $.ajax({
            url: `http://localhost:8088/Rentabike/blog-draft?type=getAll`,
            type: 'get', //send it through get method
            data: '',
            success: function (data) {
                dataRPOnLoad = JSON.parse(data)
                //data
                _$('.heading-content span').innerHTML = `${dataRPOnLoad.BlogList.length}`
                _$('.blog-list').innerHTML = (()=>{
                    if(dataRPOnLoad.BlogList==undefined || dataRPOnLoad.BlogList.length==0) {
                        return `<div class="no-thing">
                                    <i class="fa-solid fa-magnifying-glass"></i>
                                    <h3>No blog in blog draft!</h3>
                                </div>`
                    }
                    return dataRPOnLoad.BlogList.map((cur, index)=>{
                        return `<div class="blog-item" data-blogid="${cur.id}">
                                    <div class="blog-item__tittle">
                                        <a href="${`http://localhost:8088/Rentabike/blog-detail?id=${cur.id}`}">${cur.blogTitle}</a>
                                    </div>
                                    <div class="blog-item__autho">
                                        <span>${processDistanceDate(cur.updatedAt, cur.createdAt)}</span>
                                        <span>.</span>
                                        <span>Tác giả <strong>${dataRPOnLoad.Account.firstName + ' ' + dataRPOnLoad.Account.lastName}</strong></span>
                                    </div>
                                    <div class="auth-side">
                                        <button type="button" class="btn btn-warning"
                                            onclick="window.location.href ='${`http://localhost:8088/Rentabike/manage-blog?type=update&id=${cur.id}`}'"
                                        >Update</button>
                                        <button type="button" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>`
                    }).join('')
                })()

                //handle click delete
                handleClickDelete()

                //display page
                _$('#loading-page').classList.add('d-none')
                _$('.custome-container').classList.remove('d-none')
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })
}

BlogSave()