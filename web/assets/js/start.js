/*=====================OPTION-VARIABLE=====================*/
const inputs = document.querySelectorAll("input");
const widgetButtonLink = document.querySelector(".widget-button__action-link");
const widgetButtonCancelLink = document.querySelector(".widget-button__action-link[mode='back']");
const widgetCancelButton = document.querySelector(".widget-button__action[type='verifySend']");
const loadingText = document.querySelector(".loading-screen__text");
const loadingAnimation = document.querySelector(".loading-screen__icon[type='register']");
/*=====================WIDGET-OPTION-VARIABLE=====================*/
const widgetContentHeader = document.querySelector(".widget-option__content-header");
const widgetFormReset = document.querySelector(".widget-option__reset-form");
const widgetMessageNotify = document.querySelector(".widget-option__message-notify");
const widgetTake = document.querySelector(".widget-option__take");
const forgotPassSwitchButton = document.querySelector(".widget-option__switch-back");
/*=====================WIDGET-RESET-VARIABLE=====================*/
const widgetResetDescription = document.querySelector(".widget-reset__title-input");
const widgetBackLog = document.querySelector(".widget-option__reset-switch");
const resetInput = document.querySelector(".widget-option__reset-input");
/*=====================WIDGET-LOGIN-VARIABLE=====================*/
const widgetHomePage = document.querySelector(".widget-login__homepage");
const widgetDescription = document.querySelector(".widget-login__description");
const widgetLoginInputs = document.querySelectorAll(".widget-login__input");
const widgetLoginTitle = document.querySelectorAll(".widget-login__title-input");
const submitButtons = document.querySelectorAll(".widget-login__input-submit");
const widgetLeft = document.querySelector(".widget-login__content-left");
const forgotPassButton = document.querySelector("a.widget-login__option-text");
const widgetClosePage = document.querySelector(".widget-login__homepage");
const sliderImage = document.querySelector(".widget-login__background-img");
const switchButtons = document.querySelectorAll(".widget-login__switch-box");
const widgetFormElement = document.querySelector(".widget-login__form");
const widgetLoginNotify = document.querySelector(".widget-login__message-notify");
const loginSubmit = document.querySelector(".widget-login__input-submit[role='LoginSubmit']");
/*=====================WIDGET-SIGNIN-VARIABLE=====================*/
const signInSubmit = document.querySelector(".widget-login__input-submit[role='SignIn']");
const widgetSignInInputs = document.querySelectorAll(".widget-signin__input");
const widgetSignInTitle = document.querySelectorAll(".widget-signin__title-input");
const widgetSignInFirstName = document.querySelector(".widget-signin__title-input[title='firstName']");
const widgetSignInLastName = document.querySelector(".widget-signin__title-input[title='lastName']");
const widgetSignInEmail = document.querySelector(".widget-signin__title-input[title='email']");
const widgetSignInPassword = document.querySelector(".widget-signin__title-input[title='password']");
const widgetSignInConfirmPassword = document.querySelector(".widget-signin__title-input[title='confirm-password']");
const widgetSignInPhoneNumber = document.querySelector(".widget-signin__title-input[title='phoneNumber']");
const widgetRight = document.querySelector(".widget-signin__content-right");
const widgetStatusSignIn = document.querySelector(".widget-signin__status");
const widgetSignInLoading = document.querySelector(".widget-signin__status-loading");
const widgetSignIncheck = document.querySelector(".widget-get-sign-in-next");
const switchButtonPrevious = document.querySelector(".widget-get-sign-in-previous");
/*=====================REGEX=====================*/
const regexEmail = new RegExp("^\\w+([\\.-]?\\w+)+@\\w+([\\.:]?\\w+)+(\\.[a-zA-Z0-9]{2,3})+$");
const regexPassword = new RegExp("^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$");
const regexPhoneNumber = new RegExp("^(84|0[3|5|7|8|9])+([0-9]{8,9})$");
/*=====================WIDGET SIGN IN/UP=====================*/
const widget = {
    handleEvents: function () {
        for (let switchButton of switchButtons) {
            if (!(switchButton.classList.contains("widget-option__switch-box"))) {
                switchButton.onclick = function () {
                    setTimeout(() => {
                        widget.resetSettings(); }, 1000);
                    if (sliderImage.style.left === "" || sliderImage.style.left === "50%") {
                        sliderImage.style.left = "0%";
                        widgetClosePage.style.right = "88.5%";
                        sliderImage.style.backgroundImage = "url(assets/img/background/bg004.jpg)";
                    } else {
                        sliderImage.style.left = "";
                        widgetClosePage.style.right = "";
                        sliderImage.style.backgroundImage = "";
                    }
                };
            }
        }
        forgotPassButton.onclick = function () {
            if (sliderImage.style.top === "" || sliderImage.style.top === "0%") {
                setTimeout(() => {
                    widget.resetSettings();
                }, 1000);
                sliderImage.style.width = "100%";
                sliderImage.style.left = "0%";
                widgetHomePage.style.opacity = "0";
                sliderImage.style.backgroundImage = "url(assets/img/background/bg005.jpg)";
                widgetDescription.style.opacity = "0";
                widgetContentHeader.style.transform = "translateY(0%)";
                widgetContentHeader.style.display = "";
                widgetFormReset.style.transform = "translateY(0%)";
                widgetFormReset.style.display = "";
                widgetMessageNotify.style.display = "none";
                widgetMessageNotify.style.transform = "";
                setTimeout(function () {
                    sliderImage.style.top = "-100%";
                    widgetLeft.style.display = "none";
                    widgetRight.style.display = "none";
                    widgetTake.style.display = "block";
                }, 1500);
            }
        };
        forgotPassSwitchButton.onclick = function () {
            setTimeout(() => {
                widget.resetSettings();
            }, 1000);
            if (sliderImage.style.top === "-100%") {
                sliderImage.style.top = "0%";
                sliderImage.style.width = "100%";
                setTimeout(function () {
                    sliderImage.style.left = "50%";
                    sliderImage.style.width = "50%";
                    widgetTake.style.display = "none";
                    widgetLeft.style.display = "block";
                    widgetRight.style.display = "flex";
                    widgetDescription.style.opacity = "1";
                    widgetHomePage.style.opacity = "";
                    sliderImage.style.backgroundImage = "";
                }, 1500);
            }
        };
    },
    loginInput: function () {
        for (let widgetLoginInput of widgetLoginInputs) {
            let titleInput;
            let sightFocus;
            if (widgetLoginInput.role === "email") {
                titleInput = widgetLoginTitle[0];
                sightFocus = true;
            } else {
                titleInput = widgetLoginTitle[1];
                sightFocus = false;
            }

            widgetLoginInput.onkeyup = function () {
                let lengthInput = this.value.length;
                let sightCheck = false;
                if (lengthInput > 0) {
                    this.style.backgroundColor = "";
                    this.style.border = "";
                    titleInput.style.color = "";
                    titleInput.innerHTML = "Email";
                    sightFocus ? titleInput.innerHTML = "Email" : titleInput.innerHTML = "Password";
                } else {
                    this.style.backgroundColor = "var(--notify-color)";
                    this.style.border = "1px solid var(--notify-bd-color)";
                    titleInput.style.color = "var(--notify-bd-color)";
                    sightFocus ? titleInput.innerHTML = "Email - This field is required" : titleInput.innerHTML = "Password - This field is required";
                }
                sightCheck = Array.from(widgetLoginInputs).every(function (currentValue, index) {
                    return currentValue.value.length > 0;
                });
                if (sightCheck) {
                    loginSubmit.disabled = false;
                    loginSubmit.style.cursor = "pointer";
                } else {
                    loginSubmit.disabled = true;
                    loginSubmit.style.cursor = "not-allowed";
                }
            };
        }
    },
    signIn: function () {
        let checkEmail = false;
        let checkPassword = false;
        let checkCurrentPassword = false;
        let checkPhoneNumber = false;
        for (let widgetSignInInput of widgetSignInInputs) {
            let unlockSubmit = false;
            widgetSignInInput.onkeyup = function () {
                let lengthInput = this.value.length;
                let sightCheck = false;
                if (lengthInput > 0) {
                    this.style.backgroundColor = "";
                    this.style.border = "";
                } else {
                    this.style.backgroundColor = "var(--notify-color)";
                    this.style.border = "1px solid var(--notify-bd-color)";
                }
                if (this.getAttribute("role") === "firstName") {
                    if (lengthInput === 0) {
                        widgetSignInFirstName.style.color = "var(--notify-bd-color)";
                        widgetSignInFirstName.innerHTML = "First Name - This field is required.";
                        this.style.backgroundColor = "var(--notify-color)";
                        this.style.border = "1px solid var(--notify-bd-color)";
                    } else {
                        widgetSignInFirstName.style.color = "";
                        widgetSignInFirstName.innerHTML = "First Name";
                        this.style.backgroundColor = "";
                        this.style.border = "";
                    }
                }
                if (this.getAttribute("role") === "lastName") {
                    if (lengthInput === 0) {
                        widgetSignInLastName.style.color = "var(--notify-bd-color)";
                        widgetSignInLastName.innerHTML = "Last Name - This field is required.";
                        this.style.backgroundColor = "var(--notify-color)";
                        this.style.border = "1px solid var(--notify-bd-color)";
                    } else {
                        widgetSignInLastName.style.color = "";
                        widgetSignInLastName.innerHTML = "Last Name";
                        this.style.backgroundColor = "";
                        this.style.border = "";
                    }
                }
                if (this.getAttribute("role") === "email") {
                    if (regexEmail.test(this.value)) {
                        widgetSignInEmail.style.color = "";
                        widgetSignInEmail.innerHTML = "Email";
                        this.style.backgroundColor = "";
                        this.style.border = "";
                        checkEmail = true;
                    } else {
                        widgetSignInEmail.style.color = "var(--notify-bd-color)";
                        widgetSignInEmail.innerHTML = "Email - Please enter a valid email address.";
                        this.style.backgroundColor = "var(--notify-color)";
                        this.style.border = "1px solid var(--notify-bd-color)";
                        checkEmail = false;
                    }
                    if (lengthInput === 0) {
                        widgetSignInEmail.style.color = "var(--notify-bd-color)";
                        widgetSignInEmail.innerHTML = "Email - This field is required.";
                        this.style.backgroundColor = "var(--notify-color)";
                        this.style.border = "1px solid var(--notify-bd-color)";
                        checkEmail = false;
                    }
                }
                if (this.getAttribute("role") === "password") {
                    if (regexPassword.test(this.value) && lengthInput >= 8) {
                        widgetSignInPassword.style.color = "";
                        widgetSignInPassword.innerHTML = "";
                        this.style.backgroundColor = "";
                        this.style.border = "";
                        checkPassword = true;
                    } else {
                        widgetSignInPassword.style.color = "var(--notify-bd-color)";
                        widgetSignInPassword.innerHTML = "Enter at least 8 characters, one uppercase, lowercase and special character";
                        this.style.backgroundColor = "var(--notify-color)";
                        this.style.border = "1px solid var(--notify-bd-color)";
                        checkPassword = false;
                    }
                    if (lengthInput === 0) {
                        widgetSignInPassword.style.color = "var(--notify-bd-color)";
                        widgetSignInPassword.innerHTML = "This field is required.";
                        this.style.backgroundColor = "var(--notify-color)";
                        this.style.border = "1px solid var(--notify-bd-color)";
                        checkPassword = false;
                    }
                }
                if (this.getAttribute("role") === "confirm-password") {
                    let passwordCurrent = document.querySelector(".widget-signin__input[role='password']");
                    if (passwordCurrent.value !== null) {
                        if (this.value === passwordCurrent.value) {
                            widgetSignInConfirmPassword.style.color = "";
                            widgetSignInConfirmPassword.innerHTML = "";
                            this.style.backgroundColor = "";
                            this.style.border = "";
                            checkCurrentPassword = true;
                        } else {
                            widgetSignInConfirmPassword.style.color = "var(--notify-bd-color)";
                            widgetSignInConfirmPassword.innerHTML = "The password and its confirm are not the same";
                            this.style.backgroundColor = "var(--notify-color)";
                            this.style.border = "1px solid var(--notify-bd-color)";
                            checkCurrentPassword = false;
                        }
                        if (lengthInput === 0) {
                            widgetSignInConfirmPassword.style.color = "var(--notify-bd-color)";
                            widgetSignInConfirmPassword.innerHTML = "This field is required.";
                            this.style.backgroundColor = "var(--notify-color)";
                            this.style.border = "1px solid var(--notify-bd-color)";
                            checkCurrentPassword = false;
                        }
                    }
                }
                if (this.getAttribute("role") === "phoneNumber") {
                    if (regexPhoneNumber.test(this.value)) {
                        widgetSignInPhoneNumber.style.color = "";
                        widgetSignInPhoneNumber.innerHTML = "Mobile Phone";
                        this.style.backgroundColor = "";
                        this.style.border = "";
                        checkPhoneNumber = true;
                    } else {
                        widgetSignInPhoneNumber.style.color = "var(--notify-bd-color)";
                        widgetSignInPhoneNumber.innerHTML = "Mobile Phone - Phone not existed";
                        this.style.backgroundColor = "var(--notify-color)";
                        this.style.border = "1px solid var(--notify-bd-color)";
                        checkPhoneNumber = false;
                    }
                    if (lengthInput === 0) {
                        widgetSignInPhoneNumber.style.color = "var(--notify-bd-color)";
                        widgetSignInPhoneNumber.innerHTML = "Mobile Phone - This field is required.";
                        this.style.backgroundColor = "var(--notify-color)";
                        this.style.border = "1px solid var(--notify-bd-color)";
                        checkPhoneNumber = false;
                    }
                }
                sightCheck = Array.from(widgetSignInInputs).every(function (currentValue, index) {
                    return currentValue.value.length > 0;
                });
                if (checkEmail && checkPassword && checkPhoneNumber && checkCurrentPassword) {
                    unlockSubmit = true;
                }
                if (sightCheck && unlockSubmit) {
                    widgetSignIncheck.disabled = false;
                    widgetSignIncheck.style.cursor = "pointer";
                } else {
                    widgetSignIncheck.disabled = true;
                    widgetSignIncheck.style.cursor = "not-allowed";
                }
            };
            let formControls = document.querySelectorAll(".form-control");
            let sightCheck = false;
            let sightCheckCity = false
            for (let formControl of formControls) {
                formControl.onkeyup = function () {
                    let cityTitle = document.querySelector(".form-control-title-city");
                    let streetTitle = document.querySelector(".form-control-title-street");
                    if (this.getAttribute("role") === "city") {
                        if (this.value.length === 0) {
                            cityTitle.style.color = "var(--notify-bd-color)";
                            cityTitle.innerHTML = "City - This field is required.";
                            this.style.backgroundColor = "var(--notify-color)";
                            this.style.border = "1px solid var(--notify-bd-color)";
                            sightCheck = false;
                        } else {
                            cityTitle.style.color = "";
                            cityTitle.innerHTML = "City";
                            this.style.backgroundColor = "";
                            this.style.border = "";
                            sightCheck = true;
                        }
                    }
                    if (this.getAttribute("role") === "street") {
                        if (this.value.length === 0) {
                            streetTitle.style.color = "var(--notify-bd-color)";
                            streetTitle.innerHTML = "Street - This field is required.";
                            this.style.backgroundColor = "var(--notify-color)";
                            this.style.border = "1px solid var(--notify-bd-color)";
                            sightCheckCity = false;
                        } else {
                            streetTitle.style.color = "";
                            streetTitle.innerHTML = "Street";
                            this.style.backgroundColor = "";
                            this.style.border = "";
                            sightCheckCity = true;
                        }
                    }
                    if (sightCheck && sightCheckCity) {
                        signInSubmit.disabled = false;
                        signInSubmit.style.cursor = "pointer";
                    } else {
                        signInSubmit.disabled = true;
                        signInSubmit.style.cursor = "not-allowed";
                    }
                };
            }
        }
        let firstForm = document.querySelector(".widget-signin__first-form");
        let secondForm = document.querySelector(".widget-signin__second-form");

        widgetSignIncheck.onclick = function () {
            if (firstForm.style.display === "block") {
                secondForm.style.display = "block";
                firstForm.style.display = "none";
            }
        };
        switchButtonPrevious.onclick = function () {
            if (secondForm.style.display === "block") {
                firstForm.style.display = "block";
                secondForm.style.display = "none";
            }
        };
    },
    signInSubmit: function () {
        let signInMessage = document.querySelector(".widget-signin__status-reg");
        let signInMessDescription = document.querySelector(".widget-signin__status-description");

        $('#registerController').submit(function (event) {
            event.preventDefault();
            signInSubmit.disabled = true;
            signInSubmit.style.cursor = "not-allowed";
            let firstName = widgetSignInInputs[0].value,
                    lastName = widgetSignInInputs[1].value,
                    phoneNumber = widgetSignInInputs[2].value,
                    email = widgetSignInInputs[3].value,
                    passWord = widgetSignInInputs[4].value,
                    city = document.querySelector(".form-control[role='city']").value;
            street = document.querySelector(".form-control[role='street']").value;
            state = $('.form-select-option option:selected').val();
            $.ajax({
                url: "/Rentabike/registerController",
                type: "post",
                data: {
                    firstName: firstName,
                    lastName: lastName,
                    phoneNumber: phoneNumber,
                    email: email,
                    password: passWord,
                    city: city,
                    street: street,
                    state: state,
                    mode: "USER"
                },
                success: function (response) {
                    signInMessage.classList.remove("success");
                    signInMessage.classList.remove("failed");
                    if (response === "SUCCESS") {
                        signInMessage.classList.add("success");
                        signInMessage.innerHTML = "Sign up successfully.";
                        signInMessDescription.innerHTML = "We will send message to your mail to verify your account.";
                        widgetButtonLink.innerHTML = "Send";
                        widgetButtonLink.setAttribute("mode", "send");
                    }
                    if (response === "FAILED") {
                        signInMessage.classList.add("failed");
                        signInMessage.innerHTML = "Sign up failed.";
                        signInMessDescription.innerHTML = "An unexpected error occurred. Please try again.";
                        widgetButtonLink.innerHTML = "Cancel";
                        widgetButtonLink.setAttribute("mode", "cancel");
                    }
                    if (response === "EMAIL") {
                        signInMessage.classList.add("failed");
                        signInMessage.innerHTML = "Sign up not successfully.";
                        signInMessDescription.innerHTML = "Your email used to sign up had been existed. Try another email.";
                        widgetButtonLink.innerHTML = "Try again";
                        widgetButtonLink.setAttribute("mode", "tryAgain");
                    }
                    widgetStatusSignIn.style.visibility = "visible";
                    widgetStatusSignIn.style.opacity = "1";
                    widgetStatusSignIn.style.top = "0%";
                }
            });
        }
        );
    },
    signUpMode: function () {
        let inputEmail = document.querySelector("#widget-signin__input-email");
        widgetButtonLink.onclick = function () {
            if (this.getAttribute("mode") === "send") {
                loadingText.innerHTML = "NOW LOADING";
                loadingAnimation.style.display = "";
                widgetSignInLoading.style.display = "flex";
                widgetCancelButton.style.opacity = "0";
                widgetCancelButton.style.visibility = "hidden";
                $.ajax({
                    url: "/Rentabike/verifyAccountController",
                    type: "post",
                    data: {
                        email: inputEmail.value,
                        modeSwitch: "sendVerify"
                    },
                    success: function (response) {
                        setTimeout(() => {
                            if (response === "sendVerifySuccess") {
                                loadingText.innerHTML = "Process completed. Please check your mail to continue.<br>Still not received? Try again.";
                            } else {
                                loadingText.innerHTML = "Process failed. An unexpected error occurred. Please try again..";
                            }
                            loadingAnimation.style.display = "none";
                            widgetCancelButton.style.visibility = "";
                            widgetCancelButton.style.opacity = "1";
                        }, 2000);
                    }
                });
            }
            if (this.getAttribute("mode") === "cancel" || this.getAttribute("mode") === "tryAgain") {
                widgetStatusSignIn.style.visibility = "";
                widgetStatusSignIn.style.opacity = "";
                widgetStatusSignIn.style.top = "";
                signInSubmit.innerHTML = "Sign in";
                if (this.getAttribute("mode") === "tryAgain") {
                    inputEmail.value = "";
                    signInSubmit.disabled = true;
                    widgetSignIncheck.disabled = true;
                    switchButtonPrevious.click();
                    signInSubmit.style.cursor = "not-allowed";
                    widgetSignIncheck.style.cursor = "not-allowed";
                } else {
                    signInSubmit.disabled = false;
                    signInSubmit.style.cursor = "pointer";
                }
            }
        };
        widgetButtonCancelLink.onclick = function () {
            widgetStatusSignIn.style.visibility = "";
            widgetStatusSignIn.style.opacity = "";
            widgetStatusSignIn.style.top = "";
            widgetSignInLoading.style.display = "";
        };
    },
    logInSubmit: function () {
        $('#loginController').submit(function (event) {
            event.preventDefault();
            let inputEmailLogin = document.querySelector("#widget-login__input-email");
            let inputPasswordLogin = document.querySelector("#widget-login__input-password");
            let widgetLogCheckBox = document.querySelector(".widget-login__checkbox");
            let widgetLoginLoading = document.querySelector(".widget-login__process-loading");
            widgetLoginNotify.style.transform = "";
            widgetFormElement.style.marginTop = "";
            loginSubmit.innerHTML = "";
            loginSubmit.disabled = true;
            loginSubmit.style.cursor = "not-allowed";

            widgetLoginLoading.style.display = "block";
            $.ajax({
                url: "/Rentabike/loginController",
                type: "post",
                data: {
                    email: inputEmailLogin.value,
                    password: inputPasswordLogin.value,
                    rememberMe: widgetLogCheckBox.checked
                },
                success: function (response) {
                    setTimeout(() => {
                        if (response === "CUSTOMER" || response === "SALEPERSON") {
                            window.location.replace("http://localhost:8088/Rentabike/productListController");
                        } else if (response === "STAFF" || response === "ADMIN") {
                            window.location.replace("http://localhost:8088/Rentabike/auth-two-steps-basic.jsp");
                        } else {
                            widgetFormElement.style.marginTop = "0rem";
                            widgetLoginLoading.style.display = "";
                            loginSubmit.innerHTML = "Sign in";
                            loginSubmit.disabled = false;
                            loginSubmit.style.cursor = "pointer";
                            setTimeout(() => {
                                widgetLoginNotify.style.transform = "scale(1)";
                            }, 1000);
                        }
                    }, 1500);
                }
            });
        });
    },
    signInWith: function () {
        window.handleCredentialResponse = (response) => {
            responsePayload = jwt_decode(response.credential);
            var id = responsePayload.sub;
            var fullName = responsePayload.name;
            var givenName = responsePayload.given_name;
            var familyName = responsePayload.family_name;
            var image = responsePayload.picture;
            var email = responsePayload.email;

            $.ajax({
                url: "/Rentabike/loginController",
                type: "post",
                data: {
                    email: email,
                    firstName: familyName,
                    lastName: givenName,
                    avatar: image,
                    loginWith: true
                },
                success: function (response) {
                    window.location.replace("http://localhost:8088/Rentabike/productListController");
                }
            });
        };
    },
    showPassword: function () {
        let buttonShow = document.querySelector(".widget-login__input-show[type='button']");
        let closeInnerHTML = `<svg fill="none" style="width:20px;height:20px" viewBox="0 0 20 10" class="_340FWs"><path stroke="none" fill="#000" fill-opacity=".54" d="M19.834 1.15a.768.768 0 00-.142-1c-.322-.25-.75-.178-1 .143-.035.036-3.997 4.712-8.709 4.712-4.569 0-8.71-4.712-8.745-4.748a.724.724 0 00-1-.071.724.724 0 00-.07 1c.07.106.927 1.07 2.283 2.141L.631 5.219a.69.69 0 00.036 1c.071.142.25.213.428.213a.705.705 0 00.5-.214l1.963-2.034A13.91 13.91 0 006.806 5.86l-.75 2.535a.714.714 0 00.5.892h.214a.688.688 0 00.679-.535l.75-2.535a9.758 9.758 0 001.784.179c.607 0 1.213-.072 1.785-.179l.75 2.499c.07.321.392.535.677.535.072 0 .143 0 .179-.035a.714.714 0 00.5-.893l-.75-2.498a13.914 13.914 0 003.248-1.678L18.3 6.147a.705.705 0 00.5.214.705.705 0 00.499-.214.723.723 0 00.036-1l-1.82-1.891c1.463-1.071 2.32-2.106 2.32-2.106z"></path></svg>`;
        let showInnerHTML = `<svg fill="none" style="width:20px;height:20px" viewBox="0 0 20 12" class="_340FWs"><path stroke="none" fill="#000" fill-opacity=".54" fill-rule="evenodd" d="M19.975 5.823V5.81 5.8l-.002-.008v-.011a.078.078 0 01-.002-.011v-.002a.791.791 0 00-.208-.43 13.829 13.829 0 00-1.595-1.64c-1.013-.918-2.123-1.736-3.312-2.368-.89-.474-1.832-.867-2.811-1.093l-.057-.014a2.405 2.405 0 01-.086-.02L11.884.2l-.018-.003A9.049 9.049 0 0010.089 0H9.89a9.094 9.094 0 00-1.78.197L8.094.2l-.016.003-.021.005a1.844 1.844 0 01-.075.017l-.054.012c-.976.226-1.92.619-2.806 1.09-1.189.635-2.3 1.45-3.31 2.371a13.828 13.828 0 00-1.595 1.64.792.792 0 00-.208.43v.002c-.002.007-.002.015-.002.022l-.002.01V5.824l-.002.014a.109.109 0 000 .013L0 5.871a.206.206 0 00.001.055c0 .01 0 .018.002.027 0 .005 0 .009.003.013l.001.011v.007l.002.01.001.013v.002a.8.8 0 00.208.429c.054.067.11.132.165.197a13.9 13.9 0 001.31 1.331c1.043.966 2.194 1.822 3.428 2.48.974.52 2.013.942 3.09 1.154a.947.947 0 01.08.016h.003a8.864 8.864 0 001.596.16h.2a8.836 8.836 0 001.585-.158l.006-.001a.015.015 0 01.005-.001h.005l.076-.016c1.079-.212 2.118-.632 3.095-1.153 1.235-.66 2.386-1.515 3.43-2.48a14.133 14.133 0 001.474-1.531.792.792 0 00.208-.43v-.002c.003-.006.003-.015.003-.022v-.01l.002-.008c0-.004 0-.009.002-.013l.001-.012.001-.015.001-.019.002-.019a.07.07 0 01-.01-.036c0-.009 0-.018-.002-.027zm-6.362.888a3.823 3.823 0 01-1.436 2.12l-.01-.006a3.683 3.683 0 01-2.178.721 3.67 3.67 0 01-2.177-.721l-.009.006a3.823 3.823 0 01-1.437-2.12l.014-.01a3.881 3.881 0 01-.127-.974c0-2.105 1.673-3.814 3.738-3.816 2.065.002 3.739 1.711 3.739 3.816 0 .338-.047.662-.128.975l.011.009zM8.145 5.678a1.84 1.84 0 113.679 0 1.84 1.84 0 01-3.679 0z" clip-rule="evenodd"></path></svg>`;
        let inputPassword = document.querySelector("#widget-login__input-password");

        buttonShow.onclick = function () {
            document.querySelector("svg._340FWs").remove();
            if (inputPassword.type === "text") {
                inputPassword.type = "password";
                buttonShow.insertAdjacentHTML("beforeend", closeInnerHTML);
            } else {
                inputPassword.type = "text";
                buttonShow.insertAdjacentHTML("beforeend", showInnerHTML);
            }
        };
    },
    resetPassWord: function () {
        let resetSubmit = document.querySelector(".widget-login__input-submit[role='ResetPassword']");

        resetInput.onkeyup = function () {
            let lengthInput = this.value.length;
            let sightCheck = false;

            if (regexEmail.test(this.value)) {
                widgetResetDescription.style.color = "";
                widgetResetDescription.innerHTML = "Email";
                this.style.backgroundColor = "";
                this.style.border = "";
                sightCheck = true;
            } else {
                widgetResetDescription.style.color = "var(--notify-bd-color)";
                widgetResetDescription.innerHTML = "Email - Not correct format";
                this.style.backgroundColor = "var(--notify-color)";
                this.style.border = "1px solid var(--notify-bd-color)";
                sightCheck = false;
            }
            if (lengthInput === 0) {
                widgetResetDescription.style.color = "var(--notify-bd-color)";
                widgetResetDescription.innerHTML = "Email - This field is required";
                this.style.backgroundColor = "var(--notify-color)";
                this.style.border = "1px solid var(--notify-bd-color)";
                sightCheck = false;
            }

            if (sightCheck) {
                resetSubmit.disabled = false;
                resetSubmit.style.cursor = "pointer";
            } else {
                resetSubmit.disabled = true;
                resetSubmit.style.cursor = "not-allowed";
            }
        };
        resetSubmit.onclick = function () {
            let promise;
            if (regexEmail.test(resetInput.value)) {
                promise = new Promise((resolve, reject) => {
                    $.ajax({
                        url: "/Rentabike/forgotPasswordController",
                        type: "post",
                        data: {
                            email: resetInput.value,
                            switchMode: "sendEmail"
                        },
                        success: function (response) {
                            setTimeout(() => {
                                if (response === "SUCCESS") {
                                    resolve();
                                } else {
                                    reject();
                                }
                            }, 1500);
                        }
                    });
                });
                promise
                        .then(() => {
                            widgetContentHeader.style.transform = "translateY(-200%)";
                            widgetFormReset.style.transform = "translateY(300%)";
                            widgetMessageNotify.style.display = "block";
                        })
                        .then(() => {
                            setTimeout(() => {
                                widgetContentHeader.style.display = "none";
                                widgetFormReset.style.display = "none";
                            }, 500);
                        })
                        .then(() => {
                            setTimeout(() => {
                                widgetMessageNotify.style.transform = "translateX(0%)";
                            }, 500);
                        })
                        .catch(() => {
                            console.log("FAILED");
                        });
            } else {
                widgetResetDescription.style.color = "var(--notify-bd-color)";
                widgetResetDescription.innerHTML = "Email - Not correct format. Please try again";
                resetInput.style.backgroundColor = "var(--notify-color)";
                resetInput.style.border = "1px solid var(--notify-bd-color)";
                resetInput.value = "";
            }
        };
        widgetBackLog.onclick = function () {
            forgotPassSwitchButton.click();
        };
    },
    resetSettings: function () {
        let titleInputE = widgetLoginTitle[0];
        let titleInputP = widgetLoginTitle[1];
        inputs.forEach(function (currentValue) {
            currentValue.value = "";
            currentValue.style.backgroundColor = "";
            currentValue.style.border = "";
        });
        widgetSignInTitle.forEach(function (currentValue) {
            currentValue.style.color = "";
        });
        submitButtons.forEach(function (currentValue) {
            currentValue.disabled = true;
            currentValue.style.cursor = "not-allowed";
        });
        titleInputE.style.color, titleInputP.style.color, widgetResetDescription.style.color = "";
        titleInputE.innerHTML, widgetResetDescription.innerHTML = "Email";
        titleInputP.innerHTML = "Password";
        widgetLoginNotify.style.transform = "";
        widgetFormElement.style.marginTop = "";
    },
    start: function () {
        this.handleEvents();
        this.signIn();
        this.signInSubmit();
        this.signUpMode();
        this.signInWith();
        this.showPassword();
        this.loginInput();
        this.logInSubmit();
        this.resetPassWord();
    }
};
widget.start();