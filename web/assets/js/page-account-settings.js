const ecomProduct = document.querySelector("#ecommerce-products");
const ecomPagination = document.querySelector("#ecommerce-pagination");
const lidgetButtonScroll = document.querySelector(".lidget-shop-button");
const shopSearch = document.querySelector("#shop-search");
const sortingChoice = document.querySelector("#sortingChoice");
const sortingChoicePost = document.querySelector("#sortingChoice-1");
const multiRangeChoice = document.querySelectorAll(".lidget-form-checkbox");
const clearFilterButton = document.querySelector(".lidget-clear-filter");
const slider = document.getElementById("slider-price");
const formControl = document.querySelectorAll(".form-control");
/*=====================WIDGET-SHOP=====================*/
const widgetShop = {
    handleEditProfile: function () {
        $('#editProfileUserSubmit').on('click', function () {
            let firstName = formControl[2].value;
            let lastName = formControl[3].value;
            let emailAddress = formControl[4].value;
            let city = formControl[5].value;
            let street = formControl[6].value;
            let contact = formControl[7].value;
            let country = $('#modalEditUserCountry option:selected').val();
            let role = formControl[8].value.toString().toUpperCase();
            let imageBinary = document.querySelector(".hiddenI").value;
            let userAvatar = document.querySelector("#userAvatar");

            $.ajax({
                url: "/Rentabike/userProfileController",
                type: "post",
                data: {
                    mode: "editProfileuser",
                    firstName: firstName,
                    lastName: lastName,
                    emailAddress: emailAddress,
                    city: city,
                    street: street,
                    country: country,
                    contact: contact,
                    role: role,
                    image: imageBinary
                },
                success: function (response) {
                    if (response !== "FAILED" && response !== "ERROR") {
                        toastr.success("Your profile had been successfully changed", "Process Success", {
                            closeButton: !0,
                            tapToDismiss: !1,
                            rtl: !1
                        });
                        if (imageBinary !== "" || imageBinary.length !== 0) {
                            userAvatar.src = response;
                        }
                        $("#fullname").text(firstName + " " + lastName);
                        $("#fullnameE2").text(firstName + " " + lastName);
                        $("#phoneNumber").text(contact);
                        $("city").text(city);
                        $("#street").text(street);
                        $("#state").text(country);
                        $("#btn-close").click();
                    } else if (response === "ERROR") {
                        toastr.error("There are some error expected.", "Process failed", {
                            closeButton: !0,
                            tapToDismiss: !1,
                            rtl: !1
                        });
                    } else {
                        toastr.error("Your session had been failed. Please logging again", "Session failed", {
                            closeButton: !0,
                            tapToDismiss: !1,
                            rtl: !1
                        });
                    }
                }
            });
        });
    },    pageOnLoad: function () {
        let notifyStatus = true;
        $(window).on('load', function () {
            $.ajax({
                url: "/Rentabike/notifyController",
                type: "post",
                data: {
                    mode: "showNotifyOnly"
                },
                success: function (response) {
                    if (response !== 'FAILED') {
                        $("#notify").html(response);
                    }
                    if (notifyStatus) {
                        $.ajax({
                            url: "/Rentabike/notifyController",
                            type: "post",
                            data: {
                                mode: "NotifyTotalHandler",
                                setUpNotify: "calculateNotify"
                            },
                            success: function (response) {
                                if (response !== "FAILED") {
                                    $("#notifyCalculation").text(response);
                                }
                            }
                        });
                        notifyStatus = false;
                    }
                }
            });
        });
    },
    ResetSettings: function () {
        $("#editUserButton").on('click', function () {
            let userAvatar = document.querySelector("#userAvatar");
            $("#account-upload-img").attr("src", userAvatar.src);
        });
    },
    start: function () {
        this.handleEditProfile();
        this.ResetSettings();
        this.pageOnLoad();
    }
};
widgetShop.start();

