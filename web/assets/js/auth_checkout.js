
/*=====================WIDGET-SHOP=====================*/
const widgetCheckout = {
    pageOnLoad: function () {
        let notifyStatus = true;
        $(window).on('load', function () {
            $.ajax({
                url: "/Rentabike/notifyController",
                type: "post",
                data: {
                    mode: "showNotifyOnly"
                },
                success: function (response) {
                    if (response !== 'FAILED') {
                        $("#notify").html(response);
                    }
                    if (notifyStatus) {
                        $.ajax({
                            url: "/Rentabike/notifyController",
                            type: "post",
                            data: {
                                mode: "NotifyTotalHandler",
                                setUpNotify: "calculateNotify"
                            },
                            success: function (response) {
                                if (response !== "FAILED") {
                                    $("#notifyCalculation").text(response);
                                }
                            }
                        });
                        notifyStatus = false;
                    }
                }
            });
        });
    },
    deleteCheckOut: function (idProduct, email) {
        $.ajax({
            url: "/Rentabike/checkOutController",
            type: "post",
            data: {
                mode: "deleteCheckOut",
                idProduct: idProduct
            },
            success: function (response) {
                if (response === "SUCCESS") {
                    $.ajax({
                        url: "/Rentabike/notifyController",
                        type: "post",
                        data: {
                            mode: "notifyRecordAndShow",
                            productID: idProduct,
                            email: email,
                            setUpNotify: "RemoveCheckout"
                        },
                        success: function (response) {
                            if (response !== "FAILED") {
                                $("#notify").html(response);
                            } else {
                                window.location.replace("http://localhost:8088/Rentabike/homepageCController");
                            }
                            if (notifyStatus) {
                                $.ajax({
                                    url: "/Rentabike/notifyController",
                                    type: "post",
                                    data: {
                                        mode: "NotifyTotalHandler",
                                        setUpNotify: "calculateNotify"
                                    },
                                    success: function (response) {
                                        if (response !== "FAILED") {
                                            $("#notifyCalculation").text(response);
                                        }
                                    }
                                });
                                notifyStatus = false;
                            }

                        }
                    });
                } else {
                    toastr.error("", "Failed to remove Item in Your Cart 🛒", {
                        closeButton: !0,
                        tapToDismiss: !1,
                        rtl: !1
                    });
                }
            }
        });
    },
    viewContract: function (idProduct, idStore) {
        $.ajax({
            url: "/Rentabike/checkOutController",
            type: "post",
            data: {
                mode: "viewContract",
                productID: idProduct,
                storeID: idStore
            },
            success: function (response) {
                let splitItem = response.toString().split("LGN");
                $("#seller-p-name").text("Printed Name: " + splitItem[0]);
                $("#seller-p-name-alt").text(splitItem[0]);
                $("#contract-seller").text(splitItem[0]);
                $("#seller-p-city").text("City: " + splitItem[1]);
                $("#seller-p-street").text("Street: " + splitItem[2]);
                $("#seller-p-state").text("State: " + splitItem[3]);
                $("#seller-p-phone").text("Mobile Phone: " + splitItem[4]);

                $("#user-p-name").text("Printed Name: " + splitItem[5]);
                $("#user-p-name-alt").text(splitItem[5]);
                $("#contract-user").text(splitItem[5]);
                $("#user-p-city").text("City: " + splitItem[6]);
                $("#user-p-street").text("Street: " + splitItem[7]);
                $("#user-p-state").text("State: " + splitItem[8]);
                $("#user-p-phone").text("Mobile Phone: " + splitItem[9]);

                $("#motor-p-category").text("Category: " + splitItem[10]);
                $("#motor-p-name").text(splitItem[19]);
                $("#contract-product").text(splitItem[19]);
                $("#motor-p-displace").text("Displacement: " + splitItem[11] + "cc");
                $("#motor-p-trans").text("Transmission: " + splitItem[12]);
                $("#motor-p-starter").text("Starter: " + splitItem[13]);
                $("#motor-p-fuelS").text("Fuel System: " + splitItem[14]);
                $("#motor-p-fuelC").text("Fuel Capacity: " + splitItem[15] + " gallons");
                $("#motor-p-dry").text("Dry Weight: " + splitItem[16] + " pounds");
                $("#motor-p-seat").text("Seat Height: " + splitItem[17] + " inches");
                $("#motor-p-price").text(splitItem[18]);
            }
        });
    },
    start: function () {
        this.pageOnLoad();
    }
};
widgetCheckout.start();

function sendParamter(param) {
    let email = document.querySelector(".lidget-naz").getAttribute("current-login");
    let idProduct = param.getAttribute("data-attr");
    let idStore = param.getAttribute("data-alt");
    let statusHandle = param.getAttribute("id");

    if (statusHandle === "deleteCheckOut") {
        widgetCheckout.deleteCheckOut(idProduct, email);
    } else if (statusHandle === "markedCheckOut") {

    } else if (statusHandle === "viewContract") {
        widgetCheckout.viewContract(idProduct, idStore);
    }

}