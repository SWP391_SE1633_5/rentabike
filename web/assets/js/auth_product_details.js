/*=====================feedback-variable=====================*/
const leftController = document.querySelector(".Feedback-page__control[type='leftControl']");
const rightController = document.querySelector(".Feedback-page__control[type='rightControl']");
const FeedbackItems = document.querySelectorAll(".Feedback-page__item");
const FeedbackList = document.querySelector(".Feedback-page__list");
/*=====================WIDGET-SHOP=====================*/
const widgetDetails = {
    handleTouchCartSpin: function () {
        $("#touchspin-product").TouchSpin({
            min: 1,
            max: document.getElementById("auth-details").getAttribute("data-value"),
            step: 1,
            decimals: 0,
            boostat: 5,
            maxboostedstep: 1
        });
    },
        addToCheckOut: function (indexProduct, email, param) {
        let addCart = $(param);
        let findCart = addCart.find(".add-to-cart");
        let notifyStatus = true;
        findCart.text("View In Cart").removeClass("add-to-cart").addClass("view-in-cart"),
                toastr.success("", "Added Item in Your Cart 🛒", {
                    closeButton: !0,
                    tapToDismiss: !1,
                    rtl: !1
                });
        $.ajax({
            url: "/Rentabike/notifyController",
            type: "post",
            data: {
                mode: "notifyRecordAndShow",
                productID: indexProduct,
                email: email,
                setUpNotify: "Checkout"
            },
            success: function (response) {
                if (response !== "FAILED") {
                    $("#notify").html(response);
                } else {
                    window.location.replace("http://localhost:8088/Rentabike/homepageCController");
                }
                if (notifyStatus) {
                    $.ajax({
                        url: "/Rentabike/notifyController",
                        type: "post",
                        data: {
                            mode: "NotifyTotalHandler",
                            setUpNotify: "calculateNotify"
                        },
                        success: function (response) {
                            if (response !== "FAILED") {
                                $("#notifyCalculation").text(response);
                            }
                        }
                    });
                    notifyStatus = false;
                }
            }
        });
    },
    wishListHandle: function (indexProduct, email, param) {
        let addWishList = $(param);
        let switchMode;
        let notifyStatus = true;

        if (addWishList.find("svg").hasClass("text-danger")) {
            addWishList.find("svg").toggleClass("text-danger") &&
                    toastr.error("", "Remove from wishlist ❤️", {
                        closeButton: !0,
                        tapToDismiss: !1,
                        rtl: !1
                    });
            switchMode = "removeWishList";
        } else {
            addWishList.find("svg").toggleClass("text-danger"),
                    addWishList.find("svg").hasClass("text-danger") &&
                    toastr.success("", "Added to wishlist ❤️", {
                        closeButton: !0,
                        tapToDismiss: !1,
                        rtl: !1
                    });
            switchMode = "addWishList";
        }
        $.ajax({
            url: "/Rentabike/productListController",
            type: "post",
            data: {
                mode: "wishListHandle",
                productID: indexProduct,
                email: email,
                modeSwitch: switchMode
            },
            success: function (response) {
                if (response === "FAILED") {
                    window.location.replace("http://localhost:8088/Rentabike/homepageCController");
                } else {
                    $.ajax({
                        url: "/Rentabike/notifyController",
                        type: "post",
                        data: {
                            mode: "notifyRecordAndShow",
                            productID: indexProduct,
                            email: email,
                            setUpNotify: switchMode
                        },
                        success: function (response) {
                            if (response !== "FAILED") {
                                $("#notify").html(response);
                            } else {
                                window.location.replace("http://localhost:8088/Rentabike/homepageCController");
                            }
                            if (notifyStatus) {
                                $.ajax({
                                    url: "/Rentabike/notifyController",
                                    type: "post",
                                    data: {
                                        mode: "NotifyTotalHandler",
                                        setUpNotify: "calculateNotify"
                                    },
                                    success: function (response) {
                                        if (response !== "FAILED") {
                                            $("#notifyCalculation").text(response);
                                        }
                                    }
                                });
                                notifyStatus = false;
                            }
                        }
                    });
                }
            }
        });
    },
    pageOnLoad: function () {
        let notifyStatus = true;
        $(window).on('load', function () {
            $.ajax({
                url: "/Rentabike/notifyController",
                type: "post",
                data: {
                    mode: "showNotifyOnly"
                },
                success: function (response) {
                    if (response !== 'FAILED') {
                        $("#notify").html(response);
                    }
                    if (notifyStatus) {
                        $.ajax({
                            url: "/Rentabike/notifyController",
                            type: "post",
                            data: {
                                mode: "NotifyTotalHandler",
                                setUpNotify: "calculateNotify"
                            },
                            success: function (response) {
                                if (response !== "FAILED") {
                                    $("#notifyCalculation").text(response);
                                }
                            }
                        });
                        notifyStatus = false;
                    }
                }
            });
        });
    },
    sliderFeedBack: function () {
        let numberOfPages = FeedbackItems.length;
        let limitPage = parseInt(FeedbackList.style.getPropertyValue('--per-page'));
        leftController.onclick = function () {
            let indexPage = parseInt(FeedbackList.style.getPropertyValue('--slide-to-index'));
            let availablePage = parseInt(FeedbackList.style.getPropertyValue('--show-page'));
            let totalPage = parseInt(availablePage - limitPage);
            let multiplePage = parseInt(numberOfPages / limitPage);
            let movePage = parseInt(indexPage - 1);
            let lastPage = numberOfPages % 3 === 0 ? multiplePage - 1 : multiplePage;
            if (totalPage < 0) {
                indexPage = FeedbackList.style.setProperty('--slide-to-index', lastPage);
                availablePage = FeedbackList.style.setProperty('--show-page', parseInt(multiplePage + limitPage));
            } else {
                indexPage = FeedbackList.style.setProperty('--slide-to-index', movePage);
                availablePage = FeedbackList.style.setProperty('--show-page', totalPage);
            }
        };
        rightController.onclick = function () {
            let indexPage = parseInt(FeedbackList.style.getPropertyValue('--slide-to-index'));
            let availablePage = parseInt(FeedbackList.style.getPropertyValue('--show-page'));
            let totalPage = parseInt(availablePage + limitPage);
            let movePage = parseInt(indexPage + 1);
            if (totalPage >= numberOfPages) {
                indexPage = FeedbackList.style.setProperty('--slide-to-index', 0);
                availablePage = FeedbackList.style.setProperty('--show-page', 0);
            } else {
                indexPage = FeedbackList.style.setProperty('--slide-to-index', movePage);
                availablePage = FeedbackList.style.setProperty('--show-page', totalPage);
            }
        };
    },
    start: function () {
        this.handleTouchCartSpin();
        this.pageOnLoad();
        this.sliderFeedBack();
    }
};
widgetDetails.start();

function sendToWishList(param) {
    let email = document.querySelector(".lidget-naz").getAttribute("current-login");
    let indexProduct = param.getAttribute("index-data");
    widgetDetails.wishListHandle(indexProduct, email, param);
}

function sendToCheckout(param) {
    let email = document.querySelector(".lidget-naz").getAttribute("current-login");
    let indexProduct = param.getAttribute("index-data");
    widgetDetails.addToCheckOut(indexProduct, email, param);
}