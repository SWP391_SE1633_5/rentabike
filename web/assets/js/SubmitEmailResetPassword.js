function SubmitEmailResetPassword(){
    let formSubmit = document.querySelector('.form-submit')
    let btnSubmit = document.querySelector('.submit')
    let spinner = btnSubmit.querySelector('.spinner-border')
    let inputE = document.querySelector('input[type="email"]');
    // let showMess = document.querySelector('.toast.align-items-center');
    let isSubmit = false
    let toastElement = document.querySelector('.toast');
    let toast = new bootstrap.Toast(toastElement)
    let toastBody = document.querySelector('.toast-body');
    let errorMess='';
    const regexEmail = new RegExp("^\\w+([\\.-]?\\w+)+@\\w+([\\.:]?\\w+)+(\\.[a-zA-Z0-9]{2,3})+$");
    
    btnSubmit.classList.add('disabled')
    inputE.addEventListener('input', (e) => {
        if(!e.target.value.match(regexEmail)){
            btnSubmit.classList.add('disabled')
        } else {
            btnSubmit.classList.remove('disabled')
        }
    })



    //handle event toast
    toastElement.addEventListener('show.bs.toast', () => {
        toastBody.innerHTML = errorMess
    })

    let formData = {}
    // var queryString = $('.form-submit').serialize();

    formSubmit.addEventListener('submit', (e)=>{
        e.preventDefault();
        if(isSubmit) return;
        isSubmit=true;
        btnSubmit.classList.add('disabled')
        spinner.classList.remove('hide')
        formData = {...formData, email: inputE.value}
        console.log(formData)

        $.ajax({
            url: "http://localhost:8088/Rentabike/reset-password",
            type: "post", //send it through get method
            data: formData,
            success: function (data) {
                btnSubmit.classList.remove('disabled')
                spinner.classList.add('hide')
                let dataP = JSON.parse(data)
                console.log(dataP);
                errorMess = dataP.Mess
                toast.show();
                isSubmit = false;
            },
            error: function (xhr) {
                btnSubmit.classList.remove('disabled')
                spinner.classList.add('hide')
                console.log(xhr);
                errorMess = 'Something went wrong!';
                toast.show();
                isSubmit = false;
            }
        });
    })
}

SubmitEmailResetPassword()