const formControlStaff = document.querySelectorAll(".form-control-staff");
const originalHtml = $("#lidget__list-user").html();
var statusChanged = false;
var allowedChanged = false;
/*=====================WIDGET-USER=====================*/
/*=====================ICON-STATUS-VARIABLE=====================*/
const finalInActive = "<span class='badge rounded-pill badge-light-primary' text-capitalized=''>Inactive</span>";
const finalActive = "<span class='badge rounded-pill badge-light-success' text-capitalized=''>Active</span>";
const finalBanned = "<span class='badge rounded-pill badge-light-secondary' text-capitalized=''>Banned</span>";
const finalCheckAdmin = "<span class=\"text-truncate align-middle\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-slack font-medium-3 text-danger me-50\">\n"
        + "<path d=\"M14.5 10c-.83 0-1.5-.67-1.5-1.5v-5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5z\">\n"
        + "</path>\n"
        + "<path d=\"M20.5 10H19V8.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z\"></path>\n"
        + "<path d=\"M9.5 14c.83 0 1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5S8 21.33 8 20.5v-5c0-.83.67-1.5 1.5-1.5z\">\n"
        + "</path>\n"
        + "<path d=\"M3.5 14H5v1.5c0 .83-.67 1.5-1.5 1.5S2 16.33 2 15.5 2.67 14 3.5 14z\"></path>\n"
        + "<path d=\"M14 14.5c0-.83.67-1.5 1.5-1.5h5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-5c-.83 0-1.5-.67-1.5-1.5z\">\n"
        + "</path>\n"
        + "<path d=\"M15.5 19H14v1.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5z\"></path>\n"
        + "<path d=\"M10 9.5C10 8.67 9.33 8 8.5 8h-5C2.67 8 2 8.67 2 9.5S2.67 11 3.5 11h5c.83 0 1.5-.67 1.5-1.5z\">\n"
        + "</path>\n"
        + "<path d=\"M8.5 5H10V3.5C10 2.67 9.33 2 8.5 2S7 2.67 7 3.5 7.67 5 8.5 5z\"></path>\n"
        + "</svg>Admin\n"
        + "</span>";
const finalCheckStaff = "<span class=\"text-truncate align-middle\">\n"
        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\n"
        + "stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
        + "class=\"feather feather-settings font-medium-3 text-warning me-50\">\n"
        + "<circle cx=\"12\" cy=\"12\" r=\"3\"></circle>\n"
        + "<path\n"
        + "d=\"M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z\">\n"
        + "</path>\n"
        + "</svg>\n"
        + "Staff\n"
        + "</span>";
const finalCheckSalePerson = "<i data-feather='truck' style=\"\n"
        + "width: 16.8px;\n"
        + "height: 16.8px;\n"
        + "margin-right: 7px;\n"
        + "\">\n"
        + "</i>";

const finalCheckCustomer = "<span class=\"text-truncate align-middle\">\n"
        + "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\n"
        + "stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\n"
        + "class=\"feather feather-user font-medium-3 text-primary me-50\">\n"
        + "<path d=\"M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2\"></path>\n"
        + "<circle cx=\"12\" cy=\"7\" r=\"4\"></circle>\n"
        + "</svg>Customer\n"
        + "</span>";
const widgetUser = {
    handleViewProfile: function (indexUser) {
        $.ajax({
            url: "/Rentabike/roleListController",
            type: "post",
            data: {
                mode: "viewProfile",
                accountID: indexUser
            },
            success: function (response) {
                let splitItem = response.split("/;LGN");
                let status = splitItem[4];
                let role = splitItem[10];
                if (splitItem[2] !== 'null') {
                    $("#accoutAvatar").attr("src", splitItem[2]);
                    $("#accountAvatarAlt").css("display", "none");
                    $("#accoutAvatar").css("display", "");
                } else {
                    let nameSplit = splitItem[1].split(" ");
                    $("#accountAvatarAlt").text(nameSplit[0].substring(0, 1) + nameSplit[1].substring(0, 1));
                    $("#accountAvatarAlt").css("display", "");
                    $("#accoutAvatar").css("display", "none");
                }
                $("#titleDetails").text("Details of " + splitItem[1]);
                $("#userModalID").text("#" + splitItem[0]);
                $("#userModalFullName").text(splitItem[1]);
                $("#userModalEmail").text(splitItem[3]);
                if (status === "0") {
                    if (role === "CUSTOMER" || role === "SALEPERSON") {
                        $("#userModalStatus").html(finalBanned);
                    } else {
                        $("#userModalStatus").html(finalInActive);
                    }
                } else if (status === "1") {
                    $("#userModalStatus").html(finalActive);
                } else if (status === "2") {
                    if (role === "CUSTOMER" || role === "SALEPERSON") {
                        $("#userModalStatus").html(finalInActive);
                    } else {
                        $("#userModalStatus").html(finalBanned);
                    }
                }
                $("#userModalJoin").text(splitItem[5]);
                $("#userModalPhone").text(splitItem[6]);
                $("#userModalCity").text(splitItem[7]);
                $("#userModalStreet").text(splitItem[8]);
                $("#userModalState").text(splitItem[9]);

                if (role === "CUSTOMER" || role === "SALEPERSON") {
                    if (role === "CUSTOMER") {
                        $("#userModalRole").html(finalCheckCustomer);
                    } else {
                        $("#userModalRole").html(finalCheckSalePerson);
                    }
                } else {
                    if (role === "ADMIN") {
                        $("#userModalRole").html(finalCheckAdmin);
                    } else {
                        $("#userModalRole").html(finalCheckStaff);
                    }
                }
            }
        });
    },
    showEntry: function () {
        $("#showEntry").on("change", function () {
            let showEntry = $('#showEntry option:selected').val();
            $.ajax({
                url: "/Rentabike/roleListController",
                type: "post",
                data: {
                    mode: "showEntry",
                    showEntry: showEntry,
                    page: "1"
                },
                success: function (response) {
                    window.location.replace("http://localhost:8088/Rentabike/roleListController?page=1");
                }
            });
        });
    },
    searchStaff: function () {
        $("#searchUser").on("keyup", function () {
            if (this.value.length === 0 || this.value === "") {
                $("#paginationNumber").css("opacity", "1");
                $("#paginationNumber").css("visibility", "visible");
                $("#totalInfo").css("opacity", "1");
                $("#totalInfo").css("visibility", "visible");
            } else {
                $("#paginationNumber").css("opacity", "0");
                $("#paginationNumber").css("visibility", "hidden");
                $("#totalInfo").css("opacity", "0");
                $("#totalInfo").css("visibility", "hidden");
            }
            $.ajax({
                url: "/Rentabike/roleListController",
                type: "post",
                data: {
                    mode: "searchName",
                    inputSearch: this.value
                },
                success: function (response) {
                    if (response !== "RESET") {
                        $("#lidget__list-user").html(response);
                    } else {
                        $("#lidget__list-user").html("");
                        $("#lidget__list-user").html(originalHtml);
                    }
                }
            });
        });
    },
    selectRole: function () {
        $("#UserRole").on("change", function () {
            let showValue = $('#UserRole option:selected').val();
            if (showValue.length === 0 || showValue === "") {
                $("#paginationNumber").css("opacity", "1");
                $("#paginationNumber").css("visibility", "visible");
                $("#totalInfo").css("opacity", "1");
                $("#totalInfo").css("visibility", "visible");
            } else {
                $("#paginationNumber").css("opacity", "0");
                $("#paginationNumber").css("visibility", "hidden");
                $("#totalInfo").css("opacity", "0");
                $("#totalInfo").css("visibility", "hidden");
            }
            $.ajax({
                url: "/Rentabike/roleListController",
                type: "post",
                data: {
                    mode: "selectRole",
                    selectOption: showValue
                },
                success: function (response) {
                    if (response !== "RESET") {
                        $("#lidget__list-user").html(response);
                    } else {
                        $("#lidget__list-user").html("");
                        $("#lidget__list-user").html(originalHtml);
                    }
                }
            });
        });
    },
    handleSorting: function () {
        let ds = document.querySelectorAll(".ds-s");
        for (var item of ds) {
            item.onclick = function () {
                let sortContent = this.getAttribute("id");
                switching = true;
                if (this.classList.contains("non-sorting")) {
                    this.classList.remove("sorting_desc", "non-sorting");
                    this.classList.add("sorting_asc");
                    switchTableAsc(sortContent);
                } else if (this.classList.contains("sorting_asc")) {
                    this.classList.remove("sorting_asc", "non-sorting");
                    this.classList.add("sorting_desc");
                    switchTableDesc(sortContent);
                } else {
                    this.classList.remove("sorting_asc", "sorting_desc");
                    this.classList.add("non-sorting");
                    $("#lidget__list-user").html(originalHtml);
                }
            };
        }
    },
    showDetailsModal: function (indexUser, title) {
        let roleElement, nameElement, emailElement;
        if (title === "suspended-account") {
            roleElement = "#role-suspended";
            emailElement = "#email-suspended";
            nameElement = "#name-suspended";
        } else if (title === "unlock-account") {
            roleElement = "#role-unlock";
            emailElement = "#email-unlock";
            nameElement = "#name-unlock";
        }
        $.ajax({
            url: "/Rentabike/roleListController",
            type: "post",
            data: {
                mode: "viewProfile",
                accountID: indexUser
            },
            success: function (response) {
                let splitItem = response.split("/;LGN");
                let role = splitItem[10];
                if (role === "CUSTOMER" || role === "SALEPERSON") {
                    if (role === "CUSTOMER") {
                        $(roleElement).html(finalCheckCustomer);
                    } else {
                        $(roleElement).html(finalCheckSalePerson);
                    }
                } else {
                    if (role === "ADMIN") {
                        $(roleElement).html(finalCheckAdmin);
                    } else {
                        $(roleElement).html(finalCheckStaff);
                    }
                }
                $(nameElement).text(splitItem[1]);
                $(emailElement).text(splitItem[3]);
                $(emailElement).attr("data-index", splitItem[0]);
            }
        });
        $("#bannedUpdatePermission").submit(function (event) {
            event.preventDefault();
            let currentPage = $("#paginationNumber").attr("currentIndex");
            let statusSend = true;
            toastr.success("Wait for few seconds, we try to send email to this user", "Process Success!", {
                closeButton: !0,
                tapToDismiss: !1,
                rtl: !1
            });
            $.ajax({
                url: "/Rentabike/roleListController",
                type: "post",
                data: {
                    mode: "handleUserStatus",
                    switch : "banned",
                    accountID: $("#email-suspended").attr("data-index"),
                    duration: $('#DurationSelect option:selected').val(),
                    message: $('#bannedMessage').val()
                },
                success: function (response) {
                    if (response === "SUCCESS") {
                        if (statusSend) {
                            toastr.success("User had been banned, a message will be send to this user. The system will be reset automatically in 5 seconds to load new data", "Process Success!", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                            $("#suspended-close").click();
                            setTimeout(() => {
                                window.location.replace("http://localhost:8088/Rentabike/roleListController?page=" + currentPage);
                            }, 5000);
                            statusSend = false;
                        }
                    } else {
                        if (statusSend) {
                            toastr.error("Refresh your page and try again", "There are some error expected.", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                            statusSend = false;
                        }
                    }
                }
            });
        });
        $("#unlockUpdatePermission").submit(function (event) {
            event.preventDefault();
            let currentPage = $("#paginationNumber").attr("currentIndex");
            let statusSend = true;
            toastr.success("Wait for few seconds, we try to send email to this user", "Process Success!", {
                closeButton: !0,
                tapToDismiss: !1,
                rtl: !1
            });
            $.ajax({
                url: "/Rentabike/roleListController",
                type: "post",
                data: {
                    mode: "handleUserStatus",
                    switch : "unlock",
                    accountID: $("#role-data").attr("data-index"),
                    message: $('#unlockMessage').val()
                },
                success: function (response) {
                    if (response === "SUCCESS") {
                        if (statusSend) {
                            toastr.success("User had been unlocked, a message will be send to this user. The system will be reset automatically in 5 seconds to load new data", "Process Success!", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                            $("#unlock-close").click();
                            setTimeout(() => {
                                window.location.replace("http://localhost:8088/Rentabike/roleListController?page=" + currentPage);
                            }, 5000);
                            statusSend = false;
                        }
                    } else {
                        if (statusSend) {
                            toastr.error("Refresh your page and try again", "There are some error expected.", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: !1
                            });
                            statusSend = false;
                        }
                    }
                }
            });
        });
    },
    updatePermissionRole: function () {
        $("#addRoleForm").submit(function (event) {
            event.preventDefault();
            let currentPage = $("#paginationNumber").attr("currentIndex");
            let statusSend = true;
            let roleSelected;
            let roleLidgetBox = document.querySelectorAll(".lidget__role-permission");
            for (let role of roleLidgetBox) {
                if (role.checked) {
                    roleSelected = role.getAttribute("data-attr");
                }
            }
            if (!statusChanged) {
                $("#myModalLabel135").text("Change Role Status");
                $("#resetChanged").css("display", "");
                if (($("#role-set").attr("data-attr") === "STAFF" || $("#role-set").attr("data-attr") === "ADMIN") && (roleSelected === "Customer" || roleSelected === "Saleperson")) {
                    if ($(".user-name").attr("data-attr") === $("#role-data").attr("data-attr")) {
                        $(".danger_modal-allowed-text").text("The role of your account is currently '" + $("#role-set").attr("data-attr") + "', if you change your account status role to 'USER ( " + roleSelected + ")', and you was posted some product to our system, we might move their data to a new one. Think carefully before doing this. You will be logout from this account after this process finish.");
                    } else {
                        $(".danger_modal-allowed-text").text("The role of this account is currently '" + $("#role-set").attr("data-attr") + "', if you change this account to 'USER ( " + roleSelected + ")' and they are sell some products, we can convert their data but if will not totally completed. Think carefully before doing this.");
                    }
                    $(".lidget-accept-button").attr("status", "allowed");
                    $("#danger_modal").css("z-index", "9999");
                    $("#danger_modal").css("background-color", "#000000a8");
                    $("#danger_modal").modal("show");
                    acceptButton = $("#acceptChanged");
                    return;
                } else if (($("#role-set").attr("data-attr") === "CUSTOMER" || $("#role-set").attr("data-attr") === "SALEPERSON") && (roleSelected !== "Customer" || roleSelected !== "Saleperson")) {
                    $(".danger_modal-allowed-text").text("The role of this account is currently '" + $("#role-set").attr("data-attr") + "', if you change this account to 'STAFF ( " + roleSelected + ")', not only the achievements but the changes that this account has made will be completely deleted. Think carefully before doing this.");
                    $(".lidget-accept-button").attr("status", "allowed");
                    $("#danger_modal").css("z-index", "9999");
                    $("#danger_modal").css("background-color", "#000000a8");
                    $("#danger_modal").modal("show");
                    acceptButton = $("#acceptChanged");
                    return;
                } else {
                    statusChanged = true;
                }
            }
            toastr.success("Wait for few seconds, we try to send email to this user", "Process Success!", {
                closeButton: !0,
                tapToDismiss: !1,
                rtl: !1
            });
            if (statusChanged) {
                $.ajax({
                    url: "/Rentabike/roleListController",
                    type: "post",
                    data: {
                        mode: "updateRole",
                        accountID: $("#role-data").attr("data-attr"),
                        allowedChanged: allowedChanged,
                        roleSeleceted: roleSelected
                    },
                    success: function (response) {
                        if (response === "SUCCESS") {
                            if (statusSend) {
                                toastr.success("Account had been update role completed, a message will be send to this user. The system will be reset automatically in 3 seconds to load new data", "Process Success!", {
                                    closeButton: !0,
                                    tapToDismiss: !1,
                                    rtl: !1
                                });
                                $("#update-permission-close").click();
                                setTimeout(() => {
                                    window.location.replace("http://localhost:8088/Rentabike/roleListController?page=" + currentPage);
                                }, 3000);
                                statusSend = false;
                            }
                        } else {
                            if (statusSend) {
                                toastr.error("Refresh your page and try again", "There are some error expected.", {
                                    closeButton: !0,
                                    tapToDismiss: !1,
                                    rtl: !1
                                });
                                statusSend = false;
                            }
                        }
                    }
                });
            }
        });
        $("#acceptChanged").on("click", function () {
            if (this.getAttribute("status") === "allowed") {
                statusChanged = true;
                allowedChanged = true;
                $("#resetChanged").css("display", "none");
            }
        });
        $("#resetChanged").on("click", function () {
            $("#myModalLabel135").text("Permission Limited");
            $("#danger_modal").css("z-index", "");
            $("#danger_modal").css("background-color", "");
            $(".lidget-accept-button").attr("id", "");
            $(".lidget-accept-button").attr("status", "denied");
            $("#resetChanged").css("display", "none");
        });
    },
    viewRoleAccount: function (index) {
        $.ajax({
            url: "/Rentabike/roleListController",
            type: "post",
            data: {
                mode: "viewRole",
                accountID: index
            },
            success: function (response) {
                let roleLidgetBox = document.querySelectorAll(".lidget__role-permission");
                console.log(response);
                for (let role of roleLidgetBox) {
                    if (role.getAttribute("data-attr") === response) {
                        role.checked = true;
                        break;
                    }
                }
            }
        });
    },
    start: function () {
        this.searchStaff();
        this.selectRole();
        this.handleSorting();
        this.showEntry();
        this.updatePermissionRole();
    }
};
widgetUser.start();

function sendParamterHandler(param) {
    let indexUser = param.getAttribute("index");
    let roleSelected = param.getAttribute("current-action");
    let title = param.getAttribute("data-title");
    let userStatus = document.querySelector(".user-status");
    let userID = document.querySelector(".user-name");
    $("#danger_modal").css("z-index", "");
    $("#danger_modal").css("background-color", "");
    $(".lidget-accept-button").attr("id", "");
    $(".lidget-accept-button").attr("status", "denied");
    $("#myModalLabel135").text("Permission Limited");
    $("#resetChanged").css("display", "none");
    statusChanged = false;
    allowedChanged = false;
    console.log(userStatus.innerHTML);
    console.log(roleSelected);

    if (userStatus.innerHTML === "ADMIN" || userStatus.getAttribute("data") === "User Management" || userStatus.getAttribute("data") === "User Manager") {
        if ((/*userStatus.innerHTML === roleSelected ||*/ roleSelected === "ADMIN") && indexUser !== userID.getAttribute("data-attr")) {
            $(".danger_modal-allowed-text").text("Sorry, but you not allowed to change status of account with the same of permission or higher permission");
            $("#danger_modal").modal("show");
        } else {
            if (title === "suspended-account") {
                if (indexUser === userID.getAttribute("data-attr")) {
                    $(".danger_modal-allowed-text").text("Sorry, but you are not allowed to banned yourself.");
                    $("#danger_modal").modal("show");
                } else {
                    widgetUser.showDetailsModal(indexUser, title);
                    $('#suspendedUser').modal("show");
                }
            } else if (title === "edit-role-account") {
                $("#role-set").attr("data-attr", roleSelected);
                $("#role-data").attr("data-attr", indexUser);
                widgetUser.viewRoleAccount(indexUser);
                $("#addRoleModal").modal("show");
            } else if (title === "unlock-account") {
                widgetUser.showDetailsModal(indexUser, title);
                $('#unlockUser').modal("show");
            }
        }
    } else {
        $(".danger_modal-allowed-text").text("Sorry, but only people with the admin role and staff with user management permission can do this. Try contacting the admins if you want to do this.");
        $("#danger_modal").modal("show");
    }
}

function sendParamterView(param) {
    let indexUser = param.getAttribute("index");
    widgetUser.handleViewProfile(indexUser);
}

function switchTableAsc(sortContent) {
    let table, rows, switching, i, x, y, shouldSwitch;
    table = document.querySelector("#tableUserRole");
    switching = true;
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            if (sortContent === "sorting-name") {
                x = rows[i].getElementsByTagName("TD")[1].lastElementChild.firstElementChild.children[0];
                y = rows[i + 1].getElementsByTagName("TD")[1].lastElementChild.firstElementChild.children[0];
            } else if (sortContent === "sorting-role") {
                x = rows[i].getElementsByTagName("TD")[2].children[0].children[1];
                y = rows[i + 1].getElementsByTagName("TD")[2].children[0].children[1];
            } else if (sortContent === "sorting-date-join") {
                x = rows[i].getElementsByTagName("TD")[3];
                y = rows[i + 1].getElementsByTagName("TD")[3];
            } else if (sortContent === "sorting-permisson") {
                x = rows[i].getElementsByTagName("TD")[4];
                y = rows[i + 1].getElementsByTagName("TD")[4];
            } else if (sortContent === "sorting-permission") {
                x = rows[i].getElementsByTagName("TD")[5].children[0];
                y = rows[i + 1].getElementsByTagName("TD")[5].children[0];
            }
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

function switchTableDesc(sortContent) {
    let table, rows, switching, i, x, y, shouldSwitch;
    table = document.querySelector("#tableUserRole");
    switching = true;
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            if (sortContent === "sorting-name") {
                x = rows[i].getElementsByTagName("TD")[1].lastElementChild.firstElementChild.children[0];
                y = rows[i + 1].getElementsByTagName("TD")[1].lastElementChild.firstElementChild.children[0];
            } else if (sortContent === "sorting-role") {
                x = rows[i].getElementsByTagName("TD")[2].children[0].children[1];
                y = rows[i + 1].getElementsByTagName("TD")[2].children[0].children[1];
            } else if (sortContent === "sorting-date-join") {
                x = rows[i].getElementsByTagName("TD")[3];
                y = rows[i + 1].getElementsByTagName("TD")[3];
            } else if (sortContent === "sorting-permisson") {
                x = rows[i].getElementsByTagName("TD")[4];
                y = rows[i + 1].getElementsByTagName("TD")[4];
            } else if (sortContent === "sorting-status") {
                x = rows[i].getElementsByTagName("TD")[5].children[0];
                y = rows[i + 1].getElementsByTagName("TD")[5].children[0];
            }
            if (y.innerHTML.toLowerCase() > x.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}