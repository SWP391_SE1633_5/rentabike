let _$ = (selector) => document.querySelector(selector);

let processDistanceDate = (update, start)=>{
    let result;
    let d1 = Date.parse(update||start)
    let d2 = new Date()
    let diffTime = Math.abs(d1 - d2);
    let diffMinutes = Math.floor(diffTime / (1000 * 60)); 
    let diffHours = Math.floor(diffTime / (1000 * 60 * 60)); 
    let diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24)); 
    let diffMoths = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 30)); 
    let diffYears = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 365)); 
    if(diffMinutes < 1){
        result = 'Just now!'
    } else if (diffMinutes>=1 && diffHours<1){
        result = `${diffMinutes} minutes ago`
    } else if (diffHours>=1 && diffDays<1){
        result = `${diffHours} hours ago`
    } else if (diffDays>=1 && diffMoths<1){
        result = `${diffDays} days ago`
    } else if (diffMoths>=1 && diffYears<1){
        result = `${diffMoths} moths ago`
    }else if (diffYears>=1)
        result = `${diffYears} years ago`
    return `${result}${update==null ? '' : ' (has been updated)'}`
}

const BlogAccount = ()=>{
    let dataRPOnLoad
    $(window).on('load', () => {
        let url_string = document.URL;
        let url = new URL(url_string);
        let accountId = url.searchParams.get("accountId");
        $.ajax({
            url: `http://localhost:8088/Rentabike/blog-account?accountId=${accountId}&type=getAll`,
            type: 'get',
            data: '',
            success: function (data) {
                dataRPOnLoad = JSON.parse(data)
                //data
                _$('.user-profile h2').innerHTML = `${dataRPOnLoad.Account.firstName + ' ' + dataRPOnLoad.Account.lastName}`
                _$('.blog-posted__list').innerHTML = (()=>{
                    if(dataRPOnLoad.BlogHasPost.length==0){
                        return `<div class="nothing-here">
                                    <img src="./assets/img/empty.png" alt="">
                                    <h4>No Blog Has Been Posted</h4>
                                </div>`
                    }
                    return dataRPOnLoad.BlogHasPost.map((cur, index)=>{
                        return `<div class="blog-posted__item">
                                    <a href="${`http://localhost:8088/Rentabike/blog-detail?id=${cur.id}`}"><img src="${cur.image || './assets/img/slider/slider15.jpg'}" alt="mtb"></a>
                                    <div class="item-content__wrapper">
                                        <a href="${`http://localhost:8088/Rentabike/blog-detail?id=${cur.id}`}"><h3 class="itemt-content__header">${cur.blogTitle}</h3></a>
                                        <p class="item-content__detail">${cur.blogDescription}</p>
                                    </div>
                                </div>`
                    }).join('');
                })()
                _$('.time-join').innerHTML = processDistanceDate(null, dataRPOnLoad.Account.dateOfJoin)
                _$('.recent-work__list').innerHTML = (()=>{
                    if(dataRPOnLoad.CmtRecentInfoList.length==0){
                        return `<div class="nothing-here">
                                    <img src="./assets/img/empty.png" alt="">
                                    <h4>No Recent Work Has Been Found</h4>
                                </div>`
                    }
                    return dataRPOnLoad.CmtRecentInfoList.map((cur, index)=>{
                        if(cur.accountReacted){
                            return `<div class="recent-work__item">
                                        <img class="avatar" src="${dataRPOnLoad.Account.avatar || './assets/img/avatar.jpg'}" alt="avatar">
                                        <div class="item-detail__wrapper">
                                            <div class="item-detail">
                                                <a href="#" class="own-account">${dataRPOnLoad.Account.firstName + ' ' + dataRPOnLoad.Account.lastName}</a>
                                                <img src="${cur.reactType.url}" alt="" data-id="${cur.reactType.id}">
                                                <span>expressed his feelings about comment of</span>
                                                ${(()=>{
                                                    if(dataRPOnLoad.Account.accountID==cur.accountReacted.accountID)
                                                        return `<a href="#" class="other-account">themself</a>`
                                                    return `<a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${cur.accountReacted.accountID}`}" class="other-account">${cur.accountReacted.firstName + ' ' + cur.accountReacted.lastName}: </a>`
                                                })()}
                                                <a href="${`http://localhost:8088/Rentabike/blog-detail?id=${cur.blogCmt.blogId}`}" class="cmt-content">"${cur.blogCmt.content}"</a>
                                            </div>
                                        </div>
                                    </div>`
                        }else if(cur.accountReplied){
                            return `<div class="recent-work__item">
                                        <img class="avatar" src="${dataRPOnLoad.Account.avatar || './assets/img/avatar.jpg'}" alt="avatar">
                                        <div class="item-detail__wrapper">
                                            <div class="item-detail">
                                                <a href="#" class="own-account">${dataRPOnLoad.Account.firstName + ' ' + dataRPOnLoad.Account.lastName}</a>
                                                <span>replied to comment of</span>
                                                ${(()=>{
                                                    if(dataRPOnLoad.Account.accountID==cur.accountReplied.accountID)
                                                        return `<a href="#" class="other-account">themself</a>`
                                                    return `<a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${cur.accountReplied.accountID}`}" class="other-account">${cur.accountReplied.firstName + ' ' + cur.accountReplied.lastName}: </a>`
                                                })()}
                                                <a href="${`http://localhost:8088/Rentabike/blog-detail?id=${cur.blogCmt.blogId}`}" class="cmt-content">"${cur.blogCmt.content}"</a>
                                            </div>
                                        </div>
                                    </div>`
                        }else {
                            return `<div class="recent-work__item">
                                        <img class="avatar" src="${dataRPOnLoad.Account.avatar || './assets/img/avatar.jpg'}" alt="avatar">
                                        <div class="item-detail__wrapper">
                                            <div class="item-detail">
                                                <a href="#" class="own-account">${dataRPOnLoad.Account.firstName + ' ' + dataRPOnLoad.Account.lastName}</a>
                                                <span>commented on the post with content:</span>
                                                <a href="${`http://localhost:8088/Rentabike/blog-detail?id=${cur.blogCmt.blogId}`}" class="cmt-content">"${cur.blogCmt.content}"</a>
                                            </div>
                                        </div>
                                    </div>`
                        }
                    }).join('');
                })()
                //display page
                _$('#loading-page').classList.add('d-none')
                _$('.custome-container').classList.remove('d-none')
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })
}

BlogAccount()