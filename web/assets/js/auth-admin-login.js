/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


$(window).on("load", (function () {
    "use strict";
    var ha = document.querySelector(".status-login");
    if (ha.getAttribute("data") === "true") {
        var e, o, t, r, a, s = "#ebf0f7",
                i = "#5e5873",
                n = "#ebe9f1",
                d = document.querySelector("#gained-chart"),
                l = document.querySelector("#order-chart"),
                h = document.querySelector("#avg-sessions-chart"),
                p = document.querySelector("#support-trackers-chart"),
                c = document.querySelector("#sales-visit-chart"),
                w = "rtl" === $("html").attr("data-textdirection");
        setTimeout((function () {
            toastr.success("You have successfully sign up to Hilfsmotor Administrator.<br><br> Now you can start to explore as a staff in our team! Contact to admin to know more about our website", "👋 Welcome " + ha.innerHTML + "!", {
                closeButton: !0,
                tapToDismiss: !1,
                rtl: w
            });
        }));
    }
}));