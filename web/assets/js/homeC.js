/*=====================feedback-variable=====================*/
const leftController = document.querySelector(".Feedback-page__control[type='leftControl']");
const rightController = document.querySelector(".Feedback-page__control[type='rightControl']");
const FeedbackItems = document.querySelectorAll(".Feedback-page__item");
const FeedbackList = document.querySelector(".Feedback-page__list");
/*=====================rental-variable=====================*/
const rentalItems = document.querySelectorAll(".rent-page__item");
const rentalList = document.querySelector(".rent-page__list");
const leftControllerRental = document.querySelector(".rent-page__controller-arrow[mode='leftArrow']");
const rightControllerRental = document.querySelector(".rent-page__controller-arrow[mode='rightArrow']");
/*=====================sell-variable=====================*/
const sellItems = document.querySelectorAll(".sell-page__item");
const sellList = document.querySelector(".sell-page__list");
const leftControllerSell = document.querySelector(".sell-page__controller-arrow[mode='leftArrow']");
const rightControllerSell = document.querySelector(".sell-page__controller-arrow[mode='rightArrow']");
/*=====================rental-variable=====================*/
const userModel = document.querySelector(".home-page__navbar-item[role='userLogin']");
const userNavbar = document.querySelector(".home-page__user-navbar");
const userIconArrow = document.querySelector(".home-page__user-alt");
/*=====================Home-page(Customer)=====================*/
const widget = {
    sliderFeedBack: function () {
        let numberOfPages = FeedbackItems.length;
        let limitPage = parseInt(FeedbackList.style.getPropertyValue('--per-page'));
        leftController.onclick = function () {
            let indexPage = parseInt(FeedbackList.style.getPropertyValue('--slide-to-index'));
            let availablePage = parseInt(FeedbackList.style.getPropertyValue('--show-page'));
            let totalPage = parseInt(availablePage - limitPage);
            let multiplePage = parseInt(numberOfPages / limitPage);
            let movePage = parseInt(indexPage - 1);
            let lastPage = numberOfPages % 3 === 0 ? multiplePage - 1 : multiplePage;
            if (totalPage < 0) {
                indexPage = FeedbackList.style.setProperty('--slide-to-index', lastPage);
                availablePage = FeedbackList.style.setProperty('--show-page', parseInt(multiplePage + limitPage));
            } else {
                indexPage = FeedbackList.style.setProperty('--slide-to-index', movePage);
                availablePage = FeedbackList.style.setProperty('--show-page', totalPage);
            }
        };
        rightController.onclick = function () {
            let indexPage = parseInt(FeedbackList.style.getPropertyValue('--slide-to-index'));
            let availablePage = parseInt(FeedbackList.style.getPropertyValue('--show-page'));
            let totalPage = parseInt(availablePage + limitPage);
            let movePage = parseInt(indexPage + 1);
            if (totalPage >= numberOfPages) {
                indexPage = FeedbackList.style.setProperty('--slide-to-index', 0);
                availablePage = FeedbackList.style.setProperty('--show-page', 0);
            } else {
                indexPage = FeedbackList.style.setProperty('--slide-to-index', movePage);
                availablePage = FeedbackList.style.setProperty('--show-page', totalPage);
            }
        };
    },
    sliderRentals: function () {
        let indexShowPage = document.querySelector(".rent-page__index-slider-page");
        let totalShowPage = document.querySelector(".rent-page__total-slider-page");
        let numberOfPages = rentalItems.length;
        let limitPage = parseInt(rentalList.style.getPropertyValue('--per-page'));
        totalShowPage.innerHTML = numberOfPages;
        indexShowPage.innerHTML = 1;
        leftControllerRental.onclick = function () {
            let indexPage = parseInt(rentalList.style.getPropertyValue('--slide-to-index'));
            let availablePage = parseInt(rentalList.style.getPropertyValue('--show-page'));
            let totalPage = parseInt(availablePage - limitPage);
            let multiplePage = parseInt(numberOfPages / limitPage);
            let movePage = parseInt(indexPage - 1);
            let lastPage = numberOfPages % 1 === 0 ? multiplePage - 1 : multiplePage;
            console.log(totalPage);
            if (totalPage < 0) {
                indexPage = rentalList.style.setProperty('--slide-to-index', lastPage);
                availablePage = rentalList.style.setProperty('--show-page', parseInt(numberOfPages - 1));
            } else {
                indexPage = rentalList.style.setProperty('--slide-to-index', movePage);
                availablePage = rentalList.style.setProperty('--show-page', totalPage);
            }
            indexShowPage.innerHTML = parseInt(rentalList.style.getPropertyValue('--slide-to-index')) + 1;
        };
        rightControllerRental.onclick = function () {
            let indexPage = parseInt(rentalList.style.getPropertyValue('--slide-to-index'));
            let availablePage = parseInt(rentalList.style.getPropertyValue('--show-page'));
            let totalPage = parseInt(availablePage + limitPage);
            let movePage = parseInt(indexPage + 1);
            if (totalPage >= numberOfPages) {
                indexPage = rentalList.style.setProperty('--slide-to-index', 0);
                availablePage = rentalList.style.setProperty('--show-page', 0);
            } else {
                indexPage = rentalList.style.setProperty('--slide-to-index', movePage);
                availablePage = rentalList.style.setProperty('--show-page', totalPage);
            }
            indexShowPage.innerHTML = parseInt(rentalList.style.getPropertyValue('--slide-to-index')) + 1;
        };
    },
    sliderSell: function () {
        let indexShowPage = document.querySelector(".sell-page__index-slider-page");
        let totalShowPage = document.querySelector(".sell-page__total-slider-page");
        let numberOfPages = sellItems.length;
        let limitPage = parseInt(sellList.style.getPropertyValue('--per-page'));
        totalShowPage.innerHTML = numberOfPages;
        indexShowPage.innerHTML = 1;
        leftControllerSell.onclick = function () {
            let indexPage = parseInt(sellList.style.getPropertyValue('--slide-to-index'));
            let availablePage = parseInt(sellList.style.getPropertyValue('--show-page'));
            let totalPage = parseInt(availablePage - limitPage);
            let multiplePage = parseInt(numberOfPages / limitPage);
            let movePage = parseInt(indexPage - 1);
            let lastPage = numberOfPages % 1 === 0 ? multiplePage - 1 : multiplePage;
            console.log(totalPage);
            if (totalPage < 0) {
                indexPage = sellList.style.setProperty('--slide-to-index', lastPage);
                availablePage = sellList.style.setProperty('--show-page', parseInt(numberOfPages - 1));
            } else {
                indexPage = sellList.style.setProperty('--slide-to-index', movePage);
                availablePage = sellList.style.setProperty('--show-page', totalPage);
            }
            indexShowPage.innerHTML = parseInt(sellList.style.getPropertyValue('--slide-to-index')) + 1;
        };
        rightControllerSell.onclick = function () {
            let indexPage = parseInt(sellList.style.getPropertyValue('--slide-to-index'));
            let availablePage = parseInt(sellList.style.getPropertyValue('--show-page'));
            let totalPage = parseInt(availablePage + limitPage);
            let movePage = parseInt(indexPage + 1);
            if (totalPage >= numberOfPages) {
                indexPage = sellList.style.setProperty('--slide-to-index', 0);
                availablePage = sellList.style.setProperty('--show-page', 0);
            } else {
                indexPage = sellList.style.setProperty('--slide-to-index', movePage);
                availablePage = sellList.style.setProperty('--show-page', totalPage);
            }
            indexShowPage.innerHTML = parseInt(sellList.style.getPropertyValue('--slide-to-index')) + 1;
        };
    },
    pageLoad: function () {
        window.onload = function () {
            $.ajax({
                url: "/Rentabike/rememberMeController",
                type: "post",
                success: function (response) {}
            });
        };
    },
    navbarUser: function () {
        try {
            userModel.onclick = function () {
                if (userNavbar.style.height === "0rem" || userNavbar.style.height === "") {
                    userNavbar.style.height = "16rem";
                    userIconArrow.style.transform = "rotate(90deg)";
                    userNavbar.style.boxShadow = "0px 0px 4px 1px var(--black-bg-color)";
                } else {
                    userNavbar.style.height = "";
                    setTimeout(() => {
                        userNavbar.style.boxShadow = "unset";
                    }, 1000);
                    userIconArrow.style.transform = "";
                }
            };
        } catch (Exception) {
        }
    },
    scrollPage: function () {
        let homePageNavbar = document.querySelector(".home-page__header-navbar");
        window.onscroll = function () {
            let sizeScroll = window.scrollY;
            if (sizeScroll >= 300) {
                homePageNavbar.style.backgroundColor = "var(--black-bg-light-color)";
            } else {
                homePageNavbar.style.backgroundColor = "";
            }
        };
    },
    start: function () {
        this.sliderFeedBack();
        this.sliderRentals();
        this.sliderSell();
        this.navbarUser();
        this.pageLoad();
        this.scrollPage();
    }
};
widget.start();
