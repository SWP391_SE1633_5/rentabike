let _$ = (selector) => document.querySelector(selector)
let toastElement = document.querySelector('.toast');
let toast = new bootstrap.Toast(toastElement)
let toastBody = document.querySelector('.toast-body');
let errorMess='';
//handle event toast
toastElement.addEventListener('show.bs.toast', () => {
    toastBody.innerHTML = errorMess
})

let dataRP
let btnUsefulsAsk = document.querySelector('.help-useful.help-ask')
let btnUsefulsResult = document.querySelector('.help-useful.result')

function handleClickQuestion(thisElement) {
    let helpContent = document.querySelector('.help-content')
    let helpTitleAnswer = document.querySelector('.help-title')
    let index = thisElement.dataset.index
    let quesList = thisElement.closest('.help-list').querySelectorAll('li')
    for (let item of quesList) {
        if (item.classList.contains('clicked')) item.classList.remove('clicked')
    }
    thisElement.classList.add('clicked')

    // helpContent
    helpContent.innerHTML = dataRP.Data[index].answer
    helpTitleAnswer.innerHTML = dataRP.Data[index].question
    btnUsefulsAsk.classList.remove('hide')
    btnUsefulsResult.classList.add('hide')
}
const handleInputTextArea = () => {
    let inputTextArea = _$('.more-ques textarea');
    let countDetail = _$('.count-char__detail');

    inputTextArea.addEventListener('input',(e)=>{
        let count = e.target.value.length
        if(count>200){
            inputTextArea.value = e.target.value.substring(0, 200);
        }
        countDetail.innerHTML = inputTextArea.value.length
    })
}
const handleSubmitQues = ()=>{
    let inputTextArea = _$('.more-ques textarea');
    _$('.more-ques button').addEventListener('click', ()=>{
        let dataSend = {
            type: 'add-new',
            content: inputTextArea.value
        }
        if(dataSend.content.trim()===''){
            errorMess='Question is not empty!';
            toast.show();
            return;
        }
        $.ajax({
            url: `http://localhost:8088/Rentabike/assistance-quest`,
            type: 'post',
            data: dataSend,
            beforeSend: ()=>{
                inputTextArea.readOnly = true;
                _$('.more-ques button').classList.add('sending');
            },
            success: function (data) {
                dataRP = JSON.parse(data)
                errorMess = dataRP.ME
                toast.show()
                inputTextArea.readOnly = false
                _$('.more-ques button').classList.remove('sending');
                if(dataRP.SC==1){
                    inputTextArea.value = ''
                    _$('.count-char__detail').innerHTML=0
                }
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })
}
function HelpPage() {
    //-----------show data--------------------
    let helpListWrapper = document.querySelector('.help-list')
    let helpContent = document.querySelector('.help-content')
    let helpTitleAnswer = document.querySelector('.help-title')
    let askUsefull = document.querySelector('.help-ask')
    let btnUsefuls = document.querySelectorAll('.btn-useful')
    let yesUseful = document.querySelector('.Co-result')
    let notUseful = document.querySelector('.Khong-result')
    let addBtn = document.querySelector('.left-side button')
    let delBtn = document.querySelector('.help-action .btn-danger')
    let updBtn = document.querySelector('.help-action .btn-warning')
    let h4HelpPage = document.querySelector('.left-side h4')
    let noQues = document.querySelector('.no-ques')
    let loadingPage = document.querySelector('#loading-page');
    let pageContent = document.querySelector('.custome-container');

    loadingPage.classList.remove('d-none')
    pageContent.classList.add('d-none')

    $(window).on('load', () => {
        let quesList = ''
        $.ajax({
            url: `http://localhost:8088/Rentabike/help-page?type=getAll`,
            type: 'get', //send it through get method
            data: '',
            success: function (data) {
                dataRP = JSON.parse(data)

                dataRP.Data.map((cur, index) => {
                    quesList += `<li data-index="${index}" 
                        data-id="${cur.id}"
                        class="${index === 0 ? 'clicked' : ''}"
                        onclick="handleClickQuestion(this)"
                        >${cur.question}</li>`
                });

                helpListWrapper.innerHTML = quesList
                helpContent.innerHTML = dataRP.Data[0]?.answer ?? ''
                helpTitleAnswer.innerHTML = dataRP.Data[0]?.question ?? ''
                if(dataRP.Role !== 'ADMIN'){
                    addBtn.remove();
                    delBtn.remove();
                    updBtn.remove();
                }
                if(!dataRP.Data[0]){
                    askUsefull.classList.add('d-none')
                    h4HelpPage.classList.add('d-none')
                    noQues.classList.remove('d-none')
                    updBtn.classList.add('d-none')
                    delBtn.classList.add('d-none')
                }
                if(dataRP.Role==null){
                    _$('.more-ques').remove()
                }else {
                    handleInputTextArea();
                    handleSubmitQues();
                }
                loadingPage.classList.add('d-none')
                pageContent.classList.remove('d-none')
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })

    for (let btnUseful of btnUsefuls) {
        btnUseful.addEventListener('click', () => {
            let id = document.querySelector('.help-list li.clicked').dataset.id
            let formData = {}
            let type = btnUseful.dataset.type
            formData = { ...formData, type, id }
            console.log(formData)
            // btnUsefulsAsk.innerHTML = 'loading...'

            $.ajax({
                url: `http://localhost:8088/Rentabike/help-page`,
                type: 'post', //send it through get method
                data: formData,
                success: function (data) {
                    let dataRPResult = JSON.parse(data)
                    let dataUseful = Number(dataRPResult.Data.usefull)
                    let dataNotUseful = Number(dataRPResult.Data.noUsefull)
                    let usefulRate = Math.round(
                        (dataUseful / (dataUseful + dataNotUseful)) * 100
                    )
                    let notUsefulRate = Math.round(
                        (dataNotUseful / (dataUseful + dataNotUseful)) * 100
                    )
                    if(type==0){
                        notUseful.classList.add('choose');
                        yesUseful.classList.remove('choose');
                    }else if(type==1){
                        yesUseful.classList.add('choose');
                        notUseful.classList.remove('choose');
                    }
                    btnUsefulsAsk.classList.add('hide')
                    btnUsefulsResult.classList.remove('hide')
                    // animate with js
                    yesUseful.animate(
                        [{ width: '0%' }, { width: `${usefulRate}%` }],
                        {
                            duration: 800,
                            iterations: 1,
                            fill: 'forwards',
                        }
                    )
                    yesUseful
                        .closest('.result-wrapper')
                        .querySelector('span').innerHTML = `${usefulRate}%`
                    notUseful.animate(
                        [{ width: '0%' }, { width: `${notUsefulRate}%` }],
                        {
                            duration: 800,
                            iterations: 1,
                            fill: 'forwards',
                        }
                    )
                    notUseful
                        .closest('.result-wrapper')
                        .querySelector('span').innerHTML = `${notUsefulRate}%`
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        })
    }

    //---------------add ques, answers --------------------
    let AddEdtiorData
    let addDataBtn = document.querySelector('#AddModal .btn-primary')
    let intputAddQues = document.querySelector('#AddModal #recipient-name')
    let addSpinner = document.querySelector('#AddModal .spinner-border')
    // let initialData = `<figure class="image"><img src="https://scontent.fhan2-5.fna.fbcdn.net/v/t39.30808-6/306772874_4219111231680313_5202255084625731048_n.jpg?stp=dst-jpg_s640x640&amp;_nc_cat=104&amp;ccb=1-7&amp;_nc_sid=730e14&amp;_nc_ohc=2ht1tpihAL4AX8BLe8I&amp;_nc_oc=AQlpYWSsYoIMPNzEhEqasBBjCZ0420A8OPedjAgX2Tw_hkg3VI-3PW31SyU5Q3hgZhnms-CnaJjJIQt4aL2GNWC3&amp;_nc_ht=scontent.fhan2-5.fna&amp;oh=00_AT8qNRcm9nCj0Hr8RkCO-_OvqNgyAP715VoCgctrPqxE_g&amp;oe=632213D3" alt="Có thể là hình ảnh về văn bản"></figure>`
    // ckeditor
    ClassicEditor.create(document.querySelector('#Addeditor'))
        .then((editor) => {
            AddEdtiorData = editor
        })
        .catch((error) => {
            console.error(error)
        })

    addDataBtn.addEventListener('click', () => {
        let submitData = {
            action: 'add',
            question: intputAddQues.value,
            answer: AddEdtiorData.getData(),
        }
        console.log(submitData)
        addSpinner.classList.remove('hide')
        addDataBtn.classList.add('disabled')

        $.ajax({
            url: `http://localhost:8088/Rentabike/help-page`,
            type: 'post', //send it through get method
            data: submitData,
            success: function (data) {
                dataRP = JSON.parse(data)
                console.log(dataRP)
                // addSpinner.classList.add('hide')
                // addDataBtn.classList.remove('disabled')
                
                if(dataRP.SC && dataRP.SC===1){
                    location.reload();
                }
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })

    //---------------update ques, answers --------------------
    let UpdEdtiorData
    let updDataBtn = document.querySelector('#UpdModal .btn-primary')
    let intputUpdQues = document.querySelector('#UpdModal #recipient-name')
    let updSpinner = document.querySelector('#UpdModal .spinner-border')
    let initialData

    updBtn.addEventListener('click', () => {
        let helpListItems = document.querySelectorAll('.help-list li')
        let currentIndex;
        for(let helpListItem of helpListItems) {
            if(helpListItem.classList.contains('clicked')) {
                currentIndex = helpListItem.dataset.index;
                break;
            }
        }
        intputUpdQues.value = dataRP.Data[currentIndex].question
        initialData = dataRP.Data[currentIndex].answer
        
        //remove create yet if exist
        document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners').remove();

        ClassicEditor.create(document.querySelector('#Updeditor'),{initialData:initialData})
            .then((editor) => {
                UpdEdtiorData = editor
            })
            .catch((error) => {
                console.error(error)
            })
    })

    updDataBtn.addEventListener('click', () => {
        let helpListItems = document.querySelectorAll('.help-list li')
        let id;
        for(let helpListItem of helpListItems) {
            if(helpListItem.classList.contains('clicked')) {
                id = helpListItem.dataset.id;
                break;
            }
        }
        let submitData = {
            action: 'update',
            updId: id,
            question: intputUpdQues.value,
            answer: UpdEdtiorData.getData(),
        }
        console.log(submitData)
        updSpinner.classList.remove('hide')
        updDataBtn.classList.add('disabled')

        $.ajax({
            url: `http://localhost:8088/Rentabike/help-page`,
            type: 'post', //send it through get method
            data: submitData,
            success: function (data) {
                dataRP = JSON.parse(data)
                console.log(dataRP)
                // addSpinner.classList.add('hide')
                // addDataBtn.classList.remove('disabled')
                
                if(dataRP.SC && dataRP.SC===1){
                    location.reload();
                }
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })

    //---------------delete ques, answers --------------------
    let delDataBtn = document.querySelector('#DeleteModal .btn-primary')
    let delSpinner = document.querySelector('#DeleteModal .spinner-border')
    delDataBtn.addEventListener('click', () => {
        let helpListItems = document.querySelectorAll('.help-list li')
        let id;
        for(let helpListItem of helpListItems) {
            if(helpListItem.classList.contains('clicked')) {
                id = helpListItem.dataset.id;
                break;
            }
        }
        let submitData = {
            action: 'delete',
            delId: id,
        }
        console.log(submitData)
        delSpinner.classList.remove('hide')
        delDataBtn.classList.add('disabled')

        $.ajax({
            url: `http://localhost:8088/Rentabike/help-page`,
            type: 'post', //send it through get method
            data: submitData,
            success: function (data) {
                dataRP = JSON.parse(data)
                console.log(dataRP)
                // addSpinner.classList.add('hide')
                // addDataBtn.classList.remove('disabled')
                
                if(dataRP.SC && dataRP.SC===1){
                    location.reload();
                }
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })

    
}

HelpPage()
