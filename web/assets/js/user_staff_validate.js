$((function () {
    var t = $(".new-user-modal"),
            a = $(".add-new-user"),
            s = $(".select2"),
            o = "app-assets/";
    "laravel" === $("body").attr("data-framework") && (o = $("body").attr("data-asset-path"), r = o + "app/user/view/account"), s.each((function () {
        var e = $(this);
        e.wrap('<div class="position-relative"></div>'), e.select2({
            dropdownAutoWidth: !0,
            width: "100%",
            dropdownParent: e.parent()
        })
    })), a.length && (a.validate({
        errorClass: "error",
        rules: {
            "userFullname": {
                required: !0
            },
            "userFirstName": {
                required: !0
            },
            "userLastName": {
                required: !0
            },
            "userContact": {
                required: !0,
                minlength: 10,
                maxlength: 10
            },
            "userEmail": {
                email: true,
                required: !0
            },
            "userCity": {
                required: !0
            },
            "userStreet": {
                required: !0
            },
            "userPassword": {
                required: !0,
                minlength: 8,
                maxlength: 32
            }
        }
    }), a.on("submit", (function (e) {
        var s = a.valid();
        e.preventDefault();
    })));
}));