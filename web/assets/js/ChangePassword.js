function ResetPassword() {
  let formSubmit = document.querySelector(".form-submit");
  let isSubmit = false;
  let btnSubmit = document.querySelector(".submit");
  let spinner = btnSubmit.querySelector(".spinner-border");
  let inputEmail = document.querySelector("input#hiden-email");
  let inputOP = document.querySelector("input#old-password");
  let inputP = document.querySelector("input#password");
  let inputCP = document.querySelector("input#confirm-password");
  let showMess = document.querySelector(".toast.align-items-center");
  const regexPassword = new RegExp("^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$");
  let formData = {};
  let toastElement = document.querySelector('.toast');
  let toast = new bootstrap.Toast(toastElement);
  let toastBody = document.querySelector('.toast-body');
  let errorMess='';
  //handle event toast
  toastElement.addEventListener('show.bs.toast', () => {
      toastBody.innerHTML = errorMess;
  })

  $(window).on('load', () => {

    $.ajax({
      url: "http://localhost:8088/Rentabike/change-password?getEmail=email",
      type: "get", //send it through get method
      data: '',
      success: function (data) {
        let dataP = JSON.parse(data);
        inputEmail.value = dataP.email;
      },
      error: function (xhr) {
        console.log(xhr);
      },
    });
    

    formSubmit.addEventListener("submit", (e) => {
      e.preventDefault();
      if (isSubmit) return;
      isSubmit = true;
      //check password
      if(!regexPassword.test(inputP.value)){
        errorMess = 'Wrong format password!';
        toast.show();
        isSubmit = false;
        return;
      }
  
      formData = {
        ...formData,
        email: inputEmail.value,
        oldPassword: inputOP.value,
        password: inputP.value,
        confirmPassword: inputCP.value,
      };
      console.log(formData);
  
      $.ajax({
        url: "http://localhost:8088/Rentabike/change-password",
        type: "post", //send it through get method
        data: formData,
        beforeSend: function(){
          btnSubmit.classList.add("disabled");
          spinner.classList.remove("hide");
        },
        success: function (data) {
          btnSubmit.classList.remove("disabled");
          spinner.classList.add("hide");
          let dataP = JSON.parse(data);
          console.log(dataP);
          errorMess = dataP.Mess;
          toast.show();
          isSubmit = false;
          if(dataP.SC===0){
            setTimeout(()=>{
              window.location.replace(
                "http://localhost:8088/Rentabike/started.jsp"
              );
            }, 2000);
          }
        },
        error: function (xhr) {
          //Do Something to handle error
          console.log(xhr);
          isSubmit = false;
        },
      });
  
    });
  })

}

ResetPassword();
