const ecomProduct = document.querySelector("#ecommerce-products");
const ecomPagination = document.querySelector("#ecommerce-pagination");
const lidgetButtonScroll = document.querySelector(".lidget-shop-button");
const shopSearch = document.querySelector("#shop-search");
const sortingChoice = document.querySelector("#sortingChoice");
const sortingChoicePost = document.querySelector("#sortingChoice-1");
const multiRangeChoice = document.querySelectorAll(".lidget-form-checkbox");
const clearFilterButton = document.querySelector(".lidget-clear-filter");
const slider = document.getElementById("slider-price");
/*=====================WIDGET-SHOP=====================*/
const widgetShop = {
    handlePagination: function () {
        $('#pagination-slider').twbsPagination({
            totalPages: document.getElementById("auth-shop").getAttribute("data-value"),
            visiblePages: 5,
            hideOnlyOnePage: false,
            onPageClick: function (event, page) {
                lidgetButtonScroll.click();
                ecomProduct.style.opacity = "0";
                ecomPagination.style.opacity = "0";

                setTimeout(() => {
                    ecomProduct.innerHTML = "";
                    $.ajax({
                        url: "/Rentabike/productListController",
                        type: "post",
                        data: {
                            mode: "pagePagination",
                            page: page
                        },
                        success: function (response) {
                            $("#ecommerce-products").html(response);
                        }
                    });
                }, 500);
                setTimeout(() => {
                    ecomProduct.style.opacity = "1";
                    ecomPagination.style.opacity = "1";
                }, 1500);
            }
        });
    },
    searchProduct: function () {
        $('#shop-search').on('keyup', function () {
            lidgetButtonScroll.click();
            ecomProduct.style.opacity = "0";
            ecomPagination.style.opacity = "0";
            setTimeout(() => {
                ecomProduct.innerHTML = "";
                $.ajax({
                    url: "/Rentabike/productListController",
                    type: "post",
                    data: {
                        mode: "searchProduct",
                        inputSearch: this.value
                    },
                    success: function (response) {
                        $("#ecommerce-products").html(response);
                    }
                });
            }, 500);
            setTimeout(() => {
                ecomProduct.style.opacity = "1";
                if (this.value.length === 0 || this.value === "") {
                    ecomPagination.style.opacity = "1";
                    $('#pagination-slider').twbsPagination('show', 1);
                }
            }, 1500);
        });
    },
    selectOptionSort: function () {
        let observer = new MutationObserver(function (mutations) {
            let optionChoice = sortingChoice.innerText;
            ecomProduct.style.opacity = "0";
            ecomPagination.style.opacity = "0";
            setTimeout(() => {
                ecomProduct.innerHTML = "";
                $.ajax({
                    url: "/Rentabike/productListController",
                    type: "post",
                    data: {
                        mode: "sortOption",
                        option: optionChoice
                    },
                    success: function (response) {
                        if (response !== "OUT") {
                            $("#ecommerce-products").html(response);
                            if (response === "All") {
                                $('#pagination-slider').twbsPagination('show', 1);
                            }
                        }
                    }
                });
            }, 500);
            setTimeout(() => {
                ecomProduct.style.opacity = "1";
                if (optionChoice === "All") {
                    ecomPagination.style.opacity = "1";
                }
            }, 1500);
        });
        observer.observe(sortingChoice, {
            attributes: true,
            childList: true,
            characterData: true
        });

        let observer_post = new MutationObserver(function (mutations) {
            let optionChoice = sortingChoicePost.innerText;
            ecomProduct.style.opacity = "0";
            ecomPagination.style.opacity = "0";
            setTimeout(() => {
                ecomProduct.innerHTML = "";
                $.ajax({
                    url: "/Rentabike/productListController",
                    type: "post",
                    data: {
                        mode: "sortOption",
                        option: optionChoice
                    },
                    success: function (response) {
                        if (response !== "OUT") {
                            $("#ecommerce-products").html(response);
                            if (response === "All") {
                                $('#pagination-slider').twbsPagination('show', 1);
                            }
                        }
                    }
                });
            }, 500);
            setTimeout(() => {
                ecomProduct.style.opacity = "1";
                if (optionChoice === "All") {
                    ecomPagination.style.opacity = "1";
                }
            }, 1500);
        });
        observer_post.observe(sortingChoicePost, {
            attributes: true,
            childList: true,
            characterData: true
        });
    },
    MultiRange: function () {
        for (let choice of multiRangeChoice) {
            choice.onclick = function () {
                let valueChoice = this.value;
                ecomProduct.style.opacity = "0";
                ecomPagination.style.opacity = "0";
                setTimeout(() => {
                    ecomProduct.innerHTML = "";
                    $.ajax({
                        url: "/Rentabike/productListController",
                        type: "post",
                        data: {
                            mode: "multiRange",
                            choice: valueChoice
                        },
                        success: function (response) {
                            if (response !== "OUT") {
                                $("#ecommerce-products").html(response);
                                if (response === "All") {
                                    $('#pagination-slider').twbsPagination('show', 1);
                                }
                            }
                        }
                    });
                }, 500);
                setTimeout(() => {
                    ecomProduct.style.opacity = "1";
                    if (valueChoice === "All") {
                        ecomPagination.style.opacity = "1";
                    }
                }, 1500);
            };
        }
    },
    priceSlider: function () {
        let mode = "ltr", e = !1;
        "rtl" === $("html").data("textdirection") && (mode = "rtl"), "rtl" === mode && (e = !0);
        noUiSlider.create(slider, {
            start: [1500, 3500],
            direction: mode,
            connect: !0,
            tooltips: [!0, !0],
            format: wNumb({
                decimals: 0
            }),
            range: {
                min: 51,
                max: 5e3
            }
        });
        let minValue = 1500, maxValue = 3500;
        slider.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                maxValue = values[handle];
            } else {
                minValue = values[handle];
            }
            if (minValue !== 1500 && maxValue !== 3500) {
                ecomProduct.style.opacity = "0";
                ecomPagination.style.opacity = "0";
                setTimeout(() => {
                    ecomProduct.innerHTML = "";
                    $.ajax({
                        url: "/Rentabike/productListController",
                        type: "post",
                        data: {
                            mode: "priceSlider",
                            minValue: minValue,
                            maxValue: maxValue
                        },
                        success: function (response) {
                            if (response !== "OUT") {
                                $("#ecommerce-products").html(response);
                            }
                        }
                    });
                }, 500);
                setTimeout(() => {
                    ecomProduct.style.opacity = "1";
                }, 1500);
            }
        });
    },
    filterNavbar: function () {
        let categoryList = document.querySelectorAll(".categoryList[type='radio']");
        let brandList = document.querySelectorAll(".brandList[type='radio']");
        let modeList = document.querySelectorAll(".modeProduct[type='radio']");

        for (let category of categoryList) {
            category.onclick = function () {
                lidgetButtonScroll.click();
                ecomProduct.style.opacity = "0";
                ecomPagination.style.opacity = "0";
                setTimeout(() => {
                    ecomProduct.innerHTML = "";
                    $.ajax({
                        url: "/Rentabike/productListController",
                        type: "post",
                        data: {
                            mode: "filterNavbar",
                            request: "Category",
                            selectOption: this.value
                        },
                        success: function (response) {
                            if (response !== "OUT") {
                                $("#ecommerce-products").html(response);
                            }
                        }
                    });
                }, 500);
                setTimeout(() => {
                    ecomProduct.style.opacity = "1";
                    if (this.value === "All") {
                        $('#pagination-slider').twbsPagination('show', 1);
                    }
                }, 1500);
            };
        }

        for (let brand of brandList) {
            brand.onclick = function () {
                lidgetButtonScroll.click();
                ecomProduct.style.opacity = "0";
                ecomPagination.style.opacity = "0";
                setTimeout(() => {
                    ecomProduct.innerHTML = "";
                    $.ajax({
                        url: "/Rentabike/productListController",
                        type: "post",
                        data: {
                            mode: "filterNavbar",
                            request: "Brand",
                            selectOption: this.value
                        },
                        success: function (response) {
                            if (response !== "OUT") {
                                $("#ecommerce-products").html(response);
                            }
                        }
                    });
                }, 500);
                setTimeout(() => {
                    ecomProduct.style.opacity = "1";
                    if (this.value === "All") {
                        $('#pagination-slider').twbsPagination('show', 1);
                    }
                }, 1500);
            };
        }

        for (let mode of modeList) {
            mode.onclick = function () {
                lidgetButtonScroll.click();
                ecomProduct.style.opacity = "0";
                ecomPagination.style.opacity = "0";
                setTimeout(() => {
                    ecomProduct.innerHTML = "";
                    $.ajax({
                        url: "/Rentabike/productListController",
                        type: "post",
                        data: {
                            mode: "filterNavbar",
                            request: "Mode",
                            selectOption: this.value
                        },
                        success: function (response) {
                            if (response !== "OUT") {
                                $("#ecommerce-products").html(response);
                            }
                        }
                    });
                }, 500);
                setTimeout(() => {
                    ecomProduct.style.opacity = "1";
                    if (this.value === "All") {
                        $('#pagination-slider').twbsPagination('show', 1);
                    }
                }, 1500);
            };
        }
    },
    addToCheckOut: function (indexProduct, param, email, indexStore) {
        let addCart = $(param);
        let findCart = addCart.find(".add-to-cart");
        let notifyStatus = true;
        if (document.querySelector(".view-in-cart") === null) {
            findCart.text("View In Cart").removeClass("add-to-cart").addClass("view-in-cart"),
                    toastr.success("", "Added Item in Your Cart 🛒", {
                        closeButton: !0,
                        tapToDismiss: !1,
                        rtl: !1
                    });
        $.ajax({
            url: "/Rentabike/productListController",
            type: "post",
            data: {
                mode: "addToCheckOut",
                storeID: indexStore,
                productID: indexProduct
            },
            success: function (response) {
                if (response === "SUCCESS") {
                    $.ajax({
                        url: "/Rentabike/notifyController",
                        type: "post",
                        data: {
                            mode: "notifyRecordAndShow",
                            productID: indexProduct,
                            email: email,
                            setUpNotify: "Checkout"
                        },
                        success: function (response) {
                            if (response !== "FAILED") {
                                $("#notify").html(response);
                            } else {
                                window.location.replace("http://localhost:8088/Rentabike/homepageCController");
                            }
                            if (notifyStatus) {
                                $.ajax({
                                    url: "/Rentabike/notifyController",
                                    type: "post",
                                    data: {
                                        mode: "NotifyTotalHandler",
                                        setUpNotify: "calculateNotify"
                                    },
                                    success: function (response) {
                                        if (response !== "FAILED") {
                                            $("#notifyCalculation").text(response);
                                        }
                                    }
                                });
                                notifyStatus = false;
                            }

                        }
                    });
                } else {
                    toastr.error("", "Failed to add Item in Your Cart 🛒", {
                        closeButton: !0,
                        tapToDismiss: !1,
                        rtl: !1
                    });
                }
            }
        });
        }
    },
    wishListAdd: function (indexProduct, email, param) {
        let addWishList = $(param);
        let switchMode;
        let notifyStatus = true;

        if (addWishList.find("svg").hasClass("text-danger")) {
            addWishList.find("svg").toggleClass("text-danger") &&
                    toastr.error("", "Remove from wishlist ❤️", {
                        closeButton: !0,
                        tapToDismiss: !1,
                        rtl: !1
                    });
            switchMode = "removeWishList";
        } else {
            addWishList.find("svg").toggleClass("text-danger"),
                    addWishList.find("svg").hasClass("text-danger") &&
                    toastr.success("", "Added to wishlist ❤️", {
                        closeButton: !0,
                        tapToDismiss: !1,
                        rtl: !1
                    });
            switchMode = "addWishList";
        }
        $.ajax({
            url: "/Rentabike/productListController",
            type: "post",
            data: {
                mode: "wishListHandle",
                productID: indexProduct,
                email: email,
                modeSwitch: switchMode
            },
            success: function (response) {
                if (response === "FAILED") {
                    window.location.replace("http://localhost:8088/Rentabike/homepageCController");
                } else {
                    $.ajax({
                        url: "/Rentabike/notifyController",
                        type: "post",
                        data: {
                            mode: "notifyRecordAndShow",
                            productID: indexProduct,
                            email: email,
                            setUpNotify: switchMode
                        },
                        success: function (response) {
                            if (response !== "FAILED") {
                                $("#notify").html(response);
                            } else {
                                window.location.replace("http://localhost:8088/Rentabike/homepageCController");
                            }
                            if (notifyStatus) {
                                $.ajax({
                                    url: "/Rentabike/notifyController",
                                    type: "post",
                                    data: {
                                        mode: "NotifyTotalHandler",
                                        setUpNotify: "calculateNotify"
                                    },
                                    success: function (response) {
                                        if (response !== "FAILED") {
                                            $("#notifyCalculation").text(response);
                                        }
                                    }
                                });
                                notifyStatus = false;
                            }
                        }
                    });
                }
            }
        });
    },
    pageOnLoad: function () {
        let notifyStatus = true;
        $(window).on('load', function () {
            $.ajax({
                url: "/Rentabike/notifyController",
                type: "post",
                data: {
                    mode: "showNotifyOnly"
                },
                success: function (response) {
                    if (response !== 'FAILED') {
                        $("#notify").html(response);
                    }
                    if (notifyStatus) {
                        $.ajax({
                            url: "/Rentabike/notifyController",
                            type: "post",
                            data: {
                                mode: "NotifyTotalHandler",
                                setUpNotify: "calculateNotify"
                            },
                            success: function (response) {
                                if (response !== "FAILED") {
                                    $("#notifyCalculation").text(response);
                                }
                            }
                        });
                        notifyStatus = false;
                    }
                }
            });
        });
    },
    clearFilter: function () {
        clearFilterButton.onclick = function () {
            lidgetButtonScroll.click();
            let formCheckInput = document.querySelectorAll(".form-check-input[type='radio']");
            for (let formCheck of formCheckInput) {
                if (formCheck.getAttribute("value") === "All") {
                    formCheck.checked = true;
                }
            }
            $("#mode1").click();
            shopSearch.value = "";
            $("#sortingChoice").text("Sort");
            $("#sortingChoice-1").text("Post");
        };
    },
    start: function () {
        this.handlePagination();
        this.searchProduct();
        this.selectOptionSort();
        this.MultiRange();
        this.priceSlider();
        this.filterNavbar();
        this.clearFilter();
        this.pageOnLoad();
    }
};
widgetShop.start();

function sendToCheckout(param) {
    let email = document.querySelector(".lidget-naz").getAttribute("current-login");
    let indexProduct = param.getAttribute("index-data");
    let indexStoreID = param.getAttribute("index");
    widgetShop.addToCheckOut(indexProduct, param, email, indexStoreID);
}

function sendToWishList(param) {
    let email = document.querySelector(".lidget-naz").getAttribute("current-login");
    let indexProduct = param.getAttribute("index-data");
    widgetShop.wishListAdd(indexProduct, email, param);
}

