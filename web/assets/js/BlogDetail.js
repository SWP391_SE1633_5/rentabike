let _$ = (selector) => document.querySelector(selector);
let toastElement = document.querySelector('.toast');
let toast = new bootstrap.Toast(toastElement)
let toastBody = document.querySelector('.toast-body');
let errorMess='';
let modalShowReact = _$('#showReactModal')
modalShowReact.addEventListener('hidden.bs.modal', ()=>{
    modalShowReact.querySelector('.modal-title span').innerHTML = ``
    modalShowReact.querySelector('.modal-body').innerHTML = `<div class="spinner-grow" role="status">
                                                                <span class="visually-hidden">Loading...</span>
                                                            </div>`
})
//handle event toast
toastElement.addEventListener('show.bs.toast', () => {
    toastBody.innerHTML = errorMess
})
let processDistanceDate = (update, start)=>{
    let result;
    let d1 = Date.parse(update||start)
    let d2 = new Date()
    let diffTime = Math.abs(d1 - d2);
    let diffMinutes = Math.floor(diffTime / (1000 * 60)); 
    let diffHours = Math.floor(diffTime / (1000 * 60 * 60)); 
    let diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24)); 
    let diffMoths = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 30)); 
    let diffYears = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 365)); 
    if(diffMinutes < 1){
        result = 'Just now!'
    } else if (diffMinutes>=1 && diffHours<1){
        result = `${diffMinutes} minutes ago`
    } else if (diffHours>=1 && diffDays<1){
        result = `${diffHours} hours ago`
    } else if (diffDays>=1 && diffMoths<1){
        result = `${diffDays} days ago`
    } else if (diffMoths>=1 && diffYears<1){
        result = `${diffMoths} moths ago`
    }else if (diffYears>=1)
        result = `${diffYears} years ago`
    return `${result}${update==null ? '' : ' (has been updated)'}`
}

let handleTippy = (dataRPOnLoad)=>{
    let isCallingAjaxDelete = false
    let contentReport = `<ul class="tiny-wrapper">
        <li>
            <i class="fa-solid fa-flag"></i>
            <span>Report</span>
        </li>
    </ul>`
    let contentOwner = `<ul class="tiny-wrapper">
        <li>
            <i class="fa-solid fa-pen"></i>
            <span>Update</span>
        </li>
        <li>
            <i class="fa-solid fa-trash"></i>
            <span>Delete</span>
        </li>
    </ul>`
    // create tippy (logic click update cmt)
    tippy('.tippy-target', {
        trigger: 'click',
        content:  (reference) => {
            if(reference.closest('.other-comment__item').dataset.id==dataRPOnLoad?.AuthenAcc?.accountID) 
                return contentOwner;
            return contentReport;
        },
        placement: 'bottom-end',
        allowHTML: true,
        onShown(instance) {
            let elementClicked = instance.reference
            let tippyOpen = instance.popper
            let tippyWrapper = tippyOpen.querySelectorAll('.tiny-wrapper li')
            // console.log(instance)
            //report
            if(tippyWrapper.length===1){
                
                return;
            }
            //update or del
            tippyWrapper[0].addEventListener('click', (e)=>{
                let parenrWrapper = elementClicked.closest('.other-comment__item-wrapper')
                if(!parenrWrapper.querySelector('.comment-update')){
                    parenrWrapper.querySelector('.other-comment__item').style.display = 'none';
                    let cmtUpdateNode = document.createElement('div');
                    cmtUpdateNode.innerHTML = `<div class="your-comment comment-update">
                        <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${dataRPOnLoad.AuthenAcc.accountID}`}">
                            <img src=${dataRPOnLoad.AuthenAcc?.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'} alt="">
                        </a>
                        <div class="comment-input" contenteditable="true">
                            ${parenrWrapper.querySelector('.comment-content').innerHTML}
                        </div>
                        <div class="interact-comments">
                            <button class="cancel-btn">Cancel</button>
                            <button class="post-comment__btn active">Post comment</button>
                        </div>
                    </div>`
                    parenrWrapper.insertBefore(cmtUpdateNode, parenrWrapper.children[0])

                    //handle post or cancel comment btn
                    let updateCmt = _$('.comment-update .comment-input')
                    let updatePostCmt = _$('.comment-update .post-comment__btn')
                    let  cancelUpdateCmt = _$('.comment-update .cancel-btn')
                    updateCmt.addEventListener('input', (e) => {
                        if(updateCmt.textContent===''){
                            e.target.setAttribute('placeholder', 'Write your comment here...');
                            updatePostCmt.classList.remove('active');
                        }
                        else {
                            e.target.removeAttribute('placeholder');
                            updatePostCmt.classList.add('active');
                        }
                    })
                    cancelUpdateCmt.addEventListener('click', () => {
                        cmtUpdateNode.remove();
                        parenrWrapper.querySelector('.other-comment__item').style.display = 'flex';
                    })
                    updatePostCmt.addEventListener('click', ()=>{
                        let updatePostData = {
                            type: 'update',
                            cmtId: updatePostCmt.closest('.other-comment__item-wrapper').dataset.id,
                            content: updatePostCmt.closest('.other-comment__item-wrapper').querySelector('.comment-input').innerHTML
                        }
                        $.ajax({
                            url: `http://localhost:8088/Rentabike/blog-comment`,
                            type: 'post', //send it through get method
                            data: updatePostData,
                            success: function (data) {
                                let updateRes = JSON.parse(data)
                                if(updateRes.SC==0){
                                    errorMess = updateRes.ME
                                    toast.show()
                                    return;
                                }

                                updatePostCmt.closest('.other-comment__item-wrapper').querySelector('.comment-content').innerHTML = updateRes.BlogComent.content
                                updatePostCmt.closest('.other-comment__item-wrapper').querySelector('.time-passed__comment').innerHTML = processDistanceDate(updateRes.BlogComent.updateTime, updateRes.BlogComent.timeComment)
                                cancelUpdateCmt.click();
                            },
                            error: function (xhr) {
                                console.log(xhr)
                            },
                        })
                    })
                    
                }
            })
            tippyWrapper[1].addEventListener('click', ()=>{
                if(isCallingAjaxDelete) return;
                isCallingAjaxDelete = true;
                let cmtParent = elementClicked.closest('.other-comment__item-wrapper')
                let deleteData = {
                    type: 'delete',
                    cmtId: cmtParent.dataset.id
                }
                $.ajax({
                    url: `http://localhost:8088/Rentabike/blog-comment`,
                    type: 'post', //send it through get method
                    data: deleteData,
                    success: function (data) {
                        let deleteRes = JSON.parse(data)
                        errorMess = deleteRes.ME
                        toast.show()
                        cmtParent.remove();
                        isCallingAjaxDelete = false
                    },
                    error: function (xhr) {
                        console.log(xhr)
                        isCallingAjaxDelete = false
                    },
                })
            })
        },
    });

    // handle logic show & hide tippy
    let tippyOpts = document.querySelectorAll('.tippy-target')
    for(let tippyOpt of tippyOpts){
        tippyOpt.addEventListener('click', ()=> {
            tippyOpt.classList.add('d-inline-block')
        })
        window.addEventListener('click', function(e) {
            if(tippyOpt && e.target !== tippyOpt){
                tippyOpt.classList.remove('d-inline-block')
            }
        })
    }
}

let handleLoadMoreReply = (contentWrapper, blogId, dataRPOnLoad)=> {
    let replyLoadBtns = contentWrapper.querySelectorAll('.number-reply')
    for(let replyLoadBtn of replyLoadBtns){
        replyLoadBtn.addEventListener('click', ()=> {
            let cmtId = replyLoadBtn.closest('.other-comment__item-wrapper').dataset.id

            $.ajax({
                url: `http://localhost:8088/Rentabike/blog-comment?type=loadReply&cmtId=${cmtId}`,
                type: 'get', //send it through get method
                data: '',
                success: function (data) {
                    let resLoadMoreReply = JSON.parse(data)
                    if(resLoadMoreReply.SC==0){
                        console.log('check res>>', resLoadMoreReply)
                        errorMess = resLoadMoreReply.ME
                        toast.show()
                        return;
                    }
                    let contentWrapper = document.createElement('div');
                    contentWrapper.innerHTML = (()=>{
                        return resLoadMoreReply.CmtData.map((cur, index)=>{
                            return `<div class="other-comment__item-wrapper" data-id="${cur.BlogComment.id}">
                                        <div class="other-comment__item" data-id="${cur.accountCmt.accountID}">
                                            <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${cur.accountCmt.accountID}`}">
                                                <img src="${cur.accountCmt?.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'}"alt="">
                                            </a>
                                            <div class="other-comment__detail">
                                                <div class="comment-detail__top">
                                                    <h4 class="comment-name">${cur.accountCmt.firstName+' '+cur.accountCmt.lastName}</h4>
                                                    <div class=content-many>
                                                        <p class="comment-content">${cur.BlogComment.content}</p>
                                                    </div>
                                                    <div class="comment-react__quan" data-bs-toggle="modal" data-bs-target="#showReactModal">
                                                    ${(()=>{
                                                        let content1 = cur.blogReact.map((curN, indexN)=>{
                                                            if(curN.count==0) return ''
                                                            return `<img src="${curN.BlogReactType.url}" alt="${curN.BlogReactType.description}" data-id="${curN.BlogReactType.id}">`
                                                        }).join('')
                                                        
                                                        let num = cur.blogReact.reduce((total, curN)=>{
                                                            return total + curN.count
                                                        }, 0)
                                                        if(num==0) return '';
                                                        return content1.concat(`<span>${num}</span>`)
                                                        })()}
                                                    </div>
                                                </div>
                                                <div class="comment-detail__bottom">
                                                    <span class="react-comment ${cur.blogOwnReact?'clicked':''}">Like</span>
                                                    <span class="reply-comment">Reply</span>
                                                    <span class="time-passed__comment">${processDistanceDate(cur.BlogComment.updateTime, cur.BlogComment.timeComment)}</span>
                                                    <div class="react-containter">
                                                        <img src="./assets/img/icon/like.png" alt="" data-id="1">
                                                        <img src="./assets/img/icon/love.png" alt="" data-id="2">
                                                        <img src="./assets/img/icon/wow.png" alt="" data-id="3">
                                                        <img src="./assets/img/icon/sad.png" alt="" data-id="4">
                                                        <img src="./assets/img/icon/angry.png" alt="" data-id="5">
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="tippy-target__css"><i class="fa-solid fa-ellipsis tippy-target"></i></span>
                                        </div>
                                    </div>`
                            }).join('')
                    })()
                        
                    let insertedArea = replyLoadBtn.closest('.other-comment__item-wrapper').querySelector('.action-cmt_area')
                    insertedArea.insertBefore(contentWrapper, insertedArea.children[0]);
                    handleTippy(dataRPOnLoad)
                    handleReplyCmt(dataRPOnLoad, blogId)
                    handleReact()
                    handleShowReact()
                    replyLoadBtn.remove();
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        })
    }
}

let handleReplyCmt = (dataRPOnLoad, blogId)=>{
    //handle click reply comment
    let replyItemBtns = document.querySelectorAll('.comment-detail__bottom .reply-comment')
    for(let i = 0; i < replyItemBtns.length; i++){
        replyItemBtns[i].addEventListener('click', () => {
            if(dataRPOnLoad.AuthenAcc){
                let replyNest = replyItemBtns[i].closest('.other-comment__item-wrapper').querySelector('.action-cmt_area') ||  replyItemBtns[i].closest('.action-cmt_area')
                if(replyNest.querySelector('.comment-reply')){
                    return;
                }
                let replyChild = document.createElement('div');
                replyChild.classList.add('reply-div__wrapper');
                replyChild.innerHTML = 
                    `<div class="your-comment comment-reply" data-replyid="${replyItemBtns[i].closest('.other-comment__item-wrapper').dataset.id}">
                        <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${dataRPOnLoad.AuthenAcc.accountID}`}">
                            <img src="${dataRPOnLoad.AuthenAcc?.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'}" alt="">
                        </a>
                        <div class="comment-input" contenteditable="true">
                            <span class="reply-name">${replyItemBtns[i].closest('.other-comment__detail').querySelector('.comment-name').innerHTML}</span> 
                            <span>&nbsp;</span>
                        </div>
                        <div class="interact-comments">
                            <button class="cancel-btn">Cancel</button>
                            <button class="post-comment__btn">Post comment</button>
                        </div>
                    </div>`
                
                replyNest.insertBefore(replyChild, replyNest.children[0])

                let replyItem = replyItemBtns[i].closest('.action-cmt_area') || replyItemBtns[i].closest('.other-comment__item-wrapper')
                console.log("🚀 ~ file: BlogDetail.js ~ line 176 ~ replyItemBtns[i].addEventListener ~ replyItem", replyItem)
                let replyCmt = replyItem.querySelector(`.comment-reply .comment-input`)
                let replyPostCmt = replyItem.querySelector(`.comment-reply .post-comment__btn`)
                let cancelReplyCmt = replyItem.querySelector(`.comment-reply .cancel-btn`)
                replyCmt.addEventListener('input', (e) => {
                    if(replyCmt.textContent===''){
                        e.target.setAttribute('placeholder', 'Write your reply here...');
                        replyPostCmt.classList.remove('active');
                    }
                    else {
                        e.target.removeAttribute('placeholder');
                        replyPostCmt.classList.add('active');
                    }
                })
                cancelReplyCmt.addEventListener('click', () => {
                    cancelReplyCmt.closest(`.comment-reply`).closest('.reply-div__wrapper').remove();
                })
                replyPostCmt.addEventListener('click', () => {
                    let yourReplyCmtData = {
                        type: 'reply',
                        blogId,
                        content: replyPostCmt.closest('.your-comment').querySelector('.comment-input').innerHTML,
                        replyId: replyPostCmt.closest('.comment-reply').dataset.replyid
                    }
                    $.ajax({
                        url: `http://localhost:8088/Rentabike/blog-comment`,
                        type: 'post', //send it through get method
                        data: yourReplyCmtData,
                        success: function (data) {
                            let replyRes = JSON.parse(data)
                            if(replyRes.SC==0){
                                console.log('check res>>', replyRes)
                                errorMess = replyRes.ME
                                toast.show()
                                return;
                            }
                            
                            let oldLengCmt = +(_$('.comment-interact span').innerHTML)
                            _$('.comment-interact span').innerHTML = oldLengCmt +1
                            _$('.comment-news__heading h4').innerHTML = `${oldLengCmt +1} comments`
                            let contentWrapper = document.createElement('div');
                            contentWrapper.innerHTML =
                                `<div class="other-comment__item-wrapper" data-id="${replyRes.BlogComent.id}">
                                    <div class="other-comment__item" data-id="${dataRPOnLoad.AuthenAcc.accountID}">
                                        <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${dataRPOnLoad.AuthenAcc.accountID}`}">
                                            <img src="${dataRPOnLoad.AuthenAcc?.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'}"alt="">
                                        </a>
                                        <div class="other-comment__detail">
                                            <div class="comment-detail__top">
                                                <h4 class="comment-name">${dataRPOnLoad.AuthenAcc.firstName+' '+dataRPOnLoad.AuthenAcc.lastName}</h4>
                                                <div class=content-many>
                                                    <p class="comment-content">${replyRes.BlogComent.content}</p>
                                                </div>
                                                <div class="comment-react__quan" data-bs-toggle="modal" data-bs-target="#showReactModal"></div>
                                            </div>
                                            <div class="comment-detail__bottom">
                                                <span class="react-comment">Like</span>
                                                <span class="reply-comment">Reply</span>
                                                <span class="time-passed__comment">${processDistanceDate(replyRes.BlogComent.updateTime, replyRes.BlogComent.timeComment)}</span>
                                                <div class="react-containter">
                                                    <img src="./assets/img/icon/like.png" alt="" data-id="1">
                                                    <img src="./assets/img/icon/love.png" alt="" data-id="2">
                                                    <img src="./assets/img/icon/wow.png" alt="" data-id="3">
                                                    <img src="./assets/img/icon/sad.png" alt="" data-id="4">
                                                    <img src="./assets/img/icon/angry.png" alt="" data-id="5">
                                                </div>
                                            </div>
                                        </div>
                                        <span class="tippy-target__css"><i class="fa-solid fa-ellipsis tippy-target"></i></span>
                                    </div>
                                </div>`
                            let insertedArea = replyPostCmt.closest('.other-comment__item-wrapper').querySelector('.action-cmt_area')
                            insertedArea.insertBefore(contentWrapper, insertedArea.children[0]);
                            cancelReplyCmt.click();
                            handleTippy(dataRPOnLoad)
                            handleReplyCmt(dataRPOnLoad, blogId)
                            handleReact()
                            handleShowReact()
                        },
                        error: function (xhr) {
                            console.log(xhr)
                        },
                    })
                })
                return;
            }

            errorMess = 'Please login to reply comment!'
            toast.show()
        })
    }
}

let handleReact = ()=>{
    let reactCmts = document.querySelectorAll('.react-comment')
    for(let reactCmt of reactCmts){
        let hoverId, outId
        let reactPopUp = reactCmt.closest('.comment-detail__bottom').querySelector('.react-containter')
        let cmtDetailWrapper = reactCmt.closest('.comment-detail__bottom')
        var old_elements = reactPopUp.querySelectorAll('img')
        for(let old_element of old_elements) {
            let new_element = old_element.cloneNode(true);
            reactPopUp.replaceChild(new_element, old_element);
        }
        let reactPopUpItems = reactPopUp.querySelectorAll('img')
        
        reactCmt.addEventListener('mouseover', ()=>{
            if(outId) clearTimeout(outId)
            hoverId = setTimeout(()=>{
                cmtDetailWrapper.classList.add('react')
            }, 1000)
        })
        reactCmt.addEventListener('mouseout', ()=> {
            if(hoverId)  clearTimeout(hoverId)
            outId = setTimeout(()=>{
                cmtDetailWrapper.classList.remove('react')
            }, 1000)
        })
        reactPopUp.addEventListener('mouseover', ()=> {
            if(outId) clearTimeout(outId)
        })
        reactPopUp.addEventListener('mouseout', ()=>{
            if(hoverId)  clearTimeout(hoverId)
            outId = setTimeout(()=>{
                cmtDetailWrapper.classList.remove('react')
            }, 1000)
        })
        for(let reactPopUpItem of reactPopUpItems){
            reactPopUpItem.addEventListener('click', ()=>{
                let reactData = {
                    type: 'add-react',
                    cmtId: cmtDetailWrapper.closest('.other-comment__item-wrapper').dataset.id,
                    reactId: reactPopUpItem.dataset.id
                }
                $.ajax({
                    url: `http://localhost:8088/Rentabike/blog-comment`,
                    type: 'post', //send it through get method
                    data: reactData,
                    success: function (data) {
                        let resReact = JSON.parse(data)
                        if(resReact.SC==0){
                            errorMess = resReact.ME
                            toast.show()
                            return;
                        }
                        //close popup
                        if(hoverId)  clearTimeout(hoverId)
                        if(outId) clearTimeout(outId)
                        cmtDetailWrapper.classList.remove('react')
                        //check own react
                        if(resReact.blogOwnReact) reactCmt.classList.add('clicked')
                        else if(resReact.blogOwnReact==null) reactCmt.classList.remove('clicked')
                        //re-render
                        let reactQuanContent = reactPopUpItem.closest('.other-comment__item-wrapper').querySelector('.comment-react__quan')
                        reactQuanContent.innerHTML = (()=>{
                            let content1 = resReact.blogReact.map((curN, indexN)=>{
                                if(curN.count==0) return ''
                                return `<img src="${curN.BlogReactType.url}" alt="${curN.BlogReactType.description}" data-id="${curN.BlogReactType.id}">`
                            }).join('')
                            
                            let num = resReact.blogReact.reduce((total, curN)=>{
                                return total + curN.count
                            }, 0)
                            if(num==0) return '';
                            return content1.concat(`<span>${num}</span>`)
                        })()

                    },
                    error: function (xhr) {
                        console.log(xhr)
                    },
                })
            })
        }
    }
}

let handleShowReact = ()=>{
    let showReactsTmp = document.querySelectorAll('.comment-react__quan')
    for(let showReact of showReactsTmp){
        let showReactParent = showReact.closest('.comment-detail__top')
        let new_element = showReact.cloneNode(true);
        showReactParent.replaceChild(new_element, showReact);
    }
    let showReacts = document.querySelectorAll('.comment-react__quan')
    for(let showReact of showReacts){
        showReact.addEventListener('click', ()=>{
            let showReactData = {
                type: 'load-show-react',
                cmtId: showReact.closest('.other-comment__item-wrapper').dataset.id
            }
            $.ajax({
                url: `http://localhost:8088/Rentabike/blog-comment?type=${showReactData.type}&cmtId=${showReactData.cmtId}`,
                type: 'get', //send it through get method
                data: '',
                success: function (data) {
                    let resShowReact = JSON.parse(data)
                    if(resShowReact.SC==0){
                        errorMess = resShowReact.ME
                        toast.show()
                        return;
                    }
                    modalShowReact.querySelector('.modal-title span').innerHTML = `(${resShowReact.showReact.length})`
                    modalShowReact.querySelector('.modal-body').innerHTML = (()=>{
                        return resShowReact.showReact.map((cur, index)=>{
                            return `<div class="reaction-item">
                                        <div class="reaction-item__avatar">
                                            <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${cur.accountReact.accountID}`}">
                                                <img src="${cur.accountReact.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'}" alt="">
                                            </a>
                                            <img src="${cur.blogReactDetail.url}" alt="${cur.blogReactDetail.description}" data-id="${cur.blogReactDetail.id}">
                                        </div>
                                        <div class="reaction-item__name">
                                            <p>${cur.accountReact.firstName+' '+cur.accountReact.lastName}</p>
                                        </div>
                                    </div>`
                        }).join('')
                    })()
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        })
    }
}
const handleSaveOrUnsave = (blogId)=>{
    let typeChoose = _$('.news-heading__right .fa-bookmark');
    let isCallingAjaxSaveOrUnsave=false
    typeChoose.addEventListener('click', ()=>{
        if(isCallingAjaxSaveOrUnsave) return;
        isCallingAjaxSaveOrUnsave = true;
        if(typeChoose.classList.contains('fa-regular')){
            let dataSend = {
                type: 'save',
                blogId
            }
            $.ajax({
                url: `http://localhost:8088/Rentabike/blog-save`,
                type: 'post', //send it through get method
                data: dataSend,
                success: function (data) {
                    let dataRs = JSON.parse(data)
                    //data
                    errorMess = dataRs.ME
                    toast.show()
                    if(dataRs.SC!=1) return;
                    typeChoose.classList.remove('fa-regular');
                    typeChoose.classList.add('fa-solid');
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        }
        else if(typeChoose.classList.contains('fa-solid')){
            let dataSend = {
                type: 'unsave',
                blogId
            }
            $.ajax({
                url: `http://localhost:8088/Rentabike/blog-save`,
                type: 'post', //send it through get method
                data: dataSend,
                success: function (data) {
                    let dataRs = JSON.parse(data)
                    //data
                    errorMess = dataRs.ME
                    toast.show()
                    if(dataRs.SC==-1) return;
                    typeChoose.classList.remove('fa-solid');
                    typeChoose.classList.add('fa-regular');
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        }
        isCallingAjaxSaveOrUnsave = false
    })
}
function NewsDetail(){
    let blogId, dataRPOnLoad, dataRPCmt
    let isExistCmt = true, isCallingAjaxForNextCmt = false, isCallingAjaxForVote = false;
    let commentInput = _$('.comment-input');
    let interactComment = _$('.interact-comments');
    let notLoginComment = _$('.not-login__comment');
    let loginComment = _$('.your-comment');
    let cancelBtn = _$('.cancel-btn');
    let postCmtBtn = _$('.post-comment__btn');
    let loadingPage = _$('#loading-page');
    let pageContent = _$('.custome-container');
    let updownInteracts = document.querySelectorAll('.updown-interact i');

    $(window).on('load', () => {
        let url_string = document.URL;
        let url = new URL(url_string);
        let id = url.searchParams.get("id");
        $.ajax({
            url: `http://localhost:8088/Rentabike/blog-detail?id=${id}&type=getDetail`,
            type: 'get', //send it through get method
            data: '',
            success: function (data) {
                dataRPOnLoad = JSON.parse(data)
                //not found case?
                if(dataRPOnLoad.SC==0){

                    return;
                }
                //data
                blogId = dataRPOnLoad.BlogData.blog.id
                if(dataRPOnLoad.BlogData.author.email===dataRPOnLoad?.AuthenAcc?.email){
                    _$('.admin-side').remove();
                    _$('.auth-side .btn-warning').addEventListener('click', ()=>{
                        window.location.href = `${`http://localhost:8088/Rentabike/manage-blog?type=update&id=${dataRPOnLoad.BlogData.blog.id}`}`
                    })
                    _$('#DeleteModal .btn-primary').addEventListener('click', (e)=>{
                        _$('#DeleteModal .spinner-border').classList.remove('hide')
                        _$('#DeleteModal .btn-primary').classList.add('disabled')
                        let dataSend = {
                            type: 'delete',
                            blogId
                        }
                        $.ajax({
                            url: `http://localhost:8088/Rentabike/manage-blog`,
                            type: 'post', //send it through get method
                            data: dataSend,
                            success: function (data) {
                                let dataRP = JSON.parse(data)
                                errorMess = dataRP.ME;
                                toast.show();
                                
                                if(dataRP.SC && dataRP.SC===1){
                                    setTimeout(()=>{
                                        window.location.href = '/Rentabike/blog-list'
                                    }, 1000)
                                }
                            },
                            error: function (xhr) {
                                console.log(xhr)
                            },
                        })
                    })
                }
                else {
                    _$('.auth-side').remove()
                    if(dataRPOnLoad.AuthenAcc?.permission==='ADMIN'){
                        _$('.admin-side .btn-warning').addEventListener('click', ()=>{
                            let dataSend = {
                                type: 'need-update',
                                blogId
                            }
                            $.ajax({
                                url: `http://localhost:8088/Rentabike/blog-admin-control`,
                                type: 'post', //send it through get method
                                data: dataSend,
                                success: function (data) {
                                    let dataRP = JSON.parse(data)
                                    errorMess = dataRP.ME;
                                    toast.show();
                                    
                                    if(dataRP.SC && dataRP.SC===1){
                                        setTimeout(()=>{
                                            window.location.href = '/Rentabike/blog-list'
                                        }, 1000)
                                    }
                                },
                                error: function (xhr) {
                                    console.log(xhr)
                                },
                            })
                        })
                    }else {
                        _$('.admin-side').remove();
                    }
                }
                _$('.news-heading__right span:nth-child(1)').innerHTML = `<i class="${dataRPOnLoad.blogSave==undefined?'fa-regular': 'fa-solid'} fa-bookmark"></i>`
                _$('.news-username h6').innerHTML = `${dataRPOnLoad.BlogData.author.firstName} ${dataRPOnLoad.BlogData.author.lastName}`
                _$('.news-title').innerHTML = dataRPOnLoad.BlogData.blog.blogTitle
                _$('.news-heading__left img').src = dataRPOnLoad.BlogData.author.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'
                document.querySelectorAll('.news-heading__left a')[0].href = `http://localhost:8088/Rentabike/blog-account?accountId=${dataRPOnLoad.BlogData.author.accountID}`
                document.querySelectorAll('.news-heading__left a')[1].href = `http://localhost:8088/Rentabike/blog-account?accountId=${dataRPOnLoad.BlogData.author.accountID}`
                _$('.heading-detail p').innerHTML = `${dataRPOnLoad.BlogData.author.firstName} ${dataRPOnLoad.BlogData.author.lastName}`
                _$('.heading-detail span').innerHTML = processDistanceDate(dataRPOnLoad.BlogData.blog.updatedAt, dataRPOnLoad.BlogData.blog.createdAt)
                _$('.news-content').innerHTML = dataRPOnLoad.BlogData.blog.content
                _$('.updown-interact span').innerHTML = dataRPOnLoad.BlogData.vote
                _$('.comment-interact span').innerHTML = dataRPOnLoad.CmtLenght
                _$('.comment-news__heading h4').innerHTML = `${dataRPOnLoad.CmtLenght} comments`

                // update vs delete btn
                if(dataRPOnLoad.AuthenAcc){
                    loginComment.classList.remove('d-none')
                    notLoginComment.classList.add('d-none')
                    loginComment.querySelector('img').src = dataRPOnLoad.AuthenAcc?.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'
                    _$('.your-comment a').href = `http://localhost:8088/Rentabike/blog-account?accountId=${dataRPOnLoad.AuthenAcc.accountID}`
                    if(dataRPOnLoad.typeVoted && dataRPOnLoad.typeVoted.type==1)
                        updownInteracts[0].classList.add('choose')
                    if(dataRPOnLoad.typeVoted && dataRPOnLoad.typeVoted.type==0)
                        updownInteracts[1].classList.add('choose')
                } else if(dataRPOnLoad.AuthenAcc==null){
                    loginComment.classList.add('d-none')
                    notLoginComment.classList.remove('d-none')
                }

                //handle save or unsave
                handleSaveOrUnsave(blogId)

                //display page
                loadingPage.classList.add('d-none')
                pageContent.classList.remove('d-none')
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })

    $(window).scroll(function() {
        let CmtEleWrapper = document.querySelectorAll('.other-comments__wrapper .other-comment__item-wrapper')
        let after = 0
        if(CmtEleWrapper.length!=0)
            after = CmtEleWrapper[CmtEleWrapper.length-1].dataset.id
        if($(window).scrollTop() + 100 > $(document).height() - $(window).height() && isExistCmt && !isCallingAjaxForNextCmt) {
            $.ajax({
                url: `http://localhost:8088/Rentabike/blog-comment?type=loadCmt&id=${blogId}&after=${after}`,
                type: 'get', //send it through get method
                data: '',
                beforeSend: function(){
                    _$('.comment-news__area .justify-content-center').classList.remove('d-none');
                    isCallingAjaxForNextCmt = true;
                },
                success: function (data) {
                    dataRPCmt = JSON.parse(data)
                    //not found case?
                    if(dataRPCmt.SC==0){
                        return;
                    }
                    //append data
                    _$('.comment-news__area .justify-content-center').classList.add('d-none');
                    if(dataRPCmt.CmtData.length==0){
                        isExistCmt=false;
                        return;
                    }
                    let contentWrapper = document.createElement('div');
                    contentWrapper.innerHTML = (()=>{
                        return dataRPCmt.CmtData.map((cur, index)=>{
                            return `<div class="other-comment__item-wrapper" data-id="${cur.BlogComment.id}">
                                        <div class="other-comment__item" data-id="${cur.accountCmt.accountID}">
                                            <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${cur.accountCmt.accountID}`}"><img src="${cur.accountCmt.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'}"alt=""></a>
                                            <div class="other-comment__detail">
                                                <div class="comment-detail__top">
                                                    <h4 class="comment-name">${cur.accountCmt.firstName+' '+cur.accountCmt.lastName}</h4>
                                                    <div class=content-many>
                                                        <div class="comment-content">${cur.BlogComment.content}</div>
                                                    </div>
                                                    <div class="comment-react__quan" data-bs-toggle="modal" data-bs-target="#showReactModal">
                                                    ${(()=>{
                                                        let content1 = cur.blogReact.map((curN, indexN)=>{
                                                            if(curN.count==0) return ''
                                                            return `<img src="${curN.BlogReactType.url}" alt="${curN.BlogReactType.description}" data-id="${curN.BlogReactType.id}">`
                                                        }).join('')
                                                        
                                                        let num = cur.blogReact.reduce((total, curN)=>{
                                                            return total + curN.count
                                                        }, 0)
                                                        if(num==0) return '';
                                                        return content1.concat(`<span>${num}</span>`)
                                                        })()}
                                                    </div>
                                                </div>
                                                <div class="comment-detail__bottom">
                                                    <span class="react-comment ${cur.blogOwnReact?'clicked':''}">Like</span>
                                                    <span class="reply-comment">Reply</span>
                                                    <span class="time-passed__comment">${processDistanceDate(cur.BlogComment.updateTime, cur.BlogComment.timeComment)}</span>
                                                    <div class="react-containter">
                                                        <img src="./assets/img/icon/like.png" alt="" data-id="1">
                                                        <img src="./assets/img/icon/love.png" alt="" data-id="2">
                                                        <img src="./assets/img/icon/wow.png" alt="" data-id="3">
                                                        <img src="./assets/img/icon/sad.png" alt="" data-id="4">
                                                        <img src="./assets/img/icon/angry.png" alt="" data-id="5">
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="tippy-target__css"><i class="fa-solid fa-ellipsis tippy-target"></i></span>
                                        </div>
                                        <div class="action-cmt_area">
                                            ${cur.blogReply>0? 
                                                `<div class="number-reply">
                                                    <i class="fa-solid fa-reply"></i>
                                                    <span>${cur.blogReply} reply</span>
                                                </div>`:''}
                                        </div>
                                    </div>`
                        }).join('')
                    })()
                    _$('.other-comments__wrapper').appendChild(contentWrapper)
                    
                    //handle tippy
                    handleTippy(dataRPOnLoad)

                    //handle reply cmt
                    handleReplyCmt(dataRPOnLoad, blogId)

                    //handle load more reply
                    handleLoadMoreReply(contentWrapper, blogId, dataRPOnLoad)

                    //handle react cmt
                    handleReact()

                    //handle show react
                    handleShowReact()

                    isCallingAjaxForNextCmt = false;
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        }
    });

    //handle add new comment input
    commentInput.addEventListener('input', (e) => {
        if(commentInput.textContent===''){
            e.target.setAttribute('placeholder', 'Write your comment here...');
            postCmtBtn.classList.remove('active');
        }
        else {
            e.target.removeAttribute('placeholder');
            postCmtBtn.classList.add('active');
        }
    })
    commentInput.addEventListener('focus', (e) => {
        interactComment.classList.remove('d-none')
    })
    cancelBtn.addEventListener('click', () => {
        commentInput.textContent = ''
        interactComment.classList.add('d-none')
        commentInput.setAttribute('placeholder', 'Write your comment here...');
        postCmtBtn.classList.remove('active');
    })
    postCmtBtn.addEventListener('click', () => {
        let yourPostCmtData = {
            type: 'insert',
            blogId,
            content: postCmtBtn.closest('.your-comment').querySelector('.comment-input').innerHTML
        }
        $.ajax({
            url: `http://localhost:8088/Rentabike/blog-comment`,
            type: 'post', //send it through get method
            data: yourPostCmtData,
            success: function (data) {
                let insertRes = JSON.parse(data)
                if(insertRes.SC==0){
                    errorMess = insertRes.ME
                    toast.show()
                    return;
                }
                cancelBtn.click();
                let oldLengCmt = +(_$('.comment-interact span').innerHTML)
                _$('.comment-interact span').innerHTML = oldLengCmt +1
                _$('.comment-news__heading h4').innerHTML = `${oldLengCmt +1} comments`
                let contentWrapper = document.createElement('div');
                contentWrapper.innerHTML =
                    `<div class="other-comment__item-wrapper" data-id="${insertRes.BlogComent.id}">
                        <div class="other-comment__item" data-id="${dataRPOnLoad.AuthenAcc.accountID}">
                        <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${dataRPOnLoad.AuthenAcc.accountID}`}"><img src="${dataRPOnLoad.AuthenAcc?.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'}" alt=""></a>
                            <div class="other-comment__detail">
                                <div class="comment-detail__top">
                                    <h4 class="comment-name">${dataRPOnLoad.AuthenAcc.firstName+' '+dataRPOnLoad.AuthenAcc.lastName}</h4>
                                    <div class=content-many>
                                        <p class="comment-content">${insertRes.BlogComent.content}</p>
                                    </div>
                                    <div class="comment-react__quan" data-bs-toggle="modal" data-bs-target="#showReactModal"></div>
                                </div>
                                <div class="comment-detail__bottom">
                                    <span class="react-comment">Like</span>
                                    <span class="reply-comment">Reply</span>
                                    <span class="time-passed__comment">${processDistanceDate(insertRes.BlogComent.updateTime, insertRes.BlogComent.timeComment)}</span>
                                    <div class="react-containter">
                                        <img src="./assets/img/icon/like.png" alt="" data-id="1">
                                        <img src="./assets/img/icon/love.png" alt="" data-id="2">
                                        <img src="./assets/img/icon/wow.png" alt="" data-id="3">
                                        <img src="./assets/img/icon/sad.png" alt="" data-id="4">
                                        <img src="./assets/img/icon/angry.png" alt="" data-id="5">
                                    </div>
                                </div>
                            </div>
                            <span class="tippy-target__css"><i class="fa-solid fa-ellipsis tippy-target"></i></span>
                        </div>
                        <div class="action-cmt_area"></div>
                    </div>`
                _$('.other-comments__wrapper').insertBefore(contentWrapper, _$('.other-comments__wrapper').children[0]);
                handleTippy(dataRPOnLoad)
                handleReplyCmt(dataRPOnLoad, blogId)
                handleReact()
                handleShowReact()
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })

    //handle vote
    for(let i=0; i<updownInteracts.length; i++) {
        updownInteracts[i].addEventListener('click', () => {
            if(isCallingAjaxForVote) return;
            isCallingAjaxForVote=true;

            let reactType = (i==0? 1:0);
            let dataVote = {
                type: 'vote',
                blogId,
                reactType
            }
            
            $.ajax({
                url: `http://localhost:8088/Rentabike/blog-detail`,
                type: 'post', //send it through get method
                data: dataVote,
                success: function (data) {
                    let resVote = JSON.parse(data)
                    if(resVote.SC==0){
                        errorMess = resVote.ME
                        toast.show()
                        isCallingAjaxForVote=false;
                        return;
                    }
                    if(updownInteracts[i].classList.contains('choose')) {
                        updownInteracts[i].classList.remove('choose')    
                    }
                    else {
                        updownInteracts[0].classList.remove('choose')
                        updownInteracts[1].classList.remove('choose')
                        updownInteracts[i].classList.add('choose')
                    }
                    _$('.updown-interact span').innerHTML = resVote.Num
                    isCallingAjaxForVote=false;
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        })
    }
}

NewsDetail()