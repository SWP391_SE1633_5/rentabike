const changedPasswordInput = document.querySelector("#changed-page__input-password");
const changedPasswordTitle = document.querySelector(".changed-page__title-input");
const changedPasswordSubmit = document.querySelector(".changed-page__action-link");
const changedPasswordColor = document.querySelector(".changed-page__action-color");
const changedSubmitText = document.querySelector(".changed-page__action-link span");
const changedLoadingAnimation = document.querySelector(".changed-page__icon-loading");
const changedPageMessage = document.querySelector(".changed-page__message[mode='success']");
/*=====================ANIMATION=====================*/
const changedPageTitle = document.querySelector(".changed-page__title");
const changePageInputField = document.querySelector(".changed-page__input-field");
const changePageSubmitButton = document.querySelector(".changed-page__action");
/*=====================REGEX=====================*/
const regexPassword = new RegExp("^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$");
const changed = {
    changedPassword: function () {
        changedPasswordInput.onkeyup = function () {
            let lengthInput = this.value.length;
            let checkPassword = false;
            if (this.getAttribute("role") === "password") {
                if (regexPassword.test(this.value) && lengthInput >= 8) {
                    changedPasswordTitle.style.color = "";
                    changedPasswordTitle.innerHTML = "Password";
                    this.style.backgroundColor = "";
                    this.style.border = "";
                    checkPassword = true;
                } else {
                    changedPasswordTitle.style.color = "var(--notify-bd-color)";
                    changedPasswordTitle.innerHTML = "Password - At least 8 characters, one uppercase, lowercase and special character";
                    this.style.backgroundColor = "var(--notify-color)";
                    this.style.border = "1px solid var(--notify-bd-color)";
                    this.placeHolder = ""
                    checkPassword = false;
                }
                if (lengthInput === 0) {
                    changedPasswordTitle.style.color = "var(--notify-bd-color)";
                    changedPasswordTitle.innerHTML = "Password - Please fill out this field";
                    this.style.backgroundColor = "var(--notify-color)";
                    this.style.border = "1px solid var(--notify-bd-color)";
                    checkPassword = false;
                }
            }
            if(checkPassword){
                changedPasswordSubmit.setAttribute("type", "unlock");
                changedPasswordColor.setAttribute("type", "unlock");
            }else{
                changedPasswordSubmit.setAttribute("type", "disabled");
                changedPasswordColor.setAttribute("type", "disabled");
            }
        };
        changedPasswordSubmit.onclick = function(){
            if(this.getAttribute("type") === "unlock"){
                changedSubmitText.innerHTML = "";
                changedLoadingAnimation.style.display = "block";
                changedPasswordSubmit.setAttribute("type", "disabled");
                changedPasswordColor.setAttribute("type", "disabled");
                changedPasswordColor.style.transform = "rotate(-45deg) translate(80%, 0)";
                changedPageTitle.style.transform = "translateY(-300%)";
                changePageInputField.style.transform = "translateX(-200%)";
                changePageSubmitButton.style.transform = "translateX(200%)";
                changedPageMessage.style.display = "block";

                setTimeout(() => {
                    changedPageMessage.style.transform = "translateX(0%)";
                    changePageInputField.style.display = "none";
                    changedPageTitle.innerHTML = "Changed password successfully";
                    changedPageTitle.style.transform = "translateY(0%)";
                    changedPasswordSubmit.setAttribute("type", "unlock");
                    changedPasswordSubmit.href = "started.jsp";
                    changedPasswordColor.setAttribute("type", "unlock");
                    changedPasswordColor.style.transform = "";
                    changedSubmitText.innerHTML = "BACK TO SIGN IN";
                    changedLoadingAnimation.style.display = "";
                    changePageSubmitButton.style.transform = "translateX(0%)";
                }, 1500);
            }
        };
    },
    start: function () {
        this.changedPassword();
    }
};
changed.start();