let _$ = (selector) => document.querySelector(selector)
let checkResultArr = [];
let url = `http://localhost:8088/Rentabike/assistance-quest?type=getAll`;
let toastElement = document.querySelector('.toast');
let toast = new bootstrap.Toast(toastElement)
let toastBody = document.querySelector('.toast-body');
let errorMess='';
//handle event toast
toastElement.addEventListener('show.bs.toast', () => {
    toastBody.innerHTML = errorMess
})

let processDistanceDate = (update, start)=>{
    let result;
    let d1 = Date.parse(update||start)
    let d2 = new Date()
    let diffTime = Math.abs(d1 - d2);
    let diffMinutes = Math.floor(diffTime / (1000 * 60)); 
    let diffHours = Math.floor(diffTime / (1000 * 60 * 60)); 
    let diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24)); 
    let diffMoths = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 30)); 
    let diffYears = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 365)); 
    if(diffMinutes < 1){
        result = 'Just now!'
    } else if (diffMinutes>=1 && diffHours<1){
        result = `${diffMinutes} minutes ago`
    } else if (diffHours>=1 && diffDays<1){
        result = `${diffHours} hours ago`
    } else if (diffDays>=1 && diffMoths<1){
        result = `${diffDays} days ago`
    } else if (diffMoths>=1 && diffYears<1){
        result = `${diffMoths} moths ago`
    }else if (diffYears>=1)
        result = `${diffYears} years ago`
    return `${result}${update==null ? '' : ' (has been updated)'}`
}

function paginate(url){
    checkResultArr = [];
    _$('.blog-control input').checked=false;
    handleShowHideAction();
    let assistWrapper = _$('.assistances-wrapper')
    assistWrapper.innerHTML = `<div id="loading-page">
                                <div class="spinner-border" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>`
    $('#pagination').pagination({
        dataSource: function (done) {
            $.ajax({
                url: `${url}`,
                type: 'get', //send it through get method
                data: '',
                success: function (data) {
                    let dataRP = JSON.parse(data)
                    // console.log(dataRP)
                    done(dataRP.Data)
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        },
        
        pageSize: 3,
        callback: function (data, pagination) {
            if(data.length===0){
                assistWrapper.innerHTML = `<div class="no-thing">
                                            <i class="fa-solid fa-magnifying-glass"></i>
                                            <h3>No blog found!</h3>
                                        </div>`
                return;
            }
            let assistContent = data
                .map((cur, index) => {
                    return `
                    <div class='assistances-item' data-assistid="${cur.AssistanceQues.id}">
                        <div class="check-item">
                            <label class="check-all">
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="main-item__wrapper">
                            <div class="inner-main__item">
                                <div class="item-header">
                                    <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${cur?.Account?.accountID}`}" class="item-header__left">
                                        <img src="${cur?.Account?.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'
                                        }" alt="avatar'">
                                        <p>${cur?.Account?.firstName + ' ' + cur?.Account?.lastName}</p>
                                    </a>
                                </div>
                                <div class="assistance-item__content">
                                    <div class="assistance-item__left">
                                        <p class="item-description">${cur.AssistanceQues.content}</p>
                                        <div class="item-bottom">
                                            <span>
                                                ${processDistanceDate(null, cur.AssistanceQues.createdTime)}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="control-item">
                                <span><i class="fa-solid fa-trash"></i></span>
                            </div>
                        </div>
                    </div>`
                })
                .join('')

            assistWrapper.innerHTML = assistContent
            handleCheck();
            handleDeleteSingle();
        },
    })
}

const handleChildCheckOrUnCheck = ()=>{
    let checkParent = _$('.blog-control input[type="checkbox"]');
    let checkChildList = document.querySelectorAll('.assistances-wrapper input[type="checkbox"]');
    for(let checkChild of checkChildList){
        if(!checkChild.checked){
            checkParent.checked = false;
            return;
        }
    }
    checkParent.checked = true;
}


const handleShowHideAction = ()=>{
    let removeClick = _$('.remove-click');

    if(checkResultArr.length===0){
        removeClick.classList.add('d-none');
    } else {
        removeClick.classList.remove('d-none');
    }
}

const handleCheck = ()=>{
    let checkParent = _$('.blog-control input[type="checkbox"]');
    
    checkParent.addEventListener('change', () => {
        let checkChildList = document.querySelectorAll('.assistances-wrapper input[type="checkbox"]');
        let check = checkParent.checked;
        for(let checkChild of checkChildList){
            checkChild.checked = check;
            if(check){
                if(checkResultArr.indexOf(+checkChild.closest('.assistances-item').dataset.assistid)===-1)
                    checkResultArr = [...checkResultArr, +checkChild.closest('.assistances-item').dataset.assistid];
            } else {
                checkResultArr = []
            }
        }
        handleShowHideAction();
        console.log(checkResultArr);
    });

    let checkChildList = document.querySelectorAll('.assistances-wrapper input[type="checkbox"]');
    for(let checkChild of checkChildList){
        checkChild.addEventListener('change', ()=>{
            handleChildCheckOrUnCheck();
            if(checkChild.checked){
                if(checkResultArr.indexOf(+checkChild.closest('.assistances-item').dataset.assistid)===-1)
                    checkResultArr = [...checkResultArr, +checkChild.closest('.assistances-item').dataset.assistid];
            } else {
                checkResultArr = checkResultArr.filter(item=> item!==+checkChild.closest('.assistances-item').dataset.assistid)
            }
            handleShowHideAction();
            console.log(checkResultArr);
        })
    }
}
const callDeleteBlog = (dataSend)=>{
    return $.ajax({
        url: `http://localhost:8088/Rentabike/assistance-quest`,
        type: 'post', //send it through get method
        data: dataSend
    })
}

const handleDeleteMany = ()=>{
    let removeClick = _$('.remove-click');
    removeClick.addEventListener('click', async() =>{
        if(checkResultArr.length===0){
            errorMess = 'Please choose at least on blog to continue!'
            toast.show()
            return;
        }
        let dataSend = {
            type: 'handle-delete',
            checkResultArr,
        }
        let dataRsRaw = await callDeleteBlog(dataSend);
        let dataRs = JSON.parse(dataRsRaw);
        errorMess = dataRs.ME
        toast.show()
        
        let assistancesList = document.querySelectorAll('.assistances-item')
        for (let assistancesItem of assistancesList){
            if(dataSend.checkResultArr.indexOf(+assistancesItem.dataset.assistid)!==-1){
                assistancesItem.remove();
            }
        }
        paginate(url)
    })
}

const handleDeleteSingle = () => {
    let deleteBtns = document.querySelectorAll('.control-item span');
    for(let deleteBtn of deleteBtns){
        deleteBtn.addEventListener('click', async() =>{
            let singleArr = []
            singleArr.push(+deleteBtn.closest('.assistances-item').dataset.assistid)
            let dataSend = {
                type: 'handle-delete',
                checkResultArr: singleArr,
            }
            let dataRsRaw = await callDeleteBlog(dataSend);
            let dataRs = JSON.parse(dataRsRaw);
            errorMess = dataRs.ME
            toast.show()
            paginate(url)
        })
    }
}

const handleSearch = ()=>{
    let searchInput = _$('.search-wrapper input');
    let timeId;
    searchInput.addEventListener('input', ()=>{
        if(timeId) clearTimeout(timeId)
        timeId = setTimeout(()=>{
            let url = `http://localhost:8088/Rentabike/assistance-quest?type=Search&search=${searchInput.value}`;
            paginate(url);
        }, 1000)
    })
}


function AssistanceAdminControl() {    
    paginate(url);
    handleDeleteMany();
    handleSearch();
}

AssistanceAdminControl()
