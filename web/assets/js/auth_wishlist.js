const wishList = document.querySelector("#wishlist");
const wishListPagination = document.querySelector("#wishlist-pagination");
const lidgetButtonScroll = document.querySelector(".lidget-shop-button");
const wishListSearch = document.querySelector("#wishlist-search");
const sortingChoice = document.querySelector("#sortingChoice");
const sortingCategory = document.querySelector("#sortingCategory");
const sortingMode = document.querySelector("#sortingMode");
const sortingBrand = document.querySelector("#sortingBrand");

/*=====================WIDGET-SHOP=====================*/
const widgetWishList = {
    handlePagination: function () {
        $('#pagination-slider').twbsPagination({
            totalPages: document.getElementById("auth-wishlist").getAttribute("data-value"),
            hideOnlyOnePage: false,
            visiblePages: 5,
            onPageClick: function (event, page) {
                lidgetButtonScroll.click();
                wishList.style.opacity = "0";
                wishListPagination.style.opacity = "0";
                setTimeout(() => {
                    wishList.innerHTML = "";
                    $.ajax({
                        url: "/Rentabike/wishListController",
                        type: "post",
                        data: {
                            mode: "pagePagination",
                            page: page
                        },
                        success: function (response) {
                            $("#wishlist").html(response);
                        }
                    });
                }, 500);
                setTimeout(() => {
                    wishList.style.opacity = "1";
                    wishListPagination.style.opacity = "1";
                }, 1500);
            }
        });
    },
    sortConfig: function () {
        $((function () {
            var t = "ltr",
                    e = !1;
            "rtl" === $("html").data("textdirection") && (t = "rtl"), "rtl" === t && (e = !0);
            var o = c = $(".dropdown-sort .dropdown-item"),
                    v = $(".dropdown-toggle .active-sorting"),
                    c1 = $(".dropdown-mode .dropdown-item"),
                    v1 = $(".dropdown-toggle .active-mode"),
                    c2 = $(".dropdown-category .dropdown-item"),
                    v2 = $(".dropdown-toggle .active-category"),
                    c3 = $(".dropdown-brand .dropdown-item"),
                    v3 = $(".dropdown-toggle .active-brand");

            c.length && c.on("click", (function () {
                var t = $(this).text();
                v.text(t);
            })),
                    c1.length && c1.on("click", (function () {
                        var t = $(this).text();
                        v1.text(t);
                    })),
                    c2.length && c2.on("click", (function () {
                        var t = $(this).text();
                        v2.text(t);
                    })),
                    c3.length && c3.on("click", (function () {
                        var t = $(this).text();
                        v3.text(t);
                    }));
        }));
    },
    searchProduct: function () {
        $('#wishlist-search').on('keyup', function () {
            lidgetButtonScroll.click();
            wishList.style.opacity = "0";
            wishListPagination.style.opacity = "0";
            setTimeout(() => {
                wishList.innerHTML = "";
                $.ajax({
                    url: "/Rentabike/wishListController",
                    type: "post",
                    data: {
                        mode: "searchBookmark",
                        inputSearch: this.value
                    },
                    success: function (response) {
                        $("#wishlist").html(response);
                    }
                });
            }, 500);
            setTimeout(() => {
                wishList.style.opacity = "1";
                if (this.value.length === 0 || this.value === "") {
                    wishListPagination.style.opacity = "1";
                    $('#pagination-slider').twbsPagination('show', 1);
                }
            }, 1500);
        });
    },
    sortSelect: function () {
        let observer = new MutationObserver(function (mutations) {
            let optionChoice = sortingChoice.innerText;
            wishList.style.opacity = "0";
            wishListPagination.style.opacity = "0";
            setTimeout(() => {
                wishList.innerHTML = "";
                $.ajax({
                    url: "/Rentabike/wishListController",
                    type: "post",
                    data: {
                        mode: "sortOption",
                        option: optionChoice
                    },
                    success: function (response) {
                        if (response !== "OUT") {
                            $("#wishlist").html(response);
                        }
                    }
                });
            }, 500);
            setTimeout(() => {
                wishList.style.opacity = "1";
                if (optionChoice === "Sort") {
                    wishListPagination.style.opacity = "1";
                    $('#pagination-slider').twbsPagination('show', 1);
                }
            }, 1500);
        });
        observer.observe(sortingChoice, {
            attributes: true,
            childList: true,
            characterData: true
        });
    },
    sortMode: function () {
        let observer = new MutationObserver(function (mutations) {
            let optionChoice = sortingMode.innerText;
            wishList.style.opacity = "0";
            wishListPagination.style.opacity = "0";
            setTimeout(() => {
                wishList.innerHTML = "";
                $.ajax({
                    url: "/Rentabike/wishListController",
                    type: "post",
                    data: {
                        mode: "modeProduct",
                        option: optionChoice
                    },
                    success: function (response) {
                        if (response !== "OUT") {
                            $("#wishlist").html(response);
                        }
                    }
                });
            }, 500);
            setTimeout(() => {
                wishList.style.opacity = "1";
                if (optionChoice === "Mode") {
                    wishListPagination.style.opacity = "1";
                    $('#pagination-slider').twbsPagination('show', 1);
                }
            }, 1500);
        });
        observer.observe(sortingMode, {
            attributes: true,
            childList: true,
            characterData: true
        });
    },
    sortCategory: function () {
        let observer = new MutationObserver(function (mutations) {
            let optionChoice = sortingCategory.innerText;
            wishList.style.opacity = "0";
            wishListPagination.style.opacity = "0";
            setTimeout(() => {
                wishList.innerHTML = "";
                $.ajax({
                    url: "/Rentabike/wishListController",
                    type: "post",
                    data: {
                        mode: "category",
                        option: optionChoice
                    },
                    success: function (response) {
                        if (response !== "OUT") {
                            $("#wishlist").html(response);
                        }
                    }
                });
            }, 500);
            setTimeout(() => {
                wishList.style.opacity = "1";
                if (optionChoice === "Category") {
                    wishListPagination.style.opacity = "1";
                    $('#pagination-slider').twbsPagination('show', 1);
                }
            }, 1500);
        });
        observer.observe(sortingCategory, {
            attributes: true,
            childList: true,
            characterData: true
        });
    },
    sortBrand: function () {
        let observer = new MutationObserver(function (mutations) {
            let optionChoice = sortingBrand.innerText;
            wishList.style.opacity = "0";
            wishListPagination.style.opacity = "0";
            setTimeout(() => {
                wishList.innerHTML = "";
                $.ajax({
                    url: "/Rentabike/wishListController",
                    type: "post",
                    data: {
                        mode: "brand",
                        option: optionChoice
                    },
                    success: function (response) {
                        if (response !== "OUT") {
                            $("#wishlist").html(response);
                        }
                    }
                });
            }, 500);
            setTimeout(() => {
                wishList.style.opacity = "1";
                if (optionChoice === "Brands") {
                    wishListPagination.style.opacity = "1";
                    $('#pagination-slider').twbsPagination('show', 1);
                }
            }, 1500);
        });
        observer.observe(sortingBrand, {
            attributes: true,
            childList: true,
            characterData: true
        });
    },
    removeWishList: function (indexProduct, param) {
        let notifyStatus = true;
        var t = $(param),
                o = "rtl" === $("html").attr("data-textdirection");
        t.closest(".ecommerce-card").remove(), toastr.error("", "Removed Item From Wishlist 🗑️", {
            closeButton: !0,
            tapToDismiss: !1,
            rtl: o
        });
        $.ajax({
            url: "/Rentabike/wishListController",
            type: "post",
            data: {
                mode: "wishListHandle",
                choice: "removeWishList",
                productID: indexProduct
            },
            success: function (response) {
                console.log("SUCCESS");
                $.ajax({
                    url: "/Rentabike/notifyController",
                    type: "post",
                    data: {
                        mode: "notifyRecordAndShow",
                        productID: indexProduct,
                        setUpNotify: "removeWishList"
                    },
                    success: function (response) {
                        if (response !== "FAILED") {
                            $("#notify").html(response);
                        } else {
                            window.location.replace("http://localhost:8088/Rentabike/homepageCController");
                        }
                        if (notifyStatus) {
                            $.ajax({
                                url: "/Rentabike/notifyController",
                                type: "post",
                                data: {
                                    mode: "NotifyTotalHandler",
                                    setUpNotify: "calculateNotify"
                                },
                                success: function (response) {
                                    if (response !== "FAILED") {
                                        $("#notifyCalculation").text(response);
                                    }
                                }
                            });
                            notifyStatus = false;
                        }
                    }
                });
            }
        });

    },
    addToCheckOut: function (indexProduct, email, param) {
        let addCart = $(param);
        let findCart = addCart.find(".add-to-cart");
        let notifyStatus = true;
        findCart.text("Move to Cart").removeClass("add-to-cart").addClass("view-in-cart"),
                toastr.success("", "Moved Item To Your Cart 🛒", {
                    closeButton: !0,
                    tapToDismiss: !1,
                    rtl: !1
                });
        $.ajax({
            url: "/Rentabike/notifyController",
            type: "post",
            data: {
                mode: "notifyRecordAndShow",
                productID: indexProduct,
                email: email,
                setUpNotify: "Checkout"
            },
            success: function (response) {
                if (response !== "FAILED") {
                    $("#notify").html(response);
                } else {
                    window.location.replace("http://localhost:8088/Rentabike/homepageCController");
                }
                if (notifyStatus) {
                    $.ajax({
                        url: "/Rentabike/notifyController",
                        type: "post",
                        data: {
                            mode: "NotifyTotalHandler",
                            setUpNotify: "calculateNotify"
                        },
                        success: function (response) {
                            if (response !== "FAILED") {
                                $("#notifyCalculation").text(response);
                            }
                        }
                    });
                    notifyStatus = false;
                }
            }
        });
    },    pageOnLoad: function () {
        let notifyStatus = true;
        $(window).on('load', function () {
            $.ajax({
                url: "/Rentabike/notifyController",
                type: "post",
                data: {
                    mode: "showNotifyOnly"
                },
                success: function (response) {
                    if (response !== 'FAILED') {
                        $("#notify").html(response);
                    }
                    if (notifyStatus) {
                        $.ajax({
                            url: "/Rentabike/notifyController",
                            type: "post",
                            data: {
                                mode: "NotifyTotalHandler",
                                setUpNotify: "calculateNotify"
                            },
                            success: function (response) {
                                if (response !== "FAILED") {
                                    $("#notifyCalculation").text(response);
                                }
                            }
                        });
                        notifyStatus = false;
                    }
                }
            });
        });
    },
    start: function () {
        this.handlePagination();
        this.sortConfig();
        this.searchProduct();
        this.sortSelect();
        this.sortMode();
        this.sortCategory();
        this.sortBrand();
        this.pageOnLoad();
    }
};
widgetWishList.start();

function sendToMoveCart(param) {
    let email = document.querySelector(".lidget-naz").getAttribute("current-login");
    let indexProduct = param.getAttribute("index-data");
    widgetWishList.addToCheckOut(indexProduct, email, param);
}

function sendToRemoveWishList(param) {
    let indexProduct = param.getAttribute("index-data");
    widgetWishList.removeWishList(indexProduct, param);
}
            