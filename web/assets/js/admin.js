/*=====================WIDGET-ADMIN=====================*/
const widgetAdmin = {
    loginControlelr: function () {
        $('#lidget-login__form').submit(function (event) {
            event.preventDefault();
            let email = document.querySelector("#login-email").value,
                    pinCode = document.querySelector("#login-password").value,
                    formCheckBox = document.querySelector(".form-check-input"),
                    transitionElement = document.querySelector(".lidget-login__transition"),
                    loginErrorMessage = document.querySelector(".lidget-login__error");
            if (email.length > 0 && pinCode.length > 0) {
                $.ajax({
                    url: "/Rentabike/loginController",
                    type: "post",
                    data: {
                        email: email,
                        password: pinCode,
                        rememberMe: formCheckBox.checked
                    },
                    success: function (response) {
                        if (response !== "FAILED") {
                            if (loginErrorMessage.style.transform === "scale(1)") {
                                loginErrorMessage.style.transform = "scale(0)";
                                transitionElement.style.marginTop = "-6rem";
                            }
                            setTimeout(() => {
                                window.location.replace("http://localhost:8088/Rentabike/auth-two-steps-basic.jsp");
                            }, 1500);
                        } else {
                            setTimeout(() => {
                                loginErrorMessage.style.transform = "scale(1)";
                            }, 1500);
                            transitionElement.style.marginTop = "0rem";
                        }
                    }
                });
            }
        }
        );
    },
    twoStepVerification: function () {
        $('#two-step-form').submit(function (event) {
            event.preventDefault();
            let pinCode = document.querySelectorAll(".form-control");
            let pinCodeMerge = 0;
            let email = document.querySelector('#emailPinCode').innerHTML;
            for (let char of pinCode) {
                pinCodeMerge += char.value;
            }
            $.ajax({
                url: "/Rentabike/loginController",
                type: "post",
                data: {
                    pinCode: pinCodeMerge.toString().substring(1),
                    staffLogin: true,
                    email: email
                },
                success: function (response) {
                    let message = "";
                    if (response === "SUCCESS") {
                        setTimeout(() => {
                            window.location.replace("http://localhost:8088/Rentabike/dashBoardAdminController");
                        }, 1500);
                    } else if (response === "EXPIRED") {
                        let w = "rtl" === $("html").attr("data-textdirection");
                        setTimeout((function () {
                            toastr.error("Your pin code is expired. Please resend another one", "Error", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: w
                            });
                        }));
                    } else {
                        let w = "rtl" === $("html").attr("data-textdirection");
                        setTimeout((function () {
                            toastr.error("Your pin code entered is not corrected.", "Error", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: w
                            });
                        }));
                    }
                }
            });
        }
        );
    },
    resendPinCode: function () {
        let resendButton = document.querySelector(".lidget-resend-code");
        let dataEmail = document.querySelector(".data-email");
        resendButton.onclick = function () {
            $.ajax({
                url: "/Rentabike/resendPinCodeController",
                type: "post",
                data: {
                    email: dataEmail.innerHTML
                },
                success: function (response) {
                    if (response === "SUCCESS") {
                        let w = "rtl" === $("html").attr("data-textdirection");
                        setTimeout((function () {
                            toastr.success("A new pin code had been send to your mail", "Completed!", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: w
                            });
                        }));
                    } else {
                        let w = "rtl" === $("html").attr("data-textdirection");
                        setTimeout((function () {
                            toastr.error("Session failed. Please log in your account again", "Error", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: w
                            });
                        }));
                    }
                }
            });
        };
    },
    registerController: function () {
        $('#register__form').submit(function (event) {
            event.preventDefault();
            let formControl = document.querySelectorAll(".form-control");
            $.ajax({
                url: "/Rentabike/registerController",
                type: "post",
                data: {
                    username: formControl[0].value,
                    email: formControl[1].value,
                    password: formControl[2].value,
                    firstName: formControl[4].value,
                    lastName: formControl[5].value,
                    mobileNumber: formControl[6].value,
                    city: formControl[7].value,
                    street: formControl[8].value,
                    state: $('.form-control option:selected').val(),
                    mode: "STAFF"
                },
                success: function (response) {
                    if (response === "SUCCESS") {
                        window.location.replace("http://localhost:8088/Rentabike/auth-login-cover.jsp");
                    } else if (response === "EMAIL") {
                        document.querySelector(".lidget-register-switch-back").click();
                        var w = "rtl" === $("html").attr("data-textdirection");
                        setTimeout((function () {
                            toastr.error("Your email had been existed.", "Warning", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: w
                            });
                        }));
                    } else {
                        var w = "rtl" === $("html").attr("data-textdirection");
                        setTimeout((function () {
                            toastr.error("There are some error expected.", "Error", {
                                closeButton: !0,
                                tapToDismiss: !1,
                                rtl: w
                            });
                        }));
                    }
                }
            });
        }
        );
    },
    start: function () {
        this.loginControlelr();
        this.registerController();
        this.twoStepVerification();
        this.resendPinCode();
    }
};
widgetAdmin.start();
