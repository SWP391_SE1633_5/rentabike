function ResetPassword() {
  let formSubmit = document.querySelector(".form-submit");
  let isSubmit = false;
  let btnSubmit = document.querySelector(".submit");
  let spinner = btnSubmit.querySelector(".spinner-border");
  let inputP = document.querySelector("input#password");
  let inputCP = document.querySelector("input#confirm-password");
  let showMess = document.querySelector(".toast.align-items-center");
  const regexPassword = new RegExp("^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$");
  let formData = {};

  let url_string = document.URL;
  let url = new URL(url_string);
  let token = url.searchParams.get("token");

  let toastElement = document.querySelector('.toast');
  let toast = new bootstrap.Toast(toastElement)
  let toastBody = document.querySelector('.toast-body');
  let errorMess='';
  //handle event toast
  toastElement.addEventListener('show.bs.toast', () => {
      toastBody.innerHTML = errorMess
  })

  formSubmit.addEventListener("submit", (e) => {
    e.preventDefault();
    if (isSubmit) return;
    isSubmit = true;

    //check password
    if(!regexPassword.test(inputP.value)){
      errorMess = 'Wrong format password!';
      toast.show();
      isSubmit = false;
      return;
    }

    formData = {
      ...formData,
      password: inputP.value,
      confirmPassword: inputCP.value,
      token,
    };

    $.ajax({
      url: "http://localhost:8088/Rentabike/reset-password",
      type: "post", //send it through get method
      data: formData,
      beforeSend: function(){
        btnSubmit.classList.add("disabled");
        spinner.classList.remove("hide");
      },
      success: function (data) {
        console.log(data);
        btnSubmit.classList.remove("disabled");
        spinner.classList.add("hide");
        let dataP = JSON.parse(data);
        errorMess = dataP.Mess
        toast.show();
        isSubmit = false;
        if(dataP.SC===0){
          setTimeout(()=>{
            window.location.replace(
              "http://localhost:8088/Rentabike/started.jsp"
            );
          }, 2000)
        }
      },
      error: function (xhr) {
        //Do Something to handle error
        console.log(xhr);
        isSubmit = false;
        errorMess = 'Something went wrong!';
        toast.show();
      },
    });

    // console.log(11111)
  });
}

ResetPassword();
