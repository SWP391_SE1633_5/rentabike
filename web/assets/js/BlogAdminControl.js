let _$ = (selector) => document.querySelector(selector)
let checkResultArr = [];
let url = `http://localhost:8088/Rentabike/blog-admin-control?type=All`;
let toastElement = document.querySelector('.toast');
let toast = new bootstrap.Toast(toastElement)
let toastBody = document.querySelector('.toast-body');
let errorMess='';
//handle event toast
toastElement.addEventListener('show.bs.toast', () => {
    toastBody.innerHTML = errorMess
})

let processDistanceDate = (update, start)=>{
    let result;
    let d1 = Date.parse(update||start)
    let d2 = new Date()
    let diffTime = Math.abs(d1 - d2);
    let diffMinutes = Math.floor(diffTime / (1000 * 60)); 
    let diffHours = Math.floor(diffTime / (1000 * 60 * 60)); 
    let diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24)); 
    let diffMoths = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 30)); 
    let diffYears = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 365)); 
    if(diffMinutes < 1){
        result = 'Just now!'
    } else if (diffMinutes>=1 && diffHours<1){
        result = `${diffMinutes} minutes ago`
    } else if (diffHours>=1 && diffDays<1){
        result = `${diffHours} hours ago`
    } else if (diffDays>=1 && diffMoths<1){
        result = `${diffDays} days ago`
    } else if (diffMoths>=1 && diffYears<1){
        result = `${diffMoths} moths ago`
    }else if (diffYears>=1)
        result = `${diffYears} years ago`
    return `${result}${update==null ? '' : ' (has been updated)'}`
}

function paginate(url){
    checkResultArr = [];
    _$('.blog-control input').checked=false;
    handleShowHideAction();
    let newsWrapper = _$('.news-wrapper')
    newsWrapper.innerHTML = `<div id="loading-page">
                                <div class="spinner-border" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>`
    $('#pagination').pagination({
        dataSource: function (done) {
            $.ajax({
                url: `${url}`,
                type: 'get', //send it through get method
                data: '',
                success: function (data) {
                    let dataRP = JSON.parse(data)
                    // console.log(dataRP)
                    done(dataRP.Data)
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        },
        
        pageSize: 3,
        callback: function (data, pagination) {
            if(data.length===0){
                newsWrapper.innerHTML = `<div class="no-thing">
                                            <i class="fa-solid fa-magnifying-glass"></i>
                                            <h3>No blog found!</h3>
                                        </div>`
                return;
            }
            let blogContent = data
                .map((cur, index) => {
                    return `
                    <div class="${cur.blog.isRead? 'news-item read':'news-item'}" data-blogid="${cur.blog.id}">
                        <div class="check-item">
                            <label class="check-all">
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="main-item__wrapper">
                            <div class="inner-main__item">
                                <div class="item-header">
                                    <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${cur?.account?.accountID}`}" class="item-header__left">
                                        <img src="${cur?.account?.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'
                                        }" alt="avatar'">
                                        <p>${cur?.account?.firstName + ' ' + cur?.account?.lastName}</p>
                                    </a>
                                </div>
                                <div class="new-item__content">
                                    <div class="new-item__left">
                                        <a href="${`http://localhost:8088/Rentabike/blog-detail-admin?id=${cur.blog.id}`}" class="item-title">${cur.blog.blogTitle}</a>
                                        <p class="item-description">${cur.blog.blogDescription}</p>
                                        <div class="item-bottom">
                                            ${(()=>{
                                                let tagList = cur.tag.map((curTag, tagIndex)=>{
                                                    return `<a class="item-bottom__hashtag hastag">
                                                                <span>${curTag.tagTitle}</span>
                                                            </a>`
                                                }).join('')
                                                return tagList;
                                            })()}
                                            <span>
                                                ${processDistanceDate(cur.blog.updatedAt, cur.blog.createdAt)}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="new-item__right">
                                        <a href="${`http://localhost:8088/Rentabike/blog-detail-admin?id=${cur.blog.id}`}">
                                            <img src="${cur.blog.image || './assets/img/anh.jpg'}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="control-item">
                                <button type="button" class="btn btn-primary">Accept</button>
                                <button type="button" class="btn btn-danger">Delete</button>
                            </div>
                        </div>
                    </div>`
                })
                .join('')

            newsWrapper.innerHTML = blogContent
            handleCheck();
            handleDeleteSingle();
            handleAcceptBlog();
            handleGetDetail();
        },
    })
}

const handleChildCheckOrUnCheck = ()=>{
    let checkParent = _$('.blog-control input[type="checkbox"]');
    let checkChildList = document.querySelectorAll('.news-wrapper input[type="checkbox"]');
    for(let checkChild of checkChildList){
        if(!checkChild.checked){
            checkParent.checked = false;
            return;
        }
    }
    checkParent.checked = true;
}

const handleShowReadType = ()=>{
    let readClick = _$('.read-click');
    let checkChildList = document.querySelectorAll('.news-wrapper input[type="checkbox"]');
    for(let checkChild of checkChildList){
        if(checkChild.checked){
            let childP = checkChild.closest('.news-item')
            if(childP.classList.contains('read'))
                readClick.innerHTML = `<i class="fa-solid fa-envelope" data-readtype="0" title="Mark as unread">`
            else 
                readClick.innerHTML = `<i class="fa-solid fa-envelope-open" data-readtype="1" title="Mark as read"></i>`
            return;
        }
    }
}

const handleShowHideAction = ()=>{
    let removeClick = _$('.remove-click');
    let readClick = _$('.read-click');

    handleShowReadType()

    if(checkResultArr.length===0){
        readClick.classList.add('d-none');
        removeClick.classList.add('d-none');
    } else {
        readClick.classList.remove('d-none');
        removeClick.classList.remove('d-none');
    }
}

const handleCheck = ()=>{
    let checkParent = _$('.blog-control input[type="checkbox"]');
    
    checkParent.addEventListener('change', () => {
        let checkChildList = document.querySelectorAll('.news-wrapper input[type="checkbox"]');
        let check = checkParent.checked;
        for(let checkChild of checkChildList){
            checkChild.checked = check;
            if(check){
                if(checkResultArr.indexOf(+checkChild.closest('.news-item').dataset.blogid)===-1)
                    checkResultArr = [...checkResultArr, +checkChild.closest('.news-item').dataset.blogid];
            } else {
                checkResultArr = []
            }
        }
        handleShowHideAction();
        console.log(checkResultArr);
    });

    let checkChildList = document.querySelectorAll('.news-wrapper input[type="checkbox"]');
    for(let checkChild of checkChildList){
        checkChild.addEventListener('change', ()=>{
            handleChildCheckOrUnCheck();
            if(checkChild.checked){
                if(checkResultArr.indexOf(+checkChild.closest('.news-item').dataset.blogid)===-1)
                    checkResultArr = [...checkResultArr, +checkChild.closest('.news-item').dataset.blogid];
            } else {
                checkResultArr = checkResultArr.filter(item=> item!==+checkChild.closest('.news-item').dataset.blogid)
            }
            handleShowHideAction();
            console.log(checkResultArr);
        })
    }
}

const handleReadOrUnread = () => {
    let readClick = _$('.read-click');
    readClick.addEventListener('click', () =>{
        if(checkResultArr.length===0){
            errorMess = 'Please choose at least on blog to continue!'
            toast.show()
            return;
        }
        let dataSend = {
            type: 'handle-read',
            checkResultArr,
            readType: readClick.querySelector('i').dataset.readtype
        }
        console.log(dataSend)
        $.ajax({
            url: `http://localhost:8088/Rentabike/blog-admin-control`,
            type: 'post', //send it through get method
            data: dataSend,
            success: function (data) {
                let dataRs = JSON.parse(data)
                //data
                errorMess = dataRs.ME
                toast.show()
                
                let newsList = document.querySelectorAll('.news-item')
                for (let newsItem of newsList){
                    if(dataSend.checkResultArr.indexOf(+newsItem.dataset.blogid)!==-1){
                        newsItem.className = dataSend.readType==1? 'news-item read': 'news-item'
                    }
                }
                handleShowReadType()
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })
}

const callDeleteBlog = (dataSend)=>{
    return $.ajax({
        url: `http://localhost:8088/Rentabike/blog-admin-control`,
        type: 'post', //send it through get method
        data: dataSend
    })
}

const handleDeleteMany = ()=>{
    let removeClick = _$('.remove-click');
    removeClick.addEventListener('click', async() =>{
        if(checkResultArr.length===0){
            errorMess = 'Please choose at least on blog to continue!'
            toast.show()
            return;
        }
        let dataSend = {
            type: 'handle-delete',
            checkResultArr,
        }
        let dataRsRaw = await callDeleteBlog(dataSend);
        let dataRs = JSON.parse(dataRsRaw);
        errorMess = dataRs.ME
        toast.show()
        
        let newsList = document.querySelectorAll('.news-item')
        for (let newsItem of newsList){
            if(dataSend.checkResultArr.indexOf(+newsItem.dataset.blogid)!==-1){
                newsItem.remove();
            }
        }
        paginate(url)
    })
}

const handleDeleteSingle = () => {
    let deleteBtns = document.querySelectorAll('.btn-danger');
    for(let deleteBtn of deleteBtns){
        deleteBtn.addEventListener('click', async() =>{
            let singleArr = []
            singleArr.push(+deleteBtn.closest('.news-item').dataset.blogid)
            let dataSend = {
                type: 'handle-delete',
                checkResultArr: singleArr,
            }
            let dataRsRaw = await callDeleteBlog(dataSend);
            let dataRs = JSON.parse(dataRsRaw);
            errorMess = dataRs.ME
            toast.show()
            paginate(url)
        })
    }
}

const handleSearch = ()=>{
    let searchInput = _$('.search-wrapper input');
    let timeId;
    searchInput.addEventListener('input', ()=>{
        if(timeId) clearTimeout(timeId)
        timeId = setTimeout(()=>{
            let url = `http://localhost:8088/Rentabike/blog-admin-control?type=Search&search=${searchInput.value}`;
            paginate(url);
        }, 1000)
    })
}

const handleAcceptBlog=()=>{
    let acceptBtns = document.querySelectorAll('.btn-primary');
    for(let acceptBtn of acceptBtns){
        acceptBtn.addEventListener('click', () =>{
            let dataSend = {
                type: 'handle-accept',
                blogId: acceptBtn.closest('.news-item').dataset.blogid,
            }
            $.ajax({
                url: `http://localhost:8088/Rentabike/blog-admin-control`,
                type: 'post', //send it through get method
                data: dataSend,
                success: function (data) {
                    let dataRs = JSON.parse(data)
                    errorMess = dataRs.ME
                    toast.show()
                    paginate(url)
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        })
    }
}

const handleGetDetail=()=>{
    let goToDetailAreas = document.querySelectorAll('.new-item__content a[href]')
    for(let goToDetailArea of goToDetailAreas){
        goToDetailArea.addEventListener('click', (e)=>{
            e.preventDefault();
            let url = goToDetailArea.href;
            if(goToDetailArea.closest('.news-item').classList.contains('read')){
                window.location.href = url;
                return;
            }

            let singleArr = [];
            singleArr.push(+goToDetailArea.closest('.news-item').dataset.blogid)
            let dataSend = {
                type: 'handle-read',
                checkResultArr: singleArr,
                readType: 1
            }
            $.ajax({
                url: `http://localhost:8088/Rentabike/blog-admin-control`,
                type: 'post', //send it through get method
                data: dataSend,
                success: function (data) {
                    window.location.href = url;
                    return;
                },
                error: function (xhr) {
                    console.log(xhr)
                },
            })
        })
    }
}

function NewList() {    
    paginate(url);
    handleReadOrUnread()
    handleDeleteMany();
    handleSearch();
}

NewList()
