let toastElement = document.querySelector('.toast');
let toast = new bootstrap.Toast(toastElement)
let toastBody = document.querySelector('.toast-body');
let errorMess='';
//handle event toast
toastElement.addEventListener('show.bs.toast', () => {
    toastBody.innerHTML = errorMess
})

const handleSaveOrUnsave = ()=>{
    let typeChooses = document.querySelectorAll('.item-header__right .fa-bookmark');

    for(let typeChoose of typeChooses){
        let isCallingAjaxSaveOrUnsave=false
        typeChoose.addEventListener('click', ()=>{
            if(isCallingAjaxSaveOrUnsave) return;
            isCallingAjaxSaveOrUnsave = true;
            if(typeChoose.classList.contains('fa-regular')){
                let dataSend = {
                    type: 'save',
                    blogId: typeChoose.closest('.news-item').dataset.blogid
                }
                $.ajax({
                    url: `http://localhost:8088/Rentabike/blog-save`,
                    type: 'post', //send it through get method
                    data: dataSend,
                    success: function (data) {
                        let dataRs = JSON.parse(data)
                        //data
                        errorMess = dataRs.ME
                        toast.show()
                        if(dataRs.SC!=1) return;
                        typeChoose.classList.remove('fa-regular');
                        typeChoose.classList.add('fa-solid');
                    },
                    error: function (xhr) {
                        console.log(xhr)
                    },
                })
            }
            else if(typeChoose.classList.contains('fa-solid')){
                let dataSend = {
                    type: 'unsave',
                    blogId: typeChoose.closest('.news-item').dataset.blogid
                }
                $.ajax({
                    url: `http://localhost:8088/Rentabike/blog-save`,
                    type: 'post', //send it through get method
                    data: dataSend,
                    success: function (data) {
                        let dataRs = JSON.parse(data)
                        //data
                        errorMess = dataRs.ME
                        toast.show()
                        if(dataRs.SC==-1) return;
                        typeChoose.classList.remove('fa-solid');
                        typeChoose.classList.add('fa-regular');
                    },
                    error: function (xhr) {
                        console.log(xhr)
                    },
                })
            }
            isCallingAjaxSaveOrUnsave = false
        })
    }
}

function NewList() {
    let _$ = (selector) => document.querySelector(selector)
    let tagList = [];
    let rightSide = _$('.right-side')
    let rightSideContent = _$('.right-side__content')
    let newsWrapper = _$('.news-wrapper')
    let url = `http://localhost:8088/Rentabike/blog-list?type=All`

    window.addEventListener('scroll', () => {
        // console.log(document.documentElement.scrollTop)
        // console.log(rightSide.getBoundingClientRect())
        if (
            document.body.scrollTop >= 156 ||
            document.documentElement.scrollTop >= 156
        ) {
            rightSide.classList.add('scroll')
        } else {
            rightSide.classList.remove('scroll')
        }
    })

    paginate()

    function paginate(){
        $('#pagination').pagination({
            dataSource: function (done) {
                $.ajax({
                    url: `${url}`,
                    type: 'get', //send it through get method
                    data: '',
                    success: function (data) {
                        let dataRP = JSON.parse(data)
                        // console.log(dataRP)
                        tagList = [...dataRP.TagList]
                        done(dataRP.Data)
                    },
                    error: function (xhr) {
                        console.log(xhr)
                    },
                })
            },
            
            pageSize: 4,
            callback: function (data, pagination) {
                let blogContent = data
                    .map((cur, index) => {
                        return `
                        <div class="news-item" data-blogid="${cur.blog.id}">
                            <div class="item-header">
                                <a href="${`http://localhost:8088/Rentabike/blog-account?accountId=${cur?.account?.accountID}`}" class="item-header__left">
                                    <img src="${cur?.account?.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'
                                    }" alt="avatar'">
                                    <p>${cur?.account?.firstName + ' ' + cur?.account?.lastName}</p>
                                </a>
                                <div class="item-header__right">
                                    <i class="${cur.blogSave==undefined? 'fa-regular': 'fa-solid'} fa-bookmark"></i>
                                    <i class="fa-solid fa-ellipsis"></i>
                                </div>
                            </div>
                            <div class="new-item__content">
                                <div class="new-item__left">
                                    <a href="${`http://localhost:8088/Rentabike/blog-detail?id=${cur.blog.id}`}" class="item-title">${cur.blog.blogTitle}</a>
                                    <p class="item-description">${cur.blog.blogDescription}</p>
                                    <div class="item-bottom">
                                        ${(()=>{
                                            let tagList = cur.tag.map((curTag, tagIndex)=>{
                                                return `<a href="#" class="item-bottom__hashtag hastag">
                                                            <span>${curTag.tagTitle}</span>
                                                        </a>`
                                            }).join('')
                                            return tagList;
                                        })()}
                                        <span>
                                            ${(()=>{
                                                let d1 = Date.parse(cur.blog.updatedAt || cur.blog.createdAt)
                                                let d2 = new Date()
                                                let diffTime = Math.abs(d1 - d2);
                                                let diffMinutes = Math.floor(diffTime / (1000 * 60)); 
                                                let diffHours = Math.floor(diffTime / (1000 * 60 * 60)); 
                                                let diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24)); 
                                                let diffMoths = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 30)); 
                                                let diffYears = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 365)); 
                                                if(diffMinutes < 1){
                                                    return 'Just now!'
                                                } else if (diffMinutes>=1 && diffHours<1){
                                                    return `${diffMinutes} minutes ago`
                                                } else if (diffHours>=1 && diffDays<1){
                                                    return `${diffHours} hours ago`
                                                } else if (diffDays>=1 && diffMoths<1){
                                                    return `${diffDays} days ago`
                                                } else if (diffMoths>=1 && diffYears<1){
                                                    return `${diffMoths} moths ago`
                                                }else if (diffYears>=1)
                                                    return `${diffYears} years ago`
                                            })()}
                                        </span>
                                    </div>
                                </div>
                                <div class="new-item__right">
                                    <a href="${`http://localhost:8088/Rentabike/blog-detail?id=${cur.blog.id}`}">
                                        <img src="${cur.blog.image || './assets/img/anh.jpg'}" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>`
                    })
                    .join('')
    
                let tagContent = tagList.map((cur, index)=>{
                    return `<a href="#" class="hastag">${cur.tagTitle}</a>`
                })
                tagContent = [`<a href="#" class="hastag">All</a>`, ...tagContent].join('')
    
                newsWrapper.innerHTML = blogContent
                rightSideContent.innerHTML = tagContent

                //save or unsave
                handleSaveOrUnsave()

                let tagLinks = document.querySelectorAll('a.hastag')
                for(let tagLink of tagLinks) {
                    tagLink.addEventListener('click', () => {
                        let itemSkeleton = `<div class="news-item">
                        <div class="item-header">
                            <a class="item-header__left">
                                <div class="item-header__loading-img skeleton"></div>
                                <p class="item-header__loading-name skeleton"></p>
                            </a>
                            <div class="item-header__right">
                                <i class="fa-regular fa-bookmark"></i>
                                <!-- <i class="fa-solid fa-bookmark"></i> -->
                                <i class="fa-solid fa-ellipsis"></i>
                            </div>
                        </div>
                        <div class="new-item__content">
                            <div class="new-item__left">
                                <a href="#" class="new-item__loading-title skeleton"></a>
                                <p class="new-item__loading-description skeleton"></p>
                                <div class="item-bottom">
                                    <a href="#" class="new-item__loading-hashtag skeleton"></a>
                                    <span class="new-item__loading-timepass skeleton"></span>
                                </div>
                            </div>
                            <div class="new-item__right">
                                <div class="new-item__loading-bigImg skeleton"></div>
                            </div>
                        </div>
                            </div>`
                        let tagSkeleton = `<div class="right-side__loading-tag skeleton"></div>`
                        newsWrapper.innerHTML = Array(3).fill(itemSkeleton).join('\n')
                        rightSideContent.innerHTML = Array(3).fill(tagSkeleton).join('\n')

                        url = `http://localhost:8088/Rentabike/blog-list?type=${tagLink.textContent.trim()}`
                        paginate()
                    })
                }
            },
        })
    }
}

NewList()
