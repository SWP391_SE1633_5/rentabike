let _$ = (selector) => document.querySelector(selector);
let toastElement = document.querySelector('.toast');
let toast = new bootstrap.Toast(toastElement)
let toastBody = document.querySelector('.toast-body');
let errorMess='';
//handle event toast
toastElement.addEventListener('show.bs.toast', () => {
    toastBody.innerHTML = errorMess
})
let processDistanceDate = (update, start)=>{
    let result;
    let d1 = Date.parse(update||start)
    let d2 = new Date()
    let diffTime = Math.abs(d1 - d2);
    let diffMinutes = Math.floor(diffTime / (1000 * 60)); 
    let diffHours = Math.floor(diffTime / (1000 * 60 * 60)); 
    let diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24)); 
    let diffMoths = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 30)); 
    let diffYears = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 365)); 
    if(diffMinutes < 1){
        result = 'Just now!'
    } else if (diffMinutes>=1 && diffHours<1){
        result = `${diffMinutes} minutes ago`
    } else if (diffHours>=1 && diffDays<1){
        result = `${diffHours} hours ago`
    } else if (diffDays>=1 && diffMoths<1){
        result = `${diffDays} days ago`
    } else if (diffMoths>=1 && diffYears<1){
        result = `${diffMoths} moths ago`
    }else if (diffYears>=1)
        result = `${diffYears} years ago`
    return `${result}${update==null ? '' : ' (has been updated)'}`
}

const postMethod = (url, dataSend)=>{
    return $.ajax({
        url: url,
        type: 'post',
        data: dataSend
    })
}

const handleAccept = (blogId)=>{
    _$('.btn-primary').addEventListener('click',async()=>{
        let url = 'http://localhost:8088/Rentabike/blog-admin-control'
        let dataSend = {
            type: 'handle-accept',
            blogId
        }
        let data = await postMethod(url, dataSend)
        let dataRs = JSON.parse(data)
        errorMess = dataRs.ME
        toast.show()

        if(dataRs.SC && dataRs.SC===1){
            setTimeout(()=>{
                window.location.href = '/Rentabike/blog-admin-control'
            }, 1000)
        }
    })
}

const handleDelete = (blogId)=>{
    _$('#DeleteModal .btn-primary').addEventListener('click',async()=>{
        _$('#DeleteModal .spinner-border').classList.remove('hide')
        _$('#DeleteModal .btn-primary').classList.add('disabled')
        let url = 'http://localhost:8088/Rentabike/blog-admin-control'
        let singleArr = [];
        singleArr.push(blogId);
        let dataSend = {
            type: 'handle-delete',
            checkResultArr: singleArr
        }
        let data = await postMethod(url, dataSend)
        let dataRs = JSON.parse(data)
        errorMess = dataRs.ME
        toast.show()

        if(dataRs.SC && dataRs.SC===1){
            setTimeout(()=>{
                window.location.href = '/Rentabike/blog-admin-control'
            }, 1000)
        }
    })
}
function NewsDetailControl(){
    let blogId, dataRPOnLoad, dataRPCmt
    let loadingPage = _$('#loading-page');
    let pageContent = _$('.custome-container');

    $(window).on('load', () => {
        let url_string = document.URL;
        let url = new URL(url_string);
        let id = url.searchParams.get("id");
        $.ajax({
            url: `http://localhost:8088/Rentabike/blog-detail-admin?id=${id}&type=getDetail`,
            type: 'get', //send it through get method
            data: '',
            success: function (data) {
                dataRPOnLoad = JSON.parse(data)
                //data
                blogId = dataRPOnLoad.BlogData.blog.id

                _$('.news-title').innerHTML = dataRPOnLoad.BlogData.blog.blogTitle
                _$('.news-heading__left img').src = dataRPOnLoad.BlogData.author.avatar || 'https://files.fullstack.edu.vn/f8-prod/user_avatars/26348/62f2f0038ccdd.png'
                document.querySelectorAll('.news-heading__left a')[0].href = `http://localhost:8088/Rentabike/blog-account?accountId=${dataRPOnLoad.BlogData.author.accountID}`
                document.querySelectorAll('.news-heading__left a')[1].href = `http://localhost:8088/Rentabike/blog-account?accountId=${dataRPOnLoad.BlogData.author.accountID}`
                _$('.heading-detail p').innerHTML = `${dataRPOnLoad.BlogData.author.firstName} ${dataRPOnLoad.BlogData.author.lastName}`
                _$('.heading-detail span').innerHTML = processDistanceDate(dataRPOnLoad.BlogData.blog.updatedAt, dataRPOnLoad.BlogData.blog.createdAt)
                _$('.news-content').innerHTML = dataRPOnLoad.BlogData.blog.content

                handleAccept(blogId);
                handleDelete(blogId);
                //display page
                loadingPage.classList.add('d-none')
                pageContent.classList.remove('d-none')
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })
}

NewsDetailControl()