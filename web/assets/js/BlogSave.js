let _$ = (selector) => document.querySelector(selector);
let toastElement = document.querySelector('.toast');
let toast = new bootstrap.Toast(toastElement)
let toastBody = document.querySelector('.toast-body');
let errorMess='';
//handle event toast
toastElement.addEventListener('show.bs.toast', () => {
    toastBody.innerHTML = errorMess
})
let processDistanceDate = (update, start)=>{
    let result;
    let d1 = Date.parse(update||start)
    let d2 = new Date()
    let diffTime = Math.abs(d1 - d2);
    let diffMinutes = Math.floor(diffTime / (1000 * 60)); 
    let diffHours = Math.floor(diffTime / (1000 * 60 * 60)); 
    let diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24)); 
    let diffMoths = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 30)); 
    let diffYears = Math.floor(diffTime / (1000 * 60 * 60 * 24 * 365)); 
    if(diffMinutes < 1){
        result = 'Just now!'
    } else if (diffMinutes>=1 && diffHours<1){
        result = `${diffMinutes} minutes ago`
    } else if (diffHours>=1 && diffDays<1){
        result = `${diffHours} hours ago`
    } else if (diffDays>=1 && diffMoths<1){
        result = `${diffDays} days ago`
    } else if (diffMoths>=1 && diffYears<1){
        result = `${diffMoths} moths ago`
    }else if (diffYears>=1)
        result = `${diffYears} years ago`
    return `${result}${update==null ? '' : ' (has been updated)'}`
}
let handleTippy = ()=>{
    let elementClicked
    let isCallingAjaxRemove = false
    let content = `<ul class="tiny-wrapper">
        <li>
            <i class="fa-solid fa-trash"></i>
            <span>Remove</span>
        </li>
    </ul>`
    const handleClickRemove = () => {
        if(isCallingAjaxRemove)return;
        isCallingAjaxRemove = true;
        let dataSend = {
            type: 'unsave',
            blogId: elementClicked.closest('.blog-item').dataset.blogid
        }
        $.ajax({
            url: `http://localhost:8088/Rentabike/blog-save`,
            type: 'post', //send it through get method
            data: dataSend,
            success: function (data) {
                let dataRs = JSON.parse(data)
                //data
                errorMess = dataRs.ME
                toast.show()
                if(dataRs.SC!=1) return;
                elementClicked.closest('.blog-item').remove();
                _$('.heading-content span').innerHTML = Number(_$('.heading-content span').innerHTML)-1
                if(_$('.heading-content span').innerHTML==0){
                    _$('.blog-list').innerHTML = `<div class="no-thing">
                            <i class="fa-solid fa-magnifying-glass"></i>
                            <h3>You haven't save blog yet!</h3>
                        </div>`
                }
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
        isCallingAjaxRemove = false;
    }
    // create tippy 
    tippy('.tippy-target', {
        trigger: 'click',
        content:  (reference) => {
            return content;
        },
        placement: 'bottom-end',
        allowHTML: true,
        onShown(instance) {
            elementClicked = instance.reference
            let tippyOpen = instance.popper
            let tippyWrapper = tippyOpen.querySelectorAll('.tiny-wrapper li')
            //Remove
            tippyWrapper[0].addEventListener('click', handleClickRemove)
        },
        onHidden(instance){
            let tippyOpen = instance.popper
            let tippyWrapper = tippyOpen.querySelectorAll('.tiny-wrapper li')
            // remove event Remove
            tippyWrapper[0].removeEventListener('click', handleClickRemove)
        }
    });
}

const BlogSave = ()=>{
    let dataRPOnLoad
    $(window).on('load', () => {
        $.ajax({
            url: `http://localhost:8088/Rentabike/blog-save?type=getAll`,
            type: 'get', //send it through get method
            data: '',
            success: function (data) {
                dataRPOnLoad = JSON.parse(data)
                //data
                _$('.heading-content span').innerHTML = `${dataRPOnLoad.BlogList.length}`
                _$('.blog-list').innerHTML = (()=>{
                    if(dataRPOnLoad.BlogList==undefined || dataRPOnLoad.BlogList.length==0) {
                        return `<div class="no-thing">
                                    <i class="fa-solid fa-magnifying-glass"></i>
                                    <h3>You haven't save blog yet!</h3>
                                </div>`
                    }
                    return dataRPOnLoad.BlogList.map((cur, index)=>{
                        return `<div class="blog-item" data-blogid="${cur.blog.id}">
                                    <div class="blog-item__tittle">
                                        <a href="${`http://localhost:8088/Rentabike/blog-detail?id=${cur.blog.id}`}">${cur.blog.blogTitle}</a>
                                    </div>
                                    <div class="blog-item__autho">
                                        <span>${processDistanceDate(cur.blog.updatedAt, cur.blog.createdAt)}</span>
                                        <span>.</span>
                                        <span>Tác giả <strong>${cur.author.firstName + ' ' + cur.author.lastName}</strong></span>
                                    </div>
                                    <span class="tippy-target__css"><i class="fa-solid fa-ellipsis tippy-target"></i></span>
                                </div>`
                    }).join('')
                })()

                //handle tippy
                handleTippy()

                //display page
                _$('#loading-page').classList.add('d-none')
                _$('.custome-container').classList.remove('d-none')
            },
            error: function (xhr) {
                console.log(xhr)
            },
        })
    })
}

BlogSave()