/*=====================WIDGET-ADMIN=====================*/
const widgetAuthorized = {
    Authorized: function () {
        window.onload = function () {
            $.ajax({
                url: "/Rentabike/authorizatedController",
                type: "post",
                success: function (response) {
                    if (response === "FAILED") {
                        window.location.replace("http://localhost:8088/Rentabike/page-misc-not-authorized.html");
                    }
                }
            });
        };
    },
    start: function () {
        this.Authorized();
    }
};
widgetAuthorized.start();