const widget = {
    openNotify: function () {
        $("#notifyOpen").on("click", function () {
            $.ajax({
                url: "/Rentabike/notifyController",
                type: "post",
                data: {
                    mode: "NotifyTotalHandler",
                    setUpNotify: "openNotify"
                },
                success: function (response) {
                    if (response !== "FAILED") {
                        $("#notifyCalculation").text(response);
                    }
                }
            });
        
        notifyStatus = false;
        });
    },
    start: function () {
        this.openNotify();
    }
};
widget.start();